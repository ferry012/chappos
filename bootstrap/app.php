<?php

ini_set('precision', 20);

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/constants.php';
require_once __DIR__.'/helpers.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->withFacades();

$app->withEloquent();

/*
|--------------------------------------------------------------------------
| Load Configuration
|--------------------------------------------------------------------------
|
| Now we will load configuration from the config folder
|
*/
if (env('APP_DEBUG')) {
    $app->configure('debugbar');
}
$app->configure('constants');
$app->configure('auth');
$app->configure('cache');
$app->configure('database');
$app->configure('cors');
$app->configure('hashids');
$app->configure('cart');
$app->configure('api');
$app->configure('jwt');
$app->configure('laravel-omnipay');
$app->configure('dompdf');


/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
    App\Http\Middleware\TrimStrings::class,
    App\Http\Middleware\ConvertEmptyStringsToNull::class,
    App\Http\Middleware\RemoveFloatComma::class,
    Barryvdh\Cors\HandleCors::class,
]);

$app->routeMiddleware([
    'auth' => App\Http\Middleware\CheckClientCredentials::class,
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

$app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\AuthServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);

$app->register(Illuminate\Redis\RedisServiceProvider::class);
$app->register(Flipbox\LumenGenerator\LumenGeneratorServiceProvider::class);
$app->register(Barryvdh\Cors\ServiceProvider::class);
$app->register(Webpatser\Uuid\UuidServiceProvider::class);
$app->register(App\Providers\DingoServiceProvider::class);
//$app->register(App\Providers\CartServiceProvider::class);
$app->register(Tymon\JWTAuth\Providers\LumenServiceProvider::class);
$app->register(App\Providers\ApiServiceProvider::class);
$app->register(Urameshibr\Providers\FormRequestServiceProvider::class);
$app->register(Ignited\LaravelOmnipay\LumenOmnipayServiceProvider::class);
$app->register(Barryvdh\DomPDF\ServiceProvider::class);
$app->register(Milon\Barcode\BarcodeServiceProvider::class);
app('Dingo\Api\Auth\Auth')->extend('jwt', function ($app) {
    return new Dingo\Api\Auth\Provider\JWT($app['Tymon\JWTAuth\JWTAuth']);
});
$app->register(Flugg\Responder\ResponderServiceProvider::class);

if (env('APP_DEBUG')) {
    $app->register(Clockwork\Support\Lumen\ClockworkServiceProvider::class);
    $app->alias('Debugbar', Barryvdh\Debugbar\Facade::class); //optional
    $app->register(Barryvdh\Debugbar\LumenServiceProvider::class);
}

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->router->group([
    'namespace' => 'App\Http\Controllers',
], function ($router) {
    require __DIR__.'/../routes/web.php';
});

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->group(['namespace' => 'App\Api\Controllers',], function ($router) {
        require __DIR__.'/../routes/api.php';
    });
});

return $app;
