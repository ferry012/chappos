<?php

use App\Support\Str;

if (!function_exists('get_constant')) {

    function get_constant($key, $default = null)
    {
        return config('constants'.'.'.$key);
    }
}

if (!function_exists('cache')) {
    /**
     * Get / set the specified cache value.
     *
     * If an array is passed, we'll assume you want to put to the cache.
     *
     * @param dynamic  key|key,default|data,expiration|null
     *
     * @return mixed
     *
     * @throws \Exception
     */
    function cache()
    {
        $arguments = func_get_args();

        if (empty($arguments)) {
            return app('cache');
        }

        if (is_string($arguments[0])) {
            return app('cache')->get($arguments[0], isset($arguments[1]) ? $arguments[1] : null);
        }

        if (!is_array($arguments[0])) {
            throw new Exception(
                'When setting a value in the cache, you must pass an array of key / value pairs.'
            );
        }

        if (!isset($arguments[1])) {
            throw new Exception(
                'You must specify an expiration time when setting a value in the cache.'
            );
        }

        return app('cache')->put(key($arguments[0]), reset($arguments[0]), $arguments[1]);
    }
}

if (!function_exists('app_path')) {
    /**
     * Get the path to the application folder.
     *
     * @param string $path
     *
     * @return string
     */
    function app_path($path = '')
    {
        return app('path').($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}

if (!function_exists('config_path')) {
    /**
     * Get the configuration path.
     *
     * @param string $path
     *
     * @return string
     */
    function config_path($path = '')
    {
        return app()->basePath().'/config'.($path ? '/'.$path : $path);
    }
}

if (!function_exists('request')) {

    function request($key = null, $default = null)
    {
        if (is_null($key)) {
            return app('request');
        }

        if (is_array($key)) {
            return app('request')->only($key);
        }

        return data_get(app('request')->all(), $key, $default);
    }

}

if (!function_exists('today')) {

    function today()
    {
        return app(Carbon\Carbon::class)->now();
    }

}

if (!function_exists('now')) {

    function now()
    {
        return app(Carbon\Carbon::class)->now();
    }

}

if (!function_exists('is_multi_array')) {

    function is_multi_array($arr, $except = '')
    {
        array_forget($arr, $except);
        rsort($arr);

        return isset($arr[0]) && is_array($arr[0]);
    }

}

if (!function_exists('db')) {
    function db()
    {
        $arguments = func_get_args();

        if (empty($arguments)) {
            return app('db');
        }

        return app('db')->connection($arguments[0]);
    }
}

if (!function_exists('cart')) {
    function cart()
    {
        $arguments = func_get_args();

        if (empty($arguments)) {
            return app('cart');
        }

        return app()->make('cart', ['name' => $arguments[0]]);
    }
}

if (!function_exists('cart')) {
    function cart()
    {
        $arguments = func_get_args();

        if (empty($arguments)) {
            return app('cart');
        }

        return app()->make('cart', ['name' => $arguments[0]]);
    }
}

if (!function_exists('basket')) {
    function basket()
    {
        $arguments = func_get_args();

        if (empty($arguments)) {
            return app('basket');
        }

        $params = [
            'basket_id'   => array_get($arguments, 0, ''),
            'basket_name' => array_get($arguments, 1, ''),
        ];

        return app()->make('basket', $params);
    }
}

if (!function_exists('auth')) {
    function auth()
    {
        return app('auth');
    }
}

if (!function_exists('round_to_5_or_0_cents')) {
    function round_to_0_or_5_cents($value)
    {
        $valueInString = (string)number_format($value, 2);
        if (strpos($valueInString, '.') === 0) {
            $valueInString .= '.00';
        }

        $valueArray = explode('.', $valueInString);
        $valueArray[0] = str_replace(',', '', $valueArray[0]);
        $substringValue = substr($valueArray[1], 1);
        $replaceValue = $substringValue;

        if ($substringValue > 0 && $substringValue < 5) {
            $replaceValue = '0';
        }

        if ($substringValue > 5) {
            $replaceValue = '5';
        }

        if ($substringValue !== $replaceValue) {
            $tempValue = str_replace(substr($valueArray[1], 1), $replaceValue, substr($valueArray[1], 1));
            $tempValue = substr($valueArray[1], 0, 1).$tempValue;

            return (float)($valueArray[0].'.'.$tempValue);
        }

        return (float)$value;
    }
}

if (!function_exists('isnull')) {

    function isnull($var)
    {
        return null === $var;
    }
}

if (!function_exists('else_empty')) {
    /**
     * If variable is empty then return default value
     *
     * @param string $var
     * @param string $default null
     *
     * @return string
     */
    function else_empty($var, $default = null)
    {
        return empty($var) ? value($default) : $var;
    }
}

if (!function_exists('else_null')) {

    function else_null($var, $default = null)
    {
        return null === $var ? value($default) : $var;
    }
}

if (!function_exists('not_null')) {

    function not_null($var, $default = null)
    {
        if (null !== $var && null !== $default) {
            return value($default);
        }

        return null !== $var;
    }
}

if (!function_exists('user_data')) {
    function user_data($key, $default = null)
    {
        $data = [];

        if (request()->has('user_data')) {
            $data = request('user_data');
        }

        return data_get($data, $key, $default);
    }
}

if (!function_exists('user_id')) {
    function user_id()
    {
        return user_data('usr_id');
    }
}

if (!function_exists('coy_id')) {
    function coy_id()
    {
        return request('coy_id', 'CTL');
    }
}

if (!function_exists('pos_locations')) {
    function pos_locations($id = null)
    {

        $result = app('cache')->remember('table.pos_location_list', now()->endOfDay(), function () {

            return db('pg')->table('pos_location_list')->get()->map(function ($row) {

                foreach (get_object_vars($row) as $k => $v) {
                    if (is_string($row->{$k})) {
                        $row->{$k} = trim($v);
                    }
                }

                return $row;
            });
        });

        if (is_array($id)) {
            return $result->whereIn('loc_id', $id)->mapWithKeys(function ($loc) {
                return [$loc->loc_id => $loc];
            })->all();
        } else if ($id) {
            return $result->where('loc_id', trim($id))->first();
        }

        return $result;
    }
}

if (!function_exists('pos_functions')) {
    function pos_functions($func_type = '', $func_id = '')
    {

        $result = app('cache')->remember('table.pos_function_list', now()->endOfDay(), function () {

            return db('pg')->table('pos_function_list')->get()->map(function ($row) {

                foreach (get_object_vars($row) as $k => $v) {
                    if (is_string($row->{$k})) {
                        $row->{$k} = trim($v);
                    }
                }

                return $row;
            });
        });

        $collection = collect($result);

        $collection = $collection->when(!empty($func_type), function ($result) use ($func_type) {
            return $result->where('func_type', trim($func_type));
        });
        $collection = $collection->when(!empty($func_id), function ($result) use ($func_id) {
            return $result->where('func_id', trim($func_id));
        });

        return $collection->all();
    }
}

if (!function_exists('hsg_delivery')) {
    function hsg_delivery($id = null)
    {

        $result = app('cache')->remember('table.hsg_delv_mthd', now()->endOfDay(), function () {

            return db('pg')->table('hsg_delv_method')->where('status_level', '>=', 1)->orderBy('display_seq', 'asc')->get()->map(function ($row) {

                foreach (get_object_vars($row) as $k => $v) {
                    if (is_string($row->{$k})) {
                        $row->{$k} = trim($v);
                    }
                }

                return $row;
            });
        });

        if ($id) {
            $delv = $result->where('delv_method', $id)->first();

            if ($delv === null) {
                return null;
            }

            $time_from = (int)str_replace(':', '', $delv->time_from ?? '00:00:00');
            $time_to = (int)str_replace(':', '', $delv->time_to ?? '23:59:59');
            $time_now = (int)date('Gis');

            if ($delv->status_level > 0 && $time_now > $time_from && $time_now < $time_to) {
                return $delv;
            } else {
                return null;
            }
        }

        return $result;
    }
}

if (!function_exists('is_num_formatted')) {
    function is_num_formatted($value)
    {
        if (empty($value)) {
            return false;
        }

        /*
         *  Match following formats only
         *  1,234
         *  -1,234
         *  +1,234
         *  1,234,567~
         *  1,234.1
         *  1,234.12~
         *
         *  Not allowed formats
         *  1
         *  1,
         *  1,23
         *  1,123.
         */

        return preg_match('/^([-+]{0,1})[1-9]{1,3}(,[0-9]{3})+([.]{1}[0-9]{1,})?$/', $value);
    }
}

if (!function_exists('setting')) {

    function setting($key, $default = null)
    {
        // Model seems to be "cached" and trigger it to do live query to db
//        $settings = app('cache')->remember('table.settings.' . time(), now()->endOfDay(), function () {
//            return new \App\Utils\Setting();
//        });
//
//        if ($key === null) {
//            return $settings;
//        }
//
//        return $settings->get($key, $default);

        $settings = app('cache')->remember('table.setting_pos', now()->endOfDay(), function () {

            return db('pg')->table('setting')->get()->map(function ($row) {

                foreach (get_object_vars($row) as $k => $v) {
                    if (is_string($row->{$k})) {
                        $row->{$k} = trim($v);
                    }
                }

                if ($row->set_type === 'boolean' || $row->set_type === 'bool') {
                    $row->set_value = strtolower($row->set_value);

                    if ($row->set_value === 'false') {
                        $row->set_value = false;
                    } elseif ($row->set_value === 'true') {
                        $row->set_value = true;
                    } else {
                        $row->set_value = (bool)$row->set_value;
                    }
                } elseif ($row->set_type === 'int' || $row->set_type === 'integer') {
                    $row->set_value = (int)$row->set_value;
                } elseif ($row->set_type === 'float') {
                    $row->set_value = (float)$row->set_value;
                }

                return $row;
            });

        });

        if ($key === null) {
            return $settings;
        }

        $return = $settings->where('set_key', $key)->first();

        if ($return) {
            return $return->set_value;
        } else {
            return $default;
        }
    }
}

if (!function_exists('obj_to_array')) {

    function obj_to_array($data, Closure $callback = null)
    {
        return collect($data)->map(static function ($x) use ($callback) {
            $x = isnull($callback) ? $x : $callback($x);

            return (array)$x;
        })->toArray();
    }
}

if (!function_exists('clean_string')) {

    function clean_string($string, $format='') {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        if ($format=='lower') {
            $string = strtolower($string);
        }
        if ($format=='upper') {
            $string = strtoupper($string);
        }

        return $string;
    }

}

if (!function_exists('is_effective')) {

    /*
     *   Check effectivity of given datetime dataset from DB
     *      eff_data :  {"time_from": "00:00:00", "time_to": "23:59:59", "day_of_week": [0, 1, 2, 3, 4, 5, 6], "blocked_dates": ["2021-02-14"]}
     */

    function is_effective($eff_from, $eff_to, $eff_data = "")
    {

        if ($eff_data != "" && $eff_data != NULL && is_string($eff_data)) {
            $eff_data = json_decode($eff_data);
        }

        $eff = (strtotime($eff_from) < time() && strtotime($eff_to) > time());

        // blocked_dates
        if ($eff && isset($eff_data->blocked_dates)) {
            if (in_array(date('Y-m-d'), $eff_data->blocked_dates)) $eff = false;
        }

        // day_of_week
        if ($eff && isset($eff_data->day_of_week)) {
            if (!in_array(date('w'), $eff_data->day_of_week)) $eff = false;
        }

        // time_from, time_to
        if ($eff && isset($eff_data->time_from) && isset($eff_data->time_to)) {
            if (!(strtotime($eff_data->time_from) < strtotime("now") && strtotime($eff_data->time_to) > strtotime("now"))) $eff = false;
        }

        return $eff;
    }
}

if (!function_exists('encrypt_decrypt')) {
    function encrypt_decrypt($id = null, $action = 'encrypt')
    {
        $salt = 'Software@CTL2021';
        $id = trim($id);

        if ($action == 'signature') {
            $i1 = $id;
            $i2 = '@Challenger.';
            $i3 = substr($i1, -3);

            return md5($i1.$i2.$i3);
        } else if ($action == 'decrypt') {
            // Decryption
            $hashLibrary = new \Hashids\Hashids($salt, 16);

            $id_hex = $hashLibrary->decodeHex($id);
            $key = hex2bin($id_hex);

            return $key;
        } else {
            // Encryption
            $hashLibrary = new \Hashids\Hashids($salt, 16);

            $id_hex = bin2hex($id);
            $key = $hashLibrary->encodeHex($id_hex);

            return $key;
        }
    }
}

if (!function_exists('ctl_mail')) {

    function ctl_mail($to, $subject, $message, $additional_headers = [], $other = [], $from_addr = 'cherps@challenger.sg')
    {
        $endpoint = "https://ctlmailer.api.valueclub.asia/sends";
        $postdata = [
            'from_address'    => $from_addr,
            'recipients'      => $to,
            'subject'         => $subject,
            'body'            => $message,
            'copy_recipients' => $additional_headers['cc'] ?? '',
        ];

        if (isset($other['base64_filename']) && $other['base64_content']) {
            $postdata['base64_filename'] = $other['base64_filename'];
            $postdata['base64_content'] = $other['base64_content'];
        }
        if (isset($other['base64_filename1']) && $other['base64_content1']) {
            $postdata['base64_filename1'] = $other['base64_filename1'];
            $postdata['base64_content1'] = $other['base64_content1'];
        }

        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->post($endpoint, [
            'headers' => ['Authorization' => '8f413c39b4d1b4e7c584943df22e7a8c', 'Content-Type' => 'application/json'],
            'body'    => json_encode($postdata),
        ]);

        try {
            $content = json_decode($response->getBody()->getContents(), true);

            if (isset($content['code']) && $content['code'] == 1) {
                return true;
            }

        } catch (Exception $e) {
            return false;
        }

        return false;
    }

}

if (!function_exists('customer_group_ind')) {

    function customer_group_ind($customerGroup)
    {
        if (Str::upper($customerGroup) === 'MEMBER') {
            $priceInd = ['ALL', 'MBR', 'MEMBER'];
        } else if (Str::upper($customerGroup) === 'MBR_STAFF') {
            $priceInd = ['ALL', 'MBR', 'MEMBER', 'MBR_STAFF'];
        } else if ($customerGroup == "GUEST" || $customerGroup == "") {
            $priceInd = ['ALL'];
        } else {
            $priceInd = ['ALL', $customerGroup];
        }

        return $priceInd;
    }

}

if (!function_exists('hc_emarsys_send_star_warranty')) {

    function hc_emarsys_send_star_warranty($data)
    {
        $endpoint = env('API_URL_VALUECLUB');
        $endpoint .= 'api/emarsys/STAR_WARRANTY_0801';
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->post($endpoint, [
            'headers' => ['Content-Type' => 'application/json'],
            'body'    => json_encode($data),
        ]);

        $content = $response->getBody()->getContents();

        db()->table('api_log')->insert([
            "api_client"      => 'emarsys',
            "api_description" => 'EMAIL_'.trim($data['trans_id']),
            "request_method"  => 'POST',
            "request_url"     => $endpoint,
            "request_header"  => '{}',
            "request_body"    => json_encode($data),
            "request_on"      => date('Y-m-d H:i:s'),
            "response_header" => '{}',
            "response_body"   => $content,
            "response_on"     => date('Y-m-d H:i:s'),
            "error_msg"       => '',
            "created_on"      => date('Y-m-d H:i:s'),
        ]);

        return $content;
    }

}

if (!function_exists('number_of_decimals')) {

    function number_of_decimals($value)
    {
        if ((int)$value == $value) {
            return 0;
        } else if (!is_numeric($value)) {
            return false;
        }

        return strlen($value) - strrpos($value, '.') - 1;
    }

}

if(!function_exists('ctl_next_invoice_number')){
    function ctl_next_invoice_number(){
        $year = now()->format('y');
        $diff = (int)$year - 18;
        $prefix = 'HI' . chr(65 + $diff); // Last for 26 years start from 2018

        return app('generator')->with('seq_alphanum')->name("HACHI.INVOICE_{$year}")->prefix($prefix)->start('00001')->length(5)->generate();
    }
}

if(!function_exists('gst')){
    function gst($key = 'CHALLENGER_GST'){
        $sql = "SELECT CASE WHEN COALESCE(MAX(set_value),'')='' THEN '[]' ELSE MAX(set_value) END as values  FROM setting WHERE set_key = ?";
        $result = db()->select($sql, [$key])[0];
        return json_decode($result->values);
    }
}