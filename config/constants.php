<?php
return [
    'coy_id'    => 'CTL',
    's3'        => [
        'base_url'              => 'https://cdn.hachi.tech/',
        'product_images'        => 'https://cdn.hachi.tech/assets/images/product_images/',
        'product_images_thumb'  => 'https://cdn.hachi.tech/assets/images/product_images_thumb/',
        'product_color'         => 'https://cdn.hachi.tech/assets/images/color/',
    ],
    'url'      => [
        'ctl'           => 'https://www.challenger.com.sg/',
        'hachi'         => 'https://www.hachi.tech/',
        'invoice'       => 'https://www.challenger.sg/invoice/',
        'ereceipt'      => 'https://www.challenger.sg/invoice/ereceipt/',
        'ereceipt-dev'  => 'http://localhost-ctl.chugo.sg:8881/invoice/ereceipt/',
    ]
];