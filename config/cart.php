<?php

return [
    /*
    |--------------------------------------------------------------------------
    | User Id Closure
    |--------------------------------------------------------------------------
    |
    | Closure to find logged in user id; Return null/false if not logged in
    |
    */
    'user_id' => function(){
        return '';
    },
    /*
    |--------------------------------------------------------------------------
    | Save on demand
    |--------------------------------------------------------------------------
    |
    | When set to true, if a cart doesn't exist on database you'll be given a
    | new instance of cart object. You have to manually save the cart to DB by
    | $cart->save() . Useful if you want to avoid creating unnecessary empty carts
    | in database. But be sure to save cart before adding items
    |
    */
    'save_on_demand' => false,
];