<?php

return [
    'length'   => env('HASHIDS_LENGTH', 8),
    'salt'     => env('HASHIDS_SALT', 'YBlHbgUpSHt9E6xhlgHupcDnXncd3hTI'),
    'alphabet' => env('HASHIDS_ALPHABET', 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'),
];