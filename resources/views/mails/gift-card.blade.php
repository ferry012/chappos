<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title></title>
</head>
<body>

<table style="height: 95px; float: left;" border="1" cellspacing="0" cellpadding="1">
    <thead>
    <tr>
        <th>invoice</th>
        <th>cust id</th>
        <th>email addr</th>
        <th>cust name</th>
        <th>item id</th>
        <th>item desc</th>
        <th>trans id</th>
        <th>amount</th>
        <th>remark</th>
    </tr>
    </thead>
    <tbody>
    @foreach($gifts as $gift)
        <td>{{$order->invoice_id}}</td>
        <td>{{$order->customer_id}}</td>
        <td>{{$order->customer_email}}</td>
        <td>{{$order->customer_name}}</td>
        <td>{{$gift->item_id}}</td>
        <td>{{$gift->item_desc}}</td>
        <td>{{$order->order_num}}</td>
        <td>{{$gift->unit_price * $gift->qty_ordered}}</td>
        <td>{{$gift->remark}}</td>
    @endforeach
    </tbody>
</table>
</body>
</html>