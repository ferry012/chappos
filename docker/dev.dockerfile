FROM 923763705594.dkr.ecr.ap-southeast-1.amazonaws.com/basedocker:latest
LABEL maintainer="yongsheng@challenger.sg"

RUN apt-get update && apt-get install -y php7.2-pgsql
RUN echo "extension=pgsql.so" >> /etc/php/7.2/apache2/php.ini

#RUN apt-get install -y libgmp-dev re2c libmhash-dev libmcrypt-dev file
#RUN ln -s /usr/include/x86_64-linux-gnu/gmp.h /usr/local/include/
#RUN docker-php-ext-configure gmp
#RUN docker-php-ext-install gmp

#RUN docker-php-ext-install bcmath

RUN apt-get update && apt-get install -y php7.2-bcmath

RUN apt-get install -y php7.2-gmp
RUN echo "extension=php_gmp.so" >> /etc/php/7.2/apache2/php.ini


###
### WORKING DIR
###

WORKDIR /var/www/html
COPY ./ /var/www/html/
COPY ./docker/stage.env /var/www/html/.env
COPY ./docker/vhost.conf /etc/apache2/sites-enabled/000-default.conf

RUN tail /var/www/html/.env

RUN chown www-data -R /var/www/html/*
RUN chmod 775 -R *

RUN apt-get update && apt-get install nano -y

RUN composer install

#RUN php artisan serve
