
# POSAPI
Mission critical backend API for Challenger's sales system (CHROOMS),
powering the POS System, Challenger.sg, micro-sites and marketplace integrations.

Project is developed on Laravel 5.6 (requires PHP 7)
*****

### Getting started on local development
Create .env file for Laravel environment (or copy from stage.env)
> cp ./docker/stage.env ./.env

To build the docker project on your local machine:
> docker build -t chappos .

Run the container locally.
Use [bind mount](https://docs.docker.com/storage/bind-mounts/) to map local directory into the container:
> docker run -v $(pwd):/var/www/html -p 8090:80 --name posapi --rm chappos

The container is now running with your local files from this directory.
Changes made to your local files will be immediately reflect on the container.

Before you can load the website from the container, libraries (i.e. composer or npm) must be run in this directory.

> composer install --ignore-platform-reqs \
> php -d memory_limit=512M /usr/local/bin/composer update

Open browser to http://localhost:8090 to preview this site
*****

### Development & Github practice
Github Development Branch: **development**

Do not commit the following files: \
composer.lock \
public\*

Always check before committing following files: \
Dockerfile
composer.json
docker\*