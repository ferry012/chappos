<?php

/*
$router->post('auth/login', 'Auth\LoginController@login');
$router->post('auth/logout', 'Auth\LoginController@logout');
$router->get('auth/me', 'Auth\LoginController@me');
$router->get('auth/refresh', 'Auth\LoginController@refresh');

$router->get('test', ['middleware' => 'api.auth', 'uses' => 'CartController@index']);
$router->get('cart', 'CartController@show');
$router->get('member/trans', 'MemberController@transaction');
$router->get('member/{id}', 'MemberController@show');
$router->get('member/{id}/associate', 'MemberController@associate');
$router->get('cart/create', 'CartController@create');
$router->get('cart/empty', 'CartController@clear');
$router->get('cart/add', 'Cart\AddToCart');
$router->get('cart/add/pwp', 'CartController@addPWP');
$router->get('cart/update', 'CartController@update');
$router->get('cart/delete', 'CartController@delete');
$router->get('cart/place_order', 'CartController@confirmCartToOrder');
$router->get('cart/id2', 'CartController@id2');
$router->get('cart/id3', 'CartController@id3');
//$router->get('cart/transfer_to_pos', 'CartController@transferToPOS');

$router->get('search/{mode}/{keyword}/', ['uses' => 'ProductController@search']);
$router->get('product/{productId}/detail', ['uses' => 'ProductController@detail']);
$router->get('product/{productId}/pwp', 'ProductController@purchaseWithPurchaseProducts');
$router->get('product/{productId}/complementary', 'ProductController@complementaryProducts');
$router->get('product/{productId}/stock', 'InventoryController@index');

$router->get('member/{mbrId}/coupon/unused', 'CouponController@unused');
*/

$router; // Router for pos.api.valueclub.asia/api/xxx

//
// Public API
//

$router->get('ping', 'SettingController@ping');


$router->get('inventory/hachi/{item_ids}', 'TransactionController@hsgGetStocks');
$router->post('inventory/hachi', 'TransactionController@hsgCheckStocks');

//
// POS Open API - No log in required
//

$router->get('function/version', 'SettingController@device_version');
$router->get('function/device', 'SettingController@device_list'); // No use?
$router->get('function/device/{deviceId}', 'SettingController@device_get'); // Get loc_id & pos_id assigned to device
$router->post('function/device', 'SettingController@device_save'); // Save latest device info

$router->post('function/staff_login', 'SettingController@staff_login');
$router->get('function/staff_validate/{userId}', 'SettingController@staff_validate');
$router->get('function/promoter_validate/{userId}', 'SettingController@promoter_validate');

$router->get('function/store_location', 'SettingController@store_location');
$router->get('function/delivery_location', 'SettingController@delivery_location');

$router->post('valueclub/qr_login', 'SettingController@qr_login');

$router->get('function/offline_users/{locId}', 'SettingController@get_offline_users');

$router->get('logkeeper/{loc_id}/{pos_id}', 'SettingController@logreader'); // No use
$router->post('logkeeper/{loc_id}/{pos_id}', 'SettingController@logkeeper'); // No use


//
// POS Private API - Available to logged in users only
//

//$router->group([
//    'middleware' => ['auth'],
//], function ($router) {

    $router->get('staff_purchase/{userId}', 'SettingController@staff_purchase');
    $router->get('function/keylist', 'SettingController@keylist'); // Retrieve list of hotkeys for POS
    $router->get('coupon/{func_tag}', 'SettingController@generic_coupons'); // List of coupons in popup
    $router->get('coupon/payment/{mbrId}', 'SettingController@payment_voucher');

    $router->get('function/eod/{loc_id}/{pos_id}', 'SettingController@eod_get_payments');
    $router->post('function/eod/{loc_id}/{pos_id}', 'SettingController@eod_post_payments');
    $router->post('function/eod_reprint/{loc_id}/{pos_id}', 'SettingController@eod_reprint');
    $router->post('function/transactions_summary/{loc_id}/{pos_id}', 'SettingController@eod_transactions_summary');
    $router->post('function/sales_summary/{loc_id}/{pos_id}', 'SettingController@sales_summary');
    

    $router->get('receipt/{trans_id}', 'TransactionController@getOneReceipt');
    $router->get('deposit', 'TransactionController@getDeposits');
    $router->get('invoice', 'TransactionController@getSmsInvoice');
    $router->get('invoice/{invoice_id}', 'TransactionController@getOneInvoice');

    $router->post('invoice/{trans_id}/lot', 'TransactionController@updateReceiptLot');
    $router->post('invoice/{trans_id}/salesperson', 'TransactionController@updateReceiptSalesperson');
    $router->post('invoice/{trans_id}/krisflyer', 'TransactionController@updateKrisflyer');
    $router->post('invoice/{trans_id}/collection', 'TransactionController@updateCollection');

    $router->post('customerservice', 'SettingController@cs_feedback');
    $router->post('ereceipt', 'SettingController@ereceipt');

    $router->post('paymentgateway', 'PaygatewayController@gatewaypayment');
    $router->post('paymentgateway/process', 'PaygatewayController@gatewayprocess');
    $router->post('paymentqr/{mode}', 'PaygatewayController@qrpayment');
    $router->post('paymentgateway/krispay', 'PaygatewayController@krispay_status');
    $router->get('paymentgateway/custom_validate', 'PaygatewayController@custom_validate');
    $router->post('paymentgateway/uob_paynow', 'PaygatewayController@uob_paynow');
    $router->post('paymentgateway/uob_paynow_status', 'PaygatewayController@uob_paynow_status');

    $router->post('valueclub/register', 'SettingController@vc_register');
    $router->get('valueclub/item/{mbr_type}/{mode}', ['uses' => 'SettingController@vc_item']);
    $router->get('valueclub/voucher/{mbr_id}/{status_level?}', ['uses' => 'SettingController@vc_voucher']);
    $router->post('valueclub/validate', 'SettingController@vc_validate'); // Check member id
    $router->post('valueclub/verify', 'SettingController@vc_verify'); // Check member id
    $router->post('valueclub/rebate/reserve', 'SettingController@vc_rebate_reserve');
    $router->post('valueclub/rebate/cancel', 'SettingController@vc_rebate_cancel');
    $router->get('valueclub/rebate/check/{mbr_id}/{trans_id}', 'SettingController@vc_trans_rebate');
    $router->get('valueclub/transaction/check/{mbr_id}/{item_id}', 'SettingController@vc_trans_item');

    $router->post('nea/eligibilty', 'NeaController@nea_eligibilty');

    $router->get('coupons/grab/{header?}', 'CouponController@grab_coupons');

//});

//
// Reporting/Sync API
//

$router->get('incallsr/{sr_id}', 'TransactionController@getIncallSR'); // For Incall System to retrieve the receipt

$router->get('report/order_resource', 'ReportingController@order_resource');
$router->get('report/unposted', 'ReportingController@unposted_order');
$router->get('report/coupon_date', 'ReportingController@crm_coupon_date');
$router->get('report/crm_promo', 'ReportingController@crm_promo_sync_to_cherps');
$router->get('report/ims_item', 'ReportingController@ims_item_sync_from_cherps');
$router->get('report/pos_cherps', 'ReportingController@pos_from_cherps2_to_cherps');
$router->get('report/pos_cherps_single/{id}/', 'ReportingController@pos_from_cherps2_to_cherps_single');
$router->get('report/crm_promo_price', 'ReportingController@crmpromo_from_chrooms_to_cherps2');

$router->get('report/crm_announcements', 'ReportingController@crm_announcements');
$router->get('report/crm_announcements/{id}', 'ReportingController@crm_announcements');

$router->get('report/hpinklist', 'ReportingController@hp_link_list');

$router->get('report/product_id', 'ReportingController@product_id_last_modified'); // For loading offline data

/* For fraud analysis*/
$router->get('fraud/analysis_list', 'PaymentAnalysisController@full_analysis_list');
$router->post('fraud/analysis_list_by_ho', 'PaymentAnalysisController@analysis_list_by_ho');
$router->post('fraud/analysis_list_by_hi', 'PaymentAnalysisController@analysis_list_by_hi');
$router->post('fraud/analysis_detail', 'PaymentAnalysisController@analysis_detail');

