<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\DB;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/ping', function () {
    return response()->json([
        'code' => '200',
        'msg'  => 'pong',
    ]);
});

$router->get('/clear_cache', 'ClearCache@clearAll');
$router->get('/clear_cache/{tag}/{id}', 'ClearCache@clearByPattern');

$router->get('/hachi/posting_order/{id}', 'Hachi\PostingOrder');
$router->get('/hachi/async_inv/{id}/{action}', 'Hachi\AsyncInventory');
$router->get('/hachi/order_handle_stock/{id}/{action}', 'Hachi\OrderHandleStock');

$router->get('orders/deploycheck', 'Orders\ThirdPartyInvoiceController@deployCheck');

$router->group([
    'middleware' => ['auth'],
], function ($router) {

    /*
     * Members
     */
    $router->get('members/{id}', 'Members\MemberController@show');

    /*
     * Staff
     */
    $router->get('staff/{id}/purchase', 'ShowStaffPurchaseDetail');

    /*
     * Products
     */
    $router->get('products/search/{mode}', 'Products\Search');
    $router->get('products/search/{mode}/{keyword}', 'Products\Search');
    $router->get('products/item_promo/{promo_id}', 'Products\Promo');
    $router->get('products/{product}/stock', 'Products\ProductController@stock');
    $router->get('products/{product}/pwp', 'Products\ProductController@purchaseWithPurchase');
    $router->get('products/{product}/complimentary', 'Products\ProductController@complimentary');
    $router->get('products/{product}/ssew', 'Products\ProductController@ssewPrice');
    $router->get('products/{product}', 'Products\ProductController@show');
    $router->get('products/{product}/price_tag', 'Products\ProductController@priceTag');
    $router->get('products/price_tag/change', 'Products\ProductController@priceChangeTag');

    $router->get('star_redemption', 'Products\StarRedemption@catalog');

    /*
     * Baskets
     */
    $router->get('baskets', 'Basket\BasketController@index');
    $router->post('baskets/0', 'Basket\BasketController@create');
    $router->post('baskets/create', 'Basket\BasketController@create');
    $router->get('baskets/current', 'Basket\BasketController@current');
    $router->get('baskets/recall', 'Basket\BasketController@recall');
    $router->get('baskets/recall/{id}', 'Basket\BasketController@showRecallDetail');
    $router->post('baskets/{basket_id}/remove/{basket_line_id}', 'Basket\BasketController@remove');
    $router->post('baskets/{basket_id}/clear', 'Basket\BasketController@clear');
    $router->post('baskets/{basket_id}/update/{basket_line_id}', 'Basket\BasketController@update');
    $router->post('baskets/{basket_id}/destroy', 'Basket\BasketController@destroy');
    $router->post('baskets/{basket_id}/add', 'Basket\AddToBasket');
    $router->put('baskets/{basket_id}/discounts', 'Basket\BasketController@addDiscount');
    $router->post('baskets/{basket_id}/rebates', 'Basket\BasketController@applyRebates');
    $router->post('baskets/{basket_id}/cancel_rebates', 'Basket\BasketController@cancelRebates');
    $router->post('baskets/{basket_id}/order', 'Basket\StoreBasketAndOrder');
    $router->get('baskets/{basket_id}', 'Basket\BasketController@show');
    $router->delete('baskets/{basket_id}', 'Basket\BasketController@destroy');

    /*
     *  Hachi basket
     */
    $router->get('baskets/hachi/{basket_id}', 'Basket\Hachi\GetBasket');
    $router->get('baskets/hachi/order/{id}', 'Basket\Hachi\GetOrder');
    $router->post('baskets/hachi/{basket_id}/coupon', 'Basket\Hachi\ApplyCoupon');
    $router->delete('baskets/hachi/{basket_id}/coupon', 'Basket\Hachi\RemoveCoupon');
    $router->post('baskets/hachi/{basket_id}', 'Basket\Hachi\AddLine');
    $router->delete('baskets/hachi/{basket_id}', 'Basket\Hachi\DeleteLine');
    $router->post('baskets/hachi/{basket_id}/place_order', 'Basket\Hachi\PlaceOrder');

    /*
     *  Orders
     */
    $router->get('orders', 'Orders\OrderController@index');
    $router->post('orders/create', 'Orders\OrderController@create');
    $router->get('orders/posting', 'Orders\OrderController@orderPosting');
    $router->post('orders/{id}/process', 'Orders\OrderController@process');
    $router->post('orders/{id}/expire', 'Orders\OrderController@expire');
    $router->get('orders/{id}', 'Orders\OrderController@show');
    $router->get('orders/{id}/invoice', 'Orders\OrderController@invoice');
    $router->get('orders/{id}/receipt', 'Orders\ReceiptController@show');
    $router->get('orders/{id}/reprint_receipt', 'Orders\ReceiptController@reprint');

    $router->post('orders/hachi/refund_invoice', 'Hachi\MissingOrderTransactionRefund');

    $router->post('orders/oc/save', 'Orders\SpecialInvoiceController@oc_invoice');
    $router->post('orders/rd/save', 'Orders\SpecialInvoiceController@rd_invoice');
    $router->post('orders/od/save', 'Orders\SpecialInvoiceController@od_invoice');
    $router->post('orders/od/void', 'Orders\SpecialInvoiceController@od_void');

    $router->post('quote/save', 'Orders\QuotationController');

    /*
     * Partner/Magento Orders
     */
    $router->get('/orders/3p/serialno/{invoice_num}/{coy_id}', 'Orders\ThirdPartyInvoiceController@serialno');
    $router->get('/orders/3p/validate', 'Orders\ThirdPartyInvoiceController@validate_3p');
    $router->get('/orders/3p/delivered', 'Orders\ThirdPartyInvoiceController@delivered_invoice');
    $router->get('inventory/3p/{coy}', 'Products\ThirdPartyProducts@location');
    $router->get('inventory/3p/{coy}/{item_ids}', 'Products\ThirdPartyProducts@location');
    $router->post('orders/3p/save', 'Orders\ThirdPartyInvoiceController@sms_invoice');

    $router->post('orders/getinvoicestatus', 'Orders\ThirdPartyInvoiceController@getInvDeliveryStatus');
    $router->post('orders/checkstaffexist', 'Orders\ThirdPartyInvoiceController@checkStaffExist');
    //$router->get('orders/deploycheck', 'Orders\ThirdPartyInvoiceController@deployCheck');

    $router->get('orders/3p/{invoice_id}', 'Orders\ThirdPartyInvoiceController@show_invoice');
    $router->post('orders/3p/{invoice_id}', 'Orders\ThirdPartyInvoiceController@update_invoice');

    $router->get('logistics/delivery_slots/{type}/{code}', 'Logistics\DeliveryController@get_slots');
    $router->post('logistics/delivery_slots/{type}/{code}', 'Logistics\DeliveryController@book_slots');
    $router->post('logistics/delivery_slots/unbook', 'Logistics\DeliveryController@unbook_slots');


    $router->get('barcode', 'Barcode');

    /*
     * QUEUE
     */
    $router->post('queue/hachiordercomplete', 'Queue\HachiOrderQueue');
    //$router->post('queue/posordercomplete', 'Queue\PosOrderQueue');

    $router->post('orders/create_recur_subs_invoice','Invoice\CreateRecurringSubscriptionInvoice');

    /*
     * REPORT DATA
     */

    $router->get('reports/sales_data', 'Reports\ReportsController@rep_sales_data');
    $router->post('reports/sales_data', 'Reports\ReportsController@rep_sales_data');
    $router->get('reports/product_spiff', 'Reports\ReportsController@product_spiff');

    /*
     * VOUCHER
     */
    $router->post('voucher/payment', 'PaymentVoucher@sms_voucher_list');
    $router->get('voucher/payment/{item}', 'PaymentVoucher@get_voucher');
});