<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Core\Pricing\PriceCalculator;
use App\Core\Pricing\PriceCalculatorInterface;

class PricingServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(PriceCalculatorInterface::class, function ($app) {
            return $app->make(PriceCalculator::class);
        });
    }
}