<?php

namespace App\Providers;


use Dingo\Api\Provider\LumenServiceProvider;
use App\Auth\Auth;

class DingoServiceProvider extends LumenServiceProvider
{
    /**
     * Register the auth.
     *
     * @return void
     */
    protected function registerAuth()
    {
        $this->app->singleton('api.auth', function ($app) {
            return new Auth($app[\Dingo\Api\Routing\Router::class], $app, $this->config('auth'));
        });
    }
}