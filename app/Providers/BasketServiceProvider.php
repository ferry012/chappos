<?php

namespace App\Providers;

use App\Core\Baskets\BasketCriteria;
use App\Core\Baskets\BasketManager;
use App\Core\Baskets\Contracts\BasketDiscountFactoryContract;
use App\Core\Baskets\Factories\BasketDiscountFactory;
use App\Core\Baskets\Factories\BasketFactory;
use App\Core\Baskets\Factories\BasketLineFactory;
use App\Core\Baskets\Interfaces\BasketCriteriaInterface;
use App\Core\Baskets\Interfaces\BasketInterface;
use App\Core\Baskets\Interfaces\BasketLineInterface;
use App\Core\Baskets\Interfaces\BasketServiceInterface;
use App\Services\Basket\BasketService;
use Illuminate\Support\ServiceProvider;

class BasketServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function register()
    {
        $this->app->bind(BasketCriteriaInterface::class, function ($app) {
            return $app->make(BasketCriteria::class);
        });

        $this->app->singleton(BasketInterface::class, function ($app) {
            return $app->make(BasketFactory::class);
        });

        $this->app->bind(BasketLineInterface::class, function ($app) {
            return $app->make(BasketLineFactory::class);
        });

        $this->app->bind(BasketDiscountFactoryContract::class, function ($app) {
            return $app->make(BasketDiscountFactory::class);
        });

        $this->app->bind(BasketServiceInterface::class, function ($app) {
            return $app->make(BasketService::class);
        });

        $this->app['basket_instances'] = [];
        $this->app->bind('basket', function ($app, $params) {

            $name     = $app['request']->get('basket_name', 'default');
            $basketId = $app['request']->get('basket_id', null);
            if (! empty($params['basket_id'])) {
                $basketId = $params['basket_id'];
            }

            if (! empty($params['basket_name'])) {
                $name = $params['basket_name'];
            }

            //implement singleton carts
            if (empty($cart_instances[$name])) {
                $cart_instances[$name]   = new BasketManager($app[BasketFactory::class], $basketId, $name);
                $app['basket_instances'] = $cart_instances;
            }

            return $app['basket_instances'][$name];
        });



    }
}