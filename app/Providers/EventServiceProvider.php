<?php

namespace App\Providers;

use App\Core\Baskets\Events\BasketSavedEvent;
use App\Core\Baskets\Listeners\ApplyBasketLevelDiscountsListener;
use App\Core\Baskets\Listeners\ApplyDiscountsListener;
use App\Core\Baskets\Listeners\ApplyProductPurchasingLimits;
use App\Core\Baskets\Listeners\ApplyStaffPriceListener;
use App\Core\Baskets\Listeners\RefreshBasketListener;
use App\Core\Orders\Events\OrderSavedEvent;
use App\Core\Orders\Events\OrderWasPaid;
use App\Core\Orders\Listeners\DigitalItemsActivation;
use App\Core\Orders\Listeners\PostOrderToCherps;
use App\Core\Orders\Listeners\PostTransactionToValueClub;
use App\Core\Orders\Listeners\RefreshCustomerInfo;
use App\Core\Orders\Listeners\SendOrderPaidNotification;
use App\Core\Orders\Listeners\UpdateCoupon;
use App\Core\Orders\Listeners\UpdateStaffPurchase;
use App\Core\Orders\Listeners\UpdateCherpsInvoice;
use App\Core\Orders\Listeners\VoucherActivation;
use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent'  => [
            'App\Listeners\EventListener',
        ],
        BasketSavedEvent::class => [
            ApplyProductPurchasingLimits::class,
            ApplyStaffPriceListener::class,
            RefreshBasketListener::class,
            ApplyDiscountsListener::class,
        ],
        OrderSavedEvent::class  => [

        ],
        OrderWasPaid::class     => [
            UpdateCoupon::class,
            UpdateStaffPurchase::class,
            UpdateCherpsInvoice::class,
            PostTransactionToValueClub::class,
            PostOrderToCherps::class,
            RefreshCustomerInfo::class,
            DigitalItemsActivation::class,
            VoucherActivation::class,
            SendOrderPaidNotification::class,
        ],
    ];
}
