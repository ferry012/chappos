<?php

namespace App\Providers;

use App\Core\Factory;
use App\Core\Generators\GeneratorManager;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadProviders();
        $this->mapBindings();
    }

    /**
     * Load up our module providers.
     *
     * @return void
     */
    protected function loadProviders()
    {
        $providers = [
            BasketServiceProvider::class,
            EventServiceProvider::class,
            PricingServiceProvider::class,
            ProductServiceProvider::class,
            OrderServiceProvider::class,
        ];

        foreach ($providers as $provider) {
            $this->app->register($provider, true);
        }
    }

    /**
     * Do our application bindings.
     *
     * @return void
     */
    protected function mapBindings()
    {

        $this->app->singleton('api', function ($app) {
            return $app->make(Factory::class);
        });

        $this->app->singleton('generator', function ($app) {
            return $app->make(GeneratorManager::class);
        });
    }

}