<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Core\Product\Factories\ProductFactory;
use App\Core\Product\Interfaces\ProductFactoryInterface;

class ProductServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(ProductFactoryInterface::class, function ($app) {
            return $app->make(ProductFactory::class);
        });
    }
}