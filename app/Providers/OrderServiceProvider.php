<?php

namespace App\Providers;

use App\Core\Orders\Contracts\OrderFactory as OrderFactoryContract;
use App\Core\Orders\Factories\OrderFactory;
use App\Core\Orders\Factories\OrderProcessingFactory;
use App\Core\Orders\Interfaces\OrderCriteriaInterface;
use App\Core\Orders\Interfaces\OrderProcessingFactoryInterface;
use App\Core\Orders\Interfaces\OrderServiceInterface;
use App\Core\Orders\OrderCriteria;
use App\Services\OrderService;
use Illuminate\Support\ServiceProvider;

class OrderServiceProvider extends ServiceProvider
{

    public function register()
    {

        $this->app->bind(OrderCriteriaInterface::class, function ($app) {
            return $app->make(OrderCriteria::class);
        });

        $this->app->bind(OrderServiceInterface::class, function ($app) {
            return $app->make(OrderService::class);
        });

        $this->app->bind(OrderFactoryContract::class, function ($app) {
            return $app->make(OrderFactory::class);
        });

        $this->app->bind(OrderProcessingFactoryInterface::class, function ($app) {
            return $app->make(OrderProcessingFactory::class);
        });
    }
}