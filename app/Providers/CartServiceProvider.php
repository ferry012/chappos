<?php

namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class CartServiceProvider extends ServiceProvider
{

    protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

        $this->app['cart_instances'] = [];
        $this->app->bind('cart', function ($app, $params) {

            if (! empty($params['name'])) {
                $instance_name = $params['name'];
            } elseif ($app['request']->has('cartName')) {
                $instance_name = $app['request']->get('cartName', '');
            } else {
                $instance_name = 'default';
            }

            //implement singleton carts
            if (empty($cart_instances[$instance_name])) {
                $model                          = \App\Models\Cart::class;
                $cart_instances[$instance_name] = $model::current($instance_name);
                $app['cart_instances']          = $cart_instances;
            }

            return $app['cart_instances'][$instance_name];
        });

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['cart', 'cart_instances'];
    }

}
