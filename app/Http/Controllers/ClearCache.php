<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class ClearCache extends Controller
{

    public function clearAll()
    {
        ini_set("memory_limit", "-1");

        $forgetList = [
            'table.setting_pos',
            'table.pos_location_list','table.pos_function_list',
            'table.pos_bag_list',
            'table.pos_keylist', 'table.pos_keylist_static',
            'payment_mapping_list',
            'table.hsg_delivery', 'table.hsg_delv_mthd',
            'table.hsg_mbr_group',
            'api.vc.member_item'
        ];
        $tagsList = ['tokens','products'];

        foreach($forgetList as $list)
            app('cache')->forget($list);

        foreach($tagsList as $list)
            app('cache')->tags($list)->flush();

        return ["msg" => "Cleared: " . implode(', ', array_merge($forgetList,$tagsList))];

    }

    public function clearByPattern(Request $request, $tag, $id)
    {
        $pattern = $tag .'.'. $id;

        $matchedKeys = Redis::keys("*$pattern*");
        foreach($matchedKeys as $k) {
            Redis::del($k);
        }

        $msg = ($matchedKeys) ? "Cleared " . count($matchedKeys) . " records from Redis" : "No records found";
        return ["msg" => $msg];
    }

}