<?php

namespace App\Http\Controllers\Orders;

use App\Core\Baskets\Interfaces\BasketServiceInterface;
use App\Core\Orders\Contracts\OrderFactory;
use App\Core\Orders\Events\OrderWasPaid;
use App\Core\Orders\Exceptions\OrderAlreadyProcessedException;
use App\Core\Baskets\Factories\BasketFactory;
use App\Core\Baskets\Interfaces\BasketCriteriaInterface;
use App\Core\Orders\Exceptions\OrderInProcessingException;
use App\Core\Orders\Interfaces\OrderCriteriaInterface;
use App\Core\Orders\Interfaces\OrderProcessingFactoryInterface;
use App\Core\Orders\Models\Order;
use App\Core\Payments\Models\Transaction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Orders\CreateRequest;
use App\Http\Requests\Orders\IndexRequest;
use App\Http\Requests\Orders\ProcessRequest;
use App\Http\Resources\Files\PdfResource;
use App\Http\Resources\Orders\OrderCollection;
use App\Http\Resources\Orders\OrderReceiptResource;
use App\Http\Resources\Orders\OrderResource;
use App\Support\Str;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    /**
     * The orders criteria instance.
     *
     * @var OrderCriteriaInterface
     */
    protected $orders;

    /**
     * The baskets criteria instance.
     *
     * @var BasketCriteriaInterface
     */
    protected $baskets;

    public function __construct(OrderCriteriaInterface $orders, BasketCriteriaInterface $baskets)
    {
        $this->orders  = $orders;
        $this->baskets = $baskets;
    }
    
    public function index(IndexRequest $request)
    {

        if ($request->has('order_num')) {
            $orders = Order::withoutGlobalScope('open')->when($request->has('order_num'), function ($q) use ($request) {
                $q->where('order_num', strtoupper($request->get('order_num')));
            })->paginate();
            return new OrderCollection($orders);
        }

        $date = $request->get('date', Carbon::now()->toDateString());

        if ($request->has('date_from') && $request->has('date_to')) {
            $date = null;
        }

        $orders = Order::withoutGlobalScope('open')->when($date, function ($q) use ($date) {
            $q->whereDate('created_on', $date);
        })->when($request->has('order_type'), function ($q) use ($request) {
            $types = explode(',', $request->get('order_type'));
            $q->whereIn('order_type', $types);
        })->when($request->has('loc_id'), function ($q) use ($request) {
            $q->where('loc_id', Str::upper($request->get('loc_id')));
        })->when($request->has('f_pos_id'), function ($q) use ($request) {
            $q->where('pos_id', Str::upper($request->get('f_pos_id')));
        })->when($request->has('mbr_id'), function ($q) use ($request) {
            $q->where('customer_id', $request->get('mbr_id'));
        })->when($request->has('date_from') && $request->has('date_to'), function ($q) use ($request) {
            $q->whereRaw('(created_on::timestamp between ? and ?)', [$request->get('date_from'), $request->get('date_to')]);
        })->when($request->has('amount_from') && $request->has('amount_to'), function ($q) use ($request) {
            $q->amount($request->get('amount_from'), $request->get('amount_to'));
        })->when($request->has('item_id') && ! empty($request->get('item_id')), function ($q) use ($request) {
            $q->whereExists(function ($q) use ($request) {
                $q->select('id')->from('o2o_order_item')->where('item_id', 'ilike', '%'.$request->get('item_id').'%')->whereRaw('o2o_order_item.order_id = o2o_order_list.id');
            });
        })->when($request->has('item_desc') && ! empty($request->get('item_desc')), function ($q) use ($request) {
            $q->whereExists(function ($q) use ($request) {
                $q->select('id')->from('o2o_order_item')->where('item_desc', 'ilike', '%'.$request->get('item_desc').'%')->whereRaw('o2o_order_item.order_id = o2o_order_list.id');
            });
        })->when($request->has('pay_mode'), function ($q) use ($request) {
            $payModes = explode(',', $request->get('pay_mode'));

            if ($payModes[0] !== '') {
                $q->whereExists(function ($q) use ($payModes) {
                    $q->select('id')->from('o2o_order_payment');
                    foreach ($payModes as $payMode) {
                        $q->orWhere(function ($q) use ($payMode) {
                            list($mode, $type) = array_pad(explode('.', $payMode), 2, '');
                            $q->where('pay_mode', $mode)->where('pay_type', $type);
                        });
                    }
                });
            }
        });

        if ($request->has('all_status')) {
            // For query by CHROOMS - return all order type
            // $orders->where('order_status_level', '>', 0);
        } else {
            $orders = $orders->where('order_status_level', '>', 0);
        }

        $orders = $orders->orderBy('created_on', 'DESC')->paginate();

        return new OrderCollection($orders);
    }

    public function show($id)
    {

        try {

            $order = Order::where('order_num', $id)->orWhere('invoice_id', $id)->withoutGlobalScope('open')->firstOrFail()->load([
                'lines' => function ($q) {
                    return $q->when(request('exclude_voided', true), function ($q) {
                        return $q->active();
                    })->orderBy('line_num');
                }, 'transactions', 'addresses',
            ]);

            $transaction = new Transaction;

            $transaction->pay_mode     = 'CASH_PAID';
            $transaction->success      = true;
            $transaction->trans_amount = $order->transactions->whereIn('pay_mode', [
                'CASH_CHANGE_DUE', 'CASH_ROUNDING_ADJ', 'CASH',
            ])->sum('trans_amount');

            $order->transactions->push($transaction);

        } catch (ModelNotFoundException $e) {

            // Try read from Cherps
            $order = $this->getInvoiceFromCherps($id);

            if (!$order) {
                return $this->errorNotFound();
            }

            return $order; //

        }

        return new OrderResource($order);
    }

    public function create(CreateRequest $request, OrderFactory $factory, BasketFactory $basketFactory)
    {

        $basket = $this->baskets->id($request->basket_id)->first();

        if (! $basket) {
            return $this->errorNotFound();
        }

        $basket = $basketFactory->init($basket)->get();
        $order  = $factory->basket($basket)->type($request->order_type)->reference($request->get('order_ref_id'))->resolve()->load([
            'lines' => function ($q) {
                $q->active();
            },
        ]);

        return new OrderResource($order);
    }

    public function process(ProcessRequest $request, OrderProcessingFactoryInterface $factory, OrderCriteriaInterface $criteria, $id)
    {
       
        $payments = $request->get('payments', []);
        $format   = $request->get('order_resp_format', 'receipt');
        $coupons  = $request->get('coupons');
        // Needed to priority the payment mode

        try {
//            dd('JAY');
            $order = $criteria->id($id)->firstOrFail();
//            dd($order);
            if ($order->getCustomData('is_processing', false)) {
                //throw new OrderInProcessingException('This order is in processing');
            }
//            dd($order->order_status_level);
            if ($order->order_status_level === 0) {
                // delete exists records
                $order->transactions()->delete();
                $factory->order($order)->provider('Offline')->payload($payments)->resolve();
            }

            $order->unsetCustomData('is_processing');
            $order->save();

            if ($order->order_status_level === 0) {
                return $this->errorForbidden('Payment has failed');
            }

            $order->load([
                'lines'           => function ($q) {
                    $q->active();
                }, 'transactions' => function ($q) {
                    $q->charged();
                },
            ]);

            app()->make(BasketServiceInterface::class)->utilizeCoupon($coupons,$order);
            app()->make(BasketServiceInterface::class)->getFormerOrder($order);

            return $format !== 'receipt' ? new OrderResource($order) : new OrderReceiptResource($order);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        } catch (OrderInProcessingException $e) {
            return $this->errorUnprocessable('This order is in processing');
        } catch (OrderAlreadyProcessedException $e) {
            return $this->errorUnprocessable('This order has already been processed');
        }

    }

    public function invoice(Request $request, $id)
    {

        $order = Order::where('order_num', $id)->firstOrFail();

        if ($request->get('format') === 'receipt') {
            $order->load([
                'lines' => function ($q) {
                    $q->active()->orderBy('line_num');
                }, 'transactions',
            ]);

            return new OrderReceiptResource($order);
        }

        $order->load(['lines', 'transactions']);

        $pdf = app('api')->orders()->getPdf('test');

        if ($request->get('preview', 0)) {
            return $pdf->stream();
        }

        return new PdfResource($pdf);
    }

    public function orderPosting(Request $request)
    {
        $date = $request->get('date', Carbon::now()->toDateString());

        if ($request->has('order_num') || ($request->has('date_from') && $request->has('date_to'))) {
            $date = null;
        }

        $response = [];

        Order::when($request->has('loc_id'), function ($q) use ($request) {
            return $q->where('loc_id', $request->get('loc_id'));
        })->when($request->has('order_num'), function ($q) use ($request) {
            return $q->whereIn('order_num', explode(',', $request->get('order_num')));
        })->when($request->has('date_from') && $request->has('date_to'), function ($q) use ($request) {
            return $q->where('created_on', '>=', $request->get('date_from'))->where('created_on', '<=', $request->get('date_to'));
        })->when($date, function ($q) use ($date) {
            return $q->whereDate('created_on', $date);
        })->orderBy('id', 'ASC')->chunk(50, function ($orders) use(&$response) {

            $posted = [];
            foreach ($orders as $order) {

                // Do not post anything for ubi
                if ($order->loc_id === 'UBI' && app()->environment('production')) {
                    continue;
                }

                //@todo Open to all till data been posted correctly
                // Currently just ignore those not paid orders
                if ($order->order_status_level < 1) {
                    continue;
                }

                event(new OrderWasPaid($order));
                $order->save();

                if ($order->is_posted === true)
                    $response[] = $order->order_num;

            }
            // Reduce database traffic pressure?
            sleep(0);

            return $posted;
        });

        return ["orders" => $response];
    }

    public function getInvoiceFromCherps($id, $coyId='CTL') {
        $dbName = 'pg_cherps';
        $tblInvoiceItem = 'sms_invoice_item';
        $tblPaymentList = 'sms_payment_list';
        $tblInvoiceLot = 'sms_invoice_lot';

        $invoice_list = db($dbName)->table('sms_invoice_list')->where('coy_id', $coyId)->where('invoice_id', $id)->first();

        if (!$invoice_list) {

            if (!app()->environment('production')) {
                return null;
            }

            if (strlen($id) > 10) {
                $coyId = 'HSG';
            }

            $dbName = 'ms_cherps';
            $tblInvoiceItem = 'sms_invoice_item_history';
            $tblPaymentList = 'sms_payment_list_history';
            $tblInvoiceLot = 'sms_invoice_lot_history';

            $invoice_list = db($dbName)->table('sms_invoice_list_history')->where('coy_id',$coyId)->where('invoice_id', $id)->first();

            if (!$invoice_list) {
                return null;
            }

        }

        $invoice_subtotal = 0;
        $order_items = [];
        $invoice_items = db($dbName)->table($tblInvoiceItem)->where('coy_id', $coyId)->where('invoice_id', $id)->orderBy('line_num')->get();
        $invoice_items_lot = db($dbName)->table($tblInvoiceLot)->where('coy_id', $coyId)->where('invoice_id', $id)->orderBy('line_num')->get();

        foreach($invoice_items as $key => $itm) {

            $delv_method = trim($itm->delv_mode_id);
            $delv_desc = trim($itm->delv_mode_id);
            if (in_array($itm->delv_mode_id, ['NID','NJA','STD'])) {
                $delv_method = 'STD';
                $delv_desc = 'Standard Shipping';
            }
            else if (in_array($itm->delv_mode_id, ['SCL'])) {
                $delv_method = 'SCL';
                $delv_desc = 'Self Collection';
            }
            else if (in_array($itm->delv_mode_id, ['ISC'])) {
                $delv_method = 'ISC';
                $delv_desc = 'Instore Collection';
            }
            else if (in_array($itm->delv_mode_id, [''])) {
                $delv_method = '';
                $delv_desc = 'No Shipping';
            }

            $lot = [];

            if ($invoice_items_lot) {
                $lot = $invoice_items_lot->where('line_num', $itm->line_num)->pluck('lot_id')->toArray();
            }

            $order_items[$key] = array (
                'id' => $itm->line_num,
                'parent_id' => 0,
                'line_num' => 1,
                'ref_line_num' => 0,
                'item_id' => trim($itm->item_id),
                'item_desc' => trim($itm->item_desc),
                'item_type' => 'I',
                'qty_ordered' => $itm->qty_invoiced,
                'unit_price' => number_format($itm->unit_price,2),
                'unit_discount' => number_format($itm->disc_amount / $itm->qty_invoiced,2),
                'regular_price' => number_format($itm->unit_price,2),
                'total_price' => number_format( ($itm->unit_price * $itm->qty_invoiced) ,2),
                'qty_refunded' => 0,
                'discount_total' => number_format($itm->disc_amount,2),
                'lot_id' => '',
                'custom_data' =>
                    array (
                        'multiple_points' => 1,
                    ),
                'loc_id' => '',
                'delv_method' => $delv_method,
                'delv_desc' => $delv_desc,
                'description' => $itm->item_desc,
                'price_total' => number_format( ($itm->unit_price * $itm->qty_invoiced) ,2),
            );

            if ($lot) {
                $order_items[$key]['lot_id'] = $lot;
            }

            $invoice_subtotal += $itm->unit_price * $itm->qty_invoiced;
        }


        $payment_lists = db()->table('pos_function_list')->where('func_type', 'PAYMENT3')->get();

        $order_payments = [];
        $invoice_payment = db($dbName)->table($tblPaymentList)->where('coy_id',$coyId)->where('invoice_id', $id)->orderBy('line_num')->get();
        $taxable_amount = 0;
        foreach($invoice_payment as $pmt) {

            $payment_list = $payment_lists->where('func_dim1',trim($pmt->pay_mode))->first();
            $pay_mode = ($payment_list) ? trim($payment_list->func_id) : trim($pmt->pay_mode);

            $order_payments[] = array (
                'id' => $pmt->line_num,
                'transaction_id' => trim($pmt->invoice_id) . '-' . $pmt->line_num,
                'transaction_reference_id' => trim($pmt->trans_ref1),
                'pay_mode' => $pay_mode,
                'pay_type' => NULL,
                'masked_card_number' => trim($pmt->trans_ref1),
                'approval_code' => '',
                'trans_amount' => $pmt->trans_amount,
                'success' => true,
                'status' => '00',
                'custom_data' => '',
                'created_on' => date('Y-m-d H:i:s', strtotime($pmt->created_on)),
            );

            if (!in_array( trim($pmt->pay_mode) , ['REBATE','!VCH-STAR001']))
                $taxable_amount += $pmt->trans_amount;
        }

        $order_addr = [];
        $invoice_addr = db($dbName)->table('coy_address_book')->where('coy_id',$coyId)->where('ref_type', 'CASH_INV')->where('ref_id', $id)->orderBy('addr_type')->get();
        foreach($invoice_addr as $addr){
            if ($addr->addr_type == '2-DELIVERY') {
                $order_addr['shipping'] = array (
                    'addr_text' => $addr->addr_text,
                    'recipient_name' => $invoice_list->contact_person,
                    'company_name' => '',
                    'phone_num' => $invoice_list->tel_code,
                    'postal_code' => $addr->postal_code,
                    'city_name' => ($addr->country_id == 'SG') ? 'SINGAPORE' : $addr->country_id,
                    'state_name' => '',
                    'country_id' => $addr->country_id,
                );
            }
            else {
                $order_addr['billing'] = array (
                    'addr_text' => $addr->addr_text,
                    'recipient_name' => $invoice_list->contact_person,
                    'company_name' => '',
                    'phone_num' => $invoice_list->tel_code,
                    'postal_code' => $addr->postal_code,
                    'city_name' => ($addr->country_id == 'SG') ? 'SINGAPORE' : $addr->country_id,
                    'state_name' => '',
                    'country_id' => $addr->country_id,
                );
            }
        }

        $loc_id = $invoice_list->loc_id ?? 'HCL';
        if ($location = pos_locations($loc_id)) {
            $logoId            = $location->logo_id;
            $storeShopName     = $location->shop_name;
            $storeAddressLine1 = $location->street_line1;
            $storeAddressLine2 = $location->street_line2;
            $storeAddressLine3 = '';
            if (isset($location->coy_reg_code)) {
                $storeAddressLine3 .= ($storeAddressLine3!='') ? ' | ' : '';
                $storeAddressLine3 .= 'ROC ' . ($location->coy_reg_code ?? '');
            }
            if (isset($location->coy_tax_code)) {
                $storeAddressLine3 .= ($storeAddressLine3 != '') ? ' | ' : '';
                $storeAddressLine3 .= 'GST ' . ($location->coy_tax_code ?? '');
            }
        }

        $gst = number_format((gst()->tax_percent / 100) + 1,2);

        $order = array (
            'logo_id' => isset($logoId) ? $logoId : 'CTL',
            'store_name' => isset($storeShopName) ? $storeShopName : 'Ubi (Challenger HQ)',
            'store_addr_line1' => isset($storeAddressLine1) ? $storeAddressLine1 : '1 UBI LINK',
            'store_addr_line2' => isset($storeAddressLine2) ? $storeAddressLine2 : '',
            'store_addr_line3' => isset($storeAddressLine3) ? $storeAddressLine3 : '',
            'loc_id' => trim($invoice_list->loc_id),
            'pos_id' => '',
            'invoice_num' => trim($invoice_list->invoice_id),
            'order_num' => trim($invoice_list->invoice_id),
            'order_type' => $invoice_list->inv_type,
            'order_ref_id' => $invoice_list->ref_id,
            'trans_date' => date('Y-m-d H:i:s', strtotime($invoice_list->invoice_date)),
            'display_trans_date' => date('d/m/Y H:i:s', strtotime($invoice_list->invoice_date)),
            'due_on' => date('Y-m-d H:i:s', strtotime($invoice_list->invoice_date)),
            'placed_on' => date('Y-m-d H:i:s', strtotime($invoice_list->invoice_date)),
            'order_status_level' => ($invoice_list->status_level >0) ? 1 : $invoice_list->status_level,
            'pay_status_level' => ($invoice_list->status_level >0) ? 2 : $invoice_list->status_level,
            'cashier_id' => '',
            'cashier_name' => '',
            'item_count' => count($invoice_items),
            'currency' => '',
            'subtotal' => $invoice_subtotal,
            'tax_id' => '',
            'tax_amount' => number_format(($taxable_amount * ($invoice_list->tax_percent/100)) / $gst,2),
            'taxable_amount' => number_format($taxable_amount, 2),
            'total_amount' => number_format($invoice_subtotal, 2),
            'total_paid' => number_format($invoice_subtotal, 2),
            'total_due' => '0.00',
            'rounding_adj' => 0,
            'change_due' => 0,
            'total_savings' => '0.00',
            'trans_point' => 0,
            'rebates_earned' => 0,
            'shipping_total' => '0.00',
            'discount_total' => '0.00',
            'customer_info' =>
                array (
                    'id' => trim($invoice_list->cust_id),
                    'masked_id' => trim($invoice_list->cust_id),
                    'name' => $invoice_list->cust_name,
                    'exp_date' => '-- --- ----',
                ),
            'customer_id' => trim($invoice_list->cust_id),
            'customer_name' => $invoice_list->cust_name,
            'customer_phone' => $invoice_list->tel_code,
            'customer_email' => $invoice_list->email_addr,
            'customer_notes' => '',
            'is_free_shipping' => true,
            'is_guest_purchase' => false,
            'id' => 0,
            'invoice_key' => encrypt_decrypt($id),
            'items' => $order_items,
            'transactions' => $order_payments,
            'addresses' => $order_addr
        );

        return $order;

    }
}