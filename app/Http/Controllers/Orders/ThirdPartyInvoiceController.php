<?php

namespace App\Http\Controllers\Orders;


use App\Http\Controllers\Controller;
use App\Http\Resources\AbstractCollection;
use App\Models\InvoiceList;
use Carbon\Carbon;
use App\Support\Str;

//use Illuminate\Http\Response;
use Flugg\Responder\Responder;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Services\CherpsService;
use App\Services\ValueclubapiService;
use App\Services\ImsapiService;

use App\Http\Controllers\Orders\OrderController;
use App\Support\Guzzle\ChvoicesClient;

class ThirdPartyInvoiceController extends Controller
{

    private $cherps, $valueclub, $imsapi, $coySettings;

    public function __construct(CherpsService $cherps, ValueclubapiService $valueclub, ImsapiService $imsapi)
    {
        $this->cherps = $cherps;
        $this->valueclub = $valueclub;
        $this->imsapi = $imsapi;

        $this->coySettings = [
            'KTP' => [
                'coy_id' => 'CTL',
                'inv_type' => 'KI',
                'inv_loc' => 'KTP',
                'loc_id' => 'KTP',
                'delv_mode_id' => 'NID',
                'posting_list' => [
                    'fin_dim1' => '61-ONLINE SALES'
                ],
                'payment_list' => [
                    '2C2P' => ['CP', '']
                ],
                'custom_settings' => [
                    'generate_invoice' => 'N',
                ],
                'void_invoice' => [
                    'REFUND' => 'KR',
                    'CANCEL' => 'KX'
                ]
            ],
            'ITE' => [
                'coy_id' => 'ITZ',
                'inv_type' => 'TI',
                'inv_loc' => 'IT2',
                'loc_id' => 'IT2',
                'delv_mode_id' => 'NID',
                'posting_list' => [
                    'fin_dim1' => '61-ONLINE SALES'
                ],
                'payment_list' => [
                    '2C2P' => ['CP', '']
                ],
                'custom_settings' => [
                    'generate_invoice' => 'N',
                ],
                'void_invoice' => [
                    'REFUND' => 'TR',
                    'CANCEL' => 'TX'
                ]
            ],
            'LAZ' => [
                'coy_id' => 'CTL',
                'inv_type' => 'IE',
                'inv_loc' => 'HCL',
                'loc_id' => '**',
                'delv_mode_id' => 'NID',
                'posting_list' => [
                    'fin_dim1' => '20-DC'
                ],
                'payment_list' => [],
                'custom_settings' => [
                    'generate_invoice' => 'Y',
                ]
            ],
            'PAD' => [
                'coy_id' => 'CTL',
                'inv_type' => 'IE',
                'inv_loc' => '**',
                'loc_id' => '**',
                'delv_mode_id' => 'NID',
                'posting_list' => [
                    'fin_dim1' => '20-DC'
                ],
                'payment_list' => [],
                'custom_settings' => [
                    'generate_invoice' => 'Y',
                ]
            ]
        ];
    }



    public function delivered_invoice(Request $request,Responder $responder){

        $v = Validator::make($request->all(), [
            'invoice' => 'required',
            'coy' => 'required',
        ]);

        if($v->fails()){
            return response()->json(['error' => $v->errors()->all()], 400);
        }
        //0 : Parcel(s) Created and in “Cache Bin” now.
        //1 : Driver collected Parcel(s) and put into Driver Bin. (Vehieve No.)
        //2 : Driver selected the Parcel(s) that “going to deliver now” in App.
        //4: Parcel(s) delivered successfully.
        //-2: Parcel(s) failed to deliver on that day.
        //-1 : Driver unable to deliver the parcel today (Too Late) and update the parcel to deliver in Next Working Day.

        $status_remarks = ['Picking & Packing','Verified & Confirmed','Inbounded in UBI Warehouse. (Outlet → Outlet)','delivering by driver','delivered',
            'Parcel(s) failed to deliver on that day.',
            'Driver unable to deliver the parcel today (Too Late) and update the parcel to deliver in Next Working Day.'];

        $coy = Str::upper($request->get('coy'));
        $settings = $this->coySettings[$coy]['coy_id'];
        $invoice = $request->get('invoice');

        $sql = "SELECT delivered_on AS delivery_date, status_level FROM chl_parcel_tracking_list
                    WHERE coy_id = '$settings' AND invoice_id = '$invoice'";
        $query = db('pg_cherps')->select($sql);

        if(count($query) > 0){
            $remarks = null;
            if($query[0]->status_level > 0 && $query[0]->status_level <= 4){
                $remarks = $status_remarks[$query[0]->status_level];
            } elseif ($query[0]->status_level < -1){
                $remarks = $status_remarks[$query[0]->status_level + 7];
            } else{
                $remarks = $status_remarks[$query[0]->status_level + 6];
            }

            return response()->json([
                'data' => [
                    'delivery_date' => $query[0]->status_level > 3 ? Carbon::parse($query[0]->delivery_date)->format('Y-m-d h:i:s a') : null,
                    'status' => $remarks,
                ],
                'success' => true,
                'status' => 200
            ]);
        }
        else{
            return $responder->error($invoice,'invoice does not exist');
        }

    }

    public function validate_3p(Request $request)
    {
        $v = Validator::make($request->all(), [
            'invoice' => 'required',
            'serial' => 'required',
            'coy' => 'required',
        ]);

        if($v->fails()){
            return response()->json(['error' => $v->errors()->all()], 400);
        }

        $coy = Str::upper($request->get('coy'));
        $settings = $this->coySettings[$coy]['coy_id'];
        $invoice = $request->get('invoice');
        $serial_no = $request->get('serial');

        $sql = "SELECT lot.coy_id,lot.trans_id,lot.line_num,lot.lot_line_num,
                    lot.status_level,lot.string_udf1,lot.string_udf2,lot.string_udf3,
                    lot.datetime_udf1,lot.datetime_udf2,lot.datetime_udf3,lot.created_by,
                    lot.created_on,lot.modified_by,lot.modified_on,
                    list.invoice_id,lot.lot_id AS serial_no 
                    FROM pos_transaction_lot AS lot
                    JOIN pos_transaction_list AS list ON lot.trans_id = list.trans_id 
                    AND list.invoice_id IS NOT NULL AND lot.lot_id IS NOT NULL 
                    AND list.invoice_id != ''AND lot.lot_id != ''
                    WHERE list.invoice_id = '$invoice' AND lot.lot_id = '$serial_no' AND lot.coy_id = '$settings'";
        $query = db('pg_cherps')->select($sql);

        return $query;
    }

    public function serialno($invoice_num, $coy_id)
    {
        $coy_id = Str::upper($coy_id);
        //check if the params supplies the correct company id
        if ($coy_id === 'KTP' || $coy_id === 'ITE' || $coy_id === 'LAZ' || $coy_id === 'PAD') {
            $settings = $this->coySettings[$coy_id];
            if ($settings) {
                $db = InvoiceList::from('sms_invoice_list as li')
                    ->leftjoin('sms_invoice_lot as lo', 'li.invoice_id', '=', 'lo.invoice_id')
                    ->leftJoin('sms_invoice_item as it', function ($leftJoin) {
                        $leftJoin->on('lo.invoice_id', '=', 'it.invoice_id');
                        $leftJoin->on('lo.line_num', '=', 'it.line_num');
                    })
                    ->where(function ($query) use ($invoice_num, $coy_id, $settings) {
                        $query->where('li.invoice_id', $invoice_num)
                            ->where('li.coy_id', $settings['coy_id'])
                            ->where('li.loc_id', $settings['loc_id'])
                            ->where('li.inv_type', $settings['inv_type']);
                    })
                    ->select('li.*','lo.lot_id as serial_no','it.item_id')
                    ->get();
                return response()->json([
                    'data' => $db
                ]);
            }
        } else {
            return 'Invalid Company ID';
        }
    }

    private function _get_coy($invoice_id)
    {
        // Find coy from invoice_id by matching inv_type
        $coy = '';
        $inv_prefix = substr($invoice_id, 0, 2);
        foreach ($this->coySettings as $c => $setting) {
            if ($setting['inv_type'] == $inv_prefix)
                $coy = $c;
        }
        return ($coy != '') ? $coy : null;
    }

    public function show_invoice($invoice_id, Responder $responder, OrderController $ordCtrl)
    {

        $coy = $this->_get_coy($invoice_id);
        if (!$coy) {
            return [
                "status_code" => 400,
                "message" => "Invalid invoice type"
            ];
        }

        $coy_id = $this->coySettings[$coy]['coy_id'];
        $order = $ordCtrl->getInvoiceFromCherps($invoice_id, $coy_id);

        if (!$order) {
            return [
                "status_code" => 400,
                "message" => "Invalid invoice ID"
            ];
        }

        $valid_lots = 0;
        foreach ($order['items'] as $k => $item) {
            $valid_lots += (is_array($order['items'][$k]['lot_id'])) ? count($order['items'][$k]['lot_id']) : 0;
        }
        if ($valid_lots > 0) {
            $sql = "select a.coy_id,a.invoice_id,a.line_num,a.lot_line_num,a.lot_id,a.string_udf1,a.string_udf2,a.string_udf3,a.datetime_udf1,a.datetime_udf2,a.datetime_udf3,b.item_info1,b.item_info2,b.item_info3
                    from sms_invoice_lot a
                    left join b2b_po_notes b on a.string_udf1='PO_NOTES' and a.string_udf2=b.notes_id
                    where a.coy_id = ? and a.invoice_id = ? ";
            $lots_info = db('pg_cherps')->select($sql, [$coy_id, $invoice_id]);
        }
        foreach ($order['items'] as $k => $item) {
            if (is_array($order['items'][$k]['lot_id'])) {
                foreach ($order['items'][$k]['lot_id'] as $lot) {
                    $infoDb = collect($lots_info)->where('lot_id', $lot)->first();

                    $info = [];
                    $info['lot_id'] = trim($lot);
                    if ($infoDb && $infoDb->string_udf1 == 'PO_NOTES') {
                        $info['lot_url'] = $infoDb->item_info2;
                    }
                    if ($infoDb && $infoDb->string_udf1 == 'FA') {
                        $info['warranty_expiry'] = $infoDb->datetime_udf2;
                    }

                    $order['items'][$k]['lot_info'][] = $info;
                }
            }
        }

        return [
            "status_code" => 200,
            "message" => "success",
            "data" => $order
        ];

    }

    public function sms_invoice(Request $request, Responder $responder)
    {

        $input = request()->only([
            'coy', 'loc_id',
            'invoice_id', 'invoice_date', 'delv_date',
            'cust_id', 'cust_name', 'tel_code', 'email_addr',
            'cust_addr.street_line1', 'cust_addr.street_line2', 'cust_addr.postal_code', 'cust_addr.country_id',
            'delv_addr.street_line1', 'delv_addr.street_line2', 'delv_addr.postal_code', 'delv_addr.country_id', 'delv_addr.recipient_name', 'delv_addr.tel_code', 'delv_addr.email_addr',
            'items', 'payments',
            'salesperson_id', 'delv_mode_id', 'remarks',
            'order_id']);

        $rules = [
            'coy' => ['required', Rule::in(array_keys($this->coySettings))],
            'invoice_id' => 'required',
            'invoice_date' => 'required|date',
            'delv_date' => 'required|date',
//            'cust_name' => 'required',
//            'tel_code' => 'required',
//            'email_addr' => 'required',
            'cust_addr' => 'required',
            'items' => 'required|array'
        ];
        $validator = Validator::make($input, $rules, []);

        // Post order actions
        $post_order_actions = [];

        // For logging
        $log_id = (isset($input['invoice_id'])) ? $input['invoice_id'] : time();
        $log_endpoint = (app()->environment('production')) ? 'https://pos.api.valueclub.asia/' : 'https://chappos-api.sghachi.com/';
        $log_endpoint .= 'orders/3p/save';

        if ($validator->fails()) {
            $resp = [
                "error" => $validator->errors($validator)
            ];
            $this->_log($log_id, $log_endpoint, $input, $resp);

            $this->setStatusCode(400);
            return $this->respondWithError($resp);
        }

        $coy = $input['coy'];
        $created_by = 'POSAPI';

        $order_loc_id = $this->coySettings[$coy]['loc_id'];
        if ($this->coySettings[$coy]['loc_id'] == '**') {
            $order_loc_id = $input['loc_id'];
        }
        if ($this->coySettings[$coy]['inv_loc'] == '**') {
            $this->coySettings[$coy]['inv_loc'] = $input['loc_id'];
        }

        if ($this->coySettings[$coy]['custom_settings']['generate_invoice'] == 'Y') {
            // Generate invoice ID
            $coy_id = $this->coySettings[$coy]['coy_id'];
            $ref_id = trim($input['invoice_id']);
            $invoice_id = $this->cherps->sys_trans_list($this->coySettings[$coy]['inv_type'], 'INV', 'SMS', $coy_id, 8);
        } else {
            // Use provide invoice_id, but check if Prefix is acceptable
            $ref_id = (isset($input['order_id']) && !empty($input['order_id'])) ? $input['order_id'] : '';
            $invoice_id = trim($input['invoice_id']);

            if (app()->environment('production') && substr($invoice_id, 0, 2) != $this->coySettings[$coy]['inv_type']) {
                $resp = [
                    "error" => ['invoice_id' => ['Invoice ID not allowed']]
                ];
                $this->_log($log_id, $log_endpoint, $input, $resp);

                $this->setStatusCode(400);
                return $this->respondWithError($resp);
            }
        }

        // Check if invoice_id is new
        $checkInvoiceId = db('pg_cherps')->table('sms_invoice_list')->where('coy_id', $this->coySettings[$coy]['coy_id'])->where('invoice_id', $invoice_id)->count();
        if ($checkInvoiceId) {
            $resp = [
                "error" => ['invoice_id' => ['Duplicate Invoice ID']]
            ];
            $this->_log($log_id, $log_endpoint, $input, $resp);

            $this->setStatusCode(400);
            return $this->respondWithError($resp);
        }

        // Customer Details
        if (isset($input['cust_id']) && $input['cust_id'] != '') {
            $cust_db = db('pg_cherps')->table('sms_customer_list')
                ->where('coy_id', $this->coySettings[$coy]['coy_id'])->where('cust_id', $input['cust_id'])
                ->first();

            if (!$cust_db) {
                $resp = [
                    "error" => ['invoice_id' => ['Invalid Cust ID']]
                ];
                $this->_log($log_id, $log_endpoint, $input, $resp);

                $this->setStatusCode(400);
                return $this->respondWithError($resp);
            }

            $cust_info = [
                "cust_id" => trim($cust_db->cust_id),
                "cust_name" => $cust_db->cust_name,
                "tel_code" => $cust_db->tel_code,
                "email_addr" => $cust_db->email_addr,
                "tax_id" => "",
                "payment_id" => $cust_db->payment_id,
                "delv_term_id" => $cust_db->delv_term_id,
                "delv_mode_id" => $cust_db->delv_mode_id,
            ];
            $cust_addr = null;
            $delv_addr = null;

            $recipient = [
                'name' => $cust_db->contact_person,
                'contact' => $cust_db->tel_code,
                'email' => $cust_db->email_addr
            ];
            $remarks = $coy . ' ' . $ref_id;
        } else {
            $cust_info = [
                "cust_id" => "",
                "cust_name" => $input['cust_name'],
                "tel_code" => $input['tel_code'],
                "email_addr" => $input['email_addr'],
                "tax_id" => "",
                "payment_id" => "",
                "delv_term_id" => "",
                "delv_mode_id" => "",
            ];
            $cust_addr = $input['cust_addr'];
            $delv_addr = $input['delv_addr'];

            $recipient = [
                'name' => $delv_addr['recipient_name'] ?? $cust_info['cust_name'],
                'contact' => $delv_addr['tel_code'] ?? '',
                'email' => $delv_addr['email_addr'] ?? ''
            ];
            $remarks = 'Recipient: ' . $recipient['contact'] . $recipient['email'];
        }

        if (isset($input['remarks']) && $input['remarks'] != '') {
            $remarks = $input['remarks'];
        }

        $_inv_type = $this->coySettings[$coy]['inv_type'];
        $items = $input['items'];

        // check ITZ items for #RECURRING and #OTHERS
        if ($this->coySettings[$coy]['coy_id'] === 'ITZ') {
            if (count($items) > 0) {
                $post_item_id = Str::upper(trim($items[0]['item_id']));
                if ($post_item_id === '#RECURRING' || $post_item_id === '#OTHERS') {
                    $_inv_type = 'IE';
                    $invoice_id = str_replace('TI', 'IE', $invoice_id);
                    $cust_info["cust_id"] = '1ONLINEMBB';
                }
            }
        }

        // Prepare SMS Data

        $orderHead = $this->genOrderHead([
            'coy_id' => $this->coySettings[$coy]['coy_id'],
            'invoice_id' => $invoice_id,
            'inv_type' => $_inv_type,
            'invoice_date' => date('Y-m-d H:i:s', strtotime($input['invoice_date'])),
            'delv_date' => date('Y-m-d H:i:s', strtotime($input['delv_date'])),
            'cust_po_code' => $ref_id,
            'cust_id' => $cust_info["cust_id"],
            'cust_name' => $cust_info["cust_name"],
            'tel_code' => $cust_info['tel_code'],
            'email_addr' => $cust_info['email_addr'],
            "payment_id" => $cust_info['payment_id'],
            "delv_term_id" => $cust_info['delv_term_id'],
            "delv_mode_id" => $cust_info['delv_mode_id'],
            'cust_addr' => '0-PRIMARY',
            'delv_addr' => '2-DELIVERY',
            'contact_person' => $recipient['name'],
            'status_level' => 1,
            'loc_id' => $order_loc_id,
            'remarks' => $remarks,
            'salesperson_id' => isset($input['salesperson_id']) ? strtolower($input['salesperson_id']) : '',
            'staff_id' => $created_by,
        ]);

        $itemsArr = collect($items)->pluck('item_id')->map(function ($value) {
            return Str::upper($value);
        })->toArray();
        $itemList = db('pg_cherps')->table('ims_item_list')
            ->where('coy_id', $this->coySettings[$coy]['coy_id'])->whereIn('item_id', $itemsArr)
            ->selectRaw('trim(item_id) item_id,item_desc,item_type,status_level,lot_control,ext_warranty,trim(inv_dim1) inv_dim1,trim(inv_dim2) inv_dim2,trim(inv_dim3) inv_dim3,trim(inv_dim4) inv_dim4,trim(brand_id) brand_id,trim(model_id) model_id,trim(fin_dim1) fin_dim1,trim(fin_dim2) fin_dim2,trim(fin_dim3) fin_dim3, long_desc, uom_id')
            ->get();


        $isNoQtyHCL = false;

        foreach ($items as $k => $item) {
            $itm = Str::upper(trim($item['item_id']));
            $itemListDim = $itemList->where('item_id', $itm)->first();

            $items[$k]['item_id'] = Str::upper($items[$k]['item_id']);
            $items[$k]['delv_date'] = date('Y-m-d H:i:s', strtotime($input['delv_date']));
            $items[$k]['status_level'] = (isset($itemListDim) && in_array($itemListDim->item_type, ['I'])) ? 1 : 2; // fin_dim2 of ims_item_list
            $items[$k]['salesperson_id'] = isset($input['salesperson_id']) ? strtolower($input['salesperson_id']) : '';

            $items[$k]['fin_dim1'] = $this->coySettings[$coy]['posting_list']['fin_dim1'] ?? ''; // Accounts
            $items[$k]['fin_dim2'] = (isset($itemListDim)) ? $itemListDim->fin_dim2 : ''; // fin_dim2 of ims_item_list
            $items[$k]['long_desc'] = (isset($itemListDim)) ? $itemListDim->long_desc : ''; // long_desc of ims_item_list
            $items[$k]['uom_id'] = (isset($itemListDim)) ? $itemListDim->uom_id : ''; // long_desc of ims_item_list

            $items[$k]['delv_mode_id'] = $this->coySettings[$coy]['delv_mode_id'];
            $items[$k]['loc_id'] = $order_loc_id;

            // Check inv_loc
            $item_inv_loc = $this->coySettings[$coy]['inv_loc'];
            $item_inv = db('pg_cherps')->table('v_ims_inv_summary')
                ->selectRaw('coy_id,rtrim(item_id) item_id,rtrim(loc_id) loc_id,qty_on_hand,qty_reserved,(qty_on_hand-qty_reserved) qty_available')
                ->where('coy_id', $this->coySettings[$coy]['coy_id'])->where('item_id', $itm)
                ->whereRaw('(qty_on_hand-qty_reserved) >= ?', [$item['qty_invoiced']])
                ->orderByRaw('(qty_on_hand-qty_reserved) desc')->get();


            if ($item_inv->isNotEmpty() && $item_inv->where('loc_id', $this->coySettings[$coy]['inv_loc'])->isEmpty()) {
                // Preferred inv_loc do not have QOH, use loc_id with highest QOH.
                if ($coy == 'LAZ') {
                    $isNoQtyHCL = true;
                } else {
                    $item_inv_loc = (isset($item_inv[0]->loc_id)) ? $item_inv[0]->loc_id : $this->coySettings[$coy]['inv_loc'];
                }
            }

            $items[$k]['inv_loc'] = $item_inv_loc;

            if (isset($item['delv_mode_id']) && in_array($item['delv_mode_id'], ['STD', 'NID'])) {
                $items[$k]['delv_mode_id'] = 'NID';
            }
            if (isset($item['delv_mode_id']) && in_array($item['delv_mode_id'], ['SCL'])) {
                $items[$k]['delv_mode_id'] = 'SCL';
            }

            // $post_order_actions - ESD items need to trigger PO
            if (isset($itemListDim) && Str::contains($itemListDim->inv_dim4, '-ESD', false)) {
                $post_order_actions[] = "ESD";
            }
        }
        $orderItems = $this->genOrderItem(['coy_id' => $this->coySettings[$coy]['coy_id'], 'invoice_id' => $invoice_id, 'staff_id' => $created_by], $items);

        //Check if Lazada/Shopee Invoice no Qty in HCL, set invoice to pending.
        if ($isNoQtyHCL) {
            $orderHead["status_level"] = 0;
//            foreach($orderItems as $index => $item){
//                $orderItems[$index]["status_level"]=0;
//            }
        }

        $orderAddresses = null;
        if (isset($cust_addr)) {
            $orderAddresses[] = $this->genOrderAddress([
                'coy_id' => $this->coySettings[$coy]['coy_id'],
                'ref_type' => 'CASH_INV',
                'ref_id' => $invoice_id,
                'addr_type' => '0-PRIMARY',
                'street_line1' => $cust_addr['street_line1'] ?? '',
                'street_line2' => $cust_addr['street_line2'] ?? '',
                'postal_code' => $cust_addr['postal_code'] ?? '',
                'addr_text' => $cust_addr['street_line1'] . "\n" . $cust_addr['street_line2'] . "\n" . $cust_addr['country_id'] . " " . $cust_addr['postal_code'],
                'staff_id' => $created_by
            ]);
        }
        if (isset($delv_addr)) {
            $orderAddresses[] = $this->genOrderAddress([
                'coy_id' => $this->coySettings[$coy]['coy_id'],
                'ref_type' => 'CASH_INV',
                'ref_id' => $invoice_id,
                'addr_type' => '2-DELIVERY',
                'street_line1' => $delv_addr['street_line1'] ?? '',
                'street_line2' => $delv_addr['street_line2'] ?? '',
                'postal_code' => $delv_addr['postal_code'] ?? '',
                'addr_text' => $delv_addr['street_line1'] . "\n" . $delv_addr['street_line2'] . "\n" . $delv_addr['country_id'] . " " . $delv_addr['postal_code'],
                'staff_id' => $created_by
            ]);
        }

        $orderPayments = null;
        if (isset($input['payments'])) {
            $payments = [];
            foreach ($input['payments'] as $pay) {
                $pay_mode = $pay['pay_mode'];
                if (isset($this->coySettings[$coy]['payment_list'][$pay_mode])) {
                    $payments[] = [
                        "pay_mode" => $this->coySettings[$coy]['payment_list'][$pay_mode][0],
                        "trans_amount" => $pay['trans_amount'],
                        "trans_ref1" => $pay['trans_ref1'],
                        "trans_ref2" => (isset($pay['trans_ref2'])) ? $pay['trans_ref2'] : "",
                        "trans_ref3" => (isset($pay['trans_ref3'])) ? $pay['trans_ref3'] : "",
                        "fin_dim2" => $this->coySettings[$coy]['payment_list'][$pay_mode][1],
                    ];
                }
            }
            $orderPayments = $this->genOrderPayment(['coy_id' => $this->coySettings[$coy]['coy_id'], 'invoice_id' => $invoice_id, 'staff_id' => $created_by], $payments);
        }

        db('pg_cherps')->beginTransaction();

        try {
            db('pg_cherps')->table('sms_invoice_list')->insert($orderHead);
            db('pg_cherps')->table('sms_invoice_item')->insert($orderItems);
            if (isset($orderAddresses)) {
                db('pg_cherps')->table('coy_address_book')->insert($orderAddresses);
            }
            if (isset($orderPayments)) {
                db('pg_cherps')->table('sms_payment_list')->insert($orderPayments);
            }
            db('pg_cherps')->commit();

            // Reserve stocks
            $this->reserveStocks($orderHead, $orderItems);


            // Generate PO on Cherps
            if (in_array('ESD', $post_order_actions)) {
                $chvoicesClient = new ChvoicesClient();
                $esd = $chvoicesClient->triggerPoGenerate($invoice_id);
            }


        } catch (\Exception $e) {
            db('pg_cherps')->rollback();

            $resp = [
                "error" => $e
            ];
            $this->_log($log_id, $log_endpoint, $input, $resp);

            $this->setStatusCode(400);
            return $this->respondWithError($resp);
        }

        // Return RD number
        $dataResponse = [
            "msg" => 'Invoice Generated',
            "invoice" => $invoice_id,
        ];

        //return $responder->success($dataResponse);
        $this->_log($log_id, $log_endpoint, $input, $dataResponse);
        return [
            "status_code" => 200,
            "message" => "success",
            "data" => $dataResponse
        ];

    }

    public function update_invoice($invoice_id, Request $request, Responder $responder)
    {

        $coy = $this->_get_coy($invoice_id);
        if (!$coy) {
            return [
                "status_code" => 400,
                "message" => "Invalid invoice type"
            ];
        }

        // Check if $invoice_id is valid
        $orderList = db('pg_cherps')->table('sms_invoice_list')->where([
            "coy_id" => $this->coySettings[$coy]['coy_id'],
            "invoice_id" => $invoice_id
        ])->orderby('invoice_date', 'desc')->first();
        $orderItems = db('pg_cherps')->table('sms_invoice_item')->where([
            "coy_id" => $this->coySettings[$coy]['coy_id'],
            "invoice_id" => $invoice_id
        ])->orderby('line_num')->get();

        if (empty($orderList) || empty($orderItems->toArray())) {
            return [
                "status_code" => 400,
                "message" => "Invalid invoice ID"
            ];
        }

        // Check action
        $input = request()->only(['action']);
        $input['action'] = strtoupper($input['action']);

        if ($input['action'] == 'REFUND') {

            // Not refunded before
            $orderRefunded = db('pg_cherps')->table('sms_invoice_list')->where([
                "coy_id" => $this->coySettings[$coy]['coy_id'],
                "inv_type" => $this->coySettings[$coy]['void_invoice']['REFUND'],
                "cust_po_code" => trim($invoice_id),
            ])->orderby('invoice_date', 'desc')->first();
            if ($orderRefunded) {
                return [
                    "status_code" => 400,
                    "message" => "Pending refund - " . trim($orderRefunded->invoice_id)
                ];
            }

            // Check that all items are on same status (picked or delivered)
            $invHeadStatus = $orderList->status_level;
            foreach ($orderItems as $itm) {
                if ($itm->status_level != $invHeadStatus)
                    $invHeadStatus = -9; // Set -9 as not qualified for refund
            }

            if ($invHeadStatus == 1) {
                // Mark order to delivered before we can refund
                $orderItemsDelivered = $this->_updDelivered($coy, $orderList, $orderItems);
                $invHeadStatus = 3; // (count($orderItemsDelivered)>0) ? 3 : 1;
            }

            $orderRefund = '';
            if ($invHeadStatus == 3) {
                // Issue HR
                $orderRefund = $this->_updRefund($coy, $orderList, $orderItems);
            }

            return [
                "status_code" => 200,
                "invoice_id" => $invoice_id,
                "refund_id" => $orderRefund,
                "message" => ($orderRefund != '') ? "Invoice refunded" : "Fail to issue refund. Pls try again later"
            ];
        }

        if ($input['action'] == 'PICKED') {
            $orderItemsUpdated = $this->_updPicked($coy, $orderList, $orderItems);

            return [
                "status_code" => 200,
                "invoice_id" => $invoice_id,
                "message" => (count($orderItemsUpdated) > 0) ? "Invoice picked" : "No items picked",
                "data" => $orderItemsUpdated
            ];
        }
        if ($input['action'] == 'DELIVERED') {
            $orderItemsUpdated = $this->_updDelivered($coy, $orderList, $orderItems);

            return [
                "status_code" => 200,
                "invoice_id" => $invoice_id,
                "message" => (count($orderItemsUpdated) > 0) ? "Invoice delivered" : "No items to deliver",
                "data" => $orderItemsUpdated
            ];
        }

        return [
            "status_code" => 400,
            "message" => "Invalid action"
        ];

    }

    /*
     * Reserve stock on ims_inv_reserved
     */
    private function reserveStocks($orderHead, $orderItems)
    {
        $invReserved = collect($orderItems)->values()->map(function ($item) use ($orderHead) {
            if ($item['status_level'] == 1) {
                return [
                    'coy_id' => $orderHead['coy_id'],
                    'item_id' => $item['item_id'],
                    'loc_id' => $item['inv_loc'],
                    'doc_type' => 'ORDER',
                    'trans_type' => $orderHead['inv_type'],
                    'trans_id' => $orderHead['invoice_id'],
                    'line_num' => $item['line_num'],
                    'trans_date' => $orderHead['invoice_date'],
                    'qty_reserved' => $item['qty_invoiced'],
                    'status_level' => 1,
                    'created_by' => $orderHead['created_by'] ?? '',
                    'created_on' => now(),
                    'modified_by' => $orderHead['created_by'] ?? '',
                    'modified_on' => now(),
                ];
            }
        })->toArray();

        return db('pg_cherps')->table('ims_inv_reserved')->insert(array_filter($invReserved));
    }

    private function _updPicked($coy, $orderList, $orderItems)
    {
        $orderItemsToUpd = [];
        $orderItemsUpdated = [];
        foreach ($orderItems as $itm) {
            if ($itm->status_level == 1 && $itm->qty_picked < $itm->qty_invoiced) {
                $orderItemsToUpd[] = [
                    "coy_id" => $itm->coy_id,
                    "invoice_id" => trim($itm->invoice_id),
                    "line_num" => $itm->line_num,
                    "item_id" => trim($itm->item_id),
                    "item_desc" => trim($itm->item_desc),
                    "status_level" => $itm->status_level,
                    "qty_invoiced" => intval($itm->qty_invoiced),
                    "qty_picked" => intval($itm->qty_invoiced), // update
                    "pick_date" => date('Y-m-d H:i:s'), // update
                    "delv_mode_id" => trim($itm->delv_mode_id),
                    "inv_loc" => trim($itm->inv_loc),
                    "loc_id" => trim($itm->loc_id),
                    "trans_id" => trim($itm->trans_id),
                    "do_id" => trim($itm->do_id),
                    "deposit_id" => trim($itm->deposit_id)
                ];
            }
        }
        if ($orderList->status_level == 1 && count($orderItemsToUpd) > 0) {
            foreach ($orderItemsToUpd as $itm) {
                $dbSet = [
                    "qty_picked" => $itm['qty_picked'],
                    "pick_date" => $itm['pick_date']
                ];
                $dbWhr = [
                    "coy_id" => $itm['coy_id'],
                    "invoice_id" => $itm['invoice_id'],
                    "line_num" => $itm['line_num']
                ];
                $action = db('pg_cherps')->table('sms_invoice_item')->where($dbWhr)->update($dbSet);

                if ($action)
                    $orderItemsUpdated[] = $itm;
            }
        }

        return $orderItemsUpdated;
    }

    private function _updDelivered($coy, $orderList, $orderItems)
    {
        $orderItemsToUpd = [];
        $orderItemsUpdated = [];
        foreach ($orderItems as $itm) {
            if ($itm->status_level == 1 && trim($itm->do_id) == '') {
                $orderItemsToUpd[] = [
                    "coy_id" => $itm->coy_id,
                    "invoice_id" => trim($itm->invoice_id),
                    "line_num" => $itm->line_num,
                    "item_id" => trim($itm->item_id),
                    "item_desc" => trim($itm->item_desc),
                    "status_level" => $itm->status_level,
                    "qty_invoiced" => intval($itm->qty_invoiced),
                    "qty_picked" => intval($itm->qty_invoiced),
                    "pick_date" => date('Y-m-d H:i:s'),
                    "delv_mode_id" => trim($itm->delv_mode_id),
                    "inv_loc" => trim($itm->inv_loc),
                    "loc_id" => trim($itm->loc_id),
                    "trans_id" => trim($itm->trans_id),
                    "do_id" => trim($itm->invoice_id) . '-' . $itm->line_num, // update
                    "deposit_id" => trim($itm->deposit_id)
                ];
            }
        }
        if ($orderList->status_level == 1 && count($orderItemsToUpd) > 0) {
            foreach ($orderItemsToUpd as $itm) {
                $dbSet = [
                    "do_id" => $itm['do_id'],
                    "modified_on" => date('Y-m-d H:i:s')
                ];
                $dbWhr = [
                    "coy_id" => $itm['coy_id'],
                    "invoice_id" => $itm['invoice_id'],
                    "line_num" => $itm['line_num']
                ];
                $action = db('pg_cherps')->table('sms_invoice_item')->where($dbWhr)->update($dbSet);

                if ($action)
                    $orderItemsUpdated[] = $itm;
            }
        }

        return $orderItemsUpdated;
    }

    private function _updRefund($coy, $orderList, $orderItems)
    {

        $ref_id = trim($orderList->invoice_id);
        $created_by = '';

        // Generate invoice ID
        $coy_id = $this->coySettings[$coy]['coy_id'];
        $inv_type = $this->coySettings[$coy]['void_invoice']['REFUND'];
        $invoice_id = $this->cherps->sys_trans_list($inv_type, 'INV', 'SMS', $coy_id, 8);

        $orderHead = $this->genOrderHead([
            'coy_id' => $coy_id,
            'invoice_id' => $invoice_id,
            'inv_type' => $inv_type,
            'invoice_date' => date('Y-m-d H:i:s'),
            'delv_date' => date('Y-m-d H:i:s'),
            'cust_po_code' => $ref_id,
            'cust_id' => $orderList->cust_id,
            'cust_name' => $orderList->cust_name,
            'tel_code' => $orderList->tel_code,
            'email_addr' => $orderList->email_addr,
            "payment_id" => $orderList->payment_id,
            "delv_term_id" => $orderList->delv_term_id,
            "delv_mode_id" => $orderList->delv_mode_id,
            'cust_addr' => '0-PRIMARY',
            'delv_addr' => '2-DELIVERY',
            'contact_person' => $orderList->contact_person,
            'status_level' => 1,
            'loc_id' => $orderList->loc_id,
            'remarks' => '',
            'salesperson_id' => '',
            'staff_id' => $created_by,
        ]);


        $items = [];
        foreach ($orderItems as $k => $itm) {
            $itm->status_level = 2;
            $items[] = (array)$itm;
        }
        $orderItems = $this->genOrderItem(['coy_id' => $this->coySettings[$coy]['coy_id'], 'invoice_id' => $invoice_id, 'staff_id' => $created_by], $items);


        $payments = [];
        $payments_original = db('pg_cherps')->table('sms_payment_list')->where([
            "coy_id" => $this->coySettings[$coy]['coy_id'],
            "invoice_id" => $ref_id
        ])->orderby('line_num')->get();
        foreach ($payments_original as $pay) {
            $payments[] = [
                "pay_mode" => trim($pay->pay_mode),
                "trans_amount" => $pay->trans_amount * -1,
                "trans_ref1" => "",
                "trans_ref2" => "",
                "trans_ref3" => "",
                "fin_dim2" => "",
            ];
        }
        $orderPayments = $this->genOrderPayment(['coy_id' => $this->coySettings[$coy]['coy_id'], 'invoice_id' => $invoice_id, 'staff_id' => $created_by], $payments);

        db('pg_cherps')->beginTransaction();

        try {
            db('pg_cherps')->table('sms_invoice_list')->insert($orderHead);
            db('pg_cherps')->table('sms_invoice_item')->insert($orderItems);
            db('pg_cherps')->table('sms_payment_list')->insert($orderPayments);
            db('pg_cherps')->commit();
        } catch (\Exception $e) {
            db('pg_cherps')->rollback();

            $this->setStatusCode(400);
            return $this->respondWithError([
                "error" => $e
            ]);
        }

        return $invoice_id;
    }

    /*
     * Generate data for sms_invoice_list, sms_invoice_item & coy_address_book
     */

    private function genOrderHead($data)
    {
        return [
            'coy_id' => $data['coy_id'] ?? 'CTL',
            'invoice_id' => $data['invoice_id'],
            'inv_type' => $data['inv_type'],
            'invoice_date' => isset($data['invoice_date']) ? $data['invoice_date'] : now(),
            'status_level' => isset($data['status_level']) ? $data['status_level'] : 1,
            'salesperson_id' => isset($data['salesperson_id']) ? $data['salesperson_id'] : '',
            'cust_id' => isset($data['cust_id']) ? $data['cust_id'] : '',
            'cust_name' => Str::substr(isset($data['cust_name']) ? $data['cust_name'] : '', 0, 60),
            'first_name' => Str::substr(isset($data['first_name']) ? $data['first_name'] : '', 0, 40),
            'last_name' => Str::substr(isset($data['last_name']) ? $data['last_name'] : '', 0, 40),
            'tel_code' => Str::substr(isset($data['tel_code']) ? $data['tel_code'] : '', 0, 30),
            'contact_person' => Str::substr(isset($data['contact_person']) ? $data['contact_person'] : '', 0, 30),
            'email_addr' => Str::substr(isset($data['email_addr']) ? $data['email_addr'] : '', 0, 60),
            'cust_addr' => isset($data['cust_addr']) ? $data['cust_addr'] : '',
            'delv_addr' => isset($data['delv_addr']) ? $data['delv_addr'] : '',
            'delv_date' => isset($data['delv_date']) ? $data['delv_date'] : now(),
            'cust_po_code' => isset($data['cust_po_code']) ? $data['cust_po_code'] : '',
            'mbr_ind' => isset($data['mbr_ind']) ? $data['mbr_ind'] : '',
            'ref_id' => isset($data['ref_id']) ? $data['ref_id'] : '',
            'loc_id' => isset($data['loc_id']) ? $data['loc_id'] : '',
            'curr_id' => isset($data['curr_id']) ? $data['curr_id'] : 'SGD',
            'exchg_rate' => isset($data['exchg_rate']) ? $data['exchg_rate'] : 1,
            'exchg_unit' => isset($data['exchg_unit']) ? $data['exchg_unit'] : 1,
            'payment_id' => isset($data['payment_id']) ? $data['payment_id'] : '',
            'delv_term_id' => isset($data['delv_term_id']) ? $data['delv_term_id'] : '',
            'delv_mode_id' => isset($data['delv_mode_id']) ? $data['delv_mode_id'] : '',
            'fault_loc' => 'N',
            'tax_id' => (isset($data['tax_id']) && $data['tax_id'] != '') ? $data['tax_id'] : gst()->tax_id,
            'tax_percent' => isset($data['tax_percent']) ? $data['tax_percent'] : gst()->tax_percent,
            'remarks' => isset($data['remarks']) ? $data['remarks'] : '',
            'created_by' => isset($data['staff_id']) ? $data['staff_id'] : '',
            'created_on' => now(),
            'modified_by' => isset($data['staff_id']) ? $data['staff_id'] : '',
            'modified_on' => now(),
        ];
    }

    private function genOrderItem($data, $items)
    {
        return collect($items)->values()->map(function ($item, $index) use ($data) {
            $qty = isset($item['qty']) ? $item['qty'] : (isset($item['qty_invoiced']) ? $item['qty_invoiced'] : 1);
            $line_num = isset($item['line_num']) ? $item['line_num'] : $index + 1;

            return [
                'coy_id' => $data['coy_id'] ?? 'CTL',
                'invoice_id' => $data['invoice_id'],
                'line_num' => $line_num,
                'item_id' => $item['item_id'],
                'item_desc' => Str::substr($item['item_desc'], 0, 60),
                'qty_invoiced' => $qty,
                'uom_id' => isset($item['uom_id']) ? $item['uom_id'] : '',
                'unit_price' => isset($item['unit_price']) ? $item['unit_price'] : 0,
                'disc_amount' => isset($item['disc_amount']) ? $item['disc_amount'] : 0,
                'disc_percent' => isset($item['disc_percent']) ? $item['disc_percent'] : 0,
                'status_level' => isset($item['status_level']) ? $item['status_level'] : 0,
                'fin_dim1' => isset($item['fin_dim1']) ? $item['fin_dim1'] : '',
                'fin_dim2' => isset($item['fin_dim2']) ? $item['fin_dim2'] : '',
                'fin_dim3' => isset($item['fin_dim3']) ? $item['fin_dim3'] : '',
                'ref_id' => isset($item['ref_id']) ? $item['ref_id'] : '',
                'salesperson_id' => isset($item['salesperson_id']) ? $item['salesperson_id'] : '',
                'bom_ind' => isset($item['bom_ind']) ? $item['bom_ind'] : '',
                'promo_id' => isset($item['promo_id']) ? $item['promo_id'] : '',
                'promoter_id' => isset($item['promoter_id']) ? $item['promoter_id'] : '',
                'delv_mode_id' => isset($item['delv_mode_id']) ? $item['delv_mode_id'] : '',
                'delv_date' => isset($item['delv_date']) ? $item['delv_date'] : now(),
                'inv_loc' => isset($item['inv_loc']) ? $item['inv_loc'] : '',
                'loc_id' => isset($item['loc_id']) ? $item['loc_id'] : '',
                'created_by' => isset($data['staff_id']) ? $data['staff_id'] : '',
                'created_on' => now(),
                'modified_by' => isset($data['staff_id']) ? $data['staff_id'] : '',
                'modified_on' => now(),
            ];
        })->toArray();
    }

    private function genOrderPayment($data, $payments)
    {
        return collect($payments)->values()->map(function ($item, $index) use ($data) {
            return [
                'coy_id' => $data['coy_id'] ?? 'CTL',
                'invoice_id' => $data['invoice_id'],
                'line_num' => $index + 1,
                'pay_mode' => $item['pay_mode'],
                'trans_amount' => $item['trans_amount'],
                'trans_ref1' => Str::substr(((isset($item['trans_ref1'])) ? $item['trans_ref1'] : ''), 0, 20),
                'trans_ref2' => Str::substr(((isset($item['trans_ref2'])) ? $item['trans_ref2'] : ''), 0, 20),
                'trans_ref3' => Str::substr(((isset($item['trans_ref3'])) ? $item['trans_ref3'] : ''), 0, 20),
                'trans_ref4' => Str::substr(((isset($item['trans_ref4'])) ? $item['trans_ref4'] : ''), 0, 20),
                'trans_ref5' => Str::substr(((isset($item['trans_ref5'])) ? $item['trans_ref5'] : ''), 0, 20),
                'trans_ref6' => Str::substr(((isset($item['trans_ref6'])) ? $item['trans_ref6'] : ''), 0, 20),
                'fin_dim2' => Str::substr(((isset($item['fin_dim2'])) ? $item['fin_dim2'] : ''), 0, 20),
                'fin_dim3' => Str::substr(((isset($item['fin_dim3'])) ? $item['fin_dim3'] : ''), 0, 20),
                'status_level' => $item['status_level'] ?? 0,
                'created_by' => isset($data['staff_id']) ? $data['staff_id'] : '',
                'created_on' => now(),
                'modified_by' => isset($data['staff_id']) ? $data['staff_id'] : '',
                'modified_on' => now(),
            ];
        })->toArray();
    }

    private function genOrderAddress($data)
    {
        return [
            'coy_id' => $data['coy_id'] ?? 'CTL',
            'ref_type' => $data['ref_type'],
            'ref_id' => $data['ref_id'],
            'addr_type' => (isset($data['addr_type'])) ? $data['addr_type'] : '0-PRIMARY',
            'addr_format' => '1',
            'street_line1' => Str::substr(((isset($data['street_line1'])) ? $data['street_line1'] : ''), 0, 30),
            'street_line2' => Str::substr(((isset($data['street_line2'])) ? $data['street_line2'] : ''), 0, 30),
            'street_line3' => Str::substr(((isset($data['street_line3'])) ? $data['street_line3'] : ''), 0, 30),
            'street_line4' => Str::substr(((isset($data['street_line4'])) ? $data['street_line4'] : ''), 0, 30),
            'city_name' => Str::substr(((isset($data['city_name'])) ? $data['city_name'] : ''), 0, 30),
            'state_name' => Str::substr(((isset($data['state_name'])) ? $data['state_name'] : ''), 0, 30),
            'country_id' => Str::substr(((isset($data['country_id'])) ? $data['country_id'] : 'SG'), 0, 30),
            'postal_code' => Str::substr(((isset($data['postal_code'])) ? $data['postal_code'] : ''), 0, 30),
            'addr_text' => Str::substr(((isset($data['addr_text'])) ? $data['addr_text'] : ''), 0, 300),
            'created_by' => isset($data['staff_id']) ? $data['staff_id'] : '',
            'created_on' => now(),
            'modified_by' => isset($data['staff_id']) ? $data['staff_id'] : '',
            'modified_on' => now(),
        ];
    }

    private function _log($api_description, $endpoint, $request, $response)
    {

        db()->table('api_log')->insert([
            "api_client" => '3pinvoice',
            "api_description" => $api_description,
            "request_method" => 'POST',
            "request_url" => $endpoint,
            "request_header" => '{}',
            "request_body" => json_encode($request),
            "request_on" => date('Y-m-d H:i:s'),
            "response_header" => '{}',
            "response_body" => json_encode($response),
            "response_on" => date('Y-m-d H:i:s'),
            "error_msg" => '',
            "created_on" => date('Y-m-d H:i:s'),
        ]);

    }

    //For KingDom website, check whether staff exists or not.
    public function checkStaffExist(Request $request)
    {
        $staff_id = $request->staff_id;

        $coy_id = 'CTL';

        $staff_sql = <<<END
select c.coy_id, rtrim(s.usr_id) usr_id,rtrim(c.staff_id) staff_id, s.usr_name staff_name, c.security_level,
       c.shift_id, c.loc_id, RTRIM(c.fin_dim3) fin_dim3,
                    (CASE WHEN c.security_level>=2 THEN 'IC' ELSE 'FT' END) staff_level,
                    sec_pass
				  from sys_usr_list s, coy_usr_list c
                  where trim(upper(c.coy_id))=trim(upper('$coy_id')) and LEFT(UPPER(fin_dim3),4)<>'PART'
                    and c.usr_id=s.usr_id and ( trim(upper(s.usr_id))=trim(upper('$staff_id')) or
                                                trim(upper(c.staff_id))=trim(upper('$staff_id')) )
                  LIMIT 1;
END;
        $staff = db('pg_cherps')->select($staff_sql, []);
        if (count($staff) > 0) {
            $code = 1;
            $result = $staff[0];
            if ($result->fin_dim3 == 'RESIGNED') {
                $code = 0;
                return response()->json([
                    'code' => $code,
                    'message' => "Staff already resigned.",
                    'result' => []
                ], 200);
            }

            return response()->json([
                'code' => $code,
                'message' => "Success.",
                'result' => $result
            ], 200);
        } else {
            return response()->json([
                'code' => 0,
                'message' => "Staff Not Found.",
                'result' => []
            ], 200);

        }
    }

    //For KingDom website, check the delv status of KI Invoice.
    public function getInvDeliveryStatus(Request $request)
    {
        $invoice_id = $request->invoice_id;

        $delivery = db('pg_cherps')->table('chl_parcel_tracking_list')->where([
            "invoice_id" => $invoice_id
        ])->orderby('status_level', 'desc')
            ->orderby('delv_date', 'desc')->first();

        $invoice_items = db('pg_cherps')->table('sms_invoice_item')->where([
            "invoice_id" => $invoice_id
        ])->orderby('status_level', 'desc')
            ->orderby('delv_date', 'desc')->get();

        $result = array();

        if ($delivery) {
            $code = 1;
            $message = "Success";

            $status_level = $delivery->status_level;
            if ($status_level == 4) {
                $status_text = "Delivered";
                $status_no = 5;
            } else if ($status_level == 3 || $status_level == 2 || $status_level == 1) {
                $status_text = "Out of Delivery";
                $status_no = 4;
            } else if ($status_level == 0) {
                $status_text = "To Ship";
                $status_no = 3;
            } else if ($status_level == -2 || $status_level == -1) {
                $status_text = "Failed to Deliver";
                $status_no = -1;
            }

            $result["status"] = $status_no;
            $result["status_text"] = $status_text;

            $result["data"]["delivery"] = $delivery;
            $result["data"]["item"] = $invoice_items;
        } else {
            if (count($invoice_items) > 0) {
                $isProcessing = false;
                $code = 1;
                $message = "Success";

                foreach ($invoice_items as $item) {
                    $qty_picked = $item->qty_picked;
                    if ($qty_picked > 0) {
                        $isProcessing = true;
                    }
                }

                if ($isProcessing) {
                    $status_text = "Processing Order";
                    $status_no = 2;

                } else {
                    $status_text = "Order Received";
                    $status_no = 1;

                }

                $result["status"] = $status_no;
                $result["status_text"] = $status_text;

                $result["data"]["delivery"] = [];
                $result["data"]["items"] = $invoice_items;
            } else {
                $code = 0;
                $message = "Invoice not found.";

                $result["status"] = -2;
                $result["status_text"] = $message;
                $result["data"] = [];
            }
        }

        return response()->json([
            'code' => $code,
            'message' => $message,
            'result' => $result
        ], 200);
    }

    public function deployCheck()
    {
        $time = '2022-08-02 12.00 (test Check)';
        return response()->json([
            'code' => 1,
            'message' => "Success",
            'result' => $time
        ], 200);
    }

}