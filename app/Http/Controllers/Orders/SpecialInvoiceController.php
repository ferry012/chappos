<?php

namespace App\Http\Controllers\Orders;

use App\Http\Controllers\Controller;
use App\Support\Guzzle\ChvoicesClient;
use App\Support\Str;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Services\CherpsService;
use App\Services\ValueclubapiService;
use App\Services\ImsapiService;

class SpecialInvoiceController extends Controller
{

    private $cherps, $valueclub, $imsapi;

    public function __construct(CherpsService $cherps, ValueclubapiService $valueclub, ImsapiService $imsapi)
    {
        $this->cherps  = $cherps;
        $this->valueclub = $valueclub;
        $this->imsapi = $imsapi;
    }

    public function rd_invoice(Request $request, Response $response)
    {

        $input = request()->only([
            'staff_id','invoice_id','delv_date','loc_id',
            'cust_name','tel_code','cust_addr.street_line1','cust_addr.street_line2','cust_addr.postal_code',
            'items',
            'notes']);

        $rules = [
            'staff_id' => 'required',
            'invoice_id' => 'required',
            'delv_date' => 'required|date',
            'loc_id' => 'required',
            'cust_name' => 'required',
            'tel_code' => 'required',
            'cust_addr' => 'required',
            'items' => 'required|array',
        ];
        $validator = Validator::make($input, $rules, []);

        if ($validator->fails()) {
            $this->setStatusCode(400);
            return $this->respondWithError([
                "error" => $validator->errors($validator)
            ]);
        }


        // Use receipt number as RD invoice_id
        $original_id = trim($input['invoice_id']);
        $staff_id   = $input['staff_id'];
        $cust_addr  = $input['cust_addr'];

        // For invoice_id, always append -x
        if (empty($original_id) || strtolower($original_id)=='new'){
            $original_id = '';
            $invoice_id = $this->cherps->sys_trans_list('RD', 'INV', 'SMS', 'CTL', 8);
        }
        else {
            $checkInvoiceId = db('pg_cherps')->table('sms_invoice_list')->where('coy_id','CTL')->where('invoice_id', 'like', '%'.$original_id.'%')->count();
            $invoice_id = $original_id . '-' . ($checkInvoiceId + 1);
        }

        // Insert data
        $orderHead = $this->genOrderHead([
            'invoice_id'     => $invoice_id,
            'inv_type'       => 'RD',
            'salesperson_id' => $input['staff_id'],
            'cust_name'      => $input['cust_name'],
            'tel_code'       => $input['tel_code'],
            'contact_person' => $input['cust_name'],
            'cust_addr'      => '0-PRIMARY',
            'delv_addr'      => '2-DELIVERY',
            'status_level'   => 1,
            'loc_id'         => $input['loc_id'],
            'ref_id'         => $invoice_id,
            'staff_id'       => $staff_id,
            'invoice_date'   => date('Y-m-d', strtotime($input['delv_date'])),
            'delv_date'      => date('Y-m-d', strtotime($input['delv_date'])),
            'remarks'        => 'Delivery: ' . date('d M Y', strtotime($input['delv_date'])),
        ]);


        $items = $input['items'];
        foreach ($items as $k => $item) {
            $items[$k]['delv_date']     = date('Y-m-d', strtotime($input['delv_date']));
            $items[$k]['inv_loc']       = $input['loc_id'];
            $items[$k]['loc_id']        = 'HCL';
            $items[$k]['delv_mode_id']  = 'NID';
            $items[$k]['status_level']  = 1;

            if ($input['loc_id'] == 'HCL'){
                $items[$k]['qty_picked'] = $items[$k]['qty'];
            }
        }
        $orderItems = $this->genOrderItem(['invoice_id' => $invoice_id, 'staff_id' => $staff_id], $items);

        $orderAddresses[] = $this->genOrderAddress([
            'ref_type'     => 'CASH_INV',
            'ref_id'       => $invoice_id,
            'addr_type'    => '0-PRIMARY',
            'street_line1' => $cust_addr['street_line1'] ?? '',
            'street_line2' => $cust_addr['street_line2'] ?? '',
            'postal_code'  => $cust_addr['postal_code'] ?? '',
            'addr_text'    => $cust_addr['street_line1']. "\n" . $cust_addr['street_line2']. "\nSingapore " . $cust_addr['postal_code'],
            'staff_id'     => $staff_id
        ]);
        $orderAddresses[] = $this->genOrderAddress([
            'ref_type'     => 'CASH_INV',
            'ref_id'       => $invoice_id,
            'addr_type'    => '2-DELIVERY',
            'street_line1' => $cust_addr['street_line1'] ?? '',
            'street_line2' => $cust_addr['street_line2'] ?? '',
            'postal_code'  => $cust_addr['postal_code'] ?? '',
            'addr_text'    => $cust_addr['street_line1']. "\n" . $cust_addr['street_line2']. "\nSingapore " . $cust_addr['postal_code'],
            'staff_id'     => $staff_id
        ]);

        db('pg_cherps')->beginTransaction();

        try {
            db('pg_cherps')->table('sms_invoice_list')->insert($orderHead);
            db('pg_cherps')->table('sms_invoice_item')->insert($orderItems);
            db('pg_cherps')->table('coy_address_book')->insert($orderAddresses);

            // If there is notes provided
            if (isset($input['notes']) && !empty($input['notes'])) {
                $orderNotes = [
                    "coy_id" => 'CTL',
                    "invoice_id" => $invoice_id,
                    "invoice_notes" => $input['notes'],
                    "created_by" => $staff_id,
                    "created_on" => now(),
                    "modified_by" => $staff_id,
                    "modified_on" => now()
                ];
                db('pg_cherps')->table('sms_invoice_notes')->insert($orderNotes);
            }

            db('pg_cherps')->commit();

            // Generate RD transfer for Cherps
            $chvoicesClient = new ChvoicesClient();
            $ims_doc = $chvoicesClient->triggerPoGenerate($invoice_id);

            // Update the o2o_order_address in [chrooms] db
            $order_id = db()->table('o2o_order_list')->where('order_num', $original_id)->select('id')->orderby('id','desc')->first();
            if (!empty($order_id) && !empty($original_id)) {
                db()->table('o2o_order_address')->where('order_id', $order_id->id)->where('address_type', 'CASH_INV')->delete();
                $addr_id = db()->table('o2o_order_address')->insertGetId([
                    'order_id' => $order_id->id,
                    'address_type' => 'CASH_INV',
                    'recipient_name' => $input['cust_name'],
                    'address_line_1' => "0",
                    'country_id' => 'SG',
                    'city_name' => 'SINGAPORE',
                    'postal_code' => '000000',
                    'full_address' => $cust_addr['street_line1'] . "\n" . $cust_addr['street_line2'] . "\nSingapore " . $cust_addr['postal_code'],
                    'created_by' => $staff_id,
                    'created_on' => date('Y-m-d H:i:s'),
                    'modified_by' => $staff_id,
                    'modified_on' => date('Y-m-d H:i:s')
                ]);
                db()->table('o2o_order_list')->where('id', $order_id->id)->update([
                    'billing_address_id' => $addr_id,
                    'modified_by' => $staff_id,
                    'modified_on' => date('Y-m-d H:i:s')
                ]);
            }

        } catch (\Exception $e) {
            db('pg_cherps')->rollback();

            $this->setStatusCode(400);
            return $this->respondWithError([
                "error" => $e
            ]);
        }

        // Return RD number
        return [
            "msg"   => 'RD Invoice Generated',
            "invoice" => $invoice_id,
            "transfer" => (isset($ims_doc->orders[0]->messages)) ? $ims_doc->orders[0]->messages : '',
        ];

    }

    public function oc_invoice(Request $request, Response $response)
    {
        $input = $request->only(['staff_id','ref_id','delv_date','cust_name','tel_code','cust_addr.street_line1','cust_addr.street_line2','cust_addr.postal_code','items']);

        $rules = [
            'staff_id' => 'required',
            'ref_id' => 'required',
            'delv_date' => 'required|date',
            'cust_name' => 'required',
            'tel_code' => 'required',
            'cust_addr' => 'required',
            'items' => 'required|array',
        ];
        $validator = Validator::make($input, $rules, []);

        if ($validator->fails()) {
            $this->setStatusCode(400);
            return $this->respondWithError([
                "error" => $validator->errors($validator)
            ]);
        }

        $staff_id   = $input['staff_id'];
        $ref_id     = $input['ref_id'];
        $delv_date  = $input['delv_date'];
        $cust_name  = $input['cust_name'];
        $tel_code   = $input['tel_code'];
        $cust_addr  = $input['cust_addr'];
        $items      = $input['items'];

        // Get new OC number
        $invoice_id = $this->cherps->sys_trans_list('OC', 'INV', 'SMS', 'CTL', 8);

        // Insert data
        $orderHead = $this->genOrderHead([
            'invoice_id'     => $invoice_id,
            'inv_type'       => 'OC',
            'salesperson_id' => $staff_id,
            'cust_name'      => $cust_name,
            'tel_code'       => $tel_code,
            'contact_person' => $cust_name,
            'cust_addr'      => '0-PRIMARY',
            'delv_addr'      => '2-DELIVERY',
            'delv_date'      => date('Y-m-d', strtotime($delv_date)),
            'ref_id'         => $ref_id,
            'staff_id'       => $staff_id,
            'status_level'   => 1,
        ]);

        foreach ($items as $k => $item) {
            $items[$k]['item_id']   = '#NEA-EWASTE';
            $items[$k]['delv_date'] = date('Y-m-d', strtotime($delv_date));
            $items[$k]['status_level']  = 1;
        }
        $orderItems = $this->genOrderItem(['invoice_id' => $invoice_id, 'staff_id' => $staff_id], $items);

        $orderAddresses[] = $this->genOrderAddress([
            'ref_type'     => 'CASH_INV',
            'ref_id'       => $invoice_id,
            'addr_type'    => '0-PRIMARY',
            'street_line1' => $cust_addr['street_line1'] ?? '',
            'street_line2' => $cust_addr['street_line2'] ?? '',
            'postal_code'  => $cust_addr['postal_code'] ?? '',
            'addr_text'    => $cust_addr['street_line1']. "\n" . $cust_addr['street_line2']. "\nSingapore " . $cust_addr['postal_code'],
            'staff_id'      => $staff_id
        ]);
        $orderAddresses[] = $this->genOrderAddress([
            'ref_type'     => 'CASH_INV',
            'ref_id'       => $invoice_id,
            'addr_type'    => '2-DELIVERY',
            'street_line1' => $cust_addr['street_line1'] ?? '',
            'street_line2' => $cust_addr['street_line2'] ?? '',
            'postal_code'  => $cust_addr['postal_code'] ?? '',
            'addr_text'    => $cust_addr['street_line1']. "\n" . $cust_addr['street_line2']. "\nSingapore " . $cust_addr['postal_code'],
            'staff_id'      => $staff_id
        ]);

        db('pg_cherps')->beginTransaction();

        try {
            db('pg_cherps')->table('sms_invoice_list')->insert($orderHead);
            db('pg_cherps')->table('sms_invoice_item')->insert($orderItems);
            db('pg_cherps')->table('coy_address_book')->insert($orderAddresses);
            db('pg_cherps')->commit();
        } catch (\Exception $e) {
            db('pg_cherps')->rollback();
            throw $e;
        }

        // Return OC number
        return [
            "msg"   => 'OC Invoice Generated',
            "invoice" => $invoice_id
        ];

    }

    public function od_void(Request $request, Response $response)
    {
        $input = $request->all();
        $coy = 'CTL';

        $rules = [
            'staff_id' => 'required',
            'invoice_id' => 'required'
        ];
        $validator = Validator::make($input, $rules, []);

        if ($validator->fails()) {
            $this->setStatusCode(400);
            return $this->respondWithError([
                "error" => $validator->errors($validator)
            ]);
        }

        // Check there is this OD in sms_invoice_list not processed yet
        $invoice = db('pg_cherps')->table('sms_invoice_list')->where([
            'coy_id' => $coy,
            'inv_type' => 'OD',
            'invoice_id' => $input['invoice_id']
        ])->first();

        if (!empty($invoice)) {
            if ($invoice->status_level == 1) {

                $item_amount = db('pg_cherps')->select("SELECT sum(qty_invoiced) trans_qty, sum(unit_price * qty_invoiced) trans_amount FROM sms_invoice_item WHERE coy_id=? AND invoice_id=? and status_level=?", [$coy,$input['invoice_id'],1]);

                // Step 1: Unreserve stocks
                db('pg_cherps')->table('ims_inv_reserved')->where([
                    'coy_id' => $coy,
                    'trans_type' => 'OD',
                    'trans_id' => $input['invoice_id'],
                    'status_level' => 1
                ])->update([
                    'status_level' => -1,
                    'modified_by' => $input['staff_id'],
                    'modified_on' => date('Y-m-d H:i:s')
                ]);

                // Step 2: void this in sms
                db('pg_cherps')->table('sms_invoice_list')->where([
                    'coy_id' => $coy,
                    'inv_type' => 'OD',
                    'invoice_id' => $input['invoice_id']
                ])->update([
                    'status_level' => -1,
                    'modified_by' => $input['staff_id'],
                    'modified_on' => date('Y-m-d H:i:s')
                ]);

                db('pg_cherps')->table('sms_invoice_item')->where([
                    'coy_id' => $coy,
                    'invoice_id' => $input['invoice_id']
                ])->update([
                    'status_level' => -1,
                    'modified_by' => $input['staff_id'],
                    'modified_on' => date('Y-m-d H:i:s')
                ]);

                // Step 3: deduct points from VC
                $refunded_rebate = 0;
                if ($item_amount[0]->trans_amount > 0) {
                    $vcPar = [
                        'mbr_id' => trim($invoice->cust_id),
                        'trans_id' => trim($invoice->invoice_id).'R',
                        'rebate_amount' => $item_amount[0]->trans_amount,
                        'rebate_description' => 'Refund for Invoice ' . trim($invoice->invoice_id) . ' ('. intval($item_amount[0]->trans_qty) .' item/s)'
                    ];
                    $vc = $this->valueclub->award_points($vcPar);
                    if ($vc['status'] == 'success')
                        $refunded_rebate = $item_amount[0]->trans_amount;
                }

                return $this->respondWithArray([
                    "msg"   => 'OD Invoice voided',
                    "invoice" => trim($invoice->invoice_id),
                    "mbr_id" => trim($invoice->cust_id),
                    "rebates" => $refunded_rebate
                ]);

            }
            else {
                return $this->respondWithArray([
                    "error" => "Invoice processed. Cannot be voided."
                ]);
            }
        }
        else {
            return $this->respondWithArray([
                "error" => "Invoice not found."
            ]);
        }

    }

    public function od_invoice(Request $request, Response $response)
    {

        $input = $request->all();

        $rules = [
            'staff_id' => 'required',
            'mbr_id' => 'required',
            'loc_id' => 'required',
            'delv_mode' => 'required',
            'items' => 'required|array',
        ];
        $validator = Validator::make($input, $rules, []);

        if ($validator->fails()) {
            $this->setStatusCode(400);
            return $this->respondWithError([
                "error" => $validator->errors($validator)
            ]);
        }

        $store_id = $input['loc_id'];
        $blockedLoc = setting('pos.sredeem_blocked', '');
        if ($blockedLoc!='' && in_array($store_id, explode(',',$blockedLoc))) {
            return $this->respondWithArray([
                "error" => "Location not opened for redemption"
            ]);
        }


        // If STD delv, must have address
        $item_delivery = [
            'coy_id'        => 'CTL',
            'delv_required' => false,
            'delv_mode_id'  => $input['delv_mode'],
            'loc_id'        => '',
            'addr_type'     => '',
            'delv_date'     => date('Y-m-d'),
            'delv_fee'      => 0,
        ];

        if ($input['delv_mode'] == 'STD') {
            $rules = [
                'delv_addr.street_line1' => 'required',
                'delv_addr.street_line2' => 'required',
                'delv_addr.postal_code' => 'required'
            ];
            $validator = Validator::make($input, $rules, []);

            if ($validator->fails()) {
                $this->setStatusCode(400);
                return $this->respondWithError([
                    "error" => $validator->errors($validator)
                ]);
            }

            // Check that have sufficient stocks
            foreach ($input['items'] as $k => $itm) {
                $sql = "select sum(qty_on_hand) - sum(qty_reserved) qty_available from v_ims_inv_summary where coy_id=? and loc_id=? and item_id=?";
                $item_check = db('pg_cherps')->select($sql, ['CTL', $itm['loc_id'], $itm['item_id']]);

                if (!($item_check && $item_check[0]->qty_available >= 1)) {
                    return $this->respondWithArray([
                        "error" => "Item sold out"
                    ]);
                }
            }

            $item_delivery = [
                'delv_required' => true,
                'delv_mode_id'  => 'NID', // To get from DB
                'loc_id'        => 'HCL',
                'addr_type'     => '2-DELIVERY',
                'delv_date'     => date('Y-m-d', strtotime('+ 2 weekday')),
                'delv_fee'      => 8,
            ];
        }


        // Check membership
        $member = $this->valueclub->validate($input['mbr_id']);
        if ($member) {

            $mbr_id = trim($member['mbr_id']);

            $total_rebates_used = collect($input['items'])->sum(function ($itm) {
                $qty = $itm['trans_qty'] ?? 1;
                return $itm['unit_price'] * $qty;
            });

            if ($total_rebates_used > floatval($member['rebate'])) {
                return $this->respondWithArray([
                    "error" => "Insufficient member rebates"
                ]);
            }

            //
            // Pass all checking. Create invoice
            //

            // Get new Doc number
            $store_id = $input['loc_id'];
            $staff_id = strtolower($input['staff_id']);
            $trans_type = 'OD';
            $status_level = (isset($input['status_level'])) ? $input['status_level'] : 1;
            $invoice_id = $this->cherps->sys_trans_list($trans_type, 'INV', 'SMS', 'CTL', 8);

            // Billing Address
            if ((empty($member['billing']))) {
                $location = pos_locations($store_id);
                $orderAddresses[] = $this->genOrderAddress([
                    'ref_type'     => 'CASH_INV',
                    'ref_id'       => $invoice_id,
                    'addr_type'    => '0-PRIMARY',
                    'street_line1' => $location->street_line1,
                    'street_line2' => $location->street_line2,
                    'postal_code'  => '408553',
                    'addr_text'    => $location->street_line1. "\n" . $location->street_line2 . "\nSingapore",
                    'staff_id'      => $staff_id
                ]);
            }
            else {
                $orderAddresses[] = $this->genOrderAddress([
                    'ref_type'     => 'CASH_INV',
                    'ref_id'       => $invoice_id,
                    'addr_type'    => '0-PRIMARY',
                    'street_line1' => $member['billing']['street_line1'] ?? '',
                    'street_line2' => $member['billing']['street_line2'] ?? '',
                    'postal_code'  => $member['billing']['postal_code'] ?? '',
                    'addr_text'    => $member['billing']['street_line1']. "\n" . $member['billing']['street_line2']. "\nSingapore " . $member['billing']['postal_code'],
                    'staff_id'     => $staff_id
                ]);
            }

            // Shipping Address
            if ($item_delivery['delv_required'] == true) {
                $orderAddresses[] = $this->genOrderAddress([
                    'ref_type' => 'CASH_INV',
                    'ref_id' => $invoice_id,
                    'addr_type' => $item_delivery['addr_type'],
                    'street_line1' => $input['delv_addr']['street_line1'] ?? '',
                    'street_line2' => $input['delv_addr']['street_line2'] ?? '',
                    'postal_code' => $input['delv_addr']['postal_code'] ?? '',
                    'addr_text' => $input['delv_addr']['street_line1'] . "\n" . $input['delv_addr']['street_line2'] . "\nSingapore " . $input['delv_addr']['postal_code'],
                    'staff_id' => $staff_id
                ]);
            }

            // Insert data
            $orderHead = $this->genOrderHead([
                'invoice_id'     => $invoice_id,
                'ref_id'         => $invoice_id,
                'cust_po_code'   => $invoice_id,
                'inv_type'       => $trans_type,
                'payment_id'     => $trans_type,
                'delv_term_id'   => 'DEL',
                'delv_mode_id'   => 'LND',
                'salesperson_id' => $staff_id,
                'mbr_ind'        => 'Y',
                'cust_id'        => $mbr_id,
                'cust_name'      => $member['first_name'] .' '. $member['last_name'],
                'contact_person' => $member['first_name'] .' '. $member['last_name'],
                'first_name'     => $member['first_name'],
                'last_name'      => $member['last_name'],
                'tel_code'       => $member['contact_num'],
                'email_addr'     => $member['email_addr'],
                'cust_addr'      => '0-PRIMARY',
                'loc_id'         => $store_id,
                'delv_addr'      => $item_delivery['addr_type'],
                'delv_date'      => $item_delivery['delv_date'],
                'staff_id'       => $staff_id,
                'status_level'   => $status_level,
            ]);

            $items = [];
            foreach ($input['items'] as $k => $itm) {
                $items[] = [
                    "item_id"       => $itm['item_id'],
                    "item_desc"     => $itm['item_desc'],
                    "qty"           => $itm['trans_qty'] ?? 1,
                    "unit_price"    => $itm['unit_price'] ?? 1,
                    'inv_loc'       => $itm['loc_id'],
                    'loc_id'        => ($item_delivery['loc_id'] == '') ? $itm['loc_id'] : $item_delivery['loc_id'],
                    'delv_mode_id'  => $item_delivery['delv_mode_id'],
                    'delv_date'     => $item_delivery['delv_date'],
                    'fin_dim1'      => '61-ONLINE SALES',
                    'fin_dim2'      => 'HARDWARE',
                    'ref_id'        => $invoice_id,
                    'staff_id'      => $staff_id,
                    'status_level'  => $status_level
                ];
            }
            if ($item_delivery['delv_fee'] > 0) {
                $items[] = [
                    "item_id"       => '#DELV_CHG',
                    "item_desc"     => 'Delivery Charges',
                    "qty"           => 1,
                    "unit_price"    => $item_delivery['delv_fee'],
                    'inv_loc'       => '',
                    'loc_id'        => '',
                    'delv_mode_id'  => '',
                    'fin_dim1'      => '61-ONLINE SALES',
                    'fin_dim2'      => 'SERVICES',
                    'ref_id'        => $invoice_id,
                    'staff_id'      => $staff_id,
                    'status_level'  => 3
                ];
            }
            $orderItems = $this->genOrderItem(['invoice_id' => $invoice_id, 'staff_id' => $staff_id], $items);

            db('pg_cherps')->beginTransaction();
            try {
                db('pg_cherps')->table('coy_address_book')->insert($orderAddresses);
                db('pg_cherps')->table('sms_invoice_list')->insert($orderHead);
                db('pg_cherps')->table('sms_invoice_item')->insert($orderItems);
                db('pg_cherps')->commit();
            } catch (\Exception $e) {
                db('pg_cherps')->rollback();
                //throw $e;

                return $this->respondWithArray([
                    "error" => "Fail to create invoice"
                ]);
            }

            // Reserve rebate at VC
            $error = '';
            if ($this->valueclub->reserve_rebates($input['mbr_id'], $invoice_id, $total_rebates_used)) {

                // Save payment info
                db('pg_cherps')->table('sms_payment_list')->insert([
                    'coy_id'        => 'CTL',
                    'invoice_id'    => $invoice_id,
                    'line_num'      => 1,
                    'pay_mode'      => '!VCH-STAR001',
                    'trans_amount'  => $total_rebates_used,
                    'trans_ref1'    => '',
                    'status_level'  => 0,
                    'created_by'    => $staff_id,
                    'created_on'    => now(),
                    'modified_by'   => $staff_id,
                    'modified_on'   => now()
                ]);

                // Post order to VC (prepare receipt_data)
                $loc_id = ($store_id) ? $store_id : "HCL";
                if ($location = pos_locations($loc_id)) {
                    $logoId            = $location->logo_id;
                    $storeShopName     = $location->shop_name;
                    $storeAddressLine1 = $location->street_line1;
                    $storeAddressLine2 = $location->street_line2;
                    $storeAddressLine3 = '';
                    if (isset($location->coy_reg_code)) {
                        $storeAddressLine3 .= ($storeAddressLine3!='') ? ' | ' : '';
                        $storeAddressLine3 .= 'ROC ' . ($location->coy_reg_code ?? '');
                    }
                    if (isset($location->coy_tax_code)) {
                        $storeAddressLine3 .= ($storeAddressLine3 != '') ? ' | ' : '';
                        $storeAddressLine3 .= 'GST ' . ($location->coy_tax_code ?? '');
                    }
                }

                $receipt_data = [
                    "loc_id" => $store_id,
                    "pos_id" => $input['pos_id'] ?? '',
                    "invoice_num" => $invoice_id,
                    "trans_num" => $invoice_id, // To be optional
                    "rebates_earned" => number_format(0, 2),
                    "trans_type" => $trans_type,
                    "trans_date" => now()->toDateTimeString(),
                    "cashier_id" => $staff_id,
                    "customer_info" => [
                        "id" => $mbr_id,
                        "masked_id" => "******" . substr(trim($mbr_id), -4),
                        "name" => $member['first_name'] .' '. $member['last_name'],
                        "exp_date" => Carbon::createFromTimeString($member['exp_date'])->format('d M Y')
                    ],
                    "payments" => [
                        [
                            "pay_mode" => "REBATE",
                            "info" => "",
                            "approval_code" => "",
                            "trans_amount" => number_format($total_rebates_used, 2)
                        ]
                    ],
                    // For receipt printing:
                    'logo_id' => isset($logoId) ? $logoId : 'CTL',
                    'store_name' => isset($storeShopName) ? $storeShopName : 'Ubi (Challenger HQ)',
                    'store_addr_line1' => isset($storeAddressLine1) ? $storeAddressLine1 : '1 UBI LINK',
                    'store_addr_line2' => isset($storeAddressLine2) ? $storeAddressLine2 : 'Challenger TecHub',
                    'store_addr_line3' => isset($storeAddressLine3) ? $storeAddressLine3 : '',
                    "reprint" => "N",
                    "display_trans_date" => date('d/m/Y H:i:s'),
                    "cashier_name" => $staff_id,
                    "receipt_header" => "V$ REDEMPTION",
                    "receipt_message" => "",
                    "receipt_qr_msg" => "",
                    "receipt_qr" => "",
                    "trans_ref_id" => "",
                    "trans_point" => 0,
                    "rounding_adj" => 0,
                    "subtotal" => number_format(0, 2),
                    "tax_amount" => number_format(0, 2),
                    "total_paid" => number_format(0, 2),
                    "change_due" => number_format(0, 2),
                    "total_savings" => number_format(0, 2),
                    "ssew_info" => null,
                    "order_status_level" => 1,
                    "pay_status_level" => 2,
                    // Updated by items
                    "item_count" => 0,
                    "total_amount" => 0, // To change to number format later
                ];
                foreach ($items as $k => $itm) {
                    $receipt_data['items'][] = [
                        "line_num" => $k+1,
                        "item_id" => $itm['item_id'],
                        "item_desc" => $itm['item_desc'],
                        "qty_ordered" => $itm['qty'],
                        "unit_price" => number_format($itm['unit_price'], 2),
                        "unit_discount" => number_format(0, 2),
                        "regular_price" => number_format($itm['unit_price'], 2),
                        "total_price" => number_format(($itm['unit_price'] * $itm['qty']), 2),
                        "mbr_savings" => 0,
                        "trans_point" => 0,

                        "item_message" => "",
                        "item_type" => "I",
                        "ref_line_num" => 0,
                    ];
                    $receipt_data['item_count'] += 1;
                    $receipt_data['total_amount'] += $itm['unit_price'] * $itm['qty'];
                };
                // Change value to string for printing
                $receipt_data['total_amount'] = number_format($receipt_data['total_amount'], 2);

                if ($post_transaction = $this->valueclub->post_transaction($receipt_data)) {

                    // Reserve stock at Cherps
                    $rq_loc = '';
                    $rq_items = [];
                    foreach ($input['items'] as $k => $itm) {
                        $rq_loc = $itm['loc_id'];
                        $rq_items[] = [
                            "item_id" => $itm['item_id'],
                            "line_num" => $k + 1,
                            "qty_reserved" => $itm['trans_qty'] ?? 1
                        ];
                    };
                    $rq = $this->imsapi->confirm_reserved('ORDER', $trans_type, $invoice_id, $rq_loc, $rq_items, $staff_id);

                    // Send Push Notification
                    $invoice_url = get_constant('url.invoice') . encrypt_decrypt( $invoice_id,'encrypt');
                    $push_msg = "Thank you for your redemption. Invoice $invoice_id is ready.";
                    $push = $this->valueclub->app_push_notification($mbr_id,'V$ Redemption',$push_msg, $invoice_url);

                    // Return Redemption OD number
                    $msg = 'Redemption invoice '.$invoice_id.' generated. ';
                    if (isset($push['code']) && $push['code']==1) {
                        $msg .= 'Push notification should be sent out soon.';
                    }
                    return [
                        "msg"   => $msg,
                        "invoice" => $invoice_id,
                        "receipt" => $receipt_data,
                        "push" => $push
                    ];

                }
                else {
                    // Unreserve the rebates on error
                    $this->valueclub->unreserve_rebates($input['mbr_id'], $invoice_id);
                    $error = 'Fail to post transaction to VC';
                }

            }
            else {
                $error = 'Fail to reserve rebates and post transaction to VC';
            }

            return $this->respondWithArray([
                "msg"   => 'OD Invoice Generated with errors',
                "invoice" => $invoice_id,
                "error" => $error
            ]);

        }

        return $this->respondWithArray([
            "error" => "Invalid membership data"
        ]);

    }

    /*
     * Generate data for sms_invoice_list, sms_invoice_item & coy_address_book
     */

    private function genOrderHead($data){
        return [
            'coy_id'         => 'CTL',
            'invoice_id'     => $data['invoice_id'],
            'inv_type'       => $data['inv_type'],
            'invoice_date'   => isset($data['invoice_date']) ? $data['invoice_date'] : now(),
            'status_level'   => isset($data['status_level']) ? $data['status_level'] : 1,
            'salesperson_id' => isset($data['salesperson_id']) ? $data['salesperson_id'] : '',
            'cust_id'        => isset($data['cust_id']) ? $data['cust_id'] : '',
            'cust_name'      => Str::substr( isset($data['cust_name']) ? $data['cust_name'] : '' ,0,60),
            'first_name'     => Str::substr( isset($data['first_name']) ? $data['first_name'] : '' ,0,40),
            'last_name'      => Str::substr( isset($data['last_name']) ? $data['last_name'] : '' ,0,40),
            'tel_code'       => Str::substr( isset($data['tel_code']) ? $data['tel_code'] : '' ,0,30),
            'contact_person' => Str::substr( isset($data['contact_person']) ? $data['contact_person'] : '' ,0,30),
            'email_addr'     =>  Str::substr(isset($data['email_addr']) ? $data['email_addr'] : '' ,0,60),
            'cust_addr'      => isset($data['cust_addr']) ? $data['cust_addr'] : '',
            'delv_addr'      => isset($data['delv_addr']) ? $data['delv_addr'] : '',
            'delv_date'      => isset($data['delv_date']) ? $data['delv_date'] : now(),
            'cust_po_code'   => isset($data['ref_id']) ? $data['ref_id'] : '',
            'mbr_ind'        => isset($data['mbr_ind']) ? $data['mbr_ind'] : '',
            'ref_id'         => isset($data['ref_id']) ? $data['ref_id'] : '',
            'loc_id'         => isset($data['loc_id']) ? $data['loc_id'] : '',
            'curr_id'        => isset($data['curr_id']) ? $data['curr_id'] : 'SGD',
            'exchg_rate'     => isset($data['exchg_rate']) ? $data['exchg_rate'] : 1,
            'exchg_unit'     => isset($data['exchg_unit']) ? $data['exchg_unit'] : 1,
            'payment_id'     => isset($data['payment_id']) ? $data['payment_id'] : '',
            'delv_term_id'   => isset($data['delv_term_id']) ? $data['delv_term_id'] : '',
            'delv_mode_id'   => isset($data['delv_mode_id']) ? $data['delv_mode_id'] : '',
            'tax_id'         => isset($data['tax_id']) ? $data['tax_id'] : gst()->tax_id,
            'tax_percent'    => isset($data['tax_percent']) ? $data['tax_percent'] : gst()->tax_percent,
            'remarks'        => isset($data['remarks']) ? $data['remarks'] : '',
            'created_by'     => isset($data['staff_id']) ? $data['staff_id'] : '',
            'created_on'     => now(),
            'modified_by'    => isset($data['staff_id']) ? $data['staff_id'] : '',
            'modified_on'    => now(),
        ];
    }

    private function genOrderItem($data, $items) {
        return collect($items)->values()->map(function ($item, $index) use ($data) {
            $itm = [
                'coy_id'         => 'CTL',
                'invoice_id'     => $data['invoice_id'],
                'line_num'       => $index + 1,
                'item_id'        => $item['item_id'],
                'item_desc'      => Str::substr($item['item_desc'], 0, 60),
                'qty_invoiced'   => isset($item['qty']) ? $item['qty'] : 1,
                'uom_id'         => '',
                'unit_price'     => isset($item['unit_price']) ? $item['unit_price'] : 0,
                'disc_amount'    => isset($item['disc_amount']) ? $item['disc_amount'] : 0,
                'disc_percent'   => isset($item['disc_percent']) ? $item['disc_percent'] : 0,
                'status_level'   => isset($item['status_level']) ? $item['status_level'] : 0,
                'fin_dim1'       => isset($item['fin_dim1']) ? $item['fin_dim1'] : '',
                'fin_dim2'       => isset($item['fin_dim2']) ? $item['fin_dim2'] : '',
                'fin_dim3'       => isset($item['fin_dim3']) ? $item['fin_dim3'] : '',
                'ref_id'         => isset($item['ref_id']) ? $item['ref_id'] : '',
                'salesperson_id' => isset($item['salesperson_id']) ? $item['salesperson_id'] : '',
                'bom_ind'        => isset($item['bom_ind']) ? $item['bom_ind'] : '',
                'promo_id'       => isset($item['promo_id']) ? $item['promo_id'] : '',
                'promoter_id'    => isset($item['promoter_id']) ? $item['promoter_id'] : '',
                'delv_mode_id'   => isset($item['delv_mode_id']) ? $item['delv_mode_id'] : '',
                'delv_date'      => isset($item['delv_date']) ? $item['delv_date'] : now(),
                'inv_loc'        => isset($item['inv_loc']) ? $item['inv_loc'] : '',
                'loc_id'         => isset($item['loc_id']) ? $item['loc_id'] : '',
                'created_by'     => isset($data['staff_id']) ? $data['staff_id'] : '',
                'created_on'     => now(),
                'modified_by'    => isset($data['staff_id']) ? $data['staff_id'] : '',
                'modified_on'    => now(),
            ];

            if (isset($item['qty_picked']) && $item['qty_picked']>0) {
                $itm['qty_picked'] = $item['qty_picked'];
                $itm['pick_date'] = now();
            }

            return $itm;
        })->toArray();
    }

    private function genOrderAddress($data){
        return [
            'coy_id'       => 'CTL',
            'ref_type'     => $data['ref_type'],
            'ref_id'       => $data['ref_id'],
            'addr_type'    => (isset($data['addr_type'])) ? $data['addr_type'] : '0-PRIMARY',
            'addr_format'  => '1',
            'street_line1' => Str::substr(((isset($data['street_line1'])) ? $data['street_line1'] : ''), 0, 30),
            'street_line2' => Str::substr(((isset($data['street_line2'])) ? $data['street_line2'] : ''), 0, 30),
            'street_line3' => Str::substr(((isset($data['street_line3'])) ? $data['street_line3'] : ''), 0, 30),
            'street_line4' => Str::substr(((isset($data['street_line4'])) ? $data['street_line4'] : ''), 0, 30),
            'city_name'    => Str::substr(((isset($data['city_name'])) ? $data['city_name'] : ''), 0, 30),
            'state_name'   => Str::substr(((isset($data['state_name'])) ? $data['state_name'] : ''), 0, 30),
            'country_id'   => Str::substr(((isset($data['country_id'])) ? $data['country_id'] : 'SG'), 0, 30),
            'postal_code'  => Str::substr(((isset($data['postal_code'])) ? $data['postal_code'] : ''), 0, 30),
            'addr_text'    => Str::substr(((isset($data['addr_text'])) ? $data['addr_text'] : ''), 0, 300),
            'created_by'   => isset($data['staff_id']) ? $data['staff_id'] : '',
            'created_on'   => now(),
            'modified_by'  => isset($data['staff_id']) ? $data['staff_id'] : '',
            'modified_on'  => now(),
        ];
    }

    private function reserveStocks($orderHead,$orderItems){
        $invReserved = collect($orderItems)->values()->map(function ($item) use ($orderHead) {
            return [
                'coy_id'         => $orderHead['coy_id'],
                'item_id'        => $item['item_id'],
                'loc_id'         => $item['inv_loc'],
                'doc_type'       => 'ORDER',
                'trans_type'     => $orderHead['inv_type'],
                'trans_id'       => $orderHead['invoice_id'],
                'line_num'       => $item['line_num'],
                'trans_date'     => $orderHead['invoice_date'],
                'qty_reserved'   => $item['qty_invoiced'],
                'status_level'   => 1,
                'created_by'     => $orderHead['created_by'] ?? '',
                'created_on'     => now(),
                'modified_by'    => $orderHead['created_by'] ?? '',
                'modified_on'    => now(),
            ];
        })->toArray();

        return db('pg_cherps')->table('ims_inv_reserved')->insert($invReserved);
    }

}