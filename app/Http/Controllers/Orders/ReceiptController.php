<?php

namespace App\Http\Controllers\Orders;

use App\Core\Orders\Models\Order;
use App\Http\Controllers\Controller;
use App\Http\Resources\Orders\OrderReceiptResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ReceiptController extends Controller
{
    public function index()
    {

    }

    public function show($id)
    {
        try {
            $order = Order::withoutGlobalScope('open')->where('order_num', $id)->orWhere('invoice_id', $id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        $order->load([
            'lines' => function ($q) {
                $q->active()->orderBy('line_num');
            }, 'transactions',
        ]);

        return new OrderReceiptResource($order);
    }

    public function reprint($id)
    {
        try {
            $order = Order::withoutGlobalScope('open')->where('order_num', $id)->orWhere('invoice_id', $id)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        $order->load([
            'lines' => function ($q) {
                $q->active()->orderBy('line_num');
            }, 'transactions',
        ]);

        return (new OrderReceiptResource($order))->append(['reprint' => 'Y']);
    }

    public function sendReceipt()
    {

    }
}