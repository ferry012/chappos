<?php

namespace App\Http\Controllers\Orders;

use App\Http\Controllers\Controller;
use App\Support\Guzzle\ChvoicesClient;
use App\Support\Str;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

use App\Services\CherpsService;

class QuotationController extends Controller
{

    private $cherps, $coy;

    public function __construct(CherpsService $cherps)
    {
        $this->cherps  = $cherps;
        $this->coy = 'CTL';
    }

    public function __invoke(Request $request, Response $response)
    {
        $input = $request->all();

        $rules = [
            'staff_id' => 'required',
            'cust_id' => 'required',
            'cust_name' => 'required',
            'salesperson_id' => 'required',
            'tel_code' => 'required',
            'email_addr' => 'required',
            'cust_addr' => 'required',
            'items' => 'required|array',
        ];
        $validator = Validator::make($input, $rules, []);

        if ($validator->fails()) {
            $this->setStatusCode(400);
            return $this->respondWithError([
                "error" => $validator->errors($validator)
            ]);
        }

        // Get new Doc number
        $staff_id = strtolower($input['staff_id']);
        $status_level = 0;
        $trans_type = 'QC';
        $invoice_id = $this->cherps->sys_trans_list($trans_type, 'QUOTE', 'SMS', $this->coy, 8);

        // Insert data
        $orderHead = $this->genQuoteHead([
            'quote_id'       => $invoice_id,
            'quote_type'     => $trans_type,
            'quote_date'     => now(),
            'status_level'   => $status_level,
            'salesperson_id' => $input['salesperson_id'],
            'cust_id'        => $input['cust_id'],
            'cust_name'      => $input['cust_name'],
            'cust_addr'      => '0-PRIMARY',
            'tel_code'       => $input['tel_code'],
            'contact_person' => $input['contact_person'],
            'email_addr'     => $input['email_addr'],
            'loc_id'         => $input['loc_id'],
            'delv_name'      => $input['contact_person'],
            'delv_addr'      => '3-INVOICE',
            'remarks'        => $input['remarks'],
            'staff_id'       => $staff_id,
        ]);

        $items = [];
        foreach ($input['items'] as $k => $itm) {
            $items[] = [
                "item_id"       => $itm['item_id'],
                "item_desc"     => $itm['item_desc'],
                "unit_price"    => $itm['unit_price'] ?? 0,
                "qty"           => $itm['qty'] ?? 1,
                "uom"           => $itm['uom'] ?? '',
                'fin_dim1'      => '',
                'fin_dim2'      => '',
                'status_level'  => $status_level,
                'staff_id'      => $staff_id,
            ];
        }
        $orderItems = $this->genQuoteItem(['quote_id' => $invoice_id, 'staff_id' => $staff_id], $items);

        // Primary Address
        $address = $input['cust_addr'];
        $orderAddresses[] = $this->genQuoteAddress([
            'ref_type'     => 'CASH_QUOTE',
            'ref_id'       => $invoice_id,
            'addr_type'    => '0-PRIMARY',
            'street_line1' => $address['street_line1'] ?? '',
            'street_line2' => $address['street_line2'] ?? '',
            'street_line3' => $address['street_line3'] ?? '',
            'postal_code'  => $address['postal_code'] ?? '',
            'addr_text'    => $address['street_line1']. "\n" . $address['street_line2']. "\n" . $address['street_line3']. "\nSingapore " . $address['postal_code'],
            'staff_id'     => $staff_id
        ]);

        // Shipping Address
        if (isset($input['delv_addr'])) {
            $address = $input['delv_addr'];
        }
        $orderAddresses[] = $this->genQuoteAddress([
            'ref_type'     => 'CASH_QUOTE',
            'ref_id'       => $invoice_id,
            'addr_type'    => '3-INVOICE',
            'street_line1' => $address['street_line1'] ?? '',
            'street_line2' => $address['street_line2'] ?? '',
            'street_line3' => $address['street_line3'] ?? '',
            'postal_code'  => $address['postal_code'] ?? '',
            'addr_text'    => $address['street_line1']. "\n" . $address['street_line2']. "\n" . $address['street_line3']. "\nSingapore " . $address['postal_code'],
            'staff_id'     => $staff_id
        ]);


        db('pg_cherps')->beginTransaction();
        try {
            db('pg_cherps')->table('coy_address_book')->insert($orderAddresses);
            db('pg_cherps')->table('sms_quote_list')->insert($orderHead);
            db('pg_cherps')->table('sms_quote_item')->insert($orderItems);

            if (isset($input['notes'])) {
                db('pg_cherps')->table('sms_quote_notes')->insert([
                    "coy_id"      => $this->coy,
                    "quote_id"    => $invoice_id,
                    "quote_notes" => $input['notes'],
                    'created_by'     => $staff_id,
                    'created_on'     => now(),
                    'modified_by'    => $staff_id,
                    'modified_on'    => now(),
                ]);
            }

            db('pg_cherps')->commit();
        } catch (\Exception $e) {
            db('pg_cherps')->rollback();
            //throw $e;

            return $this->respondWithArray([
                "error" => "Fail to create quotation"
            ]);
        }

        $action = '';
        if ($request->get('action')) {
            if (Str::upper($request->get('action')) == "IHL") {
                $action = $this->quote_action_IHL($invoice_id, $input);
            }
        }

        // Return OC number
        return [
            "msg"    => 'Quotation Generated',
            "quote"  => $invoice_id,
            "action" => $action
        ];

    }

    /*
     * Special Quotation Actions
     */

    private function quote_action_IHL($invoice_id, $input){
        $ip = ''; //request()->ip();

        $subject = "IHL Order - " . $invoice_id . " (" . Str::upper($input["notes_arr"]["school"]). " | " .$input["notes_arr"]["first_name"] . " " . $input["notes_arr"]["last_name"] . ")";
        $email_to = (!app()->environment('production')) ? "mis@challenger.sg" : "edu@challenger.sg";
        $email_stud = $input["email_addr"];

        $item_total = 0;
        $content_item = "<table border='0' width='100%'>";
        $content_item.= "<tr><td><u>Item ID</u></td><td><u>Item Description</u></td><td align='right'><u>Unit Price</u></td></tr>";
        foreach ($input['items'] as $k => $itm) {
            $content_item.= "<tr><td>" . $itm['item_id'] . "</td><td>" . $itm['item_desc'] . "</td><td align='right'>$". number_format($itm['unit_price'],2) ."</td></tr>";
            $item_total += $itm['unit_price'];
        }
        $content_item.= "</table>";

        $content = "<table border='1' width='100%' cellpadding='5'>
    <tr>
        <td colspan='2'><h2>".$invoice_id."</h2></td>
    </tr>
    <tr>
        <td width='50%'>
            Student ID: ".$input["notes_arr"]["student_id"]."<br>
            Name: ".$input["notes_arr"]["first_name"]." ".$input["notes_arr"]["last_name"]."<br>
            Course: ".$input["notes_arr"]["course"]."<br>
            School: ". Str::upper($input["notes_arr"]["school"]) ."<br><br>
            Tel: ".$input["tel_code"]."<br>
            Email: ".$input["email_addr"]."<br>
        </td>
        <td width='50%'>
            Deliver to:<br>
            ".$input["cust_addr"]["street_line1"]."<br>
            ".$input["cust_addr"]["street_line2"]."<br>
            ".$input["cust_addr"]["street_line3"]."<br>
            ".$input["cust_addr"]["country_id"]." ".$input["cust_addr"]["postal_code"]."<br><br>
            Secondary Contact: ".$input["notes_arr"]["secondary_person"]." (".$input["notes_arr"]["secondary_contact"].")
        </td>
    </tr>
    <tr>
        <td colspan='2'>".$content_item."</td>
    </tr>
    <tr>
        <td colspan='2' align='center'>
            Selected Payment Method:<br>
            ".$input["notes_arr"]["pay_mode"]."<br>
            ($". number_format($item_total,2) .")
        </td>
    </tr>
    <tr>
        <td colspan='2'>
            ". implode("<br>", $input["notes_arr"]["terms"]) ."
            <br><br>". $input["notes_arr"]["acknowledgement"] ."
        </td>
    </tr>
    <tr>
        <td colspan='2' align='center'>
            Sent ". date('d M Y H:i:s') ." (".$ip.")
        </td>
    </tr>
</table>";

//        $content = "First Name: " . $input["notes_arr"]["first_name"];
//        $content.= "<br>Last Name: " . $input["notes_arr"]["last_name"];
//        $content.= "<br>Student ID: " . $input["notes_arr"]["student_id"];
//        $content.= "<br>Course: " . $input["notes_arr"]["course"];
//        $content.= "<br>Tel/HP: " . $input["tel_code"];
//        $content.= "<br>Email: " . $input["email_addr"];
//
//        $content.= "<br><br>Payment Method: " . $input["notes_arr"]["pay_mode"];
//
//        $content.= "<hr>Delivery Address:";
//        $content.= "<br>" . $input["cust_addr"]["street_line1"];
//        $content.= "<br>" . $input["cust_addr"]["street_line2"];
//        $content.= "<br>" . $input["cust_addr"]["street_line3"];
//        $content.= "<br>" . $input["cust_addr"]["country_id"] . " "  . $input["cust_addr"]["postal_code"];
//
//        $content.= "<br><br>Secondary Contact: " . $input["notes_arr"]["secondary_person"] . " / " . $input["notes_arr"]["secondary_contact"];
//
//        $content.= "<hr>";
//        foreach ($input['items'] as $k => $itm) {
//            $content.= "<br>" . $itm['item_id'] . " - " . $itm['item_desc'] . " ($". $itm['unit_price'] .")";
//        }

        $files = []; $files_count=0;
        if (isset($input['files']['studentId']) && !empty($input['files']['studentId'])) {
            $f = $this->genImageFromBase64('studentId', $input['files']['studentId']);
            $files['base64_filename'] =  $f[0];
            $files['base64_content'] =  $f[1];
            $files_count++;
        }
        if (isset($input['files']['loa']) && !empty($input['files']['loa'])) {
            $f = $this->genImageFromBase64('loa', $input['files']['loa']);
            $files['base64_filename1'] =  $f[0];
            $files['base64_content1'] =  $f[1];
            $files_count++;
        }
        ctl_mail($email_to, $subject, $content, [], $files);
        ctl_mail($email_stud, $subject, $content, [], [], 'edu@challenger.sg');

        if (isset($input['files']))
            return "Email - " . $subject .' (Files: '.$files_count.')';
    }

    /*
     * Generate data
     */

    private function genQuoteHead($data){
        return [
            'coy_id'         => $this->coy,
            'quote_id'       => $data['quote_id'],
            'rev_num'        => 0,
            'current_rev'    => 0,
            'quote_type'     => $data['quote_type'],
            'quote_date'     => isset($data['quote_date']) ? $data['quote_date'] : now(),
            'status_level'   => isset($data['status_level']) ? $data['status_level'] : 0,
            'salesperson_id' => isset($data['salesperson_id']) ? $data['salesperson_id'] : '',
            'cust_id'        => isset($data['cust_id']) ? $data['cust_id'] : '',
            'cust_name'      => Str::substr( isset($data['cust_name']) ? $data['cust_name'] : '' ,0,60),
            'cust_addr'      => isset($data['cust_addr']) ? $data['cust_addr'] : '',
            'tel_code'       => Str::substr( isset($data['tel_code']) ? $data['tel_code'] : '' ,0,30),
            'fax_code'       => Str::substr( isset($data['fax_code']) ? $data['fax_code'] : '' ,0,30),
            'contact_person' => Str::substr( isset($data['contact_person']) ? $data['contact_person'] : '' ,0,30),
            'email_addr'     => Str::substr(isset($data['email_addr']) ? $data['email_addr'] : '' ,0,60),
            'mbr_ind'        => isset($data['mbr_ind']) ? $data['mbr_ind'] : 'N',
            'delv_date'      => isset($data['delv_date']) ? $data['delv_date'] : now(),
            'loc_id'         => isset($data['loc_id']) ? $data['loc_id'] : '',
            'delv_name'      => Str::substr( isset($data['contact_person']) ? $data['contact_person'] : '' ,0,60),
            'delv_addr'      => isset($data['delv_addr']) ? $data['delv_addr'] : '',
            'ship_via'       => '',
            'curr_id'        => isset($data['curr_id']) ? $data['curr_id'] : 'SGD',
            'exchg_rate'     => isset($data['exchg_rate']) ? $data['exchg_rate'] : 1,
            'exchg_unit'     => isset($data['exchg_unit']) ? $data['exchg_unit'] : 1,
            'payment_id'     => isset($data['payment_id']) ? $data['payment_id'] : '30D',
            'delv_term_id'   => isset($data['delv_term_id']) ? $data['delv_term_id'] : 'DEL',
            'delv_location'  => '',
            'delv_mode_id'   => isset($data['delv_mode_id']) ? $data['delv_mode_id'] : 'LND',
            'tax_id'         => isset($data['tax_id']) ? $data['tax_id'] : gst()->tax_id,
            'tax_percent'    => isset($data['tax_percent']) ? $data['tax_percent'] : gst()->tax_percent,
            'approve_by'     => '',
            'approve_date'   => now(),
            'remarks'        => isset($data['remarks']) ? $data['remarks'] : '', 
            'bom_ind'        => isset($data['bom_ind']) ? $data['bom_ind'] : '',
            'created_by'     => isset($data['staff_id']) ? $data['staff_id'] : '',
            'created_on'     => now(),
            'modified_by'    => isset($data['staff_id']) ? $data['staff_id'] : '',
            'modified_on'    => now(),
        ];
    }

    private function genQuoteItem($data, $items) {
        return collect($items)->values()->map(function ($item, $index) use ($data) {
            return [
                'coy_id'         => $this->coy,
                'quote_id'       => $data['quote_id'],
                'rev_num'        => 0,
                'line_num'       => $index + 1,
                'item_id'        => $item['item_id'],
                'item_desc'      => Str::substr($item['item_desc'], 0, 60),
                'qty_order'      => isset($item['qty']) ? $item['qty'] : 1,
                'uom_id'         => isset($item['uom']) ? $item['uom'] : '',
                'unit_price'     => isset($item['unit_price']) ? $item['unit_price'] : 0,
                'disc_percent'   => isset($item['disc_percent']) ? $item['disc_percent'] : 0,
                'status_level'   => isset($item['status_level']) ? $item['status_level'] : 0,
                'long_desc'      => '',
                'cust_item_id'   => '',
                'fin_dim1'       => isset($item['fin_dim1']) ? $item['fin_dim1'] : '',
                'fin_dim2'       => isset($item['fin_dim2']) ? $item['fin_dim2'] : '',
                'fin_dim3'       => isset($item['fin_dim3']) ? $item['fin_dim3'] : '',
                'bom_ind'        => isset($item['bom_ind']) ? $item['bom_ind'] : '',
                'created_by'     => isset($data['staff_id']) ? $data['staff_id'] : '',
                'created_on'     => now(),
                'modified_by'    => isset($data['staff_id']) ? $data['staff_id'] : '',
                'modified_on'    => now(),
            ];
        })->toArray();
    }

    private function genQuoteAddress($data){
        return [
            'coy_id'       => $this->coy,
            'ref_type'     => $data['ref_type'],
            'ref_id'       => $data['ref_id'],
            'addr_type'    => (isset($data['addr_type'])) ? $data['addr_type'] : '0-PRIMARY',
            'addr_format'  => '1',
            'street_line1' => Str::substr(((isset($data['street_line1'])) ? $data['street_line1'] : ''), 0, 30),
            'street_line2' => Str::substr(((isset($data['street_line2'])) ? $data['street_line2'] : ''), 0, 30),
            'street_line3' => Str::substr(((isset($data['street_line3'])) ? $data['street_line3'] : ''), 0, 30),
            'street_line4' => Str::substr(((isset($data['street_line4'])) ? $data['street_line4'] : ''), 0, 30),
            'city_name'    => Str::substr(((isset($data['city_name'])) ? $data['city_name'] : ''), 0, 30),
            'state_name'   => Str::substr(((isset($data['state_name'])) ? $data['state_name'] : ''), 0, 30),
            'country_id'   => Str::substr(((isset($data['country_id'])) ? $data['country_id'] : 'SG'), 0, 30),
            'postal_code'  => Str::substr(((isset($data['postal_code'])) ? $data['postal_code'] : ''), 0, 30),
            'addr_text'    => Str::substr(((isset($data['addr_text'])) ? $data['addr_text'] : ''), 0, 300),
            'created_by'   => isset($data['staff_id']) ? $data['staff_id'] : '',
            'created_on'   => now(),
            'modified_by'  => isset($data['staff_id']) ? $data['staff_id'] : '',
            'modified_on'  => now(),
        ];
    }

    private function genImageFromBase64($field, $base64encoded) {

        $splited = explode(',', substr( $base64encoded , 5 ) , 2);
        $mime=$splited[0];
        $data=$splited[1];

        $filename = Str::lower($field);
        $mime_split_without_base64=explode(';', $mime,2);
        $mime_split=explode('/', $mime_split_without_base64[0],2);
        if(count($mime_split)==2) {
            $extension=$mime_split[1];
            if($extension=='jpeg')
                $extension='jpg';

            $filename .= '.'.$extension;
        }

        return [$filename, $data];
    }

}