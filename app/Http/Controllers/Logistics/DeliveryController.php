<?php

namespace App\Http\Controllers\Logistics;
use App\Http\Controllers\Controller;

use App\Support\Carbon;

class DeliveryController extends Controller
{

    public function unbook_slots(){
        $input = request()->only(['booking_id','invoice_id']);
        if (empty($input["booking_id"]) || empty($input["invoice_id"])) {
            return [
                "status_code" => 400,
                "message" => "Invalid parameters"
            ];
        }

        $booking = db()->table('o2o_delivery_time_slot_booking')->where("id",$input["booking_id"])->get();
        if ($booking) {
            if ($booking[0]->ref_id == $input["invoice_id"]) {
                db()->table('o2o_delivery_time_slot_booking')->where("id",$input["booking_id"])->delete();

                return [
                    "status_code" => 200,
                    "invoice_id" => $input["invoice_id"] ?? "",
                    "message" => "Slot Unreserved",
                ];
            }
        }

        return [
            "status_code" => 400,
            "message" => "No booking found"
        ];
    }

    public function book_slots($type,$code) {
        $input = request()->only(['slot_id','invoice_id']);
        if (empty($input["slot_id"]) || empty($input["invoice_id"])) {
            return [
                "status_code" => 400,
                "message" => "Invalid parameters"
            ];
        }

        $selected = ["slot_zone" => ''];

        $slots = $this->get_slots($type,$code);
        foreach($slots as $sDate) {
            foreach($sDate["time_slots"] as $sTime) {
                if ($sTime["slot_id"] == $input["slot_id"]) {

                    $selected = [
                        "delivery_date_id" => $sDate["delivery_id"],
                        "order_id" => 0,
                        "slot_zone" => $sDate["zone"],
                        "slot_date" => $sDate["date"],
                        "slot_start" => $sTime["from"],
                        "slot_end" => $sTime["to"],
                        "status_level" => 0,
                        "created_on" => date('Y-m-d H:i:s'),
                        "ref_id" => $input['invoice_id'] ?? ""
                    ];

                }
            }
        }

        if (empty($selected["slot_zone"])) {
            return [
                "status_code" => 400,
                "message" => "Invalid slot_id code"
            ];
        }

        // Save the slot into DB
        $booking_id = db()->table('o2o_delivery_time_slot_booking')->insertGetId($selected);
        return [
            "status_code" => 200,
            "invoice_id" => $selected["ref_id"] ?? "",
            "message" => "Slot Reserved",
            "data" => [
                "invoice_id" => $selected['ref_id'] ?? "",
                "booking_id" => $booking_id
            ]
        ];
    }

    public function get_slots($type,$code)
    {
        $type = $this->delivery_type($type);
        $db_date_list = $this->delivery_date_list($type);

        $first_day = date('Y-m-d', strtotime('+'.$db_date_list->start_days_limit.' days'));
        $count_days = $db_date_list->future_days_limit;

        $postal_zone = $this->delivery_zone($code);
        $booked_slots = $this->delivery_booked($first_day, 30);

        $open_dates = [];
        for($i=0;$i<$count_days;$i++){
            $date = date('Y-m-d', strtotime($first_day) + (86400 * $i));
            $day = strtolower(date('l',strtotime($date)));
            if (in_array($day,$db_date_list->shipping_days) && !in_array($date,$db_date_list->shipping_days_off) ) {

                $time_slots= [];
                $quote_limits = $db_date_list->quote_limits[$day];
                foreach($quote_limits['time_limits'] as $lm) {
                    $tFrom = date("H:i", strtotime($lm['from']));
                    $tTo = date("H:i", strtotime($lm['to']));
                    $booked_count_slot = $this->find_delivery_booked($booked_slots, $date, $postal_zone, $tFrom, $tTo);

                    $time_slots[] = [
                        "available" => ($lm['quote_limit'] - $booked_count_slot) > 0,
                        "slot_id" => md5($postal_zone.$date.$tFrom.$tTo),
                        "text" => date("g:i a", strtotime($lm['from'])) .' - '. date("g:i a", strtotime($lm['to'])),
                        "from" => $tFrom,
                        "to" => $tTo,
                        "cut_off_time" => date("H:i", strtotime($lm['cut_off_time'])),
                        "debug" => $postal_zone . " >> Limit: ".$lm['quote_limit']." / Booked: ".$booked_count_slot
                    ];
                }

                // Check if the zone is full for the day
                $booked_count_zone = $this->find_delivery_booked($booked_slots, $date, $postal_zone);
                $booked_count_date = $this->find_delivery_booked($booked_slots, $date);
                if ($quote_limits['daily_quotes'] <= $booked_count_date || $quote_limits['region_quotes'][strtolower($postal_zone)] <= $booked_count_zone) {
                    foreach($time_slots as $k=>$slot) {
                        $time_slots[$k]['available'] = false;
                    }
                }

                $open_dates[$date] = [
                    "delivery_id" => $db_date_list->id,
                    "zone" => $postal_zone,
                    "text" => date('d M, D', strtotime($date)),
                    "date" => $date,
                    "day" => $day,
                    "time_slots" => $time_slots
                ];
            }
            else {
                // Skip this date
                $count_days++;
            }
        }


        return $open_dates;
    }

    private function delivery_type($type){

        if ($type=='express'){
            $type = 2;
        }
        else {
            $type = 1;
        }

        return $type;
    }

    private function delivery_date_list($id=1){

        $db_date_list = db()->table('o2o_delivery_date_list')->find($id);

        $db_date_list->shipping_days = explode(',',$db_date_list->shipping_days);
        $db_date_list->quote_limits = json_decode($db_date_list->quote_limits, true);

        $db_shipping_days_off = json_decode($db_date_list->shipping_days_off);
        foreach($db_shipping_days_off as $dd){
            if ($dd->type == "SINGLE") {
                $shipping_days_off[] = $dd->options->date;
            }
        }

        $db_date_list->shipping_days_off = $shipping_days_off;

        return $db_date_list;
    }

    private function delivery_zone($postal){
        $sql = "SELECT * FROM v_chl_postal_driver where postal = ?";
        $result = db('pg_cherps')->select($sql,[$postal]);
        if ($result) {
            $driver = explode('/',$result[0]->driver);
            return 'ZONE_' . $driver[0];
        }

        return 'ZONE_C1';
    }

    private function delivery_booked($first_date,$count_days=30,$slot_zone='') {
        $sql_date = "AND slot_date between '" . date('Y-m-d', strtotime($first_date)) . "' and '" . date('Y-m-d', strtotime($first_date) + 86400*$count_days) ."'";
        $sql_zone = ($slot_zone=='') ? "" : "AND slot_zone='$slot_zone' ";
        $sql = "select slot_date,left(slot_start::text,5) slot_start,left(slot_end::text,5) slot_end,slot_zone,count(*) booked from o2o_delivery_time_slot_booking
                    where status_level>=0 $sql_date $sql_zone
                group by slot_date,slot_start,slot_end,slot_zone";
        return db()->select($sql);
    }

    private function find_delivery_booked($booked_slots, $date, $zone='', $start='', $end=''){
        $book_count = 0;
        foreach($booked_slots as $booked){
            if ($start!='' && $end != '') {
                // Check by date/zone/time
                if ($booked->slot_zone == $zone && $booked->slot_date == $date && $booked->slot_start == $start && $booked->slot_end == $end)
                    $book_count += $booked->booked;
            }
            else if ($zone!='') {
                // Check by date/zone
                if ($booked->slot_zone == $zone && $booked->slot_date == $date)
                    $book_count += $booked->booked;
            }
            else {
                // Check by date
                if ($booked->slot_date == $date)
                    $book_count += $booked->booked;
            }
        }
        return $book_count;
    }
}