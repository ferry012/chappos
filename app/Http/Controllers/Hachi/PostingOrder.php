<?php

namespace App\Http\Controllers\Hachi;


use \App\Http\Controllers\Controller;

class PostingOrder extends Controller
{
    public function __invoke($id)
    {
        $invoiceHead = (array)db('sql')->table('o2o_invoice_list')->where('coy_id', 'CTL')->where('invoice_id', $id)->where('status_level', 1)->first();

        if ($invoiceHead) {

            if (db('pg_cherps')->table('sms_invoice_list')->where('coy_id', 'CTL')->where('invoice_id', $id)->exists()) {
                return $this->respondWithSuccess('The order is already exists');
            }

            $invoiceLines = db('sql')->table('o2o_invoice_item')->where('coy_id', 'CTL')->where('invoice_id', $id)->get();
            $payments     = db('sql')->table('o2o_payment_list')->where('coy_id', 'CTL')->where('invoice_id', $id)->get();
            $addresses    = db('sql')->table('coy_address_book')->where('coy_id', 'CTL')->where('ref_type', 'CASH_INV')->where('ref_id', $id)->get();
            $urlKey       = (array)db('sql')->table('o2o_url_key')->where('ref_id', $id)->first();

            db('pg_cherps')->transaction(static function () use ($invoiceHead, $invoiceLines, $payments, $addresses, $urlKey) {
                db('pg_cherps')->table('sms_invoice_list')->insert($invoiceHead);
                db('pg_cherps')->table('sms_invoice_item')->insert(obj_to_array($invoiceLines, static function ($line) {
                    //unset($line->pick_date);

                    return $line;
                }));
                db('pg_cherps')->table('sms_payment_list')->insert(obj_to_array($payments));
                db('pg_cherps')->table('coy_address_book')->insert(obj_to_array($addresses));
                db('pg_cherps')->table('o2o_url_key')->insert($urlKey);
            }, 3);

            db('sql')->table('o2o_invoice_list')->where('invoice_id', $id)->update(['status_level' => 2]);

            return $this->respondWithSuccess('The order is posted');
        }

        return $this->errorNotFound('The Order does not exits.');

    }

}