<?php

namespace App\Http\Controllers\Hachi;


use App\Core\Orders\Models\Order;
use App\Core\Orders\Services\CreateHachiMissingInvoice;
use App\Core\Payments\Models\Transaction;
use App\Http\Controllers\Controller;
use App\Support\Str;
use Flugg\Responder\Responder;
use Illuminate\Http\Request;

class MissingOrderTransactionRefund extends Controller
{
    public function __invoke(Request $request, Responder $responder)
    {
        $orderId = $request->get('orderId');
        $invoiceId = $request->get('invoiceId');
        $by = $request->get('by', 'Hachi');
        $payMode = $request->get('payMode');
        $payTransId = $request->get('payTransId');
        $payRefId = $request->get('payRefId');
        $payAmount = $request->get('payAmount');

        $order = Order::where('order_num', $orderId)->first();

        if ($order === null) {
            return $responder->error('order_not_found', 'Order not found');
        }

        $transaction['trans_id'] = $payTransId;
        $transaction['pay_mode'] = $payMode;
        $transaction['trans_amount'] = $payAmount;
        $transaction['trans_ref_id'] = $payRefId;
        $transaction['is_success'] = true;
        $transaction['status_level'] = 1;
        $transaction['created_by'] = $by;
        $transaction['created_on'] = now();
        $transaction['modified_by'] = $by;
        $transaction['modified_on'] = now();

        if ((new CreateHachiMissingInvoice)->create($order, $transaction)) {

            if (empty($order->customer_id)) {
                return $responder->success();
            }

            $data = [
                'loc_id'        => 'HCL',
                'pos_id'        => '',
                'invoice_num'   => $invoiceId,
                'trans_num'     => $invoiceId,
                'trans_type'    => 'HI',
                'trans_date'    => now()->toDatetimeString(),
                'customer_info' => [
                    'id' => $order->customer_id,
                ],
                'items'         => [
                    [
                        'line_num'      => 1,
                        'item_id'       => '#HI-MISSING',
                        'item_desc'     => 'Transaction missing',
                        'qty_ordered'   => 1,
                        'unit_price'    => $payAmount,
                        'unit_discount' => '0.00',
                        'regular_price' => $payAmount,
                        'total_price'   => $payAmount,
                        'trans_point'   => 0,
                        'mbr_savings'   => 0,
                        'item_message'  => '',
                    ],
                ],
                'payments'      => [
                    [
                        'pay_mode'     => 'CARD',
                        'info'         => '',
                        'trans_amount' => '0'
                    ]
                ],
            ];

            $response = app('api')->members()->sendTransaction($invoiceId, $data);

            if ($response && ($response->status_code === 200 || $response->status_code === 404)) {
                return $responder->success();
            }
        }

        return $responder->error('', 'Failed to create. Please try again.');
    }
}