<?php

namespace App\Http\Controllers\Hachi;


use Exception;
use App\Http\Controllers\Controller;
use App\Support\Str;
use Illuminate\Support\Collection;

class OrderHandleStock extends Controller
{
    public $products;
    public $stockLog;
    public $defaultDelivery;

    public function __construct()
    {
        $this->defaultDelivery = 'NRW';
        $this->stockLog        = collect();
    }

    function __invoke($id, $action)
    {
        app('debugbar')->disable();
        $res = 0;

        if ($action === 'hold') {
            $res = $this->hold($id);
        } elseif ($action === 'release') {
            $res = $this->release($id);
        }

        return response($res);
    }

    public function release($id)
    {
        $stocks = db('sql')->table('o2o_transaction_stock_log')->where('trans_id', $id)->get();

        try {

            db('pg_cherps')->beginTransaction();
            db('sql')->beginTransaction();

            foreach ($stocks as $stock) {
                $stock->status_level = (int)$stock->status_level;

                if ($stock->status_level === 1) {

                    if (! empty($stock->inv_ref_id)) {
                        $stock->remarks .= $this->log('Start to unallocated');

                        list($locId, $lineNum, $refId) = explode('/', $stock->inv_ref_id);

                        $query = db('pg_cherps')->table('ims_inv_physical')->where('coy_id', 'CTL')->where('item_id', $stock->item_id)
                            ->where('loc_id', $locId)->where('line_num', $lineNum)->where('ref_id', $refId);

                        if ($result = $query->first()) {
                            $stock->remarks .= $this->log('Original bin found');
                            if ($result->qty_reserved > 0 && $result->qty_reserved >= $stock->qty_allocated) {
                                $result->qty_reserved -= $stock->qty_allocated;
                                $result->modified_on  = now();
                                $result->modified_by  = 'hachi';
                                $query->update((array)$result);

                                $stock->qty_unallocated = $stock->qty_allocated;
                                $stock->remarks         .= $this->log('Unallocated inventory successes');
                                $stock->status_level    = -1;
                            } else {
                                $stock->remarks .= $this->log('Not enough unallocated inventory from original bin, looking for other bin');

                                OTHERS_BIN_DEDUCTION:
                                $stocks1 = db('pg_cherps')->table('ims_inv_physical')->where('coy_id', 'CTL')->where('item_id', $stock->item_id)
                                    ->where('loc_id', $stock->inv_loc)->where('qty_reserved', '>', 0)
                                    ->orderBy('initial_date')->get();

                                $qtyToRelease = $stock->qty_allocated - $stock->qty_unallocated;

                                if ($stocks1->isEmpty()) {
                                    $this->log('No bin found');
                                }

                                foreach ($stocks1 as $stock1) {

                                    if ($qtyToRelease <= 0) {
                                        break;
                                    }

                                    if ($stock1->qty_reserved <= 0) {
                                        continue;
                                    }

                                    if ($qtyToRelease <= $stock1->qty_reserved) {
                                        $physicalQtyRelease = $qtyToRelease;
                                        $qtyToRelease       = 0;
                                    } else {
                                        $physicalQtyRelease = (int)$stock1->qty_reserved;
                                        $qtyToRelease       -= $physicalQtyRelease;
                                    }

                                    if ($physicalQtyRelease > 0) {
                                        $stock1->qty_reserved -= $physicalQtyRelease;
                                    }

                                    $refId                  = $this->buildStockRefId($stock1);
                                    $stock->qty_unallocated += $physicalQtyRelease;
                                    $stock->remarks         .= $this->log("Unallocated {$physicalQtyRelease} qty from bin {$refId}");

                                    db('pg_cherps')->table('ims_inv_physical')->where('coy_id', 'CTL')->where('item_id', $stock->item_id)
                                        ->where('loc_id', $locId)->where('line_num', $stock1->line_num)->where('ref_id', $stock1->ref_id)
                                        ->update((array)$stock1);
                                }

                                if ($qtyToRelease > 0) {
                                    $stock->remarks .= $this->log("Unable to unallocated {$qtyToRelease} qty");
                                } else {
                                    $stock->remarks      .= $this->log('Unallocated inventory successes');
                                    $stock->status_level = -1;
                                }

                            }
                        } else {
                            $stock->remarks .= $this->log('Original bin not found, looking for other bin');
                            goto OTHERS_BIN_DEDUCTION;
                        }


                    } else {
                        $stock->status_level = -1;
                    }

                    $stock->modified_on = now();
                    $stock->modified_by = 'hachi';

                    db('sql')->table('o2o_transaction_stock_log')->where('id', $stock->id)->update(array_except((array)$stock, ['id']));
                }
            }

        } catch (Exception $e) {

            db('pg_cherps')->rollBack();
            db('sql')->rollBack();

            return 0;
        }

        db('pg_cherps')->commit();
        db('sql')->commit();

        return 1;
    }

    public function log($message)
    {
        return sprintf("%s - %s\n", $message, now());
    }

    public function hold($id)
    {
        $this->defaultDelivery = 'NRW';

        $tableSource = request('tbl_src', 'cart');
        $table       = 'o2o_cart_pending_items';
        $transField  = 'transaction_id';

        if ($tableSource === 'order') {
            $table      = 'o2o_transaction_items';
            $transField = 'trans_id';
        }

        if (db('sql')->table('o2o_transaction_stock_log')->where('trans_id', $id)->where('status_level', 1)->count()) {
            return -1;
        }

        $items = db('sql')->table($table)->selectRaw(
            "{$transField} as trans_id,
                        item_id,
                        item_type,
						(select top 1 in_store_id from o2o_cart_pending_list where transaction_id = ?) as in_store_id,
						line_num,
						qty as qty_ordered,
						is_pwp_item,has_pwp_item,pwp_group_id,
						store_collection,
						 delivery_method, 
						loc_id,
						'' as delv_mode_id,
						case when isnull(loc_id,'')<>'' then loc_id else '' end as inv_loc,
						status_level,
						getdate() as created_on,
						'Admin' as created_by,
						getdate() modified_on,
						'Admin' as modified_by", [$id])
            ->where($transField, $id)->get()->each(function ($item) {
                $item->line_num         = (int)$item->line_num;
                $item->item_id          = trim($item->item_id);
                $item->is_pwp_item      = (int)$item->is_pwp_item;
                $item->has_pwp_item     = (int)$item->has_pwp_item;
                $item->store_collection = trim($item->store_collection);
                $item->status_level     = 0;
            });

        $this->products = db('sql')->table('ims_item_list')->where('coy_id', 'CTL')->whereIn('item_id', $items->pluck('item_id'))->get()->each(function ($item) {
            $item->item_id  = trim($item->item_id);
            $item->inv_type = trim($item->inv_type);
        });

        foreach ($items as $item) {
            $product = $this->products->where('item_id', $item->item_id)->first();
            $qty     = $item->qty_ordered;

            if ($product) {
                // Google card need to duplicate line by n qty
                if ($product->item_type === 'G' && $qty > 1) {
                    $item->qty_ordered = 1;
                    for ($i = 1; $i < $qty;) {
                        $items->push(clone $item);
                        $i++;
                    }
                }
            }
        }

        $items = $items->each(function ($item) use ($items) {

            $product = $this->products->where('item_id', $item->item_id)->first();

            $item->inv_type = trim($product->inv_type);

            if ($item->is_pwp_item === 1 && $item->has_pwp_item === 0) {
                $item1 = $items->where('pwp_group_id', $item->pwp_group_id)->where('is_pwp_item', 0)->where('has_pwp_item', 1)->first();

                if ($item1) {
                    $item->parent_id = (int)$item1->line_num;
                }
            } else {
                $item->parent_id = 0;
            }

            if ($item->delivery_method === 'STD') {
                $item->delv_mode_id = 'NJA';

                if ($item->loc_id === 'PRE07') {
                    $item->delv_mode_id = $this->defaultDelivery;
                } else {
                    $item->loc_id = 'HCL';
                }
            }

            if ($item->store_collection !== '') {
                $item->loc_id = $item->store_collection;
            }

            if ($item->loc_id === 'AUC') {

                if ($item->store_collection === '') {

                    $locId      = 'HCL';
                    $delvModeId = $this->defaultDelivery;
                } else {
                    $locId      = $item->store_collection;
                    $delvModeId = 'SCL';
                }

                $item->status_level = 1;
            }

            if ($item->loc_id === 'LAC') {
                if ($item->store_collection === '') {
                    $locId      = 'HCL';
                    $delvModeId = $this->defaultDelivery;
                } else {
                    $locId      = $item->store_collection;
                    $delvModeId = 'ISC';
                }

                $item->status_level = 1;
            }

            if ($item->inv_type === 'HSZ') {
                $delvModeId         = 'HSZ';
                $invLocId           = 'HSZ';
                $locId              = 'HSZ';
                $item->status_level = 1;
            }

            if ($item->inv_type === 'HSZ-C') {
                $delvModeId         = 'HSZ';
                $invLocId           = 'HSZ-C';
                $locId              = 'HSZ';
                $item->status_level = 1;
            }

            if ($item->delivery_method === 'SDD') {
                $item->inv_loc      = 'BF';
                $item->loc_id       = 'BF';
                $item->delv_mode_id = 'SPP';
            }

            if ($item->loc_id === 'PREBB' && $item->store_collection === '') {
                $locId              = 'HCL';
                $delvModeId         = $this->defaultDelivery;
                $item->status_level = 1;
            }

            if ($item->store_collection !== '' && $item->delivery_method === 'collection') {

                $item->inv_loc      = $item->inv_loc === 'AUC' ? $item->inv_loc : $item->store_collection;
                $item->loc_id       = $item->store_collection;
                $item->delv_mode_id = 'SCL';

                if (! empty($item->in_store_id) && $item->store_collection === $item->in_store_id) {
                    $item->delv_mode_id = 'ISC';
                }
            }

            // etc membership card / google card / gift card
            if (in_array(trim($product->item_type), ['M', 'G', 'E', 'W', 'T'], true)) {
                $locId      = '';
                $delvModeId = '';
                $invLocId   = '';

                if ($item->item_id === 'SSEW' && Str::endsWith($item['inv_loc'], '-C', false)) {
                    $invLocId = 'HCL';
                }

                $item->status_level = 1;
            }

            if ($item->status_level === 1) {
                $this->stockLog->push([
                    'parent_ref_num' => $item->parent_id,
                    'ref_num'        => $item->line_num,
                    'item_id'        => $item->item_id,
                    'inv_ref_id'     => '',
                    'qty_allocated'  => $item->qty_ordered,
                    'inv_loc'        => $invLocId,
                    'loc_id'         => $locId,
                    'delv_method'    => $item->delivery_method,
                    'delv_mode_id'   => $delvModeId,
                    'remarks'        => null,
                ]);
            }
        })->reject(function ($item) {
            return $item->status_level === 1;
        });

        $items = $items->each(function ($item) {

            if ($item->delivery_method === 'collection') {
                // BF is handling the express delivery items
                $stocks = db('pg_cherps')->table('ims_inv_physical')
                    ->where('coy_id', 'CTL')
                    ->where('loc_id', $item->loc_id)
                    ->where('item_id', $item->item_id)
                    ->get();

                $this->allocateStock($item, $stocks);
            }

        })->reject(function ($item) {
            return $item->status_level === 1;
        });

        $items = $items->each(function ($item) {

            if ($item->delivery_method === 'SDD') {
                // BF is handling the express delivery items
                $stocks = db('pg_cherps')->table('ims_inv_physical')
                    ->where('coy_id', 'CTL')
                    ->where('loc_id', 'BF')
                    ->where('item_id', $item->item_id)
                    ->get();

                $this->allocateStock($item, $stocks);
            }

        })->reject(function ($item) {
            return $item->status_level === 1;
        });

        if ($items->isNotEmpty()) {
            foreach (['VEN-C', 'HCL', 'BF', 'PRE00'] as $location) {
                $stocks = db('pg_cherps')->table('ims_inv_physical')
                    ->where('coy_id', 'CTL')
                    ->where('loc_id', $location)
                    ->whereIn('item_id', $items->pluck('item_id'))
                    ->get();

                $items = $items->each(function ($item) use ($stocks) {
                    if ($this->allocateStock($item, $stocks)) {
                        return true;
                    }
                })->reject(function ($item) {
                    return $item->status_level === 1;
                });
            }
        }

        $enableOneLocation = $this->products->whereIn('inv_type', [
                'BL-C', 'BRO-C', 'DSH-C', 'EA-C', 'HSZ-C', 'IM-C', 'IN-C', 'SL-C', 'TH-C', 'UP-C', 'VEN-C',
            ])->count() === 0;

        $fulfilledByOneLocation = false;

        if ($enableOneLocation) {

            $locations = [
                'IM-C', 'HCL', 'BF', 'NEX', 'CP', 'JEM', 'JP', 'TP', 'VC', 'PRE07', 'PRE06', 'PRE05', 'PRE04', 'PRE03',
                'PRE02', 'PRE01',
            ];

            $stockRows = db('pg_cherps')->table('ims_inv_physical')
                ->where('coy_id', 'CTL')
                ->whereIn('loc_id', $locations)
                ->whereIn('item_id', $items->pluck('item_id'))->get()->map(static function ($item) {
                    $item->loc_id  = trim($item->loc_id);

                    return $item;
                });

            foreach ($locations as $location) {

                $stocks = $stockRows->where('loc_id', $location);

                if ($stocks->isEmpty()) {
                    continue;
                }

                // Reset data
                $allocatedStockItems    = collect();
                $fulfilledByOneLocation = true;
                $items1                 = $items->map(function ($item) {
                    return clone $item;
                });

                foreach ($items1 as $item) {

                    $data = $this->allocateStock($item, $stocks, ['return' => true, 'one_location' => true]);

                    if (is_bool($data) && ! $data) {
                        $fulfilledByOneLocation = false;
                        break;
                    }

                    if ($data && $data instanceof Collection) {
                        $allocatedStockItems = $allocatedStockItems->concat($data);
                    } else {
                        $fulfilledByOneLocation = false;
                        break;
                    }

                }

                if ($fulfilledByOneLocation) {
                    break;
                }

            }

            if ($fulfilledByOneLocation) {
                $items          = $items1->reject(function ($item) {
                    return $item->status_level === 1;
                });
                $this->stockLog = $this->stockLog->concat($allocatedStockItems);
            }

        }

        if (! $fulfilledByOneLocation && $items->isNotEmpty()) {
            // Fulfilled by multiple store
            $items->each(function ($item) {
                $product = $this->products->where('item_id', $item->item_id)->first();

                $query = db('pg_cherps')->table('ims_inv_physical')
                    ->where('coy_id', 'CTL')
                    ->where('item_id', $item->item_id)
                    ->whereRaw('qty_on_hand - qty_reserved > 0');

                if ($product && $product->inv_type === 'LCH') {
                    $stocks = $query->whereIn('loc_id', ['HCL', 'PRE00'])->get();
                } else {
                    $stocks = $query->whereIn('loc_id', [
                        'BF', 'IM-C', 'HSG', 'VC', 'NEX', 'CP', 'JEM', 'JP', 'TP', 'PRE01', 'PRE02', 'PRE03',
                        'PRE04', 'PRE05', 'PRE06', 'PRE07', 'DSH-C', 'EA-C', 'HSZ', 'HSZ-C', 'IN-C', 'SL-C', 'TH-C',
                        'UP-C',
                        'BL-C', 'BRO-C',
                    ])->get();

                    // set location priority
                    $stocks->each(function ($stock) {
                        $locId = trim($stock->loc_id);

                        if (in_array($locId, [
                            'BL-C', 'BRO-C', 'DSH-C', 'EA-C', 'HSZ-C', 'IM-C', 'IN-C', 'SL-C', 'TH-C', 'UP-C', 'VEN-C',
                        ], true)) {
                            $stock->priority = 9999;
                        } elseif ($locId === 'HCL') {
                            $stock->priority = 9998;
                        } elseif ($locId === 'HSG') {
                            $stock->priority = 1;
                        } else {
                            $stock->priority = $stock->qty_on_hand - $stock->qty_reserved;
                        }

                    });
                }

                $this->allocateStock($item, $stocks, ['priority' => true, 'add_unallocated_record' => true]);

            })->reject(function ($item) {
                return $item->status_level === 1;
            });
        }

        // final overwrite
        $this->stockLog = $this->stockLog->map(function ($item) {

            $product = $this->products->where('item_id', $item['item_id'])->first();

            if ($product->item_type === 'T') {
                $item['inv_loc']      = '';
                $item['loc_id']       = '';
                $item['delv_mode_id'] = '';
            }

            // For collection at HCL only
            if ($item['delv_mode_id'] === '_SCL') {
                $item['delv_mode_id'] = 'SCL';
            }

            if ($item['loc_id'] === 'VEN-C') {
                $item['loc_id'] = 'HCL';
            }

            if ($item['delv_mode_id'] === 'SCL' && in_array($item['loc_id'], ['HCL', 'BF'])) {
                $item['delv_mode_id'] = 'SCL';
            }

            if ($item['delv_mode_id'] === 'SCL' && Str::startsWith($item['inv_loc'], 'PRE')) {
                $suppId         = optional(db('sql')->table('o2o_live_outlet')->where('loc_id', $item['inv_loc'])->first(['supp_id']))->supp_id;
                $item['loc_id'] = $suppId ?? $item['loc_id'];
            }

            if ($item['delv_mode_id'] === 'ISC' && $item['inv_loc'] === 'PRE00') {
                $item['loc_id'] = 'HCL';
            }

            // standard only handle by Hachi
            if ($item['delv_mode_id'] === 'STD' && $item['inv_loc'] !== 'PRE00' && Str::startsWith($item['inv_loc'], 'PRE')) {
                $item['loc_id'] = 'HCL';
            }

            if ($item['inv_loc'] === 'DSH-C') {
                $item['loc_id']       = 'DSH';
                $item['delv_mode_id'] = 'DSH';
            }

            if ($item['inv_loc'] === 'IM-C') {
                $item['loc_id']       = 'HCL';
                $item['delv_mode_id'] = $this->defaultDelivery;
            }

            // SSEW to be always set to inv_loc = 'HCL' if the main item is 'VEN-C'
            if ($item['item_id'] === 'SSEW' && $item['parent_ref_num'] > 0) {
                $parentItem = $this->stockLog->where('ref_num', $item['parent_ref_num'])->first();

                if ($parentItem) {
                    $product1 = $this->products->where('item_id', $parentItem['item_id'])->first();

                    if ($product1 && $product1->inv_type === 'VEN-C') {
                        $item['inv_loc'] = 'HCL';
                    }
                }
            }

            return $item;
        });

        $SSEW_items = array_map('trim', db('sql')->table('o2o_temp_ssew')->pluck('promo_item_id')->toArray());

        $this->stockLog = $this->stockLog->map(function ($item) use ($SSEW_items) {
            if (in_array($item['item_id'], $SSEW_items, false)) {
                $item['inv_loc'] = 'HCL';
            }

            return $item;
        });

        $insData = $this->stockLog
            ->map(function ($item) {
                $item['ref_num'] = (int)$item['ref_num'];

                return $item;
            })
            ->sortBy('ref_num')
            ->values()
            ->map(function ($item, $index) use ($id) {
                $item['trans_id']      = $id;
                $item['line_num']      = $index + 1;
                $item['qty_allocated'] = (int)$item['qty_allocated'];
                $item['status_level']  = 1;

                return $item;
            })->toArray();

        try {

            db('pg_cherps')->beginTransaction();

            // reduce on hand or reserved stock
            $this->stockLog = $this->stockLog->each(function ($stock) {

                if (! empty($stock['inv_ref_id'])) {
                    list($locId, $lineNum, $refId) = explode('/', $stock['inv_ref_id']);

                    db('pg_cherps')->table('ims_inv_physical')
                        ->where('coy_id', 'CTL')
                        ->where('item_id', $stock['item_id'])
                        ->where('loc_id', $locId)
                        ->where('line_num', $lineNum)
                        ->where('ref_id', $refId)
                        ->update([
                            'qty_reserved' => db('pg_cherps')->raw(' qty_reserved + '.$stock['qty_allocated']),
                            'modified_on'  => now(),
                            'modified_by'  => 'hachi',
                        ]);
                }

            })->groupBy(function ($item) {
                return $item['ref_num'].$item['inv_loc'].$item['loc_id'];
            })->map(function ($items) {

                if ($items->count() > 1) {

                    $item = $items->first();

                    $item['qty_allocated'] = $items->sum('qty_allocated');

                    return $item;
                }

                return $items->first();
            })->sortBy('ref_num')->values()->map(function ($item, $index) use ($id) {
                $item['trans_id']      = $id;
                $item['line_num']      = $index + 1;
                $item['qty_allocated'] = (int)$item['qty_allocated'];

                return array_only($item, [
                    'trans_id', 'line_num', 'ref_num', 'parent_ref_num', 'item_id', 'qty_allocated', 'inv_loc',
                    'loc_id', 'delv_method', 'delv_mode_id',
                ]);
            });

            db('sql')->beginTransaction();
            db('sql')->table('o2o_transaction_stock')->where('trans_id', $id)->delete();
            db('sql')->table('o2o_transaction_stock')->insert(obj_to_array($this->stockLog->toArray()));
            db('sql')->table('o2o_transaction_stock_log')->insert($insData);

        } catch (Exception $e) {
            db('pg_cherps')->rollBack();
            db('sql')->rollBack();

            return 0;
        }

        db('pg_cherps')->commit();
        db('sql')->commit();

        return 1;
    }

    public function allocateStock($item, $stocks, $options = [])
    {
        $return               = array_get($options, 'return', false);
        $priority             = array_get($options, 'priority', false);
        $addUnallocatedRecord = array_get($options, 'add_unallocated_record', false);
        $oneLocation          = array_get($options, 'one_location', false);
        $orgQtyOrdered        = $item->qty_ordered;
        $qtyOrdered           = $item->qty_ordered;
        $return_data          = collect();

        $stocks = $stocks->filter(function ($stock) {
            $stock->item_id = trim($stock->item_id);
            $stock->loc_id  = trim($stock->loc_id);
            $qtyBalance     = $stock->qty_on_hand - $stock->qty_reserved;
            // Deduct current stock
            $qtyBalance         -= $this->stockLog->where('inv_loc', $stock->loc_id)->where('item_id', $stock->item_id)->sum('qty_allocated');
            $stock->qty_balance = $qtyBalance;

            return $qtyBalance > 0;
        })
            ->where('item_id', $item->item_id)
            ->sortBy('line_num')
            ->when($priority, function ($collection) {
                // Location priority and group by location to do a line ascending sort to deduct from the early stock
                return $collection->sortByDesc('priority')->mapToGroups(function ($item) {

                    return [$item->loc_id => $item];
                })->map(function ($collection) {
                    // deduct from the early stock line number
                    return $collection->sortBy('line_num');
                })->flatten(1)->values();
            });

        foreach ($stocks as $stock) {

            if (0 >= $qtyOrdered) {
                $item->status_level = 1;
                break;
            }

            $refId = $this->buildStockRefId($stock);

            if ($stock->qty_balance >= $qtyOrdered) {
                $qtyAllocated = $qtyOrdered;
                $qtyOrdered   = 0;
            } else {
                $qtyAllocated = $stock->qty_balance;
                $qtyOrdered   -= $qtyAllocated;
            }

            $stock->qty_balance -= $qtyAllocated;
            $item->qty_ordered  -= $qtyAllocated;

            $data = [
                'parent_ref_num' => $item->parent_id,
                'ref_num'        => $item->line_num,
                'item_id'        => $item->item_id,
                'inv_ref_id'     => $refId,
                'qty_allocated'  => $qtyAllocated,
                'inv_loc'        => $stock->loc_id,
                'loc_id'         => $item->loc_id,
                'delv_mode_id'   => $item->delv_mode_id,
                'delv_method'    => $item->delivery_method,
                'remarks'        => null,
            ];

            if ($return) {
                $return_data->push($data);
            } else {
                $this->stockLog->push($data);
            }
        }

        if ($oneLocation && 0 < $qtyOrdered) {
            $item->qty_ordered = $orgQtyOrdered;

            return false;
        }

        // unable to allocate stock will be handled by HCL
        if ($addUnallocatedRecord && (0 < $qtyOrdered || $stocks->isEmpty())) {

            $item->status_level = 1;
            $remarks            = 'unable to allocate stock.';

            $data = [
                'parent_ref_num' => $item->parent_id,
                'ref_num'        => $item->line_num,
                'item_id'        => $item->item_id,
                'inv_ref_id'     => '',
                'qty_allocated'  => $qtyOrdered,
                'inv_loc'        => 'HCL',
                'loc_id'         => 'HCL',
                'delv_mode_id'   => $item->delv_mode_id,
                'delv_method'    => $item->delivery_method,
                'remarks'        => $remarks,
            ];

            if ($return) {
                $return_data->push($data);
            } else {
                $this->stockLog->push($data);
            }

        }

        if ($return) {
            return $return_data;
        }

    }

    public function buildStockRefId($stock)
    {
        // build ref id string
        $refId = implode('/', array_only((array)$stock, ['loc_id', 'line_num', 'ref_id']));
        $refId = str_replace(' ', '', $refId);

        return $refId;
    }
}