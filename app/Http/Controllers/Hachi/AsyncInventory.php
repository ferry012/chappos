<?php

namespace App\Http\Controllers\Hachi;

use \App\Http\Controllers\Controller;
use App\Utils\Request;

class AsyncInventory extends Controller
{

    public function __invoke(Request $request, $id, $action)
    {
        app('debugbar')->disable();

        // ms_cherps - current
        // pg_cherps - new

        db('ms_cherps')->beginTransaction();
        db('pg_cherps')->beginTransaction();

        try {

            if ($action === 'reserved') {
                $this->asyncReserved($id);
            } else if ($action === 'unreserved') {
                $this->asyncUnreserved($id);
            }

            db('ms_cherps')->commit();
            db('pg_cherps')->commit();

            echo 'success';
        } catch
        (\Exception $e) {
            db('ms_cherps')->rollback();
            db('pg_cherps')->rollback();
            //throw $e;
            echo 'failed';
        }

    }

    public function asyncReserved($id)
    {

        $invItems = db('ms_cherps')->table('o2o_trans_inv_item')
            ->select(['item_id', 'line_num', 'ref_id', 'loc_id', 'qty_reserved'])
            ->where('trans_id', $id)
            ->where('status_level', 0)
            ->where('qty_reserved', '>', 0)->get();

        // old update to new
        foreach ($invItems as $invItem) {

            if ($invItem->line_num === null || $invItem->qty_reserved === null || $invItem->ref_id === null) {
                continue;
            }

            // where need to be trim
            db('pg_cherps')->table('ims_inv_physical')
                ->where('coy_id', 'CTL')
                ->whereRaw('TRIM(item_id) = ?', trim($invItem->item_id))
                ->whereRaw('TRIM(loc_id) = ?', trim($invItem->loc_id))
                ->whereRaw('TRIM(ref_id) = ?', trim($invItem->ref_id))
                ->where('line_num', $invItem->line_num)
                ->update([
                    'qty_reserved' => db('pg_cherps')->raw('qty_reserved + '.$invItem->qty_reserved),
                    'modified_on'  => now(),
                    'modified_by'  => 'pos',
                ]);

        }

        db('ms_cherps')->table('o2o_trans_inv_item')->where('trans_id', $id)->update(['status_level' => 1]);

    }

    public function asyncUnreserved($id)
    {

        $invItems = db('ms_cherps')->table('o2o_cart_pending_imsinvphysical_reverse')
            ->select(['item_id', 'line_num', 'ref_id', 'loc_id', 'qty_reverse'])
            ->where('transaction_id', $id)
            ->where('qty_reverse', '>', 0)
            ->where('status_level', 0)->get();

        // old update to new
        foreach ($invItems as $invItem) {

            if ($invItem->line_num === null || $invItem->qty_reverse === null || $invItem->ref_id === null) {
                continue;
            }

            // where need to be trim
            db('pg_cherps')->table('ims_inv_physical')
                ->where('coy_id', 'CTL')
                ->whereRaw('TRIM(item_id) = ?', trim($invItem->item_id))
                ->whereRaw('TRIM(loc_id) = ?', trim($invItem->loc_id))
                ->whereRaw('TRIM(ref_id) = ?', trim($invItem->ref_id))
                ->where('line_num', $invItem->line_num)
                ->update([
                    'qty_reserved' => db('pg_cherps')->raw('qty_reserved - '.$invItem->qty_reverse),
                    'modified_on'  => now(),
                    'modified_by'  => 'pos',
                ]);

        }

        db('ms_cherps')->table('o2o_cart_pending_imsinvphysical_reverse')->where('transaction_id', $id)->update(['status_level' => 1]);

    }
}