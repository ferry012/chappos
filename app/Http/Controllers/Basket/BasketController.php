<?php

namespace App\Http\Controllers\Basket;

use App\Core\Baskets\BasketRequestCriteria;
use App\Core\Baskets\Factories\BasketFactory;
use App\Core\Baskets\Models\Basket;
use App\Core\Orders\Models\Order;
use App\Http\Controllers\Controller;
use App\Http\Requests\Baskets\ApplyRebatesRequest;
use App\Http\Requests\Baskets\CancelRebatesRequest;
use App\Http\Resources\Baskets\BasketCollection;
use App\Http\Resources\Baskets\BasketResource;
use App\Support\Str;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class BasketController extends Controller
{

    /**
     * @var BasketFactory
     */
    protected $factory;

    public function __construct(BasketFactory $factory)
    {
        $this->factory = $factory;
    }

    public function index(BasketRequestCriteria $criteria)
    {
        $baskets = $criteria->getBuilder()->where('item_count', '>', 0)->paginate();

        return new BasketCollection($baskets);
    }

    public function recall(Request $request)
    {

        $perPage = $request->get('per_page', 20);

        if ($perPage >= 100) {
            $perPage = 100;
        }

        $baskets = Basket::with('activeOrder')
            ->where('coy_id', $request->get('coy_id'))
            ->where('cart_name', $request->get('basket_name'))
            ->where('loc_id', Str::upper($request->get('loc_id')))
            ->where('item_count', '>', 0)
            ->orderBy('created_on', 'DESC')
            ->paginate($perPage);

        return new BasketCollection($baskets);
    }

    public function showRecallDetail($id = null)
    {

        $order = Order::when(is_int($id), function ($query) use ($id) {
            $query->where('cart_id', $id);
        })->where('order_num', $id)->first();

        if ($order && $order->isPendingOrder()) {
            try {
                $basket = app('api')->baskets()->getByHashedId($order->cart_id);
            } catch (ModelNotFoundException $e) {
                return $this->errorNotFound();
            }

            return new BasketResource($basket);
        }

        return $this->errorNotFound('Basket is voided or been been paid.');
    }

    public function show($id)
    {
        try {
            $basket = app('api')->baskets()->getByHashedId($id);
        } catch (ModelNotFoundException $e) {
            return $this->errorNotFound();
        }

        return new BasketResource($basket);
    }

    public function create()
    {
        $basket = basket()->create('pos');

        return new BasketResource($basket);
    }

    /**
     * Gets the basket for the current user.
     *
     * @param Request $request
     *
     * @return void
     */
    public function current(Request $request)
    {
        $basket = app('api')->baskets()->getCurrentForUser($request->mbr_id, $request->get('basket_name', ''));
        if (! $basket) {
            return $this->errorNotFound("Basket does't exist");
        }

        return new BasketResource($basket);
    }

    public function update(Request $request, $basketId, $id)
    {
        app('api')->baskets()->update($basketId, $id, ['item_qty' => $request->get('item_qty')]);
    }

    public function remove($basketId, $basketLineId)
    {
        $basketLineId = explode(',', $basketLineId);
        $basket       = app('api')->baskets()->remove($basketId, $basketLineId);

        return new BasketResource($basket);
    }

    public function clear($basketId)
    {
        $basket = app('api')->baskets()->clear($basketId);

        return new BasketResource($basket);
    }

    public function destroy(Request $request, $id)
    {
        $basket = app('api')->baskets()->getBasket($id);

        if ($basket) {
            if (empty($basket->mbr_id) && empty($request->get('mbr_id'))) {
                app('api')->baskets()->destroy($basket);
            } elseif (! empty($basket->mbr_id) && $basket->mbr_id === $request->get('mbr_id')) {
                app('api')->baskets()->destroy($basket);
            }

        }

        return $this->respondWithSuccess();
    }

    public function applyRebates(ApplyRebatesRequest $request, $id)
    {

        $amount   = $request->get('rebate_amount', 0);
        $basket   = basket($id, 'pos')->get();
        $resource = new BasketResource($basket);
        $payload  = [];

        try {
            $payload = app('api')->members()->request('POST', 'reserved', [
                'description' => 'REBATE_RESERVE_'.$basket->order->order_num,
                'json'        => [
                    'mbr_id'        => $request->get('mbr_id'), 'trans_id' => $basket->order->order_num,
                    'rebate_amount' => $amount,
                ],
            ]);

        } catch (\Exception $e) {

        }

        return $resource->append(['rebate_reservation' => $payload]);
    }

    public function cancelRebates(CancelRebatesRequest $request, $id)
    {
        $basket   = basket($id, 'pos')->get();
        $resource = new BasketResource($basket);

        try {
            $payload = app('api')->members()->request('POST', 'unreserved', [
                'description' => 'REBATE_UNRESERVED_'.$basket->order->order_num,
                'json'        => [
                    'mbr_id' => $request->get('mbr_id'), 'trans_id' => $basket->order->order_num,
                ],
            ]);

        } catch (\Exception $e) {

        }

        return $resource->append(['rebate_reservation' => $payload]);
    }
}