<?php

namespace App\Http\Controllers\Basket;

use App\Http\Controllers\Controller;
use App\Http\Requests\Baskets\AddRequest;
use App\Http\Resources\Baskets\BasketResource;

class AddToBasket extends Controller
{

    protected $product;
    protected $isGuestPrice = true;

    public function __invoke(AddRequest $request, $id)
    {
        $items  = collect($request->get('items', []));
        $basket = basket($id, 'pos');


        if ($basket->model()->name === 'pos') {
            $basket->clear();
        }

        if ($mbrId = $request->get('mbr_id')) {
            $this->isGuestPrice = false;
            $basket             = $basket->setOwnerId($mbrId)->save();
        }

        // remap
        $items = $items->map(function ($item) use ($items) {

            if ($item['pwp_line_num'] === 0) {
                $item['children'] = $items->where('pwp_line_num', $item['line_num'])->all();
            }

            unset($item['line_num']);

            return $item;
        })->filter(function ($item) {
            return $item['pwp_line_num'] === 0;
        });

        $childrenCount = $items->filter(function ($item) {
            return isset($item['children']) && is_array($item['children']) && count($item['children']) > 0;
        })->count();

        if (! $childrenCount) {

            $lines = [];

            foreach ($items as $item) {
                if ($this->product = app('api')->products()->getById($item['item_id'])) {
                    $lines[] = $this->mapBasketLine($item);
                }
            }

            if ($lines) {
                $basket->add($lines);
            }
        } else {

            foreach ($items as $item) {

                $childLines = [];

                if (! $this->product = app('api')->products()->getById($item['item_id'])) {
                    continue;
                }

                $parentLine = $this->mapBasketLine($item);
                $parentLine = $basket->add($parentLine)->getInsertedLines()->last();

                if (isset($item['children']) && is_array($item['children'])) {

                    foreach ($item['children'] as $child) {
                        if ($this->product = app('api')->products()->getById($child['item_id'])) {
                            $childLines[] = $this->mapBasketChildrenLines($parentLine, $child);
                        }
                    }

                    if ($childLines) {
                        $basket->add($childLines);
                    }
                }

            }
        }

        return new BasketResource($basket->merge()->get());
    }

    protected function mapBaseBasketLine()
    {
        $line = [
            'item_id'       => $this->product->item_id,
            'item_img'      => $this->product->image_name,
            'item_desc'     => $this->product->item_desc,
            'regular_price' => $this->product->regular_price,
            'unit_price'    => $this->product->getUnitPrice($this->isGuestPrice),
        ];

        return $line;
    }

    protected function mapBasketLine(array $item)
    {
        $base = $this->mapBaseBasketLine();

        $customData = array_get($item, 'custom_data', []);

        $line = [
            'item_qty'     => $item['item_qty'],
            'custom_data'  => $customData,
            'status_level' => array_get($item, 'status_level', 0),
        ];

        $isPriceEdited = array_get($customData, 'price_edited', false);
        $editedPrice   = (float)array_get($item, 'unit_price', 0);

        if ($isPriceEdited && 0 < $editedPrice && $base['unit_price'] > $editedPrice) {
            $line['unit_price'] = $editedPrice;
        }

        return array_merge($base, $line);
    }

    protected function mapBasketChildrenLines($parentProduct, $childProduct)
    {
        return $this->mapBasketLine($childProduct) + ['parent_id' => $parentProduct->id];
    }
}