<?php

namespace App\Http\Controllers\Basket;


use App\Core\Baskets\Events\BasketSavedEvent;
use App\Core\Baskets\Interfaces\BasketServiceInterface;
use App\Core\Orders\Contracts\OrderFactory;
use App\Core\Orders\Exceptions\BasketHasPlacedOrderException;
use App\Core\Orders\Models\Order;
use App\Core\Product\Models\ProductPurchaseWithPurchase;
use App\Http\Controllers\Controller;
use App\Http\Requests\Baskets\AddToOrderRequest;
use App\Http\Resources\Baskets\BasketResource;
use App\Support\Carbon;
use App\Support\Str;
use Illuminate\Support\Arr;
use RtLopez\Decimal;

class StoreBasketAndOrder extends Controller
{

    protected $referenceOrder;
    protected $product;
    protected $line;
    protected $isMemberPrice = false;
    protected $rebateTier = 0;
    protected $orderType;

    public function __invoke(AddToOrderRequest $request, OrderFactory $factory, $id)
    {
//        $basket = basket($id, 'pos');
//        dd($id,$basket->model(),$basket);
        $this->orderType = $request->get('order_type');
        $customer = null;
        $items = collect($request->get('items', []));
        $mbrId = $request->get('mbr_id');
        $cartName = $request->get('cart_name', 'pos');
        $orderType = $request->get('order_type');
//        $basket = basket($id, 'pos');
//        dd($id,$basket);
        if ($id > 0) {
            $basket = basket($id, $cartName);
//            dd($id,$basket);
        } else {
            $basket = basket()->create($cartName, $mbrId);
            $basket = basket($basket->id, $cartName);
            $basket->clear();
//            dd('JAY');
        }
//        dd($basket);
        if (in_array($orderType, ['RF', 'EX', 'E1'])) {
            $this->referenceOrder = optional(Order::withoutGlobalScope('open')->where('order_num', $request->order_ref_id)->first())->lines;
        }
//        dd($basket->model()->name);
        if ($basket->model()->name === 'pos') {
            $basket->clear();
        }
//        dd($basket);
        // Get the member info and check the membership expiry date
        if ($mbrId) {
            if (!Str::startsWith($mbrId, 'STAFF_', false) && ($customer = app('api')->members()->getById($mbrId)) && Carbon::parse($customer->exp_date)->isFuture()) {
                $this->isMemberPrice = true;
                $basket->setCustomData('member.rebate_tier', $customer->rebate_tier);
                $basket->save();
            }
            $basket = $basket->setOwnerId($mbrId)->save();
        }
//        dd($basket->model(),$basket);
        $customData = $request->get('custom_data');
        $customData['order_ref_invoice_id'] = $request->get('order_ref_invoice');
        $basket->setCustomData($customData);
//        dd($basket->model(),$basket);
        // remap
        $items = $items->reject(function ($item) {
            return $item === null || $item['item_qty'] === 0 || ($item['item_type'] === 'D' && $item['status_level'] >= 0);
        })->map(function ($item) use ($items) {
            $item['item_id'] = Str::upper($item['item_id']);
            $item['status_level'] = (int)$item['status_level'];

            if ($item['pwp_line_num'] === 0) {
                $item['children'] = $items->where('pwp_line_num', $item['line_num'])->all();
            }

            unset($item['line_num']);

            return $item;
        })->filter(function ($item) {
            return $item['pwp_line_num'] === 0;
        });

        // Check membership card is added, enjoy lower price
        // MEMBER-RR, use rebate to buy membership
        $membershipCardCount = $items->filter(function ($item) {
            return Str::startsWith($item['item_id'], '!MEMBER-');
        })->count();

        if ($membershipCardCount > 0) {
            $this->isMemberPrice = true;
        }

        foreach ($items as $item) {

            $childLines = [];

            $parentLine = $this->mapBasketLine($item);

            if (empty($parentLine)) {
                continue;
            }

            $parentLine = $basket->add($parentLine)->getInsertedLines()->last();

            if (isset($item['children']) && is_array($item['children'])) {

                foreach ($item['children'] as $child) {

                    if ($childLine = $this->mapBasketChildLine($parentLine, $child)) {
                        $childLines[] = $childLine;
                    }
                }

                if ($childLines) {
                    $basket->add($childLines);
                }
            }

        }
//        dd($basket->model(),$basket);
        $action = 'order';

        if ($request->has('coupons')) {
            $coupons = [];
            // Temporarily disable code unique
            // collect($request->coupons)->unique('coupon_code');
            foreach (collect($request->coupons) as $coupon) {
                $coupons[] = [
                    'coupon_id'   => $coupon['coupon_id'],
                    'coupon_code' => $coupon['coupon_code'],
                ];
            }

            if ($coupons) {
                $action = null;
                $basket->model()->discounts()->createMany($coupons);
            }
        }
//        dd($basket->model(),$basket);
        event(new BasketSavedEvent($basket->model()));

//        dd('JAY',$basket->model(),$basket);
        $basket = $basket->merge()->get();

//        dd($basket);
        if ($request->get('action', $action) === 'order') {
            try {
//                dd('JAY');

                $order = $factory->basket($basket)
                    ->customer($customer)
                    ->type($orderType)
                    ->reference($request->get('order_ref_id'))
                    ->resolve();
                app()->make(BasketServiceInterface::class)->getTaxID($basket);
            } catch (BasketHasPlacedOrderException $e) {
                return $this->errorForbidden('exceptions.basket_already_has_placed_order');
            }

            return (new BasketResource($basket))->append(['order_id' => $order->id, 'order_num' => $order->order_num]);
        }

        return new BasketResource($basket);
    }

    protected function mapBaseBasketLine($item, $main = null)
    {
        $customData = [];
        $product = app('api')->products()->getById($item['item_id']);

        if (in_array($this->orderType, ['ID', 'GS', 'IS', 'DI', 'DZ'])) {

            $customData['multiple_points'] = $product->multiple_points ?? 0;
            $customData['dimensions'] = [
                'inv_dim1' => $product->inv_dim1,
                'inv_dim2' => $product->inv_dim2,
                'inv_dim3' => $product->inv_dim3,
                'inv_dim4' => $product->inv_dim4,
                'brand_id' => $product->brand_id,
                'model_id' => $product->model_id,
            ];

            if ($this->orderType == 'DI') {
                // Deposit no points
                $customData['multiple_points'] = 0;
            }

            return [
                'item_type'     => $item['item_type'],
                'item_id'       => $item['item_id'],
                'item_desc'     => $item['item_desc'],
                'regular_price' => $item['regular_price'],
                'unit_price'    => $item['unit_price'],
                'custom_data'   => $customData,
            ];
        }

        // Exchange and refund item get detail from order
        if ($this->referenceOrder && $item['item_qty'] < 0 && $item['status_level'] === -2 && $refLine = $this->referenceOrder->where('id', array_get($item, 'order_line_id', 0))->first()) {

            $img = '';

            if ($product) {
                $img = $product->image_name;
            }

            return [
                'item_type'            => $refLine->item_type,
                'item_id'              => $refLine->item_id,
                'item_img'             => $img,
                'item_desc'            => $refLine->item_desc,
                'regular_price'        => $refLine->regular_price,
                'unit_price'           => $refLine->unit_price - $refLine->unit_discount_amount,
                'unit_discount_amount' => 0,
                // todo: Deduct the item with coupon discount
                'discount_total'       => abs($refLine->unit_coupon_discount * $item['item_qty']),
                'order_line'           => $refLine->toArray(),
            ];
        }

        if ($item['item_id'] === '$DEP_MANUAL') {

            return [
                'item_type'     => 'P',
                'item_id'       => '$DEP_MANUAL',
                'item_desc'     => $item['item_desc'],
                'regular_price' => $item['regular_price'],
                'unit_price'    => $item['unit_price'],
            ];
        }

        // exclude exchange and refund
        if ($product && $item['status_level'] !== -2) {
            $customData['multiple_points'] = $product->multiple_points ?? 0;
            $unitPrice = $product->getUnitPrice($this->isMemberPrice);

            $customerGroup = request()->get('customer_group');
            $locId = request()->get('loc_id');

            $prices = app('api')->products()->getAllPrices($item['item_id'], $product->multiple_points, $customerGroup, $locId);

            if ($prices) {
                $customData['mpt'] = [
                    'id'              => Arr::get($prices, 'mpt_promo_id', ''),
                    'type'            => Arr::get($prices, 'mpt_promo_by', ''),
                    'multiple_points' => Arr::get($prices, 'multiple_points', 0),
                    'add_amount'      => Arr::get($prices, 'mpt_add_amount', 0),
                ];

                $customData['prices'] = [
                    'regular'          => (float)$prices['regular_price'],
                    'member'           => (float)$prices['mbr_price'],
                    'public_promotion' => (float)$prices['promo_price'],
                    'member_promotion' => (float)$prices['promo_price'],
                ];

                $unitPrice = $prices['unit_price'];
                $promoPrice = $prices['promo_price'];

                if ($unitPrice > $promoPrice && $promoPrice > 0) {
                    $customData['prices']['member_promotion'] = $promoPrice;

                    if ($prices['promo_ind'] === 'ALL') {
                        $customData['prices']['public_promotion'] = $promoPrice;
                    }
                }

                if (!empty($prices['promo_id'])) {
                    $customData['promo_id'] = $prices['promo_id'];
                }

            }

            if ($main) {
                // PWP price overwrite
                $promo = ProductPurchaseWithPurchase::where('ref_id', $main->item_id)->where('promo_ref_id', $product->item_id)->location(request()->get('loc_id'))->active()->orderBy('promo_price')->first();

                if ($promo) {

                    $customData['prices']['member_promotion'] = (float)$promo->promo_price;

                    if ($promo->price_ind === 'ALL') {
                        $customData['prices']['public_promotion'] = (float)$promo->promo_price;
                    }

                    $unitPrice = $promo->promo_price;
                    $customData['promo_id'] = $promo->promo_id;
                }
            }

            // Store dimensions for after-sales processing/checking
            $customData['dimensions'] = [
                'inv_dim1' => $product->inv_dim1,
                'inv_dim2' => $product->inv_dim2,
                'inv_dim3' => $product->inv_dim3,
                'inv_dim4' => $product->inv_dim4,
                'brand_id' => $product->brand_id,
                'model_id' => $product->model_id,
            ];

            $desc = $product->item_desc;

            return [
                'item_type'     => $product->item_type,
                'item_id'       => $product->item_id,
                'item_img'      => $product->image_name,
                'item_desc'     => $desc,
                'regular_price' => $product->regular_price,
                'unit_price'    => $unitPrice,
                'custom_data'   => $customData,
            ];
        }

        return [];
    }

    protected function mapBasketLine(array $item, $main = null)
    {
        $base = $this->mapBaseBasketLine($item, $main);

        if (empty($base)) {
            return [];
        }

        $customData = array_get($item, 'custom_data', []);

        if (array_has($base, 'custom_data')) {
            $customData = array_merge($customData, $base['custom_data']);
            array_forget($base, 'custom_data');
        }

        array_forget($customData, ['message', 'is_staff_price', 'spread_discount']);

        if (array_has($base, 'order_line')) {
            array_set($customData, 'order_line', array_get($base, 'order_line'));
            array_forget($base, 'order_line');
        }

        $line = [
            'item_qty'     => $item['item_qty'],
            'custom_data'  => $customData,
            'status_level' => array_get($item, 'status_level', 0),
            'created_by'   => user_id(),
        ];

        // Precaution
        if ($this->orderType === 'PS' && $line['status_level'] !== 0 && $line['item_qty'] === 0) {
            $line['item_qty'] = 1;
        }

        $isPriceEdited = array_get($customData, 'price_edited', false);
        $editedPrice = array_get($item, 'unit_price', $base['unit_price']);

        if ($isPriceEdited && Decimal::create($editedPrice)->ge(0) /*&& $line['status_level'] !== -2*/) {
            $line['unit_price'] = $editedPrice;
        }

        return array_merge($base, $line);
    }

    protected function mapBasketChildLine($parentProduct, $childProduct)
    {
        $data = $this->mapBasketLine($childProduct, $parentProduct);

        if (empty($data)) {
            return [];
        }

        return $data + ['parent_id' => $parentProduct->id];
    }

}