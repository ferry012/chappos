<?php

namespace App\Http\Controllers\Basket\Hachi;

use App\Core\Baskets\Models\Basket;
use App\Core\Discounts\DiscountFactory;
use App\Core\Discounts\DiscountFactoryV2;
use App\Core\Discounts\Models\Discount;
use App\Core\Discounts\Models\DiscountRule;
use App\Http\Controllers\Controller;
use App\Http\Resources\Baskets\BasketResource;
use Flugg\Responder\Responder;
use Illuminate\Http\Request;

class ApplyCoupon extends Controller
{
    public function __invoke(Request $request, Responder $responder, $id)
    {
        try {

            $basket = Basket::where('cart_name', 'hachi')->where('id', $id)->first();

        } catch (\Exception $e) {
            return $responder->error('', 'Basket not found');
        }

        if ($request->has('coupons')) {
            $coupons = [];
            // Temporarily disable code unique
            // collect($request->coupons)->unique('coupon_code');
            foreach (collect($request->coupons) as $coupon) {
                $coupons[] = [
                    'coupon_id'   => isset($coupon['coupon_id']) ? $coupon['coupon_id'] : '',
                    'coupon_code' => $coupon['coupon_code'],
                ];
            }

            if ($coupons) {
                $basket->discounts()->delete();
                $basket->discounts()->createMany($coupons);

                sleep(1);

                (new DiscountFactoryV2())->setBasket($basket)->resolve();
                $basket->refresh();
                app('api')->baskets()->calculate($basket);
            }
        }

        $basket->load('discounts');

        return $responder->success((new BasketResource($basket))->toArray($request));
    }
}