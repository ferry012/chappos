<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 14/4/2020
 * Time: 12:14 PM
 */

namespace App\Http\Controllers\Basket\Hachi;


use App\Core\Baskets\Models\Basket;
use App\Core\Orders\Contracts\OrderFactory;
use App\Core\Orders\Exceptions\BasketHasPlacedOrderException;
use App\Http\Resources\Orders\OrderResource;
use App\Utils\Request;
use Exception;
use Flugg\Responder\Responder;

class PlaceOrder
{
    public function __invoke(Request $request, Responder $responder, OrderFactory $factory, $id)
    {
        ini_set('max_execution_time', 60);
        
        try {

            $basket = Basket::where('id', $id)->first();

        } catch (Exception $e) {
            return $responder->error('', 'Basket not found');
        }

        $customer = null;

        try {

            $isGuestPurchase = $basket->getCustomData('is_guest_purchase', false);

            if (!$isGuestPurchase) {
                $customer = app('api')->members()->getById($basket->owner_id);
            }

            db()->beginTransaction();

            $order = $factory->basket($basket)
                ->customer($customer)
                ->setIsGuestPurchase($isGuestPurchase)
                ->setOrderNumber($request->get('orderNumber'));

            if ($request->get('quoteToOrder', false)) {
                $order = $order->type('HQ');
            } else {
                $order = $order->type('HO');
            }

            $order = $order->resolve();

            db()->commit();

            return $responder->success((new OrderResource($order))->toArray($request));
        } catch (BasketHasPlacedOrderException $e) {

            db()->rollback();

            return $responder->error('', 'Basket has placed order');
        } catch (\Exception $e) {
            db()->rollback();

            return $responder->error('', 'Place order failed. Please try again!');
        }

    }
}