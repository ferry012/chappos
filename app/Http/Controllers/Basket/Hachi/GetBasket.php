<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 15/4/2020
 * Time: 3:07 PM
 */

namespace App\Http\Controllers\Basket\Hachi;

use App\Core\Baskets\Models\Basket;
use App\Core\Discounts\DiscountFactoryV2;
use App\Core\Discounts\Models\DiscountRule;
use App\Core\Orders\Models\Order;
use App\Core\Orders\Models\OrderLine;
use App\Core\Product\Models\Product;
use App\Core\Product\Models\ProductPurchaseWithPurchase;
use App\Http\Resources\Baskets\BasketResource;
use App\Support\Carbon;
use App\Support\Str;
use App\Utils\Request;
use Flugg\Responder\Responder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Schema;
use RtLopez\Decimal;

class GetBasket
{
    public $isGuestPurchase = false;
    public $rebateTier = 0;

    public function __invoke(Request $request, Responder $responder, $id)
    {
        ini_set('max_execution_time', 60);

        try {
            $basket = Basket::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return $responder->error('resource_not_found');
        }

        $basket->load('discounts');

        $data = [];
        $rebateTier = 0;
        $lite = $request->get('lite', 0);
        $hasPendingOrder = $request->get('has_pending_order', false);
        $this->isGuestPurchase = $request->get('customer_group') === 'GUEST';
        $expressCutOffTime = Carbon::now()->setTime(14, 50);
        $expressMinOderAmt = 100; // 2-hrs express minimum order amount

        if (!$this->isGuestPurchase) {
            $customer = app('api')->members()->getById($basket->owner_id);

            if ($customer) {
                $rebateTier = (float)$customer->rebate_tier ?? 0;
            }
        }

        $membership = $basket->lines->filter(function ($item) {
            return Str::startsWith($item->item_id, '!MEMBER-');
        })->first();

        if ($membership) {

            $memberType = app('api')->members()->getMemberTypeByItemId($membership->item_id);
            $rebateTier = app('api')->members()->getRebateTierByItemId($membership->item_id);

            $basket->setCustomData('member.type', $memberType);
            $basket->setCustomData('member.rebate_tier', $rebateTier);

        }

        if ($this->isGuestPurchase) {
            $rebateTier = setting('hachi.guest.rebate_tier', 1);
        }

        $this->rebateTier = $rebateTier;

        if (!$hasPendingOrder) {

            if (!$lite) {
                $basket = $this->refresh($basket);
            }

            $basket = app('api')->baskets()->calculate($basket);

            $basket->refresh('lines');

            $discountLineTotal = 0;
            $lineDiscountTotal = 0;

            foreach ($basket->lines as $line) {

                if (!$line->isBuyable()) {
                    if ($line->status_level >= 0 && $line->isDiscountItem()) {
                        $discountLineTotal += abs($line->unit_price) * $line->item_qty;
                    }

                    continue;
                }

                $lineDiscountTotal += $line->discount_total;

                if ($line->isOverdiscount()) {
                    $line->status_level = -1;
                    $line->setCustomData('message', 'Invalid discount amount. Please remove it.');
                    $line->save();
                }

            }

            $alerts = [];
            $basket->unsetCustomData('alerts');

            if ($discountLineTotal > 0 || $lineDiscountTotal > 0) {
                $discountVarianceAmount = abs($discountLineTotal - $lineDiscountTotal);

                if ($discountVarianceAmount > 0.03) {
                    $alerts[] = [
                        'priority' => 10,
                        'type'     => 'error',
                        'message'  => 'Invalid discount.',
                    ];
                }

            }

            if (!$basket->isPayable()) {
                $alerts[] = [
                    'priority' => 20,
                    'type'     => 'error',
                    'message'  => 'Minimum amount of $0.01 is required per transaction.',
                ];
            }

            if ($basket->anyErrors()) {
                $alerts[] = [
                    'priority' => 10,
                    'type'     => 'info',
                    'message'  => 'Please verify items with highlighted message(s).',
                ];
            }

            // Ensure subscription contains 1 line only
            if ($basket->itemCount() > 1 && $basket->anySubscriptions()) {
                $alerts[] = [
                    'priority' => 20,
                    'type'     => 'info',
                    'message'  => 'The subscription item cannot be checkout with others',
                ];
            }

            if ($basket->cart_name === 'express') {
                /*
                if (Carbon::now()->greaterThan($expressCutOffTime)) {
                    $alerts[] = [
                        'priority' => 11,
                        'type'     => 'info',
                        'message'  => 'Reached the cut-off time, Please try again next day.',
                    ];
                }
                */
                $cartItemCount = $basket->itemCount();
                $cartExpressItemCount = $basket->itemCount(Basket::DELV_EXPRESS);

                if ($cartItemCount > 0 && $cartItemCount !== $cartExpressItemCount) {
                    $alerts[] = [
                        'priority' => 11,
                        'type'     => 'error',
                        'message'  => 'Cart contains non-express delivery items, please change the delivery mode or remove it.',
                    ];
                }

                if (Decimal::create($basket->total_amount)->lt($expressMinOderAmt)) {
                    $alerts[] = [
                        'priority' => 12,
                        'type'     => 'info',
                        'message'  => sprintf('Minimum order amount of $%.2f is required.', $expressMinOderAmt),
                    ];
                }
            }

            if ($alerts) {
                $basket->setCustomData('alerts', $alerts);
            }

            $basket->save();
        }

        $data['discounts'] = [];
        $discountIds = $basket->lines->where('item_type', 'D')->pluck('unit_price', 'item_id')->all();

        if ($discountIds) {
            $discountRules = DiscountRule::whereIn('coupon_id', array_keys($discountIds))->get([
                'coupon_name',
                'coupon_description',
            ]);

            foreach ($discountRules as $discountRule) {

                $desc = $discountRule->coupon_description;
                $amount = .0;

                if (isset($discountIds[$discountRule->coupon_id])) {
                    $amount = $discountIds[$discountRule->coupon_id];
                }

                if (empty($desc)) {
                    $desc = 'Sorry, content will be available soon.';
                }

                $data['discounts'][] = [
                    'name'        => $discountRule->coupon_name,
                    'description' => $desc,
                    'amount'      => $amount,
                ];
            }

        }

        $basket = $this->finalize($basket);


        return $responder->success((new BasketResource($basket))->append($data)->toArray($request));
    }

    protected function finalize($basket)
    {
        $lineNum = 1;

        foreach ($basket->lines->sortBy('id') as $line) {

            $line->line_num = $lineNum;

            if (!isset($line->pwp_line_num) || $basket->lines->where('parent_id', $line->id)->count() > 0) {
                $line->pwp_line_num = 0;
            }

            $basket->lines->where('parent_id', $line->id)->map(function ($line) use ($lineNum) {
                $line->pwp_line_num = $lineNum;

                return $line;
            });

            $lineNum++;
        }

        // Hidden lines
        if ($basket->lines) {
            $basket->lines = $basket->lines->reject(function ($line) {

                return $line->item_type === 'D';
            });
        }

        return $basket;
    }

    protected function refresh($basket)
    {
        $customerGroup = request('customer_group', 'member');
        $locId = request('loc_id');
        $ids = $basket->lines->pluck('item_id')->unique();
        $cartName = $basket->cart_name;
        $uid = $basket->owner_id;
        $skipDiscount = false;

        $products = Product::whereIn('item_id', $ids)->get();
        $promoOOS = db()->table('crm_promo_oos')->get();
        // TODO: Need to replace with setting to control item purchase limit
        $arrGiftCardItemIds[0] = ['0885370614565', '0885370614572', '0885370614589',];
        $arrGiftCardItemIds[1] = [
            '9009111000078',
            '9009111000061',
            '5450537002902',
            '9009111000054',
            '5450537002872',
            '5051644053117',
            '5051644053131',
            '9009111000016',
            '9009111000023',
            '5051644021826',
            '5051644021819',
            '5051644021796',
            '5450537002919',
            '5051644021789',
            '5051644021772',
            '9009111000047',
            '9009111000030',
            '5051644021802',
            '9009111000092',
            '5450537006641',
            '5450537002896',
            '5450537002889',
            '5051644053094',
            '9009111000139',
            '9009111000085',
            '9009111000160',
            '9009111000115',
            '9009111000184',
            '9009111000191',
            '9009111000146',
        ];

        foreach ($basket->lines->sortBy('id') as $line) {
            $giftCardItemIds = [];

            $line->unsetCustomData([
                'is_free',
                'is_subscription',
                'multiple_points',
                'unit_points_earned',
                'unit_rebate_earned',
                'add_rebate_tier',
                'mpt',
            ]);

            // Guest not allowed to buy gift card
            if ($this->isGuestPurchase && $line->isParent() && ($line->isGiftCard() || $line->isSubscription())) {
                $line->status_level = -1;
                $line->setCustomData('actions.editable', false);
                $line->setCustomData('message', 'This product is only available for ValueClub members only. Please remove it.');
                $line->save();

                continue;
            }

            // Subscription item
            // Buy now only
            if ($line->isSubscription()) {
                $line->setCustomData('actions.editable', false);
                $line->setCustomData('actions.deletable', true);

                if ($cartName !== 'hachi_express') {
                    $line->status_level = -1;
                    $line->setCustomData('message', 'This item cannot be checkout with others.');
                    $line->save();

                    continue;
                }

                // No discount
                $skipDiscount = true;

                // Max 1 qty only
                $line->item_qty = 1;
                $line->delv_method = null;
                $line->loc_id = null;
                $line->setCustomData('is_subscription', true);
                $line->save();

                continue;
            }

            if (!in_array($line->item_type, [
                    'A',
                    'W'
                ], false) && ($line->isDigital() || Str::startsWith($line->item_id, '!'))) {
                $line->delv_method = null;
                $line->loc_id = null;
            }

            // Ignore the discount line and item id starts with '!'
            if ($line->isDiscountItem() || Str::startsWith($line->item_id, '!')) {
                $line->setCustomData('actions', ['editable' => false, 'deletable' => false]);

                if ($line->isMembershipItem()) {
                    $line->setCustomData('actions.deletable', true);
                }

                $line->save();

                continue;
            }

            if ($line->status_level === -11) {
                $line->status_level = -1;
            } else {
                $line->status_level = 0;
                $line->unsetCustomData('message');
            }

            $line->discount_total = 0;
            $line->setCustomData('actions', ['editable' => true, 'deletable' => true]);

            $product = $products->where('item_id', $line->item_id)->first();

            if ($product === null || (!empty($product) && $product->is_active === 'N')) {
                $line->status_level = -1;
                $line->setCustomData('actions.editable', false);
                $line->setCustomData('message', 'This product is currently taken out of our catalogue. Please remove it.');
                $line->save();

                continue;
            }

            if ($promoOOS->where('item_id', $line->item_id)->count() > 0) {
                $line->status_level = -1;
                $line->setCustomData('actions.editable', false);
                $line->setCustomData('message', 'This product is currently out of stock. Please remove it.');
                $line->save();

                continue;
            }

            foreach ($arrGiftCardItemIds as $arrGiftCardItemId) {
                if (in_array($line->item_id, $arrGiftCardItemId, false)) {
                    $giftCardItemIds = $arrGiftCardItemId;
                    break;
                }
            }

            if (!empty($giftCardItemIds) && is_array($giftCardItemIds)) {
                $line->setCustomData('actions.editable', false);

                $priceTotal = $line->unit_price * $line->getQuantity();
                $orderIds = Order::where('customer_id', $basket->owner_id)->purchased()->today()->pluck('id');
                $mOrderLines = OrderLine::whereIn('order_id', $orderIds)->whereIn('item_id', $giftCardItemIds)->purchased()->get();
                $purchasedAmount = $mOrderLines->sum(function ($line) {
                    return $line->unit_price * $line->qty_ordered;
                });

                $purchasedAmount += $priceTotal;

                if ($purchasedAmount > 200) {
                    $line->status_level = -1;
                    $line->setCustomData('message', 'Amount selected is above limit. Please contact Customer Service or use Live Chat to find out more.');
                    $line->save();

                    continue;
                }
            }

            $regularPrice = $line->regular_price;
            $unitPrice = $line->unit_price;
            $prices = app('api')->products()->getAllPrices($line->item_id, $product->multiple_points, $customerGroup, $locId);

            if ($prices) {
                $line->setCustomData('multiple_points', $product->multiple_points);
                $line->setCustomData('mpt', [
                    'id'              => Arr::get($prices, 'mpt_promo_id', ''),
                    'type'            => Arr::get($prices, 'mpt_promo_by', ''),
                    'multiple_points' => Arr::get($prices, 'multiple_points', 0),
                    'add_amount'      => Arr::get($prices, 'mpt_add_amount', 0),
                ]);

                $regularPrice = $prices['regular_price'];
                $unitPrice = $prices['unit_price'];
                $promoPrice = $prices['promo_price'];

                if ($unitPrice > $promoPrice && $promoPrice > 0) {
                    if ($prices['price_ind'] === 'ALL') {
                        $line->setCustomData('prices.public_promotion', (float)$promoPrice);
                    } else {
                        $line->setCustomData('prices.member_promotion', (float)$promoPrice);
                    }
                }

                if (!empty($prices['promo_id'])) {
                    $purchaseLimits = app('api')->products()->getPurchaseLimits($prices['promo_id'], $line->item_id);
                    $limitResult = $this->validatePurchaseLimit($purchaseLimits, $uid, $line, $prices);

                    if (is_array($limitResult) && $limitResult['status'] === -1) {
                        $line->status_level = $limitResult['status'];
                        $line->setCustomData('message', $limitResult['message']);
                        $line->save();

                        continue;
                    }
                }

            }

            if ($this->isGuestPurchase) {
                $prices = app('api')->products()->getAllPrices($line->item_id, $product->multiple_points, 'MEMBER', $locId, $this->rebateTier);

                if ($prices) {
                    $line->setCustomData('multiple_points', $product->multiple_points);
                    $line->setCustomData('mpt', [
                        'id'              => Arr::get($prices, 'mpt_promo_id', ''),
                        'type'            => Arr::get($prices, 'mpt_promo_by', ''),
                        'multiple_points' => Arr::get($prices, 'multiple_points', 0),
                        'add_amount'      => Arr::get($prices, 'mpt_add_amount', 0),
                    ]);
                    $line->setCustomData('prices.valueclub', (float)$prices['unit_price']);
                }
            }

            $line->setCustomData('prices.member_promotion', (float)$promoPrice);

            // PWP item
            if ($line->parent_id !== null && $line->parent_id > 0) {
                $line->setCustomData('actions.editable', false);

                $mainLine = $basket->lines->where('id', $line->parent_id)->first();

                if ($mainLine === null) {
                    $line->status_level = -1;
                    $line->setCustomData('message', 'This main item is not attached. Please remove it.');
                    $line->save();

                    continue;
                }

                if ($product->item_id === 'SSEW') {
                    $unitPrice = app('api')->products()->getExtendWarrantyPrice($mainLine->product, $mainLine->unit_price);
                    $regularPrice = $unitPrice;
                } else {
                    $pwpItem = ProductPurchaseWithPurchase::where('ref_id', $mainLine->item_id)->where('promo_ref_id', $line->item_id)
                        ->location([$basket->loc_id, 'OS', 'HCL'])->active()->orderBy('promo_price')->first();

                    if ($pwpItem) {

                        $line->setCustomData('promo_id', $pwpItem->promo_id);
                        $line->setCustomData('prices.member_promotion', (float)$pwpItem->promo_price);

                        if ($pwpItem->price_ind === 'ALL') {
                            $line->setCustomData('prices.public_promotion', (float)$pwpItem->promo_price);
                        }

                        $unitPrice = $pwpItem->promo_price;

                        if (Decimal::create($unitPrice)->eq(0)) {
                            $line->getCustomData('is_free', true);
                        }

                        if ($this->isGuestPurchase) {
                            if ($pwpItem->price_ind !== 'ALL') {
                                $line->status_level = -1;
                                $line->setCustomData('message', 'ValueClub Members Exclusive');
                            }
                        }

                    } else {
                        $line->status_level = -1;
                        $line->setCustomData('message', 'The product is taken out from the Purchase with Purchase promotion. Please remove it.');
                    }
                }
            }

            // TODO: Qty limit per member, trans

            // Global main item max limit
            // Except google card, gift
            if ($line->parent_id === null && $line->parent_id !== 0
                && !in_array($line->item_type, [
                    'G',
                    'E',
                ])
                && $line->getQuantity() > 20
            ) {
                $line->status_level = -1;
                $line->setCustomData('message', 'Please note that any order for an item with quantity exceeding 20 can only be done via our Corporate Sales.');
                $line->save();

                continue;
            }

            $isFreeItem = $line->getCustomData('is_free', false);

            if (!$isFreeItem) {
                $unitPrice = $unitPrice ?? .0;

                if ($line->isParent() && Decimal::create($unitPrice)->le(0)) {
                    $line->status_level = -1;
                    $line->setCustomData('message', 'Price went wrong. Please contact our customer service.');
                }
            }

            $line->item_type = $product->item_type;
            $line->item_desc = $product->short_desc;
            $line->regular_price = $regularPrice;
            $line->unit_price = $unitPrice;
            $line->setCustomData('multiple_points', (float)($product->multiple_points ?? .0));
            $line->save();
        }

        if (!$skipDiscount) {
            (new DiscountFactoryV2())->setBasket($basket)->resolve();
        } else {
            $basket->flushDiscounts();
        }

        $newMPT = setting('pos.enable_mpt_v2', false);

        foreach ($basket->lines->sortBy('id') as $line) {

            // Ignore the discount line and item id starts with '!'
            if (!$line->isBuyable()) {
                continue;
            }

            $enableRebateTier = true;
            $line->unsetCustomData(['rebate_promotion']);

            $multiply = $line->getCustomData('multiple_points', 0);

            if (!$newMPT) {
                $promoRebate = app('api')->products()->getPromotionRebate($line->item_id, $basket->loc_id);

                if ($promoRebate) {
                    $promoRebate['value'] = (float)$promoRebate['value'];
                    $line->setCustomData('rebate_promotion', $promoRebate);

                    if ($promoRebate['type'] === 'FIXED') {
                        $enableRebateTier = false;
                        $line->setCustomData('add_rebate_tier', $enableRebateTier);
                        $multiply = $promoRebate['value']; // Remove tier calculation
                    } else {
                        $multiply *= $promoRebate['value'];
                    }
                }

                $unitPointsEarned = floor($line->discounted_price * $multiply * $this->rebateTier);
                $unitRebateAmount = $unitPointsEarned / 100;
                $line->setCustomData('unit_points_earned', $unitPointsEarned);
                $line->setCustomData('unit_rebate_earned', $unitRebateAmount);
            } else {
                $unitPointsEarned = $line->getUnitRebatePoints($this->rebateTier);
                $unitRebateAmount = $line->getUnitRebateAmount($this->rebateTier);
                $line->setCustomData('unit_points_earned', $unitPointsEarned);
                $line->setCustomData('unit_rebate_earned', $unitRebateAmount);
            }

            $line->save();
        }

        return $basket;
    }

    protected function validatePurchaseLimit($limits, $uid, $item, $prices)
    {
        $message = '';
        $qty = $item->item_qty;
        $pid = $item->item_id;
        $price = .0;
        $status = 0;

        if (empty($limits) || !Schema::connection(env('DB_CONNECTION'))->hasTable('v_ordered_items_last_6_months')) {
            return null;
        }

        if (isset($limits['per_trans']) && $limits['per_trans'] > 0 && $qty > $limits['per_trans']) {
            $status = -1;
            $message = "Order quantity limit({$limits['per_trans']}) per Transaction exceeded";
        }

        if (!$status && isset($limits['per_member']) && $limits['per_member'] > 0) {
            $totalQty = db()->table('v_ordered_items_last_6_months')->where('customer_id', $uid)->where('item_id', $pid)->get()->sum(static function ($row) {

                return $row->qty_ordered - $row->qty_refunded;
            });

            if (($totalQty + $qty) > $limits['per_member']) {
                $status = -1;
                $message = "Order quantity limit({$limits['per_member']}) per Member exceeded";
            }
        }

        if (!$status && isset($limits['per_day']) && $limits['per_day'] > 0) {
            $totalQty = db()->table('v_ordered_items_last_6_months')->where('item_id', $pid)->whereDate('pay_on', Carbon::today()->toDateString())->get()->sum(static function ($row) {

                return $row->qty_ordered - $row->qty_refunded;
            });

            if (($totalQty + $qty) > $limits['per_day']) {
                $status = -1;
                $message = "Order quantity limit({$limits['per_day']}) per Day exceeded";
            }
        }

        /*
        if (isset($limits['max']) && $limits['max'] > 0) {
            $totalQty = db()->table('v_ordered_items_last_6_months')->where('customer_id', $uid)->where('item_id', $pid)->sum(static function ($row) {
                return $row->qty_ordered - $row->qty_refunded;
            });

            if ($totalQty > $limits['max']) {
                $status = 0;
                $price  = $prices['promo_price'];

                if (Decimal::create($price)->eq(0) && Decimal::create($prices['unit_price'])->gt(0)) {
                    $price = $prices['unit_price'];
                }

                $message = "Order quantity of {$limits['max']} at promo price {$price} has sold out";
            }
        }
        */

        return [
            'status'  => $status,
            'message' => $message,
        ];
    }
}
