<?php

namespace App\Http\Controllers\Basket\Hachi;

use App\Core\Orders\Models\Order;
use App\Http\Resources\Orders\OrderResource;
use App\Utils\Request;
use Exception;
use Flugg\Responder\Responder;

class GetOrder
{
    public function __invoke(Request $request, Responder $responder, $id)
    {
        ini_set('max_execution_time', 60);

        try {

            $order = Order::where('order_num', $id)->withoutGlobalScope('open')->firstOrFail()->load([
                'lines' => function ($q) {
                    return $q->orderBy('line_num');
                }, 'transactions',
            ]);

        } catch (Exception $e) {
            return $responder->error('', 'Order not found');
        }

        // Hidden lines
        if ($order->lines) {
            $order->lines = $order->lines->reject(function ($line) {

                return $line->item_type === 'D';
            });
        }

        return $responder->success((new OrderResource($order))->toArray($request));
    }
}