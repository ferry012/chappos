<?php

namespace App\Http\Controllers\Basket\Hachi;

use App\Http\Controllers\Controller;
use App\Http\Resources\Baskets\BasketResource;
use App\Utils\Request;
use Flugg\Responder\Responder;

class DeleteLine extends Controller
{
    public function __invoke(Request $request, Responder $responder, $basketId)
    {
        $basketLineId = explode(',', $request->get('id'));

        if(empty($basketLineId)){
            return $responder->error('', 'params missing');
        }

        $basket = app('api')->baskets()->remove($basketId, $basketLineId);

        if ($basket) {
            //$basket = app('api')->baskets()->calculate($basket);

            //return $responder->success((new BasketResource($basket))->toArray($request));
            app('api')->baskets()->calculate($basket);

            return $responder->success();
        }

        return $responder->error('', 'basket not found');
    }
}