<?php

namespace App\Http\Controllers\Basket\Hachi;

use App\Core\Baskets\Models\Basket;
use App\Core\Discounts\DiscountFactory;
use App\Core\Discounts\Models\Discount;
use App\Http\Controllers\Controller;
use App\Http\Resources\Baskets\BasketResource;
use Flugg\Responder\Responder;
use Illuminate\Http\Request;

class RemoveCoupon extends Controller
{
    public function __invoke(Request $request, Responder $responder, $id)
    {
        try {

            $basket = Basket::where('cart_name', 'hachi')->where('id', $id)->first();

        } catch (\Exception $e) {
            return $responder->error('', 'Basket not found');
        }

        if ($request->has('code')) {
            $basket->discounts()->delete();
            $basket->lines()->where('item_type', 'D')->delete();
            $basket->discounts()->delete();
        }

        return $responder->success();
    }
}