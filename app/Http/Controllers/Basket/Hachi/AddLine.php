<?php

namespace App\Http\Controllers\Basket\Hachi;

use App\Core\Baskets\Events\BasketSavedEvent;
use App\Core\Orders\Contracts\OrderFactory;
use App\Core\Product\Models\ProductPurchaseWithPurchase;
use App\Http\Controllers\Controller;
use App\Http\Resources\Baskets\BasketResource;
use App\Support\Str;
use App\Utils\Request;
use Flugg\Responder\Responder;
use Illuminate\Support\Arr;

class AddLine extends Controller
{
    public function __invoke(Request $request, Responder $responder, OrderFactory $factory, $id)
    {
        $customer = null;

        $cartName = $request->get('cart_name', 'hachi');

        $basket = basket($id, $cartName);
        $item   = $request->all();

        Arr::forget($item, ['user_data']);

        $pwpItems = Arr::pull($item, 'pwp_items', []);

        $mainItem = $this->mapBasketLine($item);

        if ($mainItem === null) {
            return $responder->error('', 'Product not found');
        }

        if (array_has($item, 'editId')) {
            app('api')->baskets()->remove($id, $item['editId']);
        }

        $mainItem = $basket->add($mainItem)->getInsertedLines()->last();

        $childLines = [];

        foreach ($pwpItems as $item) {

            if ($childLine = $this->mapBasketChildLine($mainItem, $item)) {
                $childLines[] = $childLine;
            }

        }

        if ($childLines) {
            $basket->add($childLines);
        }

        event(new BasketSavedEvent($basket->model()));

        $basket->merge();

        return $responder->success();
    }

    protected function mapBaseBasketLine($item, $main = null)
    {
        $customData = [];

        $product = app('api')->products()->getById($item['item_id']);

        if ($product === null) {
            return null;
        }

        $unitPrice = $product->getUnitPrice(true);

        $customData['prices'] = [
            'regular'          => (float)$product->regular_price,
            'member'           => (float)$product->mbr_price,
            'public_promotion' => (float)$product->orig_promo_price,
            'member_promotion' => (float)$product->mbr_price,
        ];

        if ($product->is_promo_overwritten) {

            $product1 = app('api')->products()->getCheapPromotionalItem($item['item_id']);

            if ($product1) {
                if ($product1->price_ind === 'ALL') {
                    $customData['prices']['public_promotion'] = (float)$product1->promo_price;
                } else {
                    $customData['prices']['member_promotion'] = (float)$product1->promo_price;
                }
            }

        }

        if ($main) {

            if ($product->item_id === 'SSEW') {
                $unitPrice              = app('api')->products()->getExtendWarrantyPrice($main->product, $main->unit_price);
                $product->regular_price = $unitPrice;
            } else {
                // PWP price overwrite
                $promo = ProductPurchaseWithPurchase::where('ref_id', $main->item_id)->where('promo_ref_id', $product->item_id)->location(request()->get('loc_id'))->active()->orderBy('promo_price')->first();

                if ($promo) {

                    $customData['prices']['member_promotion'] = (float)$promo->promo_price;

                    if ($promo->price_ind === 'ALL') {
                        $customData['prices']['public_promotion'] = (float)$promo->promo_price;
                    }

                    $unitPrice              = $promo->promo_price;
                    $customData['promo_id'] = $promo->promo_id;
                }
            }

        }

        $desc = $product->short_desc;

        return [
            'item_type'     => $product->item_type,
            'item_id'       => $product->item_id,
            'item_img'      => $product->image_name,
            'item_desc'     => $desc,
            'regular_price' => $product->regular_price,
            'unit_price'    => $unitPrice,
            'custom_data'   => $customData,
        ];

    }

    protected function mapBasketLine(array $item, $main = null)
    {
        $base = $this->mapBaseBasketLine($item, $main);

        if ($base === null) {
            return null;
        }

        $customData = array_get($item, 'custom_data', []);

        if (array_has($base, 'custom_data')) {
            $customData = array_merge($customData, $base['custom_data']);
            array_forget($base, 'custom_data');
        }

        array_forget($customData, ['message', 'is_staff_price', 'spread_discount']);

        $line = [
            'item_qty'     => $item['item_qty'],
            'delv_method'  => $item['delv_method'],
            'loc_id'       => Arr::get($item, 'store_id'),
            'custom_data'  => $customData,
            'status_level' => array_get($item, 'status_level', 0),
            'created_by'   => user_id(),
        ];

        return array_merge($base, $line);
    }

    protected function mapBasketChildLine($parentProduct, $childProduct)
    {
        $data = $this->mapBasketLine($childProduct, $parentProduct);

        if ($data) {
            return $data + ['parent_id' => $parentProduct->id];
        }

        return null;
    }
}