<?php

namespace App\Http\Controllers;


use App\Http\Resources\StaffPurchaseDetailResource;
use App\Models\StaffPurchase;
use App\Support\Carbon;

class ShowStaffPurchaseDetail extends Controller
{
    public function __invoke($id)
    {
        $staff = StaffPurchase::find([
            'coy_id'         => coy_id(), 'staff_id' => $id,
            'financial_year' => Carbon::now()->format('Y'),
        ]);

        if ($staff === null) {
            return $this->errorNotFound();
        }

        return new StaffPurchaseDetailResource($staff);
    }
}