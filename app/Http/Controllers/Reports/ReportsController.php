<?php

namespace App\Http\Controllers\Reports;
use App\Http\Controllers\Controller;

use App\Support\Carbon;

class ReportsController extends Controller
{

    public function rep_sales_data(){
        $input = request()->only(['skip','take','from','salesperson']);
        $take = (isset($input['take']) && $input['take']<1000) ? $input['take'] : 500;
        $skip = (isset($input['skip'])) ? $input['skip'] * $take : 0;
        $from = $input['from'] ?? date('Y-m-d', strtotime('yesterday'));
        $coy = 'CTL';

        $optional_uri = '';
        $optional_sql = '';

        if (isset($input['salesperson'])){
            $optional_uri = ''; //"&salesperson=" . $input['salesperson'];
            $optional_sql = "AND a.salesperson_id in ('" . implode("','", explode(',',$input['salesperson'])) . "')";
        }

        $sql1 = "SELECT count(*) cnt
                    from rep_sales_data a
                        left join rep_sales_data_info b on a.coy_id = b.coy_id and a.trans_id = b.trans_id and a.line_num=b.line_num
                    where a.coy_id = ? and a.modified_on::date = ? $optional_sql";
        $result1 = db('pg_cherps')->select($sql1, [$coy, $from]);

        if ($result1) {
            $sql2 = "select rtrim(a.coy_id) as coy_id,a.trans_date,rtrim(a.trans_id) as trans_id,a.line_num,
                            rtrim(a.trans_type) as trans_type,trans_date,
                            rtrim(a.loc_id) as loc_id,rtrim(a.pos_id) as pos_id,
                            case when b.ref_id is null then '' else rtrim(b.ref_id) end as ref_id,
                            rtrim(a.item_id) as item_id,rtrim(a.item_desc) as item_desc,a.trans_qty,a.unit_price,a.tax_percent,a.sales_amount,a.status_level,
                            upper(rtrim(a.salesperson_id)) as salesperson_id,
                            rtrim(item_type) as item_type,rtrim(a.brand_id) as brand_id,rtrim(a.model_id) as model_id,
                            rtrim(a.inv_dim1) as inv_dim1,rtrim(a.inv_dim2) as inv_dim2,rtrim(a.inv_dim3) as inv_dim3,rtrim(a.inv_dim4) as inv_dim4,
                            a.modified_on
                    from rep_sales_data a
                        left join rep_sales_data_info b on a.coy_id = b.coy_id and a.trans_id = b.trans_id and a.line_num=b.line_num
                    where a.coy_id = ? and a.modified_on::date = ? $optional_sql
                ORDER BY a.coy_id,a.trans_date,a.trans_id,a.line_num 
                OFFSET ? ROWS FETCH NEXT ? ROWS ONLY;";
            $result2 = db('pg_cherps')->select($sql2, [$coy, $from, $skip, $take]);

            $next_uri = (($skip/$take +1)*$take < $result1[0]->cnt) ? "?from=$from&take=$take$optional_uri&skip=" . ($skip/$take +1) : "";
            return $this->format_return($result1[0]->cnt, $next_uri, $result2);
        }

        return $this->format_return();
    }

    public function product_spiff(){
        $input = request()->only(['skip','take','from','to']);
        $take = (isset($input['take']) && $input['take']<1000) ? $input['take'] : 500;
        $skip = (isset($input['skip'])) ? $input['skip'] * $take : 0;
        $from = $input['from'] ?? date('Y-m-01');
        $to = $input['to'] ?? date('Y-m-t');
        $coy = 'CTL';

        $sql1 = "SELECT count(*) cnt
                FROM sms_product_spiff a 
                    left join ims_item_list b on a.coy_id = b.coy_id and a.item_id = b.item_id
                WHERE a.coy_id = ? and a.eff_from between ? and ? ";
        $result1 = db('pg_cherps')->select($sql1, [$coy, $from, $to]);

        if ($result1) {
            $sql2 = "SELECT trim(a.coy_id) as coy_id, a.line_num,trim(a.item_id) as item_id,
                    a.eff_from,a.eff_to,trim(a.loc_id) as loc_id,
                    a.qty_from,a.qty_to,a.incentive_point,trim(a.incentive_ind) as incentive_ind,
                    a.created_by,a.created_on,a.modified_by,a.modified_on,
                    trim(case when b.brand_id is null then '' else b.brand_id end)  as brand_id
                FROM sms_product_spiff a 
                    left join ims_item_list b on a.coy_id = b.coy_id and a.item_id = b.item_id
                WHERE a.coy_id = ? and a.eff_from between ? and ? 
                ORDER BY coy_id,line_num,item_id OFFSET ? ROWS FETCH NEXT ? ROWS ONLY;";
            $result2 = db('pg_cherps')->select($sql2, [$coy, $from, $to, $skip, $take]);

            $next_uri = (($skip/$take +1)*$take < $result1[0]->cnt) ? "?from=$from&to=$to&take=$take&skip=" . ($skip/$take +1) : "";
            return $this->format_return($result1[0]->cnt, $next_uri, $result2);
        }

        return $this->format_return();
    }

    private function format_return($total=0,$next_uri='',$results=[]) {

        if ($total==0) {
            $next_uri = '';
            $results = [];
        }

        return [
            "summary" => [
                "total" => $total,
                "count" => count($results),
                "next_uri" => $next_uri,
            ],
            "results" => $results
        ];
    }
}