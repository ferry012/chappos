<?php

/*
 * When setting up new Queue:
 *  1. Create new class for the queue and update in routes\web.php
 *  2. At __invoke: get all required data from DB
 *  3. At executeQueueStepByStep: outline the steps to be performed
 *  4. At _executeAndLog, log the results
 */

namespace App\Http\Controllers\Queue;

use App\Core\Orders\Models\Order;
use App\Http\Controllers\Controller;
use App\Http\Resources\Orders\OrderReceiptResource;
use App\Services\EmailService;
use App\Support\Carbon;
use App\Support\Str;
use Illuminate\Http\Request;
use App\Repositories\PaygatewayRepository;
use App\Support\Guzzle\ChvoicesClient;

class HachiOrderQueue extends Controller
{
    private $id, $email, $order, $order_log, $paygatewayRepo;

    public function __invoke(Request $request, PaygatewayRepository $paygatewayRepo)
    {
        $this->paygatewayRepo = $paygatewayRepo;

        $this->id = $request->get('trans_id');
        $this->email = $request->get('email');

        $this->order = Order::where('order_num', $this->id)->orWhere('invoice_id', $this->id)->withoutGlobalScope('open')->firstOrFail()->load([
            'lines' => function ($q) {
                return $q->when(request('exclude_voided', true), function ($q) {
                    return $q->active();
                })->orderBy('line_num');
            },
            'transactions',
            'addresses',
        ]);

        $this->order_log = $this->__getOrderLog($this->order->id);
        $this->queue_status = '1';

        // Execute the queue
        $steps_log = $this->executeQueueStepByStep();

        $response = [
            'code'  => $this->queue_status,
            'msg'   => "$this->id Queue executed",
            'steps' => array_values(array_filter($steps_log)),
        ];

        return $response;
    }

    private function executeQueueStepByStep()
    {

        /*
         * For every callback function in _executeAndLog, must return following array:
         *  [ (true/false result) , (message to save to log) ]
         */

        $chvoicesClient = new ChvoicesClient();

        $steps_log[] = $this->_executeAndLog('00_QUEUE_START', function ($step) {
            return [true, "Start processing queue steps for $this->id"];
        }, true);


        $steps_log[] = $this->_executeAndLog('01_POST_CHERPS', function ($step) {
            // Post order to CHERPS (same function for POS & SMS)
            $order = $this->order;
            $order->UpdateTaxAmount();

            $posting = app('api')->orders()->createSaleInvoice($order);
            if ($posting)
                return [true, "Invoice created in Cherps"];
            else
                return [false, "Fail to create invoice in Cherps"];
        });

        if (!$this->order->isGuestPurchase()) {
            $steps_log[] = $this->_executeAndLog('10_POST_VC', function ($step) {
                // Post order to VC
                $order = $this->order;
                $order->order_type = 'HI';
                $data = new OrderReceiptResource($order);
                $response = app('api')->members()->sendTransaction($order->order_num, $data);

                if ($response && $response->status_code === 200) {
                    return [true, "Successfully recorded in VC Member Transaction"];
                } else {
                    $msg = (isset($response->message)) ? $response->message : "Unknown reason";
                    return [false, "Fail to save to VC - $msg"];
                }
            });
        }

        //
        // Only if HACHI Orders
        //
        if (in_array($this->order->order_type, ['HO', 'HQ', 'HI'])) {

            $steps_log[] = $this->_executeAndLog('02_EMAIL_INVOICE', function ($step) {
                // Send Email - Only if SMS
                $api_result = $this->sendEmarsysOrderEmail($step, $this->id, [
                    "source"   => 'queue',
                    "trans_id" => $this->id,
                    "email"    => $this->email,
                    "data"     => [],
                ]);

                if ($api_result) {
                    $api_json = json_decode($api_result);
                    if ($api_json && $api_json->status_code === 200) {
                        $api_id = $api_json->data->activity_id;
                        return [true, "Invoice Email sent - activity_id: $api_id"];
                    } else {
                        $msg = (isset($api_json->message) && isset($api_json->message->response)) ? $api_json->message->response." (".$api_json->message->activity_id.")" : "Unknown reason";
                        return [false, "Invoice Email failed - $msg"];
                    }
                } else {
                    // Return unknown error
                    return [false, "Invoice Email failed - Unknown reason"];
                }
            });

            // Generate HPHCL/HPDSH PO
            $steps_log[] = $this->_executeAndLog('31_GENERATE_PO', function ($step) use ($chvoicesClient) {
                $resp = $chvoicesClient->triggerPoGenerate($this->id);

                if ($resp && isset($resp->orders) && count($resp->orders) > 0) {
                    $msg = "PO Generated - ";
                    if (isset($resp->orders[0]->messages)) {
                        foreach ($resp->orders[0]->messages as $k => $m) {
                            $msg .= $k." ";
                        }
                    } else {
                        $msg .= "Please see Cherps for info";
                    }
                    return [true, $msg];
                } else {
                    // No PO required
                    return [false, null];
                }
            });
        }

        $hasMembershipCard = false;
        $hasSsewPurchased = false;
        $hasEgiftPurchased = false;
        $hasGiftCardBlackhawk = false;
        $hasGiftCardRazer = false;

        //
        // CHECK EACH ITEMS
        //
        foreach ($this->order->lines as $line) {

            if ($line->item_type == 'M') { // (Str::startsWith($line->item_id, '!MEMBER-')) {
                $hasMembershipCard = true;
            }

            if ($line->item_type == 'W') {
                $hasSsewPurchased = true;
            }

            if ($line->item_type == 'G' || $line->item_type == 'H') {
                $dim = $line->custom_data['dimensions'];
                if ($dim && $dim['inv_dim4']=='RAZER GOLD'){
                    // Razer items
                    $hasGiftCardRazer = true;
                }
                else {
                    // Blackhawk items
                    $hasGiftCardBlackhawk = true;
                }
            }

            if ($line->item_type == 'E') {
                // E-GIFT
                $hasEgiftPurchased = true;
                $steps_log[] = $this->_executeAndLog('40_EGIFT_ACTIVATE', function ($step) use ($line) {
                    $form = $line->custom_data['form'];
                    $api_result = $this->activateEgift($step, $this->id, [
                        "coy_id"          => "CTL",
                        "coupon_id"       => "!EGIFT-20",
                        "trans_id"        => $this->id,
                        "buyer_id"        => $this->order->customer_id ?? '',
                        "buyer_name"      => $form['sender_name'],
                        "recipient_name"  => $form['recipient_name'],
                        "recipient_email" => $form['recipient_email'],
                        "gift_message"    => $form['gift_message'],
                        "gift_image"      => $form['gift_image'],
                        "trans_qty"       => $line->qty_ordered,
                        "trans_amount"    => $line->qty_ordered
                    ]);

                    if ($api_result) {
                        $api_json = json_decode($api_result);
                        if ($api_json && $api_json->status_code === 200) {
                            return [true, $api_json->info];
                        } else {
                            return [false, "Fail to generate egift"];
                        }
                    } else {
                        return [false, "Fail to generate egift - no response"];
                    }
                    return ($api_result) ? [true, $api_result] : [false, "Egift Fail"];
                });
            }

        }

        if ($hasGiftCardBlackhawk) {
            // Blackhawk Giftcards
            $steps_log[] = $this->_executeAndLog('41_BLACKHAWK', function ($step) use ($chvoicesClient) {
                $resp = $chvoicesClient->triggerItemsActivation($this->id);
                if ($resp) {
                    if ($resp->code == 1) {

                        $mailstream = 'SEND_SPOTIFY_EDM';
                        $api_result = $this->sendEmarsysEdm($step, $mailstream, $this->id, [
                            "source"   => 'queue',
                            "trans_id" => $this->id,
                            "email"    => $this->email,
                            "data"     => [
                                "r_card_value" => $resp->message[0]->product,
                                "r_claim_code" => $resp->message[0]->card_no
                            ]
                        ]);

                        return [true, "Blackhawk Cards activation success - ".json_encode($resp->stats)];
                    } else {
                        return [false, "Blackhawk Cards activation failed - ".json_encode($resp->stats)];
                    }
                } else {
                    return [false, "Blackhawk Cards activation failed - no response"];
                }
            });
        }

        if ($hasGiftCardRazer) {
            // Activate Razer Gold
            $steps_log[] = $this->_executeAndLog('41_RAZERGOLD', function ($step) use ($chvoicesClient) {
                $resp = $chvoicesClient->triggerRazergoldActivation($this->id);
                if ($resp) {
                    if ($resp->code == 1) {
                        return [true, "Razer Cards activation success - ".json_encode($resp->stats)];
                    } else {
                        return [false, "Razer Cards activation failed - ".json_encode($resp->stats)];
                    }
                } else {
                    return [false, "Razer Cards activation failed - no response"];
                }
            });
        }

        if ($hasSsewPurchased) {
            // SSEW Registration - Weekly export csv to Incall
        }

        if ($hasGiftCardBlackhawk || $hasGiftCardRazer || $hasEgiftPurchased){
            // Send CS Notification
            $this->sendGiftCardNotificationMail();
        }

        if ($hasMembershipCard && setting('pos.enable_ssew_mailing', false)) {
            $startDate = Carbon::createFromFormat('Y-m-d', '2021-11-01');
            $endDate = Carbon::createFromFormat('Y-m-d', '2021-12-31');

            if (Carbon::now()->between($startDate, $endDate)) {
                $steps_log[] = $this->_executeAndLog('51_EMAIL_ST_WARR', function ($step) {
                    // Send Email - Only if SMS
                    $api_result = $this->sendEmarsysStarWarrantyRequest($step, $this->id, [
                        "source"   => 'queue',
                        "trans_id" => $this->id,
                        "email"    => $this->email,
                        "data"     => [
                            "r_FirstName" => $this->order->customer_name,
                        ],
                    ]);

                    if ($api_result) {
                        $api_json = json_decode($api_result);
                        if ($api_json && $api_json->status_code === 200) {
                            $api_id = $api_json->data->activity_id;

                            return [true, "Star warranty Email sent - activity_id: $api_id"];
                        } else {
                            $msg = (isset($api_json->message) && isset($api_json->message->response)) ? $api_json->message->response." (".$api_json->message->activity_id.")" : "Unknown reason";

                            return [false, "Star warranty Email failed - $msg"];
                        }
                    }

                    // Return unknown error
                    return [false, "Star warranty Email failed - Unknown reason"];

                });
            }
        }

        if (setting('pos.enable_mail_intel_day_21', false)) {
            $steps_log[] = $this->_executeAndLog('52_EMAIL_INDAY_21', function ($step) {

                $isSend = (new EmailService)->issueIntelGameCode($this->order);

                if ($isSend > -1) {

                    if ($isSend) {
                        return [true, "Intel day 21 Email sent"];
                    }

                    return [false, "Intel day 21 Email failed"];
                }

                return [true, "Intel day 21 not qualified"];
            });
        }

        // Subscription welcome/activation email
        $subscriptions = $this->order->lines->filter(function ($line) {
            return $line->getCustomData('is_subscription', false);
        });

        if ($subscriptions->isNotEmpty()) {
            $subscription = $subscriptions->first();

            if ($product = $subscription->product) {
                $brandId = trim($product->brand_id);

                if ($brandId === 'STORYTEL') {
                    $mailStream = $brandId;
                    $steps_log[] = $this->_executeAndLog('53_' . $mailStream, function ($step) use ($mailStream, $subscription) {

                        $api_result = $this->sendEmarsysEdm($step, 'SUBS_' . $mailStream, $this->id, [
                            "source"   => 'queue',
                            "trans_id" => $this->id,
                            "email"    => $this->email,
                            "data"     => [
                                "r_FirstName"  => $this->order->customer_name,
                                "r_activation" => $subscription->getCustomData('activation_link')
                            ]
                        ]);

                        if ($api_result) {
                            $api_json = json_decode($api_result);

                            if ($api_json && $api_json->status_code === 200) {
                                $api_id = $api_json->data->activity_id;
                                db()->table('o2o_subscription_list')->where('order_id', $this->order->id)->where('plan_id', $subscription->item_id)->update(['status_level' => 1]);

                                return [true, "Subscription - $mailStream email sent - activity_id: $api_id"];
                            }

                            $msg = (isset($api_json->message) && isset($api_json->message->response)) ? $api_json->message->response . " (" . $api_json->message->activity_id . ")" : "Unknown reason";

                            return [false, "Subscription - $mailStream email failed - $msg"];
                        }

                        return [false, "Subscription - $mailStream email failed"];
                    });
                }
            }
        }

        return $steps_log;

    }

    private function _executeAndLog($step, $callback, $allowRerun = false)
    {

        // Check if this step has run before
        if (!$allowRerun) {
            if (!empty($this->order_log->get($step))) {
                return "";
            }
        }

        // Execute function
        $resp = $callback($step);

        if ($resp[0]) {
            $this->__putOrderLog($this->order->id, $step, [
                'message_body' => $resp[1],
                'status_level' => 1,
            ]);

            return $resp[1];
        } else {
            if ($resp[1]) {
                $this->queue_status = '0';

                $this->__putOrderLog($this->order->id, $step, [
                    'message_body' => $resp[1],
                    'status_level' => 0,
                ]);
            }

            return "";
        }

    }

    private function __putOrderLog($order_id, $step_id, $data, $by = 'QUEUE')
    {
        $insertData = [
            'order_id'     => $order_id,
            'step_id'      => $step_id,
            'message_body' => $data['message_body'] ?? '',
            'status_level' => $data['status_level'] ?? 0,
            'custom_data'  => (isset($data['custom_data'])) ? json_encode($data['custom_data']) : '{}',
            'modified_by'  => $by,
            'modified_on'  => date('Y-m-d H:i:s'),
        ];
        db()->table('o2o_order_log')->where('order_id', $order_id)->where('step_id', $step_id)->insert($insertData);

        return $step_id;
    }

    private function __getOrderLog($order_id, $step_id = '')
    {
        $collection = db()->table('o2o_order_log')
            ->where('order_id', $order_id)
            ->where('status_level', 1)
            ->when(($step_id != ''), function ($q) use ($step_id) {
                $q->where('step_id', $step_id);
            })->orderBy('created_on')->get();

        return ($collection->count() > 0) ? $collection->keyBy('step_id') : $collection;
    }

    private function sendEmarsysEdm($step, $mailstream, $id, $postdata) {

        $endpoint = env('API_URL_VALUECLUB');
        $endpoint .= 'api/emarsys/' . $mailstream;
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->post($endpoint, [
            'headers' => ['Content-Type' => 'application/json'],
            'body'    => json_encode($postdata),
        ]);
        $content = $response->getBody()->getContents();

        // Log
        $this->paygatewayRepo->api_log([
            "api_client"      => 'emarsys',
            "api_description" => 'EDM_'.substr(strtoupper($mailstream),0,8).'_'.trim($id),
            "request_method"  => 'POST',
            "request_url"     => $endpoint,
            "request_header"  => '{}',
            "request_body"    => json_encode($postdata),
            "request_on"      => date('Y-m-d H:i:s'),
            "response_header" => '{}',
            "response_body"   => $content,
            "response_on"     => date('Y-m-d H:i:s'),
            "error_msg"       => '',
            "created_on"      => date('Y-m-d H:i:s'),
        ]);

        return $content;

    }

    private function sendEmarsysOrderEmail($step, $id, $postdata)
    {

        //$endpoint = (app()->environment('production')) ? 'https://vc8.api.valueclub.asia/' : 'https://chvc-api.sghachi.com/';
        $endpoint = env('API_URL_VALUECLUB');
        $endpoint .= 'api/emarsys/HACHI_ORDERCOMPLETE';
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->post($endpoint, [
            'headers' => ['Content-Type' => 'application/json'],
            'body'    => json_encode($postdata),
        ]);
        $content = $response->getBody()->getContents();

        // Log
        $this->paygatewayRepo->api_log([
            "api_client"      => 'emarsys',
            "api_description" => 'EMAIL_'.trim($id),
            "request_method"  => 'POST',
            "request_url"     => $endpoint,
            "request_header"  => '{}',
            "request_body"    => json_encode($postdata),
            "request_on"      => date('Y-m-d H:i:s'),
            "response_header" => '{}',
            "response_body"   => $content,
            "response_on"     => date('Y-m-d H:i:s'),
            "error_msg"       => '',
            "created_on"      => date('Y-m-d H:i:s'),
        ]);

        return $content;

    }

    private function sendEmarsysStarWarrantyRequest($step, $id, $postdata)
    {

        //$endpoint = (app()->environment('production')) ? 'https://vc8.api.valueclub.asia/' : 'https://chvc-api.sghachi.com/';
        $endpoint = env('API_URL_VALUECLUB');
        $endpoint .= 'api/emarsys/STAR_WARRANTY_0801';
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->post($endpoint, [
            'headers' => ['Content-Type' => 'application/json'],
            'body'    => json_encode($postdata),
        ]);
        $content = $response->getBody()->getContents();

        // Log
        $this->paygatewayRepo->api_log([
            "api_client"      => 'emarsys',
            "api_description" => 'EMAIL_'.trim($id),
            "request_method"  => 'POST',
            "request_url"     => $endpoint,
            "request_header"  => '{}',
            "request_body"    => json_encode($postdata),
            "request_on"      => date('Y-m-d H:i:s'),
            "response_header" => '{}',
            "response_body"   => $content,
            "response_on"     => date('Y-m-d H:i:s'),
            "error_msg"       => '',
            "created_on"      => date('Y-m-d H:i:s'),
        ]);

        return $content;

    }

    private function activateEgift($step, $id, $postdata)
    {

        //$endpoint = (app()->environment('production')) ? 'https://vc8.api.valueclub.asia/' : 'https://chvc-api.sghachi.com/';
        $endpoint = env('API_URL_VALUECLUB');
        $endpoint .= 'api/egifts/insert';
        $endpoint .= '?key_code=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQxMzRiYTRl';
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->post($endpoint, [
            'headers' => ['Content-Type' => 'application/json'],
            'body'    => json_encode($postdata),
        ]);
        $content = $response->getBody()->getContents();

        // Log
        $this->paygatewayRepo->api_log([
            "api_client"      => 'emarsys',
            "api_description" => 'EGIFT_'.trim($id),
            "request_method"  => 'POST',
            "request_url"     => $endpoint,
            "request_header"  => '{}',
            "request_body"    => json_encode($postdata),
            "request_on"      => date('Y-m-d H:i:s'),
            "response_header" => '{}',
            "response_body"   => $content,
            "response_on"     => date('Y-m-d H:i:s'),
            "error_msg"       => '',
            "created_on"      => date('Y-m-d H:i:s'),
        ]);

        return $content;

    }

    protected function sendGiftCardNotificationMail()
    {
        $gifts = $this->order->lines->whereIn('item_type', ['E', 'G', 'H']);

        if ($gifts->isNotEmpty()) {

            $gifts = $gifts->map(function ($gift) {
                $gift->remark = '';

                if ($gift->item_type === 'E') {
                    $form = $gift->getCustomData('form');

                    if ($form && isset($form['recipient_email'])) {
                        $gift->remark = 'To email: '.$form['recipient_email'];
                    }

                }

                return $gift;
            });

            $content = view('mails.gift-card', ['order' => $this->order, 'gifts' => $gifts])->render();
            ctl_mail('HQ.CustServiceGroup@challenger.sg', 'Hachi eGift/ Razer Gold/ Xbox card transactions @ '.$this->order->invoice_id.' '.now()->toDatetimeString(), $content, ['cc' => 'chengyen@challenger.sg;guo.zhiming@challenger.sg']);
        }

        return null;
    }

}