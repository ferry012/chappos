<?php

namespace App\Http\Controllers\Products;


use App\Http\Controllers\Controller;
use App\Http\Resources\AbstractCollection;
use App\Http\Resources\Products\PriceCollection;
use App\Http\Resources\Products\PriceResource;
use App\Http\Resources\Products\ProductGroupCollection;
use App\Http\Resources\Products\ProductResource;
use App\Http\Resources\Products\ProductCollection;
use App\Http\Resources\Products\ProductStockCollection;
use App\Support\Carbon;
use App\Support\Str;
use Flugg\Responder\Responder;
use Illuminate\Http\Request;
use App\Http\Controllers\Products\Search;

class ProductController extends Controller
{
    private $debug_timer = [];
    
    public function show(Request $request, $id)
    {
        $this->debug_timer = [];
        $this->debug_timer['show.start'] = time();

        $id = urldecode($id);

        $pwp     = [];
        $product = app('api')->products()->getById($id);
        $this->debug_timer['show.getById'] = time();

        if (!$product) {

            $product = app('api')->products()->getByAltId($id);
            $this->debug_timer['show.getByAltId'] = time();

            if (!$product) {
                //return $this->errorNotFound();
                return $this->respondWithArray(['error' => 'No item found']);
            }

        }

        // temperately get the discount percentage
//        $itemPrice = db()->table('ims_item_price')->where('item_id', $id)->where('price_type', 'MEMBER')->where(function ($q) {
//            $today = today()->toDateTimeString();
//            $q->where('eff_from', '<=', $today)->where('eff_to', '>=', $today);
//        })->orderBy('eff_to')->first();
//
//        $product->disc_percent = .0;
//
//        if ($itemPrice && $itemPrice->disc_percent > 0) {
//            $product->disc_percent = (float)$itemPrice->disc_percent;
//        }

        if ($product->allow_discount === 'N' && $request->get('customer_group') === 'staff') {
            return $this->errorNotFound(trans('response.error.item_not_allowed', ['name' => 'staff']));
        }

        $resource = new ProductResource($product);
        $productExtWar = $product->ext_warranty;
        $productPrices = $resource->getPrices();
        $this->debug_timer['show.getPrices'] = time();

        $pwpData  = $this->purchaseWithPurchase($request, $id, $productPrices, $productExtWar);
        $this->debug_timer['show.purchaseWithPurchase'] = time();

        if ($pwpData instanceof AbstractCollection) {
            $pwp = $pwpData;
        }

        $result = $resource->only($request->get('fields'))->append(['pwp' => $pwp]);
        $this->debug_timer['show.resource-append'] = time();

        if ($request->get('debug_timer'))
            return $this->debug_timer;
        else
            return $result;
    }

    public function ssewPrice(Request $request, $id){
        $unit_price = $request->get('unit_price', 0);
        $productPrices = [];
        if ($unit_price > 0) {
            $productPrices['unit_price'] = $unit_price;
        }
        $products = app('api')->products()->genPwpSsew($id,$productPrices);

        $resource = null;
        foreach($products as $p){
            if ($p->item_id=='SSEW')
                $resource = new ProductResource($p);
        }
        return $resource;
    }

    public function purchaseWithPurchase(Request $request, $id, $productPrices = [], $productExtWar = null)
    {

        $this->debug_timer['purchaseWithPurchase.start'] = time();

        if (Str::upper(request('customer_group')) === 'STAFF') {
            // Staff purchase no PWP
            return;
        }

        $options = [
            'into_group'  => $request->get('pwp_group', false),
            'free_on_top' => $request->get('pwp_free_top', true),
        ];

        $locationId = $request->get('loc_id');

        $products = app('api')->products()->getPurchaseWithPurchase($id, $locationId, $options, $productPrices, $productExtWar);

        $this->debug_timer['purchaseWithPurchase.getPurchaseWithPurchase'] = time();

        if ($products->isEmpty()) {
            return $this->errorNotFound();
        }

        if ($options['into_group']) {
            return new ProductGroupCollection($products);
        }

        return new ProductCollection($products);
    }

    public function complimentary($id)
    {
        $products = app('api')->products()->getComplementaries($id);

        if ($products->isEmpty()) {
            return $this->errorNotFound();
        }

        return new ProductCollection($products);
    }

    public function stock(Request $request, $id)
    {
        $locId = $request->get('locId');

        $locIds = explode(',', $locId);

        if (count($locIds) > 1) {
            $locId = $locIds;
        }

        $stocks = app('api')->products()->getStock($id, $locId);

        return new ProductStockCollection($stocks);
    }

    public function priceTag(Request $request, Responder $responder, $ids)
    {

        $ids       = explode(',', $ids);
        $ids       = array_unique($ids);
        $priceDate = $request->get('priceDate', today()->toDateTimeString());
        $prices    = [];

        foreach ($ids as $id) {

            $row['item_id']       = $id;
            $row['regular_price'] = .0;
            $row['mbr_price']     = .0;
            $row['disc_percent']  = .0;

            $itemPrices = db()->table('ims_item_price')->where('coy_id', 'CTL')->where('item_id', $id)->whereIn('price_type', [
                'REGULAR', 'MEMBER',
            ])
                ->where(function ($q) use ($priceDate) {
                    $q->where('eff_from', '<=', $priceDate)->where('eff_to', '>=', $priceDate);
                })->orderBy('eff_to')->get();

            if ($itemPrices->isEmpty()) {
                continue;
            }

            $regularPriceRow = $itemPrices->where('price_type', 'REGULAR')->first();

            if ($regularPriceRow) {
                $row['regular_price'] = (float)$regularPriceRow->unit_price;
            }

            $memberPriceRow = $itemPrices->where('price_type', 'MEMBER')->first();

            $row['mbr_price'] = $row['regular_price'];

            if ($memberPriceRow) {
                $row['disc_percent'] = (float)$memberPriceRow->disc_percent;
                $row['mbr_price']    = (float)$memberPriceRow->unit_price;
            }

            $masterItem = db()->table('ims_item_list')->where('item_id', $id)->first();

            if ($masterItem && in_array($masterItem->price_tag_bc, ['P', 'Q'], false)) {
                $promoItem = app('api')->products()->getCheapPromotionalItem($id);

                if ($promoItem) {
                    $row['disc_percent'] = .0;
                    $row['mbr_price']    = (float)$promoItem->promo_price;
                }
            }

            $resource = new PriceResource((object)$row);

            $prices[] = $resource->toArray($request);
        }

        return $responder->success($prices);
    }

    public function priceChangeTag(Request $request, Responder $responder)
    {
        $inDate = $request->get('priceDate', now()->toDateString());
        $inDate = Carbon::parse($inDate)->toDateString();

        $items = db()->select('select * from get_price_change(?)', [$inDate]);

        if ($items) {
            $items = collect($items)->map(function ($item) use ($request) {
                return (new PriceResource($item))->toArray($request);
            })->toArray();
        }

        return $responder->success($items);
    }
}