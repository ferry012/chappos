<?php

namespace App\Http\Controllers\Products;


use App\Http\Controllers\Controller;
use App\Http\Resources\AbstractCollection;
use App\Support\Carbon;
use App\Support\Str;
use Flugg\Responder\Responder;
use Illuminate\Http\Request;
use App\Http\Controllers\Products\Search;

class StarRedemption extends Controller
{
    public function catalog(Request $request, Responder $responder)
    {
        $coy = 'CTL';
        $loc = $request->get('loc_id', 'BF');

        $blockedLoc = setting('pos.sredeem_blocked', '');
        if ($blockedLoc!='' && in_array($loc, explode(',',$blockedLoc))) {
            return $responder->success([]);
        }

        $loc_id = trim($loc) . '-S';

        $sql = "select a.item_id, a.item_desc, a.unit_price regular_price, a.points_required/100 unit_price, a.discount_amt disc_percent, a.image_name, 
                    b.loc_id, (b.qty_on_hand-b.qty_reserved) qty_available,
                    a.item_control
                from crm_redemption_list a
                join v_ims_inv_summary b on a.coy_id=b.coy_id and a.item_id=b.item_id
                where a.coy_id = ? and b.loc_id = ? and a.eff_from < current_timestamp and a.eff_to >= current_timestamp
                order by unit_price desc";
        $items = db('pg_cherps')->select($sql, [$coy, $loc_id]);

        if ($items) {

            $member = $request->get('mbr_id', '');
            $rebate = $request->get('rebates', 0);

            $items = collect($items)->filter(function ($value, $key) use ($rebate,$member) {
                if ($rebate > 0 && $rebate < $value->unit_price) {
                    // Rebate amount is provided. Filter away items not eligible
                    return false;
                }
                if (!empty($member) && !empty($value->item_control)) {
                    // Only if item has certain rules
                    $item_control = json_decode($value->item_control);
                    if (is_object($item_control) && count((array)$item_control)>0) {
                        if (!$this->getPurchase($member, $item_control)) {
                            return false;
                        }
                        if (isset($item_control->redemption_limit)) {
                            if ($this->redeemedBefore($member, $item_control->redemption_limit, trim($value->item_id))) {
                                return false;
                            }
                        }
                    }
                }
                return true;
            })->map(function ($item) {
                $item->item_id = trim($item->item_id);
                $item->image_name = get_constant('s3.product_images_thumb') . trim($item->image_name);
                $item->regular_price = floatval($item->regular_price);
                $item->unit_price = floatval($item->unit_price);
                $item->loc_id = trim($item->loc_id);
                $item->qty_available = intval($item->qty_available);
                unset($item->item_control);
                return (array) $item;
            })->values()->toArray();
        }

        return $responder->success($items);
    }

    private function redeemedBefore($mbr_id, $control, $item_id) {

        $sql = "select coalesce(sum(qty_invoiced),0) total_qty
                    from sms_invoice_list a
                    join sms_invoice_item b on a.coy_id=b.coy_id and a.invoice_id=b.invoice_id
                    join ims_item_list c on b.coy_id=c.coy_id and b.item_id=c.item_id
                    where a.coy_id='CTL' and a.inv_type in ('OD') and a.status_level>0
                        and a.cust_id = ? and b.item_id = ? and a.invoice_date between ? and ? ";
        $result = db('pg_cherps')->select($sql, [$mbr_id, $item_id, $control->eff_from, $control->eff_to]);

        return ($result && $result[0]->total_qty >= $control->limit);
    }

    private function getPurchase($mbr_id, $control) {

        $sql_conditions = "";
        if (isset($control->brand_id)) {
            $sql_conditions.= " c.brand_id IN ('".implode("','",$control->brand_id)."') ";
        }
        if (isset($control->item_id)) {
            if ($sql_conditions != "")
                $sql_conditions.= " OR ";
            $sql_conditions.= " c.item_id IN ('".implode("','",$control->item_id)."') ";
        }
        if (isset($control->inv_dim4)) {
            if ($sql_conditions != "")
                $sql_conditions.= " OR ";
            $sql_conditions.= " c.inv_dim4 IN ('".implode("','",$control->inv_dim4)."') ";
        }

        if ($sql_conditions == "") {
            // Missing conditions
            return true;
        }

        $sql = "select sum(trans_qty) total_qty from (
                    select coalesce(sum(trans_qty),0) trans_qty
                    from pos_transaction_list a
                    join pos_transaction_item b on a.coy_id=b.coy_id and a.trans_id=b.trans_id
                    join ims_item_list c on b.coy_id=c.coy_id and b.item_id=c.item_id
                    where a.coy_id='CTL' and a.status_level>0 and a.trans_date::date >= current_timestamp - interval '90 days'
                      and a.mbr_id='$mbr_id' and ($sql_conditions)
                    union all
                    select coalesce(sum(qty_invoiced),0) trans_qty
                    from sms_invoice_list a
                    join sms_invoice_item b on a.coy_id=b.coy_id and a.invoice_id=b.invoice_id
                    join ims_item_list c on b.coy_id=c.coy_id and b.item_id=c.item_id
                    where a.coy_id='CTL' and a.inv_type in ('HI','HR','HX') and a.status_level>0 and a.invoice_date::date >= current_timestamp - interval '90 days'
                      and a.cust_id='$mbr_id' and ($sql_conditions)
                ) tbl";
        $result = db('pg_cherps')->select($sql);

        return ($result && $result[0]->total_qty > 0);
    }

}