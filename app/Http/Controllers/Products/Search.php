<?php

namespace App\Http\Controllers\Products;

use App\Core\Product\Models\Product;
use App\Http\Controllers\Controller;
use App\Http\Resources\Products\ProductCollection;
use App\Support\Str;

class Search extends Controller
{
    protected $keyword          = '';

    public function __invoke($mode = '', $keyword = '')
    {

        if (empty($keyword)) {
            // products/search/{mode}
            $this->keyword = $mode;

            if (is_numeric($this->keyword)) {
                $scope = $this->getScopes()['ITEM'];
                list('sql' => $sql, 'bindings' => $bindings, 'order' => $order, 'seq' => $seq) = $scope;

                $products = Product::where('coy_id','CTL')->whereRaw($sql, $bindings)->orderBy($order, $seq)->limit(50)->get();
            }

            if (is_numeric($this->keyword) && (!isset($products) || $products->isEmpty())) {
                // Maybe its alt_id
                $alt_list = db()->table('ims_item_alt_id')->where('coy_id', coy_id())->whereRaw("upper(alt_id) like upper(?)", ["%$this->keyword%"])
                    ->orderBy('created_on', 'DESC')
                    ->first();

                if (isset($alt_list)) {
                    $products = Product::where('coy_id','CTL')->whereRaw("upper(item_id) like upper(?)", ["%". trim($alt_list->item_id) ."%"])->orderBy('modified_on', 'DESC')->limit(50)->get();
                }
            }

            if (!isset($products) || $products->isEmpty()) {
                // Search again by DESC
                $scope     = $this->getScopes()['DESC'];
                list('sql' => $sql, 'bindings' => $bindings, 'order' => $order, 'seq' => $seq) = $scope;
                
                $products = Product::where('coy_id','CTL')->whereRaw($sql, $bindings)->orderBy($order,$seq)->limit(50)->get();
            }

        }
        else {
            // products/search/{mode}/{keyword}
            $this->keyword = $keyword;

            $scopeName = Str::upper($keyword);
            $mode === 'genre' and isset($this->getScopes()[$scopeName]) and $scope = $this->getScopes()[$scopeName];
            list('sql' => $sql, 'bindings' => $bindings, 'order' => $order, 'seq' => $seq) = $scope;

            $products = Product::where('coy_id','CTL')->where('status_level','>=','0')->whereRaw($sql, $bindings)->orderBy($order,$seq)->limit(50)->get();
            
        }

        if ($products->isEmpty()) {
            //return $this->errorNotFound();
            return $this->respondWithArray(['error'=>'No item found']);
        }

        return new ProductCollection($products);
    }

    public function getScopes()
    {
        return [
            'MBR'   => ['sql' => 'item_type = ?', 'bindings' => 'M', 'order' => 'item_id', 'seq' => 'ASC'],
            'SSEW'  => ['sql' => 'item_type = ?', 'bindings' => 'W', 'order' => 'item_id', 'seq' => 'ASC'],
            'ARAE'  => ['sql' => 'item_type = ?', 'bindings' => 'A', 'order' => 'item_id', 'seq' => 'ASC'],
            'EGIFT' => ['sql' => 'item_type = ?', 'bindings' => 'G', 'order' => 'item_id', 'seq' => 'ASC'],
            'INK'   => ['sql' => 'item_id like ?', 'bindings' => '%INK%', 'order' => 'item_id', 'seq' => 'ASC'],
            'PB'    => ['sql' => 'item_id like ?', 'bindings' => 'PB-%', 'order' => 'item_id', 'seq' => 'ASC'],
            'BAG'   => ['sql' => 'lower(item_desc) like ?', 'bindings' => '%woven bag%', 'order' => 'item_id', 'seq' => 'ASC'],
            'REDONE'=> ['sql' => 'rtrim(brand_id) = ?', 'bindings' => 'REDONE', 'order' => 'model_id', 'seq' => 'ASC'],
            'ITEM'  => [
                'sql'      => 'upper(item_id) like upper(?)',
                'bindings' => ["%" . urldecode($this->keyword ). ""],
                'order'    => 'modified_on',
                'seq'      => 'DESC'
            ],
            'DESC'  => [
                'sql'      => 'lower(item_desc) like lower(?) or upper(item_id) like upper(?) or upper(brand_id) like upper(?)',
                'bindings' => ["%" . urldecode($this->keyword ). "%", "%" . urldecode($this->keyword ). "%", "%" . urldecode($this->keyword ). "%"],
                'order'    => 'modified_on',
                'seq'      => 'DESC'
            ],
        ];
    }
}