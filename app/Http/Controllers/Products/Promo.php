<?php

namespace App\Http\Controllers\Products;

use App\Core\Product\Models\Product;
use App\Http\Controllers\Controller;
use App\Http\Resources\Products\ProductCollection;
use App\Support\Str;
use App\Utils\Helper;
use Illuminate\Http\Request;

class Promo extends Controller
{

    public function __invoke($id)
    {
//        $cacheOpts = array_merge(["promoId" => $id]);
//        list($cacheTag, $cacheKey) = Helper::generateCacheKeys(['products', 'crm'], $productId, $cacheOpts);
//
//        $result = app('cache')->tags($cacheTag)->get($cacheKey);
//        if ($result) {
//            // Return from cache
//            return $result;
//        }

        $sql = "select *
                from crm_promo_list a
                join crm_promo_location b on a.coy_id=b.coy_id and a.promo_id=b.promo_id
                join crm_promo_price_ind c on a.coy_id=c.coy_id and a.promo_id=c.promo_id
                where a.coy_id = ? and a.promo_id = ? ";
        $promo_header = collect(db()->select($sql, ['CTL', $id]))->first();

        if (!$promo_header) {
            return $this->respondWithArray(['error'=>'No promo found']);
        }

        if ($promo_header->promo_type == 'PWP') {
            $main_item = request()->get('item_id','');
            if (empty($main_item)) {
                return $this->respondWithArray(['error'=>'Missing main item']);
            }

            $sql = "select coy_id,promo_id,promo_item item_id, promo_price unit_price,
                        limit_per_day,limit_per_member,limit_per_trans,limit_stock_main,limit_stock_promo
                     from crm_promo_item a where a.coy_id = ? and a.promo_id = ? and a.item_id = ? ";
            $promo_items = collect(db()->select($sql, ['CTL', $id, $main_item]));
        }
        else {
            $sql = "select coy_id,promo_id,item_id,unit_price,
                        limit_per_day,limit_per_member,limit_per_trans,limit_stock_main,limit_stock_promo 
                    from crm_promo_item a where a.coy_id = ? and a.promo_id = ? ";
            $promo_items = collect(db()->select($sql, ['CTL', $id]));
        }

        $product_raw = Product::where('coy_id','CTL')->whereIn('item_id', $promo_items->pluck('item_id')->toArray() )->limit(50)->get();
        $images_raw = db()->table('ims_product_image')->where('coy_id','CTL')->whereIn('item_id', $promo_items->pluck('item_id')->toArray() )->limit(50)->get();

        $products = $product_raw->map(function ($p) use($promo_header,$promo_items,$images_raw){
            $promo = $promo_items->where('item_id', $p->item_id)->first();
            $image = $images_raw->where('item_id', $p->item_id)->toArray();

            $return = [
                "item_id" => $p->item_id,
                "item_desc" => $p->item_desc,
                "short_desc" => $p->short_desc,
//                "image_name" => (!empty($p->image_name))  ? get_constant('s3.product_images_thumb') . trim($p->image_name) : "",
                "image_name" => (!empty($p->image_name))  ? get_constant('s3.product_images') . trim($p->image_name) : "",
                "item_type" => $p->item_type,
                "in_catalog" => $p->in_catalog,
                "lot_control" => $p->lot_control,
                "price_tag_bc" => $p->price_tag_bc,
                "prices" => [
                    "unit_price" => floatval($promo->unit_price),
                    "unit_price_ind" => $promo_header->price_ind,
//                    "unit_price": 399,
//                    "unit_price_ind": "",
//                    "price_savings": 0,
                    "regular_price" => floatval($p->regular_price),
                    "mbr_price" => floatval($p->mbr_price),
                    "promo_price" => floatval($promo->unit_price),
                    "promo_id" => $promo->promo_id,
                    "promo_ind" => $promo_header->price_ind,
//                    "promo_cost": 0,
//                    "disc_percent": 0,
//                    "cost_price": 279,
//                    "min_price": 0,
//                    "staff_price": 0,
//                    "customer_groups": [],
//                    "multiple_points": 1,
//                    "mpt_promo_id": "",
//                    "mpt_promo_by": "",
//                    "mpt_add_amount": 0,
//                    "unit_rebate_points": 0,
//                    "unit_rebate_amount": 0
                ],
                "images" => [],
                "limit" => [
                    "limit_per_day" => floatval($promo->limit_per_day),
                    "limit_per_member" => floatval($promo->limit_per_member),
                    "limit_per_trans" => floatval($promo->limit_per_trans),
                    "limit_stock_main" => floatval($promo->limit_stock_main),
                    "limit_stock_promo" => floatval($promo->limit_stock_promo)
                ]
            ];

            if (!empty($image)) {
                foreach($image as $im){
                    $return["images"][] = get_constant('s3.product_images') . $im->image_name;
                }
            }

            return (object) $return;
        });

//        if ($products) {
//            app('cache')->tags($cacheTag)->put($cacheKey, $products, now()->addSeconds($this->productCacheSeconds));
//        }

        if ($products->isEmpty()) {
            //return $this->errorNotFound();
            return $this->respondWithArray(['error'=>'No item found']);
        }

        return $products;
    }
}