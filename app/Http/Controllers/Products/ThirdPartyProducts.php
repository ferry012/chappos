<?php

namespace App\Http\Controllers\Products;


use App\Http\Controllers\Controller;
use App\Http\Resources\AbstractCollection;
use App\Support\Carbon;
use App\Support\Str;
use Flugg\Responder\Responder;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ThirdPartyProducts extends Controller
{

    private $coySettings;

    public function __construct() {
        $this->coySettings = [
            'KTP' => [
               'coy_id' => 'CTL',
                //'coy_id' => 'KTP',
                'inv_type' => 'KI',
                'inv_loc' => 'KTP',
                'loc_id' => 'KTP',
                'delv_mode_id' => 'NID',
                'check_loc' => ['KTP'],
                'limit_inv_dim2'=>['KINGDOM PC']
            ],
            'ITE' => [
                'coy_id' => 'ITZ',
                'inv_type' => 'TI',
                'inv_loc' => 'IT2',
                'loc_id' => 'IT2',
                'delv_mode_id' => 'NID',
                'check_loc' => ['IT2','IT4','VEN-C','DSH-C','IM-C','IT-C'],
                'limit_inv_dim2'=>[]
            ]
        ];
    }

    public function location($coy, $item_ids='', Request $request, Responder $responder){

        $coy = Str::upper($coy);
        $validator = Validator::make([
            'coy' => $coy
        ], [
            'coy' => [
                'required',
                Rule::in(array_keys($this->coySettings))
            ]
        ]);
        if ($validator->fails()) {
            $this->setStatusCode(400);
            return $this->respondWithError([
                "error" => $validator->errors($validator)
            ]);
        }


        $coy_id = $this->coySettings[$coy]['coy_id'];
        $loc_id = $this->coySettings[$coy]['inv_loc'];
        $loc_id_str = implode("','", $this->coySettings[$coy]['check_loc']);
        $inv_dim2_str = implode("','", $this->coySettings[$coy]['limit_inv_dim2']);
        $inv_dim2_query = "";
        if ($inv_dim2_str !=="") // 0889842905267 is win 11 home which is under inv_dim2 SOFTWARE 
            $inv_dim2_query = " and ( a.inv_dim2 in ('".$inv_dim2_str."') or a.item_id ='0889842905267' ) ";

        $flag_details = (Str::upper($request->get('details','')) == 'Y') ? true : false;
        $item_id = "'" . implode("','",explode(',', $item_ids)) . "'";
        $prices = [];


        if (!empty($item_ids)) {
            $sql = "select a.coy_id,'$loc_id' loc_id,trim(a.item_id) item_id,a.item_desc,
                       SUM(coalesce(b.qty_on_hand,0)) qty_on_hand,
                       SUM(coalesce(b.qty_reserved,0)) qty_reserved,
                a.item_type,a.inv_dim1,a.inv_dim2,a.inv_dim3,a.inv_dim4,a.brand_id,a.model_id,
                inv_dim5,inv_dim6,inv_dim7,inv_dim8,inv_dim9,inv_dim10,ext_warranty
                from ims_item_list a
                left join v_ims_inv_summary b on a.coy_id=b.coy_id and a.item_id=b.item_id and b.loc_id in ('".$loc_id_str."')
                where a.coy_id = ? and a.item_id in ($item_id)
                group by a.coy_id,a.item_id,a.item_type,a.inv_dim1,a.inv_dim2,a.inv_dim3,a.inv_dim4,a.brand_id,a.model_id, inv_dim5,inv_dim6,inv_dim7,inv_dim8,inv_dim9,inv_dim10,ext_warranty";
            $items = db('pg_cherps')->select($sql, [$coy_id]);

            if ($flag_details) {
                $sql_price = "select coy_id,item_id,price_type,unit_price from v_ims_price_summary where coy_id= ? and item_id in ($item_id)";
                $prices = db('pg_cherps')->select($sql_price, [$coy_id]);
                $prices = collect($prices);
            }
        }
        else {
            $sql = "select a.coy_id,'$loc_id' loc_id,trim(a.item_id) item_id,a.item_desc,
                            SUM(coalesce(b.qty_on_hand,0)) qty_on_hand,
                            SUM(coalesce(b.qty_reserved,0)) qty_reserved, 
                        a.item_type,a.inv_dim1,a.inv_dim2,a.inv_dim3,a.inv_dim4,a.brand_id,a.model_id,
                        inv_dim5,inv_dim6,inv_dim7,inv_dim8,inv_dim9,inv_dim10,ext_warranty
                from ims_item_list a
                left join v_ims_inv_summary b on a.coy_id=b.coy_id and a.item_id=b.item_id and b.loc_id in ('".$loc_id_str."')
                where a.coy_id = ? and a.item_type in ('I','A','W') ".$inv_dim2_query."
                group by a.coy_id,a.item_id,a.item_type,a.inv_dim1,a.inv_dim2,a.inv_dim3,a.inv_dim4,a.brand_id,a.model_id, inv_dim5,inv_dim6,inv_dim7,inv_dim8,inv_dim9,inv_dim10,ext_warranty";
            $items = db('pg_cherps')->select($sql, [$coy_id]);

            if ($flag_details) {
                $item_ids = collect($items)->pluck('item_id')->toArray();
                $item_id = "'" . implode("','",$item_ids) . "'";

                $sql_price = "select coy_id,trim(item_id) item_id,trim(price_type) price_type,unit_price from v_ims_price_summary where coy_id= ? and item_id in ($item_id)";
                $prices = db('pg_cherps')->select($sql_price, [$coy_id]);
                $prices = collect($prices);
            }
        }

        $data = collect($items)->map(function($x) use($flag_details,$prices){
            $r1 = [
                "coy_id"    => trim($x->coy_id),
                "loc_id"    => trim($x->loc_id),
                "item_id"   => trim($x->item_id),
                "item_desc" => $x->item_desc,
                "qty_available" => $x->qty_on_hand - $x->qty_reserved,
                "qty_on_hand"   => (int) $x->qty_on_hand,
                "qty_reserved"  => (int) $x->qty_reserved,

                "item_type"   => trim($x->item_type),
                "inv_dim1"   => trim($x->inv_dim1),
                "inv_dim2"   => trim($x->inv_dim2),
                "inv_dim3"   => trim($x->inv_dim3),
                "inv_dim4"   => trim($x->inv_dim4),
                "brand_id"   => trim($x->brand_id),
                "model_id"   => trim($x->model_id),
            ];

            if ($r1['qty_available'] <= 0)
                $r1['qty_available'] = 0;

            if ($flag_details) {

                $regular_price = $prices->where('item_id',$x->item_id)->where('price_type','REGULAR')->first();
                $cost_price = $prices->where('item_id',$x->item_id)->where('price_type','SUPPLIER')->first();

                $r2 = [
                    "storage"      => (trim($x->inv_dim5) != 'NA') ? trim($x->inv_dim5) : '',
                    "size"         => (trim($x->inv_dim6) != 'NA') ? trim($x->inv_dim6) : '',
                    "color"        => (trim($x->inv_dim7) != 'NA') ? trim($x->inv_dim7) : '',
                    "connectivity" => (trim($x->inv_dim8) != 'NA') ? trim($x->inv_dim8) : '',
                    "opssys"       => (trim($x->inv_dim9) != 'NA') ? trim($x->inv_dim9) : '',
                    "cpu"          => (trim($x->inv_dim10) != 'NA') ? trim($x->inv_dim10) : '',
                    "oem_warranty" => ($x->ext_warranty=="N") ? "N" : "Y",

                    "regular_price" => $regular_price ? $regular_price->unit_price : 0,
                    "cost_price"    => $cost_price ? $cost_price->unit_price : 0,

                    "is_digital" => (in_array($x->item_type, ['A','C','D','E','K','L','M','P','R','S','T','V','W','U', 'G','H']) || (strpos($x->inv_dim4,'-ESD') > 0) ) ? 'Y' : 'N',
                ];

                return array_merge($r1,$r2);
            }

            return $r1;

        })->filter(function ($value, $key) {
            return $value != null;
        })->values()->toArray();

        return $responder->success($data);
    }

}