<?php

namespace App\Http\Controllers;

use App\Core\Traits\Fractal;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use Fractal;
}
