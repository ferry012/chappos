<?php

namespace App\Http\Controllers\Members;

use App\Http\Controllers\Controller;
use App\Http\Resources\Members\MemberResource;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class MemberController extends Controller
{
    public function show($id)
    {

        $member = app('api')->members()->getById($id);

        if ($member === null) {
            return $this->errorNotFound('member not found.');
        }

        return new MemberResource($member);
    }

    public function register()
    {

    }

}