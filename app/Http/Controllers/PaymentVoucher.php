<?php

namespace App\Http\Controllers;

use App\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Throwable;

class PaymentVoucher extends Controller
{

    private function checkToken($value, $item=''){

        // Map allowed keys in app/Http/Middleware/CheckClientCredentials.php to partner_id
        $token = trim(str_ireplace('bearer', '', $value));

        $partners = [
            "e70f6a6f957ed681ff09a4def4e43528" => "WOGI"
        ];

        if (!isset($partners[$token])) {
            return null;
        }

        $result = db('pg_cherps::read')->table('sms_voucher_partner')->where('partner_id', $partners[$token])->where('partner_item', $item)->first();

        if ($result){
            return [
                "coy_id" => $result->coy_id,
                "item_id" => trim($result->item_id),
                "voucher_amount" => $result->voucher_amount,
                "expiry_date" => trim($result->expiry_date),
                "staff_id" => trim($result->partner_id)
            ];
        }

        return null;
    }

    function genUniqNums($length = 7) {
        date_default_timezone_set("Singapore");
        $bytes = random_int(0, date('mhis'));
        $format = '%0'.$length. 'd';
        $bytes = sprintf($format, $bytes);
        return $bytes;
    }

    private function generateVch($coy_id){

        $library = 'ABCDEFGHJKMNPQRSTUXY';
        date_default_timezone_set("Singapore");

        do {

            $new_voucher_id = "PV" . date("y") . $this->genUniqNums() . $library[rand(0, strlen($library))];
            
            $sql = "SELECT voucher_id FROM sms_voucher_list WHERE voucher_id = '" . $new_voucher_id . "' AND coy_id = ?";
            $result = db('pg_cherps::read')->select($sql, [$coy_id]);
            if (count($result) == 0) {
                break;
            }

        } while (count($result) !== 0);

        return $new_voucher_id;
    }

    public function get_voucher($item, Request $request){

        try {
            $input = request()->only(['date', 'voucher_amount']);

            $session = $this->checkToken($request->header('authorization'), $item);
            if (isnull($session)){
                $errors = "Unauthorized Access";
                $ref = $this->_log('W'.time(),request()->fullUrl(),$input,$errors, 'GET');
                $resp = [
                    "ref" => $ref,
                    "error" => $errors
                ];
                $this->setStatusCode(400);
                return $this->respondWithError($resp);
            }

            $coy_id     = ($session) ? $session['coy_id'] : "CTL";
            $item_id    = ($session) ? $session['item_id'] : $input['item_id'];
            $user       = ($session) ? $session['staff_id'] : $input['staff_id'];
            $voucher_amount = ($session) ? $session['voucher_amount'] : $input['voucher_amount'];

            if (!empty($input['date'])) {
                $date = date('Y-m-d', strtotime($input['date']));
                $result = db('pg_cherps::read')->table('sms_voucher_list')
                    ->where("coy_id", $coy_id)->where("voucher_amount", $voucher_amount)->where("item_id", $item_id)
                    //->where("created_by", substr($user,0,15))
                    ->whereRaw("sale_date::date = '$date'")
                    ->orderBy("sale_date","desc")
                    ->get(['voucher_id','voucher_amount','sale_date','expiry_date','utilize_date'])->toArray();
            } else {
                $result = db('pg_cherps::read')->table('sms_voucher_list')
                    ->where("coy_id", $coy_id)->where("voucher_amount", $voucher_amount)->where("item_id", $item_id)
                    //->where("created_by", substr($user,0,15))
                    ->orderBy("sale_date","desc")
                    ->get(['item_id','voucher_id','voucher_amount','sale_date','expiry_date','utilize_date'])->toArray();
            }

            $ref = $this->_log('G'.time(),request()->fullUrl(),$input,$result, 'GET');
            return [
                "ref" => $ref,
                "msg" => count($result) . " Voucher retrieved",
                "results" => $result
            ];
        }
        catch (Throwable $e) {
            return response()->json([
                'code' => 0,
                'message' => 'Please try again, server has a high volume of request'
            ]);
        }


    }

    public function sms_voucher_list(Request $request)
    {
        try {
            $input = request()->only(['item_id','voucher_amount','expiry_date','loc_id','staff_id']);

            $rules = [
                'item_id' => 'required'
            ];

            // If partner is not in above setting, staff_id is required.
            $session = $this->checkToken($request->header('authorization'), $input['item_id']);
            if (isnull($session)){
                $rules = [
                    'item_id' => 'required',
                    'staff_id' => 'required',
                    'expiry_date' => 'required',
                    'voucher_amount' => 'required',
                ];
            }

            $validator = Validator::make($input, $rules, []);
            if ($validator->fails()) {
                $errors = $validator->errors($validator);
                $ref = $this->_log('W'.time(),request()->fullUrl(),$input,$errors);
                $resp = [
                    "ref" => $ref,
                    "error" => $errors
                ];
                $this->setStatusCode(400);
                return $this->respondWithError($resp);
            }

            $coy_id     = ($session) ? $session['coy_id'] : "CTL";
            $item_id    = ($session) ? $session['item_id'] : $input['item_id'];
            $amount     = ($session) ? $session['voucher_amount'] : $input['voucher_amount'];
            $user       = ($session) ? $session['staff_id'] : $input['staff_id'];
            $expiry     = ($session) ? date('Y-m-d 23:59:59', strtotime($session['expiry_date'])) : date('Y-m-d 23:59:59', strtotime($input['expiry_date']));
            $loc_id     = $input['loc_id'] ?? '';
            $now        = date('Y-m-d H:i:s');
            $voucher_id = $this->generateVch($coy_id);

            $voucherItem = [
                "coy_id" => $coy_id,
                "item_id" => $item_id,
                "voucher_id" => $voucher_id,
                "status_level" => 2,
                "print_date" => $now,
                "issue_date" => $now,
                "loc_id" => $loc_id,
                "sale_date" => $now,
                "expiry_date" => $expiry,
                "receipt_id1" => '',
                "utilize_date" => '1900-01-01',
                "receipt_id2" => '',
                "voucher_amount" => $amount,
                "created_by" => substr($user,0,15),
                "created_on" => $now,
                "modified_by" => substr($user,0,15),
                "modified_on" => $now
            ];
            $result = db('pg_cherps')->table('sms_voucher_list')->insert($voucherItem);

            $ref = $this->_log($voucher_id,request()->fullUrl(),$input,$voucherItem);

            if ($result) {
                return [
                    "ref" => $ref,
                    "msg" => "Voucher generated",
                    "vch" => [
                        "item_id" => $item_id,
                        "voucher_id" => $voucher_id,
                        "voucher_amount" => $amount,
                        "sale_date" => $now,
                        "expiry_date" => $expiry
                    ]
                ];
            }


            // Unknown failure
            $resp = [
                "ref" => $ref,
                "error" => "Fail to generate voucher"
            ];
            $this->setStatusCode(400);
            return $this->respondWithError($resp);
        }

        catch (Throwable $e) {
            return response()->json([
                'code' => 0,
                'message' => 'Please try again, server has a high volume of request'
            ]);
        }

    }

    private function _log($api_description,$endpoint,$request,$response, $request_method='POST'){

        return db()->table('api_log')->insertGetId([
            "api_client"      => '3pvoucher',
            "api_description" => $api_description,
            "request_method"  => $request_method,
            "request_url"     => $endpoint,
            "request_header"  => '{}',
            "request_body"    => json_encode($request),
            "request_on"      => date('Y-m-d H:i:s'),
            "response_header" => '{}',
            "response_body"   => json_encode($response),
            "response_on"     => date('Y-m-d H:i:s'),
            "error_msg"       => '',
            "created_on"      => date('Y-m-d H:i:s'),
        ]);

    }
}