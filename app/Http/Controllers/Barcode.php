<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Barcode extends Controller
{
    public function __invoke(Request $request)
    {
        $code     = $request->get('code');
        $type     = $request->get('type', 'C39');
        $response = $request->get('response', 'HTML');
        $preview  = $request->get('preview', 0);
        $color    = $request->get('color', 'black');
        $width    = $request->get('w', 2);
        $height   = $request->get('h', 30);

        $make     = 'DNS1D';
        $type     = strtoupper($type);
        $response = strtoupper($response);

        if (! in_array($response, ['HTML', 'SVG'])) {
            return $this->errorWrongArgs();
        }

        if (in_array($type, ['QRCODE', 'PDF417', 'DATAMATRIX'])) {
            $make   = 'DNS2D';
            $width  = $request->get('w', 10);
            $height = $request->get('h', 10);
        }

        $response = 'getBarcode'.$response;

        echo app($make)->$response($code, $type, $width, $height, $color, true);
    }
}