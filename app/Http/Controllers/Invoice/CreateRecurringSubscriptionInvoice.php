<?php


namespace App\Http\Controllers\Invoice;


use App\Http\Controllers\Controller;
use App\Core\Orders\Services\CreateRecurringSubscriptionInvoice as CreateInvoice;
use Flugg\Responder\Responder;
use Illuminate\Http\Request;

class CreateRecurringSubscriptionInvoice extends Controller
{
    public function __invoke(Request $request, Responder $responder)
    {
        $invoiceId = $request->get('invoice_id');

        if ((new CreateInvoice())->create($invoiceId)) {
            return $responder->success();
        }

        return $responder->error('invoice_create_failed');
    }
}