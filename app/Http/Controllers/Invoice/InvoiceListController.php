<?php
/**
 * Created by PhpStorm.
 * User: jayartisan19
 * Date: 06/09/2022
 * Time: 10:38 AM
 */

namespace App\Http\Controllers\Invoice;

use App\Models\InvoiceList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvoiceListController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */

    protected $getInvoice;
    public function __invoke(Request $request,$invoice_num,$coy_id)
    {
        $this->ensureInvoiceExist($invoice_num,$coy_id);
        return response()->json([
            'data'=> $this->getInvoice
        ]);
    }

    protected function ensureInvoiceExist($invoice_num,$coy_id): void
    {
        $invoice = InvoiceList::where(function ($query) use ($invoice_num,$coy_id) {
            $query->where('invoice_id', $invoice_num)
                ->where('coy_id', $coy_id);
        })->get();

        if($invoice){
            $this->getInvoice = $invoice;
        }
    }
}
