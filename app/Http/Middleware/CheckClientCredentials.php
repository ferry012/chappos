<?php

namespace App\Http\Middleware;

use App\Support\Str;
use Closure;
use Exception;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class CheckClientCredentials
{
    protected $bypassToken = [
        'VpSdfK8X3K6pKVvtLa1fJyPujLRS86U3', // Universal developer
        'fw6sl9LBIQJIYwRJse5DanMH4OTwtbIm', // CHROOMS & OPS APP (internal developer)
        'amXgnuZPGE9DxOKq1S40RuI1947esWaT', // VC APP (internal developer)
        'b8e065255d5326ea3cf1f85b0dd764f3', // Magento key - KTP
        'cb2bf23fb542314741191aae720da4ed', // Magento key - ITEZ
        '99187235362f072924a2a33bfa78d25e', // Partner key - CHUGO
        'e70f6a6f957ed681ff09a4def4e43528', // Partner - WOGI (For Payment Vch Integration)
    ];

    /**
     * Create a new middleware instance.
     *
     */
    public function __construct()
    {
    }

    /**
     * Handle an incoming request.
     *
     * @param  Request $request
     * @param  Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $token    = $this->getToken($request);
        $cacheKey = 'token.'.$token;

        $result = app('cache')->tags('tokens')->get($cacheKey);

        if (! $result) {
            $result = db()->table('api_session')->where('token', $token)->first();

            if ($result) {
                app('cache')->tags('tokens')->put($cacheKey, $result, now()->addSeconds(3));
            }
        }

        if ($result === null && ! in_array($token, $this->bypassToken, true)) {
            return response('Unauthorized.', 401);
        }

        if ($result) {
            $request->merge(['user_data' => json_decode($result->custom_data)]);
        }

        return $next($request);
    }

    /**
     * Validate the requests authorization header for the provider.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
     *
     * @return bool
     */
    public function validateAuthorizationHeader(Request $request)
    {
        if (Str::startsWith(strtolower($request->headers->get('authorization')), $this->getAuthorizationMethod())) {
            return true;
        }

        throw new BadRequestHttpException();
    }

    /**
     * Get the JWT from the request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @throws \Exception
     *
     * @return string
     */
    protected function getToken(Request $request)
    {
        try {
            $this->validateAuthorizationHeader($request);

            $token = $this->parseAuthorizationHeader($request);
        } catch (Exception $exception) {
            if (! $token = $request->query('token', false)) {
                throw $exception;
            }
        }

        return $token;
    }

    /**
     * Parse token from the authorization header.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    protected function parseAuthorizationHeader(Request $request)
    {
        return trim(str_ireplace($this->getAuthorizationMethod(), '', $request->header('authorization')));
    }

    /**
     * Get the providers authorization method.
     *
     * @return string
     */
    public function getAuthorizationMethod()
    {
        return 'bearer';
    }
}