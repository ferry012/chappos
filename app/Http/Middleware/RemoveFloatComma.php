<?php

namespace App\Http\Middleware;


class RemoveFloatComma extends TransformsRequest
{
    /**
     * Transform the given value.
     *
     * @param  string $key
     * @param  mixed  $value
     *
     * @return mixed
     */
    protected function transform($key, $value)
    {

        if (is_num_formatted($value)) {
            // Replace comma(,) to empty('') string
            $value = preg_replace('/(?<=\d),(?=\d{3}\b)/', '', $value);

            return (float)$value;
        }

        return $value;
    }
}