<?php
/*
namespace App\Http\Resources;


class ProductResource extends Resource
{

    private function _calcUnitprice($item, $price_type, $item_type=''){
        if ($price_type=='M')
            $unit_price = $item->mbr_price;
        else
            $unit_price = $item->regular_price;

        if ($this->promo_price>0 && $this->promo_price<$unit_price)
            $unit_price = $item->promo_price;

        if ($item_type=='PWP')
            $unit_price = $item->unit_price;

        return $unit_price;
    }

    public function toArray($request) {

        $req = ($request->get('resp_with')) ? explode(',',$request->get('resp_with')) : [];
        $price_type = ($request->get('price_type') && in_array($request->get('price_type'),['M','S']) ) ? $request->get('price_type') : '';

        $product_image_url = get_constant('s3.product_images_thumb');
        $limits = json_decode($this->limit_options);

        $return = [
            'item_id' => $this->item_id,
            'item_desc' => $this->item_desc,
            //'short_desc' => $this->short_desc,
            'image_name' => $product_image_url . $this->image_name,
            'prices' => [
                'unit_cost' => $this->unit_cost,
                'unit_price' => $this->_calcUnitprice($this, $price_type),
                'multiple_points' => $this->multiple_points,
                'regular_price' => $this->regular_price,
                'mbr_price' => $this->mbr_price,
                'promo_price' => $this->promo_price,
                'promo_id' => trim($this->promo_id),
                'cost_price' => $this->cost_price,
                'promo_cost' => $this->promo_cost,
                'min_price' => $this->min_price,
            ]
        ];

        if (in_array("settings",$req)){
            $return['settings'] = [
                'item_type' => $this->item_type,
                'ext_warranty' => $this->ext_warranty,
                'lot_control' => $this->lot_control,
                'salesperson_control' => $this->salesperson_control,
                'status_level' => $this->status_level,
                'allow_discount' => $this->allow_discount,
                'allow_demo' => (isset($limits->is_demo)) ? $limits->is_demo : false,
                'limit_options' => [
                    'limit_per_day' => (isset($limits->limit_per_day)) ? $limits->limit_per_day : -1,
                    'limit_per_mbr' => (isset($limits->limit_per_mbr)) ? $limits->limit_per_mbr : -1,
                    'limit_per_trans' => (isset($limits->limit_per_trans)) ? $limits->limit_per_trans : -1,
                ],
                'dimensions' => [
                    'inv_dim1' => trim($this->inv_dim1),
                    'inv_dim2' => trim($this->inv_dim2),
                    'inv_dim3' => trim($this->inv_dim3),
                    'inv_dim4' => trim($this->inv_dim4),
                    'brand_id' => trim($this->brand_id),
                    'model_id' => trim($this->model_id),
                ]
            ];
        }

        if (in_array("online",$req)){
            $return['online_settings'] = [
                'parent_item_id' => $this->parent_item_id,
                'color_id' => $this->color_id,
                'is_active' => $this->is_active,
                'in_catalog' => $this->in_catalog,
                'inv_type' => $this->inv_type,
                'buffer_qty' => $this->buffer_qty,
                'shipping_methods' => $this->delivery_options,
                'listing_from' => $this->listing_from,
                'listing_to' => $this->listing_to,
            ];
        }

        if (in_array("pwp",$req)){
            if (count($this->pwp)>0){
                $pwpgrp = array();
                foreach($this->pwp as $p) {
                    $pt = [
                        'item_id' => $p->item_id,
                        'item_desc' => $p->item_desc,
                        //'short_desc' => $this->short_desc,
                        'image_name' => $product_image_url . $p->image_name,
                        'prices' => [
                            'unit_cost' => $p->unit_cost,
                            'unit_price' => $this->_calcUnitprice($p, $price_type, 'PWP'),
                            'multiple_points' => $p->multiple_points,
                            'regular_price' => $p->regular_price,
                            'mbr_price' => $p->mbr_price,
                            'promo_price' => $p->promo_price,
                            'promo_id' => trim($p->promo_id),
                            'cost_price' => $p->cost_price,
                            'promo_cost' => $p->promo_cost,
                            'min_price' => $p->min_price,
                        ],
                        'promo_grp_id' => trim($p->promo_grp_id),
                        'promo_qty' => $p->promo_qty
                    ];

                    if (in_array("settings",$req)){
                        $pt['settings'] = [
                            'item_type' => $p->item_type,
                            'lot_control' => $p->lot_control,
                            'salesperson_control' => $p->salesperson_control,
                            'ext_warranty' => $p->ext_warranty,
                            'allow_discount' => $p->allow_discount,
                            'status_level' => $p->status_level,
                            'dimensions' => [
                                'inv_dim1' => trim($p->inv_dim1),
                                'inv_dim2' => trim($p->inv_dim2),
                                'inv_dim3' => trim($p->inv_dim3),
                                'inv_dim4' => trim($p->inv_dim4),
                                'brand_id' => trim($p->brand_id),
                                'model_id' => trim($p->model_id),
                            ],
                        ];
                    }

                    if (in_array("online",$req)){
                        $pt['online_settings'] = [
                            'parent_item_id' => $p->parent_item_id,
                            'color_id' => $p->color_id,
                            'is_active' => $p->is_active,
                            'in_catalog' => $p->in_catalog,
                            'inv_type' => $p->inv_type,
                            'buffer_qty' => $p->buffer_qty,
                            'shipping_methods' => $p->delivery_options,
                            'limit_options' => [
                                'limit_per_day' => $limits->limit_per_day,
                                'limit_per_mbr' => $limits->limit_per_mbr,
                                'limit_per_trans' => $limits->limit_per_trans,
                            ],
                            'listing_from' => $p->listing_from,
                            'listing_to' => $p->listing_to,
                        ];
                    }

                    $k = trim($p->promo_grp_id);
                    $pwpgrp[$k][] = $pt;
                }

                foreach ($pwpgrp as $g){
                    $return['pwp'][] = $g;
                }
            }
        }

        return $return;

    }

}
*/