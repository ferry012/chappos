<?php

namespace App\Http\Resources;

use Illuminate\Container\Container;
use Illuminate\Support\Collection;
use Illuminate\Http\Resources\MissingValue;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

abstract class AbstractResource extends JsonResource
{
    protected $only = [];

    /**
     * @var string
     */
    protected $channel = null;

    /**
     * @var string
     */
    protected $language = null;

    /**
     * The resource data array.
     *
     * @var array
     */
    protected $data = [];

    protected $append = [];

    protected $numberFormat = [];

    /**
     * Create a new resource instance.
     *
     * @param  mixed $resource
     * @param  array $only
     *
     */
    public function __construct($resource, $only = [])
    {
        parent::__construct($resource);
        $this->only = collect($only);
    }

    /**
     * Set the singular item.
     *
     * @param mixed $resource
     *
     * @return self
     */
    public function item($resource)
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * Resolve the resource to an array.
     *
     * @param  \Illuminate\Http\Request|null $request
     *
     * @return array
     */
    public function resolve($request = null)
    {
        $data = parent::resolve($request);

        foreach ($data as $key => $value) {
            if (in_array($key, $this->numberFormat, false) && ! is_num_formatted($value)) {
                $data[$key] = number_format((float)$value, 2);
            }
        }

        return $data;
    }

    /**
     * Set the only fields we want to return.
     *
     * @param array $fields
     *
     * @return self
     */
    public function only($fields = [])
    {
        if ($fields instanceof Collection) {
            $fields = $fields->toArray();
        } elseif (is_string($fields)) {
            $fields = explode(',', $fields);
        }
        $this->only = collect($fields);

        return $this;
    }

    /**
     * Set the channel we want to use.
     *
     * @param string $channel
     *
     * @return self
     */
    public function channel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Set the language to use.
     *
     * @param string $language
     *
     * @return self
     */
    public function language($language)
    {
        $this->language = $language;

        return $this;
    }

    public function toArray($request)
    {
        $attributes = array_merge($this->payload(), $this->custom_attributes ?? []);

        if ($request->get('full_response', 0)) {
            $attributes = array_merge($attributes, [
                'attribute_data' => $this->attribute_data,
            ]);
            foreach ($this->optional() as $key => $value) {
                $attributes[$key] = $value;
            }
        } else {
            $attributes = array_merge($attributes, $this->map($this->attribute_data ?? []));
            foreach ($this->optional() as $key => $value) {
                $attributes[$key] = $this->when($request->has($key), $value);
            }
        }

        return array_merge($attributes, $this->includes(), $this->append);
    }

    protected function include ($relation, $resource)
    {
        return $this->when($this->relationLoaded($relation), function () use ($relation, $resource) {
            return ['data' => new $resource($this->whenLoaded($relation))];
        });
    }

    public function append(array $data)
    {
        $this->append = $data;

        return $this;
    }

    /**
     * The included resources.
     *
     * @return array
     */
    public function includes()
    {
        return [];
    }

    /**
     * Define optional attributes.
     *
     * @return array
     */
    public function optional()
    {
        return [];
    }

    /**
     * Map the attributes.
     *
     * @param array $data
     *
     * @return array
     */
    protected function map($data)
    {
        if (empty($data)) {
            return [];
        }
        $modified = [];
        foreach ($data as $field => $value) {

            if ($this->only->count() && ! $this->only->contains($field)) {
                continue;
            }
            $modified[$field] = $this->resource->attribute(
                $field,
                $this->channel,
                $this->language
            );
        }

        return $modified;
    }

    /**
     * Create new anonymous resource collection.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function respondWithCollection()
    {
        return new AnonymousResourceCollection($this->resource, static::class);
    }

    /**
     * Dynamically get properties from the underlying resource.
     *
     * @param  string $key
     *
     * @return mixed
     */
    public function __get($key)
    {
        return $this->resource->{$key};
    }

    /**
     * Dynamically pass method calls to the underlying resource.
     *
     * @param  string $method
     * @param  array  $parameters
     *
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        if (in_array($method, $this->numberFormat, false)) {

            $value = $this->resource->{$method}(...$parameters);

            if (! is_num_formatted($value)) {
                return number_format((float)$value, 2);
            }

        }

        return $this->resource->{$method}(...$parameters);
    }
}