<?php

namespace App\Http\Resources\Orders;

use App\Http\Resources\AbstractResource;

class OrderLineResource extends AbstractResource
{
    protected $numberFormat = [
        'regular_price', 'unit_price', 'price_total', 'discount_total', 'total_price',
    ];

    public function payload()
    {
        $delv_method      = ($this->delv_method && $this->delv_method !== '') ? $this->delv_method : 'NIL';
        $delv_description = 'NIL';

        $delv_info = hsg_delivery($delv_method);

        if ($delv_info) {
            $delv_description = $delv_info->delv_name;

            if ($delv_info->display_type === 'STORE' && $this->loc_id && $this->loc_id !== '')
                $delv_description .= ' - '.$this->loc_id;
        }


        return [
            'id'        => $this->encoded_id,
            'parent_id' => $this->parent_id,

            'line_num'      => $this->line_num,
            'ref_line_num'  => $this->ref_line_num,
            'item_id'       => $this->item_id,
            'item_desc'     => $this->item_desc,
            'item_type'     => $this->item_type,
            'qty_ordered'   => $this->qty_ordered,
            'unit_price'    => $this->unit_price,
            'unit_discount' => $this->unit_discount_amount,
            'regular_price' => $this->regular_price,
            'total_price'   => $this->price_total,

            'qty_refunded'   => $this->qty_refunded,
            'discount_total' => $this->discount_total,
            'lot_id'         => (isset($this->custom_data['lot_id'])) ? $this->custom_data['lot_id'] : '',
            'custom_data'    => (is_array($this->custom_data)) ? array_except($this->custom_data, [
                'lot_id', 'order_line',
            ]) : null,

            'loc_id'      => $this->loc_id ?? '',
            'delv_method' => $this->delv_method ?? '',
            'delv_desc'   => $delv_description,

            // To depreciate
            'description' => $this->item_desc,
            'price_total' => $this->price_total,
        ];
    }
}