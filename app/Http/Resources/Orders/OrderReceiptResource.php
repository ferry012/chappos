<?php

namespace App\Http\Resources\Orders;

use App\Http\Resources\AbstractResource;
use App\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;

class OrderReceiptResource extends AbstractResource
{
    protected $numberFormat = [
        'subtotal',
        'tax_amount',
        'total_amount',
        'total_paid',
        'total_savings',
        'rebates_earned',
        'getChangeDue',
        'getRebateEarned',
    ];

    public function payload()
    {
        $logoId = 'CTL';
        $storeShopName = '';
        $storeAddressLine1 = '';
        $storeAddressLine2 = '';
        $storeAddressLine3 = '';

        $loc_id = $this->loc_id ?? 'HCL';
        if ($location = pos_locations($loc_id)) {
            $logoId = $location->logo_id;
            $storeShopName = $location->shop_name;
            $storeAddressLine1 = $location->street_line1;
            $storeAddressLine2 = $location->street_line2;
            if (isset($location->coy_reg_code)) {
                $storeAddressLine3 .= ($storeAddressLine3 != '') ? ' | ' : '';
                $storeAddressLine3 .= 'ROC '.($location->coy_reg_code ?? '');
            }
            if (isset($location->coy_tax_code)) {
                $storeAddressLine3 .= ($storeAddressLine3 != '') ? ' | ' : '';
                $storeAddressLine3 .= 'GST '.($location->coy_tax_code ?? '');
            }
        }

        $pos_receipt = pos_functions('RECEIPT');
        $data_receipt = [
            "receipt_footer"  => collect($pos_receipt)->firstWhere('func_id','footer_text')->image_name ?? "",
            'receipt_qr'      => collect($pos_receipt)->firstWhere('func_id','qr_link')->image_name ?? "",
            "receipt_qr_msg"  => collect($pos_receipt)->firstWhere('func_id','qr_text')->image_name ?? "",
        ];

        $data = [
            'tax_id'           => $this->tax_id,
            'logo_id'          => $logoId,
            'store_name'       => $storeShopName,
            'store_addr_line1' => $storeAddressLine1,
            'store_addr_line2' => $storeAddressLine2,
            'store_addr_line3' => $storeAddressLine3,

            'loc_id' => $this->loc_id ?? '',
            'pos_id' => $this->pos_id ?? '',

            'invoice_num'        => $this->invoice_id ?? $this->order_num,
            'trans_num'          => $this->order_num,
            'trans_type'         => $this->order_type,
            'trans_ref_id'       => $this->ref_id ?? '',
            'trans_date'         => $this->created_on->format('Y-m-d H:i:s'),
            'display_trans_date' => $this->created_on->format('d/m/Y H:i:s'),
            'order_status_level' => $this->order_status_level,
            'pay_status_level'   => $this->pay_status_level,
            'cashier_id'         => $this->cashier_id ?? '',
            'cashier_name'       => $this->cashier_name ?? '',

            'item_count'     => $this->lines->sum('qty_ordered'),
            'subtotal'       => $this->item_total - $this->discount_total,
            'tax_amount'     => $this->tax_amount,
            'total_amount'   => $this->order_total,
            'total_paid'     => $this->total_paid,
            'rounding_adj'   => $this->rounding_adj,
            'change_due'     => $this->getChangeDue(),
            'total_savings'  => $this->total_savings,
            'trans_point'    => $this->points_earned,
            'rebates_earned' => $this->getRebateEarned(),

            'customer_info' => null,
            'ssew_info'     => null,

            'reprint'         => 'N',
            'receipt_message' => $this->_format_printer($this->receipt_message()),
            "receipt_footer"  => $this->_format_printer($data_receipt['receipt_footer']),
            'receipt_qr'      => $this->_format_printer($data_receipt['receipt_qr']),
            'receipt_qr_msg'  => $this->_format_printer($data_receipt['receipt_qr_msg'])
        ];
//        dd($data);
        $url = null;
        if (App::environment('local', 'development')) {
            $url = get_constant('url.ereceipt-dev') . encrypt_decrypt($data['invoice_num'], 'encrypt');
        }
        if (App::environment('staging', 'production')) {
            $url = get_constant('url.ereceipt') . encrypt_decrypt($data['invoice_num'], 'encrypt');
        }

        $data['invoice_url'] = $url;

        $data = $this->customerInfo($data);

        if ($warranty = array_get($this->custom_data, 'ssew')) {
            $data['ssew_info'] = [
                'first_name' => Str::masking($warranty['first_name'], 0, 4, true),
                'last_name'  => $warranty['last_name'],
                'email'      => Str::masking($warranty['email'], 4, 8, true),
            ];
        }

        if ($this->order_type === 'DM' && $deposit = array_get($this->custom_data, 'deposit')) {
            $data['deposit'] = $deposit;
        }

        $refundCredit = $this->transactions->where('pay_type', 'ECREDIT-RF')->first();
        if ($refundCredit) {
            $data['refund_credit'] = [
                'voucher_id'   => $refundCredit->getCustomData('ecredit.voucher_id'),
                'trans_amount' => abs($refundCredit->trans_amount),
                'expiry_date'  => $refundCredit->getCustomData('ecredit.expiry_date'),
            ];
        }

        $data['coupons'] = $this->when($this->discounts->isNotEmpty(), $this->discounts());

        // Add extra info to be printed out on separate slip
        $instructions = $this->receipt_instructions();
        if (!empty($instructions)) {
            $data['instruction_slip'] = $instructions;
        }

        return $data;
    }

    public function customerInfo($data)
    {
        if ($this->isMemberPurchase()) {

            $expDate = '';

            if ($this->hasCustomData('member.exp_date')) {
                $expDate = $this->getCustomData('member.exp_date');
            } else {
                // fallback for oldest orders
                if ($member = app('api')->members()->getById($this->customer_id, true)) {
                    $expDate = Carbon::createFromTimeString($member->exp_date)->format('d M Y');

                    $this->setCustomData('member.exp_date', Str::upper($expDate));

                    $this->save();
                }
            }

            $data['customer_info'] = [
                'id'        => $this->customer_id,
                'masked_id' => Str::masking($this->customer_id, 0, 4, true),
                'name'      => $this->customer_name,
                'exp_date'  => $expDate,
            ];

        }

        return $data;
    }

    public function discounts()
    {
        $data = [];

        foreach ($this->discounts as $discount) {
            $data[] = [
                'coupon_id'   => $discount->coupon_id,
                'coupon_name' => $discount->coupon_name,
                'coupon_code' => $discount->coupon_code,
                'amount'      => $discount->amount,
            ];
        }

        return $data;
    }

    public function includes()
    {
        $transactions = $this->transactions->filter(function ($row) {
            return $row->getCustomData('pos_printable', true) === true;
        });

        $memberType = $this->getCustomData('member.type');

        $lines = $this->lines->filter(function ($row) use ($memberType) {

            // if (in_array($memberType, ['', 'MST', 'MNS', 'MFC'], true)) {
            if (in_array($memberType, ['', 'MST', 'MFC'], true)) {
                $row->total_points_earned = 0;
            }

            return $row->getCustomData('pos_printable', true) === true;
        });

        $eCredit = $transactions->where('pay_mode', 'ECREDIT')->first();

        // Expand eCredits in to multiple record
        if ($eCredit) {
            $eCreditList = $eCredit->getCustomData('vouchers', []);

            if (count($eCreditList) > 0) {
                $eCredit->unsetCustomData('vouchers');

                $transactions = $transactions->filter(function ($row) {
                    return Str::trimRight($row->pay_mode) !== 'ECREDIT';
                });

                foreach ($eCreditList as $credit) {
                    $eCredit->trans_amount = (float)$credit['voucher_amount'];
                    $eCredit->trans_ref_id = $credit['voucher_id'];
                    $transactions->push(clone $eCredit);
                }
            }
        }

        return [
            'items'    => new OrderReceiptLineCollection($lines),
            'payments' => new OrderReceiptTransactionCollection($transactions),
        ];
    }

    public function receipt_message()
    {
        if ($this->order_type == 'GS') {
            return '* The goods sold on this invoice are sold under the GST Gross Margin Scheme. Both the seller and buyer are not allowed to make any input tax claim on the goods.';
        }
        else if ($this->hasCustomData('coupon_ref')) {
            return '* Coupon Ref: ' . $this->getCustomData('coupon_ref');
        }
        else if ($this->hasCustomData('ref_trans_id')) {
            return '* Trans Ref: ' . $this->getCustomData('ref_trans_id');
        }

        return '';
    }

    public function receipt_instructions()
    {
        if (app()->environment('production')) {
            return [];
        }

        $instructions = [];

        if (strpos($this->customer_id, 'MERDEKA') || strpos($this->customer_id, 'TOURIST')) {

            $instructions[] = [
                "text" => "** CASHIER COPY **"
            ];
            $instructions[] = [
                "qr" => "https://www.challenger.com.sg/"
            ];
            $instructions[] = [
                "barcode" => $this->invoice_id
            ];
            $instructions[] = [
                "text" => "*".$this->invoice_id."*"
            ];
            $instructions[] = [
                "text" => "This purchase is made under Merdeka/Tourist membership. Please check the invoice for more details."
            ];


        }

        return $instructions;
    }

    private function _format_printer($str) {
        return str_replace('\n',"\n", $str);
    }
}