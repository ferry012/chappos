<?php

namespace App\Http\Resources\Orders;

use App\Http\Resources\AbstractCollection;

class OrderReceiptTransactionCollection extends AbstractCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = OrderReceiptTransactionLineResource::class;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection;
    }
}