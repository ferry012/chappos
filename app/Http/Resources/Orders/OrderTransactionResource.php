<?php

namespace App\Http\Resources\Orders;

use App\Http\Resources\AbstractResource;

class OrderTransactionResource extends AbstractResource
{
    protected $numberFormat = ['amount',];

    public function payload()
    {
        $createdOn = null;

        if ($this->created_on) {
            $createdOn = $this->created_on->format('Y-m-d H:i:s');
        }

        return [
            'id'                       => $this->id,
            'transaction_id'           => $this->trans_id,
            'transaction_reference_id' => $this->trans_ref_id,
            'pay_mode'                 => $this->pay_mode,
            'pay_type'                 => $this->pay_type,
            'masked_card_number'       => $this->card_num,
            'approval_code'            => $this->approval_code,
            'trans_amount'             => $this->trans_amount,
            'success'                  => $this->is_success,
            'status'                   => $this->status_code,
            'custom_data'              => $this->custom_data,
            'created_on'               => $createdOn,
        ];
    }

}