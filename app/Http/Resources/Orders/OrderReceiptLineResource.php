<?php

namespace App\Http\Resources\Orders;

use App\Http\Resources\AbstractResource;

class OrderReceiptLineResource extends AbstractResource
{
    protected $numberFormat = ['unit_price', 'regular_price', 'total_price'];

    public function payload()
    {
        $salespersonId = '';
        $serialNum     = [];

        $this->hasCustomData('salesperson_id') && $salespersonId = implode(', ', $this->getCustomData('salesperson_id'));
        $this->hasCustomData('promoter_id') && $salespersonId .= (($salespersonId!='') ? ', ' : '') . $this->getCustomData('promoter_id') ;
        $this->hasCustomData('lot_id') && $serialNum = $this->getCustomData('lot_id');

        if ($this->unit_discount_amount > 0) {

            $totalDiscount   = $this->qty_ordered * $this->unit_discount_amount;
            $this->item_desc .= sprintf(' (Disc -%.2f)', $totalDiscount);

        }

        return [
            'line_num'       => $this->line_num,
            'ref_line_num'   => $this->ref_line_num,
            'item_id'        => $this->item_id,
            'item_desc'      => $this->item_desc,
            'item_type'      => $this->item_type,
            'qty_ordered'    => $this->qty_ordered,
            'unit_price'     => $this->unit_price,
            'unit_discount'  => $this->unit_discount_amount,
            'regular_price'  => $this->regular_price,
            'total_price'    => $this->price_total,

            'trans_point'    => $this->total_points_earned,
            'mbr_savings'    => $this->unit_savings * $this->qty_ordered,
            'salesperson_id' => $this->when(! empty($salespersonId), $salespersonId),
            'serial_num'     => $this->when(count($serialNum), $serialNum),

            'item_message'   => $this->item_message(),
        ];
    }

    public function item_message()
    {
        $message = '';

        if ($this->parent_id)
            $message .= ' [PWP]';

        if ($this->getCustomData('is_demo'))
            $message .= ' [DEMO]';

        if ($this->getCustomData('ext_warranty') && intval($this->getCustomData('ext_warranty'))>0 ) {
            $warranty = (intval($this->getCustomData('ext_warranty')) * 12);
            $message .= ' (Warranty: '.$warranty.' Months)';
        }

        if ($this->getCustomData('selfcollect') && $this->getCustomData('selfcollect')!='' ) {
            $selfcollect = $this->getCustomData('selfcollect');
            $message .= ' (Collect at '. $selfcollect .')';
        }

        // Blackhawk Card activated message

        return $message;
    }
}

