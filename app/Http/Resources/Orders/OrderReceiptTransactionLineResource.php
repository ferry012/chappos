<?php

namespace App\Http\Resources\Orders;

use App\Http\Resources\AbstractResource;
use App\Support\Str;
use App\Utils\Helper;

class OrderReceiptTransactionLineResource extends AbstractResource
{
    protected $numberFormat = ['trans_amount',];

    public function toArray($request)
    {
        $info = '';

        if (in_array($this->pay_mode, ['CARD', 'NETS', 'INSTALLMENT'])) {
            $infoStr = [];

            !empty($this->pay_type) && $infoStr[] = $this->pay_type;
            !empty($this->card_num) && $infoStr[] = str_repeat('*', 5) . $this->card_num;

            if (Str::endsWith($this->pay_type, '_IPP') && $this->hasCustomData('installment.month')) {
                $infoStr[] = Helper::parseInstallmentMonth($this->getCustomData('installment.month'));
            }

            if (count($infoStr)) {
                $info = sprintf('[%s]', implode('/', $infoStr));
            }

        } elseif ($this->pay_mode === 'DEPOSIT') {
            $info = '(' . $this->getCustomData('ref_id', 'unknown') . ')';

        }

        $result = [
            'pay_mode'      => $this->pay_mode,
            'info'          => $info,
            'approval_code' => $this->approval_code,
            'trans_amount'  => $this->trans_amount,
        ];

        if ($this->pay_mode == 'EGIFT' || $this->pay_mode == 'ECREDIT') {
            $infoStr = $this->getCustomData('vouchers', []);
            $voucherId = (isset($infoStr[0]['voucher_id'])) ? $infoStr[0]['voucher_id'] : '';

            if (empty($voucherId)) {
                $voucherId = $this->trans_ref_id;
            }

            $result['coupon_serialno'] = $voucherId;
        }
        if (substr(trim($this->pay_mode),-3) == 'VCH') {
            $vchStr = "";
            $infoStr = $this->getCustomData('vouchers', []);

            if (is_array($infoStr) && count($infoStr)) {
                $vchArr = [];
                foreach ($infoStr as $str) {
                    $vchArr[] = (isset($str['voucher_id'])) ? $str['voucher_id'] : "";
                }
                $vchStr = implode(', ', $vchArr);
            }

            $result['voucher_serialno'] = $vchStr;
        }

        return $result;
    }
}