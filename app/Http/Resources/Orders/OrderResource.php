<?php

namespace App\Http\Resources\Orders;

use App\Http\Resources\AbstractResource;
use App\Support\Str;
use Carbon\Carbon;

class OrderResource extends AbstractResource
{
    protected $numberFormat = [
        'sub_total',
        'shipping_total',
        'discount_total',
        'tax_amount',
        'total_amount',
        'total_due',
        'total_paid',
        'total_savings',
    ];

    public function payload()
    {
        $logoId = 'CTL';
        $storeShopName = '';
        $storeAddressLine1 = '';
        $storeAddressLine2 = '';
        $storeAddressLine3 = '';

        $loc_id = $this->loc_id ?? 'HCL';
        if ($location = pos_locations($loc_id)) {
            $logoId = $location->logo_id;
            $storeShopName = $location->shop_name;
            $storeAddressLine1 = $location->street_line1;
            $storeAddressLine2 = $location->street_line2;
            if (isset($location->coy_reg_code)) {
                $storeAddressLine3 .= ($storeAddressLine3 != '') ? ' | ' : '';
                $storeAddressLine3 .= 'ROC '.($location->coy_reg_code ?? '');
            }
            if (isset($location->coy_tax_code)) {
                $storeAddressLine3 .= ($storeAddressLine3 != '') ? ' | ' : '';
                $storeAddressLine3 .= 'GST '.($location->coy_tax_code ?? '');
            }
        }

        $data = [
            'logo_id'          => $logoId,
            'store_name'       => $storeShopName,
            'store_addr_line1' => $storeAddressLine1,
            'store_addr_line2' => $storeAddressLine2,
            'store_addr_line3' => $storeAddressLine3,

            'loc_id' => $this->loc_id ?? '',
            'pos_id' => $this->pos_id ?? '',

            'invoice_num'        => $this->invoice_id ?? $this->order_num,
            'order_num'          => $this->order_num,
            'order_type'         => $this->order_type,
            'order_ref_id'       => $this->ref_id ?? '',
            'trans_date'         => $this->created_on->format('Y-m-d H:i:s'),
            'display_trans_date' => $this->created_on->format('d/m/Y H:i:s'),
            'due_on'             => $this->due_on ?? $this->placed_on,
            'placed_on'          => $this->placed_on,
            'order_status_level' => $this->order_status_level,
            'pay_status_level'   => $this->pay_status_level,
            'cashier_id'         => $this->cashier_id ?? '',
            'cashier_name'       => $this->cashier_name ?? '',

            'item_count'     => $this->lines->sum('qty_ordered'),
            'currency'       => $this->currency_id ?? '',
            'subtotal'       => $this->item_total - $this->discount_total,
            'tax_id'         => $this->tax_id ?? '',
            'tax_amount'     => $this->tax_amount,
            'total_amount'   => $this->order_total,
            'total_paid'     => $this->total_paid,
            'total_due'      => $this->total_due,
            'rounding_adj'   => $this->rounding_adj,
            'change_due'     => $this->getChangeDue(),
            'total_savings'  => $this->total_savings,
            'trans_point'    => $this->points_earned,
            'rebates_earned' => $this->getRebateEarned(),
            'shipping_total' => $this->shipping_total,
            'discount_total' => $this->discount_total,

            'customer_info'  => null,
            'customer_id'    => $this->customer_id,
            'customer_name'  => $this->customer_name,
            'customer_phone' => $this->customer_phone,
            'customer_email' => $this->customer_email,
            'customer_notes' => $this->customer_note ?? '',

            //            'shipping_preference' => $this->shipping_preference,
            //            'shipping_method'     => $this->shipping_method,
            //            'reference'           => $this->reference,
            //            'customer_reference'  => $this->customer_reference,
            //            'invoice_reference'   => $this->invoice_reference,
            //            'vat_no'              => $this->vat_no,
            //            'tracking_no'         => $this->tracking_no,
            //            'dispatched_at'       => $this->dispatched_at,
            //            'billing_details'     => $this->billing_details,
            //            'shipping_details'    => $this->shipping_details,
            //            'status'              => $this->status,

            //            'created_by'         => $this->created_by,
            //            'created_on'         => $this->created_on->format('Y-m-d H:i:s'),
            //            'modified_by'        => $this->modified_by,
            //            'modified_on'        => $this->modified_on->format('Y-m-d H:i:s'),

            'is_free_shipping'  => $this->getCustomData('is_free_shipping', false),
            'is_guest_purchase' => $this->isGuestPurchase(),
            'delivery_date'     => $this->getCustomData('delivery_date'),
            'id'                => $this->encoded_id,
        ];

        $data['invoice_key'] = encrypt_decrypt($data['invoice_num'], 'encrypt');
        $data = $this->customerInfo($data);

        $data = $this->refundInfo($data);

        return $data;
    }

    public function includes()
    {
        return [
            'items'        => new OrderLineCollection($this->whenLoaded('lines')),
            'transactions' => new OrderTransactionCollection($this->whenLoaded('transactions')),
            'addresses'    => $this->addressesInfo(),
        ];
    }

    private function addressesInfo()
    {

        if ($this->addresses) {
            return $this->addresses->mapWithKeys(function ($addr, $key) {
                return [
                    strtolower($addr->address_type) => [
                        'addr_text'      => $addr->full_address,
                        'recipient_name' => $addr->recipient_name ?? '',
                        'company_name'   => $addr->company_name ?? '',
                        'phone_num'      => $addr->phone_num ?? '',
                        'postal_code'    => $addr->postal_code ?? '',
                        'city_name'      => $addr->city_name ?? '',
                        'state_name'     => $addr->state_name ?? '',
                        'country_id'     => $addr->country_id ?? ''
                    ]
                ];
            });
        }

        return null;
    }

    public function customerInfo($data)
    {
        if ($this->isMemberPurchase()) {

            $expDate = '';

            if ($this->hasCustomData('member.exp_date')) {
                $expDate = $this->getCustomData('member.exp_date');
            } else {
                // fallback for oldest orders
                if ($member = app('api')->members()->getById($this->customer_id, true)) {
                    $expDate = Carbon::createFromTimeString($member->exp_date)->format('d M Y');

                    $this->setCustomData('member.exp_date', Str::upper($expDate));

                    $this->save();
                }
            }

            $data['customer_info'] = [
                'id'        => $this->customer_id,
                'masked_id' => Str::masking($this->customer_id, 0, 4, true),
                'name'      => $this->customer_name,
                'exp_date'  => $expDate,
            ];

        }

        return $data;
    }

    public function refundInfo($data)
    {

        if (isset($_GET['refund']) && $_GET['refund'] == "Y") {

            if ($data['order_type'] == "OS") {
                // Retrieve refund info from CHERPS
                $refund = db('sql')->table('sms_invoice_list')->where('coy_id', 'CTL')->whereIn('inv_type', [
                    'HR',
                    'HX'
                ])->where('status_level', '>=', '0')
                    ->where('cust_po_code', $data['invoice_num'])->select('invoice_id', 'inv_type', 'invoice_date')->first();
            } else {
                // Check CHROOMS
                $refund = db()->table('o2o_order_list')->whereIn('order_type', [
                    'RF',
                    'EX',
                    'E1'
                ])->where('pay_status_level', '>=', '2')
                    ->where('ref_id', $data['invoice_num'])->select('invoice_id', 'order_type as inv_type', 'pay_on as invoice_date')->first();
            }

            $data['refund'] = $refund;
        }

        return $data;
    }
}