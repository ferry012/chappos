<?php

namespace App\Http\Resources\Products;

use App\Http\Resources\AbstractCollection;

class ProductStockCollection extends AbstractCollection
{
    public $collects = ProductStockResource::class;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection;
    }
}