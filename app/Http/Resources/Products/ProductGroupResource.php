<?php

namespace App\Http\Resources\Products;

use App\Http\Resources\AbstractResource;

class ProductGroupResource extends AbstractResource
{
    public function payload()
    {
        return [
            'group'         => $this->group,
            'actual_group'  => $this->actual_group,
            'is_free'       => $this->is_free,
            'max_qty'       => $this->max_qty,
            'product_count' => $this->product_count,
            'products'      => new ProductCollection($this->products),
        ];
    }
}