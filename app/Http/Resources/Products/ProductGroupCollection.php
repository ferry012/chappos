<?php

namespace App\Http\Resources\Products;

use App\Http\Resources\AbstractCollection;

class ProductGroupCollection extends AbstractCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = ProductGroupResource::class;

    public function toArray($request)
    {
        return $this->collection;
    }
}