<?php

namespace App\Http\Resources\Products;

use App\Http\Resources\AbstractResource;

class PriceResource extends AbstractResource
{
    protected $numberFormat = [
        'regular_price', 'mbr_price',
    ];

    public function payload()
    {
        return [
            'item_id'       => $this->item_id,
            'regular_price' => $this->regular_price,
            'mbr_price'     => $this->mbr_price,
            'disc_percent'  => $this->disc_percent ?? .0,
        ];
    }
}