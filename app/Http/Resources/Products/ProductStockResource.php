<?php

namespace App\Http\Resources\Products;

use App\Http\Resources\AbstractResource;

class ProductStockResource extends AbstractResource
{
    public function payload()
    {
        return [
            'item_id'      => $this->item_id,
            'loc_id'       => $this->loc_id,
            'qty_on_hand'  => $this->qty_on_hand,
            'qty_reserved' => $this->qty_reserved,
            'qty_balance'  => $this->qty_on_hand - $this->qty_reserved,
        ];
    }
}