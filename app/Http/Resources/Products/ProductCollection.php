<?php

namespace App\Http\Resources\Products;

use App\Http\Resources\AbstractCollection;

class ProductCollection extends AbstractCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = ProductResource::class;

    public function toArray($request)
    {
        return $this->collection;
    }

}