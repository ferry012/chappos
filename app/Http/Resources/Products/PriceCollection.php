<?php

namespace App\Http\Resources\Products;

use App\Http\Resources\AbstractCollection;

class PriceCollection extends AbstractCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = PriceResource::class;

    public function toArray($request)
    {
        return $this->collection;
    }

}