<?php

namespace App\Http\Resources\Products;

use App\Http\Resources\AbstractResource;
use App\Support\Str;

class ProductResource extends AbstractResource
{
    protected $numberFormat = [
        'unit_cost', 'getFinalPrice', 'regular_price', 'mbr_price', 'promo_price', 'getPriceSavings', 'cost_price',
        'promo_cost', 'min_price', 'unit_savings', 'multiple_points'
    ];

    public function payload()
    {
        return [
            'trans_qty'   => 0,
            'item_id'     => trim($this->item_id),
            'item_desc'   => trim($this->item_desc),
            'short_desc'  => ($this->short_desc == '') ? $this->item_desc : $this->short_desc,
            'image_name'  => ($this->image_name == '') ? '' : CDN_PROD_THUMB . $this->image_name,
            'item_type'   => $this->item_type,
            'in_catalog'  => $this->in_catalog,
            'lot_control' => isset($this->lot_control) ? $this->lot_control : 'N',
            'price_tag_bc' => $this->price_tag_bc,
            'prices'      => $this->getPrices()
        ];
    }

    public function includes()
    {
        return [
            //'stocks' => new ProductStockCollection($this->whenLoaded('stocks')),
        ];
    }

    public function optional()
    {
        $optional = [];

        if (request('settings')) {
            //$promo_limits = app('api')->products()->getCheapPromotionalItem($this->item_id);
            $promo_limits = $this->getPromoLimits();
            $optional['settings']   = [
                'item_type'           => $this->item_type,
                'ext_warranty'        => $this->ext_warranty,
                'lot_control'         => $this->lot_control,
                'salesperson_control' => $this->salesperson_control,
                'status_level'        => $this->status_level,
                'allow_discount'      => $this->allow_discount,
                'allow_demo'          => (substr($this->inv_dim12,0,4)=='DEMO') ? true : false,//array_get($this->limit_options, 'is_demo', false),
                'limit_options'       => [
                    'limit_per_day'   => (isset($promo_limits) && $promo_limits->limit_per_day > 0) ? intval($promo_limits->limit_per_day) : -1,
                    'limit_per_mbr'   => (isset($promo_limits) && $promo_limits->limit_per_member > 0) ? intval($promo_limits->limit_per_member) : -1,
                    'limit_per_trans' => (isset($promo_limits) && $promo_limits->limit_per_trans > 0) ? intval($promo_limits->limit_per_trans) : -1,
                    'limit_stock_main'=> (isset($promo_limits) && $promo_limits->limit_stock_main > 0) ? intval($promo_limits->limit_stock_main) : -1,
                    'limit_stock_promo'=> (isset($promo_limits) && $promo_limits->limit_stock_promo > 0) ? intval($promo_limits->limit_stock_promo) : -1
                ],
                'item_options'        => $this->limit_options, // return as an item_options for verification (?)
                'dimensions'          => [
                    'inv_dim1' => $this->inv_dim1,
                    'inv_dim2' => $this->inv_dim2,
                    'inv_dim3' => $this->inv_dim3,
                    'inv_dim4' => $this->inv_dim4,
                    'brand_id' => $this->brand_id,
                    'model_id' => $this->model_id,
                ],
            ];
        }

        if (request('online_settings')) {
            $optional['online_settings'] = [
                'parent_item_id'   => $this->parent_item_id,
                'color_id'         => $this->color_id,
                'color_opt'        => $this->getColorOpt($this->item_id),
                'is_active'        => $this->is_active,
                'in_catalog'       => $this->in_catalog,
                'inv_type'         => $this->inv_type,
                'buffer_qty'       => $this->buffer_qty,
                'shipping_methods' => $this->delivery_options,
                'listing_from'     => $this->listing_from,
                'listing_to'       => $this->listing_to,
                'fulfilled_by'     => ($this->fulfilled_by && $this->fulfilled_by!='') ? $this->fulfilled_by : 'Challenger',
                'category_map'     => $this->getCategoryMap($this->inv_dim4,$this->inv_dim3,$this->inv_dim2,$this->inv_dim1),
            ];
        }

        return $optional;
    }

    public function getPrices()
    {

        if ($this->is_pwp) {
            // PWP Items need promo price only
            $price_dataset = [
                'unit_price' => (float)$this->promo_price,
                'regular_price' => (float)$this->regular_price,
                'promo_price' => (float)$this->promo_price,
                'promo_id' => $this->promo_id,
                'promo_ind' => $this->promo_ind,
                'price_savings' => (float)$this->unit_savings,
                'multiple_points' => (float)$this->multiple_points,
            ];
        }
        else {

            $price_dataset= app('api')->products()->getAllPrices($this->item_id, $this->multiple_points);

//            $price_dataset = [
//                'unit_price' => (float)$this->getFinalPrice(),
//                'regular_price' => (float)$this->regular_price,
//                'mbr_price' => (float)$this->mbr_price,
//                'promo_price' => (float)$this->promo_price,
//                'promo_id' => $this->promo_id,
//                'promo_ind' => $this->promo_ind,
//                'price_savings' => (float)$this->unit_savings,
//                'multiple_points' => (float)$this->multiple_points,
//                'disc_percent' => (float)$this->disc_percent ?? .0,
//                'unit_cost'       => (float) $this->unit_cost ?? 0,
//                'cost_price'      => (float) $this->cost_price ?? 0,
//                'promo_cost'      => (float) $this->promo_cost ?? 0,
//                'min_price'       => (float) $this->min_price ?? 0,
//                'staff_price' => (float)$this->getStaffPrice(),
//            ];

        }
        return $price_dataset;
    }

    protected function getFinalPrice()
    {
        return $this->getUnitPrice(Str::lower(request('customer_group')) === 'member');
    }

    protected function getPriceSavings()
    {
        if ($this->regular_price > 0) {
            return $this->regular_price - $this->getUnitPrice(true);
        }

        return 0;
    }

    protected function getRebateAmount()
    {
        $price = (float)$this->unit_point_multiplier * $this->getFinalPrice();

        if ($price) {
            $number = $price / 100;

            return (float)substr($number, 0, strpos($number, '.') + 3);
        }

        return 0;
    }

    protected function getPriceAfterRebateAmount()
    {
        return $this->getFinalPrice() - $this->getRebateAmount();
    }

    protected function getStaffPrice()
    {
        if (Str::upper(request('customer_group')) === 'STAFF') {
            return app('api')->products()->getStaffPrice($this->item_id);
        }

        return 0;
    }

    protected function getPromoLimits() {
        return app('api')->products()->getPromoItemLimit($this->promo_id, $this->item_id);
    }

    protected function getColorOpt($item_id)
    {
        if (!$this->is_pwp) {
            $result = app('api')->products()->getRelatedColor($this->parent_item_id);

            if ($result)
                return $result;
        }

        return [];
    }

    protected function getCategoryMap($inv_dim4,$inv_dim3='',$inv_dim2='',$inv_dim1='')
    {
        if (!$this->is_pwp) {
            $cate = app('api')->products()->getCategoryMapping($inv_dim4,$inv_dim3,$inv_dim2,$inv_dim1)->first();
            if ($cate) {

                $cateArr = [
                    0  => [
                        'id'    => $cate->h_category_id,
                        'name'  => $cate->cate_h
                    ],
                    1  => [
                        'id'    => $cate->m_category_id,
                        'name'  => $cate->cate_m
                    ],
                    2  => [
                        'id'    => $cate->s_category_id,
                        'name'  => $cate->cate_s
                    ],
                ];
                return $cateArr;
            }
        }

        return [];
    }

}