<?php

namespace App\Http\Resources\Members;

use App\Http\Resources\AbstractResource;
use Carbon\Carbon;

class MemberResource extends AbstractResource
{
    public function toArray($request)
    {
        return [
            'mbr_id'          => $this->mbr_id,
            'mbr_type'        => $this->mbr_type,
            'full_name'       => $this->first_name.' '.$this->last_name,
            'first_name'      => $this->first_name,
            'last_name'       => $this->last_name,
            'email'           => $this->email_addr,
            'contact_number'  => $this->contact_num,
            'joined_date'     => $this->join_date,
            'expiry_date'     => $this->exp_date,
            'is_expired'      => Carbon::now()->isPast($this->exp_date),
            'rebates_balance' => (float)$this->rebate,
        ];
    }
}