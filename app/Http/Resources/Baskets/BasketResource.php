<?php

namespace App\Http\Resources\Baskets;

use App\Core\Orders\Models\Order;
use App\Http\Resources\AbstractResource;
use App\Http\Resources\Members\MemberResource;

class BasketResource extends AbstractResource
{
    protected $numberFormat = ['total_amount', 'sub_total', 'tax_total', 'discount_total', 'getTotalSavings'];

    public function payload()
    {
        $data = [
            'id'                       => $this->id,
            'mbr_id'                   => $this->owner_id,
            'basket_name'              => $this->cart_name,
            'loc_id'                   => $this->loc_id,
            'pos_id'                   => $this->pos_id,
            'item_count'               => $this->item_count,
            'sub_total'                => $this->item_total,
            'shipping_total'           => $this->getCustomData('shipping_total', 0.00),
            'has_shipping'             => $this->has_shipping,
            'free_shipping_amount'     => 88.00,
            'buy_membership_card_only' => $this->containsMembershipCardOnly(),
            'buy_digital_only'         => $this->containsDigitalOnly(),
            'discount_total'           => $this->discount_total,
            'tax_total'                => $this->tax_amount,
            'tax_id'                   => $this->tax_id,
            'total_amount'             => $this->total_amount,
            'total_savings'            => $this->getTotalSavings(),
            'is_free_shipping'         => $this->getCustomData('is_free_shipping', false),
            'alerts'                   => $this->getCustomData('alerts'),
            'custom_data'              => $this->custom_data,
            'created_on'               => $this->created_on->format('Y-m-d H:i:s'),
            'extra_rebate_amount'      => $this->getAdditionalRebateAmount(),
        ];

        if (($order = $this->whenLoaded('activeOrder')) && $order instanceof Order) {
            $data['order_num'] = $order->order_num;
        }

        return $data;
    }

    public function includes()
    {
        $data = [
            'items'   => new BasketLineCollection($this->whenLoaded('lines')),
            'coupons' => new BasketDiscountLineCollection($this->whenLoaded('discounts')),
        ];

        !empty($this->owner_id) and $member = app('api')->members()->getById($this->owner_id) and $data['member'] = new MemberResource($member);

        return $data;
    }
}