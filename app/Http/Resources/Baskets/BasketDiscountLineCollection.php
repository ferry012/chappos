<?php

namespace App\Http\Resources\Baskets;

use App\Http\Resources\AbstractCollection;

class BasketDiscountLineCollection extends AbstractCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = BasketDiscountLine::class;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection;
    }
}