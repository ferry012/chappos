<?php

namespace App\Http\Resources\Baskets;

use App\Http\Resources\AbstractResource;

class BasketLineResource extends AbstractResource
{
    protected $numberFormat = ['regular_price', 'unit_price', 'price_total', 'total_savings'];

    public function payload()
    {
        $data = [];
        $img = '';

        if ($this->item_img) {
            $img = get_constant('s3.product_images_thumb') . $this->item_img;
        }

        $baseData = [
            'id'              => $this->encodedId(),
            'line_num'        => $this->line_num,
            'pwp_line_num'    => $this->pwp_line_num,
            'item_type'       => $this->item_type,
            'item_id'         => $this->item_id,
            'image_name'      => $this->item_img,
            'image_url'       => $img,
            'item_desc'       => $this->item_desc,
            'item_qty'        => $this->item_qty,
            'regular_price'   => $this->regular_price,
            'unit_price'      => $this->unit_price,
            'unit_discount'   => $this->unit_discount_amount,
            'valueclub_price' => $this->getCustomData('prices.valueclub', .0),
            'price_total'     => $this->price_total,
            'multiple_points' => $this->when($this->hasCustomData('multiple_points'), $this->getCustomData('multiple_points')),
            'total_savings'   => $this->total_savings,
            'delv_method'     => $this->delv_method,
            'loc_id'          => $this->loc_id,
            'loc_name'        => optional(pos_locations($this->loc_id))->shop_name ?? 'Unknown',
            'status_level'    => $this->status_level,
            'actions'         => $this->getCustomData('actions', ['editable' => true, 'deletable' => true]),
            'custom_data'     => $this->custom_data,
            'order_line_id'   => $this->when($this->hasCustomData('order_line.id'), $this->getCustomData('order_line.id')),
        ];

        if ($product = $this->resource->product) {
            $data = [
                'in_catalog'          => $product->in_catalog,
                'item_type'           => $product->item_type,
                'lot_control'         => $product->lot_control,
                'salesperson_control' => $product->salesperson_control,
            ];
        }

        return array_merge($baseData, $data);
    }

    public function includes()
    {
        return [];
    }
}