<?php

namespace App\Http\Resources\Baskets;

use App\Http\Resources\AbstractCollection;

class BasketLineCollection extends AbstractCollection
{
    /**
     * The resource that this resource collects.
     *
     * @var string
     */
    public $collects = BasketLineResource::class;

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->sortBy('line_num');
    }

}