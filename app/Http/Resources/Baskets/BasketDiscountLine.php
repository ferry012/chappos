<?php

namespace App\Http\Resources\Baskets;

use App\Http\Resources\AbstractResource;

class BasketDiscountLine extends AbstractResource
{
    public function payload()
    {
        return [
            'coupon_id'   => $this->coupon_id,
            'coupon_type' => $this->coupon_type,
            'coupon_name' => $this->coupon_name,
            'coupon_code' => $this->coupon_code,
            'is_applied'  => $this->is_applied,
            'require_ref' => $this->when($this->hasCustomData('require_ref'), function () {
                return $this->getCustomData('require_ref');
            }),
            'message'     => $this->error_msg,
        ];
    }

    public function includes()
    {
        return [];
    }
}