<?php

namespace App\Http\Resources\Files;

use App\Http\Resources\AbstractResource;

class PdfResource extends AbstractResource
{
    public function payload()
    {
        return [
            'encoding' => 'base64',
            'contents' => base64_encode($this->output()),
        ];
    }
}