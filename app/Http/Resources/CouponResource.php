<?php

namespace App\Http\Resources;


class CouponResource extends Resource
{
    public function toArray($request)
    {
        return [
            'mbr_id'        => $this->mbr_id,
            'coupon_id'     => $this->coupon_id,
            'coupon_code'   => $this->coupon_serialno,
            'coupon_amount' => $this->voucher_amount,
            'expiry_date'   => $this->expiry_date,
            'issue_type'    => $this->issue_type,
            'status_level'  => $this->status_level,
        ];
    }
}