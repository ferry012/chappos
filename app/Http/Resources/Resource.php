<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 3/8/2018
 * Time: 5:18 PM
 */

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource as BaseResource;

class Resource extends BaseResource
{
    public function __construct($resource = null)
    {
        parent::__construct($resource);
    }
}