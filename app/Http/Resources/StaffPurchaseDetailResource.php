<?php

namespace App\Http\Resources;


use Illuminate\Support\Carbon;

class StaffPurchaseDetailResource extends AbstractResource
{
    public function payload()
    {
        return [
            'staff_id'          => $this->staff_id,
            'staff_name'        => $this->staff_name,
            'financial_year'    => $this->financial_year,
            'purchase_limit'    => $this->purchase_limit,
            'purchase_balance'  => $this->purchase_balance,
            'last_purchased_on' => $this->last_purchased_on,
            'purchase_control'  => $this->getCustomData('purchase_control'),
            'joined_on'         => $this->joined_on,
            'length_of_service' => Carbon::parse($this->joined_on)->diffForHumans(null, true, false, 3),
            'status_level'      => $this->status_level,
        ];
    }
}