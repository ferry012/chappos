<?php

namespace App\Http\Resources;

use App\Repositories\MemberPointRepository;
use Carbon\Carbon;

class MemberResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'coy_id'                => $this->coy_id,
            'mbr_id'                => $this->mbr_id,
            'first_name'            => $this->first_name,
            'last_name'             => $this->last_name,
            'full_name'             => $this->fullName(),
            'birth_date'            => $this->birth_date,
            'email'                 => $this->email_addr,
            'contact_num'           => $this->contact_num,
            'joined_date'           => $this->join_date,
            'expiry_date'           => $this->exp_date,
            'last_renewal_date'     => $this->last_renewal,
            'total_savings'         => (float)$this->mbr_savings,
            'points'                => $this->getPointsDetail(),
            'is_login_locked'       => $this->login_locked === 'Y',
            'is_membership_expired' => Carbon::make($this->exp_date)->isPast(),
            'is_birth_month'        => Carbon::make($this->birth_date)->isCurrentMonth(),
        ];
    }

    protected function fullName()
    {
        if ($this->last_name) {
            return $this->first_name.' '.$this->last_name;
        }

        return $this->first_name;
    }

    protected function getPointsDetail()
    {
        $pointsBalance = with(new MemberPointRepository)->balancePoints($this->mbr_id);
        $pointsBalance -= $this->points_reserved;

        return [
            'accumulated' => (int)$this->points_accumulated,
            'reserved'    => (int)$this->points_reserved,
            'redeemed'    => (int)$this->points_redeemed,
            'expired'     => (int)$this->points_expired,
            'balance'     => $pointsBalance,
            'to_amount'   => with(new MemberPointRepository)->pointRedemptionValue($pointsBalance),
        ];
    }
}
