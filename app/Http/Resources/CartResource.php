<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'mbr_id'         => $this->mbr_id,
            'salesperson_id' => $this->total_express_fee,
            'store_id'       => $this->store_id,
            'total_items_count',
            'total_qty',
            'subtotal_amount',
            'tax_amount',
            'grand_total',
            'total_savings_amount',
            'referral_code',
            'secret'         => $this->when(0, 'secret-value'),
        ];
    }
}
