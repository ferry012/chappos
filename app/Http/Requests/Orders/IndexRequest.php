<?php

namespace App\Http\Requests\Orders;

use App\Http\Requests\FormRequest;

class IndexRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'from'     => 'date_format:Y-m-d',
            'to'       => 'date_format:Y-m-d',
            'per_page' => 'numeric|max:500',
        ];

        return $rules;
    }
}