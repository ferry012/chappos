<?php

namespace App\Http\Requests\Orders;

use App\Http\Requests\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'basket_id'  => 'required',
            'order_type' => 'required',
        ];

        if ($this->get('type') === 'PS') {
            $rules['pos_id'] = 'required|max:2';
        }

        if (in_array($this->order_type, ['PS', 'DU', 'EX', 'RF', 'E1'])) {
            $rules['pos_id'] = 'required|max:2';
        }

        if (in_array($this->order_type, ['DU', 'EX', 'RF', 'E1'])) {
            $rules['order_ref_id'] = 'required';
        }

        return $rules;
    }
}