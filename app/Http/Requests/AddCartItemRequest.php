<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 11/10/2018
 * Time: 4:22 PM
 */

namespace App\Http\Requests;

use AlbertCht\Form\FormRequest;

class AddCartItemRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'mac_address' => 'required|unique:devices,mac_address',
        ];
    }
}