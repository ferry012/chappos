<?php

namespace App\Http\Requests\Baskets;

use App\Http\Requests\FormRequest;

class ApplyRebatesRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'mbr_id'        => 'required',
            'rebate_amount' => 'required|numeric|min:0.01',
        ];

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'rebate_amount.min' => 'The rebate amount must be at least $0.01.',
        ];
    }
}