<?php

namespace App\Http\Requests\Baskets;

use App\Http\Requests\FormRequest;

class CancelRebatesRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
            'mbr_id' => 'required',
        ];

        return $rules;
    }
}