<?php

namespace App\Http\Requests\Baskets;

use App\Http\Requests\FormRequest;

class AddRequest extends FormRequest
{
    public function rules()
    {
        $rules = [
         //   'item_id'  => 'required',
        //    'item_qty' => 'required',

        ];

        if ($this->has('pwp')) {
            $rules = array_merge($rules, [
                'pwp'            => 'array',
                'pwp.*.item_id'  => 'required',
                'pwp.*.item_qty' => 'required|numeric|min:1|max:10000',
            ]);
        }

        return $rules;
    }
}