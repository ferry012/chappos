<?php

namespace App\Http\Requests\Baskets;

use App\Http\Requests\FormRequest;

class AddToOrderRequest extends FormRequest
{
    public function rules()
    {

        $rules = [
            'basket_id'  => 'required',
            'order_type' => 'required',
        ];

        if ($this->has('order_type')) {
            if (in_array($this->order_type, ['PS', 'DU', 'EX', 'RF', 'E1'])) {
                $rules['pos_id'] = 'required|max:2';
            }

            if (in_array($this->order_type, ['DU', 'EX', 'RF', 'E1'])) {
                $rules['order_ref_id'] = 'required';
            }
        }

        if ($this->has('pwp')) {
            $rules = array_merge($rules, [
                'pwp'            => 'array',
                'pwp.*.item_id'  => 'required',
                'pwp.*.item_qty' => 'required|numeric|min:1|max:10000',
            ]);
        }

        return $rules;
    }
}