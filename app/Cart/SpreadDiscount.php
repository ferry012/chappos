<?php

namespace App\Cart;


use App\Contracts\Pipeline\Pipe;
use Closure;

class SpreadDiscount implements Pipe
{
    public function handle($cart, Closure $next)
    {
        if (0 < $cart->total_discount_amount) {

            $grandTotal = $cart->items->reduce(function ($carry, $item) {

                if (! empty($item->coupon_code)) {
                    $carry += $item->item_qty * $item->unit_price;
                }

                return $carry;
            });

            $cart->items->map(function ($item) use ($cart, $grandTotal) {

                // only apply item with coupon code
                if (! empty($item->coupon_code)) {
                    $total             = ($item->item_qty * $item->unit_price);
                    $discAmt           = (($total / $grandTotal) * $cart->total_discount_amount) / $item->item_qty;
                    $item->disc_amount = round($discAmt, 3, 1);
                }

                return $item;
            });

        } else {
            $cart->coupon_code = '';
            $cart->items->map(function ($item) {
                if (! empty($item->coupon_code)) {
                    $item->coupon_code = '';
                    $item->disc_amount = .0;
                }

                return $item;
            });
        }

        return $next($cart);
    }

}