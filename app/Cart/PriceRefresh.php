<?php

namespace App\Cart;


use App\Contracts\Pipeline\Pipe;
use Closure;

class PriceRefresh implements Pipe
{

    public function handle($cart, Closure $next)
    {
        $cart->items->each(function ($item) {
            // reset
            $item->unsetCustomData('error.item');
            $error              = '';
            $item->status_level = 0;

            //@todo update allowed delivery method
            $product = $this->productRepo->getOne($item->item_id);

            //$item->is_preorder = $product->is_preorder;

            // update main item price
            if (! $item->is_pwp_item && $product) {

                $item->regular_price = $product->regular_price;
                $item->member_price  = $product->mbr_price;
                $item->promo_price   = $product->promo_price;
                //$item->unit_point_multipler = $product->unit_point_multipler;

                /*
                $item->available_delivery_method = $this->buildDeliveryMethods($product);

                if (! empty($item->available_delivery_method)) {
                    if (! in_array($item->delivery_method, explode(',', $item->available_delivery_method)) && in_array($item->delivery_method, $this->except)) {
                        $error = 'Delivery method not found!';
                    }
                }
                */

                if ($product->is_active !== 'Y') {
                    $error = 'This item is currently not active in the product catalogue.';
                } else if ($item->item_qty > 20) {
                    $error = 'Please note that any order for an item with quantity exceeding 20 can only be done via our Corporate Sales.';
                }

            } else if (null === $product) {
                $error = 'off-the-shelf item, please remove it!';
            }

            // update purchase with purchase item
            if ($item->is_pwp_item) {
                $product3 = db('sql')->table('o2o_live_promo_gitem')
                    ->where('promo_item_id', $item->item_id)
                    ->where('item_id', $item->parent_id)
                    ->first(['promo_price', 'unit_point_multipler']);

                if (0 < count($product3)) {
                    $item->regular_price = $product3->promo_reg_price;
                    $item->member_price  = $product3->promo_mbr_price;
                    $item->promo_price   = $product3->promo_price;
                    //$item->is_free_item         = (int)(0 <= $item->promo_price);
                    //$item->unit_point_multipler = $product3->unit_point_multipler;
                } else {
                    $error = 'off-the-shelf item, please remove it!';
                }
            }

            // overwrite final unit price
            // update main item qty step price
            if (! $item->is_pwp_item && $item->item_qty > 1) {
                $product2 = db('sql')->table('o2o_live_promo_bitem')
                    ->where('promo_cat', '<>', 'PWP')
                    ->where('item_id', $item->item_id)
                    ->where([
                        ['min_qty', '<=', $item->item_qty],
                        ['max_qty', '>=', $item->item_qty],
                    ])->first(['promo_type', 'unit_reg', 'unit_mbr', 'unit_point_multipler']);

                if ($product2) {
                    // @todo need to rise the price logic
                    $item->regular_price = $product2->unit_reg;
                    $item->member_price  = $product2->unit_mbr;
                    //$item->unit_point_multipler = $product2->unit_point_multipler;
                }
            }

            // @todo update store location price

            // check store collection (include in store id) that not in the collection list
            /*
            if ($item->delv_method === 'collection' && ! $this->canBeCollectFromStore($item->store_collection)) {
                $error = 'This product cannot be collected from this location - ' . $item->store_collection;
            }
            */

            // final unit price
            $item->unit_price = $this->getUnitPrice($item);

            if (! empty($error)) {
                $item->status_level = -1;
                $item->setCustomData('error.item', $error);
            }

            $item->save();
        });

        $next($cart);
    }
}