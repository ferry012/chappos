<?php


namespace App\Services;

use App\Services\Traits\Service as ServiceTrait;
use Illuminate\Contracts\Container\Container as Application;

abstract class  Service
{
    use  ServiceTrait;

    protected $app;
    protected $db;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->db  = $app->get('db');
    }

    public function db()
    {
        return $this->db;
    }
}