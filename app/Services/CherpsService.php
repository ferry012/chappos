<?php
/**
 * Created by PhpStorm.
 * User: YS
 */

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CherpsService
{
    public function __construct()
    {
    }

    public function sys_trans_list($prefix, $doc_type, $sys_id, $coy_id = 'CTL', $length=8, $date_format = 'CHAR'){

        if ($date_format=='NONE') {
            $prefix = $prefix; // Use on return doc_id
            $trans_prefix = $doc_type; // In database sys_trans_list
        }
        else {
            $prefix = $prefix . $this->_date_format($date_format); // Use on return doc_id
            $trans_prefix = $doc_type . $prefix; // In database sys_trans_list
        }

        $sys_list = db('pg_cherps')->table('sys_trans_list')->where(['coy_id'=>$coy_id,'sys_id'=>$sys_id])->where('trans_prefix', $trans_prefix);

        if ($sys_list->exists()) {
            $next_num = $sys_list->first()->next_num;
            $sys_list->update([
                'next_num' => $next_num + 1
            ]);
        } else {
            $next_num = 1;
            db('pg_cherps')->table('sys_trans_list')
                ->insert([
                    'coy_id'    => $coy_id,
                    'sys_id'    => $sys_id,
                    'trans_prefix'=> $trans_prefix,
                    'next_num'  => 2,
                    'sys_date'  => Carbon::now()->toDateTimeString()
                ]);
        }

        $len = $length - strlen($prefix);
        $trans_id = $prefix . sprintf("%0".$len."d", $next_num);

        return $trans_id;
    }

    private function _date_format($format) {

        if ($format == 'YY')
            $prefix = date('y');
        else if ($format == 'YYMM')
            $prefix = date('ym');
        else {
            $char = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
            $pos = date('Y') - 2018;
            $prefix = $char[$pos];
        }

        return (string) $prefix;
    }
}