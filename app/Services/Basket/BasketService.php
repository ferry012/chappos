<?php

namespace App\Services\Basket;


use Illuminate\Support\Facades\DB;

class BasketService
{

    public function getTaxPercent($query)
    {
        if($query->getTable() === 'o2o_order_list'){
            $db = $query->where('order_num',$query['ref_id'])->first();
        } else {
            $db = $this->getTaxID($query);
        }
        $str =  str_replace("SG","",$db['tax_id'] ?? $db);
        return (int)$str;

    }

    public function utilizeCoupon($coupons,$order)
    {

        if(count($coupons) > 0){
            $data = [
                "coupons" => $coupons,
                "mbr_id" => $order->customer_id,
            ];
//            dd(json_encode($data));
            $endpoint = env('API_URL_VALUECLUB');
            $endpoint .= 'api/coupon/utilize/';
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $response = $client->post($endpoint, [
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => 'Bearer ' . env('API_URL_VALUECLUB_TOKEN')],
                'body'    => json_encode($data),
            ]);
            $content = $response->getBody()->getContents();
            $content_data = json_decode($content, true);

            return $content_data;
        }
    }

    public function getTaxID($basket)
    {
        $order = $basket->join('o2o_order_list as a','a.cart_id','o2o_cart_list.id')
            ->where('o2o_cart_list.id',$basket['id'])
            ->select(DB::raw('(SELECT tax_id FROM o2o_order_list WHERE order_num = a.ref_id) as tax_id'))
            ->get();

        // This will check when cart is created prior to o2o_order_list is created
        if(count($order->toArray()) > 0){
            // Check if order is refund
            if($order[0]->tax_id){
                $basket['tax_id'] = $order[0]->tax_id;
            } else{
                $basket['tax_id'] = gst()->tax_id;
            }
        } else {
            return gst()->tax_id;
        }

        return $basket;
    }

    public function checkBasketRefund($order)
    {
       $data =  $this->getOriginalOrder($order);

        if(isset($order['custom_data']['refund_reason'])){
            $tax_id =  $data['old']->tax_id;
        } else{
            $tax_id = gst()->tax_id;
        }
        return $tax_id;
    }

    public function getFormerOrder($order)
    {
        $order['tax_id'] = $this->checkBasketRefund($order);
        return $order;
    }

    private function getOriginalOrder($order){
        $old = db()->table('o2o_order_list')->where('order_num', $order['ref_id'])
            ->first();

        return [
            'old' => $old,
        ];
    }
}
