<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ImsapiService
{

    public function __construct()
    {
        $this->Endpoint = env('API_IMSURL');
        $this->Authorization = 'Bearer ' . env('API_IMSTOKEN');
    }

    public function confirm_reserved($doc_type, $trans_type, $invoice_id, $loc_id, $items, $staff_id) {

//        $items[] = [
//            "item_id" => '000000000000',
//            "line_num" => 1,
//            "qty_reserved" => 1
//        ];

        $request = [
            "coy_id" => "CTL",
            "loc_id" => $loc_id,
            "doc_type" => "ORDER",
            "trans_type" => $trans_type,
            "trans_id" => $invoice_id,
            "items" => $items,
            "modified_by" => $staff_id,
        ];

        $apiurl = $this->Endpoint . 'api/confirm_reserved';
        $apibody = [
            'headers' => ['Content-Type' => 'application/json', 'Authorization' => $this->Authorization],
            'http_errors' => false,
            'body' => json_encode($request)
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->post($apiurl,$apibody);
        $return = json_decode($response->getBody()->getContents(),true);

        return $return;
    }

}
