<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 23/7/2018
 * Time: 10:57 AM
 */

namespace App\Services;


use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Support\Collection;

class InventoryService
{
    public function deductByLocation(Order $order)
    {
        if ($order->items->isEmpty() || empty($order->loc_id)) {
            //  return false;
        }

        $products = db('pg_cherps')->table('ims_item_list')->where('coy_id', 'CTL')->where('item_id', $order->items->pluck('item_id'))->get()->map(function ($row) {
            $row->item_id   = trim($row->item_id);
            $row->item_type = strtoupper(trim($row->item_type));

            return $row;
        });
        $insData  = [];

        foreach ($order->items as $item) {
            //reset
            $insData = [];
            $product = $products->where('item_id', $item->item_id)->first();

            $orderedQty = $item['qty_ordered'];

            if ($product) {
                // only physical goods are deductible
                if ($product->item_type === 'I') {
                    $stocks = db('pg_cherps')->table('ims_inv_physical')->where('coy_id', 'CTL')->where('loc_id', $order->loc_id)->where('item_id', $item['item_id'])->orderBy('line_num')->get();

                    if ($stocks->isNotEmpty()) {
                        foreach ($stocks as $stock) {

                            if (0 >= $orderedQty) {
                                break;
                            }

                            $balQty = (int)$stock->qty_on_hand - (int)$stock->qty_reserved;

                            if (0 >= $balQty) {
                                continue;
                            }

                            if ($balQty >= $orderedQty) {
                                $stock->qty_on_hand -= $orderedQty;
                                $allocatedQty       = $orderedQty;
                                $orderedQty         = 0;
                            } else {
                                $stock->qty_on_hand -= $balQty;
                                $orderedQty         -= $balQty;
                                $allocatedQty       = $balQty;
                            }

                            $stock->modified_on = now();
                            $stock->modified_by = 'cherps';

                            $updData = array_only((array)$stock, ['qty_on_hand', 'modified_on', 'modified_by']);

                            $refId = $this->buildStockRefId($stock);

                            db('pg_cherps')->table('ims_inv_physical')
                                ->where([
                                    'coy_id'   => 'CTL',
                                    'loc_id'   => $order->loc_id,
                                    'item_id'  => $item->item_id,
                                    'line_num' => $stock->line_num,
                                    'ref_id'   => $refId,
                                ])->update($updData);

                            $insData[] = [
                                'order_item_id' => $item->id,
                                'item_id'       => $item->item_id,
                                'ref_id'        => $refId,
                                'qty'           => $allocatedQty,
                                'inv_loc'       => $stock->loc_id,
                                'loc_id'        => $stock->loc_id,
                                'delv_mode_id'  => 'ISC',
                                'status_level'  => 1,
                            ];

                        }
                    }

                    // if unable to reduce stock complete
                    if (0 < $orderedQty || $stocks->isEmpty()) {
                        $insData[] = [
                            'order_item_id' => $item->id,
                            'item_id'       => $item->item_id,
                            'qty'           => $orderedQty,
                            'remarks'       => 'unable to allocate stock',
                            'status_level'  => 1,
                        ];
                    }

                } else {
                    $insData[] = [
                        'order_item_id' => $item->id,
                        'item_id'       => $item->item_id,
                        'qty'           => $orderedQty,
                        'remarks'       => 'virtual item',
                        'status_level'  => 1,
                    ];
                }

            } else {
                $insData[] = [
                    'order_item_id' => $item->id,
                    'item_id'       => $item->item_id,
                    'qty'           => $orderedQty,
                    'remarks'       => 'item not found in ims_item_list',
                    'status_level'  => 1,
                ];
            }

        }

        if ($insData) {
            // bulk insert
            db()->table('o2o_order_stock_log')->insert([$insData]);

            return true;
        }

        return false;
    }

    public function prepareAllocateStock(Order $order)
    {

        $items         = $order->items;
        $masterItems   = db('pg_cherps')->table('ims_item_list')->where('coy_id', 'CTL')->whereIn('item_id', $items->pluck('item_id'))->get();
        $stdDelvMethod = 'NHB';
        $stockLog      = collect();
        $newStockLog   = collect();
        $tmpStockLog   = collect();

        $inStore = $order->loc_id;

        $masterItems->each(function ($item) {
            $item->item_id  = trim($item->item_id);
            $item->inv_type = trim($item->inv_type);
        });

        $items = $items->each(function ($item) use ($masterItems, $inStore, $stockLog, $stdDelvMethod) {

            $delvModeId = null;
            $locId      = '';
            $invLocId   = $item->loc_id;

            $item->status = 0;

            $product = $masterItems->where('item_id', $item->item_id)->first();

            if (($item->loc_id === 'AUC' || $item->loc_id === 'LAC') && $item->delv_method === 'standard') {
                $locId        = 'HCL';
                $delvModeId   = $stdDelvMethod;
                $item->status = 1;
            }

            // Need to check ims_inv_physical for loc_id
            /*
            if ($item->loc_id === 'AUC' && $item->delv_method === 'collection') {
                $locId        = ''; // store id
                $delvModeId   = 'SCL';
                $item->status = 1;
            }

            if ($item->loc_id === 'LAC' && $item->delv_method === 'collection') {
                $locId        = ''; // store id
                $delvModeId   = 'ISC';
                $item->status = 1;
            }
            */

            if ($item->loc_id === 'PREBB' && $item->delv_method === 'collection') {
                $locId        = 'HCL'; // store id
                $delvModeId   = $stdDelvMethod;
                $item->status = 1;
            }

            // etc membership card / google card / gift card
            if (in_array(trim($product->item_type), ['M', 'E', 'W', 'T'], true)) {
                $invLocId     = '';
                $item->status = 1;
            }

            // in-store purchase
            if (! empty($inStore) && $inStore === $item->loc_id && $item->delv_method === 'collection') {
                $locId        = $item->loc_id;
                $delvModeId   = 'ISC';
                $item->status = 1;
            } elseif (! empty($item->loc_id) && $item->delv_method === 'collection') {
                $locId        = $item->loc_id;
                $delvModeId   = 'SCL';
                $item->status = 1;
            }

            if ($item->inv_type === 'HSZ' || $item->inv_type === 'HSZ-C') {
                $invLocId     = 'HSZ';
                $locId        = 'HSZ';
                $delvModeId   = 'HSZ';
                $item->status = 1;
            }

            if ($item->inv_type === 'HSZ-C') {
                $invLocId = 'HSZ-C';
            }

            if (trim($product->inv_type) === 'PRE') {
                $item->status = 1;
            }

            if ($item->status === 1) {
                $stockLog->push([
                    'order_item_id' => $item->id,
                    'item_id'       => trim($item->item_id),
                    'ref_id'        => '',
                    'qty'           => $item->qty_ordered,
                    'inv_loc'       => $invLocId,
                    'loc_id'        => $locId,
                    'delv_mode_id'  => $delvModeId,
                    'remarks'       => '',
                ]);

            }

        })->reject(function ($item) {
            return $item->status;
        });

        $items->where('delv_method', 'SDD')->each(function ($item) use (&$newStockLog, &$tmpStockLog) {

            // BF is handling the express delivery items
            $stocks = db('pg_cherps')->table('ims_inv_physical')->where('coy_id', 'CTL')->where('loc_id', 'BF')->where('item_id', $item->item_id)->get();

            $r = $this->allocateStock($item, $stocks, $tmpStockLog);

            if (null !== $r) {
                $tmpStockLog = $tmpStockLog->concat($r);
                $newStockLog = $newStockLog->concat($r);
            }
        });

        if ($newStockLog->isNotEmpty()) {
            $stockLog = $stockLog->concat($newStockLog);

            $items       = $items->each(function ($item) use ($stockLog) {
                if ($stockLog->where('order_item_id', $item->id)->count()) {
                    $item->status = 1;
                }
            })->reject(function ($item) {
                return $item->status;
            });
            $newStockLog = collect();
            $tmpStockLog = $stockLog;
        }

        collect(['HCL', 'VEN-C', 'PRE00'])->each(function ($loc) use ($items, &$newStockLog, &$tmpStockLog) {

            $stocks = db('pg_cherps')->table('ims_inv_physical')->where('coy_id', 'CTL')->where('loc_id', $loc)->whereIn('item_id', $items->pluck('item_id'))->get();

            $items->each(function ($item) use ($stocks, &$newStockLog, &$tmpStockLog) {

                $r = $this->allocateStock($item, $stocks, $tmpStockLog);

                if ($r) {
                    $tmpStockLog = $tmpStockLog->concat($r);
                    $newStockLog = $newStockLog->concat($r);
                }
            });

        });

        if ($newStockLog->isNotEmpty()) {
            $stockLog = $stockLog->concat($newStockLog);

            $items       = $items->each(function ($item) use ($stockLog) {
                if ($stockLog->where('order_item_id', $item->id)->count()) {
                    $item->status = 1;
                }
            })->reject(function ($item) {
                return $item->status;
            });
            $newStockLog = collect();
            $tmpStockLog = $stockLog;
        }

        $qualifiedForOneLocation = $masterItems->whereIn('inv_type', [
                'LCH', 'BL-C', 'BRO-C', 'DSH-C', 'EA-C', 'HSZ-C', 'IM-C', 'IN-C', 'SL-C', 'TH-C', 'UP-C', 'VEN-C',
            ])->count() === 0;

        $fulfilledByOneLocation = false;

        if ($qualifiedForOneLocation) {

            collect([
                'VEN-C', 'HCL', 'BF', 'CP', 'JEM', 'JP', 'NEX', 'TP', 'VC', 'IM-C', 'HSG',
            ])->each(function ($locId) use ($items, &$fulfilledByOneLocation, &$newStockLog, &$tmpStockLog) {

                $fulfilledByOneLocation = false;
                $stocks                 = db('pg_cherps')->table('ims_inv_physical')->where('coy_id', 'CTL')->where('loc_id', $locId)->whereIn('item_id', $items->pluck('item_id'))->get();

                if ($stocks->isEmpty()) {
                    // continue
                    return true;
                }

                // start item with less stock detail
                $items->each(function ($item) use ($stocks, $locId) {
                    $item->stock_detail_count = $stocks->map(function ($stock) {
                        $stock->loc_id  = trim($stock->loc_id);
                        $stock->item_id = trim($stock->item_id);

                        return $stock;
                    })->where('loc_id', $locId)->where('item_id', $item->item_id)->count();
                })->sortBy('stock_detail_count')->each(function ($item) use ($stocks, &$fulfilledByOneLocation, &$newStockLog, &$tmpStockLog) {

                    $fulfilledByOneLocation = true;

                    $r = $this->allocateStock($item, $stocks, $tmpStockLog, ['one_location' => true]);

                    if ($r) {
                        $tmpStockLog = $tmpStockLog->concat($r);
                        $newStockLog = $newStockLog->concat($r);
                    } else {

                        // cannot fulfilled one location, reset tmp log and break the loop
                        $newStockLog            = collect();
                        $tmpStockLog            = collect();
                        $fulfilledByOneLocation = false;

                        return false;
                    }

                });

                // break loop once fulfilled
                if ($fulfilledByOneLocation) {
                    return false;
                }

            });

            if ($newStockLog->isNotEmpty()) {
                $stockLog = $stockLog->concat($newStockLog);

                $items       = $items->each(function ($item) use ($stockLog) {
                    if ($stockLog->where('order_item_id', $item->id)->count()) {
                        $item->status = 1;
                    }
                })->reject(function ($item) {
                    return $item->status;
                });
                $newStockLog = collect();
                $tmpStockLog = $stockLog;
            }

        }


        if (! $fulfilledByOneLocation) {
            // Fulfilled by multiple store
            $items->each(function ($item) use ($masterItems, &$newStockLog, &$tmpStockLog) {
                $product = $masterItems->where('item_id', $item->item_id)->first();

                $query = db('pg_cherps')->table('ims_inv_physical')
                    ->where('coy_id', 'CTL')
                    ->where('item_id', $item->item_id)
                    ->whereRaw('qty_on_hand - qty_reserved > 0');

                if ($product->inv_type === 'LCH') {
                    $stocks = $query->whereIn('loc_id', ['HCL', 'PRE00'])->get();
                } else {
                    $stocks = $query->whereIn('loc_id', [
                        'VEN-C', 'BF', 'IM-C', 'HSG', 'VC', 'NEX', 'CP', 'JEM', 'JP', 'TP', 'PRE01', 'PRE02', 'PRE03',
                        'PRE04', 'PRE05', 'PRE06', 'PRE07', 'DSH-C', 'EA-C', 'HSZ', 'HSZ-C', 'IN-C', 'SL-C', 'TH-C',
                        'UP-C',
                        'BL-C', 'BRO-C',
                    ])->get();

                    // set location priority
                    $stocks->each(function ($stock) {
                        $locId = trim($stock->loc_id);

                        $stock->priority = 0;

                        if (in_array($locId, [
                            'BL-C', 'BRO-C', 'DSH-C', 'EA-C', 'HSZ-C', 'IM-C', 'IN-C', 'SL-C', 'TH-C', 'UP-C', 'VEN-C',
                        ], true)) {
                            $stock->priority = 1;
                        } elseif ($locId === 'HSG') {
                            $stock->priority = 9999;
                        }

                    });
                }

                $r = $this->allocateStock($item, $stocks, $tmpStockLog, ['priority' => true, 'force_insert' => true]);

                if ($r) {
                    $tmpStockLog = $tmpStockLog->concat($r);
                    $newStockLog = $newStockLog->concat($r);
                }
            });

            if ($newStockLog->isNotEmpty()) {
                $stockLog    = $stockLog->concat($newStockLog);
                $tmpStockLog = null;
                $newStockLog = null;
            }
        }

        // reduce on hand or reserved stock
        $stockLog->reject(function ($item) {
            // remove those items that unable to deduct/reserved stock
            return empty($item['ref_id']);
        })->each(function ($stock) {
            list($locId, $lineNum, $refId) = explode('/', $stock['ref_id']);

            db('pg_cherps')->table('ims_inv_physical')
                ->where('coy_id', 'CTL')
                ->where('item_id', $stock['item_id'])
                ->where('loc_id', $locId)
                ->where('line_num', $lineNum)
                ->where('ref_id', $refId)
                ->update([
                    'qty_reserved' => db('pg_cherps')->raw(' qty_reserved + '.$stock['qty']),
                ]);

        });

        // final overwrite
        $stockLog = $stockLog->map(function ($stock) use ($stdDelvMethod) {

            if ($stock['delv_mode_id'] === 'SCL' && 0 === strpos($stock['inv_loc'], 'PRE')) {
                $stock['loc_id'] = optional(db('pg_cherps')->table('o2o_live_outlet')->where('loc_id', 'PRE02')->first(['supp_id']))->supp_id;
            }

            if ($stock['delv_mode_id'] === 'ISC' && $stock['inv_loc'] === 'PRE00') {
                $stock['loc_id'] = 'HCL';
            }

            // standard only handle by BF
            if ($stock['delv_mode_id'] === 'standard' && 0 === strpos($stock['inv_loc'], 'PRE')) {
                $stock['loc_id'] = 'BF';
            }

            if ($stock['delv_mode_id'] === 'standard' && 'PRE07' === $stock['inv_loc']) {
                $stock['loc_id']       = 'BF';
                $stock['delv_mode_id'] = $stdDelvMethod;
            }

            if ('DSH-C' === $stock['inv_loc']) {
                $stock['loc_id']       = 'DSH';
                $stock['delv_mode_id'] = 'DSH';
            }

            if ('IM-C' === $stock['inv_loc']) {
                $stock['loc_id']       = 'HCL';
                $stock['delv_mode_id'] = $stdDelvMethod;
            }

            return $stock;
        });

        $SSEW_items = db('sql')->table('o2o_temp_ssew')->pluck('promo_item_id')->toArray();

        array_walk_recursive($SSEW_items, 'trim');

        $stockLog = $stockLog->map(function ($item) use ($order, $SSEW_items) {
            $item['order_id'] = $order->id;
            if (in_array($item['item_id'], $SSEW_items, false)) {
                $item['inv_loc'] = 'HCL';
            }

            return $item;
        });

        $preOrderItemCount = $stockLog->filter(function ($stock) {
            return 0 === strpos($stock['inv_loc'], 'PRE');
        })->count();

        // any pre-order items with order standard delivery items are handle by one place to avoid n items, Bugis Flag(BF) store
        if (0 < $preOrderItemCount) {
            $stockLog = $stockLog->map(function ($stock) {

                if ($stock['delv_method'] === 'standard') {
                    $stock['loc_id'] = 'BF';
                }

                return $stock;
            });
        }


        //dd(db('sql')->getQueryLog());
        // dd(collect(db('sql')->getQueryLog())->sum('time'));
        if ($stockLog->isNotEmpty()) {
            db()->table('o2o_order_stock_log')->insert($stockLog->sortBy('order_item_id')->values()->toArray());

            return true;
        }

        return false;
    }

    public function allocateStock(OrderItem $item, Collection $stocks, Collection $stockLog, Array $options = [])
    {
        //$oneLocation = array_get($options, 'one_location', false);
        $priority    = array_get($options, 'priority', false);
        $forceInsert = array_get($options, 'force_insert', false);
        $return      = collect();
        $orderedQty  = $item->qty_ordered;

        // deduct allocated stock from temp data
        $stocks->each(function ($stock) use ($stockLog) {
            $stock->item_id = trim($stock->item_id);
            $balanceQty     = $stock->qty_on_hand - $stock->qty_reserved;

            if ($stockLog->isNotEmpty()) {
                // build ref id string
                $refId      = $this->buildStockRefId($stock);
                $balanceQty -= $stockLog->where('item_id', $stock->item_id)->where('ref_id', $refId)->sum('qty');
            }

            $stock->qty_balance = $balanceQty;
        });

        $stocks = $stocks->where('item_id', $item->item_id)
            ->where('qty_balance', '>', 0)
            ->sortBy('line_num')
            ->when($priority, function ($collection) {
                return $collection->sortBy('priority')->mapToGroups(function ($item) {
                    // location priority
                    return [$item->priority => $item];
                })->map(function ($collection) {
                    // deduct from the early stock line number
                    return $collection->sortBy('line_num');
                })->flatten(1)->values();
            })
            ->each(function ($stock) use ($item, &$orderedQty, $return) {

                if (0 >= $orderedQty) {
                    return false;
                }

                if ($stock->qty_balance >= $orderedQty) {
                    $allocatedQty = $orderedQty;
                    $orderedQty   = 0;
                } else {
                    $orderedQty   -= $stock->qty_balance;
                    $allocatedQty = $stock->qty_balance;
                }

                $refId = $this->buildStockRefId($stock);

                $return->push([
                    'order_item_id' => $item->id,
                    'item_id'       => trim($item->item_id),
                    'ref_id'        => $refId,
                    'qty'           => $allocatedQty,
                    'inv_loc'       => trim($stock->loc_id),
                    'loc_id'        => trim($stock->loc_id),
                    'delv_mode_id'  => '',
                    'remarks'       => '',
                ]);
            });

        // unable to allocate stock will be handled by HCL
        if ($forceInsert && (0 < $orderedQty || $stocks->isEmpty())) {

            $remarks = 'unable to allocate stock.';

            $return->push([
                'order_item_id' => $item->id,
                'item_id'       => trim($item->item_id),
                'ref_id'        => '',
                'qty'           => $orderedQty,
                'inv_loc'       => 'HCL',
                'loc_id'        => 'HCL',
                'delv_mode_id'  => '',
                'remarks'       => $remarks,
            ]);

        } elseif (0 < $orderedQty || $stocks->isEmpty()) {
            return null;
        }

        return $return->toArray();
    }

    public function releaseReservedStock(Order $order)
    {
        // get the reserved stock
        $stocks = db()->table('o2o_order_stock_log')->where('order_id', $order->id)->where('status_level', 1)->get();

        if ($stocks && is_array($stocks)) {
            foreach ($stocks as $stock) {
                if (! empty($stock->ref_id)) {
                    list($locId, $lineNum, $refId) = explode('/', $stock->ref_id);

                    db('pg_cherps')->table('ims_inv_physical')->where('loc_id', $locId)->where('line_num', $lineNum)->where('ref_id', $refId)->update([
                        'qty_reserved' => db('sql')->raw(' qty_reserved - '.$stock->qty),
                    ]);

                    // update the status as stock return
                    db()->table('o2o_order_stock_log')->where('order_id', $order->id)->where('ref_id', $stock->ref_id)->update(['status_level' => 3]);
                }
            }

            return true;
        }

        return false;
    }

    public function buildStockRefId($stock)
    {
        // build ref id string
        $refId = implode('/', array_only((array)$stock, ['loc_id', 'line_num', 'ref_id']));
        $refId = str_replace(' ', '', $refId);

        return $refId;
    }
}