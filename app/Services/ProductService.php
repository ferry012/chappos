<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 11/7/2018
 * Time: 2:31 PM
 */

namespace App\Services;


use App\Repositories\InventoryRepository;
use App\Repositories\ProductRepository;

class ProductService
{
    public function __construct(ProductRepository $productRepo, InventoryRepository $inventoryRepo)
    {
        $this->productRepo   = $productRepo;
        $this->inventoryRepo = $inventoryRepo;
    }

    public function getPurchaseWithPurchaseProducts($itemId, $grouped = false)
    {
        $results = db('sql')->table('o2o_live_promo_gitem AS a')
            ->leftJoin('o2o_live_product AS b', 'b.item_id', '=', 'a.item_id')
            ->where('a.item_id', $itemId)
            ->whereIn('a.promo_cat', ['PWP', 'PW1', 'PWM', 'HWP'])
            ->select('a.promo_id', 'a.promo_type', 'a.promo_item_id AS item_id', 'b.supplier_id', 'a.promo_qty AS max_qty',
                'a.promo_reg_price AS regular_price', 'a.promo_mbr_price AS member_price', 'a.promo_price', 'a.promo_disc AS discount_amount', 'a.unit_point_multipler',
                'a.promo_disc AS promo_discount', 'a.promo_group_id AS group_id', 'a.stock_limit_qty', 'a.saving_label'
            )
            ->get();

        $locId = request('locId', '');

        $locId = explode(',', $locId);

        $collect = collect($results)->map(function ($item) use ($locId) {

            $item->image_name    = optional($this->productRepo->getOne($item->item_id))->image_name;
            $item->item_desc     = $this->productRepo->getOne($item->item_id)->item_desc;
            $item->group_id      = trim($item->group_id);
            $item->regular_price = $item->regular_price;
            $item->promo_price   = $item->promo_price;
            $item->order         = 9;

            $item->qty_balance = $this->inventoryRepo->getItemTotalBalanceQty($item->item_id, $locId);

            if (in_array(mb_strtoupper(trim($item->promo_id)), ['ADP-PROMO', 'SSEW-PROMO'], false)) {
                $item->order = 1;
            }

            return $item;
        })->sortBy('order')->sortBy('promo_price')->sortBy('group_id')->values();

        if ($grouped) {

            $mainQty = request('mainQty', 1);

            $collect = $collect->pluck('group_id')->unique()->values()
                ->map(function ($group, $index) use ($collect, $mainQty) {

                    $groupItems = $collect->where('group_id', $group);

                    $item['group']        = $index + 1;
                    $item['actual_group'] = (int)$group;
                    $groupItemsCount      = $groupItems->count();
                    $ItemPriceZeroCount   = $groupItems->where('promo_price', 0)->count();
                    $item['is_free']      = ($groupItemsCount === $ItemPriceZeroCount);
                    $maxQty               = (int)$groupItems->max('max_qty');
                    $item['max_qty']      = $mainQty * $maxQty;
                    $item['items_count']  = $groupItemsCount;
                    $item['items']        = $groupItems->map(function ($item) {
                        unset($item->max_qty);
                        $item->price_savings = round($item->regular_price - $item->promo_price,2);

                        return $item;
                    })->sortByDesc('qty_balance')->values()->toArray();

                    return $item;
                })->when(request('freeTop', 0), function ($collection) {
                    return $collection->sortByDesc(function ($item) {
                        return (int)$item['is_free'].$item['group'];
                    })->values();
                });
        }

        return $collect->toArray();
    }
}