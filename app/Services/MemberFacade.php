<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 5/7/2018
 * Time: 12:01 PM
 */

namespace App\Services;


use \Illuminate\Support\Facades\Facade;

class MemberFacade extends Facade
{
    /**
     * Returning service name
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \App\Services\MemberService::class;
    }
}
{

}