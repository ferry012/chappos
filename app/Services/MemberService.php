<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 5/7/2018
 * Time: 11:44 AM
 */

namespace App\Services;

use App\Repositories\MemberRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Container\Container as Application;

class MemberService extends Service
{
    protected $memberRepo;

    public function __construct(Application $app, MemberRepository $memberRepo)
    {
        parent::__construct($app);

        $this->memberRepo = $memberRepo;
    }

    public function joinMembership($mbrId, $months = 0, array $updateData = [])
    {
        $updateData['join_date'] = Carbon::now();

        return $this->extendMembership($mbrId, $months, null, $updateData);
    }

    public function extendMembership($mbrId, $months = 0, $startDate = null, array $updateData = [])
    {
        if (0 >= $months) {
            return false;
        }

        $now       = today();
        $startDate = Carbon::make($startDate);

        if (null === $startDate) {
            $member      = $this->memberRepo->getOne($mbrId);
            $currExpDate = Carbon::make($member->exp_date);

            $startDate = Carbon::now();

            if (null !== $currExpDate && $currExpDate->isFuture()) {
                $startDate = $currExpDate;
            }

        }

        $newExpDate = $startDate->addMonths($months)->endOfDay()->subSecond(1);

        $updateData = array_merge($updateData, ['exp_date' => $newExpDate, 'last_renewal' => $now]);

        return $this->memberRepo->updateMember($mbrId, $updateData);
    }

}