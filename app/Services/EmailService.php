<?php


namespace App\Services;


use App\Core\Orders\Models\Order;
use App\Support\Carbon;
use Illuminate\Support\Facades\Schema;

class EmailService
{
    public function sendEmail($templateId, $data)
    {
        $endpoint = env('API_URL_VALUECLUB');
        $endpoint .= 'api/emarsys/'.$templateId;
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->post($endpoint, [
            'headers' => ['Content-Type' => 'application/json'],
            'body'    => json_encode($data),
        ]);

        $content = $response->getBody()->getContents();

        db()->table('api_log')->insert([
            "api_client"      => 'emarsys',
            "api_description" => 'EMAIL_'.trim($data['trans_id']),
            "request_method"  => 'POST',
            "request_url"     => $endpoint,
            "request_header"  => '{}',
            "request_body"    => json_encode($data),
            "request_on"      => date('Y-m-d H:i:s'),
            "response_header" => '{}',
            "response_body"   => $content,
            "response_on"     => date('Y-m-d H:i:s'),
            "error_msg"       => '',
            "created_on"      => date('Y-m-d H:i:s'),
        ]);

        try {
            return json_decode($content);
        } catch (\Exception $e) {
            return null;
        }
    }

    public function issueIntelGameCode(Order $order)
    {

        $returnValue = 0;
        $qtyToIssue = $order->lines->whereIn('item_id', [
            '4710886092667',
            '4710886382409',
            '4710886382188',
            '4710886382416',
            '4710886382287',
            '4710886382300',
            '4710886573685',
            '4710886383277',
            '4710886383246',
            '4710886216087',
            '0195553253802',
            '0195553203319',
            '0195553203326',
            '0195553164191',
            '0195553140485',
            '0195553140508',
            '0195553140492',
            '0195553203364',
            '0195553203357',
            '0195553364157',
            '0195553017299',
            '0195553017312',
            '0195553104890',
            '0195553196734',
            '0195553104906',
            '0195553099196',
            '0195553202732',
            '0195553056212',
            '0195553206730',
            '0195553134385',
            '0192876832509',
            '4719331956165',
            '4719331957568',
            '4719331956899',
            '4719331956578',
            '4719331955588',
            '4719331959579',
            '4719331820985',
            '4719331819675',
            '4719331822439',
            '0195908144373',
            '0195908144397',
            '0195713917742',
            '0195713270786',
            '0195890027203',
            '0195348706483',
            '0195235380345',
            '0195235847961',
            '0195348747486',
            '8886419369370',
            '8886419369738',
            '8886419369615',
            '8886419368779',
            '8886419368892',
            '8886419335405',
            '8886419369011',
            '8886419335528'
        ])
            ->where('status_level', '>=', 0)->sum('qty_ordered');

        if ($qtyToIssue === 0) {
            $returnValue = -1;

            return $returnValue;
        }

        if (!Schema::connection(env('DB_CONNECTION'))->hasTable('code_redemption_list')) {
            $returnValue = -1;

            return $returnValue;
        }

        $transId = $order->order_num;

        $issuedQty = db()->table('code_redemption_list')->where('vendor_id', 'INTEL')->where('campaign_id', 'INTEL_DAY_21')->where('trans_id', $transId)->count();

        $qtyToIssue -= $issuedQty;

        if ($qtyToIssue <= 0) {
            $returnValue = -1;
        }

        try {
            db()->beginTransaction();

            db()->select('lock table code_redemption_list');

            $rows = db()->table('code_redemption_list')->where('vendor_id', 'INTEL')->where('campaign_id', 'INTEL_DAY_21')->where('status_level', 0)->limit($qtyToIssue)->lockForUpdate()->get();

            if ($rows->isNotEmpty()) {
                foreach ($rows as $row) {

                    $statusLevel = 1;

                    $result = $this->sendEmail('INTEL_DAY_21', [
                        "source"   => 'pos_order_was_paid',
                        "trans_id" => $order->order_num,
                        "email"    => $order->customer_email,
                        "data"     => [
                            "r_claim_code" => $row->code,
                        ],
                    ]);

                    if ($result && $result->status_code === 200) {
                        if ($result->data->response === 'OK') {
                            $statusLevel = 2;
                            $returnValue = 1;
                        }
                    }

                    db()->table('code_redemption_list')->where('id', $row->id)->update([
                        'trans_id'     => $transId,
                        'mbr_id'       => $order->customer_id,
                        'issued_on'    => Carbon::now()->toDateTimeString(),
                        'status_level' => $statusLevel
                    ]);
                }
            }

            db()->commit();

        } catch (\Exception $e) {
            db()->rollBack();
            $returnValue = -1;
        }

        return $returnValue;

    }
}