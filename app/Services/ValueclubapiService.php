<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ValueclubapiService
{

    public function __construct()
    {
        $this->Endpoint = env('API_URL_VALUECLUB');
        $this->Authorization = 'Bearer ' . env('API_URL_VALUECLUB_TOKEN');
    }

    public function validate($id){
        $apiurl = $this->Endpoint . 'api/validate/' . $id;
        $apibody = [
            'Content-Type' => 'application/json',
            'headers' => ['Content-Type' => 'application/json', 'Authorization' => $this->Authorization],
            'http_errors' => false,
            //'body' => json_encode($body)
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->get($apiurl,$apibody);
        $return = json_decode($response->getBody()->getContents(),true);

        if (isset($return['data'])) {
            return $return['data'];
        }

        return null;
    }

    public function preRegister($body){

        $apiurl = $this->Endpoint . 'api/otp_register';
        $apibody = [
            'headers' => ['Content-Type' => 'application/json', 'Authorization' => $this->Authorization],
            'http_errors' => false,
            'body' => json_encode($body)
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->post($apiurl,$apibody);
        $return = json_decode($response->getBody()->getContents(),true);

        return $return;
    }

    public function preRegisterWithoutOtp($body){

        $apiurl = $this->Endpoint . 'api/register';
        $apibody = [
            'headers' => ['Content-Type' => 'application/json', 'Authorization' => $this->Authorization],
            'http_errors' => false,
            'body' => json_encode($body)
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->post($apiurl,$apibody);
        $return = json_decode($response->getBody()->getContents(),true);

        return $return;
    }

    public function reserve_rebates($mbr_id, $trans_id, $amount){

        $body = [
            'mbr_id' => $mbr_id,
            'trans_id' => $trans_id,
            'rebate_amount' => $amount
        ];

        $apiurl = $this->Endpoint . 'api/reserved';
        $apibody = [
            'headers' => ['Content-Type' => 'application/json', 'Authorization' => $this->Authorization],
            'http_errors' => false,
            'body' => json_encode($body)
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->post($apiurl,$apibody);
        $return = json_decode($response->getBody()->getContents(),true);

        if ($return['status_code'] == 200)
            return true;

        return false;
    }

    public function unreserve_rebates($mbr_id, $trans_id){

        $body = [
            'mbr_id' => $mbr_id,
            'trans_id' => $trans_id
        ];

        $apiurl = $this->Endpoint . 'api/unreserved';
        $apibody = [
            'headers' => ['Content-Type' => 'application/json', 'Authorization' => $this->Authorization],
            'http_errors' => false,
            'body' => json_encode($body)
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->post($apiurl,$apibody);
        $return = json_decode($response->getBody()->getContents(),true);

        if ($return['status_code'] == 200)
            return true;

        return false;
    }

    public function post_transaction($data) {
        $apiurl = $this->Endpoint . 'api/transaction';
        $apibody = [
            'headers' => ['Content-Type' => 'application/json', 'Authorization' => $this->Authorization],
            'http_errors' => false,
            'body' => json_encode($data)
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->post($apiurl,$apibody);
        $return = json_decode($response->getBody()->getContents(),true);

        return $return;
    }

    public function deduct_points($data) {
        $apiurl = $this->Endpoint . 'api/points/deduct';
        $apibody = [
            'headers' => ['Content-Type' => 'application/json', 'Authorization' => $this->Authorization],
            'http_errors' => false,
            'body' => json_encode($data)
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->post($apiurl,$apibody);
        $return = json_decode($response->getBody()->getContents(),true);

        return $return;
    }
    public function award_points($data) {
        $apiurl = $this->Endpoint . 'api/points/award';
        $apibody = [
            'headers' => ['Content-Type' => 'application/json', 'Authorization' => $this->Authorization],
            'http_errors' => false,
            'body' => json_encode($data)
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->post($apiurl,$apibody);
        $return = json_decode($response->getBody()->getContents(),true);

        return $return;
    }

    public function app_push_notification($mbr_id, $title, $msg, $url='', $datetime = '') {
        if (empty($mbr_id) || empty($title) || empty($msg) || strlen($mbr_id) < 3) {
            return false;
        }

        if ($datetime=='' || strtotime($datetime) < time()) {
            $datetime = date('Y-m-d H:i:s');
        }

        $data = [
            'mbr_id' => $mbr_id,
            'title' => $title,
            'body'  => $msg,
            'url' => $url,
            'ttos' => date('Y-m-d H:i:s', strtotime($datetime)),
            'feature' => (empty($url)) ? 'push' : 'notification',
            'link_url' => 'system'
        ];

        $apiurl = $this->Endpoint . 'api/vc2/insertEmarsysPush';
        $apiurl = 'https://vc8.api.valueclub.asia/api/vc2/insertEmarsysPush';
        $apibody = [
            'headers' => ['Content-Type' => 'application/json', 'X_AUTHORIZATION' => 'NzhCMkI0OTQtNDUyNi00QzAxLUFBMjYtNTI2OTUwREYwN0ZG'],
            'http_errors' => false,
            'body' => json_encode($data)
        ];
        $client = new \GuzzleHttp\Client();
        $response = $client->post($apiurl,$apibody);
        $return = json_decode($response->getBody()->getContents(),true);

        return $return;
    }

}
