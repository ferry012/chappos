<?php

namespace App\Services;


use App\Models\Cart;
use App\Models\CartItem;
use App\Repositories\ProductRepository;

class CartService
{
    protected $productRepo;

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepo = $productRepo;
    }

    public function cartRefresh($cart)
    {
        $cart->items->each(function ($item) {
            // reset
            $item->unsetCustomData('error.item');
            $error              = '';
            $item->status_level = 0;

            //@todo update allowed delivery method
            $product = $this->productRepo->getOne($item->item_id);

            //$item->is_preorder = $product->is_preorder;

            // update main item price
            if (! $item->is_pwp_item && $product) {

                $item->regular_price = $product->regular_price;
                //$item->member_price  = $product->mbr_price;
                //$item->promo_price   = $product->promo_price;
                //$item->unit_point_multipler = $product->unit_point_multipler;

                /*
                $item->available_delivery_method = $this->buildDeliveryMethods($product);

                if (! empty($item->available_delivery_method)) {
                    if (! in_array($item->delivery_method, explode(',', $item->available_delivery_method)) && in_array($item->delivery_method, $this->except)) {
                        $error = 'Delivery method not found!';
                    }
                }
                */

                if ($product->is_active !== 'Y') {
                    $error = 'This item is currently not active in the product catalogue.';
                } else if ($item->item_qty > 20) {
                    $error = 'Please note that any order for an item with quantity exceeding 20 can only be done via our Corporate Sales.';
                }

            } else if (null === $product) {
                $error = 'off-the-shelf item, please remove it!';
            }

            // update purchase with purchase item
            if ($item->is_pwp_item) {
                $product3 = db('sql')->table('o2o_live_promo_gitem')
                    ->where('promo_item_id', $item->item_id)
                    ->where('item_id', $item->parent_id)
                    ->first(['promo_price', 'unit_point_multipler']);

                if (0 < count($product3)) {
                    $item->regular_price = $product3->promo_reg_price;
                    //$item->member_price  = $product3->promo_mbr_price;
                    //$item->promo_price   = $product3->promo_price;
                    //$item->is_free_item         = (int)(0 <= $item->promo_price);
                    //$item->unit_point_multipler = $product3->unit_point_multipler;
                } else {
                    $error = 'off-the-shelf item, please remove it!';
                }
            }

            // overwrite final unit price
            // update main item qty step price
            if (! $item->is_pwp_item && $item->item_qty > 1) {
                $product2 = db('sql')->table('o2o_live_promo_bitem')
                    ->where('promo_cat', '<>', 'PWP')
                    ->where('item_id', $item->item_id)
                    ->where([
                        ['min_qty', '<=', $item->item_qty],
                        ['max_qty', '>=', $item->item_qty],
                    ])->first(['promo_type', 'unit_reg', 'unit_mbr', 'unit_point_multipler']);

                if ($product2) {
                    // @todo need to rise the price logic
                    $item->regular_price = $product2->unit_reg;
                    //$item->member_price  = $product2->unit_mbr;
                    //$item->unit_point_multipler = $product2->unit_point_multipler;
                }
            }

            // @todo update store location price

            // check store collection (include in store id) that not in the collection list
            /*
            if ($item->delv_method === 'collection' && ! $this->canBeCollectFromStore($item->store_collection)) {
                $error = 'This product cannot be collected from this location - ' . $item->store_collection;
            }
            */

            // final unit price
            $item->unit_price = $this->getUnitPrice($item);

            if (! empty($error)) {
                $item->status_level = -1;
                $item->setCustomData('error.item', $error);
            }

            $item->save();
        });
    }

    public function getUnitPrice($item)
    {
        // default guest price
        // $price = $item->regular_price;

        //default member price
        $price = $item->member_price;

        if (null !== $item->promo_price) {
            $price = $item->promo_price;
        }

        /*
        else if ($this->hasMbrShipCard || ! $this->isMbrshipExpired) {
            $price = $item->member_price;
        }
        */

        return $price;
    }

    public function buildInsertRows($mainItem, $pwpItems)
    {

        if (null === $pwpItems) {
            return [];
        }


        $mainItemId       = $mainItem->item_id;
        $mainItemPwpItems = $pwpItems;

        $deliveryMethod  = request('delivery_method');
        $storeCollection = ($deliveryMethod === 'collection') ? request('store_collection') : null;
        $itemIds[]       = $mainItemId;

        $reqPwp = $this->filterPWP($mainItemId, $mainItemPwpItems);

        if ($reqPwp->isEmpty()) {
            return null;
        }

        $pwpItemDetailsRows = $this->productRepo->getPWPProducts($mainItemId);

        $itemIds = $reqPwp->unique('item_id')->pluck('item_id')->toArray();

        $itemDetailsRows = $this->productRepo->getProducts($itemIds);

        $reqItems = $reqPwp->map(function ($item) use ($mainItem, $itemDetailsRows, $pwpItemDetailsRows) {

            $currItem    = $itemDetailsRows->where('item_id', $item['item_id'])->first();
            $currPwpItem = $pwpItemDetailsRows->where('promo_item_id', $item['item_id'])->first();

            $cartItem = new CartItem();

            $cartItem->item_id       = $currItem->item_id;
            $cartItem->item_desc     = $currItem->item_desc;
            $cartItem->item_qty      = $item['qty'];
            $cartItem->item_img      = $currItem->image_name;
            $cartItem->item_desc     = $currPwpItem->promo_item_desc;
            $cartItem->regular_price = $currPwpItem->promo_reg_price;
            //$cartItem->member_price  = $currPwpItem->promo_mbr_price;
            //$cartItem->promo_price   = $currPwpItem->promo_price;
            $cartItem->unit_price    = 1;
            $cartItem->parent_id     = $mainItem->id;

            return $cartItem;

        });

        return $reqItems;
    }

    protected function validatePWP()
    {
        // parent item qty
        $mainItemId = request('itemId');
        $qty        = request('qty', 1);

        $pwpRows = $this->productRepo->getMainProductPWPProducts($mainItemId);

        if ($pwpRows->isEmpty()) {
            return true;
        }

        $inputPwpArr = isset($input['pwp']) ? $this->filterPWP($input['item_id'], $input['pwp'])->groupBy('group_id') : [];

        $pwpRows
            ->groupBy('promo_group_id')
            ->each(function ($item, $key) use ($inputPwpArr, $qty) {

                // get a group qty limitation
                $maxQty    = $item->max('promo_qty') * $qty;
                $pwpGroup  = collect([]);
                $pwpReqQty = 0;

                if (isset($inputPwpArr[$key])) {

                    // get selected group items
                    $pwpGroup->push($inputPwpArr[$key]);
                    // get selected group qty
                    $pwpReqQty = $pwpGroup->sum('qty');

                    // check qty exceeded
                    if ($pwpReqQty > $maxQty) {
                        $exceededQty           = ($pwpReqQty - $maxQty);
                        $this->data['error'][] = "Group {$key} exceeded {$exceededQty} qty.";
                    }

                }

                $item->each(function ($item2) use ($pwpGroup, $maxQty, $pwpReqQty) {

                    // selected group item details
                    if (0 < count($pwpGroup)) {

                        if ($item2->promo_price <= 0 && $pwpReqQty < $maxQty) {
                            $leftQty               = $maxQty - $pwpReqQty;
                            $this->data['error'][] = "Please select {$leftQty} more free item(s) from Group {$item2->promo_group_id}";

                            return false;
                        }

                    } else {
                        // check free item that not been selected
                        if (null !== $item2->promo_price && $item2->promo_price <= 0 && $pwpReqQty < $maxQty) {
                            $leftQty               = $maxQty - $pwpReqQty;
                            $this->data['error'][] = "Please select {$leftQty} free item(s) from Group {$item2->promo_group_id}";

                            return false;
                        }

                    }

                    return $item2;
                });

            });

        return ! isset($this->data['error']);
    }

    /*
     * filter out pwp items that belongs to main item
     *
     */
    protected function filterPWP($mainId, $pwp)
    {

        $items              = collect($pwp);
        $pwpItemDetailsRows = $this->productRepo->getPWPProducts($mainId);
        $filtered           = $items->filter(function ($item) use ($pwpItemDetailsRows) {
            return $pwpItemDetailsRows->contains('promo_item_id', $item['item_id']);
        });

        return $filtered;
    }

}