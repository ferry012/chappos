<?php

namespace App\Services;


use App\Models\Order;
use App\Models\OrderItem;
use App\Support\Str;

class OrderService
{
    public function createOrder($cart, $skipStockReserved = true)
    {

        $cartItems = $cart->items;
        $gstPlus = number_format((gst()->tax_percent / 100) + 1,2);
        $gst = number_format((gst()->tax_percent / 100),2);

        // dd($items);
        if ($cartItems->isEmpty()) {
            return false;
        }

        $taxDeductibleAmt = 0;

        $order = new Order;

        $order->order_num       = 'HO'.time();
        $order->order_type      = 'PS';
        $order->shipping_total = 0;

        // if member, retrieve member info
        if (! empty($cart->mbr_id)) {
            $order->customer_name  = '';
            $order->customer_email = '';
        }

        $orderItems = $cartItems->map(function ($row, $index) {

            $orderItem                       = new OrderItem;
            $orderItem->line_num             = $index + 1;
            $orderItem->item_id              = $row->item_id;
            $orderItem->item_desc            = $row->item_desc;
            $orderItem->item_img             = $row->item_img;
            $orderItem->qty_ordered          = $row->item_qty;
            $orderItem->regular_price        = $row->regular_price;
            $orderItem->unit_price           = $row->unit_price;
            $orderItem->unit_discount_amount = 0;

            return $orderItem;
        });


        $order->item_amount = $orderItems->reduce(function ($carry, $item) {
            return $carry + ($item['qty_ordered'] * $item['unit_price']);
        });

        // spread discount
        $discountAmount = 0; // for testing
        $orderItems     = $orderItems->map(function ($item) use ($order, $discountAmount) {

            if ($discountAmount > 0) {
                $item->unit_discount_amount = ((($item->qty_ordered * $item->unit_price) / $order->item_amount) * $discountAmount) / $item->qty_ordered;

                $item['unit_points_earned'] = $item['unit_price'] - $item['unit_discount_amount'];

            }

            return $item;
        });

        $shippingAmount = data_get($order, 'shipping_amount', 0);

        $shippingItems = $orderItems->whereIn('delv_method', ['STD'])->count();

        // waive standard shipping fee for order total above $88
        if ($shippingItems > 0 && $order->item_amount > 88) {
            $order->shipping_total -= 8;
        }

        $pointsAmount = data_get($order, 'points_amount', 0);

        if ($pointsAmount > 0) {
            $pointsTaxDeductibleAmt = $pointsAmount;
            if ($pointsAmount > 200) {
                $pointsTaxDeductibleAmt = 200;
            }

            $taxDeductibleAmt += ($gst / $gstPlus) * $pointsTaxDeductibleAmt;
        }

        $totalPointsEarned = $orderItems->reduce(function ($carry, $item) {

            $points = $item['qty_ordered'] * $item['unit_points_earned'];

            if ($points < 0) {
                $points = 0;
            }

            return $carry + $points;
        });

        $totalSavingAmount = $orderItems->reduce(function ($carry, $item) {
            $unitSaving = $item['regular_price'] - $item['unit_price'];
            $saving     = $item['qty_ordered'] * $unitSaving;

            return $carry + $saving;
        });

        $totalAmount = $order->item_amount + $shippingAmount - $discountAmount;
        $totalPaid   = $pointsAmount + data_get($order, 'credit_amount', 0);
        $totalDue    = $totalAmount - $totalPaid;
        $roundingAdj = 0;

        // if pay via cash adjust the total due
        if (false) {
            $orgTotalDue = $totalDue;
            $totalDue    = round_to_0_or_5_cents($totalDue);
            $roundingAdj = $totalDue - $orgTotalDue;
        }

        $taxableAmount = $order->item_amount + $shippingAmount - $discountAmount;
        $order->tax_amount     = ($gst / $gstPlus) * $taxableAmount;
        $order->tax_amount     -= $taxDeductibleAmt;
        $order->total_incl_tax = $totalAmount;
        $order->rounding_adj   = $roundingAdj;
        $order->total_due      = $totalDue;
        $order->total_saving_amount = $totalSavingAmount;
        $order->total_points_earned = floor($totalPointsEarned);

        db('pg_cherps')->beginTransaction();

        try {

            $order->save();

            $order->items()->saveMany($orderItems);

            db('pg_cherps')->commit();
        } catch (\Exception $e) {
            db('pg_cherps')->rollback();

            throw $e;
        }

        return $order;
    }

    public function orderConfirm(Order $order)
    {

        db('pg_cherps')->beginTransaction();

        try {

            if ($order->order_type === 'HI') {
                $this->createHIInvoice($order);
            }

            if ($order->order_type === 'PS') {
                $this->createPSInvoice($order);
            }


            db('pg_cherps')->commit();
        } catch (\Exception $e) {
            db('pg_cherps')->rollback();
            throw $e;
        }
    }

    public function createPSInvoice(Order $order)
    {
        $orderHead = [
            'coy_id'             => 'CTL',
            'trans_id'           => $order->order_num,
            'trans_type'         => 'PS',
            'trans_date'         => now(),
            'status_level'       => 1,
            'mbr_id'             => $order->customer_id ?? '',
            'mbr_savings'        => $order->total_saving_amount,
            'loc_id'             => $order->loc_id ?? '',
            'pos_id'             => '',
            'cashier_id'         => '',
            'cust_id'            => $order->customer_id ?? '',
            'cust_name'          => '',
            'points_accumulated' => 0,
            'points_redeemed'    => -1 * $order->points_used,
            'mbr_points'         => $order->total_points_earned,
            'tax_percent'        => gst()->tax_percent,
            'gst_amount'         => $order->tax_amount,
            'rounding_adj'       => 0,
            'voucher_adj'        => 0,
            'remarks'            => '',
        ];

        $orderItems = $order->items->sortBy('id')->values()->map(function ($item, $index) use ($order) {
            return [
                'coy_id'         => 'CTL',
                'trans_id'       => $order->order_num,
                'line_num'       => $index + 1,
                'item_id'        => $item->item_id,
                'item_desc'      => Str::substr($item->item_desc, 0, 60),
                'trans_qty'      => $item->qty_ordered,
                'unit_price'     => $item->unit_price,
                'regular_price'  => $item->regular_price,
                'disc_amount'    => $item->unit_discount_amount,
                'disc_percent'   => 0,
                'salesperson_id' => '',
                'status_level'   => 1,
                'promo_id'       => '',
                'trans_cost'     => 0,
                'bom_ind'        => '',
                'disc_rebate'    => 0,
                'promoter_id'    => '',
                'created_by'     => '', // cashier id
                'created_on'     => now(),
                'modified_by'    => '',
                'modified_on'    => now(),
            ];
        })->toArray();

        $orderPayment = $order->payments->sortBy('id')->values()->map(function ($payment, $index) {
            return [
                'coy_id'       => 'CTL',
                'trans_id'     => $payment->trans_id,
                'line_num'     => $index + 1,
                'pay_mode'     => '',
                'trans_amount' => $payment->trans_amount,
                'trans_ref1'   => $payment->card_num, //mask card number
                'trans_ref2'   => '', // year
                'trans_ref3'   => $payment->approval_code, // approved code
                'find_dim2'    => '',
                'status_level' => 1,
                'created_by'   => '', // cashier id
                'created_on'   => now(),
                'modified_by'  => '',
                'modified_on'  => now(),
            ];
        })->toArray();

        $orderTransaction = [];

        if ($order->isMember()) {

            $orderTransaction = collect($orderItems)->map(function ($item) use ($order) {
                $item = (object)$item;

                $saving = ($item->regular_price - $item->unit_price) * $item->trans_qty;

                if (0 > $saving) {
                    $saving = 0;
                }

                return [
                    'coy_id'         => 'CTL',
                    'mbr_id'         => $order->customer_id ?? '',
                    'trans_id'       => $item->trans_id,
                    'line_num'       => $item->line_num,
                    'trans_type'     => 'PS',
                    'loc_id'         => $order->loc_id ?? '',
                    'pos_id'         => $order->pos_id ?? '',
                    'item_id'        => $item->item_id,
                    'item_desc'      => $item->item_desc,
                    'item_qty'       => $item->trans_qty,
                    'regular_price'  => $item->regular_price,
                    'unit_price'     => $item->unit_price,
                    'disc_percent'   => $item->disc_percent,
                    'disc_amount'    => $item->disc_amount,
                    'trans_points'   => 0,
                    'salesperson_id' => $item->salesperson_id,
                    'mbr_savings'    => $saving,
                    'created_by'     => $item->created_by, // cashier id
                    'created_on'     => now(),
                    'modified_by'    => $item->modified_by,
                    'modified_on'    => now(),
                ];
            })->toArray();
        }

        db('pg_cherps')->beginTransaction();

        try {
            db('pg_cherps')->table('pos_transaction_list')->insert($orderHead);
            db('pg_cherps')->table('pos_transaction_item')->insert($orderItems);
            db('pg_cherps')->table('pos_payment_list')->insert($orderPayment);
            db('pg_cherps')->table('crm_member_transaction')->insert($orderTransaction);
            db('pg_cherps')->commit();
        } catch (\Exception $e) {
            db('pg_cherps')->rollback();
            throw $e;
        }
    }

    public function createHIInvoice(Order $order)
    {
        $orderHead = [
            'coy_id'         => 'CTL',
            'invoice_id'     => $order->order_num,
            'inv_type'       => 'HI',
            'invoice_date'   => now(),
            'status_level'   => 1,
            'salesperson_id' => '',
            'cust_id'        => $order->customer_id ?? '',
            'cust_name'      => '',
            'tel_code'       => '',
            'contact_person' => '',
            'cust_po_code'   => '',
            'email_addr'     => '',
            'loc_id'         => $order->loc_id ?? '',
            'curr_id'        => 'SGD',
            'payment_id'     => '',
            'delv_mode_id'   => '',
            'tax_id'         => '',
            'tax_percent'    => gst()->tax_percent,
            'created_by'     => '',
            'created_on'     => now(),
            'modified_by'    => '',
            'modified_on'    => now(),
        ];

        $stockItems = db()->table('o2o_order_item')->join('o2o_order_stock_log','o2o_order_stock_log.order_item_id','=','o2o_order_item.id')->where('o2o_order_item.order_id',$order->id)->get();

        $orderItems = collect($stockItems)->sortBy('id')->values()->map(function ($item, $index) use ($order) {
            return [
                'coy_id'         => 'CTL',
                'invoice_id'     => $order->order_num,
                'line_num'       => $index + 1,
                'item_id'        => $item->item_id,
                'item_desc'      => Str::substr($item->item_desc, 0, 60),
                'qty_invoiced'   => $item->qty_ordered,
                'uom_id'         => '',
                'unit_price'     => $item->unit_price,
                'disc_amount'    => $item->unit_discount_amount,
                'disc_percent'   => 0,
                'status_level'   => 1,
                'fin_dim1'       => '',
                'fin_dim2'       => '',
                'fin_dim3'       => '',
                'salesperson_id' => '',
                'bom_ind'        => '',
                'promo_id'       => '',
                'promoter_id'    => '',
                'delv_mode_id'   => '',
                'inv_loc'        => '',
                'loc_id'         => '',
                'created_by'     => '', // cashier id
                'created_on'     => now(),
                'modified_by'    => '',
                'modified_on'    => now(),
            ];
        })->toArray();

        $orderPayment = $order->payments->sortBy('id')->values()->map(function ($payment, $index) {
            return [
                'coy_id'       => 'CTL',
                'trans_id'     => $payment->trans_id,
                'line_num'     => $index + 1,
                'pay_mode'     => '',
                'trans_amount' => $payment->trans_amount,
                'trans_ref1'   => $payment->card_num, //mask card number
                'trans_ref2'   => '', // year
                'trans_ref3'   => $payment->approval_code, // approved code
                'find_dim2'    => '',
                'status_level' => 1,
                'created_by'   => '', // cashier id
                'created_on'   => now(),
                'modified_by'  => '',
                'modified_on'  => now(),
            ];
        })->toArray();

        $orderTransaction = [];

        if ($order->isMember()) {

            $orderTransaction = collect($orderItems)->map(function ($item) use ($order) {
                $item     = (object)$item;
                $regPrice = $order->items->where('item_id', $item->item_id)->first()->regular_price;

                $saving = ($regPrice - $item->unit_price) * $item->qty_invoiced;

                if (0 > $saving) {
                    $saving = 0;
                }

                return [
                    'coy_id'         => 'CTL',
                    'mbr_id'         => $order->customer_id ?? '',
                    'trans_id'       => $item->invoice_id,
                    'line_num'       => $item->line_num,
                    'trans_type'     => 'PS',
                    'loc_id'         => '',
                    'pos_id'         => '',
                    'item_id'        => $item->item_id,
                    'item_desc'      => $item->item_desc,
                    'item_qty'       => $item->qty_invoiced,
                    'regular_price'  => $regPrice,
                    'unit_price'     => $item->unit_price,
                    'disc_percent'   => $item->disc_percent,
                    'disc_amount'    => $item->disc_amount,
                    'trans_points'   => 0,
                    'salesperson_id' => $item->salesperson_id,
                    'mbr_savings'    => $saving,
                    'created_by'     => $item->created_by, // cashier id
                    'created_on'     => now(),
                    'modified_by'    => $item->modified_by,
                    'modified_on'    => now(),
                ];
            })->toArray();
        }
//        dd(JAY,$orderHead);
        db('sql')->beginTransaction();

        try {
            db('sql')->table('sms_invoice_list')->insert($orderHead);
            db('sql')->table('sms_invoice_item')->insert($orderItems);
            db('sql')->table('sms_payment_list')->insert($orderPayment);
            db('sql')->table('crm_member_transaction')->insert($orderTransaction);
            db('sql')->commit();
        } catch (\Exception $e) {
            db('sql')->rollback();
            throw $e;
        }
    }
}