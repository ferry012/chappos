<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 6/7/2018
 * Time: 12:11 PM
 */

namespace App\Services;

use App\Repositories\MemberPointRepository;
use Carbon\Carbon;

class MemberPointService
{
    protected $memberPointRepo;

    public function __construct(MemberPointRepository $memberPointRepo)
    {
        $this->memberPointRepo = $memberPointRepo;
    }

    public function isPointEnough($mbrId, $points)
    {
        return 0 < $points || 0 < $this->memberPointRepo->balancePoints($mbrId);
    }

    public function addPoints($mbrId, $points = 0, $expDate = null)
    {
        if ($points <= 0) {
            return false;
        }

        $now    = Carbon::now()->startOfDay();
        $result = db('sql')->table('crm_member_points')->where('mbr_id', $mbrId)->where('exp_date', '>=', $now)->orderby('exp_date')->first();

        if ($result) {

            return db('sql')->table('crm_member_points')
                ->where([
                    'mbr_id'   => $result->mbr_id,
                    'line_num' => $result->line_num,
                ])
                ->update(['points_accumulated' => DB::raw("points_accumulated + {$points}")]);

        }

        $expDate = (null !== $expDate) ? Carbon::now($expDate)->startOfDay() : $this->memberPointRepo->getNextExpiryDate();

        $lineNum = $this->memberPointRepo->getNextLineNum($mbrId);

        return db('sql')->table('crm_member_points')->insert([
            'coy_id'             => 'CTL',
            'mbr_id'             => $mbrId,
            'line_num'           => $lineNum,
            'exp_date'           => $expDate,
            'points_accumulated' => $points,
        ]);
    }

    public function deductPoints($mbrId, $points)
    {

        if ($this->isPointEnough($mbrId, $points)) {

            $now     = Carbon::now();
            $results = db('sql')->table('crm_member_points')
                ->where([
                    'coy_id' => 'CTL',
                    'mbr_id' => $mbrId,
                ])
                ->where('exp_date', '>=', $now->startOfDay())
                ->whereRaw('points_accumulated - points_redeemed - points_expired > 0')
                ->orderby('exp_date')
                ->get();

            $collection = collect($results)->map(function ($item) use (&$points) {

                if ($points <= 0) {
                    return null;
                }

                $balance = $item->points_accumulated - $item->points_redeemed - $item->points_expired;

                if ($balance >= $points) {
                    $redeemed = $points;
                    $points   = 0;
                } else {
                    $redeemed = $balance;
                    $points   -= $balance;
                }

                $item->points_redeemed += $redeemed;

                db('sql')->table('crm_member_points')
                    ->where([
                        'coy_id'   => 'CTL',
                        'mbr_id'   => $item->mbr_id,
                        'line_num' => $item->line_num,
                    ])
                    ->update(['points_redeemed' => $item->points_redeemed]);

                return $item;
            })->reject(function ($item) {
                return null === $item;
            });

            return $collection;
        }

        return null;
    }

}