<?php

namespace App\Support\Guzzle;

use Closure;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Promise;
use InvalidArgumentException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

class LoggerMiddleware
{
    /**
     * @var \Psr\Log\LoggerInterface|callable
     */
    protected $logger;
    /**
     * @var \GuzzleHttp\MessageFormatter|callable
     */
    protected $formatter;

    protected $startDate;

    protected $clientName;

    protected $description;

    /**
     * Creates a callable middleware for logging requests and responses.
     *
     * @param string|callable $formatter Constant or callable that accepts a Response.
     */
    public function __construct($clientName = null, $formatter = null)
    {
        $this->clientName = $clientName;
        $this->setFormatter($formatter ?: $this->getDefaultFormatter());
    }

    public function setClientName($name)
    {
        $this->clientName = $name;
    }

    /**
     * Sets the logger, which can be a PSR-3 logger or a callable that accepts
     * a log level, message, and array context.
     *
     * @param LoggerInterface|callable $logger
     *
     * @throws InvalidArgumentException
     */
    public function setLogger($logger)
    {
        if ($logger instanceof LoggerInterface || is_callable($logger)) {
            $this->logger = $logger;
        } else {
            throw new InvalidArgumentException(
                'Logger has to be a Psr\Log\LoggerInterface or callable'
            );
        }
    }

    /**
     * Sets the formatter, which can be a MessageFormatter or callable that
     * accepts a request, response, and a reason if an error has occurred.
     *
     * @param MessageFormatter|callable $formatter
     *
     * @throws InvalidArgumentException
     */
    public function setFormatter($formatter)
    {
        if ($formatter instanceof MessageFormatter || is_callable($formatter)) {
            $this->formatter = $formatter;
        } else {
            throw new InvalidArgumentException(
                'Formatter has to be a \GuzzleHttp\MessageFormatter or callable'
            );
        }
    }

    /**
     * Returns the default formatter;
     *
     * @return MessageFormatter
     */
    protected function getDefaultFormatter()
    {
        return new MessageFormatter();
    }

    /**
     * Logs a request and/or a response.
     *
     * @param RequestInterface       $request
     * @param ResponseInterface|null $response
     * @param mixed                  $reason
     *
     * @return void
     */
    protected function log(
        RequestInterface $request,
        ResponseInterface $response = null,
        $reason = null
    )
    {
        if ($reason instanceof RequestException) {
            $response = $reason->getResponse();
        }

        $extraData = [];

        // Make sure that the content of the body is available again.
        if ($response) {
            $response->getBody()->seek(0);

            $extraData += [
                'response_code'   => $response->getStatusCode(),
                'response_header' => json_encode($response->getHeaders()),
                'response_body'   => $response->getBody()->getContents(),
                'response_on'     => now(),
            ];
        }

        if ($reason) {
            $extraData += ['error_msg' => $reason->getMessage()];
        }

        if ($this->clientName && $this->description) {
            db()->table('api_log')->insert([
                    'api_client' => $this->clientName,
                    'api_description' => $this->description,
                    'request_method' => $request->getMethod(),
                    'request_url' => $request->getUri(),
                    'request_header' => json_encode($request->getHeaders()),
                    'request_body' => (string)$request->getBody(),
                    'request_on' => $this->startDate,
                    'created_on' => now(),
                ] + $extraData);
        }

    }

    /**
     * Formats a request and response as a log message.
     *
     * @param RequestInterface       $request
     * @param ResponseInterface|null $response
     * @param mixed                  $reason
     *
     * @return string The formatted message.
     */
    protected function getLogMessage(RequestInterface $request, ResponseInterface $response = null, $reason = null)
    {
        if ($this->formatter instanceof MessageFormatter) {
            return $this->formatter->format(
                $request,
                $response,
                $reason
            );
        }

        return call_user_func($this->formatter, $request, $response, $reason);
    }

    /**
     * Returns a function which is handled when a request was successful.
     *
     * @param RequestInterface $request
     *
     * @return \Closure
     */
    protected function onSuccess(RequestInterface $request)
    {
        return function ($response) use ($request) {
            $this->log($request, $response);

            return $response;
        };
    }

    /**
     * Returns a function which is handled when a request was rejected.
     *
     * @param RequestInterface $request
     *
     * @return Closure
     */
    protected function onFailure(RequestInterface $request)
    {
        return function ($reason) use ($request) {

            $this->log($request, null, $reason);

            return Promise\rejection_for($reason);
        };
    }

    public function __invoke(callable $handler)
    {
        return function ($request, array $options) use ($handler) {

            $this->description = array_get($options, 'description');
            $this->startDate   = now();

            return $handler($request, $options)->then(
                $this->onSuccess($request),
                $this->onFailure($request)
            );
        };
    }
}