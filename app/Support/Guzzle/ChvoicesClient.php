<?php

namespace App\Support\Guzzle;

use \GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;

class ChvoicesClient
{
    private $client;

    public function __construct()
    {
        $stack = HandlerStack::create();

        $stack->push(new LoggerMiddleware('chvoices', ''));

        $this->client = new Client(['handler' => $stack, 'http_errors' => false,]);
    }

    public function request($method, $url, $options = [])
    {
        try {
            $response = $this->client->request($method, $this->getBaseUrl().$url, $options);

            $payload = json_decode($response->getBody());

        } catch (ClientException $e) {
            return null;
        }

        return $payload;
    }

    public function triggerItemsActivation($transId)
    {
        return $this->request('GET', 'blackhawk/'.$transId, ['description' => 'BLACKHAWK_'.$transId]);
    }

    public function triggerIncommActivation($transId)
    {
        return $this->request('GET', 'razer_posa/'.$transId, ['description' => 'INCOMMPOSA_'.$transId]);
    }

    public function triggerRazergoldActivation($transId)
    {
        return $this->request('GET', 'razer_pin_store/'.$transId, ['description' => 'RAZERGOLD_'.$transId]);
    }

    public function triggerPoGenerate($transId)
    {
        return $this->request('GET', 'challenger_po/generate/'.$transId, ['description' => 'GENPO_'.$transId]);
    }

    public function triggerGiftVoucher($transId)
    {
        return $this->request('GET', 'ctlvch/transaction/'.$transId, ['description' => 'GIFT_VOUCHER_'.$transId]);
    }

    public function triggerBurnDiscountVoucher($transId, $options = [])
    {
        $options += ['description' => 'DISCOUNT_VOUCHER_'.$transId];

        return $this->request('POST', 'ctlvch/sms_voucher/utilize?PKEY=obh0hqcxz81upu2ixyokn9t49na1ak3a', $options);
    }

    public function getBaseUrl()
    {
        $url = env('API_URL_CHVOICES') . 'api/';
        return $url;
    }
}