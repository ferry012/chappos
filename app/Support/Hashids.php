<?php

namespace App\Support;


class Hashids
{
    protected static $salt          = 'kCw1giwCPtTDOhpe9mJvl2vbLqBpyS7x';
    protected static $minHashLength = 32;
    protected static $alphabet      = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';

    public static function hasher()
    {
        return with(new \Hashids\Hashids(static::$salt, static::$minHashLength, static::$alphabet));
    }

    public static function encode()
    {
        return self::hasher()->encode(func_get_args());
    }

    public static function decode($hash)
    {
        return self::hasher()->decode($hash);
    }
}