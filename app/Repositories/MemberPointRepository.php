<?php

namespace App\Repositories;

use Carbon\Carbon;


class MemberPointRepository
{

    public function getRecentExpiringPoints($mbrId)
    {
        return db('sql')->table('crm_member_points')->where('coy_id', 'CTL')->where('mbr_id', $mbrId)->where('exp_date', '>=', Carbon::now()->startOfDay())->orderby('exp_date')->first();
    }

    public function summary($mbrId)
    {
        $result = db('sql')->table('crm_member_points')
            ->selectRaw('SUM(points_accumulated) AS points_accumulated, SUM(points_redeemed) AS points_redeemed, SUM(points_expired) AS points_expired')
            ->where([
                'coy_id' => 'CTL',
                'mbr_id' => $mbrId,
            ])
            ->first();

        $result->points_accumulated = $result->points_accumulated ?? 0;
        $result->points_redeemed    = $result->points_redeemed ?? 0;
        $result->points_expired     = $result->points_expired ?? 0;

        return $result;
    }

    public function isPointEnough($mbrId, $points)
    {
        return 0 < $points || 0 < $this->balancePoints($mbrId);
    }

    public function balancePoints($mbrId)
    {
        $now    = Carbon::now();
        $result = db('sql')->table('crm_member_points')->selectRaw('SUM(points_accumulated - points_redeemed - points_expired) as balance')
            ->where('mbr_id', $mbrId)->where('exp_date', '>=', $now->startOfDay())
            ->havingRaw('SUM(points_accumulated - points_redeemed - points_expired) > 0')
            ->first();

        $balance = $result->balance ?? 0;

        return (float)$balance;
    }

    public function getNextExpiryDate()
    {
        $now = Carbon::now();

        $result = db('sql')->table('crm_points_expiry')->where(['coy_id' => 'CTL'])->where(function ($query) use ($now) {
            $query->where('eff_from', '>=', $now)->where('eff_to', '<=', $now);
        })->first();

        if ($result) {
            return $result->expdate;
        }

        // if expiry date is not preset, use the assumed date instead
        if ($now->month > 6) {
            $expDate = $now->addYear()->endOfYear()->startOfDay();
        } else {
            $expDate = $now->addYear()->endOfYear()->subMonths(6)->subDay()->startOfDay();
        }

        return $expDate;
    }


    public function getNextLineNum($mbrId)
    {
        $lineNum = db('sql')->table('crm_member_points')->where(['coy_id' => 'CTL'])->where('mbr_id', $mbrId)->max('line_num');

        return $lineNum + 1;
    }

    public function pointRedemptionValue($points)
    {
        $num = $points / 100;

        return number_format($num, 2);
    }

    public function getQuery()
    {
        return db('sql')->table('crm_member_points')->where(['coy_id' => 'CTL']);
    }
}