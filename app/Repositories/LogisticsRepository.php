<?php

namespace App\Repositories;


class LogisticsRepository
{

    public function __construct( ){
    }

    // with(new LogisticsRepository)->assignDriver('408553');
    public function assignDriver($postal){

        $driver = '';

        if (isset($postal) && !empty($postal)) {
            $b2b_postal_list = db()->table('sg_postal_list')
                ->whereRaw("postal_6d = ? or (postal_2d = left( ? ,2) and postal_6d='000000')", [$postal, $postal])
                ->orderBy('postal_6d', 'desc')->first();
            if ($b2b_postal_list) {
                $driver = $b2b_postal_list->driver_name1;
                if ($b2b_postal_list->driver_name2) {
                    $driver .= '/'.$b2b_postal_list->driver_name2;
                }
            }
        }

        return $driver;

    }
}