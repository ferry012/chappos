<?php

namespace App\Repositories;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Container\Container as Application;

abstract class BaseRepository
{
    protected $app;

    protected $model;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    abstract function model();

    public function makeModel()
    {
        $model = $this->app->make($this->model());

        if ($model instanceof Model) {
            // throw error
        }

        return $this->model = $model;
    }

    public function resetModel()
    {
        $this->makeModel();
    }
}