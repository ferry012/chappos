<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 10/9/2018
 * Time: 5:54 PM
 */

namespace App\Repositories;

class ReportingRepository{

    public function order_resource($src_id, $order_num='', $loc_id='', $eff_from='', $eff_to='') {
        if ($order_num=='') {
            $created_on = [date('Y-m-d 00:00:00', strtotime($eff_from)), date('Y-m-d 23:59:59', strtotime($eff_to))];
            $results = db()->table('o2o_order_resource')->join('o2o_order_list', 'o2o_order_list.order_num', '=', 'o2o_order_resource.order_num')
                ->where("o2o_order_resource.src_id", $src_id)->whereBetween("o2o_order_list.pay_on", $created_on)
                ->wherein("o2o_order_list.loc_id", explode(',', $loc_id))
                ->select("o2o_order_list.order_num", "o2o_order_list.loc_id", "o2o_order_list.pos_id", "o2o_order_list.customer_id", "o2o_order_list.cashier_id", "o2o_order_list.created_on", "o2o_order_resource.custom_data")
                ->get();
        }
        else {
            $results = db()->table('o2o_order_resource')->join('o2o_order_list', 'o2o_order_list.order_num', '=', 'o2o_order_resource.order_num')
                ->where("o2o_order_list.order_num", $order_num)
                ->select("o2o_order_list.order_num", "o2o_order_list.loc_id", "o2o_order_list.pos_id", "o2o_order_list.customer_id", "o2o_order_list.cashier_id", "o2o_order_list.created_on", "o2o_order_resource.custom_data")
                ->get();
        }
        return $results;
    }

    public function order_unposted($loc_id='',$pos_id='',$trans_date=''){
        $sql_where = "";
        if ($loc_id!='') {
            $sql_where .= " AND loc_id='$loc_id' ";
        }
        if ($pos_id!='') {
            $sql_where .= " AND pos_id='$pos_id' ";
        }
        if ($trans_date!='') {
            $sql_where .= " AND TO_TIMESTAMP(cast(pay_on as TEXT),'YYYY-MM-DD')='". date('Y-m-d', strtotime($trans_date) ) ."' ";
        }
        $sql = "select id,loc_id,pos_id,order_num,order_type,pay_on trans_date
                from o2o_order_list
                where order_status_level>=1 and pay_status_level>=2 and is_posted is false
                $sql_where
                order by pay_on desc;";
        $result = db()->select($sql);
        return ($result) ? $result : NULL;
    }

    public function crm_coupon_dates($coupon_id=''){
        $sql = "select id,coupon_id,eff_from,eff_to from crm_coupon_promo ";
        if ($coupon_id!='')
            $sql.= " where coupon_id='$coupon_id' ";
        $result = db()->select($sql);
        return ($result) ? $result : NULL;
    }

    public function cherps_single_coupon($item_id){
        $sql = "select item_id,eff_from,eff_to from ims_item_price where item_id='$item_id' and price_type='REGULAR' and coy_id='CTL' order by eff_to desc;";
        $result = db('pg_cherps')->select($sql);
        return ($result) ? $result[0] : NULL;
    }

    public function update_coupon_date($param){

        $where = ['id' => $param['id']];
        $data = [
            'eff_from' => $param['eff_from'],
            'eff_to' => $param['eff_to']
        ];
        db()->table('crm_coupon_promo')->where($where)->update($data);

    }

    public function crm_promo_sync_to_cherps($promo_id){

        // crm_promo_list
        $sql = "SELECT * FROM crm_promo_list WHERE promo_id='$promo_id'; ";
        $list = (array) db()->select($sql);
        $result = array_map(function ($value) {
            return (array)$value;
        }, $list);

        $sql = "DELETE FROM crm_promo_list WHERE promo_id='$promo_id'; ";
        db('sql')->delete($sql);
        db('sql')->table('crm_promo_list')->insert($result);


        // crm_promo_item
        $sql = "SELECT * FROM crm_promo_item WHERE promo_id='$promo_id'; ";
        $list = (array) db()->select($sql);
        $result = array_map(function ($value) {
            return (array)$value;
        }, $list);

        $sql = "DELETE FROM crm_promo_item WHERE promo_id='$promo_id'; ";
        db('sql')->delete($sql);
        db('sql')->table('crm_promo_item')->insert($result);


        // crm_promo_location
        $sql = "SELECT * FROM crm_promo_location WHERE promo_id='$promo_id'; ";
        $list = (array) db()->select($sql);
        $result = array_map(function ($value) {
            return (array)$value;
        }, $list);

        $sql = "DELETE FROM crm_promo_location WHERE promo_id='$promo_id'; ";
        db('sql')->delete($sql);
        db('sql')->table('crm_promo_location')->insert($result);

    }

    public function ims_item_sync_from_cherps($item_id='') {

        $logdetails = [];

        // ims_item_list
        $sql = "SELECT modified_on FROM ims_item_list ORDER BY modified_on DESC LIMIT 1; ";
        $result = db()->select($sql);
        $last_modified = date('Y-m-d H:i:s', strtotime($result[0]->modified_on) + 1 );
        $logdetails[] = "IMS_ITEM_LIST Last Modified: " . $last_modified;

        $item_ids = [];
        $sql = "SELECT * FROM ims_item_list WHERE modified_on > '$last_modified'; ";
        $list = (array) db('pg_cherps')->select($sql);
        foreach($list as $l) {
            $item_ids[] = trim($l->item_id);
        }
        if (count($item_ids)>0) {
            $logdetails[] = "IMS_ITEM_LIST Item ID to delete: " . implode(", ", $item_ids);

            $result = array_map(function ($value) {
                $value->created_on = date('Y-m-d H:i:s', strtotime($value->created_on));
                $value->modified_on = date('Y-m-d H:i:s', strtotime($value->modified_on));
                return (array)$value;
            }, $list);

            $sql = "DELETE FROM ims_item_list WHERE rtrim(item_id) IN ('" . implode("','", $item_ids) . "')";
            db()->delete($sql);
            db()->table('ims_item_list')->insert($result);
            $logdetails[] = "IMS_ITEM_LIST Inserted records: " . count($result);

            if (date('H') < 9) {
                $sql = "UPDATE ims_item_list SET item_id = rtrim(item_id) ; ";
                db()->update($sql);
                $logdetails[] = "IMS_ITEM_LIST ID trim update - FULL";
            } else {
                $sql = "UPDATE ims_item_list SET item_id = rtrim(item_id) 
                            WHERE rtrim(item_id) IN ('" . implode("','", $item_ids) . "')";
                db()->update($sql);
                $logdetails[] = "IMS_ITEM_LIST ID trim update - after " . $last_modified;
            }
        }


        // ims_item_price
        $sql = "SELECT modified_on FROM ims_item_price ORDER BY modified_on DESC LIMIT 1; ";
        $result = db()->select($sql);
        $last_modified = date('Y-m-d H:i:s', strtotime($result[0]->modified_on) + 1 );
        $logdetails[] = "IMS_ITEM_PRICE Last Modified: " . $last_modified;

        $item_ids = [];
        $sql = "SELECT * FROM ims_item_price WHERE modified_on > '$last_modified'; ";
        $item_list = (array) db('pg_cherps')->select($sql);
        foreach($item_list as $l) {
            $item_ids[] = trim($l->item_id);
        }
        if (count($item_ids)>0) {
            $sql = "SELECT * FROM ims_item_price WHERE rtrim(item_id) IN ('" . implode("','", $item_ids) . "')";
            $list = (array)db('pg_cherps')->select($sql);
            $result = array_map(function ($value) {
                $value->created_on = date('Y-m-d H:i:s', strtotime($value->created_on));
                $value->modified_on = date('Y-m-d H:i:s', strtotime($value->modified_on));
                return (array)$value;
            }, $list);
            $logdetails[] = "IMS_ITEM_PRICE Item ID to delete: " . implode(", ", $item_ids);

            $sql = "DELETE FROM ims_item_price WHERE rtrim(item_id) IN ('" . implode("','", $item_ids) . "')";
            db()->delete($sql);
            db()->table('ims_item_price')->insert($result);
            $logdetails[] = "IMS_ITEM_PRICE Inserted records: " . count($result);

            if (date('H') < 9) {
                $sql = "UPDATE ims_item_price SET item_id = rtrim(item_id), price_type = rtrim(price_type) ; ";
                db()->update($sql);
                $logdetails[] = "IMS_ITEM_PRICE ID trim update - FULL";
            } else {
                $sql = "UPDATE ims_item_price SET item_id = rtrim(item_id), price_type = rtrim(price_type) 
                            WHERE rtrim(item_id) IN ('" . implode("','", $item_ids) . "')";
                db()->update($sql);
                $logdetails[] = "IMS_ITEM_PRICE ID trim update - after " . $last_modified;
            }
        }

        // ims_live_prices
        $sql = "SELECT modified_on FROM ims_live_prices ORDER BY modified_on DESC LIMIT 1; ";
        $result = db()->select($sql);
        $last_modified = date('Y-m-d H:i:s', strtotime($result[0]->modified_on) + 1 );
        $logdetails[] = "IMS_LIVE_PRICES Last Modified: " . $last_modified;

        $sql = "select i.coy_id,i.item_id,l.lot_id,l.lot_id2,l.loc_id,l.unit_cost,l.unit_price regular_price,l.member_price mbr_price, 0 promo_price, '' promo_id, 
				 now()::DATE eff_from, (NOW() + INTERVAL '1 DAY')::DATE eff_to,
				l.modified_by,l.modified_on 
			from ims_inv_physical_lot l
			join ims_item_list i on l.coy_id=i.coy_id and i.item_id=l.item_id
			where i.coy_id='CTL' and i.item_type in ('X','O') and l.modified_on > '$last_modified' ";
        $prices = db('pg_cherps')->select($sql);

        if (count($prices)>0) {
            // Delete if item is in
            $item_ids = [];
            foreach($prices as $prc) {
                $sql = "DELETE FROM ims_live_prices WHERE coy_id='". $prc->coy_id ."' AND item_id='". $prc->item_id ."' 
                            AND lot_id='". $prc->lot_id ."' AND lot_id2='". $prc->lot_id2 ."' AND loc_id='". $prc->loc_id ."' ";
                db()->delete($sql);
                $item_ids[] = $prc->item_id .' ('.$prc->lot_id.' - '.$prc->loc_id.')';
            }
            $logdetails[] = "IMS_LIVE_PRICES Item ID to refresh: " . implode(", ", $item_ids);

            $result = array_map(function ($value) {
                return (array)$value;
            }, $prices);
            db()->table('ims_live_prices')->insert($result);

            $logdetails[] = "IMS_LIVE_PRICES Refreshed records: " . count($result);
        }

        return $logdetails;
    }

    public function pos_from_cherps2_to_cherps($id=''){

        $sql_id = [];
        $today_list_from_pg = [];
        $today_list_from_ms = [];
        /*
        if ($id != '') {
            $sql_id[] = $id;
        }
        else {

            $today_time = (isset($_GET['date'])) ? strtotime($_GET['date']) : strtotime("now");
            $slice = (isset($_GET['slice'])) ? strtotime($_GET['slice']) : 150;
            $today_list_from_pg = [];
            $today_list_from_ms = [];

            // Get 100 orders that are in pg_cherps, not in ms_cherps
            $today_date = date('Y-m-d', $today_time);
            $sql = "SELECT trans_id from pos_transaction_list where coy_id='CTL' and trans_date>='$today_date' ";
            $result = (array)db('ms_cherps')->select($sql);
            foreach($result as $row) {
                $today_list_from_ms[] = trim($row->trans_id);
            }

            $sql = "SELECT trans_id from pos_transaction_list where coy_id='CTL' and trans_date>='$today_date' ";
            $result = (array)db('pg_cherps')->select($sql);
            foreach($result as $row) {
                $today_list_from_pg[] = trim($row->trans_id);
            }

            $sql_id1 = array_diff($today_list_from_pg,$today_list_from_ms);
            $sql_id = array_slice($sql_id1, 0, $slice, true);

        }

        if (count($sql_id)>0) {

            // Copy insert - list
            $sql = "SELECT * from pos_transaction_list where trans_id in ('" . implode($sql_id, "','") . "') ";
            $list = (array)db('pg_cherps')->select($sql);
            $result = array_map(function ($value) {
                $value->trans_date = date('Y-m-d H:i:s', strtotime($value->trans_date));
                $value->created_on = date('Y-m-d H:i:s', strtotime($value->created_on));
                $value->modified_on = date('Y-m-d H:i:s', strtotime($value->modified_on));
                return (array)$value;
            }, $list);
            db('ms_cherps')->table('pos_transaction_list')->insert($result);

            // Copy insert - item
            $sql = "SELECT * from pos_transaction_item where trans_id in ('" . implode($sql_id, "','") . "') ";
            $list = (array)db('pg_cherps')->select($sql);
            $result = array_map(function ($value) {
                $value->created_on = date('Y-m-d H:i:s', strtotime($value->created_on));
                $value->modified_on = date('Y-m-d H:i:s', strtotime($value->modified_on));
                return (array)$value;
            }, $list);
            db('ms_cherps')->table('pos_transaction_item')->insert($result);

            // Copy insert - lot
            $sql = "SELECT * from pos_transaction_lot where trans_id in ('" . implode($sql_id, "','") . "') ";
            $list = (array)db('pg_cherps')->select($sql);
            $result = array_map(function ($value) {
                $value->created_on = date('Y-m-d H:i:s', strtotime($value->created_on));
                $value->modified_on = date('Y-m-d H:i:s', strtotime($value->modified_on));
                $value->datetime_udf1 = date('Y-m-d H:i:s', strtotime($value->datetime_udf1));
                $value->datetime_udf2 = date('Y-m-d H:i:s', strtotime($value->datetime_udf2));
                $value->datetime_udf3 = date('Y-m-d H:i:s', strtotime($value->datetime_udf3));
                return (array)$value;
            }, $list);
            db('ms_cherps')->table('pos_transaction_lot')->insert($result);

            // Copy insert - pay
            $sql = "SELECT * from pos_payment_list where trans_id in ('" . implode($sql_id, "','") . "') ";
            $list = (array)db('pg_cherps')->select($sql);
            $result = array_map(function ($value) {
                $value->created_on = date('Y-m-d H:i:s', strtotime($value->created_on));
                $value->modified_on = date('Y-m-d H:i:s', strtotime($value->modified_on));
                return (array)$value;
            }, $list);
            db('ms_cherps')->table('pos_payment_list')->insert($result);

        }
        */


        // TEMP EXTRA - copy o2o_email_resent_job
        $sql = "select * from o2o_email_resent_job where status_level is null or status_level=0; ";
        $list = (array)db('pg_cherps')->select($sql);
        $result = array_map(function ($value) {
            unset($value->id);
            $value->created_on = date('Y-m-d H:i:s', strtotime($value->created_on));
            return (array)$value;
        }, $list);
        db('ms_cherps')->table('o2o_email_resent_job')->insert($result);

        $sql = "update o2o_email_resent_job set status_level=3 where status_level is null or status_level=0; ";
        db('pg_cherps')->update($sql);

//        if ( date('H') == 14) {
//            // LAST OF DAY - EOD for JL
//            $today_date = date('Y-m-d', strtotime('2020-08-31'));
//            $sql = "select * from pos_eod_list where loc_id='JL' and cast(trans_date as date)='$today_date'; ";
//            $list = (array)db('ms_cherps')->select($sql);
//            print_r($list);exit;
//            if (count($list) == 0) {
//                $sql = "select * from pos_eod_list where loc_id='JL' and trans_date::date='$today_date'::date; ";
//                $list = (array)db('pg_cherps')->select($sql);
//                $result = array_map(function ($value) {
//                    $value->trans_date = date('Y-m-d H:i:s', strtotime($value->trans_date));
//                    $value->created_on = date('Y-m-d H:i:s', strtotime($value->created_on));
//                    $value->modified_on = date('Y-m-d H:i:s', strtotime($value->modified_on));
//                    return (array)$value;
//                }, $list);
//                db('ms_cherps')->table('pos_eod_list')->insert($result);
//            }
//        }

        $msg = count($sql_id) . " of " . (count($today_list_from_pg) - count($today_list_from_ms)) . " transaction synced. ";
        $msg.= count($result) . " email jobs. ";

        if (isset($_GET['debug']) && $_GET['debug']==1) {
            $msg .= ' Trans: ' . implode(", ", $sql_id);
        }

        return $msg;

    }

    public function crmpromo_from_chrooms_to_cherps2(){

        $last_exec = db()->table('table_trans_list')->where("trans_name","JOB.CRMPROMOPRICE")->max("modified_on");
        db()->table('table_trans_list')->where("trans_name","JOB.CRMPROMOPRICE")->update(["modified_on" => date('Y-m-d H:i:s')]);
        $change_count = db()->table('crm_promo_list')->where("modified_on", ">", date('Y-m-d H:i:s', strtotime($last_exec)))->count();

        if ($change_count == 0 || strtotime($last_exec) > strtotime("15 minutes ago")) {
            // Quit if nothing is changed
            return ["sync" => false, "sets" => 0, "items" => 0];
        }

        db()->table('crm_promo_price')->truncate();
        $sql = "insert into crm_promo_price
                    select l.coy_id, i.item_id, promo_type price_type, l.promo_id ref_id,
                           (ROW_NUMBER () OVER (PARTITION BY i.item_id ORDER BY l.promo_id)) line_num,
                           eff_from, eff_to, 'SGD' curr_id, i.unit_price, promo_disc disc_percent, l.price_ind,
                           (select MAX(loc_id) from crm_promo_location where coy_id=l.coy_id and promo_id=l.promo_id) created_by,
                           '1900-01-01' created_on, l.modified_by, l.modified_on
                    from crm_promo_list l
                    join crm_promo_item i ON l.coy_id=i.coy_id AND l.promo_id=i.promo_id
                    where l.coy_id='CTL' and l.promo_type='PRC' and l.eff_from<now() and l.eff_to>now()
                    order by line_num desc;";
        db()->statement($sql);

        db('pg_cherps')->table('crm_promo_price')->truncate();

        $itms = db()->table('crm_promo_price')->get()->toArray();
        $set_list = array_chunk($itms, 1000);
        foreach($set_list as $set) {
            $setdata = json_decode(json_encode($set), true);
            db('pg_cherps')->table('crm_promo_price')->insert($setdata);
        }

        return ["sync" => true, "sets" => count($set_list), "items" => count($itms)];
    }

    public function product_id_last_modified(){
        $sql = "SELECT item_id,modified_on FROM v_ims_live_product order by modified_on DESC";
        $list = (array) db()->select($sql);
        return $list;
    }

}