<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 23/7/2018
 * Time: 2:46 PM
 */

namespace App\Repositories;


class MemberTransactionRepository
{
    public function get($transType, $transId)
    {
        return db('sql')->table('crm_member_transaction')->where([
            'trans_type' => $transType,
            'trans_id'   => $transId,
        ])->get();
    }
}