<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 10/9/2018
 * Time: 5:54 PM
 */

namespace App\Repositories;

class PaymentAnalysisRepository{

    public function analysis_list(){

        $last_date = '';

        //Find num of rows in table
        $sql = "SELECT count(*) AS exact_row_count FROM o2o_order_analysis_list;";
        $result = db()->select($sql);

        if($result[0]->exact_row_count > 0)
        {
            //Get the latest record created_on
            $sql_date = "SELECT created_on FROM o2o_order_analysis_list  ORDER BY created_on DESC LIMIT 1";
            $result = db()->select($sql_date);

            $last_date = $result[0]->created_on;

            $sql_insert = "INSERT INTO o2o_order_analysis_list (coy_id, customer_id, cart_id, order_num, order_type, invoice_id, ref_id, loc_id, pos_id, order_total, total_due, total_paid, pay_on, placed_on, order_status_level, pay_status_level, trans_id, pay_mode, card_num, trans_amount, is_success, custom_data, card_pan, card_country, reason, created_by, created_on)
                SELECT ol.coy_id, ol.customer_id, ol.cart_id, ol.order_num, ol.order_type, ol.invoice_id, ol.ref_id, ol.loc_id, ol.pos_id, ol.order_total,
                        ol.total_due, ol.total_paid, ol.pay_on, ol.placed_on, ol.order_status_level, ol.pay_status_level, op.trans_id, op.pay_mode, op.card_num, op.trans_amount, op.is_success, 
                        op.custom_data, 
                        CASE
                            WHEN op.pay_mode = 'WECHAT' THEN op.custom_data -> 'gateway' -> 'payload' ->> 'pan'
                            WHEN op.pay_mode = 'NETSQR' THEN ''
                            WHEN op.pay_mode = 'S2C2P' THEN op.custom_data -> 'gateway' -> 'payload' ->> 'pan'
                            WHEN op.pay_mode = 'AMEX'  THEN ''
                            WHEN op.pay_mode = 'CARD'  THEN op.custom_data -> 'terminal' ->> 'card_number'
                        END AS card_pan,
                        CASE
                            WHEN op.pay_mode = 'WECHAT' THEN op.custom_data -> 'gateway' -> 'payload' ->> 'issuerCountry'
                            WHEN op.pay_mode = 'NETSQR' THEN ''
                            WHEN op.pay_mode = 'S2C2P' THEN op.custom_data -> 'gateway' -> 'payload' ->> 'issuerCountry'
                            WHEN op.pay_mode = 'AMEX'  THEN ''
                            WHEN op.pay_mode = 'CARD'  THEN ''
                        END AS card_country,
                        CASE
                            WHEN op.pay_mode = 'WECHAT' THEN op.custom_data -> 'gateway' ->> 'message'
                            WHEN op.pay_mode = 'NETSQR' THEN op.custom_data -> 'gateway' ->> 'message'
                            WHEN op.pay_mode = 'S2C2P' THEN op.custom_data -> 'gateway' ->> 'message'
                            WHEN op.pay_mode = 'AMEX'  THEN op.custom_data -> 'gateway' ->> 'message'
                            WHEN op.pay_mode = 'CARD'  THEN op.custom_data -> 'gateway' ->> 'message'
                        END AS reason,
                        ol.created_by,
                        ol.created_on
                FROM o2o_order_list ol
                left join o2o_order_payment op
                on ol.id = op.order_id
                where ol.order_type in ('HO', 'PS', 'DU', 'DM', 'ID', 'IZ', 'RF', 'EX', 'E1')
                and ol.created_on > '$last_date'
                order by ol.created_on desc";
            $result = db()->insert($sql);
        }
        else
        {
            $sql = "INSERT INTO o2o_order_analysis_list (coy_id, customer_id, cart_id, order_num, order_type, invoice_id, ref_id, loc_id, pos_id, order_total, total_due, total_paid, pay_on, placed_on, order_status_level, pay_status_level, trans_id, pay_mode, card_num, trans_amount, is_success, custom_data, card_pan, card_country, reason, created_by, created_on)
                SELECT ol.coy_id, ol.customer_id, ol.cart_id, ol.order_num, ol.order_type, ol.invoice_id, ol.ref_id, ol.loc_id, ol.pos_id, ol.order_total,
                        ol.total_due, ol.total_paid, ol.pay_on, ol.placed_on, ol.order_status_level, ol.pay_status_level, op.trans_id, op.pay_mode, op.card_num, op.trans_amount, op.is_success, 
                        op.custom_data, 
                        CASE
                            WHEN op.pay_mode = 'WECHAT' THEN op.custom_data -> 'gateway' -> 'payload' ->> 'pan'
                            WHEN op.pay_mode = 'NETSQR' THEN ''
                            WHEN op.pay_mode = 'S2C2P' THEN op.custom_data -> 'gateway' -> 'payload' ->> 'pan'
                            WHEN op.pay_mode = 'AMEX'  THEN ''
                            WHEN op.pay_mode = 'CARD'  THEN op.custom_data -> 'terminal' ->> 'card_number'
                        END AS card_pan,
                        CASE
                            WHEN op.pay_mode = 'WECHAT' THEN op.custom_data -> 'gateway' -> 'payload' ->> 'issuerCountry'
                            WHEN op.pay_mode = 'NETSQR' THEN ''
                            WHEN op.pay_mode = 'S2C2P' THEN op.custom_data -> 'gateway' -> 'payload' ->> 'issuerCountry'
                            WHEN op.pay_mode = 'AMEX'  THEN ''
                            WHEN op.pay_mode = 'CARD'  THEN ''
                        END AS card_country,
                        CASE
                            WHEN op.pay_mode = 'WECHAT' THEN op.custom_data -> 'gateway' ->> 'message'
                            WHEN op.pay_mode = 'NETSQR' THEN op.custom_data -> 'gateway' ->> 'message'
                            WHEN op.pay_mode = 'S2C2P' THEN op.custom_data -> 'gateway' ->> 'message'
                            WHEN op.pay_mode = 'AMEX'  THEN op.custom_data -> 'gateway' ->> 'message'
                            WHEN op.pay_mode = 'CARD'  THEN op.custom_data -> 'gateway' ->> 'message'
                        END AS reason,
                        ol.created_by,
                        ol.created_on
                FROM o2o_order_list ol
                left join o2o_order_payment op
                on ol.id = op.order_id
                where ol.order_type in ('HO', 'PS', 'DU', 'DM', 'ID', 'IZ', 'RF', 'EX', 'E1')
                order by ol.created_on desc";
            $result = db()->insert($sql);
        }

        return $last_date ? $last_date : NULL;
    }

    public function analysis_list_by_ho($ho_list){
        //Get the record based on ho number list
        //$sql = "SELECT * from o2o_order_analysis_list where order_num in ($ho_list) order by order_num asc, created_on desc";
        $sql = "SELECT order_num, created_on, 
                        count(order_num) AS number_of_attempt,
                        count(case when is_success = 'true' then is_success end) AS number_of_success_attempt,
                        count(case when is_success <> 'true' then is_success end) AS number_of_failed_attempt,
                        count(case when pos_id = null or pos_id <> '' then pos_id end) AS number_of_pos_txn,
                        count(distinct card_pan) AS number_of_card,
                        COUNT(distinct case when card_country = 'SG' then card_pan end) as number_of_local_card,
                        COUNT(distinct case when card_country <> 'SG' then card_pan end) as number_of_foreign_card,
                        COUNT(case when reason like '%cline%' then reason end) as number_of_declined,
                        COUNT(case when reason like '%fraud%' then reason end) as number_of_fraud,
                        COUNT(case when reason not like any (array['%cline%', '%fraud%']) then reason end) as number_of_others
                from o2o_order_analysis_list
                where order_num in ($ho_list)
                group by order_num, created_on
                order by order_num asc, created_on desc";
        $result = db()->select($sql);

        return ($result) ? $result : NULL;
    }

    public function analysis_list_by_hi($hi_list){
        //Get the record based on hi number list
        //$sql = "SELECT * from o2o_order_analysis_list where invoice_id in ($hi_list) order by invoice_id asc, created_on desc";
        $sql = "SELECT invoice_id, created_on, 
                        count(invoice_id) AS number_of_attempt,
                        count(case when is_success = 'true' then is_success end) AS number_of_success_attempt,
                        count(case when is_success <> 'true' then is_success end) AS number_of_failed_attempt,
                        count(case when pos_id = null or pos_id <> '' then pos_id end) AS number_of_pos_txn,
                        count(distinct card_pan) AS number_of_card,
                        COUNT(distinct case when card_country = 'SG' then card_pan end) as number_of_local_card,
                        COUNT(distinct case when card_country <> 'SG' then card_pan end) as number_of_foreign_card,
                        COUNT(case when reason like '%cline%' then reason end) as number_of_declined,
                        COUNT(case when reason like '%fraud%' then reason end) as number_of_fraud,
                        COUNT(case when reason not like any (array['%cline%', '%fraud%']) then reason end) as number_of_others
                from o2o_order_analysis_list
                where invoice_id in ($hi_list)
                group by invoice_id, created_on
                order by invoice_id asc, created_on desc";
        $result = db()->select($sql);

        return ($result) ? $result : NULL;
    }

    public function analysis_detail($txn_id){
        //Get the record based on invoice_id or order_num
        $sql = "SELECT * FROM o2o_order_analysis_list 
                WHERE order_num = '$txn_id' 
                OR invoice_id = '$txn_id' 
                order by created_on desc";

        $result = db()->select($sql);

        return ($result) ? $result : NULL;
    }
}