<?php

namespace App\Repositories;


class ProductRepository
{

    public function getLivePrices($item_id,$lot_id,$loc_id){
        $sql = "select item_id,lot_id,lot_id2,loc_id,unit_cost,regular_price,mbr_price,promo_price,promo_id
                from ims_live_prices where item_id='$item_id' and lot_id='$lot_id' ";
        $sql.= (!empty($loc_id)) ? "and loc_id='$loc_id'" : "";
        $products = db()->select($sql);
        return ($products) ? $products[0] : NULL;
    }

    public function getSearch($field,$keyword,$eq='like') {
        $sql_extra = ($field=="item_desc") ? " or item_id like '$keyword' " : "";
        $sql = "select * from v_ims_live_product where lower($field) $eq lower('$keyword') $sql_extra LIMIT 50";
        $products = db()->select($sql);
        return collect($products);
    }

    public function getOne($item_id) {
        if (strlen($item_id)==12) {
            $product = db()->table('v_ims_live_product')->where('v_ims_live_product.item_id', 'like', $item_id . '%')->first();
        }
        else {
            $product = db()->table('v_ims_live_product')->where('v_ims_live_product.item_id', $item_id)->first();
        }
        return $product;
    }

    public function getPwp($item_id,$loc_id='**') {
        /*
        $product = db()->table('o2o_live_promo')
            ->join('o2o_live_product', 'o2o_live_product.item_id', '=', 'o2o_live_promo.promo_ref_id')
            ->where('o2o_live_promo.promo_type', 'PWP')->where('o2o_live_promo.ref_id', $item_id)->where('o2o_live_promo.loc_id', $loc_id)
            ->whereRaw('o2o_live_promo.eff_from <= NOW() and o2o_live_promo.eff_to >= NOW() and  o2o_live_promo.time_from <= CURRENT_TIME and o2o_live_promo.time_to >= CURRENT_TIME')
            ->get();
        return $product;
        */


        $sql = "select *, v_ims_live_promo.promo_price unit_price from v_ims_live_promo
                join v_ims_live_product ON v_ims_live_product.item_id=v_ims_live_promo.promo_ref_id
                where v_ims_live_promo.promo_type='PWP' and v_ims_live_promo.ref_id='$item_id' and v_ims_live_promo.loc_id in ('**','BF')
                and v_ims_live_promo.eff_from <= NOW() and v_ims_live_promo.eff_to >= NOW() and  v_ims_live_promo.time_from <= CURRENT_TIME and v_ims_live_promo.time_to >= CURRENT_TIME 
                order by promo_grp_id,eff_to";
        $pwp_products = db()->select($sql);
//        $pwp_groups = array();
//        foreach ($pwp_products as $p){
//            $key = trim($p->promo_grp_id);
//            $pwp_groups[$key][] = $p;
//        }
        return collect($pwp_products);
    }


    public function getProducts($itemId)
    {
        $products = $this->getQuery()->whereIn('item_id', $itemId)->get();
        $products->each(function ($item) {
            $item->item_id = trim($item->item_id);
        });

        return $products;
    }

}