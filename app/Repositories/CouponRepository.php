<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 10/9/2018
 * Time: 5:54 PM
 */

namespace App\Repositories;


use Carbon\Carbon;

class CouponRepository
{
    public function getCouponsByMbrId($mbrId)
    {
        return $this->getQuery()->where('mbr_id', $mbrId)->get();
    }

    public function getUnusedCouponsByMbrId($mbrId)
    {
        return $this->getQuery()->where('mbr_id', $mbrId)->where('expiry_date', '>', Carbon::now())->where('status_level', 0)->orderBy('created_on', 'DESC')->get();
    }

    public function getQuery()
    {
        return db('sql')->table('crm_voucher_list')->where('coy_id', 'CTL');
    }
}