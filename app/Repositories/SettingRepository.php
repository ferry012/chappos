<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 10/9/2018
 * Time: 5:54 PM
 */

namespace App\Repositories;

class SettingRepository{

    public function api_log($data){
        db()->table('api_log')->insert($data);
    }

    public function ins_order_resource($data){
        db()->table('o2o_order_resource')->insert($data);
    }

    public function api_log_read($prefix) {
        $sql = "SELECT api_description,request_on,request_body from api_log WHERE api_client='logkeeper' and api_description like '$prefix%' LIMIT 200";
        $result = db()->select($sql);
        return ($result) ? $result : NULL ;
    }

    public function pos_location($loc_id) {
        $sql = "SELECT loc_id,loc_name,loc_zone,loc_mall,street_line1,street_line2,logo_id FROM pos_location_list WHERE loc_id='$loc_id' LIMIT 1 ";
        $result = db()->select($sql);
        return ($result) ? $result[0] : NULL ;
    }

    public function pos_insert_token($id,$param){
        $data = [
            'token' => $id,
            'custom_data' => json_encode($param),
            'created_on' => date('Y-m-d')
        ];
        db()->table('api_session')->insert($data);
        return $data['token'];
    }

    public function device_get($device_id){
        $where = ['device_id' => $device_id];
        $insertParams['last_version'] = (isset($_GET['app_version'])) ? $_GET['app_version'] : '';
        $insertParams['last_login'] = date('Y-m-d H:i:s');
        db()->table('pos_device_list')->where($where)->update($insertParams);

        $sql = "SELECT trim(coy_id) as coy_id,trim(loc_id) as loc_id,trim(pos_id) as pos_id, posapi_endpoint, terminal_ip, terminal_settings
                    FROM pos_device_list
                    WHERE status_level>=0 AND device_id='$device_id' ";
        $result = db()->select($sql);
        return ($result) ? $result[0] : NULL ;
    }

    public function device_list(){
        $sql = "SELECT trim(coy_id) as coy_id,trim(loc_id) as loc_id,trim(pos_id) as pos_id, posapi_endpoint, terminal_ip, last_version, last_login
                    FROM pos_device_list
                    order by loc_id,pos_id";
        $result = db()->select($sql);
        return ($result) ? $result : NULL ;
    }

    public function device_upd($param){
        $sql = "select * from pos_device_list where device_id='".$param['device_id']."' ";
        $result = db()->select($sql);
        if ($result) {
            $where = ['device_id' => $param['device_id']];
            if (isset($param['coy_id']))
                $insertParams['coy_id'] = $param['coy_id'];
            if (isset($param['loc_id']))
                $insertParams['loc_id'] = $param['loc_id'];
            if (isset($param['pos_id']))
                $insertParams['pos_id'] = $param['pos_id'];
            if (isset($param['posapi_endpoint']))
                $insertParams['posapi_endpoint'] = $param['posapi_endpoint'];
            if (isset($param['terminal_ip']))
                $insertParams['terminal_ip'] = $param['terminal_ip'];
            if (isset($param['terminal_settings']))
                $insertParams['terminal_settings'] = json_encode($param['terminal_settings']);
            $insertParams['status_level'] = 1;
            $insertParams['modified_by'] = $param['usr_id'];
            $insertParams['modified_on'] = date('Y-m-d H:i:s');
            return db()->table('pos_device_list')->where($where)->update($insertParams);
        }
        else {
            $insertParams[] = [
                'device_id' => $param['device_id'],
                'coy_id' => $param['coy_id'],
                'loc_id' => $param['loc_id'],
                'pos_id' => $param['pos_id'],
                'terminal_ip' => (isset($param['terminal_ip'])) ? $param['terminal_ip'] : '',
                'posapi_endpoint' => (isset($param['posapi_endpoint'])) ? $param['posapi_endpoint'] : '',
                'terminal_settings' => (isset($param['terminal_settings'])) ? json_encode($param['terminal_settings']) : '{}',
                'status_level' => 1,
                'created_by' => $param['usr_id'],
                'created_on' => date('Y-m-d H:i:s'),
                'modified_by' => $param['usr_id'],
                'modified_on' => date('Y-m-d H:i:s')
            ];

            db()->table('pos_device_list')->insert($insertParams);
        }
    }


    public function func_hotkeys($loc_id){
        $sql = "SELECT TRIM(func_tag) as id, group_name as name, image_name as img, TRIM(func_id) func_id, TRIM(func_type) as type
                    FROM pos_function_list
                    WHERE status_level>=0 and seq_no>0 AND func_type in ('HOTKEY','COUPON','ITEMCUSTOM') AND TRIM(func_dim3) in ('','$loc_id') AND func_tag <> ''
                    ORDER BY seq_no";
        $result = db()->select($sql);
        return $result;
    }

    public function func_paylist($loc_id,$loc_mall){
        $sql = "SELECT TRIM(func_tag) as id, group_name as name, image_name as img, TRIM(func_id) as func_id,func_name,func_dim4 as func_map
                    FROM pos_function_list
                    WHERE status_level>=0 and seq_no>0 AND func_type in ('PAYMENT') AND TRIM(func_dim3) in ('','$loc_id','$loc_mall') AND func_tag <> ''
                    ORDER BY seq_no";
        $result = db()->select($sql);
        return $result;
    }

    public function func_paydefaultlist(){
        $sql = "SELECT TRIM(func_tag) as id, group_name as name, image_name as img, TRIM(func_id) as func_id,func_name,func_dim4 as func_map,func_dim5 as func_other
                    FROM pos_function_list
                    WHERE status_level>=0 and seq_no>0 AND func_type in ('PAYMENT1') AND func_tag <> ''
                    ORDER BY func_type,seq_no";
        $result = db()->select($sql);
        return $result;
    }

    public function func_type($type){
        $sql = "SELECT TRIM(func_id) as func_id,func_name, image_name as img
                    FROM pos_function_list
                    WHERE status_level>=0 and func_type = '$type'
                    ORDER BY seq_no,func_id";
        $result = db()->select($sql);
        return $result;
    }

    public function func_coupons($func_tag) {
        $sql_datetime = date('Y-m-d H:i:s');

//        $sql = "select trim(c.coupon_id) coupon_id,trim(c.coupon_type) coupon_type,
//                  trim(COALESCE(c.coupon_code,'')) coupon_code,
//                  c.coupon_name,c.coupon_excerpt,
//                  c.eff_to as expiry_date,
//                  CASE WHEN max_uses IS NOT NULL THEN (max_uses-uses_count) ELSE 1 END as quantity
//                from pos_function_list f
//                join crm_coupon_promo c ON f.func_tag=c.coupon_tag
//                where f.func_tag='' and eff_from < '$sql_datetime' and eff_to >= '$sql_datetime'
//                order by coupon_name ";
//        $result = db()->select($sql);

        $result = db()->table('v_ims_live_coupon')->where('func_id', $func_tag)->where('eff_from','<',$sql_datetime)->where('eff_to','>=',$sql_datetime)->get();
        return $result;
    }

    public function eod_get_summary($coy_id,$loc_id,$pos_id) {
        $sql = "SELECT 'ALL' as order_type, COUNT(*) ord_count, sum(order_total) ord_total, MIN(created_on) eff_from,
                    (CASE WHEN MAX(pay_on) is null THEN MAX(created_on) ELSE MAX(pay_on) END) + (2 * interval '1 minute') eff_to
                FROM o2o_order_list o
                  WHERE coy_id='$coy_id' AND loc_id='$loc_id' AND pos_id='$pos_id'
                    AND created_on > (COALESCE((SELECT MAX(eff_to) FROM o2o_payment_eod WHERE coy_id='$coy_id' AND loc_id='$loc_id' AND pos_id='$pos_id' AND status_level>=1),'2000-01-01'))
            UNION ALL
                SELECT 'VOID' as order_type, COUNT(*) ord_count, sum(order_total) ord_total, MIN(created_on) eff_from, MAX(modified_on) eff_to
                FROM o2o_order_list o
                  WHERE coy_id='$coy_id' AND loc_id='$loc_id' AND pos_id='$pos_id' AND order_status_level<=0
                    AND created_on > (COALESCE((SELECT MAX(eff_to) FROM o2o_payment_eod WHERE coy_id='$coy_id' AND loc_id='$loc_id' AND pos_id='$pos_id' AND status_level>=1),'2000-01-01'))
            UNION ALL    
                SELECT order_type, count(*) ord_count, sum(order_total) ord_total, MIN(created_on) eff_from, MAX(pay_on) eff_to
                FROM o2o_order_list o
                  WHERE coy_id='$coy_id' AND loc_id='$loc_id' AND pos_id='$pos_id' AND order_status_level>=1
                    AND created_on > (COALESCE((SELECT MAX(eff_to) FROM o2o_payment_eod WHERE coy_id='$coy_id' AND loc_id='$loc_id' AND pos_id='$pos_id' AND status_level>=1),'2000-01-01'))
                GROUP BY order_type;";
        $result = db()->select($sql);
        $summ = [
            "issued"=>["count"=>0,"amount"=>0.00],
            "refund"=>["count"=>0,"amount"=>0.00],
            "void"=>["count"=>0,"amount"=>0.00]
        ];
        foreach($result as $d) {
            if ($d->order_type=='ALL') {
                $eff_from = $d->eff_from;
                $eff_to = $d->eff_to;
            }
            else if ($d->order_type=='VOID') {
                $summ['void']['count'] += $d->ord_count;
                $summ['void']['amount'] += number_format((float) $d->ord_total, 2, '.', '');
            }
            else if ($d->order_type=='RF') {
                $summ['refund']['count'] += $d->ord_count;
                $summ['refund']['amount'] += number_format((float) $d->ord_total, 2, '.', '');
            }
            else {
                $summ['issued']['count'] += $d->ord_count;
                $summ['issued']['amount'] +=  number_format((float) $d->ord_total, 2, '.', '');
            }
        }
        $summary = [
            "issued_receipts" => $summ['issued'],
            "refunded_receipts" => $summ['refund'],
            "voided_receipts" => $summ['void'],
            "trans_date" => [
                "eff_from" => $eff_from,
                "eff_to" => $eff_to
            ]
        ];
        return $summary;
    }

    public function eod_get_submitted($coy_id,$loc_id,$pos_id){
        $sql = "SELECT eod_id,trans_date,seq_no,pay_mode,pay_type,'' pay_desc,system_count,system_amount,cashier_count,cashier_amount,eff_from,eff_to,custom_data, created_by,created_on
                  FROM o2o_payment_eod WHERE coy_id='$coy_id' and loc_id='$loc_id' and pos_id='$pos_id' and status_level=0 ORDER BY seq_no;";
        $pendingeod = db()->select($sql);
        return $pendingeod;
    }

    public function eod_get_payments($coy_id,$loc_id,$pos_id,$trans_date){
        $sql = "select
                  trim(f.func_type) func_type, (CASE WHEN f.seq_no=0 THEN 1000+f.seq_no ELSE f.seq_no END) seq_no,pay_mode,
                  (case when pay_mode='CASH' then 'Coins' else pay_type end) pay_type,
                  (case when pay_mode='CASH' then 'CASH Coins' when pay_mode='INSTALLMENT' then CONCAT(f.func_name,' (',fin_dim3,')') else f.func_name end) pay_desc,
                  '' fin_dim2, fin_dim3,
                  count(DISTINCT order_num) system_count,sum(trans_amount) system_amount, 0 cashier_count, 0 cashier_amount, 0 cashier_locked
                from (
                  SELECT
                    l.order_num,
                    l.order_type,
                    l.placed_on,
                    l.loc_id,
                    l.pos_id,
                    l.order_total,
                    l.ref_id,
                    l.order_status_level,
                    l.pay_status_level,
                    CASE
                        WHEN p.pay_mode='CASH_ROUNDING_ADJ' THEN ''
                        WHEN p.pay_mode='CASH_CHANGE_DUE' THEN 'CASH'
                        WHEN p.pay_mode='NETS_QR_PURCHASE' THEN 'NETS_PURCHASE'
                        ELSE p.pay_mode END pay_mode,
                    CASE 
                        WHEN COALESCE(p.pay_type,'')='DBS_CC_REFUND' THEN 'DBS_CC' 
                        WHEN COALESCE(p.pay_type,'')='UOB_REFUND' THEN 'UOB_CC' 
                        ELSE COALESCE(p.pay_type,'') END pay_type,
                    CASE 
                        WHEN COALESCE(p.pay_type,'')='DBS_CC_REFUND' OR COALESCE(p.pay_type,'')='DBS_CC' THEN 'DBS' 
                        WHEN COALESCE(p.pay_type,'')='UOB_REFUND' OR COALESCE(p.pay_type,'')='UOB_CC' THEN 'UOB' 
                        ELSE '' END fin_dim2,
                    CASE WHEN p.pay_mode='INSTALLMENT' THEN CONCAT( LPAD(RIGHT(COALESCE(regexp_replace(p.custom_data->'installment'->>'month','\D','','g'),''),2),2,'0') ,' Months')
                        ELSE '' END fin_dim3,
                    CASE WHEN ( (l.order_type='RF' and p.pay_mode='CASH_CHANGE_DUE') or p.pay_mode='MV_ADJ') THEN 0 ELSE p.trans_amount END trans_amount,
                    p.custom_data
                  FROM o2o_order_list l
                    JOIN o2o_order_payment p ON l.id = p.order_id
                  WHERE l.order_status_level >= 1 AND l.pay_status_level >= 2 AND l.pay_on >= '". $trans_date['eff_from'] ."'
                        AND l.coy_id = '$coy_id' AND l.loc_id = '$loc_id' AND l.pos_id = '$pos_id'
                  --ORDER BY pay_mode,pay_type desc
                ) t
                JOIN pos_function_list f ON func_type in ('PAYMENT','PAYMENT1') AND func_id=(CASE WHEN (COALESCE(pay_type,'')='') THEN pay_mode ELSE pay_type END)
                group by func_type,func_name,seq_no,pay_mode,pay_type,fin_dim2,fin_dim3
                order by func_type desc,seq_no asc;";
        $generated_db = db()->select($sql);

        $generated[] = (object) ["func_type"=>'PAYMENT1', "seq_no"=>1, "pay_mode"=>'CASH', "pay_type"=>'$2', "pay_desc"=>'CASH $2', "system_count"=>0, "system_amount"=>0, "cashier_count"=>0, "cashier_amount"=>0, "cashier_locked"=>0];
        $generated[] = (object) ["func_type"=>'PAYMENT1', "seq_no"=>1, "pay_mode"=>'CASH', "pay_type"=>'$5', "pay_desc"=>'CASH $5', "system_count"=>0, "system_amount"=>0, "cashier_count"=>0, "cashier_amount"=>0, "cashier_locked"=>0];
        $generated[] = (object) ["func_type"=>'PAYMENT1', "seq_no"=>1, "pay_mode"=>'CASH', "pay_type"=>'$10', "pay_desc"=>'CASH $10', "system_count"=>0, "system_amount"=>0, "cashier_count"=>0, "cashier_amount"=>0, "cashier_locked"=>0];
        $generated[] = (object) ["func_type"=>'PAYMENT1', "seq_no"=>1, "pay_mode"=>'CASH', "pay_type"=>'$50', "pay_desc"=>'CASH $50', "system_count"=>0, "system_amount"=>0, "cashier_count"=>0, "cashier_amount"=>0, "cashier_locked"=>0];
        $generated[] = (object) ["func_type"=>'PAYMENT1', "seq_no"=>1, "pay_mode"=>'CASH', "pay_type"=>'$100', "pay_desc"=>'CASH $100', "system_count"=>0, "system_amount"=>0, "cashier_count"=>0, "cashier_amount"=>0, "cashier_locked"=>0];
        $generated[] = (object) ["func_type"=>'PAYMENT1', "seq_no"=>1, "pay_mode"=>'CASH', "pay_type"=>'$1000', "pay_desc"=>'CASH $1000', "system_count"=>0, "system_amount"=>0, "cashier_count"=>0, "cashier_amount"=>0, "cashier_locked"=>0];
        $generated = array_merge($generated,$generated_db);
        return $generated;
    }

    public function eod_all_payments()
    {
        $sql = "select
                  trim(func_type) func_type,seq_no,trim(func_tag) pay_mode,trim(func_id) pay_type,func_name pay_desc,
                  0 system_count,0 system_amount, 0 cashier_count, 0 cashier_amount, 0 cashier_locked
                from pos_function_list
                where func_type in ('PAYMENT','PAYMENT1') and seq_no>0
                    and func_id NOT IN ('CASH','CASH_CHANGE_DUE','CASH_ROUNDING_ADJ','NETS_QR_PURCHASE','UOB_REFUND','DBS_CC_REFUND')
                order by func_type desc,seq_no asc";
        $generated = db()->select($sql);
        $templates = array();
//        for ($i=0; $i< count($generated); $i++) {
//            if ($generated[$i]->pay_mode=="INSTALLMENT") {
//                $templates[] = $this->eod_all_payments_split_installment($generated[$i],6);
//            }
//            else {
//                $templates[] = $generated[$i];
//            }
//        }
        foreach ($generated as $gen){
            if ($gen->pay_mode=="INSTALLMENT") {
                $templates[] = $this->eod_all_payments_split_installment($gen,6);
                $templates[] = $this->eod_all_payments_split_installment($gen,12);
                $templates[] = $this->eod_all_payments_split_installment($gen,24);
            }
            else {
                $templates[] = $gen;
            }
        }
        return $templates;
    }

    private function eod_all_payments_split_installment($template,$month){
        $t = (array) $template;
        $mth = str_pad($month ?? '00', 2, '0', STR_PAD_LEFT);
        $t['pay_desc'] .= ' ('.$mth.' Months)';
        $t['fin_dim3'] = $mth . ' Months';
        return (object) $t;
    }

    public function eod_get_payments_summary($coy_id,$loc_id,$pos_id,$trans_date){
        $sql = "select
                  trim(f.func_type) func_type,f.seq_no,pay_mode,(case when pay_mode='CASH' then 'Coins' else pay_type end) pay_type, f.func_name pay_desc,
                  t.order_num,t.order_type,t.trans_amount,t.pay_on
                from (
                  SELECT
                    l.order_num,
                    l.order_type,
                    l.pay_on,
                    l.loc_id,
                    l.pos_id,
                    l.order_total,
                    l.ref_id,
                    l.order_status_level,
                    l.pay_status_level,
                    CASE WHEN p.pay_mode='CASH_ROUNDING_ADJ' THEN '' WHEN p.pay_mode='CASH_CHANGE_DUE' THEN 'CASH' ELSE p.pay_mode END pay_mode,
                    CASE WHEN COALESCE(p.pay_type,'')='DBS_CC_REFUND' THEN 'DBS_CC' WHEN COALESCE(p.pay_type,'')='UOB_REFUND' THEN 'UOB_CC' ELSE COALESCE(p.pay_type,'') END pay_type,
                    CASE WHEN ( (l.order_type='RF' and p.pay_mode='CASH_CHANGE_DUE') or p.pay_mode='MV_ADJ') THEN 0 ELSE p.trans_amount END trans_amount
                  FROM o2o_order_list l
                    JOIN o2o_order_payment p ON l.id = p.order_id
                  WHERE l.order_status_level >= 1 AND l.pay_status_level >= 2 AND l.pay_on >= '". $trans_date['eff_from'] ."'
                        AND l.coy_id = '$coy_id' AND l.loc_id = '$loc_id' AND l.pos_id = '$pos_id'
                  --ORDER BY pay_mode,pay_type desc
                ) t
                JOIN pos_function_list f ON func_type in ('PAYMENT','PAYMENT1') AND func_id=(CASE WHEN (COALESCE(pay_type,'')='') THEN pay_mode ELSE pay_type END)
                --group by func_type,func_name,seq_no,pay_mode,pay_type
                order by f.func_type desc,f.seq_no asc, t.order_num";
        $generated = db()->select($sql);
        return $generated;
    }

    public function eod_generate($coy_id,$loc_id,$pos_id,$trans_date, $summary_json,$usr_id=''){

        // Step 1. Check for pending EOD. If yes, get it.
        $sql = "SELECT trans_date,seq_no,pay_mode,pay_type,'' pay_desc,system_count,system_amount,cashier_count,cashier_amount,eff_from,eff_to,custom_data, created_by,created_on
                  FROM o2o_payment_eod WHERE coy_id='$coy_id' and loc_id='$loc_id' and pos_id='$pos_id' and status_level=0 ORDER BY seq_no;";
        $pendingeod = db()->select($sql);

        // Step 2. Generate from o2o_order_payment
        $generated = $this->eod_get_payments($coy_id,$loc_id,$pos_id,$trans_date);
        for ($i=0; $i<count($generated); $i++){
            if ($generated[$i]->pay_mode=='REBATE') {
                // Minus MEMBER-RR items
                $rr_data = $this->eod_print_getmembership_rr($coy_id,$loc_id,$pos_id,$trans_date['eff_from'],$trans_date['eff_to']);
                $generated[$i]->system_amount -= $rr_data[0]->rr_amount;
            }
            if ( in_array( trim($generated[$i]->pay_mode), ['REBATE','DEPOSIT','DEP_MANUAL','VOUCHER_ADJ','ECREDIT','ECREDIT-RF'] )) {
                $generated[$i]->cashier_amount = $generated[$i]->system_amount;
                $generated[$i]->cashier_count = $generated[$i]->system_count;
                $generated[$i]->cashier_locked = 1;
            }
        }

        // Step 3. Compare the 2 lists, if different - combine to new and delete old.
        if (count($pendingeod)>0){
            foreach ($pendingeod as $old){
                if ($old->cashier_amount>0){
                    foreach ($generated as $pl1){
                        if (trim($old->pay_mode)==trim($pl1->pay_mode) && trim($old->pay_type)==trim($pl1->pay_type)){
                            $pl1->cashier_count = $old->cashier_count;
                            $pl1->cashier_amount = $old->cashier_amount;
                            $pl1->created_by = $old->created_by;
                            $pl1->created_on = $old->created_on;
                        }
                    }
                }
            }
            $sql = "DELETE FROM o2o_payment_eod WHERE coy_id='$coy_id' and loc_id='$loc_id' and pos_id='$pos_id' and status_level=0 ;";
            $ac = db()->delete($sql);
        }

        // Step 4. Insert generated EOD
        $insertParams = [];
        $summ = ['system_count'=>0,'system_amount'=>0,'cashier_count'=>0,'cashier_amount'=>0]; $i=0;
        foreach ($generated as $pl1) {
            $i++;
            $pl1->seq_no = $i;
            $pl = (array) $pl1;
            $insertParams[] = [
                "eod_id"=> $coy_id.$loc_id.$pos_id.date('YmdH'),
                "coy_id"=> $coy_id,
                "loc_id"=> $loc_id,
                "pos_id"=> $pos_id,
                "trans_date"=> date('Y-m-d', strtotime($trans_date['eff_to'])),
                "seq_no"=> $pl['seq_no'],
                "pay_mode"=> $pl['pay_mode'],
                "pay_type"=> $pl['pay_type'],
                "status_level"=> 0,
                "system_count"=> $pl['system_count'],
                "system_amount"=> $pl['system_amount'],
                "cashier_count"=> $pl['cashier_count'],
                "cashier_amount"=> $pl['cashier_amount'],
                "eff_from"=> $trans_date['eff_from'],
                "eff_to"=> $trans_date['eff_to'],
                "custom_data"=> '{}',
                "created_by"=> (isset($pl['created_by'])) ? $pl['created_by'] : $usr_id,
                "created_on"=> (isset($pl['created_on'])) ? $pl['created_on'] : date('Y-m-d H:i:s'),
                "modified_by"=> $usr_id,
                "modified_on"=> date('Y-m-d H:i:s'),
            ];
            $summ['system_count'] += $pl['system_count'];
            $summ['system_amount'] += $pl['system_amount'];
            $summ['cashier_count'] += $pl['cashier_count'];
            $summ['cashier_amount'] += $pl['cashier_amount'];
        }
        $insertParams[] = [
            "eod_id"=> $coy_id.$loc_id.$pos_id.date('YmdH'),
            "coy_id"=> $coy_id,
            "loc_id"=> $loc_id,
            "pos_id"=> $pos_id,
            "trans_date"=> date('Y-m-d', strtotime($trans_date['eff_to'])),
            "seq_no"=> 0,
            "pay_mode"=> 'SUMMARY',
            "pay_type"=> '',
            "status_level"=> 0,
            "system_count"=> $summ['system_count'],
            "system_amount"=> $summ['system_amount'],
            "cashier_count"=> $summ['cashier_count'],
            "cashier_amount"=> $summ['cashier_amount'],
            "eff_from"=> $trans_date['eff_from'],
            "eff_to"=> $trans_date['eff_to'],
            "custom_data"=> $summary_json,
            "created_by"=> $usr_id,
            "created_on"=> date('Y-m-d H:i:s'),
            "modified_by"=> $usr_id,
            "modified_on"=> date('Y-m-d H:i:s'),
        ];
        db()->table('o2o_payment_eod')->insert($insertParams);

        return $generated;
    }

    public function eod_save_trans($where, $records){
        if ($where=='NEW')
            db()->table('o2o_payment_eod')->insert($records);
        else
            db()->table('o2o_payment_eod')->where($where)->update($records);
        return;
    }

    public function eod_post_gettrans($coy_id,$loc_id,$pos_id,$trans_date){
        $sql = "SELECT coy_id,loc_id,pos_id,'N' shift_id,trans_date,seq_no line_num,
                  (select func_dim1 from pos_function_list where coy_id='CTL' and func_type IN ('PAYMENT1','PAYMENT') and func_id= (CASE WHEN o2o_payment_eod.pay_type='' OR o2o_payment_eod.pay_mode='CASH' THEN o2o_payment_eod.pay_mode ELSE o2o_payment_eod.pay_type END) ) pay_mode,
                  CONCAT((select func_name from pos_function_list where coy_id='CTL' and func_type IN ('PAYMENT1','PAYMENT') and func_id= (CASE WHEN o2o_payment_eod.pay_type='' OR o2o_payment_eod.pay_mode='CASH' THEN o2o_payment_eod.pay_mode ELSE o2o_payment_eod.pay_type END) ),(CASE WHEN pay_mode='CASH' THEN CONCAT(' ',pay_type) ELSE '' END)) pay_desc,
                  (select func_dim2 from pos_function_list where coy_id='CTL' and func_type IN ('PAYMENT1','PAYMENT') and func_id= (CASE WHEN o2o_payment_eod.pay_type='' OR o2o_payment_eod.pay_mode='CASH' THEN o2o_payment_eod.pay_mode ELSE o2o_payment_eod.pay_type END) ) fin_dim2,
                  COALESCE(custom_data->>'fin_dim3','') fin_dim3,
                        seq_no ref_num,status_level,
                        system_count,system_amount,cashier_count,cashier_amount,
                        modified_by verified_by,created_by,created_on,modified_by,modified_on
                  FROM o2o_payment_eod
                  WHERE coy_id='$coy_id' and loc_id='$loc_id' and pos_id='$pos_id' and trans_date='".date('Y-m-d',strtotime($trans_date))."'
                    AND pay_mode NOT IN ('SUMMARY','DEPOSIT','VOUCHER_ADJ'); ";
        $eod = db()->select($sql);
        return $eod;
    }

    public function eod_void_transactions($coy_id,$loc_id,$pos_id,$eff_from,$eff_to) {
        $sql = "UPDATE o2o_order_list SET order_status_level=-1 WHERE loc_id='$loc_id' and pos_id='$pos_id' and order_status_level=0 and created_on>='$eff_from' ";
        $ac = db()->update($sql);
        return;
    }

    public function eod_print_getpayments($coy_id,$loc_id,$pos_id,$trans_date) {
        $sql = "SELECT trans_date,seq_no,trim(pay_mode) pay_mode,trim(pay_type) pay_type,'' pay_desc,
                    COALESCE(custom_data->>'fin_dim3','') fin_dim3,
                    system_count,system_amount,cashier_count,cashier_amount,eff_from,eff_to,custom_data,status_level,
                    trim(created_by) created_by, created_on, trim(modified_by) modified_by, modified_on
                  FROM o2o_payment_eod WHERE coy_id='$coy_id' and loc_id='$loc_id' and pos_id='$pos_id' and trans_date='".date('Y-m-d',strtotime($trans_date))."'
                  ORDER BY seq_no;";
        $eod = db()->select($sql);
        return $eod;
    }

    public function eod_print_gettransactions($coy_id,$loc_id,$pos_id,$eff_from,$eff_to) {
        $sql = "select 'ALL' order_num,'ALL' order_type, '' ref_id, 
                  SUM(order_total) order_total, COUNT(*) order_count, SUM(points_earned) points_earned, SUM(points_used) points_used, SUM(tax_amount) tax_amount, SUM(rounding_adj) rounding_adj
                    from o2o_order_list
                    where coy_id='$coy_id' and loc_id='$loc_id' and pos_id='$pos_id' and pay_on>='$eff_from' and pay_on<'$eff_to'
                      and order_status_level >= 1 AND pay_status_level >= 2
                UNION ALL
                select 'DEPOSIT' order_num,'DM' order_type, '' ref_id, 
                  SUM(order_total) order_total, COUNT(*) order_count, SUM(points_earned) points_earned, SUM(points_used) points_used, SUM(tax_amount) tax_amount, SUM(rounding_adj) rounding_adj
                    from o2o_order_list
                    where coy_id='$coy_id' and loc_id='$loc_id' and pos_id='$pos_id' and pay_on>='$eff_from' and pay_on<'$eff_to'
                      and order_status_level >= 1 AND pay_status_level >= 2
                      and order_type in ('DM')
                UNION ALL
                select order_num,'VD' order_type,COALESCE(ref_id,'') ref_id, order_total,1 order_count,points_earned,points_used,tax_amount,rounding_adj
                    from o2o_order_list
                    where coy_id='$coy_id' and loc_id='$loc_id' and pos_id='$pos_id' and order_status_level<0 and created_on>='$eff_from' and created_on<'$eff_to'
                UNION ALL
                select order_num,order_type,COALESCE(ref_id,'') ref_id, order_total,1 order_count,points_earned,points_used,tax_amount,rounding_adj
                    from o2o_order_list
                    where coy_id='$coy_id' and loc_id='$loc_id' and pos_id='$pos_id' and order_status_level >= 1 AND pay_status_level >= 2 and pay_on>='$eff_from' and pay_on<'$eff_to'
                      and order_type in ('RF','EX','E1')
                UNION ALL
                select order_num,order_type,COALESCE(ref_id,'') ref_id, order_total,1 order_count,points_earned,points_used,tax_amount,rounding_adj
                    from o2o_order_list
                    where coy_id='$coy_id' and loc_id='$loc_id' and pos_id='$pos_id' and order_status_level >= 1 AND pay_status_level >= 2 and pay_on>='$eff_from' and pay_on<'$eff_to'
                      and order_type in ('DU')";
        $eod = db()->select($sql);
        return $eod;
    }

    public function eod_print_getmembership($coy_id,$loc_id,$pos_id,$eff_from,$eff_to) {
        $sql = "select item_id,SUM(qty_ordered) count
                    from o2o_order_list l
                    join o2o_order_item i on l.id=i.order_id
                    where l.coy_id='$coy_id' and l.loc_id='$loc_id' and l.pos_id='$pos_id' and l.pay_on>='$eff_from' and l.pay_on<'$eff_to'
                      and l.order_status_level >= 1 AND l.pay_status_level >= 2
                      and (i.item_id like '!MEMBER%' or i.item_type='M')
                    GROUP BY i.item_id; ";
        $eod = db()->select($sql);
        return $eod;
    }

    public function eod_print_getmembership_rr($coy_id,$loc_id,$pos_id,$eff_from,$eff_to) {
        $sql = "select SUM(i.qty_ordered) rr_qty, SUM(i.unit_price) rr_amount
                    from o2o_order_list l
                    join o2o_order_item i on l.id=i.order_id
                    where l.coy_id='$coy_id' and l.loc_id='$loc_id' and l.pos_id='$pos_id' and l.pay_on>='$eff_from' and l.pay_on<'$eff_to'
                      and l.order_status_level >= 1 AND l.pay_status_level >= 2
                      and (i.item_id like '!MEMBER-RR%'); ";
        $eod = db()->select($sql);
        return $eod;
    }

    public function daily_sales($coy_id,$loc_id,$pos_id, $group='category') {
        $sql = "select CURRENT_DATE trans_date,loc_id,pos_id,RTRIM(inv_dim2) category, sum(qty_ordered-qty_refunded) total_qty, sum( (qty_ordered*unit_price)-(qty_refunded*unit_price)) total_amount
                from (
                    select l.order_num,l.order_type,l.loc_id,l.pos_id,l.customer_id,
                        i.item_id,i.item_desc,i.unit_price,i.regular_price,i.qty_ordered,i.qty_refunded,
                        p.inv_dim1,p.inv_dim2,p.inv_dim3,p.inv_dim4,p.brand_id,p.model_id
                    from o2o_order_list l
                    join o2o_order_item i on l.id=i.order_id
                    left join v_ims_live_product p on i.item_id=p.item_id
                    where l.order_status_level>=1 and l.pay_status_level>=2 and (i.status_level>=0 or i.status_level=-2)
                      and l.coy_id='$coy_id' and l.loc_id='$loc_id' and l.pos_id='$pos_id'
                      and l.pay_on > COALESCE((SELECT MAX(eff_to) FROM o2o_payment_eod WHERE coy_id='$coy_id' AND loc_id='$loc_id' AND pos_id='$pos_id' AND status_level>=1),'2019-08-01')
                ) ttb
                group by loc_id,pos_id,inv_dim2
                order by loc_id,pos_id,inv_dim2";
        $data = db()->select($sql);
        $grand_amount = 0;
        $grand_qty = 0;
        if (count($data)>0) {
            foreach ($data as $d) {
                $grand_qty += $d->total_qty;
                $grand_amount += $d->total_amount;
                $d->total_amount = 'S$ ' . number_format($d->total_amount, 2);
                $eod[] = $d;
            }
            $eod[] = (object)[
                "trans_date" => $data[0]->trans_date,
                "loc_id" => $loc_id,
                "pos_id" => $pos_id,
                "category" => 'TOTAL',
                "total_qty" => $grand_qty,
                "total_amount" => 'S$ ' . number_format($grand_amount, 2)
            ];
        }
        else {
            $eod[] = (object)[
                "trans_date" => date('Y-m-d'),
                "loc_id" => $loc_id,
                "pos_id" => $pos_id,
                "category" => 'TOTAL',
                "total_qty" => $grand_qty,
                "total_amount" => 'S$ ' . number_format($grand_amount, 2)
            ];
        }
        return $eod;
    }

    private function _offline_get($api_client,$query_key) {
        $sql = "select response_body from api_offline where api_client='".$api_client."' and query_key='".$query_key."' ";
        $result = db()->select($sql);
        return ($result) ? json_decode($result[0]->response_body) : NULL;
    }
    private function _offline_save($api_client,$query_key,$response_body){
        $sql = "select * from api_offline where api_client='".$api_client."' and query_key='".$query_key."' ";
        $result = db()->select($sql);
        if ($result) {
            $where = ['api_client' => $api_client, 'query_key' => $query_key];
            $insertParams['response_body'] = json_encode($response_body);
            $insertParams['updated_on'] = date('Y-m-d H:i:s');
            return db()->table('api_offline')->where($where)->update($insertParams);
        }
        else {
            $insertParams[] = [
                'api_client' => $api_client,
                'query_key' => $query_key,
                'response_body' => json_encode($response_body),
                'updated_on' => date('Y-m-d H:i:s')
            ];
            db()->table('api_offline')->insert($insertParams);
        }
    }

    private function _offline_jobs($queue,$payload){
        $insertParams[] = [
            'queue' => $queue,
            'payload' => $payload,
            'attemps' => 0,
            'reserved_at' => 0,
            'available_at' => 0,
            'created_at' => 0
        ];
        db()->table('api_offline')->insert($insertParams);
    }

    /*
     * Connection to CHERPS DB
     */

    public function eod_post_submittrans($data){

        if (app()->environment('offline')) {
            $this->_offline_jobs('EOD_POST', json_encode(['loc_id'=>$data[0]['loc_id'],'pos_id'=>$data[0]['pos_id'],'trans_date'=>$data[0]['trans_date']]) );
            return;
        }

        $coy_id = $data[0]['coy_id'];
        $loc_id = $data[0]['loc_id'];
        $pos_id = $data[0]['pos_id'];
        $trans_date = $data[0]['trans_date'];
        $sql = "SELECT CAST(trans_date as date),COUNT(DISTINCT shift_id) cnt 
                  FROM pos_eod_list 
                  WHERE coy_id='$coy_id' and loc_id='$loc_id' and pos_id='$pos_id' 
                    and cast(trans_date as date)=cast('$trans_date' as date) 
                  GROUP BY CAST(trans_date as date)";
        $eod = db('pg_cherps')->select($sql);
        if ($eod && $eod[0]->cnt>0) {
            $shift_id = $eod[0]->cnt;
            $data2 = [];
            foreach ($data as $d) {
                $d['shift_id'] = $shift_id;
                $data2[] = $d;
            }
            db('pg_cherps')->table('pos_eod_list')->insert($data2);
        }
        else {
            db('pg_cherps')->table('pos_eod_list')->insert($data);
        }
        return;
    }

    /*
    public function ereceipt($param){

        $email_addr = (isset($param['email_addr'])) ? $param['email_addr'] : 'cherps@challenger.sg';
        $trans_id = (isset($param['trans_id'])) ? $param['trans_id'] : '';

        if (app()->environment('offline')) {
            $this->_offline_jobs('ERECEIPT', json_encode(['email_addr'=>$email_addr,'trans_id'=>$trans_id]) );
            return;
        }

        $data = [
            "xtype" => 'e_receipt',
            "email_addr" => $email_addr,
            "invoice_id" => $trans_id,
            "loc_id" => (isset($param['loc_id'])) ? $param['loc_id'] : '',
            "mbr_id" => (isset($param['mbr_id'])) ? $param['mbr_id'] : '',
            "created_on" => date('Y-m-d'),
            "status_level" => 0,
            "resent_status" => 0
        ];
        if ($email_addr == "-") {
            $data['resent_status'] = 1;
            $data['resent_date'] = date('Y-m-d H:i:s');
        }
        db('sql')->table('o2o_email_resent_job')->insert($data);

        // Record into pos_transaction_list for stats
        $sql = "UPDATE pos_transaction_list SET remarks = coalesce(remarks,'[E]') WHERE rtrim(upper(trans_id))=rtrim(upper('".$trans_id."')) ";
        return  db('pg_cherps')->update($sql);
    }
    */

    public function staff_purchase($data) {
        $coy = strtoupper(trim($data['coy_id']));
        $usr_id = strtolower(trim($data['user']));

        $sql = "SELECT staff_id,staff_name,financial_year,purchase_limit,purchase_balance,last_purchased_on,joined_on,status_level, custom_data->'purchase_control' purchase_control
                    FROM staff_purchase_list
                    WHERE coy_id='$coy' and staff_id='$usr_id' and financial_year = date_part('year', CURRENT_DATE); ";
        $result = db()->select($sql);
        if ($result) {
            return $result[0];
        }
        else {

            $usr_name = $usr_id;
            $join_date = date('Y-m-d H:i:s');

            // Check with CHUGO for new staff purchase
            $client = new \GuzzleHttp\Client();
            $url = "https://chugo-api.challenger.sg/api/opsapp/staffValidation";
            $response = $client->post($url,
                [
                    'headers' => ['Authorization' => 'enQmbFA2byQlUl4kMnJSSF5GJHFuOUsmZHNHJkclOWg=', 'Content-Type' => 'application/json'],
                    'body'    => json_encode([
                        "coy_id" => "CTL",
                        "usr" => $usr_id,
                        "password" => md5($usr_id)
                    ]),
                ]
            );
            $content = $response->getBody()->getContents();
            $chugoData = json_decode($content,true);
            if (!empty($chugoData["data"])) {
                $chugo = $chugoData["data"];

                $usr_name = $chugo["staff_name"];
                $join_date = date('Y-m-d 00:00:00', strtotime($chugo["joined_on"]));
            }

            $purchase_limit = 1000;
            if (isset($result[0]) && $result[0]->service_year > 1 && $result[0]->service_year <= 3)
                $purchase_limit = 2000;
            else if (isset($result[0]) && $result[0]->service_year > 3)
                $purchase_limit = 3000;

            $purchase_controls = [
                0 => ["qty"=>3, "max_price"=>30],
                1 => ["qty"=>3, "min_price"=>30, "max_price"=>100],
                2 => ["qty"=>1, "min_price"=>100]
            ];

            $insert['coy_id'] = $coy;
            $insert['staff_id'] = $usr_id;
            $insert['staff_name'] = $usr_name;
            $insert['joined_on'] = $join_date;
            $insert['status_level'] = 1;

            $insert['financial_year'] = date('Y');
            $insert['purchase_limit'] = $purchase_limit;
            $insert['purchase_balance'] = $purchase_limit;
            $insert['custom_data'] = json_encode(["purchase_control" => $purchase_controls]);

            $insert['created_by'] = 'CHPOSS2';
            $insert['created_on'] = date('Y-m-d H:i:s');
            $insert['modified_by'] = 'CHPOSS2';
            $insert['modified_on'] = date('Y-m-d H:i:s');
            db()->table('staff_purchase_list')->insert($insert);

            return (object) [
                "staff_id" => $insert['staff_id'],
                "staff_name" => $insert['staff_name'],
                "financial_year" => $insert['financial_year'],
                "purchase_limit" => $insert['purchase_limit'],
                "purchase_balance" => $insert['purchase_balance'],
                "last_purchased_on" => null,
                "joined_on" => $insert['joined_on'],
                "status_level" => $insert['status_level'],
                "purchase_control" => json_encode($purchase_controls)
            ];
        }

    }

    public function usr_login_info_by_loc($coy_id,$loc_id){
        $loc_id = (substr($loc_id,0,3)=='TRS') ? 'UBI' : str_replace('-R','',$loc_id);
        $sql = "select c.coy_id, rtrim(s.usr_id) usr_id,s.usr_name, c.security_level, c.shift_id, c.loc_id, RTRIM(c.fin_dim3) fin_dim3,
                    (CASE WHEN c.fin_dim3 LIKE '%PART%' THEN 'PT' ELSE (CASE WHEN c.security_level>=2 THEN 'IC' ELSE 'FT' END) END) staff_level,
                    sec_pass
                  from sys_usr_list s, coy_usr_list c
                  where UPPER(c.coy_id)=UPPER('$coy_id') and c.usr_id=s.usr_id and UPPER(c.loc_id)=UPPER('$loc_id')
                    and s.revoked_date < '2000-01-01'
                    and s.usr_id in (select usr_id from sss_grp_usr_list where left(grp_id,7)='CASHIER')";
        $result = db('pg_cherps')->select($sql);
        if ($result) {
            return $result;
        }
        else {
            return NULL;
        }
    }

    public function usr_login_info($data) {
        $coy = $data['coy_id'];
        $usr_id = $data['user'];

        $sql = "select c.coy_id, rtrim(s.usr_id) usr_id,rtrim(c.staff_id) staff_id, s.usr_name staff_name, c.security_level, c.shift_id, c.loc_id, RTRIM(c.fin_dim3) fin_dim3,
                    (CASE WHEN c.security_level>=2 THEN 'IC' ELSE 'FT' END) staff_level,
                    sec_pass
				  from sys_usr_list s, coy_usr_list c
                  where trim(upper(c.coy_id))=trim(upper('$coy')) and LEFT(UPPER(fin_dim3),4)<>'PART'
                    and c.usr_id=s.usr_id and ( trim(upper(s.usr_id))=trim(upper('$usr_id')) or trim(upper(c.staff_id))=trim(upper('$usr_id')) )
                  LIMIT 1; ";
        $result = db('pg_cherps')->select($sql);
        if ($result) {
            return $result[0];
        }
        else {
            return NULL;
        }
    }

    public function usr_login_rights($coy_id,$usr_id) {

        if (app()->environment('offline')) {
            return NULL;
        }

        $sql = "select rtrim(grp_id) grp_id from sss_grp_usr_list 
                    where rtrim(upper(coy_id))=rtrim(upper('$coy_id')) and rtrim(upper(usr_id))=rtrim(upper('$usr_id')) ; ";
        $result = db('pg_cherps')->select($sql);
        if ($result){
            $resp = [];
            foreach($result as $res)
                $resp[] = $res->grp_id;
            return $resp;
        }
        else {
            return NULL;
        }
    }

    public function usr_login_location($coy_id,$usr_id) {

        if (app()->environment('offline')) {
            return NULL;
        }

        $sql = "select rtrim(loc_id) loc_id from coy_usr_list
                    where rtrim(upper(coy_id))=rtrim(upper('$coy_id')) and rtrim(upper(usr_id))=rtrim(upper('$usr_id')) ; ";
        $result = db('pg_cherps')->select($sql);
        if ($result){
            $resp = [];
            foreach($result as $res)
                $resp[] = $res->loc_id;
            return $resp;
        }
        else {
            return NULL;
        }
    }
    
    public function func_getpos_setting($key){
        $sql = "SELECT CASE WHEN COALESCE(MAX(set_value),'')='' THEN '[]' ELSE MAX(set_value) END as values  FROM setting WHERE set_key = ?";
        $result = db()->select($sql,[$key])[0];
        return $result;
    }

}