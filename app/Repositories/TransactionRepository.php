<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 10/9/2018
 * Time: 5:54 PM
 */

namespace App\Repositories;

class TransactionRepository{

    public function eod_payments_mapping()
    {
        $sql = "select func_type,seq_no, func_dim1, func_dim2, trim(func_tag) pay_mode, trim(func_id) pay_type, func_name pay_desc
                from pos_function_list
                where func_type in ('PAYMENT','PAYMENT1')
                order by func_type desc,seq_no asc";
        $generated = db()->select($sql);
        return $generated;
    }

    // updateReceiptLot
    public function updPg_item_lot($trans_id,$line_num,$lot_ids,$upd=0,$modifed_by='POSAPI') {

        $sql = "SELECT id,custom_data 
                    FROM o2o_order_item 
                    WHERE order_id IN (select id from o2o_order_list where order_num='$trans_id' ) 
                    AND line_num=$line_num";
        $result = db()->select($sql);

        if ($result) {
            $custom_data = json_decode($result[0]->custom_data, true);
            if ($upd==1){
                // Update existing
                $custom_data['lot_id'] = (array) $lot_ids ;
            }
            else {
                // Insert new lot
                $custom_data['lot_id'] = (count($custom_data['lot_id']) >= 1) ? array_merge($custom_data['lot_id'],$lot_ids) : (array) $lot_ids ;
            }

            $where = ['id' => $result[0]->id];
            $insertParams['custom_data'] = json_encode($custom_data);
            $insertParams['modified_by'] = $modifed_by;
            $insertParams['modified_on'] = date('Y-m-d H:i:s');
            return db()->table('o2o_order_item')->where($where)->update($insertParams);
        }
        else {
            return NULL;
        }
    }

    // updateReceiptSalesperson
    public function updPg_item_salesperson($trans_id,$line_num,$salesperson,$upd=0,$modifed_by='POSAPI') {

        $sql = "SELECT id,custom_data 
                    FROM o2o_order_item 
                    WHERE order_id IN (select id from o2o_order_list where order_num='$trans_id' ) 
                    AND line_num=$line_num";
        $result = db()->select($sql);

        if ($result) {
            $custom_data = json_decode($result[0]->custom_data, true);
            if ($upd==1){
                // Update existing
                $custom_data['salesperson_id'] = (array) $salesperson ;
            }
            else {
                // Insert new lot
                $custom_data['salesperson_id'] = (count($custom_data['salesperson_id']) >= 1) ? array_merge($custom_data['salesperson_id'],$salesperson) : (array) $salesperson ;
            }

            $where = ['id' => $result[0]->id];
            $insertParams['custom_data'] = json_encode($custom_data);
            $insertParams['modified_by'] = $modifed_by;
            $insertParams['modified_on'] = date('Y-m-d H:i:s');
            return db()->table('o2o_order_item')->where($where)->update($insertParams);
        }
        else {
            return NULL;
        }
    }

    // updateKrisflyer
    public function updCh_list_remarks($trans_id,$ref_tag,$ref_id){

        $sql = "SELECT id,custom_data 
                    FROM o2o_order_list
                    WHERE order_num='$trans_id'";
        $result = db()->select($sql);

        if ($result) {
            $custom_data = json_decode($result[0]->custom_data, true);
            $custom_data[$ref_tag] = $ref_id;

            $where = ['id' => $result[0]->id];
            $insertParams['custom_data'] = json_encode($custom_data);
            $insertParams['modified_by'] = 'CHPOS20';
            $insertParams['modified_on'] = date('Y-m-d H:i:s');
            db()->table('o2o_order_list')->where($where)->update($insertParams);

            $sql = "UPDATE pos_transaction_list 
                        SET remarks = concat(remarks,' [".$ref_tag.": ".$ref_id."]') 
                        WHERE trans_id='".$trans_id."' ";
            return  db('pg_cherps')->update($sql);
        }
        else {
            return NULL;
        }
    }

    // getOneReceipt
    public function getPg_order($trans_id){
        $sql = "SELECT id order_id,order_num,order_type, order_status_level, pay_status_level,pay_on 
                    FROM o2o_order_list WHERE order_num='$trans_id' AND order_status_level=1";
        $result = db()->select($sql);
        if ($result)
            return $result[0];
        else
            return NULL;
    }

    public function getCh_order_full($coy_id,$trans_id){
        $sql = "select
                    l.coy_id,null cart_id,RTRIM(l.trans_id) order_num,l.trans_type order_type,null invoice_id,
                    rtrim(l.ref_id) ref_id,RTRIM(l.loc_id) loc_id,RTRIM(l.pos_id) pos_id,l.cust_id customer_id,l.cust_name customer_name,email_addr customer_email,
                    0 points_earned, 0 points_used, 0 item_total, 0 shipping_total, 0 discount_total, 0 taxable_amount, 0 tax_amount, 0 order_total, 0 rounding_adj,
                    0 deposit_amount, 0 rebate_amount, 0 voucher_adj, 0 total_due, 0 total_paid, 0 total_refunded, 0 total_savings,
                    l.trans_date::varchar pay_on, l.trans_date::varchar placed_on,
                    RTRIM(l.cashier_id) cashier_id, 'true' is_posted, 1 order_status_level, 2 pay_status_level, '{}' custom_data,
                    rtrim(created_by) created_by, created_on::varchar created_on,
                    rtrim(modified_by) modified_by, modified_on::varchar modified_on
                from pos_transaction_list l
                where rtrim(upper(coy_id))=rtrim(upper('$coy_id')) and rtrim(upper(trans_id))=rtrim(upper('$trans_id'))
                    and status_level>=0";
        $result['order_list'] = db('pg_cherps')->select($sql);

        if ($result['order_list']) {

            $sql = "select 0 order_id, line_num, 0 ref_line_num, rtrim(item_id) item_id, 'I' item_type, rtrim(item_desc) item_desc,
                       regular_price, unit_price, trans_qty::integer qty_ordered, 0::integer qty_invoiced, csd_qty::integer qty_refunded,
                       0 unit_points_earned, 0 status_level, '' custom_data,
                       rtrim(created_by) created_by,created_on::varchar created_on,
                       rtrim(modified_by) modified_by,modified_on::varchar modified_on,
                       promo_id promo_id
                from pos_transaction_item
                where rtrim(upper(coy_id))=rtrim(upper('$coy_id')) and rtrim(upper(trans_id))=rtrim(upper('$trans_id'))
                    and rtrim(bom_ind)<>'BAG' and status_level>=0";
            $result['order_item'] = db('pg_cherps')->select($sql);

            $sql = "select rtrim(trans_id) trans_id,line_num,rtrim(lot_id) lot_id
                from pos_transaction_lot
                where rtrim(upper(coy_id))=rtrim(upper('$coy_id')) and rtrim(upper(trans_id))=rtrim(upper('$trans_id')); ";
            $result['order_lot'] = db('pg_cherps')->select($sql);

            $sql = "select 0 order_id, rtrim(trans_id) trans_id, '' trans_ref_id,
                       rtrim(pay_mode) pay_mode, rtrim(fin_dim2) pay_type, '' card_num, rtrim(trans_ref3) approval_code, trans_amount, null amount_refunded,
                       created_on::varchar trans_date, 'true' is_success, '{}' custom_data,
                       rtrim(created_by) created_by, created_on::varchar created_on
                from pos_payment_list
                where rtrim(upper(coy_id))=rtrim(upper('$coy_id')) and rtrim(upper(trans_id))=rtrim(upper('$trans_id')) 
                    and status_level>=0";
            $result['order_payment'] = db('pg_cherps')->select($sql);

            return $result;

        }
        else {
            return NULL;
        }
    }

    public function updPg_order($tableName,$insertParams){
        $insertParams = (array) $insertParams;
        $id = db()->table($tableName)->insertGetId($insertParams);
        return $id;
    }

    public function getMs_to_Ch($id){

        $log = ["list"=>0, "item"=>0, "lot"=>0, "pay"=>0];

        // Copy insert - list
        $sql = "SELECT * from pos_trans_list_history where trans_id='$id' ";
        $list = (array)db('ms_cherps')->select($sql);
        $result = array_map(function ($value) {
            $value->trans_date = date('Y-m-d H:i:s', strtotime($value->trans_date));
            $value->created_on = date('Y-m-d H:i:s', strtotime($value->created_on));
            $value->modified_on = date('Y-m-d H:i:s', strtotime($value->modified_on));
            return (array)$value;
        }, $list);
        db('pg_cherps')->table('pos_transaction_list')->insert($result);
        $log['list'] = count($result);

        // Copy insert - item
        $sql = "SELECT * from pos_trans_item_history where trans_id='$id' ";
        $list = (array)db('ms_cherps')->select($sql);
        $result = array_map(function ($value) {
            $value->created_on = date('Y-m-d H:i:s', strtotime($value->created_on));
            $value->modified_on = date('Y-m-d H:i:s', strtotime($value->modified_on));
            return (array)$value;
        }, $list);
        db('pg_cherps')->table('pos_transaction_item')->insert($result);
        $log['item'] = count($result);

        // Copy insert - lot
        $sql = "SELECT * from pos_trans_lot_history where trans_id='$id' ";
        $list = (array)db('ms_cherps')->select($sql);
        $result = array_map(function ($value) {
            $value->created_on = date('Y-m-d H:i:s', strtotime($value->created_on));
            $value->modified_on = date('Y-m-d H:i:s', strtotime($value->modified_on));
            $value->datetime_udf1 = date('Y-m-d H:i:s', strtotime($value->datetime_udf1));
            $value->datetime_udf2 = date('Y-m-d H:i:s', strtotime($value->datetime_udf2));
            $value->datetime_udf3 = date('Y-m-d H:i:s', strtotime($value->datetime_udf3));
            return (array)$value;
        }, $list);
        db('pg_cherps')->table('pos_transaction_lot')->insert($result);
        $log['lot'] = count($result);

        // Copy insert - pay
        $sql = "SELECT * from pos_pay_list_history where trans_id='$id' ";
        $list = (array)db('ms_cherps')->select($sql);
        $result = array_map(function ($value) {
            $value->created_on = date('Y-m-d H:i:s', strtotime($value->created_on));
            $value->modified_on = date('Y-m-d H:i:s', strtotime($value->modified_on));
            return (array)$value;
        }, $list);
        db('pg_cherps')->table('pos_payment_list')->insert($result);
        $log['pay'] = count($result);

        return $log;
    }

    // getSmsInvoice
    public function getCh_invoice_full($coy_id,$loc_id,$invoice_id=''){
        $sqlwhere = ($invoice_id=='') ? " and loc_id='$loc_id' " : " and invoice_id='$invoice_id' ";
//        $sql = "select RTRIM(l.loc_id) loc_id,'' pos_id, RTRIM(l.invoice_id) invoice_id, RTRIM(l.inv_type) inv_type, l.invoice_date,
//                    (CASE when i.item_id='\$DEPOSIT' and i.status_level=3 THEN
//                            (SELECT TOP 1 rtrim(trans_id) from pos_transaction_list where coy_id=l.coy_id and trans_type='DI' and invoice_id=l.invoice_id ORDER BY trans_date DESC)
//                        ELSE '' END) deposit_ref_id,
//                    CASE WHEN LEN(l.cust_id)<7 THEN null ELSE RTRIM(l.cust_id) END mbr_id, RTRIM(l.salesperson_id) salesperson_id,
//                    0 item_count, 0 sub_total,
//                      RTRIM(i.item_id) item_id, i.item_desc, i.status_level, i.qty_invoiced item_qty, i.unit_price,
//                      coalesce(RTRIM(s.lot_id),'') lot_id, coalesce(RTRIM(s.lot_id2),'') lot_id2,
//                      coalesce(s.string_udf1,'') string_udf1, coalesce(s.string_udf2,'') string_udf2, coalesce(s.string_udf3,'') string_udf3
//                from sms_invoice_list l
//                join sms_invoice_item i on l.coy_id=i.coy_id and l.invoice_id=i.invoice_id
//                left join sms_invoice_lot s on i.coy_id=s.coy_id and i.invoice_id=s.invoice_id and i.line_num=s.line_num
//                where l.coy_id='$coy_id' and i.status_level>=0
//                    and l.invoice_id in (select TOP 50 invoice_id from sms_invoice_list where inv_type in ('GS','ID','IS') and status_level=1 $sqlwhere)
//                order by invoice_date desc, invoice_id, i.line_num;";
//        $result = db('pg_cherps')->select($sql);

        $sqlwhere = ($invoice_id=='') ? " and loc_id='$loc_id' " : " and invoice_id='$invoice_id' ";
        $sql = "select RTRIM(l.loc_id) loc_id,'' pos_id, RTRIM(l.invoice_id) invoice_id, RTRIM(l.inv_type) inv_type, l.invoice_date,
                    (CASE when i.item_id='\$DEPOSIT' and i.status_level=3 THEN
                            (SELECT rtrim(trans_id) from pos_transaction_list where coy_id=l.coy_id and trans_type='DI' and invoice_id=l.invoice_id ORDER BY trans_date DESC LIMIT 1)
                        ELSE '' END) deposit_ref_id,
                    CASE WHEN LENGTH(l.cust_id)<7 THEN null ELSE RTRIM(l.cust_id) END mbr_id, RTRIM(l.salesperson_id) salesperson_id,
                    0 item_count, 0 sub_total,
                      RTRIM(i.item_id) item_id, i.item_desc, i.status_level, i.qty_invoiced item_qty, i.unit_price, i.disc_amount, i.disc_percent,
                      coalesce(RTRIM(s.lot_id),'') lot_id, coalesce(RTRIM(s.lot_id2),'') lot_id2,
                      coalesce(s.string_udf1,'') string_udf1, coalesce(s.string_udf2,'') string_udf2, coalesce(s.string_udf3,'') string_udf3
                from sms_invoice_list l
                join sms_invoice_item i on l.coy_id=i.coy_id and l.invoice_id=i.invoice_id
                left join sms_invoice_lot s on i.coy_id=s.coy_id and i.invoice_id=s.invoice_id and i.line_num=s.line_num
                where l.coy_id='$coy_id' and i.status_level>=0
                    and l.invoice_id in (select invoice_id from sms_invoice_list where inv_type in ('GS','ID','IS') and status_level=1 $sqlwhere LIMIT 50 )
                order by invoice_date desc, invoice_id, i.line_num;";
        $result = db('pg_cherps')->select($sql);

        return ($result) ? $result : NULL;
    }

    public function getPg_order_dm($coy_id,$loc_id){
        $sql = "select l.loc_id,l.pos_id, l.order_num,l.order_type, MAX(l.created_on) created_on, COALESCE(MAX(l.customer_id),'') mbr_id, MAX(l.order_total) order_total
                    from o2o_order_list l
                    join o2o_order_item i on l.id=i.order_id and i.item_type='P'
                    where l.order_type in ('DM','DI') and (l.ref_id='' or l.ref_id is null) and l.order_status_level>=1 and l.pay_status_level>=2
                         and l.coy_id='$coy_id' and l.loc_id='$loc_id'
                         and (i.qty_ordered-i.qty_refunded)>0
                    group by l.loc_id,l.pos_id, l.order_num,l.order_type
                    order by created_on desc;";
        $result = db('')->select($sql);
        return ($result) ? $result : NULL;
    }

    public function getPg_order_incall($sr_id){
        $sqlitem = strtoupper('INCALL%'.$sr_id);
        $sql = "select l.order_num,l.pay_on order_date,l.loc_id,l.pos_id,l.customer_id,
                    i.item_id,i.unit_price, i.custom_data->'lot_id'->>0 sr_id
                from o2o_order_item i
                join o2o_order_list l on i.order_id=l.id
                where i.item_id='#INCALL-SR' --and upper(i.item_desc) like '$sqlitem'
                    and i.custom_data->'lot_id'->>0 = '$sr_id'
                and l.order_status_level>=1 and l.pay_status_level>=2";
        $result = db()->select($sql);
        if ($result)
            return $result;
        else
            return NULL;
    }


    public function getOrderItem($id){
        return db()->table('o2o_order_item')->where('id',$id)->first();
    }

    public function saveOrderItemCustomData($id, $custom_data_arr) {
        return db()->table('o2o_order_item')->where('id',$id)->update(['custom_data' => json_encode($custom_data_arr)]);
    }

}