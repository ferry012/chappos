<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 23/7/2018
 * Time: 11:34 AM
 */

namespace App\Repositories;

use Carbon\Carbon;


class MemberRepository
{
    public function create($data)
    {
        return db('sql')->table('crm_member_list')->insert($data);
    }

    public function getOne($mbrId)
    {
        $member = db('sql')->table('crm_member_list')->where([
            'coy_id' => 'CTL',
            'mbr_id' => $mbrId,
        ])->first();

        $member->mbr_id = trim($member->mbr_id);

        if (Carbon::make($member->birth_date)->year === '1900') {
            $member->birth_date = 'N/A';
        }

        return $member;
    }

    public function getAssociatedMembers($mainMbrId)
    {
        return $this->query()->where('main_id', $mainMbrId)->get();
    }

    public function updateMember($mbrId, array $data)
    {
        return db('sql')->table('crm_member_list')->where(['coy_id' => 'CTL', 'mbr_id' => $mbrId])->update($data);
    }

    public function getReferralCode($mbrId)
    {
        return db('sql')->table('o2o_member_list_options')->where('mbr_id', $mbrId)->first()->referral_code;
    }

    public function isMemberExists($mbrId)
    {
        $member = $this->getOne($mbrId);

        return null !== $member;
    }

    public function isEmailExists($email)
    {
        return $this->query()->where('email_addr', $email)->exists();
    }

    public function query()
    {
        return db('sql')->table('crm_member_list')->where('coy_id', 'CTL');
    }
}