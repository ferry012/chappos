<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 23/7/2018
 * Time: 11:22 AM
 */

namespace App\Repositories;


class InventoryRepository
{

    public function __construct(ProductRepository $productRepo)
    {
        $this->productRepo = $productRepo;
    }

    public function isExemptedItem($itemId)
    {
        if (db('sql')->table('o2o_live_inv_exempt')->where('item_id', $itemId)->exists()) {
            return true;
        }

        $product = $this->productRepo->getOne($itemId);

        return $product && ! in_array($product->item_type, ['I', 'B'], false);
    }

    public function getItemBalanceQtyList($itemId, $from = '')
    {

        // default balance from designated location
        if (empty($from)) {
            $from = db('sql')->table('o2o_live_outlet')->where('INV', 1)->get()->pluck('loc_id')->toArray();
        }

        if(!is_array($from)){
            $from = (array)$from;
        }

        $results = db('sql')->table('ims_inv_physical')->where('coy_id', 'CTL')
            ->where('item_id', $itemId)
            ->whereIn('loc_id', $from)
            ->selectRaw('loc_id, item_id, sum(qty_on_hand - qty_reserved) AS balance')
            ->groupBy('loc_id', 'item_id')
            ->havingRaw('sum(qty_on_hand - qty_reserved) > 0')
            ->get();

        $results->each(function ($item) {
            $item->loc_id  = trim($item->loc_id);
            $item->item_id = trim($item->item_id);
            $item->balance = (int)$item->balance;

            return $item;
        });

        return $results;
    }

    public function getItemTotalBalanceQty($itemId, $from = '', $deductBufferQty = false)
    {

        if ($this->isExemptedItem($itemId)) {
            return 999;
        }

        if(is_string($from)){
            $from = (array)$from;
        }

        // default balance from designated location
        if (empty($from)) {
            $from = db('sql')->table('o2o_live_outlet')->where('INV', 1)->get()->pluck('loc_id');
        }

        $balance = db('sql')->table('ims_inv_physical')->where('coy_id', 'CTL')
            ->where('item_id', $itemId)
            ->whereIn('loc_id', $from)
            ->selectRaw('sum(qty_on_hand - qty_reserved) AS balance')
            ->first()->balance;

        if ($deductBufferQty) {
            $balance -= $this->getItemBufferQty($itemId);
        }

        $balance = (int)$balance;

        return $balance > 0 ? $balance : 0;
    }

    public function getItemBufferQty($itemId)
    {
        $qty = db()->table('o2o_live_product')->where('item_id', $itemId)->first()->buffer_qty;

        return $qty ?? 0;
    }
}