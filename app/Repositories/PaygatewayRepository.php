<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 10/9/2018
 * Time: 5:54 PM
 */

namespace App\Repositories;

class PaygatewayRepository{

    public function api_log($data){
        db()->table('api_log')->insert($data);
    }

    public function func_data($func_id,$coy_id,$loc_id,$pos_id=''){
        $sql = "SELECT loc_id,pos_id,udf_data1,udf_data2,udf_data3
                    FROM pos_location_func
                    WHERE status_level>=0 AND func_id='$func_id' AND coy_id='$coy_id' AND loc_id='$loc_id' AND pos_id='$pos_id'";
        $result = db()->select($sql);
        return ($result) ? $result[0] : NULL;
    }

    public function func_list($func_id){
        $sql = "SELECT func_id,func_dim1,func_dim2,func_dim3,func_dim4,func_dim5
                    FROM pos_function_list
                    WHERE func_id='$func_id' and status_level>=0 ";
        $result = db()->select($sql);
        return ($result) ? $result[0] : NULL;
    }

    /*
     * CHERPS DB FX (SMS_VOUCHER_LIST)
     */

    public function utilize_crm($voucher_ids,$trans_id) {
        $sql = "UPDATE crm_voucher_list SET receipt_id2='$trans_id', utilize_date=getdate(), status_level=1,
                    modified_by='$trans_id', modified_on=getdate()
                    WHERE coupon_serialno in ('" . implode("','", $voucher_ids) . "') ";
        return db('sql')->update($sql);
    }

    public function utilize_sms($voucher_ids,$trans_id) {
        $sql_datetime = date('Y-m-d H:i:s');
        $sql = "UPDATE sms_voucher_list SET receipt_id2='$trans_id', utilize_date='$sql_datetime', status_level=3,
                    modified_by='$trans_id', modified_on='$sql_datetime'
                    WHERE voucher_id in ('" . implode("','", $voucher_ids) . "') ";
        return db('pg_cherps')->update($sql);
    }

}