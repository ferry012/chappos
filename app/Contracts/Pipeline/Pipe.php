<?php

namespace App\Contracts\Pipeline;

use Closure;

interface Pipe
{
    public function handle($content, Closure $next);
}