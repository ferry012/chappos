<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Scope;

class ChallengerScope implements Scope
{
    public function apply(Builder $builder, Model $model)
    {
        $builder->where('coy_id', '=', 'CTL');
    }
}