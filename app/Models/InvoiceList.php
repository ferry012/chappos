<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceList extends Model
{
    protected $connection = 'pg_cherps';
    protected $table = 'sms_invoice_list';

}
