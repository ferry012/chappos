<?php

namespace App\Models;


use App\Core\Scaffold\BaseModel;
use App\Models\Concerns\HasCompositePrimaryKey;

class CherpsInvoice extends BaseModel
{
    use HasCompositePrimaryKey;

    protected $connection = 'sql';

    protected $table = 'sms_invoice_list';

    protected $primaryKey = ['coy_id', 'invoice_id'];
}