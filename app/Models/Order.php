<?php

namespace App\Models;


use App\Models\Concerns\HasCustomData;

class Order extends Model
{
    use HasCustomData;

    protected $connection = 'pg';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'o2o_order_list';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be visible in serialization.
     *
     * @var array
     */
    protected $visible = [];

    public function items()
    {
        return $this->hasMany(OrderItem::class, 'order_id', 'id');
    }

    public function stockItems()
    {
        return $this->hasMany(OrderStock::class, 'order_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany(OrderPayment::class, 'order_id', 'id');
    }

    public function getTaxAmountAttribute()
    {
        return round($this->getAttributeFromArray('tax_amount'), 2);
    }

    public function getTotalSavingAmountAttribute()
    {
        return round($this->getAttributeFromArray('total_saving_amount'), 2);
    }

    public function isMember()
    {
        return ! empty($this->customer_id);
    }
}