<?php

namespace App\Models;

use App\Core\Scaffold\BaseModel;

class AddressBook extends BaseModel
{

    protected $table = 'hsg_address_book';

    protected $fillable = [
        'address_type',
        'customer_id',
        'recipient_name',
        'company_name',
        'address_line_1',
        'address_line_2',
        'country_id',
        'city_name',
        'postal_code',
        'phone_num',
    ];

    public function scopeDefault($query)
    {
        return $query->where('is_default', true);
    }

    public function scopeCorpBilling($query)
    {
        return $query->where('address_type','CORP_BILLING');
    }

    public function scopeBilling($query)
    {
        return $query->where('address_type','BILLING');
    }

    public function scopeShipping($query)
    {
        return $query->where('address_type','SHIPPING');
    }
}