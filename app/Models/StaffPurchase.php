<?php

namespace App\Models;


use App\Core\Scaffold\BaseModel;
use App\Core\Traits\HasCustomData;
use App\Models\Concerns\HasCompositePrimaryKey;

class StaffPurchase extends BaseModel
{
    use HasCustomData;
    use HasCompositePrimaryKey;

    protected $table = 'staff_purchase_list';

    protected $primaryKey = ['coy_id', 'staff_id', 'financial_year'];
}