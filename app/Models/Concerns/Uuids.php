<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 19/8/2017
 * Time: 10:12 PM
 */

namespace App\Models\Concerns;


use Webpatser\Uuid\Uuid;

trait Uuids
{
    /**
     * Boot function from laravel.
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::generate(1);
        });
    }
}