<?php

namespace App\Models\Concerns;


use Illuminate\Support\Arr;

trait HasCustomData
{
    protected $customField = 'custom_data';

    public function getCustomData($key, $default = null)
    {
        $data = $this->getAttribute($this->customField);

        if ($data) {
            return Arr::get($data, $key, $default);
        }

        return $default;
    }

    public function setCustomData($key, $value = null)
    {
        $data = $this->getAttribute($this->customField);

        if (is_null($data)) {
            $data = [];
        }

        Arr::set($data, $key, $value);

        $this->setAttribute($this->customField, $data);
    }

    public function unsetCustomData($key)
    {
        $data = $this->getAttribute($this->customField);

        if (is_null($data)) {
            return;
        }

        Arr::forget($data, $key);

        $this->setAttribute($this->customField, $data);

    }

    public function hasCustomData($key)
    {
        $data = $this->getAttribute($this->customField);

        if (is_null($data)) {
            return false;
        }

        return Arr::has($data, $key);
    }

    public function isCustomDataCastable()
    {
        return $this->hasCast($this->customField, ['array']);
    }
}