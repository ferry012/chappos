<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberTransactionHistory extends Model
{
    protected $table = 'v_crm_member_transaction';
}
