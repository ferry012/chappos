<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    const CREATED_AT = 'created_on';
    const UPDATED_AT = 'modified_on';

    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * Convert a DateTime to a storable string.
     * SQL Server will not accept 6 digit second fragment (PHP default: see getDateFormat Y-m-d H:i:s.u)
     * trim three digits off the value returned from the parent.
     *
     * @param  \DateTime|int $value
     *
     * @return string
     */
    public function fromDateTime($value)
    {
        $datetime = parent::fromDateTime($value);

        if (windows_os()) {
            $datetime = substr($datetime, 0, -3);
        }

        return $datetime;
    }

    /**
     * Get the format for database stored dates.
     *
     * @return string
     */
    public function getDateFormat()
    {
        if (windows_os()) {
            $this->dateFormat = 'Y-m-d H:i:s.u';
        }

        return $this->dateFormat ?: $this->getConnection()->getQueryGrammar()->getDateFormat();
    }

    public function setModifiedByAttribute($value)
    {
        $this->attributes['modified_by'] = substr($value, 0, 15);
    }
}