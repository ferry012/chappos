<?php

namespace App\Models;

use App\Models\Concerns\HasCustomData;
use App\Support\Hashids;

class CartItem extends Model
{
    use HasCustomData;

    protected $connection = 'pg';

    protected $table = 'o2o_cart_item';

    protected $fillable = [
        'item_medium', 'item_type', 'inv_type', 'item_id', 'item_desc', 'item_img', 'is_pwp_item', 'has_pwp_item',
        'pwp_group_id', 'parent_id', 'item_qty', 'regular_price', 'unit_price',
        'delv_method', 'loc_id', 'custom_data',
    ];

    protected $appends = ['unit_savings_amount', 'total_savings_amount', 'total_points_earned'];

    protected $hidden = [
        'cart_id', 'custom_data',
    ];

    protected $casts = [
        'item_qty'         => 'int',
        'has_pwp_item'     => 'boolean',
        'is_pwp_item'      => 'boolean',
        'is_editable'      => 'boolean',
        'is_deletable'     => 'boolean',
        'is_free_item'     => 'boolean',
        'is_free_shipping' => 'boolean',
        'regular_price'    => 'float',
        'member_price'     => 'float',
        'promo_price'      => 'float',
        'unit_price'       => 'float',
        'custom_data'      => 'array',
    ];

    public function cart()
    {
        return $this->belongsTo(Cart::class, 'cart_id', 'id');
    }

    /*
    * Get Item orginal quantity before update
    *
    * @return integer
    */
    public function getOriginalQuantity()
    {
        return $this->original['quantity'];
    }

    /*
    * Get Item original price before update
    *
    * @return float
    */
    public function getOriginalUnitPrice()
    {
        return $this->original['unit_price'];
    }

    /*
    * Get Item price
    *
    * @return float
    */
    public function getPrice()
    {
        return $this->quantity * $this->unit_price;
    }

    /*
    * Get Item original price before update
    *
    * @return integer
    */
    public function getOriginalPrice()
    {
        return $this->getOriginalQuantity() * $this->getOriginalUnitPrice();
    }

    /*
    * Get the singleton cart of this line item.
    *
    */
    public function getCartInstance()
    {
        $carts = app('cart_instances');
        foreach ($carts as $name => $cart) {
            if ($cart->id === $this->cart_id) {
                return $cart;
            }
        }

        return null;
    }

    /*
    * Move this item to another cart
    *
    * @param Cart $cart
    */
    public function moveTo(Cart $cart)
    {
        $this->delete(); // delete from own cart

        return $cart->items()->create($this->attributes);
    }

    public function getEncodeIdAttribute()
    {
        return Hashids::encode($this->attributes['id']);
    }

    public function getItemIdAttribute()
    {
        return trim($this->attributes['item_id']);
    }

    public function getStoreCollectionNameAttribute()
    {
        return trim($this->attributes['store_collection']);
    }

    public function getDeliveryMethodAttribute()
    {
        return trim($this->attributes['delivery_method']);
    }

    public function getAvailableDeliveryMethodAttribute()
    {
        return trim($this->attributes['available_delivery_method']);
    }

    public function getUnitSavingsAmountAttribute()
    {
        $price = $this->attributes['regular_price'] - $this->attributes['unit_price'];

        return (0 < $price) ? $price : 0;
    }

    public function getTotalSavingsAmountAttribute()
    {
        return $this->getAttribute('unit_savings_amount') * $this->attributes['item_qty'];
    }

    public function getUnitPointsEarnedAttribute()
    {
        $points = $this->attributes['unit_price'];//* $this->attributes['unit_point_multiplier'];

        return floor($points);
    }

    public function getTotalPointsEarnedAttribute()
    {
        $total = $this->getAttribute('unit_points_earned') * $this->attributes['item_qty'];

        return $total;
    }
}