<?php

namespace App\Models;

class PaymentGateway extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     * @return Builder
     */
    protected $table = 'hsg_pay_gateway';

    protected $keyType = 'str';

    public $incrementing = false;

    public function scopeActive($builder)
    {
        return $builder->where('status_level', 1);
    }
}