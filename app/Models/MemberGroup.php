<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 16/2/2021
 * Time: 9:49 AM
 */

namespace App\Models;


class MemberGroup extends Model
{
    protected $table = 'hsg_member_group';

    protected $casts = [
        'mbr_type' => 'json',
    ];

}