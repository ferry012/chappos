<?php

namespace App\Models;


use App\Models\Concerns\HasCustomData;

class OrderItem extends Model
{
    use HasCustomData;

    protected $connection = 'pg';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'o2o_order_item';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be visible in serialization.
     *
     * @var array
     */
    protected $visible = [];

    public function order()
    {
        return $this->belongsTo(Order::class, 'id', 'order_id');
    }

    public function stocks()
    {
        return $this->hasMany(OrderStock::class, 'order_item_id', 'id');
    }

}