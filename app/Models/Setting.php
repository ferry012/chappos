<?php

namespace App\Models;


class Setting extends \Illuminate\Database\Eloquent\Model
{
    protected $connection = 'pg';
    protected $table      = 'setting';

    public function findByGroup($group)
    {
        return $this->where('set_group', $group)->get();
    }

    public function findByKey($key)
    {

    }

    public function findByGroupAndKey($group, $key)
    {
        return $this->where('set_group', $group)->where('set_key', $key)->first();
    }
}