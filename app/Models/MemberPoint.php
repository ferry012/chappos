<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Model;

class MemberPoint extends Model
{
    protected $primaryKey = 'rowguid';

    protected $appends = ['points_balance'];

    public function getPointsBalanceAttribute()
    {
        $points = $this->getAttribute('points_accumulated') - $this->getAttribute('points_redeemed') - $this->getAttribute('points_expired');

        return $points;
    }
}
