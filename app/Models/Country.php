<?php

namespace App\Models;

class Country extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hsg_country';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The "type" of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'str';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public $timestamps = false;

    public static function findByISO($code)
    {
        return static::where('id', $code)->first();
    }

    public static function findByISO3($code)
    {
        return static::where('iso3', $code)->first();
    }

    public static function findByPhoneCode($code)
    {
        return static::where('phone_code', $code)->first();
    }

}