<?php

namespace App\Models;


use Carbon\Carbon;
use App\Models\Concerns\HasCustomData;
use App\Support\Hashids;


class Cart extends Model
{

    use HasCustomData;

    const UPDATED_AT = null;

    protected $connection = 'pg';

    protected $table = 'o2o_cart_list';

    protected $fillable = ['cart_label', 'mbr_id', 'session_id', 'custom_data',];

    protected $hidden = ['custom_data', 'salt'];

    protected $appends = ['is_guest', 'item_total', 'item_count', 'item_qty_count'];

    protected $casts = [
        'total_qty'             => 'int',
        'custom_data'           => 'array',
        'total_discount_amount' => 'float',
        'is_free_shipping'      => 'boolean',
        'has_mbrship_card'      => 'boolean',
        'has_shipping'          => 'boolean',
        'has_preorder'          => 'boolean',
        'has_error'             => 'boolean',
    ];

    public function items()
    {
        return $this->hasMany(CartItem::class, 'cart_id', 'id');
    }

    public function scopePending($query)
    {
        return $query->where('status_level', 1);
    }

    public function scopeCompleted($query)
    {
        return $query->where('status_level', 3);
    }

    public function scopeExpired($query)
    {
        return $query->where('status_level', 2);
    }

    public function scopeActive($query)
    {
        return $query->where('status_level', 0);
    }

    public function scopeInstance($query, $instance_name = 'default')
    {
        return $query->where('cart_label', $instance_name);
    }

    public function scopeUser($query, $user_id = null)
    {
        $user_id = $user_id ?: request('mbrId');//config('cart.user_id');

        if ($user_id instanceof \Closure) {
            $user_id = $user_id();
        }

        return $query->where('mbr_id', $user_id);
    }

    public function scopeSession($query, $session_id = null)
    {
        $session_id = $session_id ?: request('cart_sess_id');

        return $query->where('session_id', $session_id);
    }

    public function setTotalPriceAttribute($value)
    {
        $this->attributes['total_price'] = $value;
    }

    /**
     * Get the current cart instance
     *
     * @param  string $instance_name
     *
     * @return mixed
     */
    public static function current($instance_name = 'default', $save_on_demand = null)
    {
        $save_on_demand = is_null($save_on_demand) ? config('cart.save_on_demand', false) : $save_on_demand;

        return static::init($instance_name, $save_on_demand);
    }

    /**
     * Initialize the cart
     *
     * @param  string $instance_name
     * @param  boolean $save_on_demand
     *
     * @return mixed
     */
    public static function init($instance_name, $save_on_demand = false)
    {
        $session_id = request('cart_sess_id');
        $user_id    = request('mbrId');//config('cart.user_id');

        if ($user_id instanceof \Closure) {
            $user_id = $user_id();
        }

        //if user logged in
        if ($user_id) {
            $user_cart       = static::active()->user()->where('cart_label', $instance_name)->first();
            $session_cart_id = request('cart_sess_id');
            $session_cart    = is_null($session_cart_id) ? null : static::active()->session($session_cart_id)->where('cart_label', $instance_name)->first();

            switch (true) {

                case is_null($user_cart) && is_null($session_cart): //no user cart or session cart
                    $attributes = [
                        'mbr_id'       => $user_id,
                        'cart_label'   => $instance_name,
                        'status_level' => 0,
                    ];

                    if ($save_on_demand)
                        $cart = new static($attributes);
                    else
                        $cart = static::create($attributes);
                    break;

                case ! is_null($user_cart) && is_null($session_cart): //only user cart
                    $cart = $user_cart;
                    break;

                case is_null($user_cart) && ! is_null($session_cart): //only session cart
                    $cart         = $session_cart;
                    $cart->mbr_id = $user_id;
                    $cart->save();
                    break;

                case ! is_null($user_cart) && ! is_null($session_cart): //both user cart and session cart exists
                    $session_cart->moveItemsTo($user_cart); //move items from session cart to user cart
                    $session_cart->delete(); //delete session cart
                    $cart = $user_cart;
                    break;
            }

            return $cart;
        }

        //guest user, create cart with session id
        if (empty($session_id)) {
            $session_id = md5(uniqid(rand(), true));
        }

        $attributes = [
            'session_id'   => $session_id,
            'cart_label'   => $instance_name,
            'status_level' => 0,
        ];

        $cart = static::firstOrNew($attributes);

        if (! $save_on_demand) {
            $cart->save();
        }

        //save current session id, since upon login session id will be regenerated
        //we will use this id to get back the cart before login
        //$request->session()->put('cart_'.$instance_name, $session_id);

        return $cart;

    }

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        //delete line items
        static::deleting(function (Cart $cart) {
            $cart->items()->delete();
        });
    }

    /**
     * If a change is made to items, then reset lazyloaded relations to reflect new changes
     *
     */
    public function resetRelations()
    {
        foreach ($this->relations as $key => $value) {
            $this->getRelationshipFromMethod($key);
        }

        return $this;
    }

    public function getItem(array $attributes = [])
    {
        return $this->items()->where($attributes)->first();
    }

    public function getItems(array $attributes = [])
    {
        return $this->items()->where($attributes)->get();
    }

    /*
     * Add item to a cart
     *
     * @param  array $attributes
     */
    public function addItem(array $attributes = [])
    {
        return $this->items()->create($attributes);
    }

    /*
     * Add items to a cart
     *
     * @param  array $attributes
     */
    public function addItems(array $attributes = [])
    {
        return $this->items()->createMany($attributes);

    }

    /*
     * remove item from a cart
     *
     * @param  array $attributes
     */
    public function removeItem(array $attributes = [])
    {
        return $this->items()->where($attributes)->first()->delete();
    }

    public function removeItems(array $attributes = [])
    {
        return $this->items()->where($attributes)->delete();
    }

    /*
     * update item in a cart
     *
     * @param  array $attributes
     */
    public function updateItem(array $where, array $values)
    {
        return $this->items()->where($where)->first()->update($values);
    }

    public function updateItems(array $where, array $values)
    {
        return $this->items()->where($where)->update($values);
    }

    /*
     * Cart checkout.
     *
     */
    public function checkout()
    {
        return $this->update(['status_level' => 1]);
    }

    /**
     * Expires a cart
     *
     */
    public function expire()
    {
        return $this->update(['status_level' => 2]);
    }

    /**
     * Set a cart as complete
     *
     */
    public function complete()
    {
        return $this->update(['status_level' => 3]);
    }

    /**
     * Check if cart is empty
     *
     */
    public function isEmpty()
    {
        return $this->items->count() === 0;
    }

    public function hasItem($where)
    {
        return null !== $this->items()->where($where)->first();
    }

    /**
     * Empties a cart
     *
     */
    public function clear()
    {
        $this->items()->delete();
        $this->resetRelations()->updateTimestamps();

        return $this->save();
    }

    public function moveItemsTo(Cart $cart)
    {
        $this->items()->update(['cart_id' => $cart->id]);
        $cart->save();

        return $this->save();
    }

    public function getMbrIdAttribute()
    {
        $value = '';

        if (! empty($this->attributes['mbr_id'])) {
            $value = trim($this->attributes['mbr_id']);
        }

        return $value;
    }

    public function getGuestAttribute()
    {
        return $this->getCustomData('guest');
    }

    public function getShippingAttribute()
    {
        return $this->getCustomData('shipping');
    }

    public function getBillingAttribute()
    {
        return $this->getCustomData('billing');
    }

    public function getItemTotalAttribute()
    {
        $value = $this->items->reduce(function ($carry, $item) {
            return $carry + ($item->item_qty * $item->unit_price);
        });

        return $value ?? 0;
    }

    public function getItemCountAttribute()
    {
        return $this->items->count();
    }

    public function getItemQtyCountAttribute()
    {
        $value = $this->items->reduce(function ($carry, $item) {
            return $carry + $item->item_qty;
        });

        return $value ?? 0;
    }

    public function getTotalDiscountAmountAttribute()
    {
        $amount = $this->attributes['total_discount_amount'];

        return $amount > 0 ? $amount : 0;
    }

    public function getTotalSavingsAmountAttribute()
    {
        $amount = $this->attributes['total_savings_amount'];

        return $amount > 0 ? number_format($amount, 2) : 0;
    }

    public function getIsGuestAttribute()
    {
        return empty($this->attributes['mbr_id']);
    }
}