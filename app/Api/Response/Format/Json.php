<?php

namespace App\Api\Response\Format;

use Dingo\Api\Http\Response\Format\Json as BaseJson;


class Json extends BaseJson
{
    protected function encode($content)
    {
        if ($this->response->getStatusCode() === 200) {

            $content = [
                'message'     => 'success',
                'status_code' => 200,
                'data'        => $content,
            ];

        }

        return parent::encode($content);
    }
}