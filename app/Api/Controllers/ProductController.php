<?php
/*
namespace App\Api\Controllers;


use App\Http\Resources\ProductResource;
use App\Repositories\ProductRepository;
use App\Services\ProductService;

class ProductController extends Controller
{
    public function __construct(ProductRepository $productRepo, ProductService $productSvc){
        $this->productRepo = $productRepo;
        $this->productSvc = $productSvc;
    }

    public function live_prices($productId){

        $loc_id = (isset($_GET['loc_id'])) ? $_GET['loc_id'] : '';
        $lot_id = (isset($_GET['lot_id'])) ? $_GET['lot_id'] : '';
        $customer_group = (isset($_GET['customer_group'])) ? $_GET['customer_group'] : 'GUEST';

        $products = $this->productRepo->getLivePrices($productId,$lot_id,$loc_id);
        if (!$products) {
            $response = array(
                'code' => 0,
                'msg' => 'No item found'
            );
        }
        else {
            $products->unit_price = ($products->promo_price > 0 && $products->regular_price > $products->promo_price) ? $products->promo_price : $products->regular_price;
            if ($customer_group == 'MEMBER') {
                $products->unit_price = ($products->promo_price > 0 && $products->mbr_price > $products->promo_price) ? $products->promo_price : $products->mbr_price;
            }

            $response = array(
                'code' => 1,
                'data' => $products
            );
        }

        echo json_encode($response);

    }

    public function detail($item_id) {

        $product = $this->productRepo->getOne($item_id);

        if (! $product) {
            return $this->response->noContent();
        }
        else {
            $product->pwp = $this->productRepo->getPwp($item_id,'**,BF');
            return $this->response->item($product, new ProductResource);
        }

    }

    public function search($mode,$keyword) {

        if ($mode=="genre") {
            switch ($keyword) {
                case "MBR":
                    $field = 'item_type';
                    $keyword = 'M';
                    $eq = '=';
                    break;
                case "SSEW":
                    $field = 'item_type';
                    $keyword = 'W';
                    $eq = '=';
                    break;
                case "ARAE":
                    $field = 'item_type';
                    $keyword = 'A';
                    $eq = '=';
                    break;
                case "EGIFT":
                    $field = 'item_type';
                    $keyword = 'G';
                    $eq = '=';
                    break;
                case "INK":
                    $field = 'item_id';
                    $keyword = '%INK%';
                    break;
                case "PB":
                    $field = 'item_id';
                    $keyword = '%PB-%';
                    break;
                case "BAG":
                    $field = 'item_desc';
                    $keyword = '%woven bag%';
                    break;
                default:
                    $field = 'item_desc';
                    $keyword = '%'.$keyword.'%';
                    break;
            }
        }
        else {
            $field = 'item_desc';
            $keyword = '%'.$keyword.'%';
        }

        $product = $this->productRepo->getSearch($field,$keyword,$eq='like');
        if (!count($product)) {
            return $this->response->noContent();
        }
        return ProductResource::collection($product);
    }

    public function purchaseWithPurchaseProducts($productId) {
        $products = $this->productSvc->getPurchaseWithPurchaseProducts($productId, request('group', 0));

        return $this->response->array($products);
    }

    public function complementaryProducts($itemId) {
        $products = $this->productRepo->getComplimentaryProducts($itemId);

        if ($products->isEmpty()) {
            return $this->response->noContent();
        }

        return ProductResource::collection($products);
    }

}
*/