<?php
/*
namespace App\Api\Controllers;

use App\Http\Resources\MemberResource;
use App\Models\MemberTransactionHistory;
use App\Repositories\MemberRepository;
use DB;

class MemberController extends Controller
{
    protected $memberRepo;

    public function __construct(MemberRepository $memberRepo)
    {
        $this->memberRepo = $memberRepo;
    }

    public function show($id)
    {
        $result = $this->memberRepo->getOne($id);

        if ($result !== null) {
            return new MemberResource($result);
        }

        return $this->response->noContent();
    }

    public function associate($mbrId)
    {
        $result = $this->memberRepo->getAssociatedMembers($mbrId);

        if ($result->isNotEmpty()) {
            return MemberResource::collection($result);
        }

        return $this->response->noContent();
    }

    public function transaction()
    {
        DB::enableQueryLog();
        ini_set('max_execution_time', 180);
        try {
            $r = MemberTransactionHistory::where('coy_id', 'CTL')->where('mbr_id', 'V160000400')->orderBy('created_on', 'asc')->paginate();
            var_dump($r);
        } catch (\Exception $e) {
            dd(DB::getQueryLog());
        }
        var_dump(DB::getQueryLog());

    }
}
*/