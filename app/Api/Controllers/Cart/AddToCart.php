<?php
/*
namespace App\Api\Controllers\Cart;

use App\Api\Controllers\Controller;
use App\Repositories\ProductRepository;
use App\Services\CartService;
use Illuminate\Http\Request;

class AddToCart extends Controller
{
    public function __invoke(Request $request, CartService $cartService, ProductRepository $productRepo)
    {
        $itemId     = $request->get('itemId');
        $qty        = $request->get('qty', 1);
        $srcMedium  = $request->get('medium', '');
        $storeLocId = $request->get('storeId', '');

        $product = $productRepo->getOne($itemId);

        $insertData = [
            'item_id'       => $product->item_id,
            'item_medium'   => $srcMedium,
            'item_img'      => $product->image_name,
            'item_desc'     => $product->item_desc,
            'regular_price' => $product->regular_price,
            //'member_price'  => $product->mbr_price,
            //'promo_price'   => $product->promo_price,
            'unit_price'    => $product->promo_price ?? $product->mbr_price ?? $product->regular_price,
            'item_qty'      => $qty,
            'loc_id'        => $storeLocId,
        ];

        $insertData['custom_data'] = ['sada', 'asd', 'sa' => 304];

        $item = cart()->addItem($insertData);

        if ($item) {

            $pwp = $cartService->buildInsertRows($item, [
                ['item_id' => '8000000041425', 'qty' => 2],
                ['item_id' => '8887815477270', 'qty' => 2],
                ['item_id' => '8000000080882', 'qty' => 2],
            ]);

            if($pwp){

                cart()->addItems($pwp->toArray());

            }

            return 'ok';
        }
    }
}
*/