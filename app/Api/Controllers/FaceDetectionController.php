<?php

namespace App\Api\Controllers;

use App\Http\Controllers\Controller;
use Flugg\Responder\Responder;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class FaceDetectionController extends Controller
{
    public function __construct()
    {
        $this->url = env('API_FACE_URL');
        $this->key = env('API_FACE_KEY');
    }

    public function __invoke(Request $request,Responder $responder){

        $img = $request->get('image');
        $b64 = $this->check_base64($img[0]);

        if($b64){
            $data = [
                'staffcode' => 'NA',
                'func' => 'searchMultiFace',
                'collectionId' => 'chstaff',
                'loc_id' => 'UBI',
                'image'  => $img,
            ];
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $response = $client->request('POST', $this->url, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'x-api-key' => $this->key
                ],
                'body' => json_encode($data)
            ]);

            $content = $response->getBody()->getContents();
            $content_data = json_decode($content, true);

            if(isset($content_data['msg'])){
                return response()->json([
                    'success' => true,
                    'data' => $content_data,
                ]);
            }
            else{
                return $responder->error(500,$content_data['message']);
            }
        }
        else{
            return $responder->error(500,'Image base64 is not valid');
        }
    }

    public function check_base64($s){
        // Check if there are valid base64 characters
        if (!preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $s)) return false;

        // Decode the string in strict mode and check the results
        $decoded = base64_decode($s, true);
        if(false === $decoded) return false;

        // Encode the string again
        if(base64_encode($decoded) != $s) return false;

        return true;
    }
}
