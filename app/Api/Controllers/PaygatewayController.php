<?php

namespace App\Api\Controllers;
use App\Repositories\PaygatewayRepository;
use App\Support\Str;
use App\Core\Orders\Services\CustomCommonOrder;
use App\Api\Controllers\CRC16;
use App\Utils\Request;

class PaygatewayController extends Controller
{
    public function __construct(PaygatewayRepository $paygatewayRepo)
    {
        $this->paygatewayRepo = $paygatewayRepo;
        $this->coy_id = 'CTL';

        $this->api_chvoices = env('API_URL_CHVOICES');
        $this->api_valueclub = env('API_URL_VALUECLUB');
        $this->api_valueclub_token = env('API_URL_VALUECLUB_TOKEN');
        $this->customCommonOrder = (new CustomCommonOrder);

        if(strpos($_SERVER['SERVER_NAME'], 'localhost') !== false){
            $this->uob_api_url = "https://api-uat.uob.com.sg";
        } else{
            $this->uob_api_url = "https://api.uob.com.sg";
        }

       
		$this->jwt = new JwtController();
    }

    public function qrpayment($mode) {

        $data = json_decode(file_get_contents('php://input'), true);
        $data['coy_id'] = (isset($data['coy_id'])) ? $data['coy_id'] : 'CTL';
        $data['pos_id'] = (isset($data['pos_id'])) ? $data['pos_id'] : '';

        $func = $this->paygatewayRepo->func_data($mode,$data['coy_id'],$data['loc_id'],$data['pos_id']);
        if ($func) {

            if ($mode=="EP-KRISPAY") {
                $data = [
                    'title' => $func->udf_data1,
                    'qr_code' => $func->udf_data2,
                    'image' => $func->udf_data3,
                    'loc_id' => $func->loc_id,
                    'pos_id' => $func->pos_id,
                ];
            }
            else {
                $data = $func;
            }

            $return_data['code'] = 1;
            $return_data['msg'] = 'OK';
            $return_data['data'] = $data;
        }
        else {
            $return_data['code'] = 0;
            $return_data['msg'] = 'No function found for Loc/Pos';
        }

        echo json_encode($return_data);
    }

    public function gatewayprocess() {
        // Only process those that are needed
        $pay_ctlvch = [];
        $pay_ecredits = [];

        $data = json_decode(file_get_contents('php://input'), true);
        $trans_id = explode("-", $data['payments'][0]['trans_id'])[0];
        foreach ($data['payments'] as $payments){
            if ($payments['pay_mode']=="CTLVCH") {
                foreach($payments['custom_data']['vouchers'] as $vch) {
                    if ($vch['voucher_id']!='')
                        $pay_ctlvch[] = $vch['voucher_id'];
                }
            }
            if ($payments['pay_mode']=="ECREDIT") {
                foreach($payments['custom_data']['vouchers'] as $vch) {
                    if ($vch['voucher_id']!='')
                        $pay_ecredits[] = $vch['voucher_id'];
                }
            }
        }

        if (count($pay_ctlvch)>0) {
            // Utilize sms_voucher_list
            $utilize = $this->paygatewayRepo->utilize_sms($pay_ctlvch,$trans_id);
        }

        if (count($pay_ecredits)>0) {
            // Utilize crm_voucher_list
            //$utilize = $this->paygatewayRepo->utilize_crm($pay_ecredits,$trans_id);
            $data = [
                    "paytype"       => "ECREDIT",
                    "item_id"       => "",
                    "voucher_id"    => $pay_ecredits,
                    "trans_id"      => $trans_id,
                    "trans_amount"  => 0,
                    "loc_id"        => "-",
                    "pos_id"        => "-",
                    "usr_id"        => "-",
                    "action"        => "utilize"
            ];
            $utilize = $this->valueclub_ecredit($data);
        }

        if (isset($utilize)) {
            $response['code'] = 1;
            $response['msg'] = 'Voucher utilized';
        }
        else {
            $response['code'] = 0;
            $response['msg'] = 'No action';
        }

        echo json_encode($response);

    }

    

    public function gatewaypayment() {
        /*
        voucher_id: vouchers,
        item_id: item,
        trans_id: this.orderInfo.order_id,
        trans_amount: this.rtnFloat(this.orderAmountfield),
        loc_id: this.constants.DeviceLocId,
        pos_id: this.constants.DevicePosId,
        usr_id: this.constants.deviceStaffLogin.staff_id,
        action: action
         */

        $data = json_decode(file_get_contents('php://input'), true);
        $paytype = $data['paytype'];

        // Get item_id if paytype is this:
        $paytypeprefix = explode('-',$paytype)[0];
        if ( in_array($paytypeprefix, ['ECREDIT','CTL','INCALL','SMS','!P','!VCH','!MREP'] ) ) {
            $func = $this->paygatewayRepo->func_list($paytype);
            $data['item_id'] = (isset($func->func_dim5)) ? $func->func_dim5 : $data['item_id'];
        }

        // Call the respective api
        if ( in_array($paytypeprefix, ['ECREDIT','!P','!VCH','!MREP'] ) ) {
            //$response = $this->chvoices_crmvch($data);
            $response = $this->valueclub_ecredit($data);
        }
        else if ( in_array($paytypeprefix, ['SMS','CTL','INCALL'] ) ) {
            $response = $this->chvoices_smsvch($data);
        }
        else if ( in_array($paytype, ['MV-CAPITALAND','MV-CAPITALAND-C','MV-CAPITALAND-E','MV-CAPITALAND-J'] ) ){
            $response = $this->chvoices_capitastar($data);
        }
        else if ( in_array($paytype, ['MV-CDV'] ) ){
            $response = $this->local_jewelvch($data);
        }
        else if ( $paytype == "EP-ALIPAY" ) {
            $response = $this->chvoices_alipay($data);
        }
        else {
            $response['code'] = 0;
            $response['msg'] = 'Paytype not acceptable';
        }

        echo json_encode($response);
    }

    private function valueclub_ecredit($data){

        $vch = (isset($data['voucher_id'][0])) ? $data['voucher_id'][0] : '';

        if ($data['action']!='activate' && $vch == '') {
            return [
                "code"      => 0,
                "msg"       => "No voucher found",
                "voucher"   => null,
                "endpoint"  => ""
            ];
        }

        if ($data['action']=='activate') {

            if ($data['item_id'] != "!VCH-CREDIT") {
                return [
                    "code"      => 0,
                    "msg"       => "No voucher issued",
                    "voucher"   => null,
                    "endpoint"  => ""
                ];
            }

            // Issue a voucher
            $endpoint = $this->api_valueclub . "api/coupon/partner";
            $endpointquery['key_code'] = $this->api_valueclub_token;

            $body = [
                "mbr_id"        => "POS-MEMBER",
                "coupon_id"     => "!VCH-CREDIT",
                "partner_id"    => "ecredit",
                "issue_type"    => "CREDIT",
                "issue_reason"  => "REFUND",
                "receipt_id"    => $data['trans_id'] ?? '',
                "voucher_amount"=> $data['trans_amount']
            ];

            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $response = $client->request('POST', $endpoint, [
                'headers' => ['Content-Type' => 'application/json'],
                'query' => $endpointquery,
                'body' => json_encode($body)
            ]);

            $content = $response->getBody()->getContents();
            $content_data = json_decode($content,true);

            if ($content_data['status_code'] == 200) {
                $d = is_array($content_data['info']) ? $content_data['info'] : [];

                return [
                    "code"      => (strtotime($d['expiry_date']) > time()) ? 1 : 0,
                    "msg"       => (strtotime($d['expiry_date']) > time()) ? "Voucher is valid" : "Voucher has expired ($vch)",
                    "voucher"   => [
                        "voucher_id"    => $d['coupon_serialno'] ?? '',
                        "expiry_date"   => (isset($d['expiry_date'])) ? date('Y-m-d', strtotime($d['expiry_date'])) : '1900-01-01',
                        "status_level"  => 0,
                        "voucher_amount"=> $d['voucher_amount'] ?? 0
                    ],
                    "endpoint"  => $endpoint
                ];
            }
            else {
                return [
                    "code"      => 0,
                    "msg"       => "No voucher issued",
                    "voucher"   => null,
                    "endpoint"  => $endpoint
                ];
            }

        }
        else if ($data['action']=='utilize') {

            $endpoint = $this->api_valueclub . "api/coupon/credit";
            $endpointquery['key_code'] = $this->api_valueclub_token;

            $body = [
                'coupon_serialno'   => $data['voucher_id'][0],
                'trans_id'          => $data['trans_id'],
                'coupon_id'         => $data['item_id']
            ];

            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $response = $client->request('POST', $endpoint, [
                'headers' => ['Content-Type' => 'application/json'],
                'query' => $endpointquery,
                'body' => json_encode($body)
            ]);

            $content = $response->getBody()->getContents();
            $content_data = json_decode($content,true);

            if ($content_data['status_code'] == 200) {
                //return true;
                $returnVch = null;
                if (isset($content_data['data']) && isset($content_data['data'][0]))
                    $returnVch = $content_data['data'][0];
                
                return [
                    "code"      => 1,
                    "msg"       => "Voucher utilized ($vch)",
                    "voucher"   => [
                        "voucher_id"    => ($returnVch) ? $returnVch['coupon_serialno'] : $vch,
                        "expiry_date"   => ($returnVch) ? $returnVch['expiry_date'] : date('Y-m-d'),
                        "status_level"  => 1,
                        "voucher_amount"=> ($returnVch) ? $returnVch['voucher_amount'] : 0
                    ],
                    "endpoint"  => $endpoint
                ];
            }
            else {
                return null;
            }

        }
        else {

            $endpoint = $this->api_valueclub . "api/coupon/credit/" . $vch;
            $endpointquery['key_code'] = $this->api_valueclub_token;

            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $response = $client->request('GET', $endpoint, ['query' => $endpointquery]);

            $content = $response->getBody()->getContents();
            $content_data = json_decode($content,true);

            if ($content_data['status_code'] == 200) {
                $d = $content_data['data'];

                if ($d['status_level'] > 0) {
                    return [
                        "code"      => 0,
                        "msg"       => "Voucher utilized ($vch)",
                        "voucher"   => null,
                        "endpoint"  => $endpoint
                    ];
                }

                return [
                    "code"      => (strtotime($d['expiry_date']) > time()) ? 1 : 0,
                    "msg"       => (strtotime($d['expiry_date']) > time()) ? "Voucher is valid" : "Voucher has expired ($vch)",
                    "voucher"   => [
                        "voucher_id"    => $d['coupon_serialno'],
                        "expiry_date"   => date('Y-m-d', strtotime($d['expiry_date'])),
                        "status_level"  => $d['status_level'],
                        "voucher_amount"=> $d['voucher_amount']
                    ],
                    "endpoint"  => $endpoint
                ];
            }
            else {
                return [
                    "code"      => 0,
                    "msg"       => "No voucher found ($vch)",
                    "voucher"   => null,
                    "endpoint"  => $endpoint
                ];
            }
        }

    }

    private function chvoices_smsvch($data){
        $body = json_encode($data);
        $action = $data['action'];
        $url = $this->api_chvoices . 'api/ctlvch/sms_voucher/'.$action.'/' . trim($data['trans_id']) . '?PKEY=obh0hqcxz81upu2ixyokn9t49na1ak3a';

        $content_data = $this->callGateway($url,$body, $data['trans_id']);
        $return_data = $content_data;
        $return_data['endpoint'] = $url;
        return $return_data;
    }

    private function chvoices_crmvch($data){
        $body = json_encode($data);
        $action = $data['action'];
        $url = $this->api_chvoices . 'api/ctlvch/crm_voucher/'.$action.'/' . trim($data['trans_id']) . '?PKEY=obh0hqcxz81upu2ixyokn9t49na1ak3a';

        $content_data = $this->callGateway($url,$body, $data['trans_id']);
        $return_data = $content_data;
        $return_data['endpoint'] = $url;
        return $return_data;
    }

    private function chvoices_capitastar($data){
        $body = json_encode([ "voucher"=> $data['voucher_id'] ]);
        $action = ($data['action']=='utilize') ? 'pay' : $data['action']; //validate/pay
        $url = $this->api_chvoices . 'api/capitastar/'.$action.'/'. trim($data['trans_id']) .'/'. trim($data['loc_id']) .'/'. trim($data['pos_id']);

        $content_data = $this->callGateway($url,$body, $data['trans_id']);

        if ($content_data['code']>0){
            $return_data['code'] = 1;
            $return_data['msg'] = 'OK';
            $return_data['voucher'] = [
                "voucher_id"=> (isset($data['voucher_id'][0])) ? (trim($data['voucher_id'][0])) : '',
                "expiry_date"=>"-",
                "voucher_amount"=> $content_data['payment_amount']
            ];
        }
        else {
            $msg = $content_data['result'] . ' ';
            $msg.= (isset($content_data['voucher_log'][0]['remark'])) ? $content_data['voucher_log'][0]['remark'] . ' ' : '';
            $msg.= (isset($content_data['error'])) ? $content_data['error'] . ' ' : '';

            $return_data['code'] = 0;
            $return_data['msg'] = $msg;
            $return_data['data'] = $content_data;
        }
        $return_data['endpoint'] = $url;
        return $return_data;
    }

    private function chvoices_alipay($data){
        $body = "";
        $url = $this->api_chvoices . 'api/alipay/pay/'. trim($data['trans_id']) .'/'. trim($data['loc_id']) .'/'. trim($data['pos_id']) .'/'. trim($data['trans_amount']) .'/'. urlencode(trim($data['voucher_id'][0]));

        $content_data = $this->callGateway($url,$body, $data['trans_id']);

        if (isset($content_data['payment_amount']) && $content_data['payment_amount']>0){
            $return_data['code'] = 1;
            $return_data['msg'] = 'OK';
            $return_data['voucher'] = ["voucher_id"=>urlencode(trim($data['voucher_id'])),"expiry_date"=>"-","voucher_amount"=> $content_data['payment_amount']];
        }
        else {
            $return_data['code'] = 0;
            $return_data['msg'] = $content_data['result'];
            $return_data['data'] = $content_data;
        }

        return $return_data;
    }

    private function local_jewelvch($data) {
        $vch_id = urlencode(trim($data['voucher_id'][0]));
        $vch_amt = (strtoupper(substr($vch_id,0,3))=="JRT") ? 5 : 10;

        $return_data['code'] = 1;
        $return_data['msg'] = 'OK';
        $return_data['voucher'] = ["voucher_id"=>$vch_id,"expiry_date"=>"-","voucher_amount"=>$vch_amt];

        return $return_data;
    }

    private function callGateway($url,$body, $trans_id='', $log_client='epayment'){

        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->post($url,[
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $body
        ]);

        $content = $response->getBody()->getContents();
        $content_data = json_decode($content,true);

        $logging = $this->paygatewayRepo->api_log([
            "api_client"=> $log_client,
            "api_description"=> 'PAY_TRANS_'. trim($trans_id) ,
            "request_method"=> 'POST',
            "request_url"=> $url,
            "request_header"=> '{}',
            "request_body"=> json_encode($body),
            "request_on"=> date('Y-m-d H:i:s'),
            "response_header"=> '{}',
            "response_body"=> $content,
            "response_on"=> date('Y-m-d H:i:s'),
            "error_msg"=> '',
            "created_on"=> date('Y-m-d H:i:s')
        ]);

        return $content_data;
    }

    public function krispay_status() {
        $data = json_decode(file_get_contents('php://input'), true);

        $apikey = 'v49g99qrbszsqypvqsn8xx5v';
        $secret = '7nWZBVmJd9';
        $str = $apikey.$secret.time();
        $signatureStr = hash('sha256', $str);

        $endpoint = 'https://apigw.singaporeair.com/krisplus-uat/v1/transactions/decode-qr';

        $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $response = $client->request('POST', $endpoint, [
                'headers' => [
                                'Content-Type' => 'application/json',
                                'apikey' => 'v49g99qrbszsqypvqsn8xx5v',
                                'x-signature' => $signatureStr,
                                'orgID' => env('KRIS_ORGID')
                            ],
                'body' => json_encode($data)
            ]);

            $content = $response->getBody()->getContents();
            //$content_data = json_decode($content,true);

            //var_dump($content_data);

        echo json_encode($content);
    }

    public function custom_validate() {
        $action = $_GET['action'];
        if ($action=="validate"){
            $this->customCommonOrder->custom_validate();
            return;
        }
        $response['code'] = 0;
        $response['msg'] = 'Invalid action request';
        echo json_encode($response);
    }

    public function uob_paynow(Request $request){
        $data = json_decode(file_get_contents('php://input'), true);

        $error = "";

        if(empty($data['amount']) || empty($data['inv_number'])){
            $error = "Amount or invoice Number can't be blank";
        }

        if(!empty($data['amount']) && $data['amount'] <= 0 ){
            $error = "Please provide a valid amount";
        }

        if(!$error){
            $mainData = 
            [
                [ "id" => '00', "value" => '01' ],                    											// ID 00: Payload Format Indicator (Fixed to '01')
                [ "id" => '01', "value" => '12' ],                    											// ID 01: Point of Initiation Method 11: static, 12: dynamic
                [
                    "id" => '26', "value" =>                            										// ID 26: Merchant Account Info Template
                        [
                            [ "id" => '00', "value" => 'SG.PAYNOW' ],
                            [ "id" => '01', "value" => '2' ],                 					// 0 for mobile, 2 for UEN. 1 is not used.
                            [ "id" => '02', "value" => '198400182KOO1'],                // PayNow UEN (Company Unique Entity Number)
                            [ "id" => '03', "value" => 0 ],       						// 1 = Payment amount is editable, 0 = Not Editable
                            [ "id" => '04', "value" => date('YmdHis', strtotime(date('Y-m-d H:i:s'). ' + 24 hours'))]							// Expiry date (YYYYMMDDHHMMSS) 
                        ]
                ],
                [ "id" => '52', "value" => '0000' ],                  											// ID 52: Merchant Category Code (not used)
                [ "id" => '53', "value" => '702' ],                   											// ID 53: Currency. SGD is 702
                [ "id" => '54', "value" => number_format($data['amount'], 2, '.', '') ],  						// ID 54: Transaction Amount
                [ "id" => '58', "value" => 'SG' ],                    						// ID 58: 2-letter Country Code (SG)
                [ "id" => '59', "value" => 'Challenger Technologies Ltd' ],          											// ID 59: Company Name
                [ "id" => '60', "value" => 'Singapore' ]             									// ID 60: Merchant City
                
            ];
            
            $addtionalData = 
            [
                "id" => '62', "value" => 
                [
                    [                         													// ID 62: Additional data fields
                        "id" => '01', "value" => $data['inv_number']        					// ID 01: Bill Number
                    ]
                ]
            ];
    
            if($data['inv_number'] !== ""){
                array_push($mainData, $addtionalData);
            }
            
            $output = "";
            foreach ($mainData as $mainDataValue) {
                if (is_array($mainDataValue['value'])) {
                    $tempValue = "";
                    foreach ($mainDataValue['value'] as $nestedValue) {
                        $tempValue .= $nestedValue['id'] . $this->padLeft(strlen($nestedValue['value']), 2) . $nestedValue['value'];
                    }
                    $mainDataValue['value'] = $tempValue;
                }
                $output .= $mainDataValue['id'] . $this->padLeft(strlen($mainDataValue['value']), 2) . $mainDataValue['value'];
            }
            
            // Here we add "6304" to the previous string
            // ID 63 (Checksum) 04 (4 characters)
            // Do a CRC16 of the whole string including the "6304"
            // then append it to the end.
            $output .= '6304' . CRC16::calculate($output . '6304');
            // if ($this->createAsBase64Image) {
            // 	$output = $this->createAsBase64Image($output);
            // }
            $return_data['code'] = 1;
            $return_data['msg'] = 'OK';
            $return_data['data'] = $output;
        }else{
            $return_data['code'] = 0;
            $return_data['msg'] = $error;
        }

		echo json_encode($return_data);
    }

    /**
	 * @param integer $stringLength
	 * @param integer $padNumberCharacters
	 * @return string
	 */
	private function padLeft(string $s, int $n) 
	{
		if ($n <= strlen($s)) {
		  	return $s;
		} else {
		  	return '0' . $s;
		}
	}

    public function getUobJwt($payload){
        // print_r((dirname((__FILE__))));die;
        $fp = fopen(dirname(__FILE__) . "/jwtchallengersg-v2.key","r");
        $keyPrivateString = fread($fp,8192);
        fclose($fp);
        
        $SignedJWT = $this->jwt->encode($payload, $keyPrivateString,'RS256');
        return $SignedJWT;
    }

    public function uob_paynow_status(){
        $data = json_decode(file_get_contents('php://input'), true);
        $invoiceNumber = $data['inv_number'];
        $amount = $data['amount'];
        $uobHistorySearch = $data['uob_history_search'];

        if($uobHistorySearch){
            //directly call uob api search history
            $clientKey = dirname(__FILE__) . "/wmchallengersg-v2.key";
            $certFile = dirname(__FILE__) . "/www_challenger_sg.pem";

            $payload = array();
            $payload['account']['accountNumber'] = "4013195387";
            $payload['account']['accountCurrency'] = "SGD";
            $payload['account']['accountType'] = "D";
            $payload['transactionReference'] = "Txn History Ref";
            $payload['ourReference'] = "";
            $payload['yourReference'] = "";
            $payload['transactionText'] = "$invoiceNumber";
            $payload['amount'] = (float)$amount;

            $this->uob_jwt = $this->getUobJwt($payload);

            $header = array(
                'Content-Type:application/json',
                'Accept: application/json',
                'Application-ID: 73310971-16a0-410b-a066-27c4665d1811',
                'API-Key: 8f2eba90-deb4-497c-982f-871f4c876412',
                'Client-ID: c8707cb7-0e95-409b-811f-370b7d716256',
                'Country: SG',
                "Authorization: $this->uob_jwt"
            );

            // $curl = curl_init($this->uob_api_url . "/business/v1/accounts/transactionsearch?fromDate=07112022&toDate=11112022&fromTime=00:00&toTime=23:59");
            // get within 14 days results
            $date_14days_before = date('dmY', strtotime(date('Y-m-d'). ' - 14 days'));
            $curl = curl_init($this->uob_api_url . "/business/v1/accounts/transactionsearch?fromDate=".$date_14days_before."&toDate=".date('dmY')."&fromTime=00:00&toTime=23:59");

            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLINFO_HEADER_OUT,true);
            curl_setopt($curl, CURLOPT_TIMEOUT,1000);
            curl_setopt($curl, CURLOPT_SSLCERT, $certFile);
            curl_setopt($curl, CURLOPT_SSLKEY, $clientKey);
            $curl_response = curl_exec($curl);
            // $header_data = curl_getinfo($curl);
            $response = json_decode($curl_response,true);
            if(!empty($response['account']['accountNumber'])){
                $return_data['code'] = 1;
                $return_data['msg'] = "Transaction successful";
                // $return_data['data'] = $response;
            }else{
                $return_data['code'] = 0;
                $return_data['msg'] = "Transaction not found";
            }
        }else{
            // get from our uob stage server
            $header = array(
                'Content-Type:application/json',
                'Accept: application/json',
            );

            if(strpos($_SERVER['SERVER_NAME'], 'localhost') !== false){
                $uob_url = 'https://uobpay-stage.challenger.sg';
            } else{
                $uob_url = 'https://uobpay.challenger.sg';
            }

            $post_data = array(
                "invoiceNumber" => $invoiceNumber
            );

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $uob_url.'/v1/uob/getUOBCallbackTrans',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>json_encode($post_data),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                ),
            ));
            $response = json_decode(curl_exec($curl), true);
            
            if($response['code']){
                $return_data['code'] = 1;
                $return_data['msg'] = "Transaction successful";
            }else{
                $return_data['code'] = 0;
                $return_data['msg'] = "Transaction not found";
            }
        }
        echo json_encode($return_data);   
    }
}
