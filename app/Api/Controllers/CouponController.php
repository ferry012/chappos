<?php

namespace App\Api\Controllers;

use App\Support\Str;
use App\Utils\Helper;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    private $coy_id;

    public function __construct()
    {
        $this->coy_id = 'CTL';
    }

    public function grab_coupons($header=''){

        $cacheSec = app()->environment('production') ? 3600 : 1;
        list($cacheTag, $cacheKey) = Helper::generateCacheKeys(['products', 'promoItems'], 'GR-'.$header, [$header]);
        $dataVch = app('cache')->tags($cacheTag)->remember($cacheKey, $cacheSec, function () {
            $groupVch = [];

            $dbVch = db()->table('crm_coupon_grab')->get();
            foreach ($dbVch as $vch) {
                if (!empty($vch->grab_type) && $vch->grab_type != "HIDDEN"){
                    $display_data = json_decode($vch->display_data);

                    $header1 = $display_data->header ?? 'ALL';
                    $header2 = $display_data->subheader ?? '';

                    $voucher = [
                        "voucher_id" => $vch->coupon_id,
                        "voucher_name" => $display_data->name ?? '',
                        "image_name" => $vch->image_name ?? '',
                        "discount_desc" => $display_data->desc ?? '',
                        "discount_type" => $display_data->type ?? '',
                        "discount_amt" => $display_data->amt ?? '',
                        "collect_count" => 0,
                        "redeem_count" => 0,
                        "collect_limit" => 0,
                        "status_level" => 1,
                        "grab_type" => $vch->grab_type,
                    ];
                    if (isset($display_data->tnc)) {
                        $voucher["tnc"] = [
                            "redeem_limit" => ($display_data->tnc) ? $display_data->tnc->redeem_limit ?? 0 : 0,
                            "min_spend" => ($display_data->tnc) ? $display_data->tnc->min_spend ?? 0 : 0,
                            "scope" => ($display_data->tnc) ? $display_data->tnc->scope ?? '' : '',
                            "exp_date" => ($display_data->tnc) ? $display_data->tnc->exp_date ?? '' : '',
                            "vpoints_cap" => ($display_data->tnc) ? $display_data->tnc->vpoints_cap ?? 0 : 0
                        ];
                    }
                    if (isset($display_data->item) && count($display_data->item) > 0) {

                        // Get item info
                        $item_list = db()->table('v_ims_live_product')
                            ->select('item_id','item_desc','short_desc','image_name','regular_price')
                            ->whereIn('item_id', $display_data->item)->get();

                        foreach ($display_data->item as $itm) {
                            $itm_list = collect($item_list)->where('item_id',$itm)->first();
                            if ($itm_list) {
                                $voucher['item'][] = [
                                    "item_id" => $itm,
                                    "item_name" => ($itm_list->short_desc) ? $itm_list->short_desc : ($itm_list->item_desc ?? ''),
                                    "item_img" => (!empty($itm_list->image_name)) ? get_constant('s3.product_images_thumb') . $itm_list->image_name : "",
                                    "regular_price" => $itm_list->regular_price
                                ];
                            }
                        }
                    }

                    if (!empty($header2))
                        $groupVch[$header1][$header2][] = $voucher;
                    else
                        $groupVch[$header1][] = $voucher;
                }
            }

            return $groupVch;
        });

        // Usage data has to be live
        $sql = "select a.coupon_id,a.eff_from,a.eff_to,a.grab_type,
                   coalesce(max_grab,0) collect_limit, a.grab_stats->>'total_count' collect_count,
                   coalesce(max_use,0) redeem_limit, usage_stats->>'total_count' redeem_count
            from crm_coupon_grab a
            left join crm_coupon_list b on a.coupon_id=b.coupon_id";
        $vchStats = collect(db()->select($sql));

        $data = [];
        foreach ($dataVch as $gKey => $gVch) {
            if (isset($gVch[0]['voucher_id'])){
                // 1 level vouchers
                if (empty($header)  || (!empty($header) && clean_string($header,'lower') == clean_string($gKey,'lower'))) {
                    $data[] = [
                        "id" => clean_string($gKey,'lower'),
                        "type" => $gKey,
                        "vouchers" => $this->_updVchStat($gVch, $vchStats)
                    ];
                }
            }
            else {
                // 2 level vouchers
                $headerArr = explode('.', $header);
                $categories = [];
                foreach($gVch as $cKey => $cVch) {
                    if (isset($cVch[0]['voucher_id'])) {
                        if (empty($headerArr[1]) || (!empty($headerArr[1]) && clean_string($headerArr[1],'lower') == clean_string($cKey,'lower'))) {
                            $categories[] = [
                                "id" => clean_string($cKey,'lower'),
                                "category" => $cKey,
                                "vouchers" => $this->_updVchStat($cVch, $vchStats)
                            ];
                        }
                    }
                }
                if (empty($headerArr[0]) || (!empty($headerArr[0]) && clean_string($headerArr[0],'lower') == clean_string($gKey,'lower'))) {
                    $data[] = [
                        "id" => clean_string($gKey,'lower'),
                        "type" => $gKey,
                        "categories" => $categories
                    ];
                }
            }
        }

        return $data;
    }

    private function _updVchStat($vouchers, $stats){
        foreach($vouchers as $k=>$vch){
            $vchStat = $stats->where('coupon_id', $vch['voucher_id'])->first();

            $collect_limit = $vchStat->collect_limit ?? 0;
            $redeem_limit = $vchStat->redeem_limit ?? 0;
            $vouchers[$k]['collect_limit'] = $collect_limit;
            $vouchers[$k]['collect_count'] = $vchStat->collect_count ?? 0;
            $vouchers[$k]['redeem_count'] = $vchStat->redeem_count ?? 0;

            if ($vchStat && strtotime($vchStat->eff_to) < time())
                $vouchers[$k]['status_level'] = -1;
            else if ($vchStat && strtotime($vchStat->eff_from) > time())
                $vouchers[$k]['status_level'] = 0;
            else if ( ($collect_limit > 0 && $vouchers[$k]['collect_count'] >= $collect_limit) ||
                ($redeem_limit > 0 && $vouchers[$k]['redeem_count'] >= $redeem_limit) )
                $vouchers[$k]['status_level'] = 2;
            else
                $vouchers[$k]['status_level'] = 1;
        }
        return $vouchers;
    }

    public function couponAllow(Request $request){
        //This will check the coupon if the order is valid or not depending on the total amount of the purchase


    }

}