<?php

namespace App\Api\Controllers;
use App\Repositories\ReportingRepository;
use App\Support\Str;

class ReportingController extends Controller
{
    public function __construct(ReportingRepository $reportingRepository){
        $this->reportingRepository = $reportingRepository;
    }

    public function order_resource(){

        $src_id = (isset($_GET['src_id'])) ? $_GET['src_id'] : '';
        $loc_id = (isset($_GET['loc_id'])) ? $_GET['loc_id'] : '';
        $eff_from = (isset($_GET['eff_from'])) ? $_GET['eff_from'] : date('Y-m-d');
        $eff_to = (isset($_GET['eff_to'])) ? $_GET['eff_to'] : date('Y-m-d');
        $order_num = (isset($_GET['trans_id'])) ? $_GET['trans_id'] : '';

        $results = null;
        $result_data = [];
        $result_stats = [];

        if ($src_id == 'RATING') {
            $results =  $this->reportingRepository->order_resource($src_id, $order_num, $loc_id, $eff_from,$eff_to);
            foreach($results as $row){
                $custom_data = json_decode($row->custom_data);
                $result_data[] = [
                    "order_num"     => $row->order_num,
                    "order_date"    => $row->created_on,
                    "loc_id"        => $row->loc_id,
                    "pos_id"        => $row->pos_id,
                    "customer_id"   => $row->customer_id,
                    "cashier_id"    => $row->cashier_id,
                    "rating"        => $custom_data->rating,
                    "improve_on"    => $custom_data->improve_on,
                ];

                if (isset($result_stats[$custom_data->rating]))
                    $result_stats[$custom_data->rating]++;
                else
                    $result_stats[$custom_data->rating] = 1;
            }
        }

        if ($results)
            $response = ['code'=>1, 'msg' => $src_id, 'stats'=>$result_stats, 'data'=>$result_data];
        else
            $response = ['code'=>0, 'msg' => 'No results'];

        echo json_encode($response);
    }

    public function unposted_order() {

        $param['loc_id'] = (isset($_GET['loc_id'])) ? $_GET['loc_id'] : '';
        $param['pos_id'] = (isset($_GET['pos_id'])) ? $_GET['pos_id'] : '';
        $param['trans_date'] = (isset($_GET['trans_date'])) ? $_GET['trans_date'] : '';

        $data =  $this->reportingRepository->order_unposted($param['loc_id'],$param['pos_id'],$param['trans_date']);

        $response = [
            'data' => $data,
//            'links' => ['first'=>''],
//            'meta' => ['total'=>0]
        ];

        echo json_encode($response);
    }

    public function crm_coupon_date(){
        $coupon_id = (isset($_GET['coupon_id'])) ? $_GET['coupon_id'] : '';
        $data = $this->reportingRepository->crm_coupon_dates($coupon_id);
        foreach ($data as $d){

            $dcherps = $this->reportingRepository->cherps_single_coupon($d->coupon_id);
            if ($dcherps) {
                $d->cherps_eff_from = $dcherps->eff_from;
                $d->cherps_eff_to = $dcherps->eff_to;

                if ($d->eff_to != $d->cherps_eff_to) {
                    $params['id'] = $d->id;
                    $params['eff_from'] = $d->cherps_eff_from;
                    $params['eff_to'] = $d->cherps_eff_to;
                    $this->reportingRepository->update_coupon_date($params);
                    $d->update = 'YES';
                }
            }
        }
        echo json_encode($data);
    }

    public function crm_promo_sync_to_cherps(){
        $promo_id = (isset($_GET['promo_id'])) ? $_GET['promo_id'] : '';
        $this->reportingRepository->crm_promo_sync_to_cherps($promo_id);
        echo json_encode(['code'=>1,'msg'=>'Success']);
    }

    public function ims_item_sync_from_cherps(){
        $item_id = (isset($_GET['item_id'])) ? $_GET['item_id'] : '';
        $log = $this->reportingRepository->ims_item_sync_from_cherps($item_id);
        echo json_encode(['code'=>1,'msg'=>'Success', 'log'=>$log]);
    }

    public function pos_from_cherps2_to_cherps(){
        $log = $this->reportingRepository->pos_from_cherps2_to_cherps();
        echo json_encode(['code'=>1,'msg'=>'Success', 'log'=>$log]);
    }
    public function pos_from_cherps2_to_cherps_single($id){
        $log = $this->reportingRepository->pos_from_cherps2_to_cherps($id);
        echo json_encode(['code'=>1,'msg'=>'Success', 'log'=>$log]);
    }

    public function crmpromo_from_chrooms_to_cherps2(){
        $log = $this->reportingRepository->crmpromo_from_chrooms_to_cherps2();
        echo json_encode(['code'=>1,'msg'=>'Success', 'log'=>$log]);
    }

    public function product_id_last_modified(){
        // For offline data sync
        header("Access-Control-Allow-Origin: *");

        $list = $this->reportingRepository->product_id_last_modified();
        echo json_encode(['code'=>1,'data'=>$list, 'count'=>count($list) ]);
    }

    public function hp_link_list() {
        $result = db()->table('ims_product_complimentary')->where('coy_id','CTL')->where('ref_type','HPINK')->where('ref_id','<>','')
            ->selectRaw('coy_id, rtrim(item_id) item_id, rtrim(comp_id) comp_id, ref_type, ref_id')->get();
        return $result;
    }

    public function crm_announcements($id=''){

        $synced_results = ["past_days" => 7, "outstanding" => 0, "synced" => 0, "skipped" => 0, "data" => []];

        if ($id!=''){
            $sql = "select a.coy_id,a.promo_id,
                           c.action_id announcement_id,coalesce(c.modified_on,'1900-01-01') announced_date,
                           a.modified_on last_modified,
                           a.promo_type,a.promo_desc,a.eff_from,a.eff_to,
                            coalesce((SELECT string_agg(price_ind, ', ') actor_list from crm_promo_price_ind d where d.coy_id=a.coy_id and d.promo_id=a.promo_id group by promo_id),'ALL') price_ind,
                            (select count(*) from crm_promo_location b where b.coy_id=a.coy_id and b.promo_id=a.promo_id) loc_count,
                            (select count(*) from crm_promo_location b where b.coy_id=a.coy_id and b.promo_id=a.promo_id and b.loc_id='**') loc_all
                    from crm_promo_list a
                    left join crm_promo_action c on a.coy_id=c.coy_id and a.promo_id=c.promo_id and c.action_type='ANNOUNCEMENT'
                    where a.coy_id='CTL' and a.promo_id=?";
            $promos = db()->select($sql,[$id]);
        }
        else {

            $sql = "select * from (
                    select a.coy_id,a.promo_id,
                           c.action_id announcement_id,coalesce(c.modified_on,'1900-01-01') announced_date,
                           a.modified_on last_modified,
                           a.promo_type,a.promo_desc,a.eff_from,a.eff_to,
                            coalesce((SELECT string_agg(price_ind, ', ') actor_list from crm_promo_price_ind d where d.coy_id=a.coy_id and d.promo_id=a.promo_id group by promo_id),'ALL') price_ind,
                            (select count(*) from crm_promo_location b where b.coy_id=a.coy_id and b.promo_id=a.promo_id) loc_count,
                            (select count(*) from crm_promo_location b where b.coy_id=a.coy_id and b.promo_id=a.promo_id and b.loc_id='**') loc_all
                    from crm_promo_list a
                    left join crm_promo_action c on a.coy_id=c.coy_id and a.promo_id=c.promo_id and c.action_type='ANNOUNCEMENT'
                    where a.coy_id='CTL' and a.promo_type IN ('PRC','MPT') and a.eff_to > current_timestamp and a.modified_on::date >= current_timestamp - interval '" . $synced_results["past_days"] . " day'
                ) tbl where (loc_count>5 or loc_all>=1) and announced_date<last_modified
                order by announced_date";
            $promos = db()->select($sql);

        }

        $synced_results["outstanding"] = count($promos);

        if ($promos){
            //foreach($promos as $promo) {
            $ixmax = 50;
            for($ix=0; $ix<$ixmax; $ix++){
                if (!isset($promos[$ix])) {
                    $ix = $ixmax;
                    continue;
                }

                $count = 0;
                $promo = $promos[$ix];
                $desc = "<b>". $promo->promo_id ." ". $promo->promo_type ." ITEM PROMOTION</b><br><br>";
                $desc.= "<u>".$promo->promo_desc."</u><br><br>";
                $desc.= "Period from " . date('d M Y H:i', strtotime($promo->eff_from)) . "H to " . date('d M Y H:i', strtotime($promo->eff_to)) . "H<br>";
                $desc.= "Applicable to " . ( ($promo->price_ind=='ALL' ? 'PUBLIC' : $promo->price_ind )) . "";

                $sql_item= <<<str
select promo.item_id,
       promo.item_desc,
       unit_price,
       promo_disc, multiple_points,promo_grp_id,
       RTRIM(item.brand_id) as brand_id,
       RTRIM(item.inv_dim3) as category_id
from crm_promo_item promo
inner join ims_item_list item on item.item_id= promo.item_id and item.coy_id= promo.coy_id
 where promo.coy_id = ? and promo_id = ?
str;

                $items = db()->select($sql_item,
                    [$promo->coy_id,$promo->promo_id]);

                $category= [];
                $category[]= $promo->promo_type;
                if ($items){
                    $desc.= "<br><br><table border='1' cellpadding='1' width='100%' valign='top'>";
                    foreach($items as $i) {
                        $brand_id= $i->brand_id;
                        $category_id= $i->category_id;
                        if(!in_array($brand_id,$category)){
                            $category[]= $brand_id;
                        }
                        if(!in_array($category_id,$category)){
                            $category[]= $category_id;
                        }

                        $price = "";
                        if ($promo->promo_type=='MPT') {
                            if ($i->multiple_points != 0 && $i->multiple_points != 1) {
                                $price .= ($i->promo_grp_id == 'FIXED') ? "" : "+";
                                $price .= $i->multiple_points . "%";
                            }
                            $price .= ($i->unit_price > 0) ? "+V$" . number_format($i->unit_price, 2) : "";
                        }
                        else {
                            $price = ($i->unit_price == 0 && ($i->promo_disc > 0 && $i->promo_disc < 100)) ? number_format($i->promo_disc, 2) . "%" : "$" . number_format($i->unit_price, 2);
                        }
                        $desc.= "<tr><td>". $i->item_id ."<br>". $i->item_desc ."</td><td align='right'>". $price ."</td></tr>";
                        $count++;
                    }
                    $desc.= "</table>";
                }
                else {
                    $items = db()->select("select inv_dim4,promo_disc from crm_promo_category where coy_id = ? and promo_id = ? ",[$promo->coy_id,$promo->promo_id]);
                    if ($items){
                        $desc.= "<br><br><table border='1' cellpadding='1' width='100%' valign='top'>";
                        foreach($items as $i) {
                            $price = number_format($i->promo_disc, 2)."%";
                            $desc.= "<tr><td>". $i->item_id ."<br>". $i->item_desc ."</td><td align='right'>". $price ."</td></tr>";
                            $count++;
                        }
                        $desc.= "</table>";
                    }
                }

                $location = db()->select("select loc_id from crm_promo_location where coy_id = ? and promo_id = ? order by length(loc_id),loc_id ",[$promo->coy_id,$promo->promo_id]);
                $location_filters= [];
                if ($location) {
                    $desc.= "<br><br>Valid at: ";
                    foreach($location as $k=>$i) {
                        if ($k>0)
                            $desc.= ", ";

                        if($i->loc_id=='**'){
                            $location_filters[] = 'ALL STORES';
                        }else{
                            $location_filters[] = $i->loc_id;
                        }

                        $desc.= ($i->loc_id=='**') ? 'ALL STORES' : $i->loc_id;
                    }
                }


                $desc.= "<br><br>Promotion last modified on " . date('d M Y H:i', strtotime($promo->last_modified)) . "H";
                $desc.= "<br>Synced on " . date('d M Y H:i') . "H";

                if ($id!=''){

                    $promo->announcement_id = ''; // Reset this so it will save new record in DB later
                    db()->table('crm_promo_action')->where('coy_id',$promo->coy_id)->where('promo_id',$promo->promo_id)->delete();

                }

                //add category / type / locations
                if ($promo->announcement_id != '') {
                    // Update
                    $body = [
                            "type" => "announcement",
                            "func" => "update",
                            "id" => $promo->announcement_id,
                            "user" => "MIS",
                            "item" => [
                                "title" => $promo->promo_desc,
                                "description" => $count . " items in this promotion valid from " . date('d M Y', strtotime($promo->eff_from)) . ". " . $promo->promo_desc,
                                "desc_long" => $desc,
                                "showdate" => $promo->last_modified, //(strtotime($promo->last_modified) < time()) ? date('Y-m-d H:i:s') : $promo->last_modified,
                                "eff_from" => date('Y-m-d 00:00:00', strtotime($promo->eff_from) - 86400),
                                "eff_to" => $promo->eff_to,
                                "has_push" => false,
                                "push_time" => $promo->eff_from,
                                "message" => "Promotion: " . $promo->promo_desc,
                                "category" => $category,
                                "locations" => $location_filters
                            ]
                    ];
                }
                else {
                    // Add
                    $body = [
                        "type" => "announcement",
                        "func" => "add",
                        "user" => "MIS",
                        "item" => [
                            "title" => $promo->promo_desc,
                            "description" => $count . " items in this promotion valid from " . date('d M Y', strtotime($promo->eff_from)) . ". " . $promo->promo_desc,
                            "desc_long" => $desc,
                            "showdate" => $promo->last_modified, //(strtotime($promo->last_modified) < time()) ? date('Y-m-d H:i:s') : $promo->last_modified,
                            "eff_from" => $promo->eff_from,
                            "eff_to" => $promo->eff_to,
                            "has_push" => false,
                            "push_time" => $promo->eff_from,
                            "message" => "Promotion: " . $promo->promo_desc,
                            "category" => $category,
                            "locations" => $location_filters
                        ]
                    ];
                }

                // Sync to Announcement
                if ($count > 0) {
                    $endpoint = "https://nem5kj82lb.execute-api.ap-southeast-1.amazonaws.com/default/opsappone";
                    $client = new \GuzzleHttp\Client(['http_errors' => false]);
                    $response = $client->request('POST', $endpoint, [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'x-api-key' => 'RqIGDJg8UG1BxeWsSadAc7LJAJDySPqq1wUSUdUK'
                        ],
                        'body' => json_encode($body)
                    ]);

                    $content = $response->getBody()->getContents();
                    $content_data = json_decode($content, true);

                    if ($content_data['body']['code'] == 1) {
                        // Success
                        if ($promo->announcement_id != '') {
                            // Update crm_promo_action
                            $announcement_id = $promo->announcement_id;
                            $where = [
                                'coy_id' => $promo->coy_id,
                                'promo_id' => $promo->promo_id,
                                'action_type' => 'ANNOUNCEMENT',
                                'action_id' => $announcement_id
                            ];
                            $successdata = [
                                'modified_on' => date('Y-m-d H:i:s')
                            ];
                            db()->table('crm_promo_action')->where($where)->update($successdata);
                        } else {
                            // Save crm_promo_action
                            $announcement_id = $content_data['body']['result'];
                            $successdata = [
                                'coy_id' => $promo->coy_id,
                                'promo_id' => $promo->promo_id,
                                'action_type' => 'ANNOUNCEMENT',
                                'action_id' => $announcement_id,
                                'created_on' => date('Y-m-d H:i:s'),
                                'modified_on' => date('Y-m-d H:i:s')
                            ];
                            db()->table('crm_promo_action')->insert($successdata);
                        }

                        $synced_results["synced"]++;
                        $synced_results["data"][] = [
                            "promo_id" => $promo->promo_id,
                            "promo_desc" => $promo->promo_desc,
                            "announcement_id" => $announcement_id,
                            "body" => $body
                        ];
                    }
                }
                else {
                    $synced_results["skipped"]++;
                }
            }
        }

        return $synced_results;

    }


    
}