<?php

namespace App\Api\Controllers;
use App\Repositories\SettingRepository;
use App\Support\Str;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function __construct(SettingRepository $settingRepo)
    {
        $this->settingRepo = $settingRepo;
        $this->coy_id = 'CTL';

        //header("Access-Control-Allow-Origin: *");

        $this->api_chvoices = env('API_URL_CHVOICES');
        $this->api_valueclub = env('API_URL_VALUECLUB');
        $this->api_valueclub_token = env('API_URL_VALUECLUB_TOKEN');

    }

    public function ping(){
        if (isset($_SERVER['SERVER_ADDR'])){
            $SERVER_ADDR = $_SERVER['SERVER_ADDR'];
        }else{
            $SERVER_ADDR = "TEST";
        }
        $response = array(
            'code' => '200',
            'msg' => 'pong',
            'env' => app()->environment(),
            'posapi_addr' => $SERVER_ADDR,
            'app_version' => $this->device_version(false)
        );

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    public function keylist(){

        $loc_id = $_GET['loc_id'];
        $location_list = $this->settingRepo->pos_location($loc_id);
        $hotkeys = $this->settingRepo->func_hotkeys($location_list->loc_id);

        $paymentkey = [];
        $paymentkeys = $this->settingRepo->func_paylist($location_list->loc_id, $location_list->loc_mall);
        foreach($paymentkeys as $pkeys){
            $paymentkey[$pkeys->id]['id'] = $pkeys->id;
            $paymentkey[$pkeys->id]['name'] = $pkeys->name;
            $paymentkey[$pkeys->id]['img'] = $pkeys->img;
            $paymentkey[$pkeys->id]['options'][] = [
                "func_id"=> $pkeys->func_id,
                "func_name"=> $pkeys->func_name,
                "func_map"=> $pkeys->func_map
            ];
        }

        // Get below from cache if available (non location base)
        $keylist_cache = app('cache')->remember('table.pos_keylist_static', now()->endOfDay(), function () {

            $ssapdata = [];
            $ssapkeys = $this->settingRepo->func_type('SSAP');
            foreach($ssapkeys as $ssapkey) {
                $ssapdata[$ssapkey->func_id] = $ssapkey->img;
            }

            $bagdata = [];
            $bagkeys = $this->settingRepo->func_type('BAG');
            foreach($bagkeys as $bagkey) {
                $bagdata[$bagkey->func_id] = [
                    "name" => $bagkey->func_name,
                    "img" => $bagkey->img,
                ];
            }

            $memberkey = [];
            try {
                $memberitems = app('api')->members()->apiMemberItems();
                // $memberitems = collect($memberitems)->where('status_level', '>=', 1);
                foreach ($memberitems as $mbrdata) {
                    if (!empty($mbrdata->item_group)) {
                        $memberkey[$mbrdata->item_group][$mbrdata->item_type] = [
                            "key" => $mbrdata->item_type,
                            "mbr_type" => $mbrdata->item_type,
                            "item_id" => $mbrdata->item_id,
                            "title" => $mbrdata->item_desc,
                            "price" => $mbrdata->unit_price,
                            "allow" => 1 // Static param, use by POS
                        ];
                    }
                    if ($mbrdata->item_group == "NEW") {
                        $ssapdata["mbr_signup"][] = [
                            "mbr_type" => $mbrdata->item_type,
                            "title" => $mbrdata->item_desc,
                            "desc" => $mbrdata->item_desc,
                            "price" => $mbrdata->unit_price,
                        ];
                    }
                }
            } catch (\Exception $e) {
                //
            }

            $paymentdefaultkey = [];
            $paymentdefaultkeys = $this->settingRepo->func_paydefaultlist();
            foreach($paymentdefaultkeys as $pkeys){
                $paymentdefaultkey[$pkeys->id]['id'] = $pkeys->id;
                $paymentdefaultkey[$pkeys->id]['name'] = $pkeys->name;
                $paymentdefaultkey[$pkeys->id]['img'] = $pkeys->img;

                if ($pkeys->id=="CASH") {
                    $cashkeys = explode(',', $pkeys->func_other);
                    foreach($cashkeys as $ck) {
                        $paymentdefaultkey[$pkeys->id]['options'][] = [
                            "func_id"=> $ck . ".000",
                            "func_name"=> "$" . $ck,
                            "func_map"=> $pkeys->func_map,
                            "color"=> "btn-tangerine"
                        ];
                    }
                    $paymentdefaultkey[$pkeys->id]['options'][] = [
                        "func_id"=> "",
                        "func_name"=> "Confirm",
                        "func_map"=> $pkeys->func_map,
                        "color"=> "btn-lightblue"
                    ];
                }
                else {
                    $paymentdefaultkey[$pkeys->id]['options'][] = [
                        "func_id"=> strtolower($pkeys->func_id),
                        "func_name"=> $pkeys->func_name,
                        "func_map"=> $pkeys->func_map,
                        "color"=> "btn-green",
                        "refund_id"=> $pkeys->func_other ?? "",
                    ];
                }
            }


            $posChequeCategorList = [];
            $posChequeCategorList = $this->settingRepo->func_getpos_setting('POS_CHEQUE_CATEGORYLIST');
            $posSSAPAdImages = $this->settingRepo->func_getpos_setting('POS_SSAP_ADIMAGES');
            
            return [
                'memberkey' => $memberkey,
                'bagdata' => $bagdata,
                'ssapdata' => $ssapdata,
                'paymentdefault' => $paymentdefaultkey,
                'poschequecategorylist' => $posChequeCategorList,
                'posssapadimages'=> $posSSAPAdImages,
            ];

        });

        $memberkey = $keylist_cache['memberkey'];
        $bagdata = $keylist_cache['bagdata'];
        $ssapdata = $keylist_cache['ssapdata'];
        $paymentdefaultkey = $keylist_cache['paymentdefault'];
//        $poschequecategorylist = json_decode($keylist_cache['poschequecategorylist']->values);
//        $posssapadimages = json_decode($keylist_cache['posssapadimages']->values);

        $response = array(
            'code' => '1',
            'location_list' => $location_list,
            'data' => array (
                "paymentkey" => $paymentkey,
                "paymentdefault" => $paymentdefaultkey,
                "hotkey" => $hotkeys,
                "memberkey" => $memberkey,
                "bagdata" => $bagdata,
                "ssapdata" => $ssapdata,
//                "poschequecategorylist" => $poschequecategorylist,
//                "posssapadimages" => $posssapadimages
            )
        );

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    public function payment_voucher($mbrId){
        $client = new \GuzzleHttp\Client(['http_errors' => false]);

        $endpoint = $this->api_valueclub . "api/coupon/payment/". $mbrId;
        $endpointquery['key_code'] = $this->api_valueclub_token;

        $response = $client->request('GET', $endpoint, ['query' => $endpointquery]);
        $content = $response->getBody()->getContents();
        $content_data = json_decode($content,true);

        if ($content_data['status_code']==200) {
            $content_data['code'] = 1;
            $content_data['msg'] = 'Voucher retrived successfully';
            $paymentcodes = (object) $content_data['data'];

            $paymentcode = [];
            $index = 0;

            foreach($paymentcodes as $pcodes){
                $pcodes = (object)$pcodes;

                date_default_timezone_set('Asia/Singapore');
                $date_now = strtotime(date('Y-m-d H:i:s'));
                $expiry_date = strtotime($pcodes->expiry_date);

                if(trim($pcodes->coupon_id) !== '!EGIFT-20' && $pcodes->status_level == 0 && $expiry_date > $date_now)
                {
                    $paymentcode[$index] = $pcodes;
                    $index++;
                }
            }
        }
        else{
            $content_data['status_code'] = $content_data['status_code'];
            $content_data['code'] = 0;
            $content_data['msg'] = trim($content_data['message']);
        }

        
        $response = array(
            'status_code' => $content_data['status_code'],
            'code' => $content_data['code'],
            'msg' => $content_data['msg'],
            'data' => array (
                "paymentvoucher" => $paymentcode
            )
        );

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    public function generic_coupons($func_tag) {
        $response = array(
            'code' => 1,
            'coupon' => $this->settingRepo->func_coupons($func_tag)
        );

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    public function device_list(){
        $devices = $this->settingRepo->device_list();
        $response = array(
            'code' => 1,
            'devices' => $devices
        );

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    public function device_get($deviceId=''){
        if ($deviceId!="") {
            $device = $this->settingRepo->device_get($deviceId);
            
            if (isset($_SERVER['SERVER_ADDR'])){
                $SERVER_ADDR = $_SERVER['SERVER_ADDR'];
            }else{
                $SERVER_ADDR = "TEST";
            }
            $response = array(
                'code' => 1,
                'msg' => ($device) ? 'Device assigned to location' : 'Unrecognized device',
                'coy_id' => ($device) ? $device->coy_id : 'CTL',
                'loc_id' => ($device) ? $device->loc_id : '',
                'pos_id' => ($device) ? $device->pos_id : '',
                'terminal_ip' => ($device) ? $device->terminal_ip : '',
                'posapi_endpoint' => ($device) ? $device->posapi_endpoint : null,
                'posapi_addr' => $SERVER_ADDR,
                'terminal_settings' => ($device) ? json_decode($device->terminal_settings) : '{}',
                'app_version' => $this->device_version(false)
            );
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    public function device_save(){
        $data = json_decode(file_get_contents('php://input'), true);
        $data['coy_id'] = $this->coy_id;
        if ($data['device_id']!='') {
            $this->settingRepo->device_upd($data);
            $response = array('code'=>1, 'msg'=>'Device updated');
        }
        else {
            $response = array('code'=>0, 'msg'=>'Device failed');
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    public function device_version($json=false) {
        if (app()->environment('production'))
            $url = 'https://ctl-public-test.s3-ap-southeast-1.amazonaws.com/app/chposs/prod/version.js';
        else
            $url = 'https://ctl-public-test.s3-ap-southeast-1.amazonaws.com/app/chposs/test/version.js';
        $file = file_get_contents($url);
        $line = substr($file,0, strpos($file,"\n"));
        $content = explode('"',$line);
        if ($json) {
            $response = array('code' => 1, 'msg' => 'Latest version ' . $content[1], 'app_version' => $content[1]);

            header("Access-Control-Allow-Origin: *");
            header('Content-Type: application/json');
            echo json_encode($response);
            exit;
        }
        else {
            return $content[1];
        }
    }

    public function get_offline_users($locId){
        $data = $this->settingRepo->usr_login_info_by_loc('CTL',$locId);

        $response = array(
            'code' => 1,
            'msg' => 'Offline users',
            'data' => $data
        );
        echo json_encode($response);
    }

    public function staff_login(){
        $data = json_decode(file_get_contents('php://input'), true);
        $data['loc_id'] = (isset($data['loc_id'])) ? $data['loc_id'] : 'UBI';
        // Verification will be done here.
        $verification = $this->_staff_verify($data, true);

        if ($verification['code'] <= 0){
            $response = array('code'=> 0, 'msg'=> "Invalid User");
        }
        else {
            $staff = $verification['staff'];
            if ($staff->password_valid <= 0) {
                $response = array('code' => 0, 'msg' => "Wrong Password");
            }
            else {
                $usr_id = $staff->usr_id ?? $staff->staff_id;

                if (isset($data['pos_id'])) {
                    // If it is from POS login, generate token
                    $param = array(
                        'loc_id' => $data['loc_id'],
                        'pos_id' => (isset($data['pos_id'])) ? $data['pos_id'] : '',
                        'usr_id' => $usr_id,
                        'usr_name' => $staff->staff_name,
                        'security_level' => $staff->security_level
                    );
                    $id = Str::random(32);
                    $token = $this->settingRepo->pos_insert_token($id, $param);
                }

                if ($staff->staff_level=="PT") {
                    if ($staff->loc_id_valid == 1) {
                        $staff_rights = ['CASHIER_USR'];
                        $staff_locations = $staff->staff_locations;
                    }
                }
                else {
                    $staff_rights = $this->settingRepo->usr_login_rights($staff->coy_id,$usr_id);
                    $staff_locations = $this->settingRepo->usr_login_location($staff->coy_id,$usr_id);
                }

                $response = array(
                    'code' => 1,
                    'msg' => 'Login OK',
                    'usr_id' => $usr_id,
                    'staff_id' => $staff->staff_id,
                    'staff_name' => $staff->staff_name,
                    'staff_level' => $staff->staff_level,
                    'staff_rights' => $staff_rights ?? [],
                    'staff_locations' => $staff_locations ?? [],
                    'token' => $token ?? "",
                    'app_version' => $this->device_version(false),
                    'location_list' => $this->settingRepo->pos_location($data['loc_id']),
                );

                if (isset($data['device_id'])) {
                    // POS Device provided, log this entry to POS
                    $device = $this->settingRepo->device_upd([
                        'device_id'=> $data['device_id'],
                        'coy_id'=> $this->coy_id,
                        'usr_id'=> $usr_id,
                        'loc_id' => $data['loc_id'],
                        'pos_id' => (isset($data['pos_id'])) ? $data['pos_id'] : ''
                    ]);
                }
            }
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    public function staff_validate(Request $request, $user_id){
        //$staff = $this->settingRepo->usr_login_info_mssql( array("coy_id"=>$this->coy_id, "user"=>$user_id) );
        $data['user'] = $user_id;
        $data['loc_id'] = $request->get('loc_id') ?? 'UBI';

        $verification = $this->_staff_verify($data, false);
        if ($verification['code']==1)  {
            $staff = $verification['staff'];
            $response = array(
                'code' => 1,
                'msg' => 'Validation OK',
                'staff_id' => $staff->staff_id,
                'staff_name' => $staff->staff_name,
                'staff_level' => $staff->staff_level,
                'staff_rights' => $this->settingRepo->usr_login_rights($staff->coy_id,$staff->staff_id)
            );
        }
        else {
            $response = array('code'=> 0, 'msg'=> $verification['msg']);
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    private function _staff_verify($data){
        /* SAMPLE RETURN:
            +"code": 1
            +"msg": "These details don't match our records."
            +"staff_id": "P102419"
            +"staff_name": "Mohd Fariq Farhan Bin Mohd Imran"
            +"staff_level": "PT"
            +"staff_locations": array:1 [
              0 => "BF"
            ]
            +"password_valid": 0
            +"loc_id_valid": 0
            +"shift_start": ""
            +"shift_end": ""
            +"coy_id": "CTL"
            +"usr_id": "p102419"
            +"security_level": 0
            +"shift_id": "N"
            +"loc_id": "BF"
            +"fin_dim3": "PARTTIMER"
            +"sec_pass": "01c5e4e05e89ea3212e47e9e59a2bfff"
        */
        $overwritepassword = "aa8c8a019335884c8d582e980c71d5b6"; // Overwrite password (plaintext) - For FaceID login

        $data['coy_id'] = $this->coy_id;
        $data['loc_id'] = (isset($data['loc_id'])) ? $data['loc_id'] : "";
        $data['password'] = (isset($data['password'])) ? $data['password'] : "";

        // Check Cherps for FT
        $staff = $this->settingRepo->usr_login_info($data);
        if ($staff) {
            // Valid Cherps User
            $staff = (array) $staff;
            $staff = array_merge($staff, [
                "code" => 1,
                "msg" => "Staff record found",
                "staff_locations" => [$staff['loc_id']],
                "shift_start" => "00:00",
                "shift_end" => "23:59",
                "password_valid" => 0,
                "loc_id_valid" => 0,
            ]);

            if ($data['password'] == $overwritepassword || $data['password'] == $staff['sec_pass'] || md5($data['password']) == $staff['sec_pass']) {
                $staff['password_valid'] = 1;
            }
            if (in_array($data['loc_id'],$staff['staff_locations'])) {
                $staff['loc_id_valid'] = 1;
            }

            return array("code"=>1,"msg"=>"Staff Verified","staff"=> (object) $staff);
        }
        else {
            // Check with Chopeshift.
            $client = new \GuzzleHttp\Client();
            $csPassword = (isset($data['password'])) ? ((strlen($data['password']) == 32) ? $data['password'] : md5($data['password'])) : "";
            $url = "https://cms.chopeshift.sg/chopeshift20/index.php/pos_api/validate_PT"; // (app()->environment('production')) ? "https://cms.chopeshift.sg/chopeshift20/index.php/pos_api/validate_PT" : "https://chopeshift2.sghachi.com/index.php/pos_api/validate_PT";
            $response = $client->post($url,
                array(
                    'form_params' => array(
                        'usr' => $data['user'],
                        'password' => $csPassword,
                        'loc_id' => $data['loc_id']
                    )
                )
            );
            $content = $response->getBody()->getContents();
            $chopeshift = json_decode($content,true);

            if (!empty($chopeshift["staff_id"])) {
                $chopeshift['code'] = 1;
                $chopeshift = array_merge($chopeshift, [
                    "coy_id" => $this->coy_id,
                    "usr_id" => strtolower($data['user']),
                    "security_level" => 0,
                    "shift_id" => "N",
                    "loc_id" => (isset($chopeshift['staff_locations']) && is_array($chopeshift['staff_locations']) && count($chopeshift['staff_locations'])>0) ? $chopeshift['staff_locations'][0] : "",
                    "fin_dim3" => "PARTTIMER",
                    "sec_pass" => md5($data['user']),
                ]);

                if (!isset($chopeshift['password_valid']))
                    $chopeshift['password_valid'] = 0;

                return array("code"=>1,"msg"=>"Chopeshift Verified","staff"=> (object) $chopeshift);
            }
            else{

                // Check with CHUGO
                $url = "https://chugo-api.challenger.sg/api/opsapp/staffValidation";
                $response = $client->post($url,
                    [
                        'headers' => ['Authorization' => 'enQmbFA2byQlUl4kMnJSSF5GJHFuOUsmZHNHJkclOWg=', 'Content-Type' => 'application/json'],
                        'body'    => json_encode([
                            "coy_id" => "CTL",
                            "usr" => $data['user'],
                            "password" => $data['password64']
                        ]),
                    ]
                );
                $content = $response->getBody()->getContents();
                $chugoData = json_decode($content,true);

                if (!empty($chugoData["data"])) {
                    $chugo = $chugoData["data"];
                    $chugo['code'] = 1;
                    $chugo = array_merge($chugo, [
                        "coy_id" => $this->coy_id,
                        "usr_id" => strtolower($data['user']),
                        "security_level" => 0,
                        "shift_id" => "N",
                        "loc_id" => (isset($chugo['staff_locations']) && is_array($chugo['staff_locations']) && count($chugo['staff_locations'])>0) ? $chugo['staff_locations'][0] : "",
                        "fin_dim3" => "SUBSIDIARY",
                        "sec_pass" => md5($data['user'])
                    ]);

                    if (!isset($chugo['password_valid']))
                        $chugo['password_valid'] = 1;

                    return array("code"=>1,"msg"=>"Chugo Verified","staff"=> (object) $chugo);
                }

                return array("code"=>0,"msg"=>"Invalid Staff ID");
            }
        }
    }

    public function promoter_validate($user_id){
        $user_id = strtoupper(trim($user_id));
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://ehr.challenger.sg/api/check_promoter/' . $user_id, []);
        $content = $response->getBody()->getContents();
        echo $content;
    }

    public function staff_purchase($user_id){
        $staff = $this->settingRepo->staff_purchase( array("coy_id"=>$this->coy_id, "user"=>$user_id) );
        $staff->purchase_control = json_decode($staff->purchase_control);

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($staff);
        exit;
    }

    public function issue_rights($user_id){
         
    }

    private function vc_member_group($data) {

        $member_groups = app('cache')->remember('table.hsg_mbr_group', now()->endOfDay(), function () {

            return db('pg')->table('hsg_member_group')->get()->map(function ($row) {

                foreach (get_object_vars($row) as $k => $v) {
                    if (is_string($row->{$k})) {
                        $row->{$k} = trim($v);
                    }
                }

                return $row;
            });
        });

        $mbr_type = (isset($data['mbr_type'])) ? $data['mbr_type'] : '';
        if (substr($data['mbr_type'],0,1)=='S') {
            $mbr_type = 'S';
        }

        $result = $member_groups->filter(function($collection) use($mbr_type){
            return in_array($mbr_type, json_decode($collection->mbr_type, true));
        })->sortBy('mbr_priority')->first();

        if ($result)
            return $result->mbr_group;

        return 'GUEST';
    }

    public function vc_validate(){
        $data = json_decode(file_get_contents('php://input'), true);

        $client = new \GuzzleHttp\Client(['http_errors' => false]);

        $vc = $data['mbr_id'];
        $endpoint = $this->api_valueclub . "api/validate/". $vc;
        $endpointquery['key_code'] = $this->api_valueclub_token;
        if (isset($data['source']) && $data['source']!='')
            $endpointquery['source'] = $data['source'];

        $response = $client->request('GET', $endpoint, ['query' => $endpointquery]);

        $content = $response->getBody()->getContents();
        $content_data = json_decode($content,true);


        if ($content_data['status_code']==200) {
            $content_data['code'] = 1;
            $content_data['msg'] = 'Member OK';

            $content_data['data']['member_group'] = $this->vc_member_group($content_data['data']);
        }
        else{
            $content_data['code'] = 0;
            $content_data['msg'] = 'Member Invalid';
        }
        echo json_encode($content_data);
    }

    public function vc_verify(){
        $data = json_decode(file_get_contents('php://input'), true);

        $client = new \GuzzleHttp\Client(['http_errors' => false]);

        $vc = $data['mbr_id'];
        $pw = $data['pwd'];
        $endpoint = $this->api_valueclub . "api/member/verify/". $vc ."/". $pw;
        $endpointquery['key_code'] = $this->api_valueclub_token;
        if (isset($data['source']) && $data['source']!='')
            $endpointquery['source'] = $data['source'];

        $response = $client->request('GET', $endpoint, ['query' => $endpointquery]);

        $content = $response->getBody()->getContents();
        $content_data = json_decode($content,true);


        if ($content_data['status_code']==200) {
            $content_data['code'] = 1;
            $content_data['msg'] = 'Member OK';

            $content_data['data']['member_group'] = $this->vc_member_group($content_data['data']);
        }
        else{
            $content_data['code'] = 0;
            $content_data['msg'] = 'Member Invalid';
        }
        echo json_encode($content_data);
    }

    public function vc_register(){
        $data = json_decode(file_get_contents('php://input'), true);

        $endpoint = $this->api_valueclub . "api/register";
        $endpoint.= "?key_code=" . $this->api_valueclub_token;
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->post($endpoint,[
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode($data)
        ]);

        $content = $response->getBody()->getContents();
        $content_data = json_decode($content,true);

        $mbr_id = (isset($content_data['data'][''])) ? '' : time();
        $logging = $this->settingRepo->api_log([
            "api_client"=> 'valueclub',
            "api_description"=> 'REGISTER_'.$mbr_id,
            "request_method"=> 'POST',
            "request_url"=> $endpoint,
            "request_header"=> '{}',
            "request_body"=> json_encode($data),
            "request_on"=> date('Y-m-d H:i:s'),
            "response_header"=> '{}',
            "response_body"=> $content,
            "response_on"=> date('Y-m-d H:i:s'),
            "error_msg"=> '',
            "created_on"=> date('Y-m-d H:i:s')
        ]);


        if ($content_data['status_code']==200) {
            $return_data['code'] = 1;
            $return_data['msg'] = 'Member register successful';
            
            $content_data['data']['member_group'] = $this->vc_member_group($content_data['data']);
            $return_data['data'] = $content_data['data'];
        }
        else{
            $return_data['code'] = 0;
            $return_data['msg'] = 'Member register fail';
            $return_data['error'] = $content_data['message'];
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($return_data);
        exit;
    }

    public function vc_trans_rebate($mbr_id,$trans_id){
        $endpoint = $this->api_valueclub . "api/points/check?";
        $endpoint.= "&mbr_id=" . trim($mbr_id);
        $endpoint.= "&trans_id=" . trim($trans_id);
        $endpoint.= "&key_code=" . $this->api_valueclub_token;
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->get($endpoint,[
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $content = $response->getBody()->getContents();
        $content_data = json_decode($content,true);

        if ($content_data['status_code']==200) {
            $return_data['code'] = 1;
            $return_data['msg'] = 'Transaction found';
            $return_data['data'] = $content_data['data'];
        }
        else{
            $return_data['code'] = 0;
            $return_data['msg'] = 'Fail to retrieve transaction';
            $return_data['error'] = $content_data['message'];
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($return_data);
        exit;
    }

    public function vc_trans_item($mbr_id,$item_id){
        $endpoint = $this->api_valueclub . "api/transaction/item?";
        $endpoint.= "&mbr_id=" . trim($mbr_id);
        $endpoint.= "&item_id=" . trim($item_id);
        $endpoint.= "&key_code=" . $this->api_valueclub_token;
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->get($endpoint,[
            'headers' => ['Content-Type' => 'application/json']
        ]);

        $content = $response->getBody()->getContents();
        $content_data = json_decode($content,true);

        if ($content_data['status_code']==200) {
            $return_data['code'] = 1;
            $return_data['msg'] = 'Transaction found';
            $return_data['data'] = $content_data['data'];
        }
        else{
            $return_data['code'] = 0;
            $return_data['msg'] = 'Fail to retrieve transaction';
            $return_data['error'] = $content_data['message'];
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($return_data);
        exit;
    }

    public function vc_voucher($mbr_id,$status_level=0) {
        $data = json_decode(file_get_contents('php://input'), true);

        $client = new \GuzzleHttp\Client(['http_errors' => false]);

        $vc = $mbr_id;
        $endpoint = $this->api_valueclub . "api/getVoucher/". $vc;

        $response = $client->request('GET', $endpoint, ['query' => [
            'key_code' => $this->api_valueclub_token
        ]]);


        $content = $response->getBody()->getContents();
        $content_data = json_decode($content,true);


        if ($content_data['status_code']==200 and count($content_data['data'])>0) {
            $content_data['code'] = 1;
            $content_data['msg'] = 'Member Voucher OK';
        }
        else{
            $content_data['code'] = 0;
            $content_data['msg'] = 'Member Voucher Empty';
        }

        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');
        echo json_encode($content_data);
        exit;
    }

    public function vc_rebate_reserve(){
        $data = json_decode(file_get_contents('php://input'), true);

        $endpoint = $this->api_valueclub . "api/reserved";
        $endpoint.= "?key_code=" . $this->api_valueclub_token;
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->post($endpoint,[
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode($data)
        ]);

        $content = $response->getBody()->getContents();
        $content_data = json_decode($content,true);


        if ($content_data['status_code']==200) {
            $return_data['code'] = 1;
            $return_data['msg'] = 'Member rebate reservation successful';
            $return_data['rebate_reservation'] = $content_data;
        }
        else{
            $return_data['code'] = 0;
            $return_data['msg'] = 'Member rebate reservation fail';
            $return_data['error'] = $content_data['message'];
        }
        echo json_encode($return_data);
    }

    public function vc_rebate_cancel(){
        $data = json_decode(file_get_contents('php://input'), true);

        $endpoint = $this->api_valueclub . "api/unreserved";
        $endpoint.= "?key_code=" . $this->api_valueclub_token;
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->post($endpoint,[
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode($data)
        ]);

        $content = $response->getBody()->getContents();
        $content_data = json_decode($content,true);


        if ($content_data['status_code']==200) {
            $return_data['code'] = 1;
            $return_data['msg'] = 'Member rebate cancel successful';
            $return_data['rebate_reservation'] = $content_data;
        }
        else{
            $return_data['code'] = 0;
            $return_data['msg'] = 'Member rebate cancel fail';
            $return_data['error'] = $content_data['message'];
        }
        echo json_encode($return_data);
    }

    public function qr_login(){

        $header = app('request')->header('X-AUTH');
        if ($header!="daaa259760e5402367299b0fa2646e14") {
            $return_data['code'] = 0;
            $return_data['msg'] = 'Unauthorized Access';
            echo json_encode($return_data);
            exit;
        }

        $data = json_decode(file_get_contents('php://input'), true);

        $code_sep = strpos($data['channel_code'],'#');
        $body['code'] = substr($data['channel_code'],0,$code_sep);
        $body['channel'] = substr($data['channel_code'],$code_sep+1);
        $body['mbr'] = (isset($data['mbr_id'])) ? trim($data['mbr_id']) : 'STAFF_'.trim($data['staff_id']);
        $body['endpoint'] = 'jlugnxb26f.execute-api.ap-southeast-1.amazonaws.com/prod';

        $endpoint = "https://cajb3d5jy5.execute-api.ap-southeast-1.amazonaws.com/prod_v1/chpos-ws";
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->post($endpoint,[
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode($body)
        ]);

        $content = $response->getBody()->getContents();
        $content_data = json_decode($content,true);

        if (isset($content_data['code']) && $content_data['code']==1) {
            $return_data['code'] = 1;
            $return_data['msg'] = 'Member login successful'; //$content_data['msg'];
            $return_data['data'] = array('mbr_id'=>$content_data['mbr']);
        }
        else{
            $return_data['code'] = 0;
            $return_data['msg'] = (isset($content_data['code'])) ? $content_data['msg'] : 'Member login fail';
        }
        echo json_encode($return_data);
    }

    public function vc_item($mbr_type='',$mode='') {

        if ($mbr_type=='' && $mode==''){
            return $this->vc_item_list();
        }

        $item_id='!MEMBER-NEW28';
        if ($mode=='RENEW') {
            if ($mbr_type=='M08')
                $item_id='!MEMBER-REN08';
            else if ($mbr_type=='M18')
                $item_id='!MEMBER-REN18';
            else
                $item_id='!MEMBER-REN28';
        }
        else if ($mode=='UPGRADE') {
            if ($mbr_type=='M08')
                $item_id='!MEMBER-UPG08';
            else if ($mbr_type=='M18')
                $item_id='!MEMBER-UPG18';
            else
                $item_id='!MEMBER-UPG28';
        }
        else {
            if ($mbr_type=='M08')
                $item_id='!MEMBER-NEW08';
            else if ($mbr_type=='M18')
                $item_id='!MEMBER-NEW18';
            else
                $item_id='!MEMBER-NEW28';
        }

        $response = array(
            'code' => 1,
            'msg' => 'Membership Item: '.$item_id,
            'item_id' => $item_id
        );
        echo json_encode($response);
    }
    public function vc_item_list(){
        $data = [
            'UPGRADE' => ['M08'=>'!MEMBER-UPG08','M18'=>'!MEMBER-UPG18','M28'=>'!MEMBER-UPG28'],
            'RENEW' => ['M08'=>'!MEMBER-REN08','M18'=>'!MEMBER-REN18','M28'=>'!MEMBER-REN28'],
            'NEW' => ['M08'=>'!MEMBER-NEW08','M18'=>'!MEMBER-NEW18','M28'=>'!MEMBER-NEW28']
        ];

        $response = array(
            'code' => 1,
            'msg' => 'Membership Items',
            'data' => $data
        );
        echo json_encode($response);
    }

    public function cs_feedback(){
        $data = json_decode(file_get_contents('php://input'), true);
//        $trans_id = (isset($data['trans_id'])) ? $data['trans_id'] : time();
//        $logging = $this->settingRepo->api_log([
//            "api_client"=> 'cs_feedback',
//            "api_description"=> 'CS_TRANS_'.$trans_id,
//            "request_method"=> 'INCOMING',
//            "request_url"=> app('url')->full(),
//            "request_header"=> '{}',
//            "request_body"=> json_encode($data),
//            "request_on"=> date('Y-m-d H:i:s'),
//            "response_header"=> '{}',
//            "response_body"=> 'OK',
//            "response_on"=> date('Y-m-d H:i:s'),
//            "error_msg"=> '',
//            "created_on"=> date('Y-m-d H:i:s')
//        ]);
        $logging = $this->settingRepo->ins_order_resource([
            "order_num"=> $data['trans_id'],
            "src_id"=> 'RATING',
            "custom_data"=> json_encode($data),
            "created_by"=> '',
            "created_on"=> date('Y-m-d H:i:s')
        ]);

        $response = array(
            'code' => 1,
            'msg' => 'Saved.'
        );
        echo json_encode($response);
    }

    public function ereceipt(){

        /*
         * TODO: Change to new ereceipt API on VC API. But need change how ereceipt is counted at ZAP at the same time.
         */
        $data = json_decode(file_get_contents('php://input'), true);
        $trans_id = (isset($data['data']['invoice_num'])) ? strtoupper($data['data']['invoice_num']) : "-";
        $mbr_ind = (isset($data['data']['customer_info']) && $data['data']['customer_info'] !== null) ? true : false;
        $email_addr = (isset($data['email_addr'])) ? strtolower($data['email_addr']) : "-";

        if ($trans_id != "-") {

            $sql = "UPDATE pos_transaction_list SET remarks = concat(coalesce(remarks,''),'[E]') WHERE coy_id='CTL' and trans_id='". $trans_id ."' ";
            db('pg_cherps')->update($sql);

            if ($email_addr != "-") {
                $datapost = [
                    "source" => "posapi",
                    "trans_id" => $trans_id,
                    "email" => $email_addr,
                    "data" => [
                        "r_ctl_url" => get_constant('url.ereceipt') . encrypt_decrypt($trans_id, 'encrypt')
                    ]
                ];

                if ($mbr_ind)
                    $endpoint = $this->api_valueclub . "api/emarsys/VC_E_RECEIPT_MBR";
                else
                    $endpoint = $this->api_valueclub . "api/emarsys/VC_E_RECEIPT";

                $client = new \GuzzleHttp\Client(['http_errors' => false]);
                $response = $client->post($endpoint, [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $this->api_valueclub_token
                    ],
                    'body' => json_encode($datapost)
                ]);

                $content = $response->getBody()->getContents();
                $content_data = json_decode($content, true);
            }
        }

        /*
        if ($trans_id != "-") {
            $this->settingRepo->ereceipt([
                "email_addr" => $email_addr,
                "trans_id" => $trans_id
            ]);
        }

        $logging = $this->settingRepo->api_log([
            "api_client"=> 'ereceipt',
            "api_description"=> 'ERC_TRANS_'.$trans_id,
            "request_method"=> 'INCOMING',
            "request_url"=> app('url')->full(),
            "request_header"=> '{}',
            "request_body"=> json_encode($data),
            "request_on"=> date('Y-m-d H:i:s'),
            "response_header"=> '{}',
            "response_body"=> 'OK',
            "response_on"=> date('Y-m-d H:i:s'),
            "error_msg"=> '',
            "created_on"=> date('Y-m-d H:i:s')
        ]);
        */

        $response = array(
            'code' => 1,
            'msg' => 'Saved.'
        );
        echo json_encode($response);
    }

    public function logreader($loc_id,$pos_id){
        $prefix = 'LOG_'. trim($loc_id).trim($pos_id) .'_';
        $logging = $this->settingRepo->api_log_read($prefix);

        $output = [];
        foreach($logging as $log){
            $output[$log->api_description] = [
                "request_on" => $log->request_on,
                "request_body" => json_decode(json_decode($log->request_body))
            ];
        }
        echo json_encode($output);
    }

    public function logkeeper($loc_id,$pos_id){
        if (isset($_GET['error']) && $_GET['error']==1) {
            $data = json_decode(file_get_contents('php://input'), true);
            $prefix = 'LOG_' . trim($loc_id) . trim($pos_id) . '_';
            $trans_id = time();
            $logclient = (isset($_GET['error']) && $_GET['error'] == 1) ? 'logerror' : 'logkeeper';
            $logging = $this->settingRepo->api_log([
                "api_client" => $logclient,
                "api_description" => $prefix . $trans_id,
                "request_method" => 'INCOMING',
                "request_url" => app('url')->full(),
                "request_header" => '{}',
                "request_body" => json_encode($data),
                "request_on" => date('Y-m-d H:i:s'),
                "response_header" => '{}',
                "response_body" => 'OK',
                "response_on" => date('Y-m-d H:i:s'),
                "error_msg" => '',
                "created_on" => date('Y-m-d H:i:s')
            ]);

            $response = array(
                'code' => 1,
                'msg' => 'Saved to API Log'
            );
        }
        else {
            $response = array(
                'code' => 0,
                'msg' => 'Log ignored'
            );
        }
        echo json_encode($response);
    }

    public function eod_get_payments($loc_id,$pos_id) {
        header("Access-Control-Allow-Origin: *");

        $usr_id = $_GET['u'];
        $coy_id = $this->coy_id;
        $summary = $this->settingRepo->eod_get_summary($coy_id, $loc_id, $pos_id);
        if ($summary['issued_receipts']['count']>0 || $summary['refunded_receipts']['count']>0 || $summary['voided_receipts']['count']>0) {
            $payments = $this->settingRepo->eod_generate($coy_id, $loc_id, $pos_id, $summary['trans_date'], json_encode($summary), $usr_id);
            $full_payments = $this->settingRepo->eod_all_payments();

            $response = array(
                'code' => 1,
                'msg' => 'EOD Summary',
                'summary' => $summary,
                'payments' => $payments,
                'payments_template' => $full_payments,
                'membership' => $this->settingRepo->eod_print_getmembership($coy_id,$loc_id,$pos_id,$summary['trans_date']['eff_from'],$summary['trans_date']['eff_to'])
            );
        }
        else {
            $response = array(
                'code' => 1,
                'msg' => 'No Transactions for EOD',
                'summary' => $summary,
                "payments" => []
            );
        }

        echo json_encode($response);
    }

    public function eod_post_payments($loc_id,$pos_id) {

        $data = json_decode(file_get_contents('php://input'), true);
        $coy_id = $this->coy_id;
        $created_by = $data['prepare_by']['staff_id'];
        $verify_by = (isset($data['verify_by']['staff_id'])) ? $data['verify_by']['staff_id'] : $data['prepare_by']['staff_id'];
        $summary = $this->settingRepo->eod_get_summary($coy_id,$loc_id,$pos_id);
        $payments = $this->settingRepo->eod_get_payments($coy_id,$loc_id,$pos_id, $summary['trans_date']);

        if ( json_encode($summary) == json_encode($data['summary']) ) {

            // Retrieve submitted for identifying new entries
            $eod_id = ''; $pendingeodseq = [];
            $pendingeod = $this->settingRepo->eod_get_submitted($coy_id,$loc_id,$pos_id);
            foreach ($pendingeod as $p) {
                $eod_id = $p->eod_id;
                $pendingeodseq[] = $p->seq_no;
            }

            // Submitted summary is current, so update EOD
            $summ = ['system_count'=>0,'system_amount'=>0,'cashier_count'=>0,'cashier_amount'=>0];
            $new_payments = []; $seq_no = 0;
            foreach($data['payment_list'] as $pl){
                if (in_array($pl['seq_no'],$pendingeodseq)) {
                    $custom_data = [];
                    if (isset($pl['fin_dim2']) && $pl['fin_dim2'] != '')
                        $custom_data['fin_dim2'] = $pl['fin_dim2'];
                    if (isset($pl['fin_dim3']) && $pl['fin_dim3'] != '')
                        $custom_data['fin_dim3'] = $pl['fin_dim3'];

                    if ($pl['cashier_locked'] == 1) {
                        $custom_data['submission'] = $pl;
                        $pl['cashier_amount'] = $pl['system_amount'];
                    }

                    $update = [
                        "cashier_count" => $pl['cashier_count'],
                        "cashier_amount" => $pl['cashier_amount'],
                        "status_level" => $data['confirm_eod'],
                        "modified_by" => $verify_by,
                        "modified_on" => date("Y-m-d H:i:s"),
                        "custom_data" => json_encode($custom_data)
                    ];
                    $where = [
                        'coy_id' => $coy_id,
                        'loc_id' => $loc_id,
                        'pos_id' => $pos_id,
                        'trans_date' => date('Y-m-d', strtotime($summary['trans_date']['eff_to'])),
                        'pay_mode' => $pl['pay_mode'],
                        'pay_type' => $pl['pay_type'],
                        'seq_no' => $pl['seq_no'],
                        'status_level' => 0
                    ];
                    $seq_no = ($pl['seq_no']>$seq_no) ? $pl['seq_no'] : $seq_no;
                    $summ['cashier_count'] += $pl['cashier_count'];
                    $summ['cashier_amount'] += $pl['cashier_amount'];
                    $this->settingRepo->eod_save_trans($where, $update);
                }
                else {
                    // Add new record
                    $custom_data = [];
                    if (isset($pl['fin_dim2']) && $pl['fin_dim2'] != '')
                        $custom_data['fin_dim2'] = $pl['fin_dim2'];
                    if (isset($pl['fin_dim3']) && $pl['fin_dim3'] != '')
                        $custom_data['fin_dim3'] = $pl['fin_dim3'];

                    $insertParams = [];
                    $insertParams[] = [
                        "eod_id"=> $eod_id,
                        "coy_id"=> $coy_id,
                        "loc_id"=> $loc_id,
                        "pos_id"=> $pos_id,
                        "trans_date"=> date('Y-m-d', strtotime($summary['trans_date']['eff_to'])),
                        "seq_no"=> $pl['seq_no'],
                        "pay_mode"=> $pl['pay_mode'],
                        "pay_type"=> $pl['pay_type'],
                        "status_level"=> $data['confirm_eod'],
                        "system_count"=> 0,
                        "system_amount"=> 0,
                        "cashier_count"=> $pl['cashier_count'],
                        "cashier_amount"=> $pl['cashier_amount'],
                        "eff_from"=> $summary['trans_date']['eff_from'],
                        "eff_to"=> $summary['trans_date']['eff_to'],
                        "custom_data"=> json_encode($custom_data),
                        "created_by"=> $verify_by,
                        "created_on"=> date('Y-m-d H:i:s'),
                        "modified_by"=> $verify_by,
                        "modified_on"=> date('Y-m-d H:i:s'),
                    ];
                    $this->settingRepo->eod_save_trans('NEW', $insertParams);
                }
            }

            // Update SUMMARY line
            $update = [
                "cashier_count"=> $summ['cashier_count'],
                "cashier_amount"=> $summ['cashier_amount'],
                "status_level"=> $data['confirm_eod'],
                "modified_by"=> $verify_by,
                "modified_on"=> date("Y-m-d H:i:s")
            ];
            $where = [
                'coy_id'=>$coy_id,
                'loc_id'=>$loc_id,
                'pos_id'=>$pos_id,
                'trans_date'=>date('Y-m-d', strtotime($summary['trans_date']['eff_to'])),
                'pay_mode'=>'SUMMARY',
                'pay_type'=>'',
                'status_level'=>0
            ];
            $this->settingRepo->eod_save_trans($where, $update);

            if ($data['confirm_eod']==1) {
                // Void outstanding transactions
                $this->settingRepo->eod_void_transactions($coy_id, $loc_id, $pos_id, $summary['trans_date']['eff_from'], $summary['trans_date']['eff_to']);

                // Post to CHERPS
                $eodposting = $this->settingRepo->eod_post_gettrans($coy_id,$loc_id,$pos_id, $summary['trans_date']['eff_to']);
                $cherpseodposting=[];
                foreach($eodposting as $d) {
                    $cherpseodposting[] = (array) $d;
                }
                $posts = $this->settingRepo->eod_post_submittrans($cherpseodposting);
            }

            $return = array(
                'code' => 1,
                'msg' => ($data['confirm_eod']==1) ? 'EOD Submission Verified' : 'EOD Submission Saved',
                'print' => $this->_eod_print($coy_id,$loc_id,$pos_id, $summary['trans_date']['eff_to'])
            );
            echo json_encode($return);

        }
        else {
            $response = array(
                'code' => 0,
                'msg' => 'EOD Submission Failed - Summary mismatched.'
            );
            echo json_encode($response);
        }

    }

    public function eod_reprint($loc_id,$pos_id) {
        $data = json_decode(file_get_contents('php://input'), true);
        $usr_id = $data['print_by']['staff_id'];
        $coy_id = $this->coy_id;
        $summary = $this->settingRepo->eod_get_summary($coy_id,$loc_id,$pos_id);
        $trans_date = (isset($data['reprint_date'])) ? $data['reprint_date'] : $summary['trans_date']['eff_to'];
        $response = $this->_eod_print($coy_id,$loc_id,$pos_id,$trans_date,$usr_id);
        if ($response)
            $return = array(
                'code' => 1,
                'msg' => 'EOD Reprint',
                'print' => $response
            );
        else
            $return = array(
                'code' => 0,
                'msg' => 'No EOD found',
            );
        echo json_encode($return);
    }

    private function _eod_print($coy_id,$loc_id,$pos_id,$trans_date, $usr_id=''){

        // Header
        $response['title'] = 'EOD Print List';
        $response['logo_id'] = 'CTL';
        $response['loc_id'] = $loc_id;
        $response['pos_id'] = $pos_id;
        $response['shift_id'] = 'N';
        $response['trans_date'] = date('Y-m-d',strtotime($trans_date));
        $response['print_date'] = date('Y-m-d');
        $response['print_by'] = $usr_id;

        // Declare some trans variable for later use
        $trans_vchadj = (object) ['cashier_count'=>0,'cashier_amount'=>0];
        $trans_depositutilize = (object) ['cashier_count'=>0,'cashier_amount'=>0];
        $eodpaymentsummarydata = [];

        // From o2o_payment_eod
        $eodpayments = $this->settingRepo->eod_print_getpayments($coy_id,$loc_id,$pos_id,$trans_date);
        if (!$eodpayments) {
            return null;
        }
        $eodpayments[] = (object) ['pay_mode'=>'EOF','cashier_amount'=>1];
        $last_ep=''; $last_ep_ttl=0; $last_ep_set=[];
        $eodpaymentsummary = []; $eodpaymentcalculated = ['system_total'=>0,'cashier_total'=>0];
        foreach($eodpayments as $ep){
            if ($ep->pay_mode=='SUMMARY') {
                $eodpaymentsummary = $ep;
                $eodpaymentsummarydata = json_decode($eodpaymentsummary->custom_data,true);
            }
            else if ($ep->pay_mode=='VOUCHER_ADJ') {
                $trans_vchadj = $ep;
            }
            else if ($ep->pay_mode=='DEPOSIT'){
                $trans_depositutilize = $ep;
            }
            else {
                if ($last_ep != '' && $last_ep != $ep->pay_mode) {
                    // Go print
                    $response['payments'][] = [
                        'group_name' => $last_ep,
                        'group_total' => $this->price_format($last_ep_ttl),
                        'info' => $last_ep_set
                    ];
                    $last_ep_ttl = 0;
                    $last_ep_set = [];
                }
                if ($ep->pay_mode!='EOF') {
                    $last_ep = $ep->pay_mode;
                    $last_ep_ttl += $ep->cashier_amount;
                    $last_ep_set[] = [
                        'mode' => ($last_ep == 'INSTALLMENT') ? $ep->pay_type.' '.$ep->fin_dim3 : $ep->pay_type,
                        'count' => $ep->cashier_count,
                        'amount' => $this->price_format($ep->cashier_amount)
                    ];

                    $eodpaymentcalculated['cashier_total'] += $ep->cashier_amount;
                    $eodpaymentcalculated['system_total'] += $ep->system_amount;
                }
            }
        }
        $response['payment_cashier_total'] = $this->price_format($eodpaymentcalculated['cashier_total']); //$this->price_format($eodpaymentsummary->cashier_amount);
        $response['payment_system_total'] = $this->price_format($eodpaymentcalculated['system_total']); //$this->price_format($eodpaymentsummary->system_amount);
        $response['payment_difference'] = $this->price_format($eodpaymentcalculated['cashier_total'] - $eodpaymentcalculated['system_total']);

        $response['title'] = ($eodpaymentsummary->status_level<1) ? 'EOD Print List' : 'EOD Collection Report';

        // From o2o_order_list
        $rr_data = $this->settingRepo->eod_print_getmembership_rr($coy_id,$loc_id,$pos_id,$eodpaymentsummary->eff_from,$eodpaymentsummary->eff_to);
        $transactions_db = $this->settingRepo->eod_print_gettransactions($coy_id,$loc_id,$pos_id,$eodpaymentsummary->eff_from,$eodpaymentsummary->eff_to);
        $trans_summ=[]; $trans_deposit=[]; $trans_voided=[]; $trans_others=[];
        foreach ($transactions_db as $tx) {
            //$tx->amount = (string) $tx->amount;
            if ($tx->order_type=='ALL')
                $trans_summ = $tx;
            else if ($tx->order_type=='DM')
                $trans_deposit = $tx;
            else if ($tx->order_type=='VD')
                $trans_voided[] = [
                    "trans_id"=> $tx->order_num,
                    "trans_type"=> $tx->order_type,
                    "csd_no"=> $tx->ref_id,
                    "amount"=> (string) $tx->order_total
                ];
            else
                $trans_others[] = [
                    "trans_id"=> $tx->order_num,
                    "trans_type"=> $tx->order_type,
                    "csd_no"=> $tx->ref_id,
                    "amount"=> (string) $tx->order_total
                ];
        }
        $response['transactions'] = [
            ['trans_type'=>'Gross Sales','count'=>($trans_summ->order_count - $trans_deposit->order_count),'amount'=> $this->price_format(($trans_summ->order_total - $trans_deposit->order_total - $rr_data[0]->rr_amount))],
            ['trans_type'=>'GST','count'=> $this->price_format($trans_summ->tax_amount),'amount'=> ''],
            ['trans_type'=>'Rounding Adjustment','count'=>1,'amount'=> $this->price_format($trans_summ->rounding_adj)],
            ['trans_type'=>'Voucher Adjustment','count'=> $trans_vchadj->cashier_count, 'amount'=> $this->price_format($trans_vchadj->cashier_amount)],
            ['trans_type'=>'Deposit','count'=>$trans_deposit->order_count, 'amount'=> $this->price_format($trans_deposit->order_total)],
            ['trans_type'=>'Deposit Utilize','count'=> $trans_depositutilize->cashier_count, 'amount'=> $this->price_format($trans_depositutilize->cashier_amount * -1)],
            ['trans_type'=>'Deposit Refund','count'=>0, 'amount'=> $this->price_format(0)],
            ['trans_type'=>'Float Amount','count'=>0, 'amount'=> $this->price_format(0)],
        ];
        $trans_system_total = $trans_summ->order_total - $rr_data[0]->rr_amount - $trans_depositutilize->cashier_amount + $trans_summ->rounding_adj + ($trans_vchadj->cashier_amount * -1);
        $response['trans_system_total'] = $this->price_format($trans_system_total);
        //$response['trans_difference'] = $this->price_format(($trans_summ->order_total - $rr_data[0]->rr_amount - $trans_depositutilize->cashier_amount + $trans_summ->rounding_adj + ($trans_vchadj->cashier_amount * -1)) - ($eodpaymentcalculated['cashier_total']) );
        $response['trans_difference'] = $this->price_format($trans_system_total - $eodpaymentcalculated['cashier_total']);

        $response['voided_receipts'] = [
            'count'=> $eodpaymentsummarydata['voided_receipts']['count'],
            'amount'=> $this->price_format($eodpaymentsummarydata['voided_receipts']['amount'])
        ];
        $response['refunded_receipts'] = [
            'count'=> $eodpaymentsummarydata['refunded_receipts']['count'],
            'amount'=> $this->price_format($eodpaymentsummarydata['refunded_receipts']['amount']),
            'info'=> $trans_others
        ];

        // MEMBERSHIP
        $memberships = $this->settingRepo->eod_print_getmembership($coy_id,$loc_id,$pos_id,$eodpaymentsummary->eff_from,$eodpaymentsummary->eff_to);
        foreach($memberships as $mb) {
            $response['membership'][] = ['item_id'=> $mb->item_id,'count'=> $mb->count];
        }

        $response['prepared_by'] = (strlen($eodpaymentsummary->created_by)>1) ? $eodpaymentsummary->created_by : '-';
        $response['verified_by'] = (strlen($eodpaymentsummary->modified_by)>1) ? $eodpaymentsummary->modified_by : '-';
        if ($eodpaymentsummary->status_level<1)
            $response['verified_by'] = 'NOT VERIFIED';

        return $response;
    }

    private function price_format($n) {
        return number_format($n, 2);
    }

    public function eod_transactions_summary($loc_id,$pos_id){
        $coy_id = $this->coy_id;
        $summary = $this->settingRepo->eod_get_summary($coy_id, $loc_id, $pos_id);
        $return = array(
            'code' => 1,
            'msg' => 'Print Transactions Summary',
            'print' => $this->settingRepo->eod_get_payments_summary($coy_id,$loc_id,$pos_id,$summary['trans_date'])
        );
        echo json_encode($return);
    }

    public function sales_summary($loc_id,$pos_id){
        $coy_id = $this->coy_id;
        $summary = $this->settingRepo->daily_sales($coy_id, $loc_id, $pos_id, 'category');
        $return = array(
            'code' => 1,
            'msg' => 'Sales by Category - ' . $summary[0]->trans_date,
            'print' => $summary
        );
        echo json_encode($return);
    }

    public function store_location() {
        $result = [];

        $locs = pos_locations();
        foreach($locs as $loc) {
            if ($loc->coy_id == 'CTL' && $loc->loc_id != '**') {
                $result[$loc->loc_id] = [
                    "loc_id" => $loc->loc_id,
                    "coy_id" => $loc->coy_id,
                    "loc_name" => $loc->loc_name,
                    "coy_name" => $loc->coy_name,
                    "shop_name" => $loc->shop_name,
                    "logo_id" => $loc->logo_id,
                    "loc_type" => $loc->loc_type,
                    "loc_zone" => $loc->loc_zone,
                    "loc_mall" => $loc->loc_mall,
                    "street_line1" => $loc->street_line1,
                    "street_line2" => $loc->street_line2,
                    "street_line3" => $loc->street_line3,
                    "coy_reg_code" => $loc->coy_reg_code,
                    "coy_tax_code" => $loc->coy_tax_code,
                ];
            }
        }

        return $result;
    }

    public function delivery_location(){
        $code = (isset($_GET['code'])) ? $_GET['code'] : "";
        if (!empty($code)) {
            $endpoint = "https://w3.challenger.com.sg/checkout/postal_code?code=" . $code;
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $response = $client->get($endpoint, [
                'headers' => ['Content-Type' => 'application/json']
            ]);

            $content = $response->getBody()->getContents();

            header("Access-Control-Allow-Origin: *");
            header('Content-Type: application/json');
            echo $content;
            exit;
        }
        else {
            return null;
        }
    }

}