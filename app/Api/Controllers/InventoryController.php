<?php
/*
namespace App\Api\Controllers;


use App\Repositories\InventoryRepository;

class InventoryController extends Controller
{
    protected $inventoryRepo;

    public function __construct(InventoryRepository $inventoryRepo)
    {
        $this->inventoryRepo = $inventoryRepo;
    }

    public function index($type = '')
    {
       // $r = $this->self('0008562006461      ');

        $r = $this->location('0008562006461',['BF','TP','WP']);
        dd($r->toArray());
    }

    public function express($itemId)
    {
        $balance = $this->inventoryRepo->getItemTotalBalanceQty($itemId, ['BF', 'HCL']);

        $hasStock = ($balance > 0);

        $result = [
            'delivery_code' => 'XPD',
            'delivery_name' => 'Express Delivery',
            'item_id'       => $itemId,
            'has_stock'     => $hasStock,
            'qty_balance'   => $balance,
        ];

        return collect($result);
    }

    public function standard($itemId)
    {
        $balance = $this->inventoryRepo->getItemTotalBalanceQty($itemId, '', true);

        $hasStock = ($balance > 0);

        $result = [
            'delivery_code' => 'STD',
            'delivery_name' => 'Standard Delivery',
            'item_id'       => $itemId,
            'has_stock'     => $hasStock,
            'qty_balance'   => $balance,
        ];

        return collect($result);
    }

    public function self($itemId)
    {
        $results = db('sql')->table('o2o_live_outlet')->where('SCL', 1)->get(['loc_id', 'loc_name AS location_name']);

        $results = $results->map(function ($item) use ($itemId) {
            //$item->delivery_code = 'SCL';
            //$item->delivert_name = 'Self Collection';
            //$item->item_id       = $itemId;
            $item->loc_id      = trim($item->loc_id);
            $item->qty_balance = $this->inventoryRepo->getItemTotalBalanceQty($itemId, $item->loc_id);

            return $item;
        })->where('qty_balance', '>', 0)->values();

        $hasStock = ($results->count() > 0);

        return collect([
            'delivery_code' => 'SCL',
            'delivery_name' => 'Self Collection',
            'item_id'       => $itemId,
            'has_stock'     => $hasStock,
            'items'         => $results,
        ]);
    }

    public function location($itemId, $locId = '')
    {
        $deductBufferQty = request('buffer_qty', false);
        $results         = $this->inventoryRepo->getItemBalanceQtyList($itemId, $locId);

        $results->each(function ($item) use ($deductBufferQty) {

            if ($deductBufferQty) {
                $item->balance -= $this->inventoryRepo->getItemBufferQty($item->item_id);
            }

            if ($item->balance < 0) {
                $item->balance = 0;
            }

            return $item;
        });

        return $results;
    }
}
*/