<?php
/*
namespace App\Api\Controllers\Auth;


use App\Api\Controllers\Controller;

class LoginController extends Controller
{
    protected $groups = [
        'crm' => ['CS_MGR', 'CS_SUP', 'CS_USR'],
        'pos' => ['CASHIER_HOD', 'CASHIER_SUP', 'CASHIER_USR'],
    ];

    protected $groupIds = [];

    public function login()
    {
        $credentials = request(['usr_id', 'password']);
        $sysId       = request('sys_type');

        $group = data_get($this->groups, $sysId, []);

        $this->groupIds = db('pg_cherps')->table('sss_grp_usr_list')->select(['grp_id'])->where('coy_id', 'CTL')->whereIn('grp_id', $group)->where('usr_id', request('usr_id'))->get();

        if ($this->groupIds->isEmpty() || ! $token = auth()->attempt($credentials)) {
            return $this->response->error('Unauthorized', 401);
        }

        return $this->respondWithToken($token);
    }

    public function me()
    {
        return response()->json(auth()->user());
    }

    public function logout()
    {
        auth()->logout();

        return $this->response->error('Successfully logged out', 200);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    protected function respondWithToken($token)
    {
        return $this->response->array([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60,
            'user'         => [
                'id'     => auth()->user()->usr_id,
                'name'   => auth()->user()->usr_name,
                'groups' => $this->groupIds->pluck('grp_id'),
            ],
        ]);
    }
}
*/