<?php

namespace App\Api\Controllers;

use App\Support\Str;
use Illuminate\Http\Request;

class NeaController extends Controller
{
    private $coy_id;

    public function __construct()
    {
        $this->coy_id = 'CTL';
    }

    public function nea_eligibilty()
    {

        $data = json_decode(file_get_contents('php://input'), true);

        if (isset($data['items']) && count($data['items']) > 0) {

            // Get the item categories
            $items = collect($data['items']);
            $itemsInfo = db('pg_cherps')->table('ims_item_list')->where('coy_id', $this->coy_id)->whereIn('item_id', $items->pluck('item_id'))
                ->selectRaw('rtrim(item_id) item_id, rtrim(inv_dim2) inv_dim2, rtrim(inv_dim3) inv_dim3, rtrim(inv_dim4) inv_dim4')
                ->get()
                ->keyBy('item_id')->toArray();

            $neaInfo = db('pg_cherps')->table('b2b_nea_list')->get();

            $collection = [];

            $items = $items->map(function ($itm) use ($itemsInfo, $neaInfo, &$collection) {

                $nea = null;
                // If item inv_dim is in neaInfo
                $id = $itm['item_id'];

                if (isset($itemsInfo[$id]) && ($itmInfo = $itemsInfo[$id])) {
                    $nea = $neaInfo->filter(function ($value) use ($itmInfo) {
                        return (Str::trim($value->inv_dim2) == $itmInfo->inv_dim2 && Str::trim($value->inv_dim3) == $itmInfo->inv_dim3);// || Str::trim($value->inv_dim4) == $itmInfo->inv_dim4);
                    })->first();
                }

                if ($nea) {
                    if (isset($collection[$nea->option_text])) {
                        $collection[$nea->option_text]["qty"] += $itm['qty'];
                    } else {
                        $collection[$nea->option_text] = [
                            "category" => $nea->option_text,
                            "remarks"  => $nea->customer_info,
                            "qty"      => $itm['qty']
                        ];
                    }
                }

                $itm['nea_eligible'] = !empty($nea);

                return $itm;
            });

            return [
                "code"       => 1,
                "msg"        => 'NEA eWaste eligibility checked',
                "items"      => $items,
                "collection" => array_values($collection)
            ];
        }

        return [
            "code" => 0,
            "msg"  => 'Invalid items'
        ];
    }

}