<?php

namespace App\Api\Controllers;
use App\Repositories\PaymentAnalysisRepository;
use App\Support\Str;
use Illuminate\Http\Request;

class PaymentAnalysisController extends Controller
{
    public function __construct(PaymentAnalysisRepository $paymentanalysisRepo)
    {
        //paygatewayRepo -> paymentanalysisRepo
        $this->paymentanalysisRepo = $paymentanalysisRepo;
        $this->api_chvoices = env('API_URL_CHVOICES');
        $this->api_valueclub = env('API_URL_VALUECLUB');
        $this->api_valueclub_token = env('API_URL_VALUECLUB_TOKEN');
    }

    public function full_analysis_list() {
        $full_analysis_list = $this->paymentanalysisRepo->analysis_list();

        $response['code'] = 200;

        if($full_analysis_list !== null)
        {
            $response['msg'] = 'All new record from '. $full_analysis_list .' added into o2o_order_analysis_list';
        }
        else
        {
            $response['msg'] = 'All new record added into o2o_order_analysis_list';
        }
        

        echo json_encode($response);
    }

    public function analysis_list_by_ho() {
        $data = json_decode(file_get_contents('php://input'), true);

        $ho_list = '';
        foreach($data['ho_list'] as $key=>$ho_number)
        {
            if($ho_list == '')
            {
                $ho_list = "'".$ho_number."'";
            }
            else
            {
                $ho_list = $ho_list.', '."'".$ho_number."'";
            }
        }

        $analysis_list_by_ho = $this->paymentanalysisRepo->analysis_list_by_ho($ho_list);

        $response['code'] = 200;
        $response['msg'] = $analysis_list_by_ho;
        
        echo json_encode($response);
    }

    public function analysis_list_by_hi() {
        $data = json_decode(file_get_contents('php://input'), true);

        $hi_list = '';
        foreach($data['hi_list'] as $key=>$hi_number)
        {
            if($hi_list == '')
            {
                $hi_list = "'".$hi_number."'";
            }
            else
            {
                $hi_list = $hi_list.', '."'".$hi_number."'";
            }
        }

        $analysis_list_by_hi = $this->paymentanalysisRepo->analysis_list_by_hi($hi_list);

        $response['code'] = 200;
        $response['msg'] = $analysis_list_by_hi;
        
        echo json_encode($response);
    }

    public function analysis_detail() {
        $data = json_decode(file_get_contents('php://input'), true);

        $txn_id = trim($data['txn_id']);

        $analysis_list_by_ho = $this->paymentanalysisRepo->analysis_detail($txn_id);

        $response['code'] = 200;
        $response['msg'] = $analysis_list_by_ho;
        
        echo json_encode($response);
    }

    public function paymentrecord_daily() {
        //oms_temp_missing_transaction
    }
}
