<?php
/*
namespace App\Api\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderItem;
use App\Services\CartService;
use App\Services\InventoryService;
use App\Services\OrderService;
use App\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class CartController extends Controller
{
    public function show(CartService $cartSvc)
    {

        if (! (request()->has('mbrId') || request()->has('cart_sess_id'))) {
            return $this->response()->errorInternal('Missing of required params.');
        }

        $query = Cart::active()->where('cart_label', request('cartName', 'default'));

        if (request()->has('mbrId')) {
            $query = $query->where('mbr_id', request('mbrId'));
        } elseif (request()->has('cart_sess_id')) {
            $query = $query->where('session_id', request('cart_sess_id'));
        }

        $cart = $query->first();

        if ($cart) {
            $cartSvc->cartRefresh($cart);

            $data          = $cart->toArray();
            $data['items'] = $cart->items->toArray();

            return $this->response->array($data);
        }

        return [];
    }

    public function create()
    {
        $cart = cart(request('cartName', 'default'));

        return [
            'cart_label' => $cart->cart_label,
            'cart_sess_id' => $cart->session_id,
            'mbr_id'       => optional($cart)->mbr_id,
        ];
    }

    public function addPWP()
    {
        $rowId = request('ItemRowId', '');


    }

    public function delete(Request $request)
    {
        $id = $request->get('itemRowId');

        $item = cart()->getItem(['id' => $id]);

        if (! $item) {
            return '';
        }

        if ($item->has_pwp_item) {
            cart()->removeItems(['pwp_group_id' => $item->pwp_group_id]);
        } else {
            cart()->removeItem(['id' => $item->id]);
        }

        return 'ok';
    }

    public function update(Request $request)
    {
        $id  = $request->get('id');
        $qty = $request->get('qty', 0);

        $item = cart()->getItem(['id' => $id]);

        if (! $item) {
            return '';
        }

        $updateData = [];

        if ($qty > 0) {
            $updateData['item_qty'] = $qty;
        }

        if ($updateData) {
            cart()->updateItem(['id' => $id], $updateData);
        }

        return 'ok';
    }

    public function clear()
    {
      $basket =  app('api')->baskets()->save(51,'test1');

      dd($basket);

      //cart()->clear();
    }

    public function confirmCartToOrder(OrderService $orderSvc)
    {
        $cart  = cart();
        $order = $orderSvc->createOrder($cart);

        return $order->toArray();
    }

    public function transferToPOS(Request $request, $id = '')
    {
        $cartHead  = db('sql')->table('o2o_cart_list')->where('cart_id', $id)->first();
        $cartItems = db('sql')->table('o2o_cart_items')->where('cart_id', $id)->get();

        $cartExists = db('sql')->table('o2o_basket_list')->where('mbr_id', $id)->where('basket_label', 'pos')->exists();

        if ($cartExists) {
            return $this->response->created();
        }

        if ($cartHead && $cartItems && ! $cartExists) {
            $basketHead = [
                //'basket_label' => 'pos',
                'mbr_id'     => $cartHead->mbr_id,
                'loc_id'     => $request->get('storeId', ''),
                'created_on' => '',
                'created_by' => $cartHead->mbr_id,
            ];

            try {

                db('sql')->transaction(function () use ($basketHead, $cartItems) {

                    $basketId = db()->table('o2o_cart_list')->insertGetId($basketHead);

                    collect($cartItems)->each(function ($row) use ($basketId, &$basketItems) {
                        $basketItems[] = [
                            'basket_id'     => $basketId,
                            'item_id'       => $row->item_id,
                            'item_img'      => $row->img_name,
                            'item_desc'     => $row->item_desc,
                            'is_pwp_item'   => $row->is_pwp_item,
                            'has_pwp_item'  => $row->has_pwp_item,
                            'pwp_group_id'  => $row->pwp_group_id,
                            'regular_price' => $row->regular_price,
                            'member_price'  => $row->member_price,
                            'promo_price'   => $row->promo_price,
                            'unit_price'    => $row->final_price,
                            'item_qty'      => $row->qty,
                            'created_on'    => $row->created_on,
                            'created_by'    => $row->created_by,
                        ];

                    }, 3);

                    db()->table('o2o_cart_item')->insert($basketItems);

                });

                return $this->response->created();
            } catch (\Exception $e) {
                $this->response->error('Failed to transfer cart details.', 200);
            }

        }

    }

    public function id3(OrderService $orderService)
    {

        $order = Order::where('order_num', 'HO1547179159')->first();

        $orderService->orderConfirm($order);

    }

    public function id2(InventoryService $inventorySvc)
    {
        db('sql')->enableQueryLog();

        $order = Order::where('order_num', 'HO1547179159')->first();

        echo $inventorySvc->deductByLocation($order);
        //$inventorySvc->releaseReservedStock($order);
    }

}
*/