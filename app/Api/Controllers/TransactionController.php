<?php

namespace App\Api\Controllers;
use App\Repositories\TransactionRepository;
use App\Support\Str;
use App\Http\Controllers\Orders\ReceiptController;

class TransactionController extends Controller
{
    public function __construct(TransactionRepository $transactionRepo, ReceiptController $receiptController){
        $this->transactionRepo = $transactionRepo;
        $this->receiptController = $receiptController;
    }

    private function _functionmap($functions, $cherps_param){
        foreach ($functions as $func) {
            if (
                !in_array(trim($func->pay_type), ['DBS_CC_REFUND','UOB_REFUND']) &&
                trim($func->func_dim1) == trim($cherps_param['pay_mode']) &&
                trim($func->func_dim2) == trim($cherps_param['pay_type'])
            ) {
                $cherps_param['pay_mode'] = trim($func->pay_mode);
                $cherps_param['pay_type'] = trim($func->pay_type);
            }
        }
        return $cherps_param;
    }

    private function _calcOrder($return_type, $cherps_param){
        if ($return_type=='order_total') {
            $order_total = 0;
            foreach($cherps_param as $param){
                $order_total += $param->trans_amount;
            }
            return $order_total;
        }
    }

    public function updateCollection($trans_id) {
        $data = json_decode(file_get_contents('php://input'), true);

        foreach($data['items'] as $item) {
            $itemExisting = $this->transactionRepo->getOrderItem($item['id']);
            $customData = json_decode($itemExisting->custom_data);

            if (isset($customData->selfcollect)) {
                // Update custom_data where available
                if (isset($item['lot_id']) && count($item['lot_id']) > 0)
                    $customData->lot_id = $item['lot_id'];
                if (isset($item['rfid']) && count($item['rfid']) > 0)
                    $customData->rfid = $item['rfid'];
//                if (isset($item['salesperson_id']) && count($item['salesperson_id']) > 0)
//                    $customData->salesperson_id = $item['salesperson_id'];

                $customData->selfcollected = $customData->selfcollect;
                $customData->selfcollect = '';

                $update = $this->transactionRepo->saveOrderItemCustomData($item['id'], $customData);
            }
        }

        return ($update) ? json_encode(["code"=>1,"msg"=>"Update success"]) : json_encode(["code"=>0,"msg"=>"Update failed"]) ;
    }

    public function updateKrisflyer($trans_id) {
        $data = json_decode(file_get_contents('php://input'), true);
        $update = $this->transactionRepo->updCh_list_remarks($data['trans_id'],'KRISPAY',$data['ref_id']);
        return ($update) ? json_encode(["code"=>1,"msg"=>"Update success"]) : json_encode(["code"=>0,"msg"=>"Update failed"]) ;
    }

    public function updateReceiptSalesperson($trans_id) {
        /* Sample request:
         *      {"trans_id":"B9C00001", "line_num":1, "salesperson_id":"XXX", "update":0, "user":"YOU"}
         */
        $data = json_decode(file_get_contents('php://input'), true);
        $trans_id = $data['trans_id'];
        $line_num = $data['line_num'];
        $saleperson_ids = (is_array($data['salesperson_id'])) ? $data['salesperson_id'] : [$data['salesperson_id']] ; // Must be an array
        $saleperson_update = (isset($data['update']) && $data['update']==1) ? 1 : 0;
        $modifed_by = (isset($data['user'])) ? $data['user'] : 'POSAPI' ;
        $execute = $this->transactionRepo->updPg_item_salesperson($trans_id,$line_num,$saleperson_ids,$saleperson_update,$modifed_by);

        return ($execute) ? json_encode(["code"=>1,"msg"=>"Update success"]) : json_encode(["code"=>0,"msg"=>"Update failed"]) ;
    }

    public function updateReceiptLot($trans_id) {
        /* Sample request:
         *      {"trans_id":"B9C00001", "line_num":1, "lot_id":"XXX", "update":0, "user":"YOU"}
         */
        $data = json_decode(file_get_contents('php://input'), true);
        $trans_id = $data['trans_id'];
        $line_num = $data['line_num'];
        $lot_ids = (is_array($data['lot_id'])) ? $data['lot_id'] : [$data['lot_id']] ; // Must be an array
        $lot_update = (isset($data['update']) && $data['update']==1) ? 1 : 0;
        $modifed_by = (isset($data['user'])) ? $data['user'] : 'POSAPI' ;
        $execute = $this->transactionRepo->updPg_item_lot($trans_id,$line_num,$lot_ids,$lot_update,$modifed_by);

        return ($execute) ? json_encode(["code"=>1,"msg"=>"Update success"]) : json_encode(["code"=>0,"msg"=>"Update failed"]) ;
    }

    public function getSmsInvoice() {

        $param['coy_id'] = (isset($_GET['coy_id'])) ? $_GET['coy_id'] : 'CTL';
        $param['loc_id'] = (isset($_GET['loc_id'])) ? $_GET['loc_id'] : '';

        $invoices =  $this->transactionRepo->getCh_invoice_full($param['coy_id'],$param['loc_id']);

        $data = []; // For data response
        $lastinvoice = '';
        $thisdata = [];
        $thisdataitems = [];
        $thisdataitemsubttl = 0;
        if (!empty($invoices)) {
            foreach ($invoices as $inv) {

                if ($lastinvoice != $inv->invoice_id) {
                    // New $data
                    if (!empty($thisdata)) {
                        $thisdata['items'] = $thisdataitems;
                        $thisdata['item_count'] = count($thisdataitems);
                        $thisdata['sub_total'] = $thisdataitemsubttl;
                        $data[] = $thisdata;

                        // RESET
                        $thisdata = [];
                        $thisdataitems = [];
                        $thisdataitemsubttl = 0;
                    }

                    $thisdata = [
                        'loc_id' => $inv->loc_id,
                        'pos_id' => $inv->inv_type,
                        'mbr_id' => $inv->mbr_id,
                        'inv_type' => $inv->inv_type,
                        'invoice_id' => $inv->invoice_id,
                        'created_on' => $inv->invoice_date,
                    ];

                    $unit_price = $inv->unit_price;
                    if ($inv->disc_amount > 0) {
                        $unit_price -= $inv->disc_amount;
                    }
                    else if ($inv->disc_percent > 0) {
                        $disc = ($inv->disc_percent/100) * $unit_price;
                        $unit_price -= $disc;
                    }

                    $thisdataitems[] = [
                        'item_id' => $inv->item_id,
                        'item_desc' => $inv->item_desc,
                        'item_qty' => intval($inv->item_qty),
                        'unit_price' => $unit_price
                    ];
                    $thisdataitemsubttl += $inv->unit_price;
                } else {
                    // Append $dataitems
                    $thisdataitems[] = [
                        'item_id' => $inv->item_id,
                        'item_desc' => $inv->item_desc,
                        'item_qty' => intval($inv->item_qty),
                        'unit_price' => $inv->unit_price
                    ];
                    $thisdataitemsubttl += $inv->unit_price;
                }

                $lastinvoice = $inv->invoice_id;
            }
        }

        if (!empty($thisdata)) {
            $thisdata['items'] = $thisdataitems;
            $thisdata['item_count'] = count($thisdataitems);
            $thisdata['sub_total'] = $thisdataitemsubttl;
            $data[] = $thisdata;
        }

        $response = [
            'data' => $data,
//            'links' => ['first'=>''],
//            'meta' => ['total'=>0]
        ];

        echo json_encode($response);
    }

    public function getOneInvoice($invoice_id) {

        $param['coy_id'] = (isset($_GET['coy_id'])) ? $_GET['coy_id'] : 'CTL';
        $param['loc_id'] = (isset($_GET['loc_id'])) ? $_GET['loc_id'] : '';

        $invoice =  $this->transactionRepo->getCh_invoice_full($param['coy_id'],$param['loc_id'],$invoice_id);

        $data = []; // For data response
        $deposit_item = 0;
        $deposit_ref = '';
        $thisdataitems = []; $thisdatadeposit = [];
        $thisdataitemsubttl = 0;
        $thisdataline = 0;
        if (!empty($invoice)) {
            foreach ($invoice as $inv) {

                // Prepare a set of deposit item (DI)
                if ($inv->item_id=='$DEPOSIT' && $inv->item_qty>0 && in_array($inv->status_level,[1,2])) {
                    $thisdatadeposit[] = [
                        'item_id' => $inv->item_id,
                        'item_desc' => $inv->item_desc,
                        'item_qty' => intval($inv->item_qty),
                        'unit_price' => $inv->unit_price,
                        'lot_id' => ($inv->lot_id == '') ? '' : $inv->lot_id,
                        'lot_id2' => ($inv->lot_id2 == '') ? '' : $inv->lot_id2,
                        'string_udf1' => ($inv->string_udf1 == '') ? '' : $inv->string_udf1,
                        'string_udf2' => ($inv->string_udf2 == '') ? '' : $inv->string_udf2,
                        'string_udf3' => ($inv->string_udf3 == '') ? '' : $inv->string_udf3,
                        'status_level' => 1
                    ];
                }

                // Append all valid items $dataitems
                if (in_array($inv->status_level,[1,2])) {
                    $thisdataitems[] = [
                        'item_id' => $inv->item_id,
                        'item_desc' => $inv->item_desc,
                        'item_qty' => intval($inv->item_qty),
                        'unit_price' => $inv->unit_price,
                        'lot_id' => ($inv->lot_id == '') ? '' : $inv->lot_id,
                        'lot_id2' => ($inv->lot_id2 == '') ? '' : $inv->lot_id2,
                        'salesperson_id' => $inv->salesperson_id,
                        'string_udf1' => ($inv->string_udf1 == '') ? '' : $inv->string_udf1,
                        'string_udf2' => ($inv->string_udf2 == '') ? '' : $inv->string_udf2,
                        'string_udf3' => ($inv->string_udf3 == '') ? '' : $inv->string_udf3,
                        'status_level' => 1
                    ];
                    $thisdataitemsubttl += $inv->unit_price;
                }

                $deposit_ref = ($inv->deposit_ref_id!='') ? $inv->deposit_ref_id : $deposit_ref;
                if ($inv->item_id=='$DEPOSIT')
                    $deposit_item++;

                $thisdataline++;
                if ($thisdataline == count($invoice)) {

                    if ($deposit_item>0){
                        $trans_type = ($deposit_ref=='') ? 'DI' : 'DZ';
                    }
                    else {
                        $trans_type = $inv->inv_type;
                    }

                    $data = [
                        'loc_id' => $inv->loc_id,
                        'pos_id' => $inv->inv_type,
                        'mbr_id' => $inv->mbr_id,
                        'inv_type' => $inv->inv_type,
                        'invoice_id' => $inv->invoice_id,
                        'created_on' => $inv->invoice_date,
                        'ref_id' => $deposit_ref,
                        'trans_type' => $trans_type
                    ];

                    $data['items'] = ($trans_type=='DI') ? $thisdatadeposit : $thisdataitems;
                    $data['item_count'] = count($thisdataitems);
                    $data['sub_total'] = $thisdataitemsubttl;

                }
            }
        }

        echo json_encode($data);

    }

    public function getOneReceipt($trans_id){

        $receipt_id = $this->transactionRepo->getPg_order($trans_id);
        if ($receipt_id) {
            return $this->receiptController->reprint($receipt_id->order_num);
        }
        else {
            $coy_id = request()->get('coy_id','CTL'); //(isset($_GET['coy_id'])) ? $_GET['coy_id'] : 'CTL';
            $receipt =  $this->transactionRepo->getCh_order_full($coy_id,$trans_id);

            if (!$receipt) {
                // Try get from ms_cherps
                $getMs = $this->transactionRepo->getMs_to_Ch($trans_id);
                if ($getMs['list'] > 0) {
                    $receipt =  $this->transactionRepo->getCh_order_full($coy_id,$trans_id);
                }
                else {
                    return $this->errorNotFound();
                }
            }

            $receipt['order_list'][0]->item_total = count($receipt['order_item']);
            $receipt['order_list'][0]->order_total = $this->_calcOrder('order_total',$receipt['order_payment']);
            $order_id = $this->transactionRepo->updPg_order('o2o_order_list',$receipt['order_list'][0]);

            // ADD ITEM LIST
            for ($i = 0; $i < count($receipt['order_item']); $i++) {
                $receipt['order_item'][$i] = (array) $receipt['order_item'][$i];
                $order_lots = [];
                if (count($receipt['order_lot'])>0) {
                    foreach($receipt['order_lot'] as $lot) {
                        if ($lot->line_num == $receipt['order_item'][$i]['line_num'])
                            $order_lots[] = $lot->lot_id;
                    }
                }
                $receipt['order_item'][$i]['order_id'] = $order_id;
                $receipt['order_item'][$i]['custom_data'] = json_encode([
                    "promo_id"=> $receipt['order_item'][$i]['promo_id'],
                    "lot_id"=> $order_lots
                ]);
                unset($receipt['order_item'][$i]['promo_id']);
                $this->transactionRepo->updPg_order('o2o_order_item',$receipt['order_item'][$i]);
            }

            // ADD PAYMENT LIST
            $functions =  $this->transactionRepo->eod_payments_mapping();
            for ($i = 0; $i < count($receipt['order_payment']); $i++) {
                $receipt['order_payment'][$i] = (array) $receipt['order_payment'][$i];
                $receipt['order_payment'][$i]['order_id'] = $order_id;
                $receipt['order_payment'][$i] = $this->_functionmap($functions, $receipt['order_payment'][$i]);
                $this->transactionRepo->updPg_order('o2o_order_payment',$receipt['order_payment'][$i]);
            }

            return $this->receiptController->reprint($receipt['order_list'][0]->order_num);
        }

    }

    public function getDeposits() {

        $param['coy_id'] = (isset($_GET['coy_id'])) ? $_GET['coy_id'] : 'CTL';
        $param['loc_id'] = (isset($_GET['loc_id'])) ? $_GET['loc_id'] : '';

        $data =  $this->transactionRepo->getPg_order_dm($param['coy_id'],$param['loc_id']);

        $response = [
            'data' => $data,
//            'links' => ['first'=>''],
//            'meta' => ['total'=>0]
        ];

        echo json_encode($response);
    }

    public function getIncallSR($sr_id){

        $header = app('request')->header('X-AUTH');
        if ($header!="daaa259760e5402367299b0fa2646e14") {
            $return_data['code'] = 0;
            $return_data['msg'] = 'Unauthorized Access';
            echo json_encode($return_data);
            exit;
        }

        $receipt = $this->transactionRepo->getPg_order_incall($sr_id);
        if ($receipt) {
            $response = [
                'code'  => 1,
                'msg'   => 'Service request found',
                'data'  => $receipt
            ];
        }
        else {
            $response = [
                'code'  => 0,
                'msg'   => 'Service request not found'
            ];
        }
        echo json_encode($response);

    }

    public function hsgGetStocks($item_ids, $src='LIVE', $rtn=0) {

        $loc_ids = '';
        $env = (app()->environment('production')) ? 'cherps' : 'cherps_stage';
        $endpoint = env('API_URL_LAMBDA') . "v1/ctlstock/" . $item_ids . "?loc_id=" . $loc_ids . "&src=" .strtolower($src). "&env=" .$env;
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $response = $client->get($endpoint,[
            'headers' => ['Authorization' => '8f413c39b4d1b4e7c584943df22e7a8c','Content-Type' => 'application/json']
        ]);
        $content = json_decode($response->getBody()->getContents());
        $content->endpoint = $endpoint;

        $delv_response = [
            'code'  => 1,
            'data'  => [
            ]
        ];
        foreach($content->data as $key=>$item) {

            // Reformat the loc QOH easier to use
            $item_qoh = [];
            foreach($item->loc as $loc) {
                $item_qoh[$loc->loc_id] = $loc;
            }

            $delv_response['data'][$key]['item_id'] = $item->item_id;
            $delv_response['data'][$key]['item_desc'] = $item->item_desc;
            $delv_response['data'][$key]['buffer_qty'] = $item->buffer_qty;
            $delv_response['data'][$key]['qty_available'] = 0 - $item->holding_qty; // Add avail_qty after calcuation
            $total_qty_available = 0;

            foreach( hsg_delivery() as $hsg_delivery ) {

                $time_from = (int) str_replace(':','', $hsg_delivery->time_from ?? '00:00:00' );
                $time_to = (int) str_replace(':','', $hsg_delivery->time_to ?? '23:59:59');
                $time_now = (int) date('Gis');
                if ($hsg_delivery->status_level < 1 || $time_now < $time_from || $time_now > $time_to) {
                    // delv_method is not active
                    continue;
                }

                $delv_method = [
                    "delv_method"   => $hsg_delivery->delv_method,
                    "delv_name"     => $hsg_delivery->delv_name,
                    "avail_qty"     => 0,
                    "max_qty"       => 20
                ];

                if ($hsg_delivery->delv_type == "MULTI") {
                    $delv_method['delv_selection'] = []; // Preset for use by MULTI selection
                }

                $hsg_delv_loc = explode(',', $hsg_delivery->delv_loc);
                foreach($hsg_delv_loc as $loc) {
                    if (isset($item_qoh[$loc])) {
                        $delv_method['avail_qty'] += $item_qoh[$loc]->qty_on_hand - $item_qoh[$loc]->qty_reserved;

                        if ($hsg_delivery->delv_type == "MULTI" && ($item_qoh[$loc]->qty_on_hand - $item_qoh[$loc]->qty_reserved) > 0 ) {
                            $location = pos_locations($loc);
                            $delv_method['delv_selection'][] = [
                                'loc_id' => $location->loc_id,
                                'loc_name' => $location->shop_name,
                                'avail_qty' => $item_qoh[$loc]->qty_on_hand - $item_qoh[$loc]->qty_reserved
                            ];
                        }
                    }
                }

                if ($item->is_digital == 'Y') {
                    // Digital Items, every outlet get 99 pcs
                    foreach(pos_locations() as $location) {
                        $delv_method['avail_qty'] += 99;

                        if ($hsg_delivery->delv_type == "MULTI") {
                            $delv_method['delv_selection'][] = [
                                'loc_id' => $location->loc_id,
                                'loc_name' => $location->shop_name,
                                'avail_qty' => 99
                            ];
                        }
                    }
                }

                // Add buffer_qty
                $delv_method['avail_qty'] -= $item->buffer_qty;

                // Add this method into response
                if ($delv_method['avail_qty']>0) {
                    $total_qty_available = ($delv_method['avail_qty']>$total_qty_available) ? $delv_method['avail_qty'] : $total_qty_available;
                    $delv_method['max_qty'] = ($delv_method['avail_qty'] > 20) ? 20 : $delv_method['avail_qty'];
                    $delv_response['data'][$key]['delv_method'][] = $delv_method;
                }
            }

            $delv_response['data'][$key]['qty_available'] += $total_qty_available;
        }

        if ($rtn==1)
            return $delv_response['data'];
        else
            echo json_encode($delv_response);

    }

    public function hsgCheckStocks() {

        $data['items']    = request()->input('items', []);
        $data['action']   = request()->input('action');
        $data['trans_id'] = request()->input('trans_id');

        // Get items from POST
        $item_id_qty = [];
        $item_id_arr = [];
        foreach($data['items'] as $item){
            $id = trim($item['item_id']);
            $item_id_qty[$id] = 0;
            $item_id_arr[] = $id;
        }

        $hsgGetStocksArray = [];
        $hsgGetStocks = $this->hsgGetStocks(implode(',',$item_id_arr), 'LIVE', 1);
        foreach($hsgGetStocks as $response) {

            $id = trim($response['item_id']);
            $hsgGetStocksArray[$id] = [
                "item_id" => $response['item_id'],
                "item_desc" => $response['item_desc'],
                "qty_available" => $response['qty_available'],
                "buffer_qty" => $response['buffer_qty']
            ];

            $stockDelvMethod = [];
            if (isset($response['delv_method'])) {
                foreach ($response['delv_method'] as $response1) {
                    $stockDelvMethod[$response1['delv_method']] = $response1;
                }
            }
            $hsgGetStocksArray[$id]['available_methods'] = $stockDelvMethod;

        }

        // Format response
        $stockCheckFail = 0;
        $delv_response = [];
        foreach($data['items'] as $item) {
            $this_response = $item;

            $id = trim($item['item_id']);
            $this_method = $item['delv_method'];

            if (!isset($hsgGetStocksArray[$id])) {
                $this_response['error'] = 'Item unavailable';
            }
            elseif (!isset($hsgGetStocksArray[$id]['available_methods'][$this_method])) {
                $this_response['error'] = 'Please choose another delivery method';
            }
            else {
                $this_response['error'] = '';
                $this_method_data = hsg_delivery($this_method);

                if ($this_method_data->delv_type == "MULTI") {
                    // Availablity is by outlet
                    if (!in_array( $item['loc_id'] , explode(',', $this_method_data->delv_loc))) {
                        $this_response['error'] = 'Outlet unavailable for this delivery method';
                    }
                    else {
                        foreach($hsgGetStocksArray[$id]['available_methods'][$this_method]['delv_selection'] as $actual_loc) {
                            if ($actual_loc['loc_id'] == $item['loc_id']) {
                                $available_qty = $actual_loc['avail_qty']; // Same store order?
                            }
                        }
                    }
                }
                else {
                    $available_qty = $hsgGetStocksArray[$id]['available_methods'][$this_method]['avail_qty'] - $item_id_qty[$id];
                }

                if ($this_response['error']=='') {
                    // Lastly, check qty
                    if ($this_response['qty_ordered'] > $available_qty) {
                        $this_response['error'] = 'Quantity ordered exceeds the available quantity ('.$available_qty.')';
                    }
                    else {
                        $item_id_qty[$id] += $this_response['qty_ordered'];
                    }
                }


                $this_response['item_desc'] = $hsgGetStocksArray[$id]['item_desc'];

//                $this_response['this_method_stock'] = $hsgGetStocksArray[$id];
//                $this_response['this_method_data'] = $this_method_data;
                $this_response['qty_available'] = $available_qty;

            }

            if ($this_response['error']!='') {
                $stockCheckFail++;
            }
            ksort($this_response);
            $delv_response[] = $this_response;
        }

        if ( strtolower($data['action']) == "hold" || strtolower($data['action']) == "release" ) {

            if ($stockCheckFail>0) {
                foreach($delv_response as $key=>$resp) {
                    $delv_response[$key]['hold_status'] = 0;
                }
            }
            else {
                // Call microservice to hold/release holding qty
                $holding_data_item = [];
                foreach($delv_response as $key=>$resp) {
                    $holding_data_item[] = [
                        "item_id"   => $resp['item_id'],
                        "qty"       => $resp['qty_ordered']
                    ];
                }
                $holding_data = [
                    "trans_id"  => $data['trans_id'],
                    "action"    => $data['action'],
                    "items"     => $holding_data_item
                ];

                $endpoint = env('API_URL_LAMBDA') . "v1/ctlstock/"; // . $item_ids . "?loc_id=" . $loc_ids . "&src=" .strtolower($src). "&env=" .$env;
                $client = new \GuzzleHttp\Client(['http_errors' => false]);
                $response = $client->post($endpoint, [
                    'headers' => ['Authorization' => '8f413c39b4d1b4e7c584943df22e7a8c', 'Content-Type' => 'application/json'],
                    'body' => json_encode($holding_data)
                ]);

                $content = $response->getBody()->getContents();
                $content_data = json_decode($content, true);

                foreach($delv_response as $key=>$resp) {
                    $delv_response[$key]['hold_status'] = 1;
                }
            }
        }

        return response()->json($delv_response);
    }

}