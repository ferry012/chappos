<?php

namespace App\Utils;

use App\Models\Setting as Model;
use App\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class Setting
{

    protected        $model;
    protected        $data  = [];
    protected static $cache = [];

    public function __construct()
    {
        $this->model = new Model;

        $this->initialize();
    }

    public function initialize()
    {
        // Preload settings
        $results = $this->model->where('autoload', 1)->get(['set_key', 'set_value', 'set_type']);

        $this->putData($results);
    }

    public function findData($name)
    {
        // only allow 2 level
        list($group, $key) = array_pad(explode('.', $name, 2), 2, null);

        if (empty($key)) {
            $query = $this->model->where('set_key', 'like', $group.'.%');
        } else {
            // Get specific data
            $query = $this->model->where('set_key', $name);
        }

        $results = $query->get(['set_key', 'set_value', 'set_type']);

        $this->putData($results);
    }

    protected function putData($data)
    {
        if ($data instanceof Collection) {
            $data->each(function ($item) {

                $val = $item->set_value;

                if ($val !== null) {
                    // Remove left and right spacing
                    $val = trim($val);
                }

                $this->put($item->set_key, [
                    'value' => $val,
                    'type'  => $item->set_type,
                ]);

            });
        }
    }

    public function get($name, $default = null)
    {
        // Remove dot
        $name = rtrim($name, '.');
        // Main level retrieve new set data
        $refresh = substr_count($name, '.') === 0;

        if (! $refresh && Arr::has(static::$cache, $name)) {
            return Arr::get(static::$cache, $name, $default);
        }

        if ($refresh || ! Arr::has($this->data, $name)) {
            $this->findData($name);
        }

        $data = Arr::get($this->data, $name, $default);

        if (is_array($data)) {
            list($value, $type) = Arr::flatten($data);

            if (empty($value)) {
                if ($type == 'int')
                    $value = 0;
                else if ($type == 'bool')
                    $value = (bool) 0;
                else
                    return $default;
            }

            Arr::set(static::$cache, $name, $this->castData($type, $value));
        } else {
            return $default;
        }

        return Arr::get(static::$cache, $name, $default);
    }

    public function put($key, $value = null)
    {
        if (! is_array($key)) {
            $key = [$key => $value];
        }

        foreach ($key as $arrayKey => $arrayValue) {
            $this->set($arrayKey, $arrayValue);
        }
    }

    public function set($name, $value)
    {
        Arr::set($this->data, $name, $value);
    }

    protected function castData($type, $value)
    {
        if ($value === null) {
            return $value;
        }

        switch ($this->getCastType($type)) {
            case 'int':
            case 'integer':
                return (int)$value;
            case 'decimal':
            case 'real':
            case 'float':
            case 'double':
                return (float)$value;
            case 'string':
                return (string)$value;
            case 'bool':
            case 'boolean':
                return (bool)$value;
            case 'object':
                return $this->fromJson($value, true);
            case 'array':
            case 'json':
                return $this->fromJson($value);
            case 'collection':
                //
            case 'date':
                //
            case 'datetime':
                //
            case 'timestamp':
                //
            default:
                return $value;
        }
    }

    /**
     * Get the type of cast for a model attribute.
     *
     * @param  string $key
     *
     * @return string
     */
    protected function getCastType($key)
    {
        return strtolower(trim($key));
    }

    /**
     * Decode the given JSON back into an array or object.
     *
     * @param  string $value
     * @param  bool   $asObject
     *
     * @return mixed
     */
    public function fromJson($value, $asObject = false)
    {
        return json_decode($value, ! $asObject);
    }

    /**
     * Changes PHP array to PostgreSQL array format
     *
     * @param array $array
     *
     * @return string
     */
    public static function phpArrayToPostgresArray(array $array)
    {
        return str_replace(['[', ']'], [
            '{', '}',
        ], json_encode(collect($array)->values()->toArray(), JSON_UNESCAPED_UNICODE));
    }

    public function pgArrayParse($s, $start = 0, &$end = null)
    {
        if (empty($s) || $s[0] !== '{') {
            return null;
        }

        $return = [];
        $string = false;
        $quote  = '';
        $len    = strlen($s);
        $v      = '';

        for ($i = $start + 1; $i < $len; $i++) {
            $ch = $s[$i];
            if (! $string && $ch === '}') {
                if ($v !== '' || ! empty($return)) {
                    $return[] = $v;
                }
                $end = $i;
                break;
            }

            if (! $string && $ch === '{') {
                $v = self::pgArrayParse($s, $i, $i);
            } else
                if (! $string && $ch === ',') {
                    $return[] = $v;
                    $v        = '';
                } else
                    if (! $string && ($ch === '"' || $ch === "'")) {
                        $string = true;
                        $quote  = $ch;
                    } else
                        if ($string && $ch === $quote && $s[$i - 1] === "\\") {
                            $v = substr($v, 0, -1).$ch;
                        } else
                            if ($string && $ch === $quote && $s[$i - 1] !== "\\") {
                                $string = false;
                            } else {
                                $v .= $ch;
                            }
        }

        foreach ($return as &$r) {
            if (is_numeric($r)) {
                if (ctype_digit($r)) {
                    $r = (int)$r;
                } else {
                    $r = (float)$r;
                }
            }
        }

        return $return;
    }
}