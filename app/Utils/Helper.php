<?php

namespace App\Utils;

use App\Models\Country;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Webpatser\Uuid\Uuid;

class Helper
{
    /**
     * Ensures that a given variable is an array
     *
     * @param mixed $value Variable to check
     *
     * @return array
     **/
    public static function ensureArray($value)
    {
        if (! is_array($value)) {
            return [$value];
        }

        return $value;
    }

    /**
     * Checks whether the given $value is an empty array or not
     *
     * @param mixed $value Value to check
     *
     * @return bool
     */
    public static function isEmptyArray($value)
    {
        if (is_array($value)) {
            foreach ($value as $subvalue) {
                if (! self::isEmptyArray($subvalue)) {
                    return FALSE;
                }
            }
        } elseif (! empty($value) || $value !== '') {
            return FALSE;
        }

        return TRUE;
    }

    /**
     * Compares two values
     *
     * Returns 1 if first is greater, -1 if second is, 0 if same
     *
     * @param mixed $one Value 1 to compare
     * @param mixed $two Value 2 to compare
     *
     * @return int
     */
    public static function compareValues($one, $two)
    {
        // something is null
        if (is_null($one) || is_null($two)) {
            if (is_null($one) && ! is_null($two)) {
                return 1;
            } elseif (! is_null($one) && is_null($two)) {
                return -1;
            }

            return 0;
        }

        // something is an array
        if (is_array($one) || is_array($two)) {
            if (is_array($one) && ! is_array($two)) {
                return 1;
            } elseif (! is_array($one) && is_array($two)) {
                return -1;
            }

            return 0;
        }

        // something is an object
        if (is_object($one) || is_object($two)) {
            if (is_object($one) && ! is_object($two)) {
                return 1;
            } elseif (! is_object($one) && is_object($two)) {
                return -1;
            }

            return 0;
        }

        // something is a boolean
        if (is_bool($one) || is_bool($two)) {
            if ($one && ! $two) {
                return 1;
            } elseif (! $one && $two) {
                return -1;
            }

            return 0;
        }

        // string based
        if (! is_numeric($one) || ! is_numeric($two)) {
            return strcasecmp($one, $two);
        }

        // number-based
        if ($one > $two) {
            return 1;
        } elseif ($one < $two) {
            return -1;
        }

        return 0;
    }

    public static function makeUuid()
    {
        return (string)Uuid::generate(4);
    }

    public static function getPhoneParse($numberToParse, $defaultRegion = '')
    {
        $phoneUtil = PhoneNumberUtil::getInstance();

        try {
            $phone = $phoneUtil->parse($numberToParse, $defaultRegion);

            if ($phoneUtil->isValidNumber($phone)) {
                return $phone;
            }

        } catch (NumberParseException $e) {

        }

        return null;
    }

    public static function getPhoneParts($numberToParse, $defaultRegion = '')
    {
        $phone = static::getPhoneParse($numberToParse, $defaultRegion);

        $static = new \stdClass();

        $static->country_code = null;
        $static->calling_code = null;
        $static->number       = '';

        if ($phone) {
            $countryCode = null;
            /*
            $country     = Country::findByCallingCode($phone->getCountryCode());

            if ($country) {
                $countryCode = $country->iso_3166_2;
            }
            */
            $static->country_code = $countryCode;
            $static->calling_code = $phone->getCountryCode();
            $static->number       = $phone->getNationalNumber();

        }

        return $static;
    }

    public static function parseInstallmentMonth($str)
    {
        $month = (int)filter_var($str, FILTER_SANITIZE_NUMBER_INT);

        return str_pad($month ?? '00', 2, '0', STR_PAD_LEFT).' Months';
    }

    public static function generateCacheKeys($tags=[], $id='', $req=[]) {

        $cacheTag = $tags;
        $cacheKey = $tags[0] . '.' . $id . '.' . md5(json_encode($req));

        return [$cacheTag, $cacheKey];
    }
}