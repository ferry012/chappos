<?php

namespace App\Core;

use App\Core\Baskets\Services\BasketService;
use App\Core\Members\Services\MemberService;
use App\Core\Orders\Services\OrderService;
use App\Core\Pricing\PriceCalculatorService;
use App\Core\Product\Services\ProductService;
use App\Exceptions\InvalidServiceException;

class Factory
{
    /**
     * @var BasketService
     */
    protected $baskets;

    protected $members;

    protected $products;

    protected $price;

    public function __construct(BasketService $baskets, MemberService $members, ProductService $products, OrderService $orders, PriceCalculatorService $price)
    {
        $this->baskets  = $baskets;
        $this->members  = $members;
        $this->products = $products;
        $this->price    = $price;
        $this->orders   = $orders;
    }

    public function __call($name, $arguments)
    {
        if (! property_exists($this, $name)) {
            throw new InvalidServiceException(trans('exceptions.invalid_service', [
                'service' => $name,
            ]), 1);
        }

        return app()->make(
            get_class($this->{$name})
        );
    }
}