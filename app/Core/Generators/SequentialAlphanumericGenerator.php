<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 24/4/2019
 * Time: 11:34 AM
 */

namespace App\Core\Generators;

use App\Core\Generators\Contracts\NumberGenerator;
use Carbon\Carbon;

class SequentialAlphanumericGenerator implements NumberGenerator
{
    protected $name;
    protected $startNum;
    protected $prefix;
    protected $padLength;
    protected $padString;

    public function __construct()
    {
        $this->startNum  = '1';
        $this->prefix    = '';
        $this->padLength = 8;
        $this->padString = '0';
    }

    public function name($name)
    {
        $this->name = $name;

        return $this;
    }

    public function start($code)
    {
        $this->startNum = $code;

        return $this;
    }

    public function length($length)
    {
        $this->padLength = $length;

        return $this;
    }

    public function padString($string)
    {
        $this->padString = $string;

        return $this;
    }

    public function prefix($prefix)
    {
        $this->prefix = $prefix;

        return $this;
    }

    public function generate()
    {
        return retry(5, function () {
            try {
                db()->beginTransaction();

                db()->select('lock table table_trans_list');

                $query = $this->getQuery()->where('trans_name', $this->name)->where('trans_prefix', $this->prefix);
                $row   = $query->lockForUpdate()->first();
                $next  = $this->startNum;

                if ($row) {
                    $next = $row->next_code;
                    $query->update(['next_code' => $this->getNextAlphanumeric($next), 'modified_on' => Carbon::now()]);
                } else {
                    $this->getQuery()->insert([
                        'trans_name' => $this->name, 'trans_prefix' => $this->prefix,
                        'next_code'  => $this->getNextAlphanumeric($next), 'created_on' => Carbon::now(),
                    ]);
                }

                db()->commit();

            } catch (\Exception $e) {
                db()->rollBack();
            }

            return sprintf('%s%s', $this->prefix,
                str_pad($next, $this->padLength, $this->padString, STR_PAD_LEFT)
            );

        }, 100);

    }

    public function getNextAlphanumeric($code)
    {
        list($alpha, $numeric) = sscanf($code, '%[A-Z]%[0-9]');

        $codeLength = strlen($code);

        if ($alpha === null && $numeric === null) {
            $beforeNumLength = strlen($code);
            $numeric         = (int)$code;
        } else {
            $beforeNumLength = strlen($numeric);
        }

        $numeric++;

        $afterNumLength = strlen((string)$numeric);

        if ($afterNumLength > $beforeNumLength && $alpha === null) {
            $alpha   = 'A';
            $numeric = 0;
        } elseif ($afterNumLength > $beforeNumLength) {
            $beforeAlphaLength = strlen($alpha);
            $alpha             = strtoupper($alpha);
            ++$alpha;
            $afterAlphaLength = strlen($alpha);
            if ($afterAlphaLength > $beforeAlphaLength) {
                $numeric = 0;
            }
        }

        $numeric = str_pad($numeric, $codeLength, '0', STR_PAD_LEFT);
        $numeric = substr($numeric, strlen($alpha));

        return $alpha.$numeric;
    }

    protected function getQuery()
    {
        return db()->table('table_trans_list');
    }
}