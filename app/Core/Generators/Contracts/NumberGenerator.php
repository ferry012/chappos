<?php

namespace App\Core\Generators\Contracts;

interface NumberGenerator
{
    public function generate();
}