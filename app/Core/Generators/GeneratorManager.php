<?php

namespace App\Core\Generators;

use App\Support\Manager;

class GeneratorManager extends Manager
{
    /**
     * Get a driver instance.
     *
     * @param  string $driver
     *
     * @return mixed
     */
    public function with($driver)
    {
        return $this->driver($driver);
    }

    public function createSeqAlphanumDriver()
    {
        return $this->buildProvider(SequentialAlphanumericGenerator::class);
    }

    public function buildProvider($provider)
    {
        return $this->app->make($provider);
    }

    public function getDefaultDriver()
    {
        // TODO: Implement getDefaultDriver() method.
    }
}