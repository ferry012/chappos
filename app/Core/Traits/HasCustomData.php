<?php

namespace App\Core\Traits;

use App\Support\Str;
use Illuminate\Support\Arr;

trait HasCustomData
{
    protected $customField = 'custom_data';

    public function getCustomData($key = null, $default = null)
    {
        $key = $this->filterCustomDataKeyName($key);

        $data = $this->getAttribute($this->customField);

        if ($key === null) {
            return $data;
        }

        return Arr::get($data, $key, $default);
    }

    public function setCustomData($key, $value = null)
    {
        $key = $this->filterCustomDataKeyName($key);

        $data = $this->getCustomData();

        Arr::set($data, $key, $value);

        $this->setAttribute($this->customField, $data);

        return $this;
    }

    public function setBlankCustomData($key)
    {
        $key  = $this->filterCustomDataKeyName($key);
        $data = $this->getCustomData();

        Arr::set($data, $key, null);
        Arr::forget($data, $key);

        $this->setAttribute($this->customField, $data);

        return $this;
    }

    public function unsetCustomData($keys)
    {
        $data = $this->getCustomData();

        if ($data === null) {
            return $this;
        }

        if (! is_array($keys)) {
            $keys = (array)$keys;
        }

        $keys = collect($keys)->map(function ($key) {
            return $this->filterCustomDataKeyName($key);
        })->toArray();

        Arr::forget($data, $keys);

        $this->setAttribute($this->customField, $data);

        return $this;
    }

    public function hasCustomData($key)
    {
        $key = $this->filterCustomDataKeyName($key);

        $data = $this->getCustomData();

        return Arr::has($data, $key);
    }

    public function isCustomDataEmpty($key, $default = null)
    {
        $key = $this->filterCustomDataKeyName($key);

        $data = $this->getCustomData();

        $value = Arr::get($data, $key);

        if (empty($value)) {
            return value($default);
        }

        return $value;
    }

    public function isCustomDataCastable()
    {
        return $this->hasCast($this->customField, ['array']);
    }

    public function filterCustomDataKeyName($key)
    {
        if (Str::endsWith($key, '.')) {
            return Str::substr($key, 0,-1);
        }

        return $key;
    }
}