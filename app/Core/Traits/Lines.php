<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 6/1/2020
 * Time: 11:57 AM
 */

namespace App\Core\Traits;


use RtLopez\Decimal;

trait Lines
{
    public function getUnitCouponDiscountAttribute()
    {
        $value = .0;
        $discountTotal = $this->getAttribute('discount_total');
        $qty = abs($this->getQuantity());

        if ($discountTotal > 0 && $qty > 1) {
            $value = $discountTotal / $qty;

            return $value;
        }

        return $value;
    }

    public function getFinalPriceAttribute()
    {
        return $this->unit_price - $this->unit_discount_amount;
    }

    public function getDiscountedPriceAttribute()
    {
        $unitPrice = $this->getAttribute('unit_price');
        $discountTotal = $this->getAttribute('discount_total');

        $qty = abs($this->getQuantity());

        if ($unitPrice > 0 && $discountTotal > 0) {
            $priceTotal = $unitPrice * $qty;
            $priceTotal -= $discountTotal;

            if ($qty > 1) {
                $unitPrice = $priceTotal / $qty;
            } else {
                $unitPrice = $priceTotal;
            }

            return $unitPrice;
        }

        return $unitPrice;
    }

    public function getPriceToTalAttribute()
    {
        $amount = $this->final_price;

        if ($this->status_level === -2) {
            $amount -= $this->unit_coupon_discount;
        }

        return $amount * $this->getQuantity();
    }

    public function getUnitDiscountAmountAttribute($value)
    {
        return $value ?? .0;
    }

    public function getDiscountTotalAttribute($value)
    {
        return $value ?? .0;
    }

    public function getUnitDiscountTotalAttribute()
    {
        return $this->unit_discount_amount * $this->getQuantity();
    }

    public function getUnitSavingsAttribute()
    {
        if ($this->status_level < 0 || $this->item_type === 'D') {
            return 0;
        }

        if ($this->hasCustomData('prices')) {
            $prices = $this->getCustomData('prices');

            $prices['regular'] = isset($prices['regular']) ? $prices['regular'] : 0;
            $prices['member'] = isset($prices['member']) ? $prices['member'] : 0;

            $regularPrice = $prices['regular'];
            $unitPrice = $prices['member'];

            if (isset($prices['public_promotion']) && Decimal::create($prices['public_promotion'])->gt(0) && Decimal::create($prices['public_promotion'])->eq($prices['member'])) {
                $regularPrice = $prices['public_promotion'];
            }

            if (isset($prices['member_promotion']) && Decimal::create($prices['member_promotion'])->gt(0) && Decimal::create($prices['member_promotion'])->lt($unitPrice)) {
                $unitPrice = $prices['member_promotion'];
            }

            // Exclude savings for channel promotion
            if (isset($prices['public_promotion'], $prices['member_promotion'])
                && Decimal::create($prices['public_promotion'])->gt(0) && Decimal::create($prices['member_promotion'])->gt(0)
                && Decimal::create($prices['public_promotion'])->eq($prices['member_promotion'])) {
                $regularPrice = .0;
                $unitPrice = .0;
            }

            if ($this->getCustomData('price_edited', false) && Decimal::create($this->unit_price)->lt($unitPrice)) {
                $regularPrice = $this->regular_price;
                $unitPrice = $this->unit_price;
            }

        } else {

            $regularPrice = $this->regular_price;
            $unitPrice = $this->unit_price;

            if ($product = $this->product) {
                $unitPrice = $product->mbr_price;
            }

        }

        return ($regularPrice - $unitPrice) + $this->unit_discount_amount;
    }

    public function getTotalSavingsAttribute()
    {
        return $this->unit_savings * $this->getQuantity();
    }

    abstract public function getQuantity();
}