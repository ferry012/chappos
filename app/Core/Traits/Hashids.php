<?php

namespace App\Core\Traits;

trait Hashids
{
    protected $hashidsKey = 'id';

    public function hashids()
    {
        return app('hashids');
    }

    public function getEncodedIdAttribute()
    {
        //return app('hashids')->encode($this->getHashidsKey());
        return $this->getHashidsKey();
    }

    public function decodeId($value)
    {
        /*
        $result = app('hashids')->decode($value);

        return empty($result[0]) ? null : $result[0];
        */
        return $value;
    }

    public function decodeIds($value)
    {
        $ids = [];

        foreach ($value as $id) {
            $realId = app('hashids')->decode($id);
            if (! empty($realId[0])) {
                $ids[] = $realId[0];
            }
        }

        return $ids;
    }

    public function encode($id)
    {
        //return app('hashids')->encode($id);
        return $id;
    }

    public function encodedId()
    {
        //return app('hashids')->encode($this->getHashidsKey());
        return $this->getHashidsKey();
    }

    public function getHashidsKey()
    {
        $key = $this->hashidsKey;

        return $this->{$key};
    }

    public function setHashidsKey($key)
    {
        return $this->hashidsKey = $key;
    }
}