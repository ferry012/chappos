<?php

namespace App\Core\Discounts\Helpers;


use App\Core\Product\Models\Product as ProductModel;
use App\Support\Str;

class Filter
{
    public function match($product, $type, $method, $values, $options, $cart_item = [])
    {

        if ($product instanceof ProductModel) {
            $method     = ! empty($method) ? $method : 'in_list';
            $product_id = $product->item_id;

            if ('all_products' === $type) {
                return true;
            }

            if ('products' === $type) {
                return $this->compareWithProducts($values, $method, $product_id);
            }

            if ('categories' === $type) {
                return $this->compareWithCategories($product, $values, $method);
            }

            if ('brands' === $type) {
                return $this->compareWithBrands($product, $values, $method);
            }
        }

        return false;
    }

    public function matchFilters($product, $filters)
    {
        $rule = new Rule();

        $status = false;

        $product_id = $product->item_id;

        if (! empty($filters)) {

            foreach ($filters as $filter) {

                $type   = $rule->getFilterType($filter);
                $method = $rule->getFilterMethod($filter);
                $values = $rule->getFilterOptionValue($filter);
                $method = ! empty($method) ? $method : 'in_list';

                $processing_result = true;

                if ('all_products' === $type && $values === true) {
                    $processing_result = true;
                } else if ('products' === $type) {
                    $processing_result = $this->compareWithProducts($values, $method, $product_id);
                } elseif ('categories' === $type) {
                    $processing_result = $this->compareWithCategories($product, $values, $method);
                } elseif ('brands' === $type) {
                    $processing_result = $this->compareWithBrands($product, $values, $method);
                }

                if ($processing_result) {
                    $status = true;
                } else {
                    $status = false;
                    break;
                }

            }
        }

        return $status;
    }

    protected function compareWithProducts($operation_values, $operation_method, $product_id)
    {
        return $this->checkInList($product_id, $operation_method, $operation_values);
    }

    protected function compareWithBrands($product, $operation_values, $operation_method)
    {
        return $this->checkInList(optional($product)->brand_id, $operation_method, $operation_values);
    }

    protected function compareWithCategories($product, $operation_values = [], $operation_method)
    {

        $delimiter      = '->';
        $mainCate       = trim(optional($product)->inv_dim3);
        $subCate        = trim(optional($product)->inv_dim4);
        $subCate        = $mainCate.$delimiter.$subCate;
        $includeSubCate = false;

        foreach ($operation_values as $catalog) {
            list(, $subCate1) = array_pad(explode($delimiter, $catalog), 2, null);

            if ($subCate1) {
                $includeSubCate = true;
                break;
            }
        }

        $operation_values = array_map(static function ($item) use ($delimiter) {
            list($mainCate, $subCate) = array_pad(explode($delimiter, $item), 2, null);

            $mainCate = trim($mainCate);

            if ($subCate) {
                $subCate = trim($subCate);

                return $mainCate.$delimiter.$subCate;
            }

            return $mainCate;
        }, $operation_values);

        if (! $includeSubCate) {
            return $this->checkInList($mainCate, $operation_method, $operation_values);
        }

        return $this->checkInList($subCate, $operation_method, $operation_values);
    }

    public function checkInList($value, $operation_method, $operation_values = [])
    {

        if (! is_array($operation_values)) {
            return false;
        }

        if (empty($operation_values)) {
            return true;
        }

        $result = false;

        // Ensure case insensitive
        $value = Str::lower(Str::trim($value));

        foreach ($operation_values as $key => $operation_value) {
            $operation_values[$key] = Str::lower(Str::trim($operation_value));
        }

        if ('in_list' === $operation_method) {
            $result = in_array($value, $operation_values, false);
        } elseif ('not_in_list' === $operation_method) {
            $result = ! in_array($value, $operation_values, false);
        } elseif ('any' === $operation_method) {
            $result = true;
        }

        return $result;
    }
}