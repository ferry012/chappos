<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 20/1/2021
 * Time: 4:52 PM
 */

namespace App\Core\Discounts\Helpers;


use App\Core\Baskets\Models\BasketDiscount;
use App\Core\Baskets\Models\BasketLine;
use App\Core\Discounts\DiscountCalculator;
use App\Core\Discounts\Models\DiscountRule;
use App\Core\Orders\Models\OrderLine;
use App\Support\Str;
use Illuminate\Support\Arr;
use RtLopez\Decimal;

class Rule
{
    public $id;
    public $rule;
    public $model;
    public $conditions;

    public function __construct($rule = null, $conditions = [])
    {
        if ($rule === null) {
            $this->rule = new DiscountRule;
        }

        $this->model = $rule;
        $this->conditions = $conditions;
    }

    public function getAvailableRules($availableConditions, $discountCoupons)
    {
        $rules = collect();

        $availableRules = DiscountRule::where('eff_from', '<', now())
            ->where(static function ($query) {
                $query->where('eff_to', '>', now())
                    ->orWhereNull('eff_to');
            })->get();

        foreach ($availableRules as $key => $availableRule) {

            if (in_array($availableRule->coupon_type, ['PUBLIC', 'INDIVIDUAL'])) {

                $availableRule->priority = 10;

                if ($availableRule->discount_type === 'BULK_DISCOUNT') {
                    $availableRule->priority = 60;
                }

                if ($availableRule->discount_type === 'STEP_DISCOUNT') {
                    $availableRule->priority = 70;
                }

                if ($availableRule->discount_type === 'PRODUCT_DISCOUNT') {
                    $availableRule->priority = 80;
                }

                if ($availableRule->discount_type === 'CART_DISCOUNT') {
                    $availableRule->priority = 490;
                }
            } else {

                if ($availableRule->discount_type === 'BUY_X_GET_X') {
                    $availableRule->priority = 100;
                }

                if ($availableRule->discount_type === 'BULK_DISCOUNT') {
                    $availableRule->priority = 200;
                }

                if ($availableRule->discount_type === 'STEP_DISCOUNT') {
                    $availableRule->priority = 300;
                }

                if ($availableRule->discount_type === 'PRODUCT_DISCOUNT') {
                    $availableRule->priority = 400;
                }

                if ($availableRule->discount_type === 'CART_DISCOUNT') {
                    $availableRule->priority = 500;
                }

                $rules->push($availableRule);
            }

        }

        foreach ($discountCoupons as $discountCoupon) {

            $availableRule = $availableRules->where('coupon_id', $discountCoupon->coupon_id)->first();
            $isRuleExists = $rules->where('coupon_id', $discountCoupon->coupon_id)->count() > 0;

            if ($availableRule === null) {
                continue;
            }

            $availableRule = clone $availableRule;

            $isStackable = $availableRule->getCustomData('is_stackable', false);

            if (!empty($discountCoupon->coupon_code)) {
                $availableRule->coupon_code = $discountCoupon->coupon_code;
            }

            if ($isStackable) {
                $rules->push($availableRule);
                continue;
            }

            if ($isRuleExists) {
                continue;
            }

            $rules->push($availableRule);
        }

        $rules = collect($rules)->sortBy('priority');

        return $this->getRuleObject($rules, $availableConditions);
    }

    public function getRuleObject($rules, $conditions)
    {
        $ruleList = [];

        if (!empty($rules)) {
            $i = 1;

            foreach ($rules as $rule) {
                $rule = clone $rule;
                $ruleObj = new self($rule, $conditions);
                // assign a unique id due to rule can apply more than 1 time etc stacking same coupon
                $ruleObj->id = $i;
                $ruleList[$i] = $ruleObj;
                $i++;
            }
        }

        return $ruleList;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCouponType()
    {
        return $this->model->coupon_type;
    }

    public function getCouponId()
    {
        return $this->model->coupon_id;
    }

    public function getCode()
    {
        return $this->model->coupon_code;
    }

    public function isStackable()
    {
        return $this->model->getCustomData('is_stackable', false);
    }

    public function getTitle()
    {
        return $this->model->coupon_name;
    }

    function getDiscountType()
    {
        if (isset($this->model->discount_type)) {
            return $this->model->discount_type;
        }

        return false;
    }

    public function getFilter()
    {
        return $this->arrayToJson($this->model->discount_filters);
    }

    public function getFilterType($filter)
    {
        if (is_object($filter) && isset($filter->type)) {
            return $filter->type;
        }

        if (is_array($filter) && isset($filter['type'])) {
            return $filter['type'];
        }

        return null;
    }

    public function getFilterMethod($filter)
    {
        if (is_object($filter) && isset($filter->method)) {
            return $filter->method;
        }

        if (is_array($filter) && isset($filter['method'])) {
            return $filter['method'];
        }

        return null;
    }

    public function getFilterOptionValue($filter)
    {
        if (is_object($filter) && isset($filter->value)) {
            return $filter->value;
        }

        if (is_array($filter) && isset($filter['value'])) {
            return $filter['value'];
        }

        return null;
    }

    public function getFilterProductId(){
        $filters = $this->getFilter();

        foreach ($filters as $filter) {
            if ($this->getFilterType($filter) == "products") {
                return $this->getFilterOptionValue($filter);
            }
        }
    }

    public function getConditions()
    {
        return $this->arrayToJson($this->model->discount_conditions);
    }

    public function arrayToJson($data)
    {
        return json_decode(json_encode($data));
    }

    function getPriorityId()
    {
        if (isset($this->model->discount_priority)) {
            return $this->model->discount_priority;
        }

        return null;
    }

    public function getMaxDiscountSum()
    {
        $value = 999999;

        if (($data = $this->getDiscountAdjustment()) && isset($data->options->max_discount)) {
            $value = $data->options->max_discount;
        }

        if (empty($value)) {
            $value = 999999;
        }

        return $value;
    }

    public function getMaxQtyDiscount()
    {

        $value = 999999;

        if (($data = $this->getDiscountAdjustment()) && isset($data->options->discount_qty)) {
            $value = $data->options->discount_qty;
        }

        if (empty($value)) {
            $value = 999999;
        }

        return $value;
    }

    public function applyDiscountAsRebate()
    {

        $value = 'no';

        if (($data = $this->getDiscountAdjustment()) && isset($data->as_rebate)) {
            $value = $data->as_rebate;
        }

        return $value === 'yes';
    }

    public function getMaxUses()
    {

        $value = $this->model->usage_stats->uses_count;

        return $value === null ? 0 : $value;
    }

    public function getUsedCount()
    {

        $value = $this->model->usage_stats->uses_count;

        return $value === null ? 0 : $value;
    }

    public function hasUsageLimitsReached()
    {
        $max = $this->getMaxUses();
        $used = $this->getUsedCount();
        if (!$max) {
            return $used > $max;
        }

        return false;
    }

    public function isCartLevelDiscount()
    {
        return $this->model->coupon_type === 'CART';
    }

    public function isCouponDiscount()
    {
        return in_array($this->getCouponType(), ['PUBLIC', 'INDIVIDUAL'], false);
    }

    public function isCalculatePriceFromRegular()
    {
        if (($data = $this->getDiscountAdjustment()) && isset($data->calculate_from)) {

            $discountType = isset($data->type) ? $data->type : '';

            if (Str::startsWith($discountType, 'fixed')) {
                return false;
            }

            return $data->calculate_from === 'regular_price';
        }

        return false;
    }

    public function isCouponRequired()
    {
        return !empty($this->model->coupon_code);
    }

    public function isEnabled()
    {
        return $this->model->status_level > 0;
    }

    public function hasFilter()
    {
        if (isset($this->model->discount_filters) && !empty($this->model->discount_filters)) {
            return true;
        }

        return false;
    }

    public function hasConditions()
    {
        return !empty($this->model->discount_conditions);
    }

    public function isFilterPassed($product)
    {

        if (!$this->hasFilter()) {
            return true;
        }

        $filters = $this->getFilter();
        $filterPassed = false;

        if (!empty($filters)) {
            $filterHelper = new Filter;
            $filterPassed = $filterHelper->matchFilters($product, $filters);
        }

        return $filterPassed;
    }

    public function isCartConditionsPassed($cart)
    {

        $conditionResults = [];

        $conditions = $this->getConditions();

        foreach ($conditions as $condition) {

            $isConditionsPassed = false;

            $type = isset($condition->type) ? $condition->type : null;
            $options = isset($condition->options) ? $condition->options : [];

            if (empty($type) || empty($options)) {
                continue;
            }

            if (isset($this->conditions[$type]['object']) && is_object($this->conditions[$type]['object'])) {

                $this->conditions[$type]['object']->rule = $this;

                if (method_exists($this->conditions[$type]['object'], 'check')) {
                    $isConditionsPassed = $this->conditions[$type]['object']->check($cart, $options);
                }

            }

            $conditionResults[] = $isConditionsPassed;

        }

        return !in_array(false, $conditionResults, false);
    }

    public function hasDiscountAdjustment()
    {
        return $this->getDiscountAdjustment() !== null;
    }

    public function hasRebateAdjustment()
    {
        $data = $this->getRebateAdjustment();

        if ($data !== null) {
            return isset($data->type, $data->value) && $data->value > 0;
        }

        return false;
    }

    public function getDiscountAdjustment()
    {
        $data = null;

        switch ($this->model->discount_type) {
            case 'PRODUCT_DISCOUNT':
                $data = $this->getAdjustment('product');

                if ($data) {
                    $data = $data->options;
                }

                break;
            case 'CART_DISCOUNT':
                $data = $this->getAdjustment('cart');

                if ($data) {
                    $data = $data->options;

                    if ($data->type === 'flat') {
                        $data->type = 'flat_in_subtotal';
                    }

                }

                break;
            case 'STEP_DISCOUNT':
                $data = $this->getAdjustment('step', false);

                if ($data) {
                    $data = $this->arrayToJson(['ranges' => $data['options']]);
                }

                break;
            case 'SUBTOTAL_STEP_DISCOUNT':
                $data = $this->getAdjustment('subtotal_step', false);

                if ($data) {
                    $data = $this->arrayToJson(['ranges' => $data['options']]);
                }

                break;
            case 'BULK_DISCOUNT':
                $data = $this->getAdjustment('bulk', false);

                foreach ($data['options'] as $key => $option) {
                    if ($option['type'] === 'fixed') {
                        $data['options'][$key]['type'] = 'fixed_set_price';
                    }
                }

                if ($data) {
                    $data = $this->arrayToJson(['ranges' => $data['options']]);
                }

                break;
            case 'BUY_X_GET_X':
                $data = $this->getAdjustment('buy_x_get_x', false);

                if ($data) {
                    $data = $this->arrayToJson(['ranges' => $data['options']]);
                }

                break;
        }

        return $data;
    }

    public function getRebateAdjustment()
    {
        $data = $this->getAdjustment('rebate');

        if ($data) {
            return $data->options;
        }

        return $data;
    }

    public function getAdjustment($type, $toJson = true)
    {
        $adjustment = collect($this->model->discount_adjustments)->where('type', $type)->first();

        if ($adjustment) {

            if ($toJson) {
                return $this->arrayToJson($adjustment);
            }

            return $adjustment;
        }

        return null;
    }

    public function getMessages()
    {
        return $this->arrayToJson($this->model->discount_messages);
    }

    public function getSuccessMessage()
    {
        $messages = $this->getMessages();

        if ($messages && isset($messages->success_message) && !empty($messages->success_message)) {
            return $messages->success_message;
        }

        return null;
    }

    public function calculator($type, $price, $value, $max = null)
    {
        $discount = 0;

        if (empty($value)) {
            return $discount;
        }

        switch ($type) {
            case 'fixed_set_price':
            case 'fixed':
                $discount = $price - $value;

                if ($discount < 0) {
                    $discount = 0;
                }

                break;
            case 'percent':
            case 'percentage':

                if ($value > 100) {
                    $value = 100;
                }

                $discount = $price * ($value / 100);

                if ($max !== null && $max > 0 && $discount > $max) {
                    $discount = $max;
                }

                break;
            case 'flat_in_subtotal':
            case 'flat':
                $discount = $value;
                break;
            default:
                //
        }

        return $discount;
    }

    public function hasUsageReached()
    {
        $discount = $this->model;
        $maxUse = $discount->max_use ?? 0;

        if ($maxUse > 0) {

            $totalCount = array_get($discount->usage_stats, 'total_count', 0);

            if ($totalCount >= $maxUse) {
                return true;
            }

            // pos trans voided at the end of the day
            $offlineCount = BasketLine::where('item_id', $discount->coupon_id)->where('status_level', '>=', 0)
                ->where('created_on', now()->startOfDay())->whereExists(function ($query) {
                    $query->selectRaw('1')
                        ->from('o2o_cart_list')
                        ->whereColumn('o2o_cart_list.id', 'o2o_cart_item.cart_id')
                        ->where('o2o_cart_list.cart_name', 'pos');
                })->count();

            // online trans record will be deleted once cancel order
            $onlineCount = BasketLine::where('item_id', $discount->coupon_id)->where('status_level', '>=', 0)->where('created_on', '>', $this->model->eff_from)
                ->whereExists(function ($query) {
                    $query->selectRaw('1')
                        ->from('o2o_cart_list')
                        ->whereColumn('o2o_cart_list.id', 'o2o_cart_item.cart_id')
                        ->where('o2o_cart_list.cart_name', 'hachi');
                })->count();

            $pendingCount = OrderLine::where('item_id', $discount->coupon_id)->where('created_on', '>', $this->model->eff_from)->whereExists(function ($query) {
                $query->selectRaw('1')
                    ->from('o2o_order_list')
                    ->whereColumn('o2o_order_list.id', 'o2o_order_item.order_id')
                    ->where('o2o_order_list.order_status_level', 0);
            })->count();

            if ($pendingCount > 0 && $pendingCount >= $onlineCount) {
                $pendingCount -= $onlineCount;
            } else {
                $pendingCount += ($onlineCount - $pendingCount);
            }

            if ($pendingCount > 0 && $pendingCount >= $offlineCount) {
                $pendingCount -= $offlineCount;
            } else {
                $pendingCount += ($offlineCount - $pendingCount);
            }

            $totalCount = $pendingCount + $totalCount;

            return $totalCount >= $maxUse;

        }

        return false;
    }
}