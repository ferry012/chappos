<?php

namespace App\Core\Discounts\Contracts;

interface DiscountCriteriaContract
{
    public function check($discount, $basket);
}