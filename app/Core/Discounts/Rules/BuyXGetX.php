<?php

namespace App\Core\Discounts\Rules;


use App\Core\Baskets\Models\Basket;
use App\Core\Discounts\Helpers\Rule;
use Illuminate\Support\Collection;

class BuyXGetX
{
    public function handle(Collection $lines, Rule $rule)
    {
        $discountData  = [];

        if ($lines->isEmpty() || ! $rule->hasDiscountAdjustment()) {
            return $discountData;
        }

        $discountAdjustment = $rule->getDiscountAdjustment();
        $ranges             = collect($discountAdjustment->ranges);

        if ($ranges->isEmpty()) {
            return $discountData;
        }

        $totalQuantity  = $lines->sum('item_qty');
        $recursiveRange = $ranges->where('recursive', 1)->first();

        if (! empty($recursiveRange)) {
            $matchedRule = $this->getMatchedRule([$recursiveRange], $totalQuantity);
        } else {
            $matchedRule = $this->getMatchedRule($ranges, $totalQuantity);
        }

        if (empty($matchedRule)) {
            return $discountData;
        }

        $itemIds = collect();

        // Sort order by lowest price and quantity
        $lines = $lines->sortBy('item_qty')->sortBy('unit_price')->values();

        foreach ($lines as $line) {
            // Repeat n times of id based on qty
            $itemIds->push(collect($line)->times($line->item_qty, static function () use ($line) {
                return $line->id;
            }));

        }

        $itemIds       = $itemIds->flatten();
        $uniqueItemIds = $itemIds->unique()->values()->toArray();

        $adjMode = 'cheapest';

        if ($adjMode === 'highest') {
            $itemIds = $itemIds->reverse();
        } elseif ($adjMode === 'evenly') {
            $itemIds = $itemIds->filter(function ($value, $key) {
                return $key % 2 !== 0;
            });
        }

        $qualifiedQty  = $matchedRule->free_qty;
        $discountType  = $matchedRule->type;
        $discountValue = (float)$matchedRule->value;
        $itemIds       = $itemIds->take($qualifiedQty)->toArray();

        while ($id = array_shift($itemIds)) {

            $line = $lines->where('id', $id)->first();

            $discountPrice = $this->calculateDiscountFromRuleRange($matchedRule, $line->discounted_price);

            if ($discountPrice < 0) {
                $discountPrice = 0;
            }

            if ($discountType === 'free_product') {
                $discountType = 'buy_x_get_x';
            }

            $discountData[] = [
                'rule_id'           => $rule->getId(),
                'discount_type'     => $discountType,
                'discount_value'    => $discountValue,
                'discount_quantity' => 1,
                'discount_per_price'   => $discountPrice,
                'discount_price_total' => $discountPrice,
                'cart_item_key'     => $line->id,
                'cart_item_keys'    => $uniqueItemIds,
            ];

        }

        return $discountData;
    }

    public function calculateDiscountFromRuleRange($matchedRule, $price)
    {
        $discountPrice = 0;

        if (! empty($matchedRule) && ! empty($matchedRule->type)) {

            if ($matchedRule->type === 'percentage') {
                if ($matchedRule->value > 0) {
                    $discountPrice = ($matchedRule->value / 100) * $price;
                }
            } else if ($matchedRule->type === 'flat') {
                if ($matchedRule->value > 0) {
                    $discountPrice = $matchedRule->value;
                }
            } else if ($matchedRule->type === 'free_product') {
                $discountPrice = $price;
            }

        }

        return $discountPrice;
    }

    protected function getMatchedRule($ranges, $quantity)
    {
        $matchedRule = [];

        foreach ($ranges as $key => $range) {

            $start = (int)$range->from;
            $end   = ! empty($range->to) ? (int)$range->to : null;

            if (! empty($range->recursive) && $range->recursive == 1) {
                $freeQuantity = (int)$range->free_qty;

                // Qty need to added first
                $start += $range->free_qty;

                if ($quantity < $start) {
                    $freeQuantity = 0;
                } else {
                    $freeQuantity = $this->getRecursiveQuantity($start, $quantity, $freeQuantity);
                }

                if ($freeQuantity) {
                    $matchedRule           = $range;
                    $matchedRule->free_qty = $freeQuantity;
                }

                return $matchedRule;
            }

            if ($end !== null) {
                if ($start <= $quantity && $end >= $quantity) {
                    $matchedRule = $range;
                    break;
                }
            } else {
                if ($start <= $quantity) {
                    $matchedRule = $range;
                    break;
                }
            }

        }

        return $matchedRule;
    }

    public function getRecursiveQuantity($range_start, $cart_quantity, $freeQuantity)
    {
        if ($cart_quantity < $range_start) {
            return $freeQuantity;
        }

        $customized_range_start = (int)($cart_quantity / $range_start);
        $freeQuantity           = $customized_range_start * $freeQuantity;

        return $freeQuantity;

    }
}