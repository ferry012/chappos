<?php

namespace App\Core\Discounts\Rules;


use App\Core\Discounts\Helpers\Rule;
use Illuminate\Support\Collection;

class StepDiscount
{
    public function handle(Collection $lines, Rule $rule)
    {
        $discountData = [];

        if ($lines->isEmpty() || ! $rule->hasDiscountAdjustment()) {
            return $discountData;
        }

        $discountAdjustment = $rule->getDiscountAdjustment();
        $ranges             = collect($discountAdjustment->ranges);

        if ($ranges->isEmpty()) {
            return $discountData;
        }

        $operator = 'product';

        foreach ($lines as $line) {

            $totalQuantity = 0;

            if ($operator === 'product') {
                $totalQuantity = $lines->where('item_id', $line->item_id)->sum('item_qty');
            } elseif ($operator === 'product_cumulative') {
                $totalQuantity = $lines->sum('item_qty');
            }

            $matchedRule = $this->getMatchedRule($ranges, $totalQuantity);

            if ($matchedRule) {
                $type  = (isset($matchedRule->type) && ! empty($matchedRule->type)) ? $matchedRule->type : false;
                $value = (isset($matchedRule->value) && ! empty($matchedRule->value)) ? $matchedRule->value : 0;

                if ($type && ! empty($value)) {
                    $discountType  = $matchedRule->type;
                    $discountValue = $matchedRule->value;

                    $unitPrice = $line->discounted_price;

                    if ($rule->isCalculatePriceFromRegular()) {
                        $unitPrice = $line->regular_price;
                    }

                    $discountPrice = $rule->calculator($discountType, $unitPrice, $discountValue);

                    if ($rule->isCalculatePriceFromRegular()) {
                        $priceAfterDiscount = $line->regular_price - $discountPrice;

                        if ($line->discounted_price > $priceAfterDiscount) {
                            $discountPrice = $line->discounted_price - $priceAfterDiscount;
                        } else {
                            $discountPrice = 0;
                        }
                    }

                    $discountData[] = [
                        'rule_id'              => $rule->getId(),
                        'discount_type'        => $discountType,
                        'discount_value'       => $discountValue,
                        'discount_quantity'    => $line->item_qty,
                        'discount_per_price'   => $discountPrice,
                        'discount_price_total' => $line->item_qty * $discountPrice,
                        'cart_item_key'        => $line->id,
                        'cart_item_keys'       => [
                            $line->id,
                        ],
                    ];

                }
            }
        }

        return $discountData;
    }

    protected function getMatchedRule($ranges, $quantity)
    {
        $matchedRule = [];

        foreach ($ranges as $range) {
            if (isset($range->value) && ! empty($range->value)) {
                $from = isset($range->from) ? (int)$range->from : 0;
                $to   = isset($range->to) ? (int)$range->to : 0;

                if (empty($to) && empty($from)) {
                    continue;
                }

                if (empty($to) && ! empty($from)) {
                    if ($quantity >= $from) {
                        $matchedRule = $range;
                        break;
                    }
                } elseif (! empty($to) && ! empty($from)) {
                    if ($quantity >= $from && $quantity <= $to) {
                        $matchedRule = $range;
                        break;
                    }
                } elseif (! empty($to) && empty($from)) {
                    if ($quantity <= $to) {
                        $matchedRule = $range;
                        break;
                    }
                }
            }
        }

        return $matchedRule;
    }
}