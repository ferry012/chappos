<?php

namespace App\Core\Discounts\Rules;


use App\Core\Discounts\Helpers\Rule;
use App\Support\Str;
use function Clue\StreamFilter\fun;
use Illuminate\Support\Collection;

class CartDiscount
{
    public function handle(Collection $lines, Rule $rule)
    {
        $discountData = [];

        if ($lines->isEmpty() || ! $rule->hasDiscountAdjustment()) {
            return $discountData;
        }

        $adjustment = $rule->getDiscountAdjustment();

        $discountType  = isset($adjustment->type) ? $adjustment->type : 'flat';
        $discountValue = isset($adjustment->value) ? (float)$adjustment->value : 0;
        $discountMax   = isset($adjustment->max_discount) ? (float)$adjustment->max_discount : 0;
        $label         = isset($adjustment->cart_label) ? $adjustment->cart_label : 'Discount';
        $discountPrice = $discountValue;
        $currentQty    = 1;

        $ids = $lines->pluck('id')->toArray();

        if ($discountType === 'fixed') {
            $currentQty = $lines->sum('item_qty');
        } else if (Str::startsWith($discountType, 'percent')) {
            $total = $lines->sum(static function ($line) {
                return ($line->item_qty * $line->unit_price) - $line->discount_total;
            });

            $discountPrice = $rule->calculator($discountType, $total, $discountValue, $discountMax);
        }

        $discountData[] = [
            'rule_id'              => $rule->getId(),
            'discount_type'        => $discountType,
            'discount_value'       => $discountValue,
            'discount_quantity'    => $currentQty,
            'discount_price'       => $discountPrice,
            'discount_price_total' => $currentQty * $discountPrice,
            'cart_item_key'        => 0,
            'cart_item_keys'       => $ids,
        ];

        return $discountData;
    }
}