<?php

namespace App\Core\Discounts\Rules;


use App\Core\Discounts\Helpers\Rule;
use Illuminate\Support\Collection;

class SimpleDiscount
{
    public function handle(Collection $lines, Rule $rule)
    {
        $ids          = [];
        $discountData = [];

        if ($lines->isEmpty() || ! $rule->hasDiscountAdjustment()) {
            return $discountData;
        }

        $discountAdjustment = $rule->getDiscountAdjustment();
        $discountType       = isset($discountAdjustment->type) ? $discountAdjustment->type : '';
        $discountValue      = isset($discountAdjustment->value) ? (float)$discountAdjustment->value : 0;
        $discountMax        = isset($discountAdjustment->max_discount) ? (float)$discountAdjustment->max_discount : -1;
        $discountQty        = isset($discountAdjustment->discount_qty) ? (int)$discountAdjustment->discount_qty : -1;
        $totalQuantity      = $lines->sum('item_qty');

        if ($discountQty > 0) {

            $totalQuantity = 0;

            foreach ($lines as $line) {
                $currentQty = $line->item_qty;

                if ($currentQty >= $discountQty) {
                    $currentQty  = $discountQty;
                    $discountQty = 0;
                } else {
                    $discountQty -= $currentQty;
                }

                $ids[]         = $line->id;
                $totalQuantity += $currentQty;

                if ($discountQty === 0) {
                    break;
                }


            }
        } else {
            $ids = $lines->pluck('id')->all();
        }

        if (count($ids) > 1) {
            $lines = $lines->whereIn('id', $ids);
        } else {
            $ids   = $ids[0];
            $lines = $lines->where('id', $ids);
        }

        if ($totalQuantity > 0) {
            foreach ($lines as $line) {
                $currentQty = $line->item_qty;

                if ($totalQuantity < 0) {
                    break;
                }

                if ($currentQty >= $totalQuantity) {
                    $currentQty    = $totalQuantity;
                    $totalQuantity = 0;
                } else {
                    $totalQuantity -= $currentQty;
                }

                $unitPrice = $line->discounted_price;

                if ($rule->isCalculatePriceFromRegular()) {
                    $unitPrice = $line->regular_price;
                }

                $discountPrice = $rule->calculator($discountType, $unitPrice, $discountValue, $discountMax);

                if ($rule->isCalculatePriceFromRegular()) {
                    $priceAfterDiscount = $line->regular_price - $discountPrice;

                    if ($line->discounted_price > $priceAfterDiscount) {
                        $discountPrice = $line->discounted_price - $priceAfterDiscount;
                    } else {
                        $discountPrice = 0;
                    }
                }

                $discountData[] = [
                    'rule_id'              => $rule->getId(),
                    'discount_type'        => $discountType,
                    'discount_value'       => $discountValue,
                    'discount_quantity'    => $currentQty,
                    'discount_per_price'   => $discountPrice,
                    'discount_price_total' => $currentQty * $discountPrice,
                    'cart_item_key'        => $line->id,
                    'cart_item_keys'       => [$ids],
                ];

            }
        }

        return $discountData;
    }

}