<?php

namespace App\Core\Discounts\Rules;


use App\Core\Discounts\Helpers\Rule;
use App\Support\Str;
use Illuminate\Support\Collection;

class RebateDiscount
{
    public function handle(Collection $lines, Rule $rule)
    {

        $prefix       = 'rebate_';
        $discountData = [];

        if ($lines->isEmpty() || ! $rule->hasRebateAdjustment()) {
            return $discountData;
        }

        $discountAdjustment = $rule->getRebateAdjustment();
        $discountType       = Str::lower($discountAdjustment->type);
        $discountValue      = (float)$discountAdjustment->value;
        $discountMax        = (float)$discountAdjustment->max_discount;

        if (! in_array($discountType, ['percentage', 'percent', 'flat', 'fixed'], false)) {
            return $discountData;
        }

        if ($discountType === 'flat') {

            $ids = $lines->pluck('id')->values()->all();

            $discountData[] = [
                'rule_id'              => $rule->getId(),
                'discount_type'        => $prefix.$discountType,
                'discount_value'       => $discountValue,
                'discount_quantity'    => 1,
                'discount_price'       => 0,
                'discount_price_total' => 0,
                'cart_item_key'        => 0,
                'cart_item_keys'       => $ids,
            ];

            return $discountData;
        }

        if (! in_array($discountType, ['percentage', 'percent'], false)) {
            $discountMax = 0;
        }

        foreach ($lines as $line) {

            $discountData[] = [
                'rule_id'           => $rule->getId(),
                'discount_type'     => $prefix.$discountType,
                'discount_value'    => $discountValue,
                'discount_max'      => $discountMax,
                'discount_quantity' => 1,
                'discount_price'    => 0,
                'cart_item_key'     => $line->id,
                'cart_item_keys'    => [
                    $line->id,
                ],
            ];
        }

        return $discountData;
    }
}