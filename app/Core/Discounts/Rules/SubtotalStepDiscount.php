<?php

namespace App\Core\Discounts\Rules;


use App\Core\Discounts\DiscountCalculator;
use App\Core\Discounts\Helpers\Rule;
use Illuminate\Support\Collection;

class SubtotalStepDiscount
{
    public function handle(Collection $lines, Rule $rule)
    {
        $discountData = [];

        if ($lines->isEmpty() || !$rule->hasDiscountAdjustment()) {
            return $discountData;
        }

        $discountAdjustment = $rule->getDiscountAdjustment();
        $ranges = collect($discountAdjustment->ranges);

        if ($ranges->isEmpty()) {
            return $discountData;
        }

        $operator = 'product_cumulative';

        $itemTotal = DiscountCalculator::getFilterBasedCartQuantities($lines, 'cart_subtotal', $rule);

        $options = $ranges->filter(function ($item) use ($itemTotal) {
            return $item->price_from <= $itemTotal && $item->price_to >= $itemTotal;
        })->first();

        if (empty($options)) {
            return $discountData;
        }

        $ranges = $options->steps;

        $totalQuantity = 0;

        if ($operator === 'product_cumulative') {
            $totalQuantity = $lines->sum('item_qty');
        }

        $matchedRule = $this->getMatchedRule($ranges, $totalQuantity);

        if ($matchedRule) {
            $type = (isset($matchedRule->type) && !empty($matchedRule->type)) ? $matchedRule->type : false;
            $value = (isset($matchedRule->value) && !empty($matchedRule->value)) ? $matchedRule->value : 0;

            if ($type && !empty($value)) {
                $discountType = $matchedRule->type;
                $discountValue = $matchedRule->value;

                $discountData[] = [
                    'rule_id'              => $rule->getId(),
                    'discount_type'        => $discountType,
                    'discount_value'       => $discountValue,
                    'discount_quantity'    => 1,
                    'discount_per_price'   => 0,
                    'discount_price_total' => 0,
                    'cart_item_key'        => 0,
                    'cart_item_keys'       => $lines->pluck('id')->all()
                ];

            }
        }

        return $discountData;
    }

    protected function getMatchedRule($ranges, $quantity)
    {
        $matchedRule = [];

        foreach ($ranges as $range) {
            if (isset($range->value) && !empty($range->value)) {
                $from = isset($range->from) ? (int)$range->from : 0;
                $to = isset($range->to) ? (int)$range->to : 0;

                if (empty($to) && empty($from)) {
                    continue;
                }

                if (empty($to) && !empty($from)) {
                    if ($quantity >= $from) {
                        $matchedRule = $range;
                        break;
                    }
                } elseif (!empty($to) && !empty($from)) {
                    if ($quantity >= $from && $quantity <= $to) {
                        $matchedRule = $range;
                        break;
                    }
                } elseif (!empty($to) && empty($from)) {
                    if ($quantity <= $to) {
                        $matchedRule = $range;
                        break;
                    }
                }
            }
        }

        return $matchedRule;
    }
}