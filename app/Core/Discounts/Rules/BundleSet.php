<?php

namespace App\Core\Discounts\Rules;


use App\Core\Discounts\Helpers\Rule;
use Illuminate\Support\Collection;

class BundleSet
{
    public function handle(Collection $lines, Rule $rule)
    {
        $discountData = [];

        if ($lines->isEmpty() || ! $rule->hasDiscountAdjustment()) {
            return $discountData;
        }

        $discountAdjustment = $rule->getDiscountAdjustment();
        $ranges             = collect($discountAdjustment->ranges);

        if ($ranges->isEmpty()) {
            return $discountData;
        }

        $operator = 'product';

        if ($operator === 'product') {
            $groupRanges = [];

            $groupItems = $lines->groupBy('item_id')->map(function ($item, $key) {
                $obj          = new \stdClass;
                $obj->item_id = $key;
                $obj->qty     = collect($item)->sum('item_qty');

                return $obj;
            })->values();

            foreach ($groupItems as $item) {

                $totalQuantity = $item->qty;
                $matchedRanges = $this->getMatchedRules($ranges, $totalQuantity);

                if ($matchedRanges) {
                    $groupRanges[$item->item_id] = $matchedRanges;
                }
            }

            if ($groupRanges) {
                foreach ($groupRanges as $itemId => $ranges) {
                    $list = collect();
                    // Sort order by lowest price and quantity
                    $currentLines = $lines->where('item_id', $itemId)->sortBy('item_qty')->sortBy('unit_price')->values();

                    foreach ($currentLines as $line) {
                        // Repeat n times of id based on qty
                        $list->push(collect($line)->times($line->item_qty, static function () use ($line) {
                            return clone $line;
                        }));

                    }

                    $list           = $list->flatten(1);
                    $discountData[] = $this->getDiscountData($rule, $list, $ranges);

                }
            }

        }

        if ($operator === 'product_cumulative') {
            $lines         = $lines->sortBy('item_qty')->sortBy('unit_price')->values();
            $totalQuantity = $lines->sum('item_qty');

            $list = collect();

            foreach ($lines as $line) {
                // Repeat n times of id based on qty
                $list->push(collect($line)->times($line->item_qty, static function () use ($line) {
                    return clone $line;
                }));

            }

            $list          = $list->flatten(1);
            $matchedRanges = $this->getMatchedRules($ranges, $totalQuantity);
            $discountData  = $this->getDiscountData($rule, $list, $matchedRanges);
        }

        return $discountData;

    }

    protected function getDiscountData($rule, $list, $ranges)
    {

        $discountData = [];
        $start        = 0;

        foreach ($ranges as $range) {

            $discountType  = (isset($range->type) && ! empty($range->type)) ? $range->type : false;
            $discountValue = (isset($range->value) && ! empty($range->value)) ? $range->value : 0;
            $end           = $range->from;

            if ($discountType === 'fixed_set_price') {
                $discountValue /= $range->from;
            }

            $lines = $list->slice($start, $end);
            $ids   = $lines->pluck('id')->unique()->values()->all();

            foreach ($lines as $line) {

                $discountPrice = $rule->calculator($discountType, $line->discounted_price, $discountValue);

                if ($discountPrice < 0) {
                    $discountPrice = 0;
                }

                $discountData[] = [
                    'rule_id'              => $rule->getId(),
                    'discount_type'        => $discountType,
                    'discount_value'       => $discountValue,
                    'discount_quantity'    => 1,
                    'discount_per_price'   => $discountPrice,
                    'discount_price_total' => $discountPrice,
                    'cart_item_key'        => $line->id,
                    'cart_item_keys'       => $ids,
                ];
            }


            $start += $range->from;

        }

        return $discountData;
    }

    protected function getMatchedRules($ranges, $quantity)
    {
        $matchedRanges = [];

        while ($quantity > 0) {

            $matchedRule = $this->getMatchedRule($ranges, $quantity);

            if (empty($matchedRule)) {
                break;
            }

            $matchedRanges[] = $matchedRule;
            $quantity        -= $matchedRule->from;
        }

        $setPriceDiscounts = collect($matchedRanges)->where('type', 'fixed_set_price')->sortByDesc('from');

        if ($setPriceDiscounts) {
            $matchedRanges = $setPriceDiscounts->merge(collect($matchedRanges)->where('type', '!=', 'fixed_set_price')->all())->all();
        }

        return $matchedRanges;
    }

    protected function getMatchedRule($ranges, $quantity)
    {
        $fullyMatched = $partiallyMatched = $qualifiedRange = [];

        foreach ($ranges as $key => $range) {
            $maxQty = (isset($range->from) && ! empty($range->from)) ? (int)$range->from : 0;

            if ($quantity === $maxQty) {
                $fullyMatched[$key] = $maxQty;
            } else if ($quantity >= $maxQty) {
                $partiallyMatched[$key] = $maxQty;
            }
        }

        if (empty($fullyMatched)) {

            if (empty($partiallyMatched)) {
                return [];
            }

            $qualifiedRange = $partiallyMatched;
            $matchedRange   = array_keys($qualifiedRange, max($qualifiedRange));
        } else {
            $qualifiedRange = $fullyMatched;
            $matchedRange   = array_keys($qualifiedRange, min($qualifiedRange));
        }

        if (empty($qualifiedRange)) {
            return [];
        }

        $matchedRangeKey = isset($matchedRange[0]) ? $matchedRange[0] : NULL;
        $range           = isset($ranges[$matchedRangeKey]) ? $ranges[$matchedRangeKey] : [];

        return $range;
    }
}