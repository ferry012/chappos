<?php

namespace App\Core\Discounts\Conditions;


use App\Core\Discounts\DiscountCalculator;
use RtLopez\Decimal;

class CartSubTotal extends Base
{

    public function __construct()
    {
        parent::__construct();
        $this->name = 'cart_subtotal';
    }

    public function check($cart, $options)
    {
        $calculateFrom = 'filter';

        if (isset($options->calculate_from) && ! empty($options->calculate_from)) {
            $calculateFrom = $options->calculate_from;
        }

        if (isset($options->operator, $options->value) && $options->value > 0) {
            $operator  = $options->operator;
            $value     = (float)$options->value;
            $itemTotal = (float)$cart->item_total;

            if ($calculateFrom === 'filter') {
                $itemTotal = DiscountCalculator::getFilterBasedCartQuantities($cart, 'cart_subtotal', $this->rule);
            }

            $status = $this->doComparisionOperation($operator, $itemTotal, $value);

            if (! $status) {
                $differenceAmount = $value - $itemTotal;

                if ($differenceAmount > 0) {
                    $message = 'Psst! Spend ${{difference_amount}} more to enjoy this coupon.';
                    $message = str_replace('{{difference_amount}}', number_format($differenceAmount, 2), $message);
                    $this->setError($message);
                }

                return $status;
            }

            return $status;
        }

        return true;
    }

}