<?php

namespace App\Core\Discounts\Conditions;


use App\Support\Str;

class UserRole extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->name = 'member_type';
    }

    public function check($cart, $options)
    {
        if (isset($options->operator, $options->value) && !empty($options->value) && is_array($options->value)) {
            $operator = $options->operator;
            $list = $options->value;
            $userID = $cart->owner_id;
            $userRole = Str::upper(request('customer_group'));

            $list = $this->cleanUpList($list);

            if (empty($list)) {
                return true;
            }

            if (in_array('ALL', $list, false)) {
                return true;
            }

            $status = $this->doCompareInListOperation($operator, $userRole, $list);

            // customer group not found then check user role
            if (!$status && !empty($userID) && !Str::startsWith($userID, 'GT', false)) {
                $userInfo = app('api')->members()->getById($userID);

                if ($userInfo) {
                    $status = $this->doCompareInListOperation($operator, optional($userInfo)->mbr_type, $list);
                }
            }

            if (!$status) {
                $message = 'This coupon is not applicable based on your membership type.';
                $this->setError($message);
            }

            return $status;
        }

        return true;
    }
}