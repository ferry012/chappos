<?php

namespace App\Core\Discounts\Conditions;


class CartItemProductCombination extends Base
{
    public function check($cart, $options)
    {
        if (empty($cart)) {
            return false;
        }

        $result = true;

        if (isset($options->operator, $options->type, $options->value, $options->qty_from) && ! empty($options->value) && is_array($options->value)) {

            $products = $options->value;
            $operator = $options->operator;
            $qtyFrom  = $options->qty_from;

            $products           = array_unique($products);
            $totalQuantitiesArr = array_fill_keys($products, 0);

            $lines = $cart->lines()->get();

            foreach ($lines as $line) {

                $itemId = $line->item_id;

                if (in_array($itemId, $products, false)) {
                    $totalQuantitiesArr[$itemId] += $line->item_qty;
                }
            }

            if (! empty($totalQuantitiesArr)) {
                switch ($options->type) {
                    case 'any':
                        $res = [];
                        foreach ($totalQuantitiesArr as $quantity) {
                            if ($quantity > 0 && $this->doComparisionOperation($operator, $quantity, $qtyFrom)) {
                                $res[] = 1;
                            }
                        }

                        $result = ! empty($res);
                        break;
                    case 'each':
                        $res = [];
                        foreach ($totalQuantitiesArr as $quantity) {
                            if ($quantity > 0) {
                                if (! $this->doComparisionOperation($operator, $quantity, $qtyFrom)) {
                                    $res[] = 0;
                                    break;
                                }
                            } else {
                                $res[] = 0;
                                break;
                            }
                        }

                        $result = empty($res);
                        break;
                    default:
                    case 'combine':
                        $totalQuantitiesArr = array_sum($totalQuantitiesArr);
                        $result             = false;

                        if ($totalQuantitiesArr > 0) {
                            $result = $this->doComparisionOperation($operator, $totalQuantitiesArr, $qtyFrom);
                        }

                        break;
                }
            }

        }

        return $result;
    }
}