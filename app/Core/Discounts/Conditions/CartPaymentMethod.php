<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 28/1/2021
 * Time: 3:06 PM
 */

namespace App\Core\Discounts\Conditions;


class CartPaymentMethod extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->name = 'payment_method';
    }

    public function check($cart, $options)
    {
        if (isset($options->value, $options->operator)) {
            $operator = $cart->operator;
            $value    = $options->value;

            $value = $this->cleanUpList($value);

            if (empty($value)) {
                return true;
            }

            $paymentMethod = '';

            $status = $this->doCompareInListOperation($operator, $paymentMethod, $value);

            if (! $status) {
                // message
            }

            return true;
        }

        return true;
    }
}