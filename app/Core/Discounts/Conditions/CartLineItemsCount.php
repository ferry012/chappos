<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 16/2/2021
 * Time: 11:18 AM
 */

namespace App\Core\Discounts\Conditions;


use App\Core\Discounts\DiscountCalculator;

class CartLineItemsCount extends Base
{

    public function check($cart, $options)
    {
        if (empty($cart)) {
            return true;
        }

        $calculateFrom = 'filter';

        if (isset($options->calculate_from) && ! empty($options->calculate_from)) {
            $calculateFrom = $options->calculate_from;
        }

        if (isset($options->operator, $options->value) && $options->value > 0) {

            $lineCount = $cart->lines->count();
            $operator  = $options->operator;
            $value     = (int)$options->value;

            if ($calculateFrom === 'filter') {
                $lineCount = DiscountCalculator::getFilterBasedCartQuantities($cart, 'cart_line_items_count', $this->rule);
            }

            return $this->doComparisionOperation($operator, $lineCount, $value);
        }

        return true;

    }
}