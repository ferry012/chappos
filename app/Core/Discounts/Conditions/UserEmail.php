<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 20/1/2021
 * Time: 2:44 PM
 */

namespace App\Core\Discounts\Conditions;


use App\Support\Str;

class UserEmail extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->name = 'member_email';
    }

    public function check($cart, $options)
    {
        if (isset($options->operator, $options->value)) {
            $userID = $cart->owner_id;
            $values = $options->value;

            if (empty($userID) || empty($values)) {
                return true;
            }

            $userEmail = '';
            $userInfo  = app('api')->members()->getById($userID);

            if ($userInfo) {
                $userEmail = Str::lower(optional($userInfo)->email);
            }

            if (! is_array($values)) {
                $values = explode(',', $values);
            }

            if (is_array($values) && ! empty($values)) {
                foreach ($values as $key => $value) {
                    $values[$key] = Str::lower(trim(trim($value), '.'));
                }
            }

            switch ($options->operator) {
                case 'email_tld':
                    $emailPart = $this->getTLDFromEmail($userEmail);
                    break;
                default:
                case 'email_domain':
                    $emailPart = $this->getDomainFromEmail($userEmail);
                    break;
            }

            $status = in_array($emailPart, $values, false);

            if (! $status) {
                $message = "Oops! You're not eligible for this coupon redemption based on your email domain.";
                $this->setError($message);
            }

            return $status;
        }

        return true;
    }

    /**
     * Get tld from email
     *
     * @param $email
     *
     * @return string
     */
    protected function getTLDFromEmail($email)
    {
        $emailArray = explode('@', $email);
        if (isset($emailArray[1])) {
            $emailDomainArray = explode('.', $emailArray[1]);
            if (count($emailDomainArray) > 1) {
                unset($emailDomainArray[0]);
            }

            return implode('.', $emailDomainArray);
        }

        return $emailArray[0];
    }

    /**
     * Get domain from email
     *
     * @param $email
     *
     * @return mixed
     */
    protected function getDomainFromEmail($email)
    {
        $emailArray = explode('@', $email);
        if (isset($emailArray[1])) {
            return $emailArray[1];
        }

        return $emailArray[0];
    }
}