<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 20/1/2021
 * Time: 12:31 PM
 */

namespace App\Core\Discounts\Conditions;


class UserList extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->name = 'member_list';
    }

    public function check($cart, $options)
    {
        if (isset($options->value, $options->operator)) {
            $userID = $cart->owner_id;
            $list   = $options->value;

            if (! is_array($list)) {
                $list = (array)$list;
            }

            $list = $this->cleanUpList($list);

            if (empty($list)) {
                return true;
            }

            $status = $this->doCompareInListOperation($options->operator, $userID, $list);

            if (! $status && ! empty($userID)) {
                $userInfo = app('api')->members()->getById($userID);

                if ($userInfo) {
                    $status = $this->doCompareInListOperation($options->operator, optional($userInfo)->email_addr, $list);
                }

            }

            if (! $status) {
                $message = "Oops! You're not eligible for this coupon redemption.";
                $this->setError($message);

            }

            return $status;
        }

        return true;
    }
}