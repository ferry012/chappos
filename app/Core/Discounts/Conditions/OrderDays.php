<?php

namespace App\Core\Discounts\Conditions;


use App\Support\Carbon;
use App\Support\Str;

class OrderDays extends Base
{

    public function check($cart, $options)
    {
        $dayOfWeek = [
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday',
        ];

        if (isset($options->operator, $options->value) && ! empty($options->value) && is_array($options->value)) {
            $days = array_map('strtolower', $options->value);
            $day  = Carbon::now()->format('l');
            $day  = Str::lower($day);

            $status = $this->doCompareInListOperation($options->operator, $day, $days);

            if (! $status) {

                if ($options->operator !== 'in_list') {
                    $days = array_values(array_diff($dayOfWeek, $options->value));
                }

                foreach ($days as $k => $day) {
                    $days[$k] = Str::upperCaseFirst($day);
                }

                $count   = count($days);
                $strDays = $days[0];

                if ($count === 2) {
                    $strDays = implode(' and ', $days);
                } elseif ($count > 2) {
                    $lastDay = array_pop($days);
                    $strDays = implode(', ', $days);
                    $strDays .= ' and '.$lastDay;
                }

                $message = 'Oops! This coupon is only valid for {{days}}.';
                $message = str_replace('{{days}}', $strDays, $message);

                $this->setError($message);
            }

            return $status;
        }

        return true;
    }
}