<?php

namespace App\Core\Discounts\Conditions;


class UserLoggedIn extends Base
{
    public function check($cart, $options)
    {
        if (isset($options->value)) {

            if (empty($options->value)) {
                $options->value = 'yes';
            }

            $isUserLoggedIn = ! empty($cart->owner_id) ? 'yes' : 'no';
            $status         = ($options->value === $isUserLoggedIn);

            if (! $status) {
                $message = 'Please login with your account for this coupon redemption.';
                $this->setError($message);
            }

            return $status;
        }

        return true;
    }
}