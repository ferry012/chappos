<?php

namespace App\Core\Discounts\Conditions;


class InStorePurchase extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->name = 'instore_purchase';
    }

    public function check($cart, $options)
    {
        if ($cart->cart_name === 'pos') {
            return true;
        }

        if (isset($options->value)) {

            if (empty($options->value)) {
                $options->value = 'no';
            }

            $inStore = ! empty($cart->loc_id) && $cart->loc_id !== 'HCL' ? 'yes' : 'no';

            $status = ($options->value === $inStore);

            if (! $status) {
                $this->setError('Oops! This coupon is only valid for Shop In-Store purchases.');
            }

            return $status;
        }

        return true;
    }
}