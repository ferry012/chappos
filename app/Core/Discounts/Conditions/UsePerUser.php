<?php

namespace App\Core\Discounts\Conditions;


use App\Core\Discounts\DiscountCalculator;
use App\Core\Orders\Models\Order;
use App\Core\Orders\Models\OrderDiscount;
use App\Support\Carbon;

class UsePerUser extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->name = 'use_per_member';
    }

    public function check($cart, $options)
    {
        if (isset($options->value)) {
            $perMax = (int)$options->value;

            if (empty($cart->owner_id) || $perMax <= 0) {
                return true;
            }

            $fromDate = Carbon::today()->subMonth(6)->startOfDay();

            $type = $this->rule->model->getCustomData('use_per_member.type', 'once');

            if ($type === 'per_day') {
                $fromDate = Carbon::today()->startOfDay();
            }

            // Check the coupon used count
            $count = Order::where('customer_id', $cart->owner_id)->where('order_status_level', '>', 0)
                ->where('created_on', '>', $fromDate)->whereExists(function ($query) {
                    $query->selectRaw('1')
                        ->from('o2o_order_discount')
                        ->where('coupon_id', $this->rule->model->coupon_id)
                        ->whereRaw('o2o_order_list.id = o2o_order_discount.order_id');
                })->count();

            $status = ($perMax > $count);

            if (!$status) {
                $message = "Oops! You've reached the max redemption limit of {{count}} for this coupon.";
                $message = str_replace('{{count}}', $perMax, $message);
                $this->setError($message);
            }

            return $status;
        }

        return true;
    }
}