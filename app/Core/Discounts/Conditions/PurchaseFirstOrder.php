<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 20/1/2021
 * Time: 12:14 PM
 */

namespace App\Core\Discounts\Conditions;


use App\Core\Discounts\DiscountCalculator;
use App\Core\Orders\Models\Order;

class PurchaseFirstOrder extends Base
{
    public function __construct()
    {
        parent::__construct();
        $this->name = 'first_purchase';
    }

    public function check($cart, $options)
    {

        $userId = $cart->owner_id;

        if (empty($options->value)) {
            $options->value = 'no';
        }

        $firstOrder = ! empty($userId) && $options->value === 'yes';

        if ($firstOrder) {
            $count = Order::where('customer_id', $userId)->where('order_status_level', '>', 0)->count();

            $status = ($count === 0);

            if (! $status) {
                $message = 'Sorry, coupon is only eligible for new members first purchase.';
                $this->setError($message);
            }

            return $status;
        }

        return true;
    }
}