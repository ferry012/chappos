<?php

namespace App\Core\Discounts\Conditions;


use App\Support\Carbon;

class OrderTime extends Base
{

    public function check($cart, $options)
    {
        if (isset($options->from, $options->to) && ! empty($options->from) && ! empty($options->to)) {

            $status  = false;
            $today   = Carbon::now();
            $from    = Carbon::now()->setTimeFromTimeString($options->from);
            $to      = Carbon::now()->setTimeFromTimeString($options->to);
            $message = 'Oops! This coupon is only valid ';

            if (! empty($from) && ! empty($to)) {
                $message .= 'from {{start}} till {{end}}.';
                $status  = $today->getTimestamp() >= $from->getTimestamp() && $today->getTimestamp() <= $to->getTimestamp();
            } elseif (! empty($from) && empty($to)) {
                $message .= 'after {{start}}.';
                $status  = $today->getTimestamp() >= $from->getTimestamp();
            } elseif (empty($from) && ! empty($to)) {
                $message .= 'before {{end}}.';
                $status  = $today->getTimestamp() <= $to->getTimestamp();
            }

            if (! $status) {
                $message = str_replace(['{{start}}', '{{end}}'], [$options->from, $options->to], $message);
                $this->setError($message);
            }

            return $status;
        }

        return true;
    }
}