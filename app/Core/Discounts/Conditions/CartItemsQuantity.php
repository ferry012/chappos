<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 20/1/2021
 * Time: 12:34 PM
 */

namespace App\Core\Discounts\Conditions;


use App\Core\Discounts\DiscountCalculator;

class CartItemsQuantity extends Base
{

    public function check($cart, $options)
    {
        $totalQty      = $cart->item_count;
        $calculateFrom = 'filter';

        if (isset($options->calculate_from) && ! empty($options->calculate_from)) {
            $calculateFrom = $options->calculate_from;
        }

        if (isset($options->operator, $options->value) && $options->value > 0) {
            $operator = $options->operator;
            $value    = (int)$options->value;

            if ($calculateFrom === 'filter') {
                $totalQty = DiscountCalculator::getFilterBasedCartQuantities($cart, 'cart_quantities', $this->rule);
            }

            $status = $this->doComparisionOperation($operator, $totalQty, $value);

            if (! $status) {
                $more    = $value - $totalQty;
                $message = "Psst! You'd need {{count}} more products in your cart to enjoy this coupon!";
                $message = str_replace('{{count}}', $more, $message);
                $this->setError($message);
            }

            return $status;
        }

        return true;
    }
}