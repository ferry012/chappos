<?php

namespace App\Core\Discounts\Conditions;

use App\Core\Discounts\DiscountCalculator;

class StoreLocation extends Base
{

    public function check($cart, $options)
    {
        if (isset($options->operator, $options->value) && ! empty($options->value) && is_array($options->value)) {
            $currentLocations = $cart->loc_id;
            $locations        = $options->value;

            $locations = $this->cleanUpList($locations);

            if (empty($locations)) {
                return true;
            }

            $status = $this->doCompareInListOperation($options->operator, $currentLocations, $locations);

            if (! $status) {
                $message = 'Sorry, coupon is not valid for this store.';
                $this->setError($message);
            }

            return $status;
        }

        return true;
    }
}