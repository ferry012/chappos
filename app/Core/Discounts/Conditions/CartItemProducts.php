<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 10/2/2021
 * Time: 8:35 AM
 */

namespace App\Core\Discounts\Conditions;


class CartItemProducts extends Base
{

    public function check($cart, $options)
    {
        if (empty($cart)) {
            return false;
        }

        $comparisionOperator = isset($options->cartqty) ? $options->cartqty : 'greater_than_or_equal';
        $comparisionQuantity = isset($options->qty_from) ? $options->qty_from : 0;

        $comparisionMethod = isset($options->operator) ? $options->operator : 'in_list';
        $comparisionValue  = (array)isset($options->value) ? $options->value : [];

        if (empty($comparisionValue)) {
            return true;
        }

        $quantity = $not_in_list_quantity = 0;

        foreach ($cart->lines as $line) {

            if (self::$filter->match($line->product, 'products', $comparisionMethod, $comparisionValue, $options)) {

                if ($comparisionMethod === 'not_in_list') {
                    continue;
                }

                $quantity += (int)$line->item_qty;

            } else {
                if ($comparisionMethod === 'not_in_list' && in_array($line->item_id, $comparisionValue, false)) {
                    $not_in_list_quantity += (int)$line->item_qty;
                }
            }

        }

        $cartInList    = [];
        $cartNotInList = [];

        foreach ($cart->lines as $line) {

            if (self::$filter->match($line->product, 'products', $comparisionMethod, $comparisionValue, $options)) {

                if ($comparisionMethod === 'not_in_list') {
                    $cartInList[] = 'yes';
                    continue;
                }

                switch ($comparisionOperator) {
                    case 'less_than':
                        if ($quantity < $comparisionQuantity) {
                            $cartInList[] = 'yes';
                        } else {
                            $cartInList[] = 'no';
                        }
                        break;
                    case 'greater_than_or_equal':
                        if ($quantity >= $comparisionQuantity) {
                            $cartInList[] = 'yes';
                        } else {
                            $cartInList[] = 'no';
                        }
                        break;
                    case 'greater_than':
                        if ($quantity > $comparisionQuantity) {
                            $cartInList[] = 'yes';
                        } else {
                            $cartInList[] = 'no';
                        }
                        break;
                    default:
                    case 'less_than_or_equal':
                        if ($quantity <= $comparisionQuantity) {
                            $cartInList[] = 'yes';
                        } else {
                            $cartInList[] = 'no';
                        }
                        break;
                }

            } else {
                if ($comparisionMethod === 'not_in_list' && in_array($line->item_id, $comparisionValue, false)) {
                    switch ($comparisionOperator) {
                        case 'less_than':
                            if ($not_in_list_quantity < $comparisionQuantity) {
                                $cartNotInList[] = 'no';
                            } else {
                                $cartNotInList[] = 'yes';
                            }
                            break;
                        case 'greater_than_or_equal':
                            if ($not_in_list_quantity >= $comparisionQuantity) {
                                $cartNotInList[] = 'no';
                            } else {
                                $cartNotInList[] = 'yes';
                            }
                            break;
                        case 'greater_than':
                            if ($not_in_list_quantity > $comparisionQuantity) {
                                $cartNotInList[] = 'no';
                            } else {
                                $cartNotInList[] = 'yes';
                            }
                            break;
                        default:
                        case 'less_than_or_equal':
                            if ($not_in_list_quantity <= $comparisionQuantity) {
                                $cartNotInList[] = 'no';
                            } else {
                                $cartNotInList[] = 'yes';
                            }
                            break;
                    }
                }
            }

        }

        if ((! empty($cartInList) && in_array('no', $cartInList, false)) || (! empty($cartNotInList) && in_array('no', $cartNotInList, false))) {
            return false;
        }

        if ((! empty($cartInList) && in_array('yes', $cartInList, false)) || (! empty($cartNotInList) && in_array('yes', $cartNotInList, false))) {
            return true;
        }

        return false;
    }
}