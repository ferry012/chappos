<?php

namespace App\Core\Discounts\Conditions;


use App\Core\Discounts\DiscountFactoryV2;
use App\Core\Discounts\Helpers\Filter;
use App\Support\Str;

abstract class Base
{
    public static $filter;

    public $name    = null;
    public $rule    = null;
    public $label   = null;
    public $message = null;

    public function __construct()
    {
        $this->name   = Str::snake(last(explode('\\', get_class($this))));
        self::$filter = ! empty(self::$filter) ? self::$filter : new Filter();
    }

    abstract public function check($cart, $options);

    public function name()
    {
        return $this->name;
    }

    public function doCartItemsCheck($cart, $options, $type)
    {
        if (empty($cart)) {
            return false;
        }

        return false;
    }

    public function doComparisionOperation($operation, $operand1, $operand2, $operand3 = NULL)
    {
        $result = false;
        switch ($operation) {
            case 'equal_to':
                $result = ($operand1 == $operand2);
                break;
            case 'not_equal_to';
                $result = ($operand1 != $operand2);
                break;
            case 'greater_than';
                $result = ($operand1 > $operand2);
                break;
            case 'less_than';
                $result = ($operand1 < $operand2);
                break;
            case 'greater_than_or_equal';
                $result = ($operand1 >= $operand2);
                break;
            case 'less_than_or_equal';
                $result = ($operand1 <= $operand2);
                break;
            case 'in_range';
                if (! empty($operand3)) {
                    $result = (($operand1 >= $operand2) && ($operand1 <= $operand3));
                }
                break;
            default:
                break;
        }

        return $result;
    }

    public function doCompareInListOperation($operation, $key, $list)
    {
        $result = false;

        if (! is_array($list)) {
            return $result;
        }

        if (is_string($key)) {
            $key = Str::upper(Str::trim($key));
        }

        foreach ($list as $k => $value) {
            $list[$k] = Str::upper(Str::trim($value));
        }

        switch ($operation) {
            case 'not_in_list':
                if (is_array($key) || is_object($key)) {
                    $key = (array)$key;

                    return ! array_intersect($key, $list);
                }

                $result = ! in_array($key, $list, false);

                break;
            case 'in_list';
                if (is_array($key) || is_object($key)) {
                    $key = (array)$key;

                    return array_intersect($key, $list);
                }

                $result = in_array($key, $list, false);

                break;
            default:

        }

        return $result;
    }

    public function cleanUpList($list)
    {
        if (is_array($list)) {
            foreach ($list as $key => $val) {
                if (empty($val)) {
                    unset($list[$key]);
                }
            }
        }
        
        return $list;
    }

    protected function setError($message)
    {
        DiscountFactoryV2::$failedRules[$this->rule->getId()] = $message;
    }
}