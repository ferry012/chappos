<?php

namespace App\Core\Discounts;


use App\Core\Baskets\Models\Basket;

class DiscountCalculator
{
    public static $rules;
    public static $appliedRules                     = [];
    public static $totalDiscounts                   = [];
    public static $cartAdjustments                  = [];
    public static $rebateAdjustments                = [];
    public static $priceDiscountApplyAsCartDiscount = [];
    public static $failedRules                      = [];

    public function __construct($rules)
    {
        self::$rules = $rules;
    }

    public function mayApplyPriceDiscount($product, $quantity, $customPrice = 0, $cartItem, $cart)
    {
        if (empty(self::$rules) || empty($product)) {
            return false;
        }

        $productId              = $product->item_id;
        $matchedItemKey         = isset($cartItem) ? $cartItem->id : $productId;
        $discounts              = [];
        $priceAsCartDiscount    = [];
        $currentApplyAsCartRule = false;
        // Each product run through set of discount rule
        foreach (self::$rules as $rule) {

            $discountType = $rule->getDiscountType();

            if (! $rule->isEnabled()) {
                self::$failedRules[$rule->model->coupon_id] = 'Coupon is inactive';
                continue;
            }

            if (! $rule->isFilterPassed($product)) {
                self::$failedRules[$rule->model->coupon_id] = 'No applicable item for this coupon code';
                continue;
            }

            if ($rule->hasConditions() && ! $rule->isCartConditionsPassed($cart)) {
                continue;
            }

            $ruleId = $rule->getId();

            $productPrice = $product->unit_price;

            if (empty($customPrice)) {
                if ($rule->isCalculatePriceFromRegular()) {
                    $productPrice = $product->regular_price;
                }
            } else {
                $productPrice = $customPrice;
            }

            if ($rule->hasRebateAdjustment()) {
                $rebate                    = $rule->getRebateAdjustment();
                self::$rebateAdjustments[] = [
                    'cart_item_key' => $matchedItemKey,
                    'rule_id'       => $ruleId,
                    'item_id'       => $rule->getCouponId(),
                    'name'          => $rule->getTitle(),
                    'type'          => $rebate->type,
                    'value'         => $rebate->value,
                    'max_discount'  => $rebate->max_discount ?? 0,
                    'message'       => $rule->getSuccessMessage(),
                ];
            }

            $discountedPrice     = $rule->calculateDiscount($productPrice, $quantity, $product, $cart->lines);
            $cartDiscountedPrice = 0;

            if (! is_array($discountedPrice)) {
                $cartDiscountedPrice = $discountedPrice * $quantity;
            } else {
                $discountLabel    = (isset($discountedPrice[0]['label']) && ! empty($discountedPrice[0]['label'])) ? $discountedPrice[0]['label'] : '';
                $discountedPrices = $discountedPrice;
                $discountedPrice  = (isset($discountedPrice[0]['discount_fee']) && ! empty($discountedPrice[0]['discount_fee'])) ? $discountedPrice[0]['discount_fee'] : 0;
                if (isset($discountedPrices[0]['discount_type']) && $discountedPrices[0]['discount_type'] !== 'flat_in_subtotal') {
                    $discountedPrice *= $quantity;
                }
            }

            if (is_bool($discountedPrice)) {
                continue;
            }

            switch ($discountType) {
                case 'PRODUCT_DISCOUNT':
                    if ($rule->hasDiscountAdjustment()) {

                        $simpleDiscount = $rule->getDiscountAdjustment();

                        // Max qty to apply
                        if (isset($simpleDiscount->options->discount_qty) && $simpleDiscount->options->discount_qty > 0) {

                            $totalQty   = $simpleDiscount->options->discount_qty;
                            $appliedQty = 0;

                            if (isset(self::$priceDiscountApplyAsCartDiscount[$rule->getId()])) {
                                $appliedRule = self::$priceDiscountApplyAsCartDiscount[$rule->getId()];
                                $appliedQty  = collect($appliedRule)->sum('discount_qty');
                            }

                            $totalQty -= $appliedQty;

                            if ($totalQty < $quantity) {
                                $quantity = $totalQty;
                            }

                            if ($quantity <= 0) {
                                $discountedPrice = 0;
                                break;
                            }

                            $cartDiscountedPrice = $discountedPrice * $quantity;
                        }

                        $message = $rule->getSuccessMessage();

                        $priceAsCartDiscount[$ruleId][$matchedItemKey] = [
                            'discount_type'    => 'PRODUCT_DISCOUNT',
                            'discount_label'   => isset($simpleDiscount->cart_label) ? $simpleDiscount->cart_label : '',
                            'discount_value'   => $simpleDiscount->value,
                            'discount_qty'     => $quantity,
                            'discounted_price' => $cartDiscountedPrice,
                            'message'          => $message,
                            'rule_name'        => $rule->getTitle(),
                            'cart_item_key'    => isset($cartItem['id']) ? $cartItem['id'] : '',
                            'product_id'       => $productId,
                            'coupon_id'        => $rule->getCouponId(),
                            'rule_id'          => $ruleId,
                        ];

                        $discounts[$ruleId] = $discountedPrice;
                    }
                    break;
                case 'CART_DISCOUNT':
                    if ($rule->hasDiscountAdjustment()) {

                        $cartDiscount           = $rule->getDiscountAdjustment();
                        $currentApplyAsCartRule = true;

                        if (! empty($cartItem)) {

                            $priceAsCartDiscount[$ruleId][$matchedItemKey] = [
                                'discount_type'    => 'CART_DISCOUNT',
                                'apply_type'       => $cartDiscount->type,
                                'discount_label'   => $discountLabel,
                                'discount_value'   => $cartDiscount->value,
                                'discounted_price' => $discountedPrice,
                                'rule_name'        => $rule->getTitle(),
                                'cart_item_key'    => $matchedItemKey,
                                'product_id'       => $productId,
                                'coupon_id'        => $rule->getCouponId(),
                                'rule_id'          => $ruleId,
                            ];

                            $discounts[$ruleId] = (isset($discountedPrices[0]['discount_fee']) && ! empty($discountedPrices[0]['discount_fee'])) ? $discountedPrices[0]['discount_fee'] : 0;

                        }
                    }
                    break;
                case 'STEP_DISCOUNT':
                    if ($rule->hasDiscountAdjustment()) {

                        $currentApplyAsCartRule = true;

                        $priceAsCartDiscount[$ruleId][$matchedItemKey] = [
                            'discount_type'    => 'STEP_DISCOUNT',
                            'discount_label'   => '',
                            'discount_value'   => 0,
                            'discounted_price' => $discountedPrice,
                            'rule_name'        => $rule->getTitle(),
                            'cart_item_key'    => $matchedItemKey,
                            'product_id'       => $productId,
                            'coupon_id'        => $rule->getCouponId(),
                            'rule_id'          => $ruleId,
                        ];

                        $discounts[$ruleId] = $discountedPrice;

                    }
                    break;
                default:

            }

            if ($currentApplyAsCartRule === true) {
                continue;
            }

            if ($discountType === 'CART_DISCOUNT') {
                continue;
            }


            if ($rule->hasDiscountAdjustment()) {
                $discountAdj = $rule->getDiscountAdjustment();

                if (in_array($discountAdj->type, [
                        'percentage', 'fixed',
                    ], false) && $rule->isCalculatePriceFromRegular()
                ) {

                    $diffAmt = $cartItem->regular_price - $cartItem->final_price;

                    if ($discountedPrice >= $diffAmt) {
                        $discountedPrice -= ($cartItem->regular_price - $cartItem->final_price);
                    } else {
                        $discountedPrice = 0;
                    }

                }
            }

            if ($discountedPrice > 0) {

                if (! isset(self::$totalDiscounts[$matchedItemKey][$ruleId])) {
                    self::$totalDiscounts[$matchedItemKey][$ruleId] = [];
                }

                self::$totalDiscounts[$matchedItemKey][$ruleId][] = [];

                $discounts[$ruleId] = $discountedPrice;

            }

        }

        if (empty($discounts)) {

            return false;
        }

        //$applyRuleTo = 'all';
        $applyRuleTo = 'biggest_discount';

        $rules          = $this->pickRule($discounts, $applyRuleTo);
        $validDiscounts = [];
        $discountPrice  = 0;

        if (isset($rules) && ! empty($rules) && ! empty($discounts)) {

            foreach ($rules as $ruleId) {

                if (isset(self::$totalDiscounts[$matchedItemKey][$ruleId])) {
                    $validDiscounts[$matchedItemKey][$ruleId] = self::$totalDiscounts[$matchedItemKey][$ruleId];
                }

                if (! empty($priceAsCartDiscount) && isset($priceAsCartDiscount[$ruleId])) {
                    if (isset(self::$priceDiscountApplyAsCartDiscount[$ruleId])) {
                        self::$priceDiscountApplyAsCartDiscount[$ruleId] = array_merge(self::$priceDiscountApplyAsCartDiscount[$ruleId], $priceAsCartDiscount[$ruleId]);
                    } else {
                        self::$priceDiscountApplyAsCartDiscount[$ruleId] = $priceAsCartDiscount[$ruleId];
                    }
                } else {
                    if (isset(self::$rules[$ruleId], $discounts[$ruleId])) {
                        if (! empty($discounts[$ruleId])) {
                            $discountPrice += $discounts[$ruleId];
                        }
                        self::$appliedRules[$ruleId] = self::$rules[$ruleId];
                    }
                }
            }
        }

        if (! empty($validDiscounts)) {
            unset(self::$totalDiscounts[$matchedItemKey]);
            self::$totalDiscounts[$matchedItemKey] = $validDiscounts[$matchedItemKey];
        }

        $discountedPrice = $cartItem->unit_price - $discountPrice;

        if ($discountedPrice < 0) {
            $discountedPrice = 0;
        }

        return $discountedPrice;
    }

    public function pickRule($matchedRules, $pick)
    {
        $rules = [];

        switch ($pick) {
            case 'all':
                if (! empty($matchedRules)) {
                    foreach ($matchedRules as $ruleId => $discount) {
                        $rules[] = $ruleId;
                        if (isset(self::$rules[$ruleId])) {
                            self::$appliedRules[$ruleId] = self::$rules[$ruleId];
                        }
                    }
                }
                break;
            case 'biggest_discount':
                $ruleIdList = array_keys($matchedRules, max($matchedRules));
                $ruleId     = reset($ruleIdList);
                $rules[]    = $ruleId;
                if (isset(self::$rules[$ruleId])) {
                    self::$appliedRules[$ruleId] = self::$rules[$ruleId];
                }
                break;
            case 'lowest_discount':
                $ruleIdList = array_keys($matchedRules, min($matchedRules));
                $ruleId     = reset($ruleIdList);
                $rules[]    = $ruleId;
                if (isset(self::$rules[$ruleId])) {
                    self::$appliedRules[$ruleId] = self::$rules[$ruleId];
                }
                break;
            default:
            case 'first':
                reset($matchedRules);
                $ruleId  = key($matchedRules);
                $rules[] = $ruleId;
                if (isset(self::$rules[$ruleId])) {
                    self::$appliedRules[$ruleId] = self::$rules[$ruleId];
                }
                break;
        }

        return $rules;
    }


    public static function getFilterBasedCartQuantities($cart, $conditionType, $rule)
    {

        if ($cart instanceof Basket) {
            $lines = $cart->lines;
        } else {
            $lines = $cart;
        }

        $filterCalculateValues = 0;

        foreach ($lines as $line) {
            if ($rule->isFilterPassed($line->product)) {
                if ($conditionType === 'cart_subtotal') {
                    $filterCalculateValues += ($line->item_qty * $line->unit_price) - $line->discount_total;
                } elseif ($conditionType === 'cart_quantities') {
                    $filterCalculateValues += (int)$line->item_qty;
                } elseif ($conditionType === 'cart_line_items_count') {
                    $filterCalculateValues++;
                } else {
                    return 0;
                }
            }
        }

        return $filterCalculateValues;
    }
}