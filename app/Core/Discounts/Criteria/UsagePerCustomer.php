<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 21/6/2019
 * Time: 11:06 AM
 */

namespace App\Core\Discounts\Criteria;


use App\Core\Discounts\Contracts\DiscountCriteriaContract;

class UsagePerCustomer extends Criteria implements DiscountCriteriaContract
{
    public $priority = 70;

    public function check($discount, $basket)
    {

        if (! $discount->is_public && $discount->uses_per_customer > 0) {
            $this->error = 'Coupon is limit to 1 per customer';

            return false;
        }

        return true;
    }
}