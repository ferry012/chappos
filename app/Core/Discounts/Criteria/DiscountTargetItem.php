<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 21/6/2019
 * Time: 11:08 AM
 */

namespace App\Core\Discounts\Criteria;


use App\Core\Discounts\Contracts\DiscountCriteriaContract;

class DiscountTargetItem extends Criteria implements DiscountCriteriaContract
{
    public $priority = 80;

    public function check($discount, $basket)
    {
        if ($discount->discount_target === 'ITEM') {

            $lines = $basket->lines()->active()->get();

            if ($basket->lines->isEmpty()) {
                $this->error = 'No qualified product.';
            }

            if ($discount->min_order_qty > 0 && $discount->min_order_qty > $basket->item_count) {
                $this->error = '';
            }

            if ($discount->min_order_amount > 0 && $discount->min_order_amount > $lines->sum('price_total')) {
                $this->error = 'Coupon code only valid with minimum spend of S$'.$discount->min_order_amount.' on selected products.';
            }

            if ($discount->min_order_qty > 0 && $discount->min_order_qty > $lines->sum('item_qty')) {
                $this->error = '';
            }

        }
    }
}