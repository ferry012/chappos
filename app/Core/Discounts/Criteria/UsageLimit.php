<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 21/6/2019
 * Time: 11:05 AM
 */

namespace App\Core\Discounts\Criteria;


use App\Core\Baskets\Models\BasketDiscount;
use App\Core\Discounts\Contracts\DiscountCriteriaContract;

class UsageLimit extends Criteria implements DiscountCriteriaContract
{
    public $priority = 60;

    public function check($discount, $basket)
    {

        if ($discount->max_uses > 0) {

            $maxUses         = $discount->max_uses;
            $appliedCount    = $discount->uses_count;
            $includeLocation = false;
            $currLocation    = request('loc_id');

            // Check location usage
            if ($discount->hasCustomData('uses_per_location')) {
                $usesPerLocation = $discount->getCustomData('uses_per_location', []);

                if (array_key_exists($currLocation, $usesPerLocation)) {
                    $includeLocation = true;
                    $maxUses         = $usesPerLocation[$currLocation];

                    $usesLocations = $discount->getCustomData('uses_locations_count', []);

                    if (isset($usesLocations[$basket->loc_id])) {
                        $appliedCount = $usesLocations[$basket->loc_id];
                    }

                }
            }

            $pendingCount = BasketDiscount::where('coupon_id', $discount->coupon_id)->applied()->active()->whereExists(function ($query) use ($includeLocation, $currLocation) {
                $query->selectRaw('1')
                    ->from('o2o_cart_list')
                    ->when($includeLocation, function ($query) use ($currLocation) {
                        $query->where('loc_id', $currLocation);
                    })
                    ->whereRaw('o2o_cart_list.id = o2o_cart_discount.cart_id');
            })->count();

            $total = $pendingCount + $appliedCount;

            if ($total > $maxUses) {

                $this->error = 'Coupon has been fully redeemed.';

                return false;
            }

        }

        return true;
    }
}