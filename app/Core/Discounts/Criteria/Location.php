<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 21/6/2019
 * Time: 11:05 AM
 */

namespace App\Core\Discounts\Criteria;


use App\Core\Discounts\Contracts\DiscountCriteriaContract;

class Location extends Criteria implements DiscountCriteriaContract
{
    public $priority = 40;

    public function check($discount, $basket)
    {
        if ($discount->loc_id !== '*') {

            $locations    = explode(',', $discount->loc_id);
            $currLocation = request()->get('loc_id', user_data('loc_id'));

            if (! in_array($currLocation, $locations, false)) {
                $this->error = 'Coupon is not applicable for current location.';

                return false;
            }

        }

        return true;
    }
}