<?php

namespace App\Core\Discounts\Criteria;


use App\Core\Discounts\Contracts\DiscountCriteriaContract;

class IsPublic extends Criteria implements DiscountCriteriaContract
{
    public $priority = 20;

    public function check($discount, $basket)
    {
        $userId = $basket->owner_id;

        if (! $discount->is_public && empty($userId)) {
            $this->error = 'Coupon is applicable for member only.';

            return false;
        }

        return true;
    }
}