<?php

namespace App\Core\Discounts\Criteria;


use App\Core\Discounts\Contracts\DiscountCriteriaContract;
use RtLopez\Decimal;

class ApplyDiscount extends Criteria implements DiscountCriteriaContract
{

    public $priority = 999999;

    public function check($discount, $basket)
    {
        $ids                = [];
        $itemDiscount       = 0;
        $itemsDiscountTotal = 0;
        $lines              = $lines1 = $basket->lines->sortByDesc('unit_price');
        $discountQtyBalance = $discount->discount_qty ?? -1;
        $stackQty           = $discount->stack_qty ?? -1;
        $couponPromoId      = $discount->getCustomData('cpn_promo_id');
        $discountLines      = collect();
        $totalQtyApplied    = 0;
        $currQtyApplied     = 0;

        if ($discountQtyBalance > 0 || $stackQty > 0) {

            $appliedDiscounts = $basket->discounts()->applied()->where('coupon_id', $discount->coupon_id)->get();

            $appliedDiscountCount = $appliedDiscounts->count();

            // Check for stack coupon
            if ($stackQty > 0) {
                $stackQty -= $appliedDiscountCount;

                if ($stackQty <= 0) {
                    $this->error = 'Coupon max stack is '.$discount->stack_qty.' only';

                    return false;
                }
            }

            if ($discountQtyBalance > 0) {

                $maxQtyToApply = ($appliedDiscountCount + 1) * $discountQtyBalance;

                foreach ($appliedDiscounts as $appliedDiscount) {
                    $totalQtyApplied += $appliedDiscount->getCustomData('qty_applied', 0);
                }

                $discountQtyBalance = $maxQtyToApply - $totalQtyApplied;
            }


        }


        /*
        if ($discount->discount_qty > 0) {
            //  $appliedDiscount = $basket->discounts()->where('coupon_id', $discount->coupon_id)->applied()->count();
            //   $discountMaxQtyLimit = $discountQtyBalance * ($appliedDiscount + 1);

            $lineCount = $lines->count();
            $aLines    = $lines->sortBy('id')->sortByDesc('unit_price')->values();
            $storeQty  = [];
            $newLines  = collect();

            // Repeatedly n times of row according to item quantity
            for ($a = 0; $a < $highestQty; $a++) {
                for ($b = 0; $b < $lineCount; $b++) {

                    $aaLine = clone $aLines->get($b);

                    if (! isset($storeQty[$b])) {
                        $storeQty[$b] = (int)$aaLine->item_qty;
                    }

                    if ($storeQty[$b] === 0) {
                        continue;
                    }

                    $aaLine->item_qty = 1;
                    $newLines->push($aaLine);
                    $storeQty[$b]--;
                }
            }

            $appliedDiscountQty = $basket->discounts()->where('coupon_id', $discount->coupon_id)->applied()->count();

            // Get the max quantity could apply
            $appliedDiscountItemQty = $appliedDiscountQty * $discount->discount_qty;

            // More than the current qty, set the max quantity to current qty
            if ($appliedDiscountItemQty > $totalQty) {
                $appliedDiscountItemQty = $totalQty;
            }

            // Remove coupon applied line
            $lines = $newLines->slice($appliedDiscountItemQty);

        }
        */

        if ($discount->discount_target === 'ITEM') {
            foreach ($lines as $line) {

                $lineQty = (int)$line->item_qty;

                if ($totalQtyApplied > 0) {

                    // Full qty applied should skip
                    if ($totalQtyApplied >= $lineQty) {
                        $totalQtyApplied -= $lineQty;
                        continue;
                    }

                    $lineQty         -= $totalQtyApplied;
                    $totalQtyApplied = 0;
                }

                if ($discountQtyBalance === 0) {
                    break;
                }

                // check discount qty limit
                if ($discountQtyBalance > 0) {
                    if ($discountQtyBalance >= $lineQty) {
                        $discountQtyBalance -= $lineQty;
                    } else {
                        $lineQty            = $discountQtyBalance;
                        $discountQtyBalance = 0;
                    }
                }

                switch ($discount->discount_unit) {
                    case 'PERCENT':

                        $price           = ! $discount->discount_regular_price ? $line->final_price : $line->regular_price;
                        $unitDiscountAmt = $this->applyPercentage($price, $discount->discount_value);

                        // reg price 10, unit price 8, given 50% off
                        // after disc 5, 4
                        // 10 - 8 = 2
                        // 5 - 2 = 3 off
                        if ($discount->discount_regular_price) {

                            $diffAmt = $line->regular_price - $line->final_price;

                            if ($unitDiscountAmt >= $diffAmt) {
                                $unitDiscountAmt -= ($line->regular_price - $line->final_price);
                            } else {
                                $unitDiscountAmt = 0;
                            }

                        }

                        // Discount capped amount
                        if ($discount->max_discount > 0 && $unitDiscountAmt > $discount->max_discount) {
                            $unitDiscountAmt = $discount->max_discount;
                        }

                        $itemDiscount = $unitDiscountAmt;

                        break;
                    case 'FIXED':

                        $unitDiscountAmt = $discount->discount_value;

                        if ($discount->discount_regular_price) {
                            $unitDiscountAmt -= ($line->regular_price - $line->final_price);
                        }

                        $itemDiscount = $unitDiscountAmt;

                        break;
                    case 'FIXED_TO':
                        $itemDiscount = $this->applyFixedToAmount($line->unit_price, $discount->discount_value);

                        break;
                    case 'PROMO':
                        // Discount value base on campaign item promo price
                        if ($couponPromoId) {

                            $promoItem = db()->table('v_ims_live_promo')->where('promo_id', $couponPromoId)->where('promo_ref_id', $discount->coupon_id)->where('ref_id', $line->item_id)
                                ->where(function ($q) {
                                    $q->whereDate('eff_from', '<=', today()->toDateString())->whereTime('time_from', '<=', today()->toTimeString());
                                })
                                ->where(function ($q) {
                                    $q->whereDate('eff_to', '>=', today()->toDateString())->whereTime('time_to', '>=', today()->toTimeString());
                                })->first();

                            if (! $promoItem) {
                                continue 2;
                            }

                            $itemDiscount = $promoItem->promo_price;
                        }
                        break;
                    default:
                        continue 2;
                        break;
                }

                $currQtyApplied    += $lineQty;
                $itemDiscountTotal = ($itemDiscount * $lineQty);

                if ($discount->getCustomData('discount_line_breakdown', false)) {
                    $discountLines->push([
                        'id'        => $line->id,
                        'item_desc' => $line->item_desc,
                        'amount'    => -1 * $itemDiscount,
                        'qty'       => $lineQty,
                    ]);
                }

                $ids[]                = $line->id;
                $itemsDiscountTotal   += $itemDiscountTotal;
                $line->discount_total += $itemDiscountTotal;
                $line->save();
            }

            $discount->qty_applied = $currQtyApplied;

            // Discount not applied on any error
            if ($discount->discount_qty > 0 && $discount->discount_qty === $discountQtyBalance) {
                $this->error = 'No more applicable item.';

                return false;
            }

        }

        $discountTotal = $itemsDiscountTotal;

        if ($discount->discount_target === 'TOTAL') {

            $ids = $lines->pluck('id');

            // assume is fixed amount
            $discountTotal       = $discount->discount_value;
            $qualifiedItemsTotal = $lines->sum('price_total');

            // Discount amount should not exceed the qualified total
            if ($qualifiedItemsTotal > 0 && $discountTotal > $qualifiedItemsTotal) {
                $discountTotal = $qualifiedItemsTotal;
            }

            if ($discount->discount_unit === 'PERCENT') {
                $discountTotal = $this->applyPercentage($qualifiedItemsTotal, $discount->discount_value);

                if ($discount->max_discount > 0 && $discountTotal > $discount->max_discount) {
                    $discountTotal = $discount->max_discount;
                }
            }

            $discount->total_amount_deducted = $discountTotal;

            foreach ($lines1->whereIn('id', $ids) as $line) {
                $line->setCustomData('spread_discount', true);
                $line->save();
            }

            $discount->qty_applied = $lines->sum('item_qty');
        }

        foreach ($lines1->whereIn('id', $ids) as $line) {

            // Add record
            $tmpDiscounts   = $line->getCustomData('discounts', []);
            $tmpDiscounts[] = [
                'coupon_id'   => $discount->coupon_id,
                'coupon_type' => $discount->coupon_type,
                'coupon_name' => $discount->coupon_name,
                'coupon_code' => $discount->coupon_code,
            ];

            $line->unsetCustomData('tracking_id');

            // For report purpose to claim discount from vendor
            $trackingId = else_null(optional($discount)->promo_id, $discount->getCustomData('tracking_id'));

            if (! empty($trackingId)) {
                $line->setCustomData('tracking_id', $trackingId);
            }

            $line->setCustomData('discounts', $tmpDiscounts);
            $line->save();
        }

        $dbBasketDiscount = $basket->discounts()->where('id', $discount->cart_discount_id)->first();

        $reqRefId = false;

        if ($dbBasketDiscount) {
            $dbBasketDiscount->coupon_amount = -$discountTotal;

            // Coupon require reference id
            if ($discount->hasCustomData('require_ref')) {
                $reqRefId = $discount->getCustomData('require_ref');
                $dbBasketDiscount->setCustomData('require_ref', $reqRefId);
            }

            $dbBasketDiscount->save();
        }

        if ($discountLines->isNotEmpty()) {
            foreach ($discountLines as $discountLine) {
                $dbInsData = [
                    'item_id'       => $discount->coupon_id,
                    'item_type'     => 'D',
                    'item_desc'     => $discount->coupon_name.' - '.$discountLine['item_desc'],
                    'item_qty'      => $discountLine['qty'],
                    'regular_price' => $discountLine['amount'],
                    'unit_price'    => $discountLine['amount'],
                    'status_level'  => 0,
                ];

                if ($reqRefId && $couponRefId = $basket->getCustomData('coupon_ref')) {
                    $dbInsData['custom_data'] = ['lot_id' => [$couponRefId]];
                }

                $basket->lines()->create($dbInsData);
            }
        } else {
            // add a discount line
            $dbInsData = [
                'item_id'       => $discount->coupon_id,
                'item_type'     => 'D',
                'item_desc'     => $discount->coupon_name,
                'item_qty'      => 1,
                'regular_price' => -$discountTotal,
                'unit_price'    => -$discountTotal,
                'status_level'  => 0,
            ];

            if ($reqRefId && $couponRefId = $basket->getCustomData('coupon_ref')) {
                $dbInsData['custom_data'] = ['lot_id' => [$couponRefId]];
            }

            $basket->lines()->create($dbInsData);
        }

    }

    protected function applyPercentage($price, $amount)
    {
        return $price * ($amount / 100);
    }

    protected function applyFixedToAmount($price, $amount)
    {
        // Get the diff amount
        if (Decimal::create($price)->gt($amount)) {
            return $price - $amount;
        }

        return .0;
    }
}