<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 21/6/2019
 * Time: 11:13 AM
 */

namespace App\Core\Discounts\Criteria;

abstract class Criteria
{
    public    $priority = 0;
    protected $stop     = false;
    protected $error;

    public function stop()
    {
        $this->stop = true;
    }

    public function error()
    {
        return $this->error;
    }

    public function clearError()
    {
        $this->error = '';
    }
}