<?php

namespace App\Core\Discounts\Criteria;

use App\Core\Discounts\Contracts\DiscountCriteriaContract;

class DiscountTargetSubTotal extends Criteria implements DiscountCriteriaContract
{
    public $priority = 80;

    public function check($discount, $basket)
    {

        if ($discount->discount_target === 'TOTAL') {
            if ($discount->min_order_amount > 0 && $discount->min_order_amount > $basket->item_total) {
                $this->error = 'Coupon code only valid with minimum spend of S$'.$discount->min_order_amount.'.';
            }

            if ($discount->min_order_qty > 0 && $discount->min_order_qty > $basket->item_count) {
                $this->error = 'only for member only';
            }

        }

    }
}