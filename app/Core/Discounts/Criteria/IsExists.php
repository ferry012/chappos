<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 21/6/2019
 * Time: 5:51 PM
 */

namespace App\Core\Discounts\Criteria;


use App\Core\Discounts\Contracts\DiscountCriteriaContract;

class IsExists extends Criteria implements DiscountCriteriaContract
{
    public $priority = 5;

    public function check($discount, $basket)
    {
        if ($discount === null || ! $discount->exists) {
            $this->error = 'Not a valid code.';
        }
    }
}