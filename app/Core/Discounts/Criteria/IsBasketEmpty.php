<?php

namespace App\Core\Discounts\Criteria;

use App\Core\Discounts\Contracts\DiscountCriteriaContract;

class IsBasketEmpty extends Criteria implements DiscountCriteriaContract
{
    public $priority = 0;

    public function check($discount, $basket)
    {
        if ($basket->item_count === 0) { // before filter lines
            $this->error = 'Cart should not be empty.';
        } elseif ($basket->lines->isEmpty()) { // after filtered lines
            $this->error = 'No applicable item.';
        }
    }

}