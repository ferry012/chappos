<?php

namespace App\Core\Discounts\Criteria;

use App\Core\Discounts\Contracts\DiscountCriteriaContract;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DateRange extends Criteria implements DiscountCriteriaContract
{
    public $priority = 50;
    public $error;

    public function check($discount, $basket = null)
    {

        $now  = Carbon::now();
        $from = Carbon::parse($discount->eff_from);

        if ($from->isFuture()) {
            $this->error = 'Coupon have not start yet.';

            return false;
        }

        if ($discount->eff_to !== null) {
            $to = Carbon::parse($discount->eff_to);

            if ($now->greaterThan($to)) {
                $this->error = 'Coupon is ended.';

                return false;
            }

        }

        return true;
    }
}