<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 21/6/2019
 * Time: 2:32 PM
 */

namespace App\Core\Discounts\Criteria;


use App\Core\Discounts\Contracts\DiscountCriteriaContract;

class IsActive extends Criteria implements DiscountCriteriaContract
{
    public $priority = 10;

    public function check($discount, $basket)
    {
        if ($discount->status_level === 0) {
            $this->error = 'Coupon is not activated.';
        }
    }
}