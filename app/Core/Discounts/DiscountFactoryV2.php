<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 20/1/2021
 * Time: 5:39 PM
 */

namespace App\Core\Discounts;


use App\Core\Baskets\Models\Basket;
use App\Core\Discounts\Conditions\Base as ConditionBase;
use App\Core\Discounts\Helpers\Rule;
use App\Core\Discounts\Models\DiscountRule;
use App\Core\Discounts\Rules\BundleSet;
use App\Core\Discounts\Rules\BuyXGetX;
use App\Core\Discounts\Rules\CartDiscount;
use App\Core\Discounts\Rules\RebateDiscount;
use App\Core\Discounts\Rules\SimpleDiscount;
use App\Core\Discounts\Rules\StepDiscount;
use App\Core\Discounts\Rules\SubtotalStepDiscount;
use App\Support\Str;
use RtLopez\Decimal;

class DiscountFactoryV2
{
    public static $availableRules = [];
    public static $calculator;
    public static $calculatedSpreadDiscounts = [];
    public static $failedRules = [];

    public $discountRule;
    public $availableConditions = [];

    public function __construct()
    {
        $this->discountRule = new DiscountRule;

    }

    public function setBasket(Basket $basket)
    {
        $this->basket = $basket;

        return $this;
    }

    public function getDiscountRules()
    {
        if (empty(self::$availableRules)) {
            $ruleHelper = new Rule;
            self::$availableRules = $ruleHelper->getAvailableRules($this->getAvailableConditions(), $this->basket->discounts);
        }

        self::$calculator = new DiscountCalculator(self::$availableRules);

        return self::$availableRules;
    }

    public function getAvailableRules($availableConditions = [])
    {
        $rules = $this->discountRule->get();

        $discountCoupon = $this->basket->discounts;

        foreach ($rules as $key => $rule) {

            if ($rule->isCouponDiscount()) {

                // Get generic coupon
                $count = $discountCoupon->where('coupon_id', $rule->coupon_id)->count();

                if ($count === 0) {
                    unset($rules[$key]);
                    continue;
                }
            }

            $rule->available_conditions = $availableConditions;
        }

        self::$availableRules = $rules;

        return self::$availableRules;
    }

    public function getAvailableConditions()
    {
        //Read the conditions directory and create condition object
        if (file_exists(__DIR__.'/Conditions/')) {
            $conditions_list = array_slice(scandir(__DIR__.'/Conditions/'), 2);
            if (!empty($conditions_list)) {
                foreach ($conditions_list as $condition) {
                    $class_name = basename($condition, '.php');
                    if ($class_name !== 'Base') {
                        $condition_class_name = 'App\Core\Discounts\Conditions\\'.$class_name;
                        if (class_exists($condition_class_name)) {
                            $condition_object = new $condition_class_name();
                            if ($condition_object instanceof ConditionBase) {
                                $rule_name = $condition_object->name();
                                if (!empty($rule_name)) {
                                    $this->availableConditions[$rule_name] = [
                                        'object' => $condition_object,
                                    ];
                                }
                            }
                        }
                    }
                }
            }
        }

        return $this->availableConditions;
    }

    public function resolve()
    {

        // Remove discount lines
        $this->basket->lines()->where('item_type', 'D')->delete();
        // Get latest lines details
        $this->basket->refresh();

        // Reset discount amount to zero
        // Excluded PWP items
        $this->basket->lines->map(static function ($line) {
            $line->discount_total = 0;
            $line->unsetCustomData('discounts');
            $line->unsetCustomData('rewards');

            return $line;
        });

        if ($this->basket->lines->isEmpty()) {
            return;
        }

        $this->getDiscountRules();

        $_rules = self::$availableRules;

        $discounts = [];
        $rebateDiscounts = [];

        foreach ($_rules as $rule) {

            $ruleType = Str::lower($rule->model->discount_type);
            $returnDiscounts = [];
            $returnRebateDiscounts = [];
            $skipApplied = false;

            if (!$rule->isEnabled()) {
                self::$failedRules[$rule->id] = 'Promotion is inactive.';
                continue;
            }

            if ($rule->hasUsageReached()) {
                self::$failedRules[$rule->id] = 'Coupon code has been fully redeemed!';
                continue;
            }

            if ($ruleType === 'cart_discount') {
                $skipApplied = true;
            }

            if ($rule->model->getCustomData('exception', false)) {
                $skipApplied = true;
            }

            $lines = $this->getDiscountableItems($this->basket, $rule, $discounts, $skipApplied);

            if ($lines->isEmpty()) {
                if (!isset(self::$failedRules[$rule->id])) {
                    self::$failedRules[$rule->id] = 'No applicable item line.';
                }
                // Skip current rule
                continue;
            }

            if ($rule->hasRebateAdjustment()) {
                $returnRebateDiscounts = (new RebateDiscount)->handle($lines, $rule);
            }

            if ($ruleType === 'step_discount') {
                $returnDiscounts = (new StepDiscount)->handle($lines, $rule);
            } elseif ($ruleType === 'buy_x_get_x') {
                $returnDiscounts = (new BuyXGetX)->handle($lines, $rule);
            } elseif ($ruleType === 'cart_discount') {
                $returnDiscounts = (new CartDiscount)->handle($lines, $rule);
            } elseif ($ruleType === 'bulk_discount') {
                $returnDiscounts = (new BundleSet)->handle($lines, $rule);
            } elseif ($ruleType === 'product_discount') {
                $returnDiscounts = (new SimpleDiscount)->handle($lines, $rule);
            } elseif ($ruleType === 'subtotal_step_discount') {
                $returnDiscounts = (new SubtotalStepDiscount)->handle($lines, $rule);
            }

            // check the stack unique coupon discount no more than balance amount
            if ($returnDiscounts && $rule->isStackable()) {

                $discountTotal = collect($discounts)->flatten(1)->sum('discount_price_total');

                if ($discountTotal <= 0) {

                    $discountTotal = $returnDiscounts[0]['discount_price_total'];
                } else {
                    $discountTotal += $returnDiscounts[0]['discount_price_total'];
                }

                $totalAfterDiscount = $this->basket->item_total - $discountTotal;

                if ($totalAfterDiscount < 0) {
                    self::$failedRules[$rule->id] = 'Discount more than the balance.';

                    $returnDiscounts = [];
                }

            }

            if ($returnDiscounts) {
                $additionalRebateDiscounts = collect($returnDiscounts)->filter(function ($item) {
                    return Str::startsWith($item['discount_type'], 'rebate_');
                })->all();

                if (!empty($additionalRebateDiscounts)) {
                    $rebateDiscounts += $additionalRebateDiscounts;
                }
            }

            if (!empty($returnRebateDiscounts)) {
                $rebateDiscounts += $returnRebateDiscounts;
            }

            if (!empty($returnDiscounts)) {

                $isGroupArray = isset($returnDiscounts[0][0]);

                if (!$isGroupArray) {
                    $tempReturnDiscounts = $returnDiscounts;
                    $returnDiscounts = [];
                    $returnDiscounts[0] = $tempReturnDiscounts;
                }

                foreach ($returnDiscounts as $returnDiscount) {
                    $discounts[] = $returnDiscount;
                    $this->spreadDiscountAmongItems($lines, $returnDiscount);
                }

            } else {
                if (empty($returnRebateDiscounts)) {
                    if (!isset(self::$failedRules[$rule->id])) {
                        self::$failedRules[$rule->id] = 'No applicable item..';
                    }
                }
            }

        }

        $lines = $this->basket->lines;

        if ($rebateDiscounts) {
            $discounts[] = $rebateDiscounts;
        }

        // Get the highest rebate
        // Each item entitled one reward only
        $rebateDiscounts = collect($rebateDiscounts)->filter(static function ($item) {
            // Rebate do not need to spread discount amount
            return in_array($item['discount_type'], [
                'rebate_fixed',
                'rebate_percent',
                'rebate_percentage',
                'rebate_flat'
            ], false);
        })->map(static function ($rule) use ($lines) {

            $factor = 1; // Assume is 1
            $id = $rule['cart_item_key'];
            $type = $rule['discount_type'];
            $value = $rule['discount_value'];

            $rule['discount_points'] = 0;

            $line = $lines->where('id', $id)->first();

            if ($line) {

                switch ($type) {
                    case 'rebate_fixed':
                        $factor = $value;
                        break;
                    case 'rebate_percent':
                    case 'rebate_percentage':
                        $factor *= $value;
                        break;
                }

                $rule['discount_points'] = floor($factor * $line->discounted_price) * 100;
            }

            return $rule;
        })->sortByDesc('discount_points')->unique('cart_item_key')->values();

        foreach ($rebateDiscounts as $rebateDiscount) {
            $ruleId = $rebateDiscount['rule_id'];
            $discount = .0;

            $rule = self::$availableRules[$ruleId];

            $rule->getCouponId();
            $rule->getTitle();

            $line = $this->basket->lines()->where('item_id', $rule->getCouponId())->first();

            if (empty($line)) {
                $dbInsData = [
                    'item_id'        => $rule->getCouponId(),
                    'item_type'      => 'D',
                    'item_desc'      => $rule->getTitle(),
                    'item_qty'       => 1,
                    'regular_price'  => $discount,
                    'unit_price'     => $discount,
                    'discount_total' => .0,
                    'status_level'   => 0,
                ];

                $line = $this->basket->lines()->create($dbInsData);
            }

            $line->save();
        }

        $appliedDiscounts = collect($discounts)->flatten(1)->groupBy('rule_id')->map(static function ($item) {

            $rules = collect($item);
            $discountTotal = $rules->sum('discount_price_total');
            $appliedLines = $rules->pluck('cart_item_keys')->flatten(1)->unique()->values()->all();
            $rebateFlat = $rules->where('discount_type', 'rebate_flat')->first();

            $obj = new \stdClass;
            $obj->rule_id = $item[0]['rule_id'];
            $obj->discount_total = $discountTotal;
            $obj->trans_points = 0;
            $obj->applied_lines = $appliedLines;

            if ($rebateFlat) {
                $obj->trans_points = $rebateFlat['discount_value'] * 100;
            }

            return $obj;

        });

        // Add discount line to cart
        foreach ($appliedDiscounts as $appliedDiscount) {
            $ruleId = $appliedDiscount->rule_id;
            $discountTotal = $appliedDiscount->discount_total * -1;
            $rule = self::$availableRules[$ruleId];

            $itemIds = $this->basket->lines->whereIn('id', $appliedDiscount->applied_lines)->pluck('item_id')->all();
            $line = $this->basket->lines()->where('item_id', $rule->getCouponId())->first();

            if ($rule->isCouponDiscount()) {
                $successMessage = $rule->getSuccessMessage();
                $discount = $this->basket->discounts()->where('coupon_id', $rule->getCouponId())->where('coupon_code', $rule->getCode())->first();

                if ($discount) {

                    if (!empty($successMessage)) {
                        $discount->error_msg = $successMessage;
                    }

                    $discount->coupon_name = $rule->getTitle();
                    $discount->coupon_type = $rule->getCouponType();
                    $discount->coupon_amount = $discountTotal;
                    $discount->is_applied = true;

                    $discount->save();

                }
            }

            if (empty($line)) {
                $dbInsData = [
                    'item_id'        => $rule->getCouponId(),
                    'item_type'      => 'D',
                    'item_desc'      => $rule->getTitle(),
                    'item_qty'       => 1,
                    'regular_price'  => $discountTotal,
                    'unit_price'     => $discountTotal,
                    'discount_total' => .0,
                    'status_level'   => 0,
                ];

                $line = $this->basket->lines()->create($dbInsData);
            } else {

                if ($rule->isStackable()) {
                    $line->item_qty += 1;
                }

                $line->regular_price = $discountTotal;
                $line->unit_price = $discountTotal;
                $line->discount_total = .0;
            }

            if ($appliedDiscount->trans_points > 0) {
                $line->setCustomData('item_ids', $itemIds);
                $line->setCustomData('unit_points_earned', $appliedDiscount->trans_points);

                $rebateAmount = app('api')->products()->convertPointsToRebate($appliedDiscount->trans_points);

                if (number_of_decimals($rebateAmount) === 0) {
                    $rebateAmount = (int)$rebateAmount;
                } else {
                    $rebateAmount = number_format($rebateAmount, 2);
                }

                $line->item_desc = sprintf('%s (Rebate $%s)', $rule->getTitle(), $rebateAmount);
            }

            $line->save();
        }

        $failedRules = self::$failedRules;

        if (!empty($failedRules)) {
            foreach ($failedRules as $ruleId => $message) {

                $rule = self::$availableRules[$ruleId];
                $couponId = $rule->getCouponId();

                if (!empty($rule->getCode())) {
                    $discount = $this->basket->discounts()->where('coupon_id', $rule->getCouponId())->where('coupon_code', $rule->getCode())->first();
                } else {
                    $discount = $this->basket->discounts()->where('coupon_id', $couponId)->first();
                }

                if ($discount) {
                    $discount->error_msg = $message;
                    $discount->save();
                    break;
                }

            }
        }

        foreach ($lines as $line) {

            $id = (string)$line->id;

            if (isset(self::$calculatedSpreadDiscounts[$id])) {

                // Add discount record
                $tmpDiscounts = $line->getCustomData('discounts', []);

                foreach (self::$calculatedSpreadDiscounts[$id] as $ruleId => $spreadDiscount) {
                    $rule = self::$availableRules[$ruleId];

                    $tmpDiscounts[] = [
                        'rule_id'        => $rule->model->id,
                        'coupon_id'      => $rule->getCouponId(),
                        'coupon_name'    => $rule->getTitle(),
                        'discount_type'  => Str::lower($rule->getDiscountType()),
                        'discount_total' => $spreadDiscount,
                    ];

                }

                $line->setCustomData('discounts', $tmpDiscounts);

            }

            if ($rebateDiscounts->isNotEmpty()) {
                $rebateDiscount = $rebateDiscounts->where('cart_item_key', $line->id)->first();

                if ($rebateDiscount) {
                    $pointsArr = null;

                    if ($rebateDiscount['discount_type'] === 'rebate_percentage') {
                        $pointsArr = [
                            'type'         => 'percentage',
                            'value'        => $rebateDiscount['discount_value'],
                            'max_discount' => isset($rebateDiscount['discount_max']) ? $rebateDiscount['discount_max'] : 0,
                        ];
                    } elseif ($rebateDiscount['discount_type'] === 'rebate_fixed') {
                        $pointsArr = [
                            'type'  => 'fixed',
                            'value' => $rebateDiscount['discount_value'],
                        ];
                    } elseif ($rebateDiscount['discount_type'] === 'rebate_flat') {
                        $pointsArr = [
                            'type'  => 'flat',
                            'value' => $rebateDiscount['discount_value'],
                        ];
                    }

                    $line->setCustomData('rewards.points', $pointsArr);
                }
            }

            $line->save();
        }

    }

    protected function getDiscountableItems(Basket $basket, Rule $rule, $discounts, $skipApplied = false)
    {
        $self = $this;

        // Clone for cart conditions check to prevent overwrite
        $_basket = clone $basket;

        // filter out exception items that can't apply discount
        $lines = $_basket->lines->filter(static function ($line) use ($rule) {

            $mainLine = true;

            if ($line->parent_id !== null && $line->parent_id > 0) {
                $mainLine = false;
            }

            $return = $mainLine && $line->status_level >= 0;

            $ruleProducts = $rule->getFilterProductId();
            if ($return && is_array($ruleProducts) && in_array($line->item_id,$ruleProducts)) {
                // If this item is in filter, allow it to apply no matter what
                $return = true;
            }
            else if ($return) {
                $return = !in_array($line->item_type, ['C', 'G', 'H'], false);
            }
            return $return;
        });

        if (!$skipApplied) {
            $lines = $lines->filter(static function ($item) use ($self, $discounts) {
                return !$self->isItemAppliedDiscount($item, $discounts);
            });
        }

        $_basket->lines = $lines;

        $lines = $lines->filter(static function ($item) use ($_basket, $rule) {

            if ($rule->isFilterPassed($item->product)) {
                if ($rule->hasConditions()) {
                    return $rule->isCartConditionsPassed($_basket);
                }

                return true;
            }

            return false;
        });

        return $lines;
    }

    protected function isItemAppliedDiscount($item, $discounts)
    {
        $discounts = collect($discounts)->flatten(1)->pluck('cart_item_keys')->flatten(1)->unique()->values()->all();

        return in_array($item->id, $discounts, false);
    }

    protected function spreadDiscountAmongItems($lines, $discounts)
    {

        $spreadDiscountPerLine = true;
        $discounts = collect($discounts);

        $discounts = $discounts->filter(static function ($item) {
            // Rebate do not need to spread discount amount
            return !Str::startsWith($item['discount_type'], 'rebate_');
        });

        if ($discounts->isEmpty()) {
            return $lines;
        }

        $ruleId = $discounts->first()['rule_id'];
        $discountCount = $discounts->count();
        $keyCount = $discounts->where('cart_item_key', '>', 0)->sum(static function ($item) {
            return count($item['cart_item_keys']);
        });

        if ($discountCount === $keyCount) {
            $spreadDiscountPerLine = false;
        }

        if (!$spreadDiscountPerLine) {
            foreach ($lines as $line) {
                $filteredDiscounts = $discounts->where('cart_item_key', $line->id);

                if ($filteredDiscounts->isNotEmpty()) {

                    $discountTotal = $filteredDiscounts->sum('discount_price_total');

                    $id = (string)$line->id;
                    self::$calculatedSpreadDiscounts[$id][$ruleId] = $discountTotal;

                    $line->discount_total += $discountTotal;
                }

            }

            return $lines;
        }

        // Grab all the qualified item key
        $ids = $discounts->pluck('cart_item_keys')->flatten()->unique()->values()->all();

        $lines = $lines->whereIn('id', $ids);

        $discountTotal = $discounts->sum('discount_price_total');

        $linesTotal = $lines->sum(static function ($line) {

            $total = $line->unit_price * $line->item_qty;
            $total -= $line->discount_total;

            return $total;
        });

        $spreadDiscountTotal = 0;

        foreach ($lines as $line) {
            $lineTotal = $line->unit_price * $line->item_qty;
            $lineTotal -= $line->discount_total;

            $discountPerLine = $this->spreadCalculator($lineTotal, $linesTotal, $discountTotal);
            $spreadDiscountTotal += $discountPerLine;

            $id = (string)$line->id;
            self::$calculatedSpreadDiscounts[$id][$ruleId] = $discountTotal;

            $line->discount_total += $discountPerLine;
        }

        $discountVarianceAmount = Decimal::create($discountTotal)->sub($spreadDiscountTotal)->toString();

        // Discount variance adjustment
        if (Decimal::create($discountVarianceAmount)->gt(0)) {
            $line = $lines->where('price_total', '>=', $discountVarianceAmount)->sortByDesc('discount_total')->first();
            $line->discount_total += $discountVarianceAmount;

            $id = (string)$line->id;
            $discountTotal = self::$calculatedSpreadDiscounts[$id][$ruleId];
            self::$calculatedSpreadDiscounts[$id][$ruleId] = $discountTotal + $discountVarianceAmount;

        }

        if (Decimal::create($discountVarianceAmount)->lt(0)) {
            $line = $lines->sortByDesc('discount_total')->first();
            $line->discount_total += $discountVarianceAmount;

            $id = (string)$line->id;
            $discountTotal = self::$calculatedSpreadDiscounts[$id][$ruleId];
            self::$calculatedSpreadDiscounts[$id][$ruleId] = $discountTotal + $discountVarianceAmount;
        }

        return $lines;
    }

    protected function spreadCalculator($price, $total, $discountTotal)
    {
        return ($price / $total) * $discountTotal;
    }
}