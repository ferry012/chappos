<?php

namespace App\Core\Discounts;

use App\Core\Baskets\Models\Basket;
use App\Core\Discounts\Contracts\DiscountFactoryContract;
use App\Core\Discounts\Criteria\ApplyDiscount;
use App\Core\Discounts\Criteria\DateRange;
use App\Core\Discounts\Criteria\DiscountPercentage;
use App\Core\Discounts\Criteria\DiscountTargetItem;
use App\Core\Discounts\Criteria\DiscountTargetSubTotal;
use App\Core\Discounts\Criteria\IsActive;
use App\Core\Discounts\Criteria\IsBasketEmpty;
use App\Core\Discounts\Criteria\IsExists;
use App\Core\Discounts\Criteria\IsPublic;
use App\Core\Discounts\Criteria\Location;
use App\Core\Discounts\Criteria\UsageLimit;
use App\Core\Discounts\Criteria\UsagePerCustomer;
use App\Support\Str;
use Illuminate\Support\Collection;
use RtLopez\Decimal;

class DiscountFactory implements DiscountFactoryContract
{
    protected $basket;

    protected $lines;

    protected $criteria;

    protected $discounts;

    protected $user;

    public function __construct()
    {
        $this->criteria = new Collection();

        $this->pushCriteria(new IsBasketEmpty())
            ->pushCriteria(new IsExists())
            ->pushCriteria(new IsActive())
            ->pushCriteria(new DateRange())
            ->pushCriteria(new DiscountPercentage())
            ->pushCriteria(new DiscountTargetItem())
            ->pushCriteria(new DiscountTargetSubTotal())
            ->pushCriteria(new IsPublic())
            ->pushCriteria(new Location())
            ->pushCriteria(new UsagePerCustomer())
            ->pushCriteria(new UsageLimit())
            ->pushCriteria(new UsagePerCustomer())
            ->pushCriteria(new ApplyDiscount());
    }

    public function setBasket(Basket $basket)
    {
        $this->basket = $basket;

        return $this;
    }

    public function setDiscounts($discounts)
    {
        $this->discounts = $discounts;

        return $this;
    }

    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    public function getBasketLines($discount)
    {
        $this->basket->refresh();
        $hasItemCondition = false;

        foreach ($discount->only([
            'items_only', 'items_except', 'item_brands', 'item_brands_except', 'item_catalogs', 'item_catalogs_except',
        ]) as $field) {
            if (! empty($field)) {
                $hasItemCondition = true;
                break;
            }
        }

        if ($hasItemCondition) {

            $eligibleItems = db()->table('v_ims_live_product')->where('coy_id', coy_id())->when($discount->item_brands, function ($q) use ($discount) {

                return $q->whereIn('brand_id', explode(',', $discount->item_brands));
            })->when($discount->item_brands_except, function ($q) use ($discount) {

                return $q->whereNotIn('brand_id', $discount->item_brands_except);
            })->when($discount->items_only, function ($q) use ($discount) {

                return $q->whereIn('item_id', explode(',', $discount->items_only));
            })->when($discount->items_except, function ($q) use ($discount) {

                return $q->whereNotIn('item_id', explode(',', $discount->items_except));
            })->when($discount->item_catalogs, function ($q) use ($discount) {

                $catalogs = explode(';', $discount->item_catalogs);

                $q->where(function ($q) use ($catalogs) {

                    foreach ($catalogs as $catalog) {

                        $q->orWhere(function ($q) use ($catalog) {

                            list($parCatalog, $subCatalog) = array_pad(explode(',', $catalog), 2, null);

                            $q->where('inv_dim2', $parCatalog);

                            if ($subCatalog) {
                                $q->Where('inv_dim3', $subCatalog);
                            }
                        });
                    }

                });

                return $q;
            })->when($discount->item_catalogs_except, function ($q) use ($discount) {
                $catalogs = explode(';', $discount->item_catalogs_except);

                $q->where(function ($q) use ($catalogs) {

                    foreach ($catalogs as $catalog) {

                        $q->orWhere(function ($q) use ($catalog) {

                            list($parCatalog, $subCatalog) = explode(',', $catalog);

                            if ($subCatalog == null) {
                                $q->where('inv_dim2', '!=', $parCatalog);
                            } else {
                                $q->where('inv_dim2', '=', $parCatalog)->Where('inv_dim3', '!=', $subCatalog);
                            }

                        });
                    }

                });

                return $q;
            })->pluck('item_id');

        } else {
            $eligibleItems = $this->basket->lines->pluck('item_id');
        }

        $eligibleItems = $eligibleItems->map(function ($id) {
            return Str::trim($id);
        });

        // Exclude void or refund line
        $qualifiedBasketLines = $this->basket->lines->whereIn('item_id', $eligibleItems->toArray())->where('unit_price', '>', 0)->where('status_level', '>=', 0);
        /*
        // commented, due to Apple promotion allow to have additional discount
        ->filter(function ($line) {

            return ($line->parent_id === null || $line->parent_id === 0);
        });
        */

        return $qualifiedBasketLines;
    }

    public function pushCriteria($criteria)
    {
        if (is_string($criteria)) {
            $criteria = new $criteria;
        }

        $this->criteria->push($criteria);

        return $this;
    }

    public function getCriteria()
    {
        // Sort criteria ascending
        return $this->criteria->sortBy(function ($class) {
            return $class->priority;
        });
    }

    protected function checkCriteria()
    {
        $er = [];
        //dd($this->discounts);
        //dd($this->getCriteria());

        // clear all discounts records
        $this->basket->lines->each(function ($line) {
            $line->unsetCustomData('discounts');
            $line->save();
        });

        foreach ($this->discounts as $index => $discount) {
            $valid               = true;
            $this->basket->lines = $this->getBasketLines($discount);

            foreach ($this->getCriteria() as $criteria) {

                $criteria->check($discount, $this->basket);

                if (! empty($criteria->error())) {
                    $valid = false;
                    $this->basket->discounts()
                        ->where('id', $discount->cart_discount_id)
                        ->update([
                            'id'          => $discount->cart_discount_id,
                            'coupon_type' => $discount->coupon_type,
                            'coupon_name' => $discount->coupon_name,
                            'is_applied'  => false,
                            'error_msg'   => $criteria->error(),
                        ]);

                    $criteria->clearError();

                    break;
                }
            }

            if ($valid) {

                $this->spreadDiscount($discount, $this->basket);

                $discount1              = $this->basket->discounts()->where('id', $discount->cart_discount_id)->first();
                $discount1->coupon_type = $discount->coupon_type;
                $discount1->coupon_name = $discount->coupon_name;
                $discount1->is_applied  = true;
                $discount1->setCustomData('qty_applied',$discount->qty_applied);
                $discount1->save();

            }

        }

        return $er;
    }

    protected function spreadDiscount($discount, $basket)
    {
        if ($discount->discount_target !== 'TOTAL') {
            return;
        }

        $this->basket->refresh('lines');

        $lines = $this->basket->lines->where('status_level', '>=', 0);

        $discountTotal = $discount->total_amount_deducted;

        // get the discount items only
        $lines = $lines->filter(function ($line) {
            return $line->hasCustomData('spread_discount') && $line->getCustomData('spread_discount', false);
        });

        if ($lines->isEmpty()) {
            return;
        }

        $itemTotal           = $lines->sum('price_total');
        $spreadDiscountLines = collect();

        foreach ($lines as $line) {

            // spread discount
            $spreadAmount         = ((($line->price_total) / $itemTotal) * $discountTotal);
            $line->discount_total += $spreadAmount;
            $spreadDiscountLines->push(Decimal::create($spreadAmount)->round(2)->toString());

            $line->save();
        }

        $spreadDiscountTotal = $spreadDiscountLines->sum();

        $discountVarianceAmount = Decimal::create($discountTotal)->sub($spreadDiscountTotal)->toString();

        // spread discount variance add to the highest amount
        if (Decimal::create($discountVarianceAmount)->gt(0)) {
            $line                 = $lines->sortByDesc('discount_total')->first();
            $line->discount_total += $discountVarianceAmount;
            $line->save();
        }
    }

    public function resolve()
    {
        $this->basket->lines()->where('item_type', 'D')->delete();
        // reset discount amount to zero
        $this->basket->lines->map(function ($line) {
            $line->discount_total = 0;

            return $line;
        });

        $res = $this->checkCriteria();

        //   $this->spreadDiscount();

        return $res;
    }

}