<?php

namespace App\Core\Discounts\Models;


use App\Core\Scaffold\BaseModel;

class DiscountRule extends BaseModel
{
    protected $connection = 'pg';

    protected $table = 'crm_coupon_list';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'mbr_type'             => 'json',
        'loc_id'               => 'json',
        'discount_filters'     => 'json',
        'discount_conditions'  => 'json',
        'discount_adjustments' => 'json',
        'discount_messages'    => 'json',
        'pay_mode'             => 'json',
        'usage_stats'          => 'json',
        'custom_data'          => 'json',
    ];


    public function getAvailableRules()
    {

        return $this;

    }

}