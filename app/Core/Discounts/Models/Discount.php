<?php

namespace App\Core\Discounts\Models;

use App\Core\Scaffold\BaseModel;

class Discount extends BaseModel
{
    protected $connection = 'pg';

    //protected $table = 'o2o_coupon_list';
    protected $table = 'crm_coupon_promo';

    public function getMinOrderAmountAttribute()
    {
        return $this->attributes['min_order_amount'] ?? 0;
    }
}