<?php

namespace App\Core\Criteria;

abstract class Criteria
{
    /**
     * @param $model
     *
     * @return mixed
     */
    abstract public function apply($model);
}