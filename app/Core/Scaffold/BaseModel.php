<?php

namespace App\Core\Scaffold;

use App\Core\Traits\HasCustomData;
use App\Core\Traits\Hashids;
use App\Support\Str;
use Illuminate\Database\Eloquent\Model;

abstract class BaseModel extends Model
{
    use Hashids;
    use HasCustomData;

    const CREATED_AT = 'created_on';
    const UPDATED_AT = 'modified_on';

    protected $dateFormat = 'Y-m-d H:i:s.u';

    /**
     * Return a timestamp as DateTime object.
     *
     * @param  mixed $value
     *
     * @return \Illuminate\Support\Carbon
     */
    protected function asDateTime($value)
    {
        // Datetime with milliseconds of 000 will be truncated by framework
        // append to avoid Carbon raise error
        if (! Str::contains($value, '.')) {
            $value .= '.000';
        }

        return parent::asDateTime($value);
    }

    /**
     * Convert a DateTime to a storable string.
     * SQL Server will not accept 6 digit second fragment (PHP default: see getDateFormat Y-m-d H:i:s.u)
     * trim three digits off the value returned from the parent.
     *
     * @param  \DateTime|int $value
     *
     * @return string
     */
    public function fromDateTime($value)
    {
        $datetime = parent::fromDateTime($value);

        if (Str::endsWith($datetime, '.000000') && Str::contains($this->getConnection()->getConfig('driver'), 'sqlsrv')) {
            $datetime = Str::removeRight($datetime, '000');
        }

        return $datetime;
    }

    /**
     * Get the format for database stored dates.
     *
     * @return string
     */
    public function getDateFormat()
    {
        if (windows_os()) {
            //$this->dateFormat = 'Y-m-d H:i:s.u';
        }

        return $this->dateFormat ?: $this->getConnection()->getQueryGrammar()->getDateFormat();
    }

    public function setModifiedByAttribute($value)
    {
        $this->attributes['modified_by'] = substr($value, 0, 15);
    }

    /**
     * Determine if the given relationship (method) exists.
     *
     * @param  string $key
     *
     * @return bool
     */
    public function hasRelation($key)
    {
        // If the key already exists in the relationships array, it just means the
        // relationship has already been loaded, so we'll just return it out of
        // here because there is no need to query within the relations twice.
        if ($this->relationLoaded($key)) {
            return true;
        }

        // If the "attribute" exists as a method on the model, we will just assume
        // it is a relationship and will load and return results from the query
        // and hydrate the relationship's value on the "relationships" array.
        if (method_exists($this, $key)) {
            //Uses PHP built in function to determine whether the returned object is a laravel relation
            return is_a($this->$key(), "Illuminate\Database\Eloquent\Relations\Relation");
        }

        return false;
    }

    public function trim()
    {
        return [];
    }

    /**
     * Set the array of model attributes. No checking is done.
     *
     * @param  array $attributes
     * @param  bool  $sync
     *
     * @return $this
     */
    public function setRawAttributes(array $attributes, $sync = false)
    {
        $this->attributes = $attributes;

        foreach ($this->trim() as $key) {
            if (isset($this->attributes[$key])) {
                $this->attributes[$key] = trim($attributes[$key]);
            }

        }

        if ($sync) {
            $this->syncOriginal();
        }

        return $this;
    }

    /**
     * Cast an attribute to a native PHP type.
     *
     * @param  string $key
     * @param  mixed  $value
     *
     * @return mixed
     */
    protected function castAttribute($key, $value)
    {
        $value = parent::castAttribute($key, $value);

        switch ($this->getCastType($key)) {
            case 'pg_array':
                return $this->pgArrayParse($value);
            default:
                return $value;
        }
    }

    public function setAttribute($key, $value)
    {
        parent::setAttribute($key, $value);

        if ($this->hasCast($key, ['pg_array'])) {
            $this->attributes[$key] = self::phpArrayToPostgresArray($value);
        }

        return $this;
    }

    /**
     * Get the casts array.
     *
     * @return array
     */
    public function getCasts()
    {
        property_exists($this, 'customField') && $this->casts[$this->customField] = 'json';

        return parent::getCasts();
    }

    /**
     * Changes PHP array to PostgreSQL array format
     *
     * @param array $array
     *
     * @return string
     */
    public static function phpArrayToPostgresArray(array $array)
    {
        return str_replace(['[', ']'], [
            '{', '}',
        ], json_encode(collect($array)->values()->toArray(), JSON_UNESCAPED_UNICODE));
    }

    public function pgArrayParse($s, $start = 0, &$end = null)
    {
        if (empty($s) || $s[0] != '{') return null;
        $return = [];
        $string = false;
        $quote  = '';
        $len    = strlen($s);
        $v      = '';
        for ($i = $start + 1; $i < $len; $i++) {
            $ch = $s[$i];
            if (! $string && $ch == '}') {
                if ($v !== '' || ! empty($return)) {
                    $return[] = $v;
                }
                $end = $i;
                break;
            } else
                if (! $string && $ch == '{') {
                    $v = self::pgArrayParse($s, $i, $i);
                } else
                    if (! $string && $ch == ',') {
                        $return[] = $v;
                        $v        = '';
                    } else
                        if (! $string && ($ch == '"' || $ch == "'")) {
                            $string = true;
                            $quote  = $ch;
                        } else
                            if ($string && $ch == $quote && $s[$i - 1] == "\\") {
                                $v = substr($v, 0, -1).$ch;
                            } else
                                if ($string && $ch == $quote && $s[$i - 1] != "\\") {
                                    $string = false;
                                } else {
                                    $v .= $ch;
                                }
        }
        foreach ($return as &$r) {
            if (is_numeric($r)) {
                if (ctype_digit($r)) $r = (int)$r;
                else $r = (float)$r;
            }
        }

        return $return;
    }

    /**
     * Dynamically retrieve attributes on the model.
     *
     * @param  string $key
     *
     * @return mixed
     */
    public function __get($key)
    {
        $value = $this->getAttribute($key);

        if (in_array($key, $this->trim(), false)) {
            $value = trim($value);
        }

        return $value;
    }

}