<?php

namespace App\Core\Scaffold;


abstract class AbstractCriteria
{
    /**
     * Set a limit to the number of resources returned.
     */
    protected $limit = 50;

    protected $id;

    public function __call($field, $arguments)
    {
        $method = 'set'.ucfirst($field);
        if (method_exists($this, $method)) {
            $this->{$method}(...$arguments);
        } elseif (property_exists($this, $field)) {
            if (count($arguments) <= 1) {
                $this->{$field} = $arguments[0] ?? null;
            } else {
                $this->{$field} = $arguments;
            }
        }

        return $this;
    }

    abstract public function getBuilder();

    /**
     * Get the first result from the query.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function first()
    {
        return $this->getBuilder()->first();
    }

    /**
     * Get the first result from the query.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function firstOrFail()
    {
        return $this->getBuilder()->firstOrFail();
    }

    public function all()
    {
        return $this->getBuilder()->get();
    }

    /**
     * Get the result.
     *
     * @return LengthAwarePaginator
     */
    public function get()
    {
        $query = $this->getBuilder();

        return $query->paginate($this->limit);
    }
}