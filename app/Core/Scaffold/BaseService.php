<?php

namespace App\Core\Scaffold;

abstract class BaseService
{
    /**
     * Gets the decoded id for the model.
     *
     * @param  string $hash
     *
     * @return int
     */
    public function getDecodedId($hash)
    {
        return $this->model->decodeId($hash);
    }

    public function getEncodedId($id)
    {
        return $this->model->encode($id);
    }

    public function getById($id)
    {
        return $this->model->find($id);
    }

    public function getDecodedIds(array $ids)
    {
        $decoded = [];
        foreach ($ids as $id) {
            $decoded[] = $this->getDecodedId($id);
        }

        return $decoded;
    }
}