<?php

namespace App\Core\Pricing;

class PriceCalculator implements PriceCalculatorInterface
{
    protected $isPriceIncludedTax = true;

    public function withPriceIncludingTax()
    {
        $this->isPriceIncludedTax = true;

        return $this;
    }

    public function withPriceExcludingTax()
    {
        $this->isPriceIncludedTax = false;

        return $this;
    }

    public function get($price, $tax, $qty = 1)
    {
        if ((float)$price <= 0) {
            return new PriceCalculatorResult([
                'unitPrice'               => 0,
                'unitTax'                 => 0,
                'unitPriceIncludingTax'   => 0,
                'totalAmount'             => 0,
                'totalTax'                => 0,
                'totalAmountIncludingTax' => 0,
                'qty'                     => (int)$qty,
            ]);
        }

        $unitPrice = $price;
        // currency converter
        $converted = $price * $qty;
        $taxAmount = $this->calculateTax($converted, $tax);
        $unitTax   = round($taxAmount / $qty, 2);

        if ($this->isPriceIncludedTax) {
            $unitPrice -= $unitTax;
        }

        return new PriceCalculatorResult([
            'unitPrice'               => round($unitPrice, 2),
            'unitTax'                 => $unitTax,
            'unitPriceIncludingTax'   => $unitPrice + $unitTax,
            'totalAmount'             => $converted,
            'totalTax'                => $taxAmount,
            'totalAmountIncludingTax' => $converted + $taxAmount,
            'qty'                     => (int)$qty,
        ]);
    }

    public function calculateTax($price, $tax)
    {
        if (! $this->isPriceIncludedTax) {
            $exVat  = $price * (($tax + 100) / 100);
            $amount = $exVat - $price;
        } else {
            $amount = ($price / ($tax + 100)) * 7;
        }

        return round($amount, 2);
    }
}