<?php

namespace App\Core\Pricing;

class PriceCalculatorResult implements PriceCalculatorInterface
{
    protected $unitPrice;
    protected $unitTax;
    protected $unitPriceIncludingTax;
    protected $totalAmount;
    protected $totalTax;
    protected $totalAmountIncludingTax;
    protected $qty;

    public function __construct(array $data = [])
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }

    public function __get($property)
    {
        $prop = camel_case($property);

        if (property_exists($this, $prop)) {
            return $this->{$prop};
        }

        throw new \InvalidArgumentException("Method or Property {$property} doesn't exist");
    }
}