<?php

namespace App\Core\Pricing;

use InvalidArgumentException;

class PriceCalculatorService
{
    protected $pricing = [];

    protected $isPriceIncludedTax = true;

    public function withPriceIncludingTax()
    {
        $this->isPriceIncludedTax = true;

        return $this;
    }

    public function withPriceExcludingTax()
    {
        $this->isPriceIncludedTax = false;

        return $this;
    }

    public function get($price, $tax = 0, $qty = 1)
    {
        $price = (float)$price;

        if ($price <= 0) {
            return new PriceCalculatorResult([
                'unitPrice'               => 0,
                'unitTax'                 => 0,
                'unitPriceIncludingTax'   => 0,
                'totalAmount'             => 0,
                'totalTax'                => 0,
                'totalAmountIncludingTax' => 0,
                'qty'                     => (int)$qty,
            ]);
        }

        $unitPrice = $price;
        // currency converter
        $converted = $price * $qty;
        $taxAmount = $this->calculateTax($converted, $tax);
        $unitTax   = round($taxAmount / $qty, 2);

        if ($this->isPriceIncludedTax) {
            $unitPrice -= $unitTax;
        }

        $this->pricing = [
            'unit_price'                 => $unitPrice,
            'unit_tax'                   => $unitTax,
            'unit_price_including_tax'   => $unitPrice + $unitTax,
            'total_amount'               => $converted,
            'total_tax'                  => $taxAmount,
            'total_amount_including_tax' => $converted + $taxAmount,
            'qty'                        => (int)$qty,
        ];

        return $this;
    }

    public function calculateTax($price, $tax)
    {
        if (! $this->isPriceIncludedTax) {
            $exVat  = $price * (($tax + 100) / 100);
            $amount = $exVat - $price;
        } else {
            $amount = ($price / ($tax + 100)) * 7;
        }

        return round($amount, 2);
    }

    public function __get($property)
    {
        if (isset($this->pricing[$property])) {
            return $this->pricing[$property];
        }
        throw new InvalidArgumentException("Method or Property {$property} doesn't exist");
    }

}