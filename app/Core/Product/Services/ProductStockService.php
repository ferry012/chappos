<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 1/4/2019
 * Time: 5:52 PM
 */

namespace App\Core\Product\Services;


use App\Core\Product\Models\ProductStock;

class ProductStockService
{
    public function deductQuantityOnHand($itemId, $locId, $qty)
    {
        $rows = ProductStock::where('item_id', $itemId)->where('loc_id', $locId)->whereRaw('(qty_on_hand - qty_reserved) > 0')->orderBy('trans_date')->get();

        if ($rows) {

            foreach ($rows as $row) {

                if ($qty <= 0) {
                    break;
                }

                // Actual balance can be deducted
                $balanceQty = $row->qty_on_hand - $row->qty_reserved;

                if ($balanceQty <= $qty) {
                    $qty              -= $balanceQty;
                    $row->qty_on_hand -= $balanceQty;
                } else {
                    $row->qty_on_hand -= $qty;
                    $qty              = 0;
                }

                $row->save();
            }

            return true;
        }

        return false;
    }

    public function deductQtyOnHandFromReserved($itemId, $locId, $qty)
    {
        $rows = ProductStock::where('item_id', $itemId)->where('loc_id', $locId)->whereRaw('(qty_on_hand - qty_reserved) >= 0')->where('qty_reserved', '>', 0)->orderBy('trans_date')->get();

        if ($rows) {

            foreach ($rows as $row) {

                if ($qty <= 0) {
                    break;
                }

                if ($row->qty_reserved <= $qty) {
                    $row->qty_on_hand  -= $row->qty_reserved;
                    $qty               -= $row->qty_reserved;
                    $row->qty_reserved = 0;
                } else {
                    $row->qty_on_hand  -= $qty;
                    $row->qty_reserved -= $qty;
                    $qty               = 0;
                }

                $row->save();
            }

            return true;
        }

        return false;
    }
}