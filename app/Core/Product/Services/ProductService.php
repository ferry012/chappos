<?php

namespace App\Core\Product\Services;

use App\Core\Product\Interfaces\ProductFactoryInterface;
use App\Core\Product\Models\Product;
use App\Core\Product\Models\ProductComplementary;
use App\Core\Product\Models\ProductPurchaseWithPurchase;
use App\Core\Product\Models\ProductStock;
use App\Support\Str;
use RtLopez\Decimal;
use App\Utils\Helper;

class ProductService
{

    /**
     * The product factory instance.
     *
     * @var ProductFactoryInterface
     */
    protected $factory;
    protected $productCacheSeconds;

    public function __construct(ProductFactoryInterface $factory)
    {
        $this->model = new Product();
        $this->factory = $factory;
        $this->productCacheSeconds = setting('posapi.cache.svr.product', 300);
    }

    public function getById($id, array $columns = ['*'])
    {

        $cacheOpts = ["locationId" => request('loc_id'), "customerGroup" => request('customer_group')];
        list($cacheTag, $cacheKey) = Helper::generateCacheKeys(['products', 'detail'], $id, $cacheOpts);

        $result = app('cache')->tags($cacheTag)->get($cacheKey);
        if ($result) {
            // Return from cache
            return $result;
        }

        $product = $this->model->where(function ($q) use ($id) {
            if (strlen($id) === 13) {
                $q->where('item_id', $id);
            } else if (strlen($id) === 12) {
                $q->where('item_id', 'like', $id.'%');
            } else {
                $q->whereRaw("RTRIM(item_id) = ?", $id);
            }
        })->first($columns);

        if ($product) {
            $result = $this->factory->init($product)->get();
            app('cache')->tags($cacheTag)->put($cacheKey, $result, now()->addSeconds($this->productCacheSeconds));

            return $result;
        }

        return null;
    }

    public function getByAltId($productId)
    {

        $cacheOpts = ["productId" => $productId];
        list($cacheTag, $cacheKey) = Helper::generateCacheKeys(['products', 'alt_id'], $productId, $cacheOpts);

        return app('cache')->tags($cacheTag)->remember($cacheKey, $this->productCacheSeconds, function () use ($productId) {

            $alt_id = db()->table('ims_item_alt_id')->where('coy_id', coy_id())
                ->whereRaw("upper(alt_id) like upper(?)", ["%$productId%"])
                ->orderBy('created_on', 'DESC')
                ->first();

            if ($alt_id) {
                $id = trim($alt_id->item_id);

                return $this->getById($id);
            }

            return null;

        });

    }

    public function getPurchaseWithPurchase($productId, $locationId = '**', array $options = [], $productPrices = [], $productExtWar = null)
    {
        // settings
        $intoGroup = data_get($options, 'into_group', false);
        $freeOnTop = data_get($options, 'free_on_top', false);

        $customerGroup = request('customer_group') ?? '';
        $mbrPriceInd = customer_group_ind($customerGroup);

        $cacheOpts = array_merge(["locationId" => $locationId], ["mbrPriceInd" => $mbrPriceInd], $options);
        list($cacheTag, $cacheKey) = Helper::generateCacheKeys(['products', 'pwp'], $productId, $cacheOpts);

        $result = app('cache')->tags($cacheTag)->get($cacheKey);
        if ($result) {
            // Return from cache
            return $result;
        }

        $builder = ProductPurchaseWithPurchase::where('ref_id', $productId)->location($locationId)->active();
        $builder->whereIn('price_ind', $mbrPriceInd);
        $PWPProducts = $builder->get();

        $products = $this->model->whereIn('item_id', $PWPProducts->pluck('promo_ref_id'))->get();

        $products = $this->factory->collection($products);

        // remapping lines
        $products->map(function ($product) use ($PWPProducts) {
            $isFreeGift = false;
            $promoProduct = $PWPProducts->where('promo_ref_id', $product->item_id)->sortBy('promo_price')->first();

            $product->is_pwp = true;

            $product->promo_id = optional($promoProduct)->promo_id;
            $product->promo_ind = optional($promoProduct)->price_ind;
            $product->promo_loc_id = optional($promoProduct)->loc_id;
            $product->promo_min_qty = optional($promoProduct)->min_qty;
            $product->promo_qty = optional($promoProduct)->promo_qty;
            $product->max_qty = optional($promoProduct)->promo_qty;
            $product->promo_group_id = optional($promoProduct)->promo_grp_id;
            $price = optional($promoProduct)->promo_price;
            if ($price <= 0) {
                $isFreeGift = true;
            }
            $product->is_free_gift = $isFreeGift;
            $product->promo_price = optional($promoProduct)->promo_price;
            $product->promo_discount = optional($promoProduct)->promo_disc;
            $product->multiple_points = optional($promoProduct)->multiple_points;
        })->sortBy('promo_price')->sortBy('group_id')->values();

        // Add SSEW to PWP collection
        if (!empty($productId)) {
            $ssew = $this->genPwpSsew($productId, $productPrices,$locationId);

            //$products->push($ssew);
            if($ssew !== null)
            {
                foreach($ssew as $ind_ssew)
                {
                    $products->push($ind_ssew);
                }
            }
            else
            {
                $products->push($ssew);
            }
        }

        if ($intoGroup) {

            $mainQty = request('mainQty', 1);

            // get the unique group id and grouping accordingly
            $products = $products->pluck('promo_group_id')->unique()->values()
                ->map(function ($groupId, $index) use ($products, $mainQty) {

                    $promoGroupProducts = $products->where('promo_group_id', $groupId);

                    $groupItemsCount = $promoGroupProducts->count();
                    $ProductPriceZeroCount = $promoGroupProducts->where('promo_price', 0)->count();
                    $maxQty = (int)$promoGroupProducts->max('max_qty');

                    $header = new \stdClass();

                    $header->group = $index + 1;
                    $header->actual_group = $groupId;
                    $header->is_free = ($groupItemsCount === $ProductPriceZeroCount);
                    $header->max_qty = $mainQty * $maxQty;
                    $header->product_count = $groupItemsCount;
                    $header->products = $promoGroupProducts->map(function ($item) {
                        if ($item) {
                            unset($item->max_qty);
                            $item->price_savings = round($item->regular_price - $item->promo_price, 2);

                            return $item;
                        }
                    })->filter(function ($item) {
                        return !is_null($item);
                    })->sortByDesc('qty_balance')->values();

                    $header->products = collect($header->products);

                    return $header;
                })->when($freeOnTop, function ($collection) {
                    return $collection->sortByDesc(function ($item) {

                        if ($item->is_free) {
                            return 99;
                        } else if ($item->actual_group == 'SSEW') {
                            return 98;
                        } else {
                            return $item->product_count;
                        }

                    })->values();
                })->filter(function ($collection) {
                    return count($collection->products) > 0;
                });
        }

        if ($products) {
            app('cache')->tags($cacheTag)->put($cacheKey, $products, now()->addSeconds($this->productCacheSeconds));
        }

        return $products;
    }

    /*public function genPwpSsew($productId, $productPrices = [])
    {
        $productIds = ['SSEW', 'SSEW1YR', 'SSEW-DEMO', 'SSEW-PREOWNED', $productId];
        $allProducts = $this->model->where('coy_id', coy_id())->whereIn('item_id', $productIds)->get();

        $product = $allProducts->where('item_id', $productId)->first();

        if ($product->ext_warranty == 'N' || $product->ext_warranty == '' || $product->ext_warranty == '0') {
            return null;
        }

        $ssewId = 'SSEW';

        if (substr($product->ext_warranty, 0, 1) == 'D') {
            $ssewId = 'SSEW-DEMO';
        } else if (substr($product->ext_warranty, 0, 1) == 'P') {
            $ssewId = 'SSEW-PREOWNED';
        }

        $ssew = $allProducts->where('item_id', $ssewId)->first();

        if (!$ssew) {
            return null;
        }

        $finalPrice = (isset($productPrices['unit_price']) && $productPrices['unit_price'] > 0) ? $productPrices['unit_price'] : $product->unit_price;

        $warrantyMinPrice = setting('pos.ssew_min_price', 0);

        if ($finalPrice < $warrantyMinPrice) {
            return null;
        }

        $ssewPercent = ($ssew->inv_dim12 && $ssew->inv_dim12 > 0 && $ssew->inv_dim12 < 1) ? (float)$ssew->inv_dim12 : 0.15;

        $ssew->is_pwp = true;
        $ssew->promo_id = 'CRMPWP00000SSEW';
        $ssew->promo_group_id = "SSEW";
        $ssew->promo_ind = 'ALL';
        $ssew->promo_loc_id = '**';
        $ssew->promo_min_qty = 1;
        $ssew->promo_qty = 1;
        $ssew->max_qty = 1;
        $ssew->is_free_gift = false;
        $ssew->regular_price = ceil($finalPrice * $ssewPercent);
        $ssew->promo_price = ceil($finalPrice * $ssewPercent);
        $ssew->promo_discount = 0;
        $ssew->multiple_points = 1;
        $ssew->test_data = $product;

        return $ssew;
    }*/

    public function genPwpSsew($productId, $productPrices = [],$locationId = '**')
    {

        $productIds = ['SSEW', 'SSEW1YR', 'SSEW-DEMO', 'SSEW-PREOWNED', $productId];
        //$productIds = ['SSEW', 'SSEW-DEMO', 'SSEW-PREOWNED', $productId];
        $allProducts = $this->model->where('coy_id', coy_id())->whereIn('item_id', $productIds)->get();

        $product = $allProducts->where('item_id', $productId)->first();

        if ($product->ext_warranty == 'N' || $product->ext_warranty == '' || $product->ext_warranty == '0') {
            return null;
        }

        $ssewId = array('SSEW1YR', 'SSEW');
        //$ssewId = array('SSEW');

        if (substr($product->ext_warranty, 0, 1) == 'D' || $product->ext_warranty == '0.5') {
            $ssewId = array('SSEW-DEMO');
        } else if (substr($product->ext_warranty, 0, 1) == 'P') {
            $ssewId = array('SSEW-PREOWNED');
        }

        $ssew_arr = array();
        foreach($ssewId as $idValue) {
            $ssew = $allProducts->where('item_id', $idValue)->first();

            if (!$ssew) {
                $ssew_arr = null;
                //return null;
            }
    
            $finalPrice = (isset($productPrices['unit_price']) && $productPrices['unit_price'] > 0) ? $productPrices['unit_price'] : $product->unit_price;
    
            $warrantyMinPrice = setting('pos.ssew_min_price', 0);
    
            if ($finalPrice < $warrantyMinPrice) {
                $ssew_arr = null;
                //return null;
            }
            //&& $locationId!=="OS" // OS = Online Shop, pass by Hachi3 Website/Challenger.sg
            if($ssew_arr !== null) 
            {
                $ssewPercent = ($ssew->inv_dim12 && $ssew->inv_dim12 > 0 && $ssew->inv_dim12 < 1) ? (float)$ssew->inv_dim12 : 0.15;
    
                $ssew->is_pwp = true;
                $ssew->promo_id = 'CRMPWP00000SSEW';
                $ssew->promo_group_id = "SSEW";
                $ssew->promo_ind = 'ALL';
                $ssew->promo_loc_id = '**';
                $ssew->promo_min_qty = 1;
                $ssew->promo_qty = 1;
                $ssew->max_qty = 1;
                $ssew->is_free_gift = false;
                $ssew->regular_price = ceil($finalPrice * $ssewPercent);
                $ssew->promo_price = ceil($finalPrice * $ssewPercent);
                $ssew->promo_discount = 0;
                $ssew->multiple_points = 1;
                $ssew->test_data = $product;
            
                array_push($ssew_arr, $ssew);
            }
        }

        return $ssew_arr;
    }

    public function getExtendWarrantyPrice($product, $price, $vendor = 'STAR_SHIELD')
    {

        $warrantyPrice = null;
        $priceFactor = 0;

        $warrantyYear = Str::upper($product->ext_warranty);

        if (in_array($warrantyYear, ['N', '', '0'], false)) {
            return $warrantyPrice;
        }

        switch ($vendor) {
            case 'STAR_SHIELD':
                $itemCode = 'SSEW';

                if (substr($warrantyYear, 0, 1) == 'D') {
                    $itemCode = 'SSEW-DEMO';
                } else if (substr($warrantyYear, 0, 1) == 'P') {
                    $itemCode = 'SSEW-PREOWNED';
                }

                $mWarranty = $this->model->where('coy_id', coy_id())->where('item_id', $itemCode)->first();

                if ($mWarranty) {
                    $priceFactor = ($mWarranty->inv_dim12 && $mWarranty->inv_dim12 > 0 && $mWarranty->inv_dim12 < 1) ? (float)$mWarranty->inv_dim12 : 0.15;
                }

                $warrantyMinPrice = setting('pos.ssew_min_price', 0);

                if ($price < $warrantyMinPrice) {
                    return $warrantyPrice;
                }

                if ($priceFactor > 0) {
                    $warrantyPrice = $price * $priceFactor;
                }

                // round to nearest whole number, 52.2 to 53
                $warrantyPrice = ceil($warrantyPrice);

                break;
            default:
        }

        return $warrantyPrice;
    }

    public function getPromotionPrice($itemId, $customerGroup = null)
    {
        $result = $this->getCheapPromotionalItem($itemId, $customerGroup);

        if ($result) {
            return $result->promo_price;
        }

        return null;
    }

    public function getPromotionRebate($itemId, $locId)
    {
        $row = db()->table('v_ims_live_promo')
            ->where('promo_type', 'MPT')
            ->where(function ($q) {
                $q->whereDate('eff_from', '<=', today()->toDateString())->whereTime('time_from', '<=', today()->toTimeString());
            })
            ->where(function ($q) {
                $q->whereDate('eff_to', '>=', today()->toDateString())->whereTime('time_to', '>=', today()->toTimeString());
            })
            ->whereIn('loc_id', ['**', $locId])
            ->where('ref_id', $itemId)
            ->orderBy('multiple_points')
            ->first();

        if ($row) {
            $type = Str::upper($row->promo_grp_id);

            if (empty($type)) {
                $type = 'FIXED';
            }

            return [
                'id'    => $row->promo_id,
                'type'  => $type,
                'value' => $row->multiple_points,
            ];
        }

        return null;
    }

    public function getCheapPromotionalItem($itemId, $customerGroup = null)
    {
        $customerGroup = (empty($customerGroup)) ? Str::upper(request('customer_group', '')) : Str::upper($customerGroup);
        $mbrPriceInd = customer_group_ind($customerGroup);

        $cacheOpts = ["locationId" => request('loc_id'), "mbrPriceInd" => $mbrPriceInd];
        list($cacheTag, $cacheKey) = Helper::generateCacheKeys(['products', 'promo_prc'], $itemId, $cacheOpts);

        $result = app('cache')->tags($cacheTag)->get($cacheKey);
        if ($result) {
            // Return from cache
            return $result;
        }

        // Get the best promo price that still active
        $builder = db()->table('v_ims_live_promo')
            ->where(function ($q) {
                $q->whereDate('eff_from', '<=', today()->toDateString())->whereTime('time_from', '<=', today()->toTimeString());
            })
            ->where(function ($q) {
                $q->whereDate('eff_to', '>=', today()->toDateString())->whereTime('time_to', '>=', today()->toTimeString());
            })
            ->where(function ($q) {
                $q->whereIn('loc_id', ['**', request('loc_id')]);
            })
            ->where('promo_type', 'PRC')
            ->where('ref_id', $itemId)
            ->whereIn('price_ind', $mbrPriceInd);

        $result = $builder->orderBy('promo_price')->first();

        if ($result) {
            app('cache')->tags($cacheTag)->put($cacheKey, $result, now()->addSeconds($this->productCacheSeconds));
        }

        return $result;
    }

    public function getStaffPrice($productId)
    {

        $product = db()->table('v_ims_live_product')->where('item_id', $productId)->first();


        $markup = 0;
        $newPrice = number_format($product->regular_price, 2);

        if ($product->allow_discount === 'N') {
            return $newPrice;
        }

        $fields = ['item_id', 'inv_dim4', 'inv_dim3', 'inv_dim2'];

        // Get the according markup
        foreach ($fields as $field) {
            if ($row = db('pg_cherps')->table('ims_staff_markup')->where($field, $product->{$field})->first()) {
                $markup = ($row->markup_percent / 100) + 1;
                break;
            }
        }

        if (Decimal::create($markup)->le(0)) {
            return $newPrice;
        }

        $rows = db()->table('ims_item_price')->where('coy_id', coy_id())
            ->where('item_id', $product->item_id)
            ->where('price_type', 'SUPPLIER')
            ->where('eff_from', '<', now())
            ->where('eff_to', '>', now())
            ->where('unit_price', '>', 0)
            ->where('price_ind', 'PRI')
            ->get();

        if ($rows->count() === 1) {
            $row = $rows->first();
            $row->curr_id = Str::trim($row->curr_id);

            // Supplier price could be set as foreign currency, need to convert to local currency first
            if ($row->curr_id && $row->curr_id !== 'SGD') {
                $exchange = db('pg_cherps')->table('coy_foreign_exchg')->where('coy_id', coy_id())
                    ->where('eff_from', '<', now())
                    ->where('eff_to', '>', now())
                    ->where('curr_id', $row->curr_id)
                    ->orderBy('created_on', 'DESC')
                    ->first();

                if ($exchange) {
                    $newPrice = ($row->unit_price * $exchange->exchg_rate);
                } else {
                    return $newPrice;
                }

            } else {
                $newPrice = $row->unit_price;
            }

        }

        return number_format(($newPrice * $markup * 1.07), 2);
    }

    public function getStock($productId, $locationId = '*')
    {
        return ProductStock::where('item_id', $productId)->location($locationId)->get();
    }

    public function getRelatedColor($productId)
    {
        if (empty($productId)) {
            return null;
        }

        $cacheOpts = ["productId" => $productId];
        list($cacheTag, $cacheKey) = Helper::generateCacheKeys(['products', 'color_opt'], $productId, $cacheOpts);

        return app('cache')->tags($cacheTag)->remember($cacheKey, $this->productCacheSeconds, function () use ($productId) {

            $products = db()->table('ims_product_detail')
                ->join('ims_color_master', 'ims_product_detail.color_id', '=', 'ims_color_master.color_id')
                ->where('ims_product_detail.coy_id', coy_id())->where('parent_item_id', $productId)
                ->select('ims_product_detail.item_id', 'ims_product_detail.image_name', 'ims_product_detail.color_id', 'ims_color_master.color_image_url')
                ->get();

            foreach ($products as $product) {
                $product->image_name = get_constant('s3.product_images_thumb') . $product->image_name ?? '';
                $product->color_image = get_constant('s3.product_color') . $product->color_image_url ?? '';
                unset($product->color_image_url);
            }

            return $products;
        });

    }

    public function getCategoryMapping($inv_dim4,$inv_dim3='',$inv_dim2='',$inv_dim1='')
    {

        $cacheOpts = ["inv_dim4" => $inv_dim4,"inv_dim3"=> $inv_dim3,"inv_dim2"=> $inv_dim2,"inv_dim1"=> $inv_dim1];
        list($cacheTag, $cacheKey) = Helper::generateCacheKeys(['products', 'inv_dim4'], $inv_dim4, $cacheOpts);

        return app('cache')->tags($cacheTag)->remember($cacheKey, $this->productCacheSeconds, function () use ($inv_dim4,$inv_dim3,$inv_dim2,$inv_dim1) {

            $products = db()->table('v_ims_live_category')
                ->where('inv_dim4', $inv_dim4)
                ->select('inv_dim4', 'h_category_id', 'cate_h', 'm_category_id', 'cate_m', 's_category_id', 'cate_s')
                ->orderByRaw('seq_h,seq_m,seq_s')
                ->get();

            if ($products->isEmpty()) {
                $sql = "select a.coy_id, a.inv_dim2, a.inv_dim3,
                               h_category_id, h.category_name cate_h, m_category_id, m.category_name cate_m, s_category_id, s.category_name cate_s
                        from ims_category_default a
                             JOIN ims_category_list h ON a.coy_id::text = h.coy_id::text AND a.h_category_id::text = h.category_id::text AND h.is_live::text = 'Y'::text AND h.status_level > 0
                             JOIN ims_category_list m ON a.coy_id::text = m.coy_id::text AND a.m_category_id::text = m.category_id::text AND m.is_live::text = 'Y'::text AND m.status_level > 0
                             JOIN ims_category_list s ON a.coy_id::text = s.coy_id::text AND a.s_category_id::text = s.category_id::text AND s.is_live::text = 'Y'::text AND s.status_level > 0
                        where inv_dim3= ? or inv_dim2= ?
                        order by case when inv_dim3= ? then 1 else 9 end,line_num limit 1";
                $products = collect(db()->select($sql, [$inv_dim3, $inv_dim2, $inv_dim3]));
            }

            return $products;
        });

    }

    public function getAllPrices($productId, $productMutiplePoints = 1, $customerGroup = null, $locationId = null, $CustomerRebateTier = 2)
    {

        if (empty($locationId)) {
            $locationId = request('loc_id');
        }

        $locationId = explode(',', $locationId);
        $locationId[] = '**';

        $customerGroup = (empty($customerGroup)) ? Str::upper(request('customer_group', '')) : Str::upper($customerGroup);
        if ($customerGroup === 'GUEST') {
            $customerGroup = '';
        }
        $mbrPriceInd = customer_group_ind($customerGroup);
        if ($customerGroup == '') {
            $CustomerRebateTier = 0;
        }


        $cacheOpts = ["productId" => $productId];
        list($cacheTag, $cacheKey) = Helper::generateCacheKeys(['products', 'prices'], $productId, $cacheOpts);

        list($product, $prices) = app('cache')->tags($cacheTag)->remember($cacheKey, $this->productCacheSeconds, function () use ($productId) {

            $product = null; // db()->table('v_ims_live_product')->where('item_id', $productId)->first();

            $prices = db()->table('v_ims_live_price')->where('coy_id', coy_id())->where('item_id', $productId)->orderBy('price_type')->get();

            return [$product, $prices];

        });

        // REGULAR PRICE
        $regular_price = ($p = $prices->firstWhere('price_type', 'REGULAR')) ? (float)$p->unit_price : 0;

        // MEMBER PRICE
        $mbr_price = $regular_price;
        if ($p = $prices->firstWhere('price_type', 'MEMBER')) {
            $mbr_price = ($p->unit_price == 0) ? ($regular_price * ((100 - $p->unit_disc) / 100)) : $p->unit_price;
        }

        // PROMO PRICE - for ALL/MBR with loc_id
        $promo_p = $prices->where('price_type', 'PROMOTION')
            ->whereIn('mbr_group', ['ALL', 'MBR', 'MEMBER'])->whereIn('loc_id', $locationId)
            //->sortByDesc('loc_id')->sortBy('mbr_group')
            ->sortBy('unit_price')
            ->first();

        if ($promo_p && $promo_p->unit_price == 0 && $promo_p->unit_disc > 0) {
            $promo_p->unit_price = $regular_price * ((100 - $promo_p->unit_disc) / 100);
        }

        $price_dataset = [
            'unit_price'         => (float)$regular_price,
            'unit_price_ind'     => '',
            'price_savings'      => 0,
            'regular_price'      => (float)$regular_price,
            'mbr_price'          => (float)$mbr_price,
            'promo_price'        => ($promo_p) ? (float)$promo_p->unit_price : 0,
            'promo_id'           => ($promo_p) ? $promo_p->promo_id : '',
            'promo_ind'          => ($promo_p) ? $promo_p->mbr_group : '',
            'promo_cost'         => ($promo_p) ? (float)$promo_p->unit_cost : 0,
            'disc_percent'       => ($p = $prices->firstWhere('price_type', 'MEMBER')) ? (float)$p->unit_disc : 0,
            'cost_price'         => ($p = $prices->firstWhere('price_type', 'SUPPLIER')) ? (float)$p->unit_price : 0,
            'min_price'          => ($p = $prices->firstWhere('price_type', 'MIN_PRICE')) ? (float)$p->unit_price : 0,
            'staff_price'        => ($customerGroup == 'STAFF') ? (float)$this->getStaffPrice($productId) : 0,
            'customer_groups'    => [],
            'multiple_points'    => (float)$productMutiplePoints,
            'mpt_promo_id'       => '',
            'mpt_promo_by'       => '',
            'mpt_add_amount'     => 0,
            'unit_rebate_points' => 0,
            'unit_rebate_amount' => 0,
        ];

        // Get all other prices set by customer_group
        $price_dataset['customer_groups'] = $prices
            ->whereIn('price_type', ['REGULAR', 'MEMBER', 'PROMOTION'])->whereNotIn('mbr_group', [
                'ALL',
                'MBR',
                'MEMBER'
            ])
            ->whereIn('loc_id', $locationId)
            ->keyBy('mbr_group')->map(function ($item, $key) {
                return [
                    'unit_price' => (float)$item->unit_price,
                    'promo_id'   => $item->promo_id,
                    'promo_ind'  => $item->mbr_group,
                ];
            });

        // Decide which is the effective unit_price
        if ($customerGroup !== '') {

            // By default, member price will be available
            $price_dataset['unit_price_ind'] = 'MEMBER';
            $price_dataset['unit_price'] = (in_array($price_dataset['promo_ind'], [
                    'MBR',
                    'MEMBER'
                ]) && $price_dataset['promo_price'] < $price_dataset['mbr_price']) ? $price_dataset['promo_price'] : $price_dataset['mbr_price'];
            $price_dataset['price_savings'] = (float)$price_dataset['regular_price'] - $price_dataset['unit_price'];

            if ($customerGroup == 'STAFF' && $price_dataset['unit_price'] > $price_dataset['staff_price']) {
                $price_dataset['unit_price_ind'] = 'STAFF';
                $price_dataset['unit_price'] = $price_dataset['staff_price'];
                $price_dataset['price_savings'] = (float)$price_dataset['regular_price'] - $price_dataset['unit_price'];
            }

            if ($price_dataset['customer_groups']->has($customerGroup) && $price_dataset['unit_price'] > $price_dataset['customer_groups'][$customerGroup]['unit_price']) {
                // Other customer_group
                $price_dataset['promo_id'] = $price_dataset['customer_groups'][$customerGroup]['promo_id'];
                $price_dataset['promo_ind'] = $price_dataset['customer_groups'][$customerGroup]['promo_ind'];
                $price_dataset['promo_price'] = (float)$price_dataset['customer_groups'][$customerGroup]['unit_price'];

                $price_dataset['unit_price_ind'] = $customerGroup;
                $price_dataset['unit_price'] = $price_dataset['customer_groups'][$customerGroup]['unit_price'];
                $price_dataset['price_savings'] = (float)$price_dataset['regular_price'] - $price_dataset['unit_price'];
            }

        }

        // If public promo is better than unit_price, use public promo
        if ($price_dataset['promo_ind'] === 'ALL' && $price_dataset['promo_price'] < $price_dataset['unit_price']) {
            $price_dataset['unit_price'] = $price_dataset['promo_price'];
            $price_dataset['price_savings'] = 0;
        }

        // REBATE
        if ($CustomerRebateTier > 0) {

            $rebate_p = $prices->where('price_type', 'REBATE')
                ->whereIn('mbr_group', $mbrPriceInd)->whereIn('loc_id', $locationId)
                ->sortBy('loc_id')->sortBy('mbr_group')
                ->first();

            if ($rebate_p) {
                $price_dataset['mpt_promo_id'] = $rebate_p->promo_id;
                $price_dataset['mpt_promo_by'] = Str::upper($rebate_p->ref_id); // crm_promo_item.promo_grp_id
                $price_dataset['multiple_points'] = (float)$rebate_p->unit_disc;
                $price_dataset['mpt_add_amount'] = (float)$rebate_p->unit_price;
            }

            $price_dataset['unit_rebate_amount'] = $this->calculateUnitRebateAmount($price_dataset['unit_price'], $price_dataset['multiple_points'], $CustomerRebateTier, $price_dataset['mpt_add_amount'], $price_dataset['mpt_promo_by']);
            $price_dataset['unit_rebate_points'] = $price_dataset['unit_rebate_amount'] * 100;

        }

        return $price_dataset;

    }

    public function calculateUnitRebateAmount($unitPrice, $multiplier = 0, $rebateTier = 1, $extraPoints = 0, $mpt_promo_by = '')
    {
        // No rebate tier entitlement equivalent no rebate at all
        if (Decimal::create($rebateTier)->le(0)) {
            return 0;
        }

        // Fixed MPT - ignore customer tier
        if (strtoupper($mpt_promo_by) == 'FIXED') {
            $rebateTier = 1;
        }

        return ((floor($unitPrice) * $multiplier * $rebateTier) / 100) + $extraPoints;
    }

    public function convertPointsToRebate($points)
    {
        if ($points <= 0) {
            return 0;
        }

        return $points / 100;
    }

    public function calculateUnitRebatePoints($unitPrice, $multiplier = 0, $rebateTier = 1, $extraPoints = 0, $mpt_promo_by = '')
    {
        return $this->calculateUnitRebateAmount($unitPrice, $multiplier, $rebateTier, $extraPoints, $mpt_promo_by) * 100;
    }

    public function getPromoItemLimit($promoId, $itemId)
    {

        $cacheOpts = ["promoId" => $promoId, "productId" => $itemId];
        list($cacheTag, $cacheKey) = Helper::generateCacheKeys(['products', 'promoLimit'], $itemId, $cacheOpts);

        return app('cache')->tags($cacheTag)->remember($cacheKey, $this->productCacheSeconds, function () use ($promoId, $itemId) {

            return db()->table('v_ims_live_promo')->where('promo_id', $promoId)->where('ref_id', $itemId)->first();

        });

    }

    public function getPurchaseLimits($promoId, $itemId)
    {

        $cacheOpts = [
            "productId" => $itemId,
            "promoId"   => $promoId,
        ];
        list($cacheTag, $cacheKey) = Helper::generateCacheKeys(['products', 'purchaseLimit'], $itemId, $cacheOpts);

        return app('cache')->tags($cacheTag)->remember($cacheKey, $this->productCacheSeconds, function () use ($promoId, $itemId) {

            $item = db()->table('v_ims_live_promo')->where('promo_type', 'PRC')->where('promo_id', $promoId)->where('ref_id', $itemId)->first();

            if ($item) {
                return [
                    'max'        => (int)$item->limit_stock_main,
                    'per_day'    => (int)$item->limit_per_day,
                    'per_trans'  => (int)$item->limit_per_trans,
                    'per_member' => (int)$item->limit_per_member,
                    'promo_max'  => (int)$item->limit_stock_promo,
                ];
            }

            return [];

        });
    }

    //
    // Following to be depreciated
    //

    public function getComplementaries($productId, $limit = 6)
    {
        $productIds = ProductComplementary::where('item_id', $productId)->limit($limit)->pluck('comp_id');

        return $this->model->whereIn('item_id', $productIds)->get();
    }
}
