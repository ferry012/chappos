<?php

namespace App\Core\Product;

use App\Core\Product\Models\Product;
use App\Core\Scaffold\AbstractCriteria;

class ProductCriteria extends AbstractCriteria
{

    public function getBuilder()
    {
        $product = new Product();

        if ($this->id) {

            return $product->where('item_id', '=', $this->id);
        }

        return $product;
    }
}