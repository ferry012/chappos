<?php

namespace App\Core\Product\Models;


use App\Core\Scaffold\BaseModel;
use App\Scopes\ChallengerScope;
use RtLopez\Decimal;

class Product extends BaseModel
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pg';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'v_ims_live_product';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'item_id';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['unit_savings'];


    protected $casts = [
        'parent_id'     => 'int',
        'regular_price' => 'float',
        'unit_price'    => 'float',
        'mbr_price'     => 'float',
        'promo_price'   => 'float',
        'limit_options' => 'json',
    ];

    const UPDATED_AT = null;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ChallengerScope);
    }

    public function stocks()
    {
        return $this->setConnection('sql')->hasMany(ProductStock::class, 'item_id', 'item_id');
    }

    public function getShortDescAttribute($value)
    {

        if (empty($value)) {
            return $this->getAttribute('item_desc');
        }

        return $value;
    }

    public function getAllowDiscountAttribute($value)
    {
        return $value !== '' ? $value : 'N';
    }

    public function getRegularPriceAttribute($price)
    {
        return $price ?? 0;
    }

    public function getMemberPriceAttribute($price)
    {
        return $price ?? $this->regular_price;
    }

    public function getPromoPriceAttribute($price)
    {
        return $price ?? 0;
    }

    public function getIsFreeGiftAttribute($value)
    {
        return isset($value) && $value;
    }

    public function getMultiplePointsAttribute($multiplier)
    {
        $multiplier = $multiplier ?? 0;

        // prevent negative points
        if ($multiplier < 0) {
            $multiplier = 0;
        }

        // item that is not qualified to earn points
        if (in_array($this->item_type, ['M', 'S', 'W'])) {
            $multiplier = 0;
        }

        return $multiplier;
    }

    public function getUnitPointMultiplierAttribute()
    {
        return $this->getMultiplePointsAttribute(4);
    }

    public function getUnitSavingsAttribute()
    {
        if ($this->promo_ind == 'MBR') {
            return $this->regular_price - $this->promo_price;
        }

        if ($this->regular_price > $this->mbr_price) {
            return $this->regular_price - $this->mbr_price;
        }

        return .0;
    }

    public function getIsPromoOverwrittenAttribute($value)
    {
        return isset($value) && $value;
    }

    public function getUnitPrice($isMemberPrice = false)
    {
        $prices[] = $this->regular_price;

        if ($this->promo_ind == 'ALL' && ($this->promo_price > 0 || $this->is_promo_overwritten)) {
            $prices[] = $this->promo_price;
        }

        if ($isMemberPrice) {
            $prices[] = $this->mbr_price;

            if ($this->promo_ind == 'MBR' && ($this->promo_price > 0 || $this->is_promo_overwritten)) {
                $prices[] = $this->promo_price;
            }
        }

        if ($this->is_free_gift) {
            $prices[] = .0;
        }

        $price = min($prices);

        // product not a gift but price not set then get the max price
        if (! ($this->is_free_gift || $this->is_promo_overwritten) && Decimal::create($price)->le(0)) {
            $price = max($prices);
        }

        // last holy guard
        /*
        if ($price <= 0) {
            $price = 9999;
        }
        */

        return $price;
    }

    public function getEarnPoints($price = 0)
    {

        if ($this->is_free_gift) {
            return 0;
        }

        if ($price === null || $price < 0) {
            $price = 0;
        }

        return $this->multiple_points * $price;
    }

    public function trim()
    {
        return [
            'item_id', 'ext_warranty', 'inv_dim1', 'inv_dim2', 'inv_dim3', 'inv_dim4', 'brand_id', 'model_id',
            'parent_item_id', 'color_id', 'inv_type', 'promo_id',
        ];
    }

}