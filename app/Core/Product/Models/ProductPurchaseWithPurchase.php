<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 27/3/2019
 * Time: 4:30 PM
 */

namespace App\Core\Product\Models;


use App\Core\Scaffold\BaseModel;
use App\Support\Str;
use Illuminate\Database\Eloquent\Builder;

class ProductPurchaseWithPurchase extends BaseModel
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pg';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'v_ims_live_promo';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = '';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('purchase_with_purchase', function (Builder $builder) {
            $builder->where('promo_type', '=', 'PWP');
        });
    }

    public function scopeLocation($query, $location = null)
    {
        /*
        // Show trade-show items only
        if (Str::startsWith($location, 'TRS-')) {
            return $query->where('loc_id', $location);
        }
        */

        $locations[] = '**';

        if (! empty($location)) {

            if (! is_array($location)) {
                $location = (array)$location;
            }

            $locations = array_merge($locations,$location);
        }

        return $query->whereIn('loc_id', $locations);
    }

    public function scopeActive($query)
    {
        return $query->where(function ($q) {
            $q->whereDate('eff_from', '<=', today()->toDateString())->whereTime('time_from', '<=', today()->toTimeString());
        })->where(function ($q) {
            $q->whereDate('eff_to', '>=', today()->toDateString())->whereTime('time_to', '>=', today()->toTimeString());
        });
    }

    public function getRefIdAttribute()
    {
        return trim($this->getArrayAttributeByKey('ref_id'));
    }

    public function getUnitPrice()
    {
        return $this->promo_price;
    }

    public function trim()
    {
        return ['ref_id', 'promo_ref_id', 'promo_grp_id'];
    }
}