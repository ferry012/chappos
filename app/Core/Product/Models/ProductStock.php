<?php

namespace App\Core\Product\Models;

use App\Core\Scaffold\BaseModel;
use App\Scopes\ChallengerScope;
use Illuminate\Database\Query\Builder;

class ProductStock extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ims_inv_physical';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = '';

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public $casts = [
        'qty_on_hand'  => 'int',
        'qty_reserved' => 'int',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ChallengerScope);
    }

    public function product()
    {
        return $this->setConnection('pg')->belongsTo(Product::class, 'item_id', 'item_id');
    }

    /**
     * location
     *
     * @param  string $query
     * @param  mixed  $location
     *
     * @return Builder
     */
    public function scopeLocation($query, $location = null)
    {
        if (is_array($location) && ! empty($location[0])) {
            return $query->whereIn('loc_id', $location);
        }

        if (empty($location)) {
            return $query;
        }

        return $query->where('loc_id', $location);
    }

    public function trim()
    {
        return ['item_id', 'loc_id'];
    }
}