<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 27/3/2019
 * Time: 2:53 PM
 */

namespace App\Core\Product\Models;


use App\Core\Scaffold\BaseModel;

class ProductComplementary extends BaseModel
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ims_item_comp_id';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'item_id';

    public function product()
    {
        return $this->belongsTo(Product::class, 'comp_id');
    }
}