<?php

namespace App\Core\Product\Factories;


use App\Core\Product\Interfaces\ProductFactoryInterface;
use App\Support\Str;
use Illuminate\Support\Collection;

class ProductFactory implements ProductFactoryInterface
{
    protected $product;

    public function __construct()
    {

    }

    public function init($product)
    {
        $this->product = $product;

        return $this;
    }

    public function get()
    {

        if ($loc = request('loc_id')) {
            // Get the best promo price that still active
            // $promoPrice = app('api')->products()->getPromotionPrice($this->product->item_id);

            // Get best promo details (with promo_id)
            $public = app('api')->products()->getCheapPromotionalItem($this->product->item_id, 'guest');
            $member = app('api')->products()->getCheapPromotionalItem($this->product->item_id, 'member');

            $this->product->is_promo_overwritten = false;
            $this->product->orig_promo_price     = $this->product->promo_price;

            if ($public !== null) {
                $this->product->is_promo_overwritten = true;
                $this->product->promo_price          = (float) $public->promo_price;
                $this->product->promo_id             = $public->promo_id;
                $this->product->promo_ind            = $public->price_ind;
            }

            if ($member !== null) {
                // Make sure mbr promo is better than public promo
                if ($this->product->is_promo_overwritten == false || $this->product->promo_price > $member->promo_price ){
                    $this->product->is_promo_overwritten = true;
                    $this->product->promo_price          = (float) $member->promo_price;
                    $this->product->promo_id             = $member->promo_id;
                    $this->product->promo_ind            = $member->price_ind;
                }
            }

        }

        return $this->product;
    }

    public function collection(Collection $products)
    {
        foreach ($products as $product) {
            $this->init($product)->get();
        }

        return $products;
    }
}