<?php

namespace App\Core\Members\Helpers;

use \App\Models\MemberGroup;

class Helper
{
    public static function getMemberGroupByType($type)
    {
        $results = MemberGroup::all();

        $type = (strpos($type, 'S') === 0) ? 'S' : $type;

        if ($results->isNotEmpty()) {
            $result = $results->filter(static function ($item) use ($type) {
                return in_array($type, $item->mbr_type, false);
            })->first();

            if ($result) {
                return $result->mbr_group;
            }

        }

        return null;
    }
}