<?php

namespace App\Core\Members\Services;

use App\Support\Carbon;
use App\Support\Guzzle\LoggerMiddleware;
use App\Support\Str;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\HandlerStack;
use Illuminate\Support\Facades\Cache;

class MemberService
{
    protected $baseUrl;
    protected $accessToken;

    public function __construct()
    {
        $this->baseUrl = env('API_URL_VALUECLUB')."api/";
        $this->accessToken = env('API_URL_VALUECLUB_TOKEN');
    }

    private function prepareDefaults($options)
    {
        return array_merge([
            'query'       => [
                'key_code' => $this->accessToken,
            ],
            'http_errors' => false,
        ], $options);
    }

    public function request($method = 'GET', $url, $options = [])
    {
        $options = $this->prepareDefaults($options);

        $stack = HandlerStack::create();

        $stack->push(new LoggerMiddleware('valueclub'));

        try {
            $response = with(new Client(['handler' => $stack]))->request($method, $this->baseUrl.$url, $options);

            $payload = json_decode($response->getBody());

        } catch (ClientException $e) {
            return null;
        }

        return $payload;
    }

    public function getUnusedCoupons($mbrId)
    {

        if (empty($mbrId)) {
            return [];
        }

        return $this->request('GET', 'member/voucher/available/'.$mbrId);
    }

    public function getById($id, $skipCache = false)
    {
        if (empty($id)) {
            return null;
        }

        if (!$skipCache && Cache::has('member.'.$id)) {
            return Cache::get('member.'.$id);
        }

        try {

            $payload = $this->request('GET', 'validate/'.$id, ['description' => 'MEMBER_INFO_'.$id]);

        } catch (ClientException $e) {
            return null;
        }

        if ($payload && $payload->status_code === 200) {
            Cache::put('member.'.$id, $payload->data, 1);

            return $payload->data;
        }

        return null;
    }

    public function sendTransaction($transId, $data)
    {
        try {

            $payload = $this->request('POST', 'transaction', [
                'description' => 'POST_TRANS_'.$transId,
                'json'        => $data,
            ]);

        } catch (ClientException $e) {
            return null;
        }

        return $payload;
    }

    public function apiMemberItems()
    {
        $rebateTiers = app('cache')->remember('api.vc.member_item'.time(), now()->endOfDay(), function () {
            $response = $this->request('GET', 'member_item?status=all');
            // $response = $this->request('GET', 'member_item_pos?status=all'); // enable this for NS Men membership signup
            return $response->data;
        });

        return $rebateTiers;
    }

    // $group: [NEW/RENEW/UPGRADE/RENEW_POINTS]
    public function getRebateTier($type, $group = 'NEW')
    {
        $items = $this->apiMemberItems();

        $value = collect($items)->where('item_type', $type)->where('item_group', $group)->first();

        if ($value) {
            return (float)$value->rebate_tier;
        }

        return 0;
    }

    public function getMemberTypeByItemId($itemId)
    {
        return 'M'.Str::substr(Str::trimRight($itemId), -2);
    }

    public function getRebateTierByItemId($itemId)
    {

        $memberType = $this->getMemberTypeByItemId($itemId);

        if (Str::contains($itemId, '-NEW', false)
            || Str::contains($itemId, '-UPG', false)
            || Str::contains($itemId, '-REN', false)
            || Str::contains($itemId, '-RR', false)
            || Str::contains($itemId, '-NS', false)
        ) {
            return $this->getRebateTier($memberType);
        }

        return 0;
    }
}