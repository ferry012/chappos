<?php

namespace App\Core\Baskets;

use App\Core\Baskets\Interfaces\BasketInterface;
use App\Core\Baskets\Models\Basket;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class BasketManager
{
    /**
     * The basket factory.
     *
     * @var BasketInterface
     */
    protected $factory;

    protected $basket;

    protected $name;

    protected $ownerId;

    protected $insertedLines;

    protected $discounts;

    public function __construct(BasketInterface $factory, $id, $name = 'default')
    {
        $this->basket        = new Basket;
        $this->name          = $name;
        $this->factory       = $factory;
        $this->insertedLines = new Collection();

        if ($id) {
            $id     = $this->basket->decodeId($id);
            $basket = Basket::where('id', $id)->where('cart_name', $name)->first();

            if ($basket === null) {
                throw (new ModelNotFoundException)->setModel(get_class($this->basket));
            }

            $this->basket = $basket;
        }
    }

    public function create($name = 'default')
    {
        $this->basket             = new Basket;
        $this->basket->coy_id     = coy_id();
        $this->basket->cart_name  = $name;
        $this->basket->loc_id     = request('loc_id') ?? user_data('loc_id');
        $this->basket->pos_id     = request('pos_id') ?? user_data('pos_id');
        $this->basket->owner_id   = $this->ownerId;
        $this->basket->created_by = user_id();
        $this->basket->save();

        return $this->basket();
    }

    public function add($lines)
    {

        if (is_multi_array($lines, 'custom_data')) {
            $lines = $this->basket->lines()->createMany($lines);
        } else {
            $lines = $this->basket->lines()->create($lines);
        }

        $this->insertedLines->push($lines);

        return $this;
    }

    public function update()
    {
        return $this;
    }

    /**
     * Remove items
     *
     * @param mixed $id
     *
     * @return BasketManager
     */
    public function remove($id)
    {
        $where = 'where';

        if (! is_array($id)) {
            $id = (array)$id;
        }

        $id = $this->basket->decodeIds($id);

        if (count($id) === 1) {
            $id = $id[0];
        } else {
            $where = 'whereIn';
        }

        $products = $this->basket->lines()->{$where}('id', $id)->get();

        if ($products) {
            $parentIds = $products->whereNotNull('parent_id')->orWhere('parent_id', '!=', '')->pluck('parent_id')->unique()->toArray();

            $ids = array_merge($id, $parentIds);

            $where = 'whereIn';

            if (count($ids) === 1) {
                $where = 'where';
            }

            // delete child and parent
            $this->basket->lines()->{$where}('parent_id', $ids)->delete();
            $this->basket->lines()->{$where}('id', $ids)->delete();
        }

        return $this;
    }

    public function get()
    {
        return $this->factory->init($this->basket->fresh('lines'))->get();
    }

    public function forget()
    {
        $this->basket->owner_id = null;
        $this->basket->save();

        return $this;
    }

    public function clear()
    {
        $this->basket->lines()->delete();
        $this->basket->discounts()->delete();

        return $this;
    }

    public function destroy()
    {
        $this->clear();
        $this->basket->delete();
    }

    public function merge()
    {
        $this->basket->refresh();
        // parent_id ascending to ensure child level
        $lines = $this->basket->lines->where('status_level', '>=', 0)->whereIn('item_type', ['I', 'W'])->sortBy('id');

        $arrHash               = [];
        $toBeRemovedItemRowIds = [];

        foreach ($lines as $line) {
            $line->hash = $line->getHash();
        }

        foreach ($lines as $line) {
            $hash     = $line->hash;
            $parentId = $line->parent_id ?? 0;
            $hasChild = $lines->where('parent_id', $line->id)->count() > 0;

            if ($parentId > 0) {
                $mainItem = $lines->where('id', $parentId)->first();

                if ($mainItem) {
                    $hash = md5($mainItem->hash.$hash);
                }
            } else if ($hasChild) {
                $hash .= $lines->where('parent_id', $line->id)->reduce(function ($carry, $item) {
                    return $carry.$item->hash;
                });
                $hash = md5($hash);
            }

            $line->hash = $hash;
        }

        foreach ($lines as $line) {

            if (in_array($line->id, $toBeRemovedItemRowIds, false)) {
                continue;
            }

            $identicalLines = $lines->where('id', '!=', $line->id)->where('hash', $line->hash);

            if (! in_array($line->hash, $arrHash, false) && $identicalLines->isNotEmpty()) {
                $arrHash[]      = $line->hash;
                $line->item_qty += $identicalLines->sum('item_qty');

                unset($line->hash);

                $line->update();
                $toBeRemovedItemRowIds = array_merge($toBeRemovedItemRowIds, $identicalLines->pluck('id')->all());
            }

        }

        if (count($toBeRemovedItemRowIds) > 0) {
            $this->basket->lines()->whereIn('id', $toBeRemovedItemRowIds)->delete();
        }

        return $this;
    }

    public function basket()
    {
        return $this->basket;
    }

    public function save()
    {
        $this->basket->owner_id = $this->ownerId;
        $this->basket->save();

        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setOwnerId($id)
    {
        $this->ownerId = $id;

        return $this;
    }

    public function setCustomData($data)
    {
        $this->basket->custom_data = $data;
        $this->save();

        return $this;
    }

    public function setDiscounts($discounts)
    {
        $this->discounts = $discounts;
    }

    public function isEmpty()
    {
        return $this->basket->lines->isEmpty();
    }

    public function model()
    {
        return $this->basket;
    }

    public function getInsertedLines()
    {
        return $this->insertedLines;
    }

}