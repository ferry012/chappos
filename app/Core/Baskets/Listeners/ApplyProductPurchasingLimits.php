<?php


namespace App\Core\Baskets\Listeners;


use App\Core\Baskets\Events\BasketSavedEvent;
use App\Support\Carbon;
use App\Utils\Helper;
use Illuminate\Support\Facades\Schema;

class ApplyProductPurchasingLimits
{
    public $isGuest;

    /**
     * Handle the event.
     *
     * @param BasketSavedEvent $event
     *
     * @return void
     */
    public function handle(BasketSavedEvent $event)
    {
        $basket = $event->basket;
        $this->isGuest = $basket->isGuest();
        $uid = $basket->owner_id;

        if ($basket->isStaff() || $basket->cart_name !== 'pos') {
            return;
        }

        foreach ($basket->lines as $line) {

            $promoId = $line->getCustomData('promo_id');

            if (!empty($line->parent_id) || empty($promoId) || $line->status_level < 0) {
                continue;
            }

            $purchaseLimits = app('api')->products()->getPurchaseLimits($promoId, $line->item_id);
            $isExceeded = $this->isExceedOfPurchasingLimits($purchaseLimits, $uid, $line);

            $line->unsetCustomData('message');

            if ($isExceeded) {
                $prices = $line->getCustomData('prices');
                $newUnitPrice = $line->regular_price;

                if (!empty($uid) && isset($prices['member'])) {
                    $newUnitPrice = $prices['member'];
                }

                if ($newUnitPrice > 0) {
                    $line->unit_price = $newUnitPrice;
                }

                $line->setCustomData('message', 'Oops! Item has exceeded the purchase quantity limit.');
            }

            $line->save();
        }
    }

    protected function isExceedOfPurchasingLimits($limits, $uid, $item)
    {

        $qty = $item->item_qty;
        $pid = $item->item_id;

        if (empty($limits)) {
            return false;
        }

        if (isset($limits['per_trans']) && $limits['per_trans'] > 0 && $qty > $limits['per_trans']) {
            return true;
        }

        if (!Schema::connection(env('DB_CONNECTION'))->hasTable('v_ordered_items_last_6_months')) {
            return false;
        }

        if (!$this->isGuest && isset($limits['per_member']) && $limits['per_member'] > 0) {

            $totalQty = db()->table('v_ordered_items_last_6_months')->where('customer_id', $uid)->where('item_id', $pid)->get()->sum(static function ($row) {
                return $row->qty_ordered - $row->qty_refunded;
            });

            if (($totalQty + $qty) > $limits['per_member']) {
                return true;
            }

        }

        if (isset($limits['per_day']) && $limits['per_day'] > 0) {

            $totalQty = db()->table('v_ordered_items_last_6_months')->where('item_id', $pid)->whereDate('pay_on', Carbon::today()->toDateString())->get()->sum(static function ($row) {
                return $row->qty_ordered - $row->qty_refunded;
            });

            if (($totalQty + $qty) > $limits['per_day']) {
                return true;
            }

        }

        if (isset($limits['max']) && $limits['max'] > 0) {

            $totalQty = db()->table('v_ordered_items_last_6_months')->where('item_id', $pid)->get()->sum(static function ($row) {
                return $row->qty_ordered - $row->qty_refunded;
            });

            if (($totalQty + $qty) > $limits['max']) {
                return true;
            }
        }

        // Table: crm_promo_item.limit_stock_promo
        if (!$this->isGuest && isset($limits['promo_max']) && $limits['promo_max'] > 0) {

            if (isset($item->custom_data) && isset($item->custom_data['promo_id'])) {

                $promoItems = $this->getPromoItems($item->custom_data['promo_id'], $pid);

                $totalQty = db()->table('v_ordered_items_last_6_months')->where('customer_id', $uid)->whereIn('item_id', $promoItems)->get()->sum(static function ($row) {
                    return $row->qty_ordered - $row->qty_refunded;
                });

                if (($totalQty + $qty) > $limits['promo_max']) {
                    return true;
                }
            }

        }

        return false;
    }

    protected function getPromoItems($promoId, $itemId = '')
    {

        $cacheOpts = [
            "promoId" => $promoId,
            "itemId"  => $itemId
        ];
        list($cacheTag, $cacheKey) = Helper::generateCacheKeys(['products', 'promoItems'], $itemId, $cacheOpts);

        return app('cache')->tags($cacheTag)->remember($cacheKey, 2 /*$this->productCacheSeconds*/, function () use ($promoId, $itemId) {

            $items = db()->table('v_ims_live_promo')->where('promo_type', 'PRC')->where('promo_id', $promoId)->select('ref_id')->get();

            if ($items) {
                return $items->pluck('ref_id');
            }

            return [];

        });
    }
}