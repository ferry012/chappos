<?php

namespace App\Core\Baskets\Listeners;

use App\Core\Baskets\Events\BasketSavedEvent;
use App\Core\Discounts\DiscountFactory;
use App\Core\Discounts\DiscountFactoryV2;
use App\Core\Discounts\Models\Discount;

class ApplyDiscountsListener
{
    /**
     * Handle the event.
     *
     * @param  BasketSavedEvent $event
     *
     * @return void
     */
    public function handle(BasketSavedEvent $event)
    {
        $basket = $event->basket;

        (new DiscountFactoryV2())->setBasket($basket)->resolve();
    }
}