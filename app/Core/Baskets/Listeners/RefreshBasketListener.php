<?php

namespace App\Core\Baskets\Listeners;

use App\Core\Baskets\Events\BasketSavedEvent;

class RefreshBasketListener
{
    /**
     * Handle the event.
     *
     * @param  BasketSavedEvent $event
     *
     * @return void
     */
    public function handle(BasketSavedEvent $event)
    {
        $basket = $event->basket;

        app('api')->baskets()->calculate($basket);

    }
}