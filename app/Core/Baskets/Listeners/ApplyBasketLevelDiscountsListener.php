<?php

namespace App\Core\Baskets\Listeners;


use App\Core\Baskets\Events\BasketSavedEvent;
use App\Core\Baskets\Models\BasketLevelDiscount;
use App\Support\Str;

class ApplyBasketLevelDiscountsListener
{
    /**
     * The priority-sorted list of discount rule.
     *
     * Forces the listed discount rule to always be in the given order.
     * Free item should be top priority
     * Order discount should be last priority
     *
     * @var array
     */
    public $discountPriority = [
        'BUY_X_GET_X',
        'BUY_X_GET_Y',
        'AMOUNT_OFF_Y',
        'X_OFF_ITEMS_IN_Y_BRANDS',
    ];

    /**
     * Handle the event.
     *
     * @param  BasketSavedEvent $event
     *
     * @return void
     */
    public function handle(BasketSavedEvent $event)
    {

        $basket = $event->basket;

        if ($basket->isStaff() || $basket->cart_name === 'hachi') {
            return;
        }

        $discounts = BasketLevelDiscount::active()->get();

        foreach ($this->sort($discounts) as $discount) {
            $class = 'App\\Core\\Baskets\\Promotions\\'.Str::studly(Str::lower($discount->rule_type));

            if (class_exists($class)) {
                (new $class())->setBasket($basket)->setDiscount($discount)->run();
            }

        }

    }

    /**
     * Sort the given discount by priority.
     *
     * @param  \Illuminate\Support\Collection $discounts
     *
     * @return \Illuminate\Support\Collection
     */
    protected function sort($discounts)
    {

        if ($discounts->isNotEmpty()) {
            foreach ($discounts as $discount) {

                $discount->priority = 999;

                if ($index = array_search($discount->rule_type, $this->discountPriority, false)) {
                    $discount->priority = $index;
                }

            }
        }

        // Sort ascending
        return $discounts->sortBy('priority');
    }
}