<?php

namespace App\Core\Baskets\Listeners;


use App\Core\Baskets\Events\BasketSavedEvent;
use App\Models\StaffPurchase;
use App\Support\Carbon;
use App\Support\Str;
use RtLopez\Decimal;

/**
 * Staff not allowed to enjoy member and promo price
 *
 * Class ApplyStaffPriceListener
 *
 * @package App\Core\Baskets\Listeners
 */
class ApplyStaffPriceListener
{
    /**
     * Handle the event.
     *
     * @param  BasketSavedEvent $event
     *
     * @return void
     */
    public function handle(BasketSavedEvent $event)
    {
        $basket = $event->basket;

        if (! $basket->isStaff() || $basket->cart_name === 'hachi') {
            return;
        }

        $superPurchase = $basket->getCustomData('staff_super_purchase', false);

        list(, $id) = array_pad(explode('_', $basket->owner_id), -2, '');

        $staffPurchase = StaffPurchase::find([
            'coy_id'         => coy_id(), 'staff_id' => $id,
            'financial_year' => Carbon::now()->format('Y'),
        ]);

        if ($staffPurchase === null) {
            $basket->setCustomData('message', 'Oops! Staff purchase not found. Please clarify with HR department.');

            return;
        }

        if ($staffPurchase->status_level < 1) {
            $basket->setCustomData('message', 'Oops! Your staff purchase is not activated. Please clarify with HR department.');

            return;
        }

        if ($staffPurchase->purchase_balance <= 0 && ! $superPurchase) {
            $basket->setCustomData('message', 'Oops! Purchase balance is not enough.');

            return;
        }

        // Daily one transaction only
        /*
        if ($staffPurchase->last_purchased_on && Carbon::parse($staffPurchase->last_purchased_on)->isToday()) {
            $basket->setCustomData('message', 'Only one transaction per day.');

            return;
        }
        */

        $purchaseBalance  = $staffPurchase->purchase_balance;
        $itemsMarkupPrice = collect();

        $purchasedItemList = app('cache')->remember("staff.{$id}_purchase_item", Carbon::now()->addSeconds(30), function () use ($basket) {
            return db()->table('o2o_order_item')->whereIn('order_id', function ($query) use ($basket) {
                $query->select('id')->from('o2o_order_list')
                    ->where('coy_id', coy_id())
                    ->where('customer_id', $basket->owner_id)
                    ->where('order_status_level', '>', 0)
                    ->whereYear('placed_on', Carbon::now()->format('Y'));
            })->where('status_level', '>=', 0)
                ->where('custom_data->is_staff_price', 'true')
                ->groupBy('item_id')
                ->selectRaw('item_id, sum(qty_ordered - qty_refunded) as qty')
                ->get();
        });

        // Calculate all the items markup price first
        foreach ($basket->lines as $line) {

            $message = '';
            $markup  = 0;

            if ($line->status_level >= 0 && $line->item_type !== 'D' && ! $itemsMarkupPrice->has($line->item_id) && $line->product && $line->product->allow_discount === 'Y') {

                $fields = ['item_id', 'inv_dim4', 'inv_dim3', 'inv_dim2'];

                // Get the according markup
                foreach ($fields as $field) {
                    if ($row = db('pg_cherps')->table('ims_staff_markup')->where($field, $line->product->{$field})->first()
                    ) {
                        $markup = ((float)$row->markup_percent / 100) + 1;
                        break;
                    }
                }

                if (Decimal::create($markup)->gt(0)) {

                    $newPrice = $line->regular_price;

                    $rows = db('pg_cherps')->table('ims_item_price')->where('coy_id', coy_id())
                        ->where('item_id', $line->item_id)
                        ->where('price_type', 'SUPPLIER')
                        ->where('eff_from', '<', now())
                        ->where('eff_to', '>', now())
                        ->where('unit_price', '>', 0)
                        ->where('price_ind', 'PRI')
                        ->get();

                    if ($rows->count() === 1) {
                        $row          = $rows->first();
                        $row->curr_id = Str::trim($row->curr_id);

                        // Supplier price could be set as foreign currency
                        // need to convert to local currency first
                        if (Str::contains($row->curr_id, 'SGD', false)) {
                            $newPrice = $row->unit_price * $markup;
                        } else {
                            $exchange = db('pg_cherps')->table('coy_foreign_exchg')->where('coy_id', coy_id())
                                ->where('eff_from', '<', now())
                                ->where('eff_to', '>', now())
                                ->where('curr_id', $row->curr_id)
                                ->orderBy('created_on', 'DESC')
                                ->first();

                            if ($exchange) {
                                $newPrice = ($row->unit_price * (float)$exchange->exchg_rate) * $markup;
                            } else {
                                $message = "Oops! Currency {$row->curr_id} rate is not set. Please contact Account department.";
                            }

                        }

                    }
                    else {
                        if ($rows->count()>1)
                            $message = "Oops! ".$rows->count()." different cost price found. Please contact MRD department.";
                        else
                            $message = "Oops! No cost price setting found. Please contact MRD department.";
                    }

                    $boughtItemQty = optional($purchasedItemList->where('item_id', $line->item_id)->first(), function ($obj) {
                            return (int)$obj->qty;
                        }) ?? 0;

                    $itemsMarkupPrice->put($line->item_id, [
                        'unit_price' => $newPrice * 1.07, 'qty' => $boughtItemQty, 'message' => $message,
                    ]);
                }
            }
        }

        $purchaseCtrl = $staffPurchase->getCustomData('purchase_control');

        foreach ($basket->lines as $line) {

            $rowItemsTotal    = 0;
            $enjoy            = false;
            $line->unit_price = $line->regular_price;
            $itemMarkup       = $itemsMarkupPrice->get($line->item_id);

            if ($itemMarkup) {

                $line->setCustomData('message', $itemMarkup['message']);

                if (! $superPurchase) {
                    $rowItemsTotal = $line->item_qty * $itemMarkup['unit_price'];

                    if ($purchaseBalance > 0 && $rowItemsTotal < $purchaseBalance) {
                        // Each SKU limit to certain qty
                        if ($purchaseCtrl) {

                            $itemTotalQty = $line->item_qty + $itemMarkup['qty'];

                            if (isset($purchaseCtrl[0]) && $itemTotalQty <= $purchaseCtrl[0]['qty'] && $line->unit_price <= $purchaseCtrl[0]['max_price']) {
                                $enjoy = true;
                            } elseif (isset($purchaseCtrl[1]) && $itemTotalQty <= $purchaseCtrl[1]['qty'] && $line->unit_price > $purchaseCtrl[1]['min_price'] && $line->unit_price <= $purchaseCtrl[1]['max_price']) {
                                $enjoy = true;
                            } elseif (isset($purchaseCtrl[2]) && $itemTotalQty <= $purchaseCtrl[2]['qty'] && $line->unit_price > $purchaseCtrl[2]['min_price']) {
                                $enjoy = true;
                            }

                            if (! $enjoy) {
                                $line->setCustomData('message', 'Oops! Item has exceeded the purchase quantity limit.');
                            }

                        }
                    } else {
                        $line->setCustomData('message', 'Oops! Item total price has exceeded the purchase balance.');
                    }
                }

            } else {
                $line->setCustomData('message', 'Oops! Item is not qualified for staff price.');
            }

            if ($itemMarkup && ($superPurchase || $enjoy)) {

                $line->setCustomData('is_staff_price', true);
                $line->unit_price = $itemMarkup['unit_price'];

                $itemMarkup['qty'] += $line->item_qty;
                // Update the quantity enjoyed for next apply
                $itemsMarkupPrice->put($line->item_id, $itemMarkup);
                // store temporary purchase balance
                $purchaseBalance -= $rowItemsTotal;
            }

            $line->save();
        }

    }
}