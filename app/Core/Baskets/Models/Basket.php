<?php

namespace App\Core\Baskets\Models;

use App\Core\Scaffold\BaseModel;
use App\Core\Orders\Models\Order;
use App\Support\Str;
use RtLopez\Decimal;

class Basket extends BaseModel
{
    const DELV_STANDARD = 'STD';
    const DELV_COLLECT = 'SCL';
    const DELV_EXPRESS = 'EXP';
    const DELV_SAMEDAY = 'SDD';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pg';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'o2o_cart_list';

    const UPDATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cart_name',
        'loc_id',
        'status_level',
        'owner_id',
        'custom_data',
    ];


    /**
     * Get the basket lines.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lines()
    {
        return $this->hasMany(BasketLine::class, 'cart_id');
    }

    public function discounts()
    {
        return $this->hasMany(BasketDiscount::class, 'cart_id');
    }

    public function order()
    {
        return $this->hasOne(Order::class, 'cart_id');
    }

    public function activeOrder()
    {
        return $this->hasOne(Order::class, 'cart_id')->where('order_status_level', 0);
    }

    public function placedOrder()
    {
        return $this->hasOne(Order::class, 'cart_id')->withoutGlobalScope('open')->whereNotNull('placed_on');
    }

    public function getNameAttribute()
    {
        return $this->cart_name;
    }

    public function getHasShippingAttribute()
    {
        if ($this->containsGiftCardOnly()) {
            return false;
        }

        if ($this->containsDigitalOnly()) {
            return false;
        }

        if ($this->hasAnyShippingItems()) {
            return true;
        }

        return false;
    }

    public function isGuest()
    {
        return empty($this->owner_id) || Str::startsWith($this->owner_id, 'GT');
    }

    public function isMember()
    {
        return !$this->isGuest() && !$this->isStaff();
    }

    public function isStaff()
    {
        return !$this->isGuest() && Str::startsWith($this->owner_id, 'STAFF_', false);
    }

    public function hasMembershipCard()
    {
        return $this->lines->where('item_type', 'M')->count() > 0;
    }

    public function hasAnyStandardDeliveryItems()
    {
        return $this->lines->where('item_type', 'I')->where('delv_method', 'STD')->count() > 0;
    }

    public function hasAnyExpressDeliveryItems()
    {
        return $this->lines->where('item_type', 'I')->where('delv_method', 'SDD')->count() > 0;
    }

    public function hasAnyShippingItems()
    {
        return $this->hasAnyStandardDeliveryItems() || $this->hasAnyExpressDeliveryItems();
    }

    public function containsDigitalOnly()
    {
        $isDigitalOnly = true;
        $lines = $this->lines->where('status_level', '>=', 0)->all();

        if ($lines) {
            foreach ($lines as $line) {
                if (!$line->isDigital()) {
                    $isDigitalOnly = false;
                    break;
                }
            }
        } else {
            $isDigitalOnly = false;
        }

        return $isDigitalOnly;
    }

    public function containsGiftCardOnly()
    {
        $totalLineCount = $this->lines->where('status_level', '>=', 0)->count();
        $itemCount = $this->lines->where('item_type', 'E')->count();

        return $itemCount > 0 && ($totalLineCount === $itemCount);
    }

    public function containsMembershipCardOnly()
    {
        $totalLineCount = $this->lines->where('status_level', '>=', 0)->count();
        $itemCount = $this->lines->where('item_type', 'M')->count();

        return $totalLineCount === 1 && $itemCount === 1;
    }

    public function isEntitledGuestPrice()
    {
        return $this->isGuest() && !$this->hasMembershipCard();
    }

    public function isEntitledMemberPrice()
    {
        return !$this->isGuest() || $this->hasMembershipCard();
    }

    public function getNextLineNumber()
    {
        return $this->lines->max('line_num') + 1;
    }

    public function getTotalSavings()
    {
        $value = $this->lines->filter(function ($line) {
            return $line->status_level >= 0 && $line->item_type !== 'D';
        })->sum(function ($line) {
            $memberPrice = .0;
            $unitPrice = $line->unit_price;

            $isFreeItem = $line->getCustomData('is_free', false);

            if ($isFreeItem || Decimal::create($unitPrice)->eq(0)) {
                return .0;
            }

            if ($product = $line->product) {
                $memberPrice = $product->mbr_price;

                if ($memberPrice > 0) {
                    $unitPrice = $memberPrice;
                }
            }

            if (Decimal::create($line->regular_price)->eq($memberPrice)) {
                return .0;
            }

            return ($line->regular_price - $unitPrice) * $line->item_qty;
        });

        return 1.0 * $value;
    }

    public function anyErrors()
    {
        return $this->lines()->where('status_level', -1)->exists();
    }

    public function isPayable()
    {
        return Decimal::create($this->total_amount)->gt(0);
    }

    public function getAdditionalRebateAmount()
    {
        $points = 0;
        $lines = $this->lines()->where('status_level', '>=', 0)->where('item_type', 'D')->get();

        if ($lines->isNotEmpty()) {
            $points = $lines->sum(function ($item) {
                return $item->getCustomData('unit_points_earned', 0);
            });
        }

        return app('api')->products()->convertPointsToRebate($points);
    }

    public function flushDiscounts()
    {
        $this->lines()->where('item_type', 'D')->delete();
        // Remove discount data attributes
        $this->lines->map(static function ($line) {
            $line->discount_total = 0;
            $line->unsetCustomData('discounts');
            $line->unsetCustomData('rewards');
            $line->save();

            return $line;
        });

        $this->refresh();

        return true;
    }

    public function itemCount($deliveryMode = null, $getQty = false)
    {
        $lines = $this->lines->filter(function ($line) use ($deliveryMode) {
            if ($line->item_type === 'D') {
                return false;
            }

            if ($line->status_level === -1) {
                return false;
            }

            if (!empty($line->delv_method) && $line->delv_method === $deliveryMode) {
                return true;
            }

            return true;
        });

        if ($getQty) {
            return $lines->sum(function ($line) {
                return abs($line->item_qty);
            });
        }

        return $lines->count();
    }

    public function anySubscriptions()
    {
        $count = $this->lines->filter(function ($line) {
            return $line->getCustomData('is_subscription', false);
        })->count();

        return $count > 0;
    }
}