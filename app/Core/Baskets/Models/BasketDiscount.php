<?php

namespace App\Core\Baskets\Models;

use App\Core\Scaffold\BaseModel;

class BasketDiscount extends BaseModel
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pg';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'o2o_cart_discount';

    const UPDATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cart_id', 'coupon_id', 'coupon_type', 'coupon_name', 'coupon_code', 'is_applied', 'error_msg', 'coupon_amount',
    ];

    /**
     * Get applied discount
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     *
     * @return self
     */
    public function scopeApplied($builder)
    {
        return $builder->where('is_applied', true);

    }

    /**
     * Get active discount
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param                                        $minutes int default 5
     *
     * @return self
     */
    public function scopeActive($builder, $minutes = 5)
    {
        return $builder->whereRaw("created_on::timestamp - interval '{$minutes} minutes' > ?", [now()]);
    }
}