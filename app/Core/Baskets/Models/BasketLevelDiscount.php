<?php

namespace App\Core\Baskets\Models;


use App\Core\Scaffold\BaseModel;
use Illuminate\Database\Eloquent\Builder;

class BasketLevelDiscount extends BaseModel
{
    protected $table = 'crm_cart_promo';

    protected $casts = [
        'attribute_data' => 'json',
        'loc_id'         => 'pg_array',
    ];

    public function scopeActive(Builder $query)
    {
        return $query->where('status_level', 1);
    }
}