<?php

namespace App\Core\Baskets\Models;

use App\Core\Product\Models\Product;
use App\Core\Scaffold\BaseModel;
use App\Core\Traits\Lines;
use App\Support\Str;
use Illuminate\Database\Eloquent\Builder;
use RtLopez\Decimal;

class BasketLine extends BaseModel
{
    use Lines;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pg';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'regular_price' => 'float',
        'unit_price'    => 'float',
        'item_qty'      => 'float',
        'status_level'  => 'int',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'o2o_cart_item';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item_medium',
        'item_id',
        'item_type',
        'item_img',
        'item_desc',
        'parent_id',
        'regular_price',
        'unit_price',
        'unit_discount_amount',
        'discount_total',
        'item_qty',
        'delv_method',
        'loc_id',
        'custom_data',
        'status_level',
        'created_by',
    ];

    protected $appends = [
        'final_price',
        'serialized_custom_data',
    ];

    public function basket()
    {
        return $this->belongsTo(Basket::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'item_id', 'item_id');
    }

    /**
     * Scope to query items of a cart
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $basket Basket object or basket id
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfBasket($query, $basket)
    {
        $basketId = is_object($basket) ? $basket->id : $basket;

        return $query->where('cart_id', $basketId);
    }

    public function scopeActive(Builder $builder)
    {
        return $builder->where('status_level', '<>', -1);
    }

    public function isParent()
    {
        $parentId = $this->parent_id ?? 0;

        return !$parentId;
    }

    public function isChild()
    {
        return !$this->isParent();
    }

    public function total()
    {
        return $this->final_price * $this->item_qty;
    }

    public function getIsFreeAttribute()
    {
        return Decimal::create($this->price_after_discount)->le(0);
    }

    public function getPriceAfterDiscountAttribute()
    {
        $value = $this->final_price - $this->unit_coupon_discount;

        if ($value < 0) {
            return .0;
        }

        return $value;
    }

    public function getSerializedCustomDataAttribute()
    {
        $data = $this->getCustomData();
        $data = array_except($data, ['actions', 'discounts', 'rewards', 'message', 'multiple_points']);
        ksort($data, SORT_NATURAL | SORT_FLAG_CASE);
        $data = array_values($data);

        return serialize($data);
    }

    public function setUnitDiscountAmountAttribute($value)
    {
        if ($this->unit_price < $value) {
            $value = $this->unit_amount;
        }

        $this->attributes['unit_discount_amount'] = $value;
    }

    public function getQuantity()
    {
        return $this->item_qty;
    }

    public function getHash()
    {
        $fields = [
            'item_id',
            'unit_price',
            'delv_method',
            'loc_id',
            'status_level',
            'serialized_custom_data',
        ];
        $str = '';

        if (!empty($this->getAttribute('parent_id'))) {
            $str .= 'CHILDREN';
        }

        foreach ($fields as $name) {
            $str .= $this->getAttribute($name);
        }

        return md5($str);
    }

    public function isDiscountItem()
    {
        return $this->item_type === 'D';
    }

    public function isMembershipItem()
    {
        return $this->item_type === 'M';
    }

    public function isBuyable()
    {
        return $this->status_level >= 0 && $this->item_type !== 'D' and $this->item_qty > 0;
    }

    public function isOverdiscount()
    {
        $priceTotal = $this->unit_price * $this->item_qty;

        return $this->discount_total > 0 && $this->discount_total > $priceTotal;
    }

    public function getUnitRebateAmount($rebateTier = 0)
    {
        $multiplier = (float)$this->getCustomData('multiple_points', 0);
        $promoId = Str::trim($this->getCustomData('mpt.id'));
        $type = '';
        $extraPoints = 0;

        if (!empty($promoId)) {
            $type = $this->getCustomData('mpt.type', '');
            $multiplier = (float)$this->getCustomData('mpt.multiple_points', 0);
            $extraPoints = (float)$this->getCustomData('mpt.add_amount', 0);
        }


        return app('api')->products()->calculateUnitRebateAmount($this->discounted_price, $multiplier, $rebateTier, $extraPoints, $type);
    }

    public function getUnitRebatePoints($rebateTier = 0)
    {
        $multiplier = (float)$this->getCustomData('multiple_points', 0);
        $promoId = Str::trim($this->getCustomData('mpt.id'));
        $type = '';
        $extraPoints = 0;

        if (!empty($promoId)) {
            $type = $this->getCustomData('mpt.type', '');
            $multiplier = (float)$this->getCustomData('mpt.multiple_points', 0);
            $extraPoints = (float)$this->getCustomData('mpt.add_amount', 0);
        }

        return app('api')->products()->calculateUnitRebatePoints($this->discounted_price, $multiplier, $rebateTier, $extraPoints, $type);
    }

    public function isDigital()
    {
        return in_array($this->item_type, [
                'A',
                'C',
                'D',
                'E',
                'K',
                'L',
                'M',
                'P',
                'R',
                'S',
                'T',
                'V',
                'W',
                'U',
                'G',
                'H',
            ])
            || Str::containsAny(Str::lower($this->item_desc), ['gift card', 'digital download']);
    }

    public function isGiftCard()
    {
        return in_array($this->item_type, [
                'G', 'E'
            ]) && Str::contains(Str::lower($this->item_desc), 'gift card');
    }

    public function isSubscription()
    {
        return $this->item_type === 'K' && Str::contains(Str::upper($this->product->inv_dim4), '-SUBSCRIBE', false);
    }
}