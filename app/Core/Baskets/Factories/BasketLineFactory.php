<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 22/3/2019
 * Time: 5:59 PM
 */

namespace App\Core\Baskets\Factories;


use App\Core\Baskets\Contracts\BasketDiscountFactoryContract as BasketDiscountFactory;
use App\Core\Baskets\Interfaces\BasketLineInterface;
use App\Core\Baskets\Models\BasketLine;
use Illuminate\Support\Collection;

class BasketLineFactory implements BasketLineInterface
{
    /**
     * The basket lines.
     *
     * @var Collection
     */
    public $lines;

    /**
     * The discount factory.
     *
     * @var DiscountFactoryInterface
     */
    protected $discounts;

    /**
     * The tax calculator instance.
     *
     * @var TaxCalculatorInterface
     */
    protected $tax;

    public function __construct(BasketDiscountFactory $discount)
    {
        $this->lines = collect();
    }

    /**
     * Add lines to the instance.
     *
     * @param Collection $lines
     *
     * @return void
     */
    public function add($lines)
    {
        $this->lines = $lines;

        return $this;
    }

    /**
     * Initialise the factory.
     *
     * @param BasketLine $lines
     *
     * @return BasketLineFactory
     */
    public function init(BasketLine $lines)
    {
        $this->lines = $lines;

        return $this;
    }

    /**
     * Add a discount to the instance.
     *
     * @param string $coupon
     *
     * @return self
     */
    public function discount($coupon)
    {
        $this->discounts->add($coupon);

        return $this;
    }

    /**
     * Get the basket line.
     *
     * @return Collection
     */
    public function get()
    {

        $lineNum = 1;

        foreach ($this->lines->sortBy('id') as $line) {

            $line->line_num = $lineNum;

            if (! isset($line->pwp_line_num) || $this->lines->where('parent_id', $line->id)->count() > 0) {
                $line->pwp_line_num = 0;
            }

            $this->lines->where('parent_id', $line->id)->map(function ($line) use ($lineNum) {
                $line->pwp_line_num = $lineNum;

                return $line;
            });

            $lineNum++;
        }

        return $this->lines->sortBy('id');
    }

}