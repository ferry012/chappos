<?php

namespace App\Core\Baskets\Factories;


use App\Core\Baskets\Interfaces\BasketInterface;
use App\Core\Baskets\Models\Basket;
use App\Core\Baskets\Interfaces\BasketLineInterface;

class BasketFactory implements BasketInterface
{
    /**
     * The current basket.
     *
     * @var BasketFactory
     */
    protected $basket;

    /**
     * The order attached to the basket.
     *
     * @var Order
     */
    protected $order;

    /**
     * The basket lines.
     *
     * @var BasketLineInterface
     */
    public $lines;

    protected $tax;

    public function __construct(BasketLineInterface $basketLineFactory)
    {
        $this->lines = $basketLineFactory;
    }

    public function init(Basket $basket)
    {
        $basket->load('discounts');
        $this->basket = $basket;
        $this->lines->add($basket->lines)->get();

        return $this;
    }

    public function get()
    {
        return app('api')->baskets()->calculate($this->basket);
    }

    /**
     * Clone the basket.
     *
     * @return Basket
     */
    public function clone()
    {
        $clone = $this->basket->replicate();

        $clone->save();

        foreach ($clone->lines as $line) {
            $cloned = $line->replicate();
            $cloned->save();
        }

        return $clone;
    }
}