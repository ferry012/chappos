<?php

namespace App\Core\Baskets\Factories;

use App\Core\Baskets\Contracts\BasketDiscountFactoryContract;

class BasketDiscountFactory implements BasketDiscountFactoryContract
{
    protected $discounts;

    public function __construct()
    {
        $this->discounts = collect();
    }

    public function add($discount)
    {
        $this->discounts->push($discount);

        return $this;
    }

    public function get()
    {
        return $this->discounts;
    }
}