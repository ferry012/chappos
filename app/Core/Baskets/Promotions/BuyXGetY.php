<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 26/9/2019
 * Time: 11:32 AM
 */

namespace App\Core\Baskets\Promotions;


use App\Core\Baskets\Models\Basket;

class BuyXGetY extends DiscountRule
{
    public function handle(Basket $basket)
    {
        $items = $this->basket->lines()->where('status_level', '>=', 0)->whereNull('parent_id')->whereNotNull('custom_data->promo->p'.$this->discount->id)->get();

        if ($items->isNotEmpty()) {

            $hash      = $items->first()->getCustomData($this->getPromoPrefix().'hash');
            $strToHash = $items->sortBy('item_id')->reduce(function ($str, $item) {
                return $str.$item->item_id.$item->item_qty;
            });

            if ($hash === md5($strToHash)) {

                $total = $items->sum('item_qty');

                $leastQty = $this->min_qty + $this->discount->discount_qty;

                if ($total < $leastQty) {
                    foreach ($items as $item) {
                        $item->unit_discount_amount = .0;
                        $item->unsetCustomData($this->getPromoPrefix());
                        $item->save();
                    }
                } else {

                    $items->filter(function ($item) {
                        return $item->getCustomData($this->getPromoPrefix().'is_free', false);
                    })->each(function ($item) {
                        $item->unit_discount_amount = $item->unit_price;
                        $item->save();
                    });
                }

                return;
            }

            $freeItemsIssued = $items->filter(function ($item) {
                return $item->getCustomData($this->getPromoPrefix().'is_free', false);
            });

            $payableItems = $items->filter(function ($item) {
                return ! $item->getCustomData($this->getPromoPrefix().'is_free', false);
            });

            foreach ($freeItemsIssued as $item) {
                $payableItem = $payableItems->where('item_id', $item->item_id)->first();

                if ($payableItem) {
                    $payableItem->item_qty += $item->item_qty;
                    $payableItem->save();
                    $item->delete();
                } else {
                    $item->unit_discount_amount = .0;
                    $item->unsetCustomData($this->getPromoPrefix());
                    $item->save();
                }

            }

            foreach ($payableItems as $item) {
                $item->unsetCustomData($this->getPromoPrefix());
                $item->save();
            }

            $basket->refresh();
        }

        $this->items = $this->getEligibleItems($basket);

        $total = $this->items->sum('item_qty');

        $leastQty = $this->min_qty + $this->discount->discount_qty;

        if ($total < $leastQty) {
            return;
        }

        $freeQty = $this->discount->discount_qty;

        if (! $this->discount->apply_only_once) {
            $freeQty = $this->getFreeQuantities($total);
        }

        $items = $this->items->sortByDesc('unit_price');

        $itemIds = collect();

        foreach ($items as $item) {

            // Repeat n times of id depends on qty
            $itemIds->push(collect($item)->times($item->item_qty, function () use ($item) {
                return (string)$item->id;
            }));

        }

        $itemIds = $itemIds->flatten();

        $adjMode = $this->getAttribute('adjustment_mode', 'cheapest');

        if ($adjMode === 'cheapest') {
            // Free item
            // get the lowest item price (default sort high to low)
            $itemIds = $itemIds->reverse();

            if ($itemIds->count() > 1) {
                $freeIds = $itemIds->take($freeQty);
            } else {
                $freeIds = $itemIds->take(1);
            }

        } elseif ($adjMode === 'evenly') {
            if ($itemIds->count() > 1) {
                $freeIds = $itemIds->filter(function ($value, $key) {
                    return $key % 2 !== 0;
                })->take($freeQty);
            } else {
                $freeIds = $itemIds->take(1);
            }
        }

        $freeIds = $freeIds->unique()->values()->map(function ($id) use ($freeIds) {
            $qty = $freeIds->filter(function ($tId) use ($id) {
                return $tId === $id;
            })->count();

            return ['id' => $id, 'qty' => $qty];
        });

        foreach ($freeIds as $freeId) {
            $item = $items->where('id', $freeId['id'])->first();
            $item->setBlankCustomData($this->getPromoPrefix());

            if ($item->item_qty === $freeId['qty']) {
                $item->setCustomData($this->getPromoPrefix().'is_free', true);
                $item->unit_discount_amount = $item->unit_price;
            } else {
                $newFreeItem = $item->replicate([
                    'parent_id', 'item_qty', 'discount_total', 'custom_data',
                ]);
                $newFreeItem->setBlankCustomData($this->getPromoPrefix());
                $newFreeItem->setCustomData('lot_id', $item->getCustomData('lot_id'));
                $newFreeItem->setCustomData('salesperson_id', $item->getCustomData('salesperson_id'));
                $newFreeItem->setCustomData('price_edited', $item->getCustomData('price_edited'));
                $newFreeItem->setCustomData($this->getPromoPrefix().'is_free', true);
                $newFreeItem->item_qty             = $freeId['qty'];
                $newFreeItem->unit_discount_amount = $item->unit_price;

                /* YS: disable for timebeing for testing.
                // RRFID scan traces
                if ($item->hasCustomData('rfid')) {
                    $newFreeItem->setCustomData('rfid', $item->getCustomData('rfid'));
                }
                if ($item->hasCustomData('rfidscanned')) {
                    $newFreeItem->setCustomData('rfidscanned', $item->getCustomData('rfidscanned'));
                }
                if ($item->hasCustomData('rssi')) {
                    $newFreeItem->setCustomData('rssi', $item->getCustomData('rssi'));
                }
                if ($item->hasCustomData('rssiscanned')) {
                    $newFreeItem->setCustomData('rssiscanned', $item->getCustomData('rssiscanned'));
                }
                */
                
                $newFreeItem->push();

                $item->item_qty -= $freeId['qty'];
            }

            if ($item->item_qty > 0) {
                $item->save();
            } else {
                $item->delete();
            }
        }

        $items = $this->basket->lines()->where('status_level', '>=', 0)->whereNull('parent_id')->whereIn('id', $itemIds->unique()->toArray())->orWhereNotNull('custom_data->promo->p'.$this->discount->id)->get();

        $strToHash = $items->sortBy('item_id')->reduce(function ($str, $item) {
            return $str.$item->item_id.$item->item_qty;
        });

        $strHashed = md5($strToHash);

        $items->each(function ($item) use ($strHashed) {
            $item->setCustomData($this->getPromoPrefix().'hash', $strHashed);
            $item->save();
        });

    }
}