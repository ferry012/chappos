<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 25/9/2019
 * Time: 6:38 PM
 */

namespace App\Core\Baskets\Promotions;


use App\Core\Baskets\Models\Basket;
use App\Support\Carbon;
use App\Support\Str;

abstract class DiscountRule
{
    protected $attributes = [];
    protected $discount;
    protected $discount_unit;
    protected $discount_value;
    protected $basket;
    protected $item;
    protected $items;

    public function getAttribute($key, $default = null)
    {
        return array_get($this->discount->attribute_data, $key, $default);
    }

    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    public function __set($key, $value)
    {
    }

    public function offsetExists($offset)
    {
        return $this->getAttribute($offset) !== null;
    }

    public function __isset($key)
    {
        return $this->offsetExists($key);
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    public function setBasket($basket)
    {
        $this->basket = $basket;

        return $this;
    }

    public function checkPeriodAvailable()
    {

        // Prepare setting data
        $effPeriod        = $this->effective_period ?? [];
        $now              = Carbon::now();
        $dayOfWeek        = else_empty(array_get($effPeriod, 'day_of_week'), range(0, 7));
        $dayOfMonth       = else_empty(array_get($effPeriod, 'day_of_month'), range(1, 31)); // Assume all months have 31 days
        $timeSlots        = array_get($effPeriod, 'time', []);
        $exceptDayOfWeek  = array_get($effPeriod, 'except.day_of_week', []);
        $exceptDayOfMonth = array_get($effPeriod, 'except.day_of_month', []);

        $timeSlots     = collect($timeSlots);
        $dayOfWeek     = collect($dayOfWeek);
        $dayOfMonth    = collect($dayOfMonth);
        $timeAvailable = true;

        $dayOfWeek = $dayOfWeek->reject(function ($value) use ($exceptDayOfWeek) {
            return in_array($value, $exceptDayOfWeek, false);
        })->toArray();

        $dayOfMonth = $dayOfMonth->reject(function ($value) use ($exceptDayOfMonth) {
            return in_array($value, $exceptDayOfMonth, false);
        })->toArray();

        if ($timeSlots->isNotEmpty()) {

            // Get the future available slots and sort from the early time slot and reset the slot index value
            $timeSlots = $timeSlots->filter(function ($slot) use ($now) {

                if (isset($slot['end'])) {
                    // Ensure the end time is greater than the start time
                    return $now->gt(Carbon::parse($slot['start'])) && Carbon::parse($slot['end'])->gt(Carbon::parse($slot['start']));
                }

                // Current datetime is greater than start datetime
                return $now->gt(Carbon::parse($slot['start']));
            })->sortBy('start')->values();

            $timeSlotCount = $timeSlots->map(function ($slot, $key) use ($timeSlots) {

                // Next slot key
                $nextKey = $key + 1;

                // End time not set then get from next slot and minus 1 second to prevent overlapping
                if (! isset($slot['end']) && $timeSlots->has($nextKey)) {
                    $nextSlot    = $timeSlots->get($nextKey);
                    $slot['end'] = Carbon::parse($nextSlot['start'])->subSecond()->toTimeString();
                } else {
                    // Can't find the next slot then put end day
                    $slot['end'] = '23:59:59';
                }

                return $slot;
            })->filter(function ($slot) use ($now) {
                return $now->between(Carbon::parse($slot['start']), Carbon::parse($slot['end']), true);
            })->count();

            // Not slot found then current time is not available
            $timeAvailable = ($timeSlotCount > 0);
        }

        return in_array($now->dayOfWeek, $dayOfWeek, true) && in_array($now->day, $dayOfMonth, true) && $timeAvailable;
    }

    public function isRunnable()
    {

        if (! $this->discount->is_public && $this->basket->isGuest()) {
            return false;
        }

        // check active time period
        if (! Carbon::now()->between(Carbon::parse($this->discount->eff_from), Carbon::parse($this->discount->eff_to))) {
            return false;
        }

        if (! $this->checkPeriodAvailable()) {
            return false;
        }

        if ($this->discount->min_value && $this->discount->min_value > $this->basket->item_total) {
            return false;
        }

        // check location
        if (! empty($this->discount->loc_id) && ! in_array(request('loc_id'), $this->discount->loc_id, false)) {
            return false;
        }

        return true;
    }

    public function getEligibleItems(Basket $basket)
    {
        $items = $basket->lines()->where('status_level', '>=', 0)->whereNull('parent_id')->whereIn('item_id', function ($query) {

            $query->from('v_ims_live_product')->where('coy_id', 'CTL')
                ->when($this->discount->item_brands, function ($q) {

                    return $q->whereIn('brand_id', explode(',', $this->discount->item_brands));
                })->when($this->discount->item_brands_except, function ($q) {

                    return $q->whereNotIn('item_brands', $this->discount->item_brands_except);
                })->when($this->discount->items_only, function ($q) {

                    return $q->whereIn('item_id', explode(',', $this->discount->items_only));
                })->when($this->discount->items_except, function ($q) {

                    return $q->whereNotIn('item_id', explode(',', $this->discount->items_except));
                })->when($this->discount->item_catalogs, function ($q) {

                    $catalogs = explode(';', $this->discount->item_catalogs);

                    $q->where(function ($q) use ($catalogs) {

                        foreach ($catalogs as $catalog) {

                            $q->orWhere(function ($q) use ($catalog) {

                                list($parCatalog, $subCatalog) = array_pad(explode(',', $catalog), 2, null);

                                $q->where('inv_dim2', $parCatalog);

                                if ($subCatalog) {
                                    $q->Where('inv_dim3', $subCatalog);
                                }
                            });
                        }

                    });

                    return $q;
                })->when($this->discount->item_catalogs_except, function ($q) {
                    $catalogs = explode(';', $this->discount->item_catalogs_except);

                    $q->where(function ($q) use ($catalogs) {

                        foreach ($catalogs as $catalog) {

                            $q->orWhere(function ($q) use ($catalog) {

                                list($parCatalog, $subCatalog) = explode(',', $catalog);

                                if ($subCatalog === null) {
                                    $q->where('inv_dim2', '!=', $parCatalog);
                                } else {
                                    $q->where('inv_dim2', '=', $parCatalog)->Where('inv_dim3', '!=', $subCatalog);
                                }

                            });
                        }

                    });

                    return $q;
                })->select('item_id');

        })->orderBy('unit_price')->get();

        return $items;
    }

    public function handle(Basket $basket)
    {

    }

    public function run()
    {
        if (! $this->isRunnable()) {
            return;
        }

        $this->basket->refresh();

        $this->handle($this->basket);
    }

    public function getFreeQuantities($itemsQty = 0)
    {
        $minQty  = $this->getData('min_qty', 999) ?? 1;
        $freeQty = (int)$this->discount->discount_qty;

        return floor($itemsQty / ($minQty + $freeQty)) * $freeQty;
    }

    public function getDiscountStep($value)
    {
        $steps = collect($this->discount_steps ?? []);

        if ($step = $steps->sortBy('min_value')->where('min_value', '<=', $value)->sortBy('min_value')->last()) {

            if (! isset($step['max_discount'])) {
                $step['max_discount'] = .0;
            }

            return [$step['discount_value'], $step['max_discount']];
        }

        return [];
    }

    public function spreadDiscount($discountAmount)
    {
        // get the discount items only
        $lines = $this->items;

        if ($lines->isEmpty()) {
            return;
        }

        $itemTotal           = $lines->sum('price_total');
        $spreadDiscountLines = collect();

        foreach ($lines as $line) {

            // spread discount
            $spreadAmount         = ((($line->unit_price * $line->item_qty) / $itemTotal) * $discountAmount);
            $line->discount_total = $spreadAmount;
            $spreadDiscountLines->push($spreadAmount);

            $line->save();
        }

        $spreadDiscountTotal = $spreadDiscountLines->sum();

        $discountVarianceAmount = $discountAmount - $spreadDiscountTotal;

        // spread discount variance add to the highest amount
        if ($discountVarianceAmount > 0.0) {
            $line                 = $lines->sortByDesc('discounted_price_total')->first();
            $line->discount_total += $discountVarianceAmount;
            $line->save();
        }
    }

    public function calculateDiscountAmount($item, $qty = 0)
    {
        $unitPrice     = $item->price_after_discount;
        $step          = [];
        $discountValue = $this->discount->discount_value;
        $cappedAmount  = $this->discount->max_discount;

        if ($item->product && $item->product->brand_id === 'PLG' && app('api')->products()->getPromotionPrice($item->item_id) !== null) {
            return .0;
        }

        if ($qty > 0) {
            $step = $this->getDiscountStep($qty);
        } else {
            //$step = $this->getDiscountStep($amount);
        }

        if ($step) {
            list($discountValue, $cappedAmount) = $step;
        }

        $this->discount_value = $discountValue;
        $this->discount_unit  = $this->discount->discount_unit;

        if ($this->discount_on_regular_price && $item->product) {
            $unitPrice = $item->product->regular_price;
        }

        if ($this->discount->discount_unit === 'PERCENT') {
            $discountValue = $this->applyPercentage($unitPrice, $discountValue, $cappedAmount);
        }

        if ($this->discount_on_regular_price && $item->product) {
            $discountedPrice = $unitPrice - $discountValue;
            $priceDiff       = $item->unit_price - $discountedPrice;

            if ($priceDiff > 0) {
                $discountValue = $priceDiff;
            } else {
                $discountValue = 0;
            }

        }

        return $discountValue * 1.0;
    }

    public function getPromoPrefix()
    {
        return 'promo.p'.$this->discount->id.'.';
    }

    public function applyPercentage($amount, $percentage, $max = 0)
    {
        $amount *= ($percentage / 100);

        if ($max > 0 && $amount > $max) {
            $amount = $max;
        }

        return $amount;
    }

    public function getData($key, $default = null)
    {
        return array_get($this->discount->attribute_data, $key, $default);
    }
}