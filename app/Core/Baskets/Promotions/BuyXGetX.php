<?php

namespace App\Core\Baskets\Promotions;


use App\Core\Baskets\Models\Basket;

class BuyXGetX extends DiscountRule
{
    public function handle(Basket $basket)
    {
        $excludeIds = [];

        $items = $this->basket->lines()->where('status_level', '>=', 0)->whereNull('parent_id')->whereNotNull('custom_data->promo->p'.$this->discount->id)->get();

        if ($items->isNotEmpty()) {

            foreach ($items->groupBy('item_id') as $itemId => $itemGroup) {

                $hash      = $itemGroup->first()->getCustomData($this->getPromoPrefix().'hash');
                $strToHash = $itemGroup->sum('item_qty');

                if ($hash === md5($strToHash)) {

                    $total = $itemGroup->sum('item_qty');

                    $leastQty = $this->min_qty + $this->discount->discount_qty;

                    if ($total < $leastQty) {
                        foreach ($itemGroup as $item) {
                            $item->unit_discount_amount = .0;
                            $item->unsetCustomData($this->getPromoPrefix());
                            $item->save();
                        }
                    } else {

                        $freeItemIssued = $itemGroup->filter(function ($item) {
                            return $item->getCustomData($this->getPromoPrefix().'is_free', false);
                        })->first();

                        if ($freeItemIssued) {
                            $freeItemIssued->unit_discount_amount = $freeItemIssued->unit_price;
                            $freeItemIssued->save();
                        }
                    }

                    $excludeIds[] = $itemId;
                    continue;
                }

                $freeItemIssued = $itemGroup->filter(function ($item) {
                    return $item->getCustomData($this->getPromoPrefix().'is_free', false);
                })->first();

                if ($freeItemIssued) {
                    $payableItem = $itemGroup->filter(function ($item) {
                        return ! $item->getCustomData($this->getPromoPrefix().'is_free', false);
                    })->first();

                    if ($payableItem) {
                        $payableItem->item_qty += $freeItemIssued->item_qty;

                        $payableItem->unsetCustomData($this->getPromoPrefix());
                        $payableItem->save();
                        $freeItemIssued->delete();
                    } else {
                        $freeItemIssued->unit_discount_amount = .0;
                        $freeItemIssued->unsetCustomData($this->getPromoPrefix());
                        $freeItemIssued->save();
                    }

                }

            }

            $basket->refresh();
        }

        $items = $this->getEligibleItems($basket);

        foreach ($items->whereNotIn('item_id', $excludeIds) as $item) {

            $total    = $item->item_qty;
            $leastQty = $this->min_qty + $this->discount->discount_qty;

            if ($total < $leastQty) {
                $item->unit_discount_amount = .0;
                $item->unsetCustomData($this->getPromoPrefix());
                $item->save();

                continue;
            }

            $hashedStr = md5($total);
            $item->setCustomData($this->getPromoPrefix().'hash', $hashedStr);

            $freeQty = $this->discount->discount_qty;

            if (! $this->discount->apply_only_once) {
                $freeQty = (int)abs($this->getFreeQuantities($item->item_qty));
            }

            // Add a new line for free item
            $newFreeItem = $item->replicate([
                'parent_id', 'unit_price', 'item_qty', 'discount_total', 'custom_data',
            ]);
            $newFreeItem->setCustomData('lot_id', $item->getCustomData('lot_id'));
            $newFreeItem->setCustomData('salesperson_id', $item->getCustomData('salesperson_id'));
            $newFreeItem->setCustomData('price_edited', $item->getCustomData('price_edited'));
            $newFreeItem->setCustomData($this->getPromoPrefix().'is_free', true);
            $newFreeItem->setCustomData($this->getPromoPrefix().'hash', $hashedStr);

            /*
            // RRFID scan traces
            if ($item->hasCustomData('rfid')) {
                $newFreeItem->setCustomData('rfid', $item->getCustomData('rfid'));
            }
            if ($item->hasCustomData('rfidscanned')) {
                $newFreeItem->setCustomData('rfidscanned', $item->getCustomData('rfidscanned'));
            }
            if ($item->hasCustomData('rssi')) {
                $newFreeItem->setCustomData('rssi', $item->getCustomData('rssi'));
            }
            if ($item->hasCustomData('rssiscanned')) {
                $newFreeItem->setCustomData('rssiscanned', $item->getCustomData('rssiscanned'));
            }
            */

            $newFreeItem->item_qty             = $freeQty;
            $newFreeItem->unit_price           = $item->unit_price;
            $newFreeItem->unit_discount_amount = $item->unit_price;
            $newFreeItem->push();

            $item->item_qty -= $freeQty;

            $item->save();
        }

    }
}