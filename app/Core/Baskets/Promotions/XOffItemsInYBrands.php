<?php

namespace App\Core\Baskets\Promotions;


use App\Core\Baskets\Models\Basket;
use App\Core\Baskets\Models\BasketLevelDiscount;

class XOffItemsInYBrands extends DiscountRule
{
    public function handle(Basket $basket)
    {

        $this->items = $this->getEligibleItems($basket);

        $total = $this->items->sum(function ($line) {
            return $line->item_qty * $line->price_after_discount;
        });

        if ($total >= $this->discount->min_value) {
            foreach ($this->items as $item) {

                if ($item->is_free) {
                    continue;
                }

                $discountAmount = $this->calculateDiscountAmount($item);

                $item->unit_discount_amount += $discountAmount;
                $item->save();
            }
        } else {

            foreach ($this->items as $item) {
                // todo: avoid overlap promotions
                $item->save();
            }
        }

    }

}