<?php

namespace App\Core\Baskets\Promotions;


use App\Core\Baskets\Models\Basket;

class AmountOffY extends DiscountRule
{
    public function handle(Basket $basket)
    {
        $this->items = $this->getEligibleItems($basket);

        foreach ($this->items as $item) {

            if ($item->is_free) {
                continue;
            }

            $discountValue              = $this->calculateDiscountAmount($item, $item->item_qty);
            $item->unit_discount_amount += $discountValue;
            $item->save();
        }

    }
}