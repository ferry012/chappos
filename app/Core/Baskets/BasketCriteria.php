<?php

namespace App\Core\Baskets;

use App\Core\Baskets\Interfaces\BasketCriteriaInterface;
use App\Core\Baskets\Models\Basket;
use App\Core\Scaffold\AbstractCriteria;

class BasketCriteria extends AbstractCriteria implements BasketCriteriaInterface
{

    public function getBuilder()
    {
        $basket = new Basket;

        $builder = $basket->where('id', $basket->decodeId($this->id));

        return $builder;
    }
}
