<?php

namespace App\Core\Baskets;

use App\Core\Baskets\Models\Basket;
use App\Core\Contracts\Criteria\RequestCriteria;

class BasketRequestCriteria extends RequestCriteria
{

    public function getFieldsSearchable()
    {
        return ['cart_name', 'loc_id', 'pos_id', 'owner_id'];
    }

    public function getBuilder()
    {
        $basket = new Basket;
        $basket = $this->apply($basket);

        return $basket;
    }

}