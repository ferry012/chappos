<?php

namespace App\Core\Baskets;

class BasketItemBuilder
{
    protected $type;
    protected $order_num;
    protected $line;

    public function orderType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function orderReferenceNum($num)
    {
        $this->order_num = $num;

        return $this;
    }

    public function line($line)
    {
        $this->line = $line;

        return $this;
    }

    public function resolve()
    {

    }
}