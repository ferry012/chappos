<?php

namespace App\Core\Baskets\Services;

use App\Core\Baskets\Models\BasketLine;
use App\Core\Scaffold\BaseService;

class BasketLineService extends BaseService
{
    protected $model;

    public function __construct()
    {
        $this->model = new BasketLine;
    }

    public function destroy($id)
    {
        $id = $this->getDecodedId($id);
        $this->model->where('id', $id)->delete();
    }
}