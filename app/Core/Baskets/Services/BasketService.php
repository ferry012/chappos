<?php

namespace App\Core\Baskets\Services;

use App\Core\Baskets\Interfaces\BasketInterface;
use App\Core\Baskets\Interfaces\BasketServiceInterface;
use App\Core\Baskets\Models\Basket;
use App\Core\Baskets\Models\BasketLine;
use App\Core\Product\Models\Product;
use App\Core\Scaffold\BaseService;
use App\Support\Carbon;

class BasketService extends BaseService
{
    /**
     * @var Basket
     */
    protected $model;

    /**
     * The basket factory.
     *
     * @var BasketInterface
     */
    protected $factory;

    /**
     * The variant factory.
     *
     * @var BasketInterface
     */
    protected $variantFactory;

    public $insertedLine;

    public function __construct(
        BasketInterface $factory
    )
    {
        $this->model = new Basket();
        $this->factory = $factory;
    }

    /**
     * Gets either a new or existing basket for a user.
     *
     * @param mixed $id
     * @param string $userId
     *
     * @return Basket
     */
    public function getBasket($id = null, $userId = null)
    {
        $basket = new Basket();

        if ($id) {
            $basket = Basket::findOrFail($id);
        } elseif ($userId && $userBasket = $this->getCurrentForUser($userId)) {
            $basket = $userBasket;
        } else {
            $basket->cart_name = 'default';
        }

        $basket->save();

        return $basket;
    }

    /**
     * Get a basket by it's hashed ID.
     *
     * @param string $id
     *
     * @return BasketFactory
     */
    public function getByHashedId($id)
    {
        $BasketId = $this->model->decodeId($id);
        $basket = $this->model->findOrFail($BasketId);
        $basket->discounts;

        return $this->factory->init($basket)->get();
    }

    /**
     * Store a basket.
     *
     * @param array $data
     *
     * @return Basket
     */
    public function store(array $data, $userId = null)
    {
        $basket = $this->getBasket(
            !empty($data['basket_id']) ? $data['basket_id'] : null,
            $userId
        );

        //$basket->lines()->createMany($data);

        /*
        if (empty($data['currency'])) {
            $basket->currency = app('api')->currencies()->getDefaultRecord()->code;
        } else {
            $basket->currency = $data['currency'];
        }

        $basket->lines()->delete();

        if (! empty($data['variants'])) {
            $this->remapLines($basket, $data['variants']);
        }
        $basket->load([
            'lines',
            'lines.variant.product.routes',
            'lines.variant.image.transforms',
        ]);

        $discounts = Discount::all();

        $eligible = [];

        foreach ($discounts as $discount) {
            foreach ($discount->items as $item) {
                if ($item->check($basket->user, $basket)) {
                    $eligible[] = $discount->id;
                }
            }
        }

        $basket->discounts()->sync($eligible);
        */
        $basket = $this->factory->init($basket)->get();

        $basket->save();

        //event(new BasketStoredEvent($basket));

        return $basket;
    }

    public function add($basket, $lines, $insertedLines = false)
    {

        if (is_multi_array($lines)) {
            $lines = $basket->lines()->createMany($lines);
        } else {
            $lines = $basket->lines()->create($lines);
        }

        if ($insertedLines) {
            return $lines;
        }

        $basket = $this->factory->init($basket)->get();
        $basket->save();


        return $basket;
    }

    /**
     * Update a item in basket.
     *
     * @param mixed $basketId
     * @param string $id
     * @param array $data
     *
     * @return Basket
     */
    public function update($basketId, $id, array $data)
    {
        $basket = $this->getBasket($basketId);
        $basket->lines()->where('id', $id)->update($data);

        $basket = $this->factory->init($basket)->get();
        $basket->save();

        return $basket;
    }

    protected function mapBasketParentLine(Product $product)
    {
        return [
            'item_id'       => $product->item_id,
            'item_img'      => $product->item_img,
            'item_desc'     => $product->item_desc,
            'regular_price' => $product->regular_price,
            'unit_price'    => $product->getUnitPrice(),
            'item_qty'      => $product->item_qty,
        ];
    }

    protected function mapBasketChildrenLines(Product $childrenProducts, BasketLine $parentProduct)
    {
        $lines = [];

        foreach ($childrenProducts as $childProduct) {
            $lines[] = [
                'item_id'       => $childProduct->item_id,
                'item_img'      => $childProduct->item_img,
                'item_desc'     => $childProduct->item_desc,
                'parent_id'     => $parentProduct->id,
                'regular_price' => $childProduct->regular_price,
                'unit_price'    => $childProduct->getUnitPrice(),
                'item_qty'      => $childProduct->item_qty,
            ];

        }

        return $lines;
    }

    /**
     * Update a item in basket.
     *
     * @param mixed $basketId
     * @param array $data
     *
     * @return Basket
     */
    public function clearAndAddNew($basketId, array $data = [])
    {
        $basket = $this->getBasket($basketId);

        $basket->lines()->delete();

        $basket->lines()->createMany($data);

        $basket = $this->factory->init($basket)->get();

        $basket->save();

        return $basket;
    }

    /**
     * Set a user to a basket.
     *
     * @param Basket|string $basket
     * @param string $userId
     *
     * @return Basket
     */
    public function setUser($basket, $userId)
    {
        if (is_string($basket)) {
            $basket = $this->getBasket($basket);
        }

        $basket->owner_id = $userId;
        $basket->save();

        return $basket;
    }

    /**
     * Remove a item from basket.
     *
     * @param mixed $basketId
     * @param mixed $ids
     *
     * @return Basket
     */
    public function remove($basketId, $ids)
    {
        $basket = $this->getBasket($basketId);

        if ($basket === null) {
            return null;
        }

        if (!is_array($ids)) {
            $ids = (array)$ids;
        }

        $lines = $basket->lines()->whereIn('id', $ids)->get();

        if ($lines) {

            $parentIds = $lines->where('parent_id', '>', 0)->pluck('parent_id')->unique()->toArray();

            if (count($parentIds) > 0) {
                // Delete children lines
                //$basket->lines()->whereIn('parent_id', $parentIds)->delete();
                //$ids = array_merge($ids, $parentIds);
            }

        }

        if (count($ids) > 1) {
            $basket->lines()->whereIn('id', $ids)->delete();
            $basket->lines()->whereIn('parent_id', $ids)->delete();
        } else {
            $id = $ids[0];
            $basket->lines()->where('id', $id)->delete();
            $basket->lines()->where('parent_id', $id)->delete();
        }

        $basket = $this->factory->init($basket)->get();

        $basket->save();

        return $basket;
    }

    /**
     * Clear a basket. Delete lines only.
     *
     * @param mixed $basket
     *
     * @return Basket
     */
    public function clear($basket)
    {
        if (is_string($basket)) {
            $basket = $this->getBasket($basket);
        }

        // Delete any lines.
        $basket->lines()->delete();

        $basket = $this->factory->init($basket)->get();

        $basket->save();

        return $basket;
    }

    /**
     * Delete a basket.
     *
     * @param mixed $basket
     *
     * @return bool
     */
    public function destroy($basket)
    {
        $basket = $this->getBasket($basket);
        // Delete any lines.
        $basket->lines()->delete();

        return $basket->delete();
    }

    /**
     * Disconnects the current user/session from the current cart, but the cart will be kept intact in the database.
     *
     * @param mixed $basket
     *
     * @return bool
     */
    public function forget($basket)
    {
        if (is_string($basket)) {
            $basket = $this->getBasket($basket);
        }

        $basket->owner = null;

        return $basket->save();
    }

    /**
     * Saves a basket with a name.
     *
     * @param string $basketId
     * @param string $name
     *
     * @return Basket
     */
    public function save($basketId, $name = 'default')
    {

        // Get the original basket
        $basket = $this->getByHashedId($basketId);

        if ($basket && $basket->cart_name === $name) {
            return $this->factory->init($basket)->get();
        }

        $basket->cart_name = $name;

        // Clone the basket
        $clone = $this->factory->init($basket)->clone();

        return $this->factory->init($clone)->get();

    }

    /**
     * Get a basket for a user.
     *
     * @param string $userId
     * @param string $name
     *
     * @return mixed
     */
    public function getCurrentForUser($userId, $name = 'default')
    {
        if (empty($name)) {
            $name = 'default';
        }

        $basket = $this->model->where('owner_id', $userId)->where('cart_name', $name)->first();

        if ($basket) {
            return $this->factory->init($basket)->get();
        }

        return $this->factory->init(new Basket())->get();
    }

    /**
     * Resolves a guest basket with an existing basket.
     *
     * @param User $user
     * @param string $basketId
     * @param string $basketName
     * @param bool $merge
     *
     * @return Basket
     */
    public function resolve($userId, $basketName = 'default', $basketId, $merge = true)
    {
        // Guest basket
        $basket = $this->getByHashedId($basketId);

        // User basket
        $userBasket = $this->getCurrentForUser($userId, $basketName);

        if ($merge && $userBasket) {
            $basket = $this->merge($basket, $userBasket);
        }

        $basket->owner_id = $userId;
        $basket->save();

        $basket->load('lines');

        return $this->factory->init($basket)->get();
    }

    /**
     * Merges two baskets.
     *
     * @param Basket $guestBasket
     * @param Basket $userBasket
     *
     * @return Basket
     */
    public function merge($guestBasket, $userBasket)
    {
        $newLines = $guestBasket->lines;
        $overrides = $newLines->pluck('item_id');
        $oldLines = $userBasket->lines->filter(function ($line) use ($overrides) {
            if (!$overrides->contains($line->item_id)) {
                return $line;
            }
        });

        $userBasket->lines()->delete();
        $userBasket->lines()->createMany(
            $newLines->merge($oldLines)->toArray()
        );

        return $this->factory->init($userBasket)->get();
    }

    public function calculate(Basket $basket)
    {
        $basket->item_total = 0;
        // Get lines with active item
        $lines = $basket->lines;

        // Without tax, without discounts.
        $subTotal = .0;
        // Discount total, without tax.
        $discountTotal = .0;

        // Total is subtotal minus discount total
        // Tax is the amount from taking off the discount total from sub total.

        foreach ($lines as $line) {

            // POS need to trace for deleted items, other mainly for display error but still considered subtract the amount
            if ($line->item_type === 'D' || ($basket->cart_name === 'pos' && $line->status_level === -1)) {
                continue;
            }

            $subTotal += $line->price_total;

            if ($line->status_level !== -2) {
                $discountTotal += $line->discount_total;
            }
        }

        $basket->item_count = $lines->sum(function ($line) {

            if ($line->item_type === 'D') {
                return 0;
            } elseif ($line->status_level !== -1) {
                return abs($line->item_qty);
            }

            return 0;
        });

        $shippingTotal = 0;
        $basket->unsetCustomData(['is_free_shipping', 'shipping_total', 'shipping_fees']);

        if (!$basket->containsDigitalOnly()) {
            if ($basket->hasAnyStandardDeliveryItems()) {

                $entitledStdFreeShip = false;
                $memberType = $basket->getCustomData('member.type');

                // member type VIP enjoy free standard shipping
                if ($memberType === 'MVIP') {
                    $entitledStdFreeShip = true;
                } elseif (($subTotal - $discountTotal) > 88) {
                    $entitledStdFreeShip = true;
                }

                if (!$entitledStdFreeShip) {
                    $shippingTotal += 8;
                    $basket->setCustomData('is_free_shipping', false);
                    $basket->setCustomData('shipping_fees.standard', 8);
                } else {
                    $basket->setCustomData('is_free_shipping', true);
                    $basket->setCustomData('shipping_fees.standard', 0);
                }

            }

            if ($basket->hasAnyExpressDeliveryItems()) {
                $shippingTotal += 8;
                $basket->setCustomData('shipping_fees.express', 8);
            }

            if ($basket->hasAnyShippingItems()) {
                $basket->setCustomData('shipping_total', $shippingTotal);
            }
        }

        $totalAmountPayable = ($subTotal + $shippingTotal) - $discountTotal;
        $gstAmount = gst()->tax_percent;
        if($totalAmountPayable < 0){
            $gstAmount = app()->make(BasketServiceInterface::class)->getTaxPercent($basket);
        }

        $basket->item_total = $subTotal;
        $basket->discount_total = $discountTotal;
        $basket->tax_amount = ($totalAmountPayable * $gstAmount) / (100 + $gstAmount);
        $basket->total_amount = $totalAmountPayable;
        $basket->save();

        return $basket;
    }
}