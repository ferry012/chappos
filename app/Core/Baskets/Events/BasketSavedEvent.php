<?php

namespace App\Core\Baskets\Events;

use App\Core\Baskets\Models\Basket;

class BasketSavedEvent
{
    /**
     * Create a new event instance.
     *
     * @param $basket Basket
     *
     * @return void
     */
    public function __construct(Basket $basket)
    {
        $this->basket = $basket;
    }

    public function getBasket()
    {
        return $this->basket;
    }
}