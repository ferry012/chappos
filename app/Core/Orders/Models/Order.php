<?php

namespace App\Core\Orders\Models;

use App\Core\Baskets\Models\Basket;
use App\Core\Payments\Models\Transaction;
use App\Core\Traits\AppliesCriteria;
use App\Core\Scaffold\BaseModel;
use App\Support\Str;
use App\Traits\Paginatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use RtLopez\Decimal;
use Webpatser\Uuid\Uuid;

class Order extends BaseModel
{
    use Paginatable;
    use AppliesCriteria;

    const REBATE_RELIEF = 200;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pg';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'o2o_order_list';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'points_earned'      => 'float',
        'points_used'        => 'float',
        'item_total'         => 'float',
        'shipping_total'     => 'float',
        'discount_total'     => 'float',
        'rounding_adj'       => 'float',
        'taxable_amount'     => 'float',
        'tax_amount'         => 'float',
        'order_total'        => 'float',
        'deposit_amount'     => 'float',
        'rebate_amount'      => 'float',
        'voucher_adj'        => 'float',
        'total_paid'         => 'float',
        'total_due'          => 'float',
        'total_savings'      => 'float',
        'is_posted'          => 'boolean',
        'order_status_level' => 'int',
        'pay_status_level'   => 'int',
        'created_on'         => 'date:Y-m-d',
    ];

    protected $required = [
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->setCustomData('uuid', (string)Uuid::generate(4));
        });

        static::addGlobalScope('open', function (Builder $builder) {
            //$builder->whereNull('placed_on');
        });

    }

    /**
     * Define the type scope.
     *
     * @param Builder $builder
     * @param string $type
     *
     * @return Builder
     */
    public function scopeType(Builder $builder, $type)
    {
        if (!$type) {
            return $builder;
        }
        if ($type === 'Unknown') {
            $builder->whereNull('order_type');

            return $builder->whereNull('order_type');
        }

        return $builder->where('order_type', '=', $type);
    }

    public function scopeRange($qb, $from = null, $to = null)
    {
        if ($from) {
            $qb->whereDate('created_on', '>=', Carbon::parse($from));
        }
        if ($to) {
            $qb->whereDate('created_on', '<=', Carbon::parse($to));
        }

        return $qb;
    }

    public function scopeAmount($qb, $from = null, $to = null)
    {
        if ($from) {
            $qb->where('order_total', '>=', ($from));
        }
        if ($to) {
            $qb->where('order_total', '<=', ($to));
        }

        return $qb;
    }

    /**
     * Define the status scope.
     *
     * @param Builder $builder
     * @param string $status
     *
     * @return Builder
     */
    public function scopeStatus($builder, $status)
    {
        if (!$status) {
            return $builder;
        }

        return $builder->where('status_code', '=', $status);
    }

    public function scopePurchased($builder)
    {
        return $builder->where('order_status_level', '>', 0);
    }

    public function scopeToday(Builder $builder)
    {
        return $builder->whereRaw('cast(created_on as date) = ?', Carbon::now()->toDateString());
    }

    public function referenceOrder()
    {
        if ($this->ref_id) {
            return $this->where('order_num', $this->ref_id)->first();
        }

        return null;
    }

    public function lines()
    {
        return $this->hasMany(OrderLine::class);
    }

    public function discounts()
    {
        return $this->hasMany(OrderDiscount::class);
    }

    public function addresses()
    {
        return $this->hasMany(OrderAddress::class);
    }

    public function basket()
    {
        return $this->belongsTo(Basket::class, 'cart_id', 'id');
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function getRequiredAttribute()
    {
        return collect($this->required);
    }

    public function getRebateAmountAttribute($value)
    {
        return $value ?? .0;
    }

    public function getRebateReliefAmountAttribute()
    {
        $amount = $this->rebate_amount;

        // If amount more than $200, there's no tax rebate relief
        if ($amount > self::REBATE_RELIEF) {
            $amount = .0;
        }

        return $amount;
    }

    public function getCashierNameAttribute()
    {
        return $this->getCustomData('cashier_name', $this->cashier_id);
    }

    public function getBillingAddressAttribute()
    {
        return $this->addresses->where('address_type', 'BILLING')->first();
    }

    public function getShippingAddressAttribute()
    {
        return $this->addresses->where('address_type', 'SHIPPING')->first();
    }

    public function isRefundOrder()
    {
        return $this->order_type === 'RF';
    }

    public function isOneToOneExchangeOrder()
    {
        return $this->order_type === 'E1';
    }

    public function isDepositOrder()
    {
        return $this->order_type === 'DM';
    }

    public function isDepositUtiliseOrder()
    {
        return $this->order_type === 'DU' || $this->order_type === 'DZ';
    }

    public function isExchangeOrder()
    {
        return $this->order_type === 'EX';
    }

    public function isPendingOrder()
    {
        return $this->order_status_level === 0;
    }

    public function isRefundOrExchangeOrder()
    {
        return $this->isRefundOrder() || $this->isOneToOneExchangeOrder() || $this->isExchangeOrder();
    }

    public function isAwaitingOrder()
    {
        return $this->isPendingOrder();
    }

    public function isInvoiced()
    {
        return !empty($this->invoice_id);
    }

    public function isGuestPurchase()
    {
        return empty(trim($this->customer_id)) || Str::startsWith($this->customer_id, 'GT') || $this->getCustomData('is_guest_purchase', false);
    }

    public function isMemberPurchase()
    {
        return !$this->isGuestPurchase() && !$this->isStaffPurchase();
    }

    public function isStaffPurchase()
    {
        return !empty(trim($this->customer_id)) && Str::startsWith(trim($this->customer_id), 'STAFF_', false);
    }

    public function isPartialPaid()
    {
        $due = Decimal::create($this->total_due, 2);
        $paid = Decimal::create($this->total_paid, 2);

        return $due->gt(0) && $paid->gt(0) && $paid->lt($this->order_total + $this->rounding_adj);
    }

    public function isFullyPaid()
    {
        // Don't trust PHP floating point numbers when equating
        $value = Decimal::create($this->total_due, 2);

        return $value->le(0);
    }

    public function isOverPaid()
    {
        $due = Decimal::create($this->total_due, 2);

        return $due->lt(0);
    }

    public function isPaid()
    {
        return $this->isFullyPaid() || $this->isOverPaid();
    }

    public function isCashInvolved()
    {
        return $this->transactions()->charged()->where('pay_mode', '=', 'CASH')->count() > 0;
    }

    public function isVoucherInvolved()
    {
        $count = $this->transactions()->charged()->whereIn('pay_mode', [
            'CTLVCH',
            'MALLVCH',
            'ECREDIT',
            'MBRVCH',
        ])->count();

        if ($count > 0) {
            return true;
        }

        $itemIds = db()->table('v_ims_live_product')->whereIn('item_type', ['C', 'E'])->pluck('item_id')->toArray();

        if ($itemIds) {
            $count = $this->transactions()->charged()->whereIn('pay_mode', $itemIds)->count();

            if ($count > 0) {
                return true;
            }
        }

        return false;
    }

    public function isRefundAmountTally()
    {
        $total = Decimal::create($this->order_total, 2);

        return $total->eq($this->total_refunded);
    }

    public function hasShippingAddress()
    {
        return $this->addresses()->shipping()->exists();
    }

    public function hasAnyStandardDeliveryItems()
    {
        return $this->lines->where('item_type', 'I')->where('delv_method', 'STD')->count() > 0;
    }

    public function hasAnyExpressDeliveryItems()
    {
        return $this->lines->where('item_type', 'I')->where('delv_method', 'SDD')->count() > 0;
    }

    public function hasAnyShippingItems()
    {
        return $this->hasAnyStandardDeliveryItems() || $this->hasAnyExpressDeliveryItems();
    }

    public function containsDigitalOnly()
    {
        $isDigitalOnly = true;
        $lines = $this->lines->where('status_level', '>=', 0)->all();

        if ($lines) {
            foreach ($lines as $line) {
                if (!$line->isDigital()) {
                    $isDigitalOnly = false;
                    break;
                }
            }
        } else {
            $isDigitalOnly = false;
        }

        return $isDigitalOnly;
    }

    public function containsGiftCardOnly()
    {
        $totalLineCount = $this->lines->count();
        $giftCardCount = $this->lines->where('item_type', 'E')->count();

        return $totalLineCount === 1 && $giftCardCount === 1;
    }

    public function getCashPaid()
    {
        return $this->transactions()->charged()->where('pay_mode', '=', 'CASH')->sum('trans_amount');
    }

    public function getRoundingAdjustment()
    {
        return $this->transactions()->charged()->where('pay_mode', '=', 'CASH_ROUNDING_ADJ')->sum('trans_amount');
    }

    public function getPaidAmountExceptCash()
    {
        return $this->transactions()->charged()->where('pay_mode', '!=', 'CASH')->sum('trans_amount');
    }

    public function getOverPaidBalance()
    {
        if (!$this->isOverPaid()) {
            return 0;
        }

        return $this->total_due;
    }

    public function getChangeDue()
    {
        return $this->transactions()->where('pay_mode', 'CASH_CHANGE_DUE')->sum('trans_amount');
    }

    public function getDepositPaid()
    {
        if ($this->isDepositUtiliseOrder()) {
            return $this->transactions()->charged()->where('pay_mode', 'DEPOSIT')->sum('trans_amount');
        }

        return .0;
    }

    public function getTotalSavings()
    {
        return $this->lines->filter(function ($line) {
            return $line->status_level >= 0 && $line->item_type !== 'D' && $line->regular_price > 0;
        })->sum(function ($line) {

            $unitPrice = $line->unit_price;

            if ($this->isGuestPurchase()) {

                $mbrPrice = $line->getCustomData('prices.member');

                if ($mbrPrice === null) {
                    $product = $line->product;

                    if ($product) {
                        $mbrPrice = $product->mbr_price;
                    }
                }

                if ($mbrPrice > 0) {
                    $unitPrice = $mbrPrice;
                }
            } else {

                $mbrPromoPrice = $line->getCustomData('prices.member_promotion', 0);
                $publicPromoPrice = $line->getCustomData('prices.public_promotion', 0);

                // Exclude savings for channel promotion
                if ($mbrPromoPrice > 0 && $publicPromoPrice > 0 && Decimal::create($mbrPromoPrice)->eq($publicPromoPrice)) {
                    return .0;
                }

            }

            return ($line->regular_price - $unitPrice) * $line->qty_ordered;
        });
    }

    public function getVoucherPaid()
    {
        return $this->transactions()->charged()->whereIn('pay_mode', ['CTLVCH', 'MALLVCH'])->sum('trans_amount');
    }

    public function getVoucherAdjustmentAmount()
    {
        return $this->transactions()->charged()->where('pay_mode', 'VOUCHER_ADJ')->sum('trans_amount');
    }

    public function getRebateEarned()
    {

        if ($this->points_earned > 0) {
            return floor($this->points_earned) / 100;
        }

        return 0;
    }

    public function getRebatePaid()
    {
        return $this->transactions()->charged()->where('pay_mode', 'REBATE')->get()->sum('trans_amount');
    }

    public function getOrderTotal()
    {
        return $this->order_total;
    }

    public function getTotalPaidAmount()
    {
        return $this->transactions()->charged()->whereNotIn('pay_mode', [
            'CASH_CHANGE_DUE',
            'CASH_ROUNDING_ADJ',
        ])->get()->sum('trans_amount');
    }

    public function getTransactionTotal()
    {
        return $this->transactions()->charged()->get()->sum('trans_amount');
    }

    public function getHasShippingAttribute()
    {
        if ($this->containsGiftCardOnly()) {
            return false;
        }

        if ($this->containsDigitalOnly()) {
            return false;
        }

        if ($this->hasAnyShippingItems()) {
            return true;
        }

        return false;
    }

    public function updateAmount()
    {
        $this->fresh('transactions');

        $this->rounding_adj = $this->getRoundingAdjustment() * -1;
        $this->deposit_amount = $this->getDepositPaid();
        $this->rebate_amount = $this->getRebatePaid();

        if ($this->isRefundOrder()) {
            $this->total_refunded = $this->getTotalPaidAmount();
        } else {
            $this->total_paid = $this->getTotalPaidAmount();
        }

        $this->total_due = $this->order_total - $this->getTransactionTotal();

        $this->save();

        return $this;
    }

    public function UpdateTaxAmount()
    {
        $taxAmount = .0;

        $taxableAmount = $this->lines->whereNotIn('item_type', ['D', 'E', 'G', 'J', 'P', 'Q'])->sum(function ($line) {
            // Pre-owned item
            if ($line->item_type === 'O') {
                // Tax only the gross margin
                return ($line->final_price - $line->getCustomData('unit_cost', 0)) * $line->qty_ordered;
            }

            return $line->price_total;
        });

        // Tax calculation
        $taxableAmount -= $this->rebate_relief_amount ?? .0;
        $taxableAmount -= $this->discount_total ?? .0;
        $gstPlus = number_format((gst()->tax_percent / 100) + 1,2);
        $gst = number_format((gst()->tax_percent / 100),2);

        if (Decimal::create($taxableAmount)->ne(0)) {
            // Get the inclusive
            $taxAmount = ($taxableAmount * $gst) / $gstPlus;
        }

        $this->taxable_amount = $taxableAmount;
        $this->tax_amount = $taxAmount;

        $this->save();

        return $this;
    }

    public function updateTotalDue()
    {
        $this->total_due = $this->getOrderTotal() - $this->getTransactionTotal();
        $this->save();

        return $this;
    }

    public function hasMembershipCard()
    {
        return $this->lines->where('item_type', 'M')->count() > 0;
    }
}