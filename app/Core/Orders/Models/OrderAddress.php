<?php

namespace App\Core\Orders\Models;

use App\Core\Scaffold\BaseModel;
use App\Models\Country;

class OrderAddress extends BaseModel
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pg';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'o2o_order_address';

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function scopeBilling($query)
    {
        return $query->where('address_type', 'BILLING');
    }

    public function scopeShipping($query)
    {
        return $query->where('address_type', 'SHIPPING');
    }

    public function getCountryNameAttribute()
    {
        $country = $this->country;

        if ($country) {
            return $country->country_name;
        }

        return 'unknown';
    }

    public function countryCode()
    {
        return $this->country->iso;
    }

}