<?php

namespace App\Core\Orders\Models;

use App\Core\Product\Models\Product;
use App\Core\Scaffold\BaseModel;
use App\Core\Traits\Lines;
use App\Support\Carbon;
use App\Support\Str;
use Illuminate\Database\Eloquent\Builder;

class OrderLine extends BaseModel
{
    use Lines;

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pg';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'o2o_order_item';

    protected $fillable = [
        'line_num',
        'ref_line_num',
        'item_type',
        'item_id',
        'item_img',
        'item_desc',
        'parent_id',
        'regular_price',
        'unit_price',
        'qty_ordered',
        'qty_refunded',
        'unit_points_earned',
        'unit_discount_amount',
        'discount_total',
        'delv_method',
        'loc_id',
        'custom_data',
        'salesperson_id',
        'status_level',
        'modified_by',
        'created_by',
    ];

    protected $casts = [
        'parent_id'          => 'int',
        'regular_price'      => 'float',
        'unit_price'         => 'float',
        'qty_ordered'        => 'float',
        'unit_points_earned' => 'float',
        'discount_total'     => 'float',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'item_id', 'item_id');
    }

    public function scopeActive(Builder $builder)
    {
        return $builder->where('status_level', '<>', -1);
    }

    public function scopePurchased($builder)
    {
        return $builder->where('status_level', '>=', 0);
    }

    public function scopeToday(Builder $builder)
    {
        return $builder->whereRaw('cast(created_on as date) = ?', Carbon::now()->toDateString());
    }

    public function getDiscountsAttribute()
    {
        return $this->getCustomData('discounts', []);
    }

    public function getParentLineAttribute()
    {
        if ($this->parent_id) {
            return static::where('id', $this->parent_id)->first();
        }

        return null;
    }

    public function getParentIdAttribute($value)
    {
        return $value ?? 0;
    }

    public function getTotalPointsEarnedAttribute()
    {
        $points = $this->unit_points_earned * $this->qty_ordered;

        return floor($points);
    }

    public function getSalespersonIdAttribute()
    {
        $salesperson = $this->getCustomData('salesperson_id', []);

        if (isset($salesperson[0]) && ! empty($salesperson[0])) {
            return $salesperson[0];
        }

        return null;
    }

    public function getSecondSalespersonIdAttribute()
    {
        $salesperson = $this->getCustomData('salesperson_id', []);

        if (isset($salesperson[1]) && ! empty($salesperson[1])) {
            return $salesperson[1];
        }

        return null;
    }

    public function getPromoterIdAttribute()
    {
        return $this->getCustomData('promoter_id', null);
    }

    public function setItemDescAttribute($value)
    {
        $this->attributes['item_desc'] = Str::trimRight($value);
    }

    public function isDigital()
    {
        return in_array($this->item_type, [
                'A', 'C', 'D', 'E', 'K', 'L', 'M', 'P', 'R', 'S', 'T', 'V', 'W', 'U', 'G', 'H',
            ])
            || Str::containsAny($this->item_desc, ['gift card', 'Digital Download'], false);
    }

    public function isSecondSalespersonInvolved()
    {
        return $this->second_salesperson_id !== null;
    }

    public function getQuantity()
    {
        return $this->qty_ordered;
    }
}