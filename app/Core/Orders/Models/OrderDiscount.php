<?php

namespace App\Core\Orders\Models;

use App\Core\Scaffold\BaseModel;

class OrderDiscount extends BaseModel
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pg';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'o2o_order_discount';

    protected $fillable = [
        'order_id', 'coupon_id', 'coupon_type', 'coupon_name', 'coupon_code', 'coupon_amount', 'modified_on', 'created_on',
    ];

    public function order()
    {
        $this->belongsTo(Order::class);
    }
}