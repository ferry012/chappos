<?php

namespace App\Core\Orders\Factories;

use App\Core\Orders\Events\OrderProcessedEvent;
use App\Core\Orders\Events\OrderWasPaid;
use App\Core\Orders\Interfaces\OrderProcessingFactoryInterface;
use App\Core\Orders\Exceptions\OrderAlreadyProcessedException;
use App\Core\Orders\Models\Order;
use App\Core\Orders\Models\OrderLine;
use App\Core\Payments\PaymentManager;
use App\Core\Payments\PaymentResponse;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class OrderProcessingFactory implements OrderProcessingFactoryInterface
{
    /**
     * The order instance.
     *
     * @var Order
     */
    protected $order;

    /**
     * Additional payload fields.
     *
     * @var string
     */
    protected $payload = [];

    /**
     * The payment provider.
     *
     * @var PaymentType
     */
    protected $provider;

    /**
     * The payment token nonce.
     *
     * @var string
     */
    protected $nonce;

    /**
     * The payment manager instance.
     *
     * @var PaymentManager
     */
    protected $manager;

    /**
     * The order notes.
     *
     * @var string
     */
    protected $notes;

    /**
     * The order type.
     *
     * @var string
     */
    protected $type;

    /**
     * The customer reference.
     *
     * @var string
     */
    protected $customerReference;

    public function __construct(PaymentManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Set the value for nonce.
     *
     * @param string $nonce
     *
     * @return self
     */
    public function nonce($nonce)
    {
        $this->nonce = $nonce;

        return $this;
    }

    /**
     * Bulk set the value for payload.
     *
     * @param array $payload
     *
     * @return self
     */
    public function payload(array $payload)
    {
        $this->payload = $payload;

        return $this;
    }

    /**
     * Set the value for provider.
     *
     * @param $provider
     *
     * @return self
     */
    public function provider($provider = null)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Set a value to the payload.
     *
     * @param string $key
     * @param string $value
     *
     * @return self
     */
    public function set($key, $value)
    {
        $this->payload[$key] = $value;

        return $this;
    }

    /**
     * Set the value for notes.
     *
     * @param string $notes
     *
     * @return self
     */
    public function notes($notes = null)
    {
        $this->notes = $notes;

        return $this;
    }

    public function customerReference($ref = null)
    {
        $this->customerReference = $ref;

        return $this;
    }

    /**
     * Set the value of order.
     *
     * @param Order $order
     *
     * @return self
     */
    public function order(Order $order)
    {
        $this->order = $order;

        return $this;
    }

    public function resolve()
    {
        // order paid
        if ($this->order->placed_on) {
            throw new OrderAlreadyProcessedException(trans('exceptions.order_already_processed'));
        }

        $driver = $this->manager->with($this->provider);

        $this->order->setCustomData('is_processing', true);
        //$this->order->customer_remark = $this->notes;
        //$this->order->customer_reference = $this->customerReference;

        $this->order->save();

        $response = $driver
            ->token($this->nonce)
            ->order($this->order)
            ->fields($this->payload)
            ->charge();

        return $this->processResponse($response);
    }

    /**
     * Handle the response from the payment driver.
     *
     * @param PaymentResponse $response
     *
     * @return Order
     */
    protected function processResponse(PaymentResponse $response)
    {
        $this->order->updateAmount();

        if ($response->success) {

            $this->order = app('api')->orders()->calculate($this->order);

            // If basket attached, remove it
            if ($this->order->basket) {
                $this->order->basket->delete();
                $this->order->basket->lines()->delete();
                $this->order->basket->discounts()->delete();
            }

            $this->order->cart_id    = null;
            $this->order->due_on     = null;
            $this->order->invoice_id = $this->order->order_num;
            $this->order->pay_on     = Carbon::now();

            if ($this->order->isRefundOrder() && $this->order->isRefundAmountTally()) {
                $this->order->order_status_level = 1;
                $this->order->pay_status_level   = 2;

            } elseif ($this->order->isPaid()) {
                // fully paid
                $this->order->order_status_level = 1;
                $this->order->pay_status_level   = 2;
                $this->order->placed_on          = Carbon::now();
            }

            if ($this->order->ref_id && $refOrder = Order::where('order_num', $this->order->ref_id)->first()) {

                if ($this->order->isDepositUtiliseOrder()) {
                    // update the DM ref_id
                    $refOrder->ref_id      = $this->order->order_num;
                    $refOrder->modified_by = user_id();
                    $refOrder->save();
                } elseif ($this->order->isRefundOrExchangeOrder()) {

                    $this->order->lines->where('status_level', -2)->each(function ($line) use ($refOrder) {
                        $qty = abs($line->qty_ordered);
                        OrderLine::where('order_id', $refOrder->id)
                            ->whereRaw("qty_ordered - qty_refunded >= {$qty}")
                            ->where('line_num', $line->ref_line_num)
                            ->update([
                                'qty_refunded' => DB::raw("qty_refunded + {$qty}"),
                            ]);
                    });

                }

            }

            $this->order->save();

            if ($this->order->isPaid()) {

                /* TODO: Only Digital items post now, else queue them */

                // Do not post anything for ubi order
                if ((app()->environment('production') && $this->order->loc_id !== 'UBI') || app()->environment('local')) {
                    event(new OrderWasPaid($this->order));
                }

            }

        }

        event(new OrderProcessedEvent($this->order));

        return $this->order;
    }

    /**
     * Get the order instance.
     *
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Determines whether an order can be processed.
     *
     * @return bool
     */
    protected function canProcess($order)
    {
        $fields = $order->required->filter(function ($field) use ($order) {
            return $order->getAttribute($field);
        });

        return $fields->count() === $order->required->count();
    }

}