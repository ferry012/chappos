<?php

namespace App\Core\Orders\Factories;

use App\Core\Baskets\Interfaces\BasketServiceInterface;
use App\Core\Baskets\Models\Basket;
use App\Core\Baskets\Models\BasketLine;
use App\Core\Orders\Contracts\OrderFactory as OrderFactoryContract;
use App\Core\Orders\Exceptions\BasketHasPlacedOrderException;
use App\Core\Orders\Models\Order;
use App\Core\Orders\Models\OrderAddress;
use App\Core\Orders\Models\OrderDiscount;
use App\Models\AddressBook;
use App\Support\Str;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use RtLopez\Decimal;

class OrderFactory implements OrderFactoryContract
{
    /**
     * The basket model.
     *
     * @var Basket
     */
    protected $basket;

    /**
     * The order model instance.
     *
     * @var Order
     */
    protected $order;

    protected $quoteToOrder = false;

    protected $orderNumber = null;

    protected $isGuestPurchase = false;

    /**
     * The customer information
     */
    protected $customer;

    protected $reference_id;

    /**
     * The order type
     *
     * @var string
     */
    protected $type;

    public function __construct()
    {
    }

    /**
     * Set the value for basket.
     *
     * @param Basket $basket
     *
     * @return self
     */
    public function basket(Basket $basket)
    {
        $this->basket = $basket;

        return $this;
    }

    /**
     * Set the value of order.
     *
     * @param Order $order
     *
     * @return self
     */
    public function order(Order $order)
    {
        $this->order = $order;

        return $this;
    }

    public function setOrderNumber($orderNumber = null)
    {
        $this->orderNumber = Str::trim($orderNumber);

        return $this;
    }

    public function setIsGuestPurchase($isGuestPurchase = false)
    {
        $this->isGuestPurchase = $isGuestPurchase;

        return $this;
    }

    public function customer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Set the value for type
     *
     * @param string $type
     *
     * @return self
     */
    public function type($type)
    {
        $this->type = Str::upper($type);

        return $this;
    }

    /**
     * The order reference for refund/exchange/deposit utilize
     *
     * @param string $reference
     *
     * @return self
     */
    public function reference($reference)
    {
        $this->reference_id = Str::upper($reference);

        return $this;
    }

    public function resolve()
    {
//        dd($this->order,$this->basket);
        if (!$this->order) {
            $order = $this->getActiveOrder();
            $this->order = $order;
        } else {
            $order = $this->order;
        }

        if (!$this->basket) {
            $this->basket = $order->basket;
        }

        $gst_tax_id = app()->make(BasketServiceInterface::class)->checkBasketRefund($order);

        $order->item_total = 0;
        $order->shipping_total = 0;
        $order->tax_amount = (0.07 / 1.07) * 0;
        $order->tax_amount -= 0;
        $order->order_total = $order->item_amount + $order->shipping_total - $order->discount_total;
        $order->rounding_adj = 0;
        $order->total_due = $order->order_total - $order->rebate_amount;
        $order->total_savings = 0;
        $order->points_earned = 0;
        $order->tax_id = $gst_tax_id;

        $order->cashier_id = user_id();
        $order->setCustomData('cashier_name', user_data('usr_name', user_id()));
        $order->unsetCustomData('last_updated_on');

        if ($order->isGuestPurchase()) {
            //$order->customer_name = null;
            //$order->customer_email = null;
        }

        if ($order->isDepositOrder()) {
            $order->customer_name = $order->getCustomData('deposit.name');
            $order->customer_phone = $order->getCustomData('deposit.contact');
        }

        $order->created_by = user_id() ?? $order->customer_id;
        $order->modified_by = user_id() ?? $order->customer_id;

        $order->save();
        $order->lines()->delete();

        $this->resolveLines($order);

        if (in_array($order->order_type, ['HO', 'HQ'])) {
            $this->addBillAndShipAddresses($order);

            if ($order->hasAnyShippingItems()) {
                $this->addShippingLine($order);
            }

        }

        $this->resolveDiscounts($order);

        // update line num accordingly
        $this->refreshLineNumber($order);

        // Should set before add addresses
        if ($this->isGuestPurchase) {
            $order->customer_id = null;
        }

        return app('api')->orders()->calculate($order);
    }

    /**
     * Resolve the lines for our order.
     *
     * @param Order $order
     *
     * @return Order
     */
    public function resolveLines(Order $order)
    {

        $lineNum = 1;
        $basketLines = $this->basket->lines->sortBy('id');
        $rebateTier = 0;
        $canEarnRebate = true;
        $memberExpDate = Carbon::now()->subMinute()->toDateTimeString();
        $newMPT = setting('pos.enable_mpt_v2', false);

        if ($this->customer) {
            if (isset($this->customer->rebate_tier)) {
                $rebateTier = $this->customer->rebate_tier ?? 0;
            }

            if (isset($this->customer->mbr_type) && (Str::startsWith($this->customer->mbr_type, 'CORP') || in_array($this->customer->mbr_type, [
                        'M08',
                        'MST',
                        'NS'
                    ]))) {
                $canEarnRebate = false;
            }
           
            if (isset($this->customer->exp_date)) {
                $memberExpDate = $this->customer->exp_date;
            }
        }

        if ($order->order_type === 'HO' && $order->getCustomData('is_guest_purchase', false)) {
            $canEarnRebate = false;
        }

        // todo: think of a way to get the rebate tier
        // Currently, temporarily solution is to hard code the rebate tier for renewal
        $membership = $basketLines->filter(function ($item) {
            return Str::startsWith($item->item_id, '!MEMBER-');
        })->first();

        if ($membership) {
            $memberType = app('api')->members()->getMemberTypeByItemId($membership->item_id);
            $rebateTier = app('api')->members()->getRebateTierByItemId($membership->item_id);

            $order->setCustomData('member.type', $memberType);
            $order->setCustomData('member.rebate_tier', $rebateTier);
            $canEarnRebate = true;
        }

        if ($order->isDepositOrder()) {
            $canEarnRebate = false;
        }

        if (!$canEarnRebate) {
            $rebateTier = 0;
        }

        $this->rebateTier = $rebateTier;

        foreach ($basketLines as $line) {

            if ($this->quoteToOrder) {

                // Skip gift card item
                if (Str::start($line->item_id, '!EGIFT')) {
                    continue;
                }

            }

            $lineId = $line->id;
            $product = $line->product;
            $itemRebateTier = $rebateTier;
            $points = 0;
            $pointsLimit = 0;
            $extraPointsPerUnit = 0;

            $orderLineData = $this->mapOrderLine($line);
            $orderLineData['line_num'] = $lineNum++;

            if ($orderLineData['item_type'] === 'D' && $orderLineData['status_level'] >= 0) {

                if (array_has($orderLineData['custom_data'], 'unit_points_earned')) {
                    $points = array_get($orderLineData['custom_data'], 'unit_points_earned');
                    $orderLineData['unit_points_earned'] = (float)$points;
                }

                $order->lines()->create($orderLineData);

                continue;
            }

            if ($product && $orderLineData['status_level'] >= 0) {

                $points = $product->getEarnPoints($line->discounted_price);

                // Get MPT
                $builder = db()->table('v_ims_live_promo')->where('promo_type', 'MPT')
                    ->where(function ($q) {
                        $q->whereDate('eff_from', '<=', today()->toDateString())->whereTime('time_from', '<=', today()->toTimeString());
                    })
                    ->where(function ($q) {
                        $q->whereDate('eff_to', '>=', today()->toDateString())->whereTime('time_to', '>=', today()->toTimeString());
                    })
                    ->whereIn('loc_id', ['**', $order->loc_id])// ->where('loc_id', $order->loc_id)
                    ->where('ref_id', $product->item_id);

                // only allow member have a basic rebate tier to earn more
                if ($itemRebateTier > 0) {

                    if (!$newMPT && ($product_mpt = $builder->orderBy('multiple_points')->first())) {
                        $orderLineData['custom_data']['mpt_promo_id'] = $product_mpt->promo_id;

                        if ($product_mpt->promo_grp_id === 'FIXED') {
                            $itemRebateTier = $product_mpt->multiple_points; // Remove tier calculation
                        } else {
                            $itemRebateTier *= $product_mpt->multiple_points;
                        }
                    } elseif ($line->hasCustomData('rewards.points')) {

                        if (!$canEarnRebate) {
                            Arr::forget($orderLineData, 'custom_data.rewards.points');
                        } else {
                            // TODO: Need to review
                            $rewardPoints = $line->getCustomData('rewards.points');

                            if (isset($rewardPoints['type'], $rewardPoints['value'])) {
                                if ($rewardPoints['type'] === 'fixed') {
                                    $itemRebateTier = $rewardPoints['value'];
                                } elseif ($rewardPoints['type'] === 'percentage') {
                                    $points *= $rewardPoints['value'];

                                    if (isset($rewardPoints['max_discount']) && $rewardPoints['max_discount'] > 0) {
                                        $pointsLimit = $rewardPoints['max_discount'] * 100;
                                    }

                                }
                            }
                        }

                    }

                }

                if ($order->isMemberPurchase()) {
                    //$points *= $itemRebateTier;
                    $points = $line->getUnitRebatePoints($itemRebateTier);
                    $points += $extraPointsPerUnit;

                    if ($pointsLimit > 0 && $points > $pointsLimit) {
                        $points = $pointsLimit;
                    }
                }

                // No rebate given
                if (!$canEarnRebate || Str::startsWith($order->loc_id, 'TRS-')) {
                    $points = .0;
                }

                $orderLineData['unit_points_earned'] = $points;

                if ($line->parent_id === null) {
                    if ($line->getCustomData('promo_id','') != '')
                        $orderLineData['custom_data']['promo_id'] = $line->getCustomData('promo_id');
                    else
                        $orderLineData['custom_data']['promo_id'] = $product->promo_id;
                }

                if (array_get($product->limit_options, 'pos_printable', true) === false) {
                    $orderLineData['custom_data']['pos_printable'] = false;
                }

                if ($product->item_type === 'O') {
                    $orderLineData['custom_data']['is_preowned_item'] = true;
                    $orderLineData['custom_data']['unit_cost'] = $product->unit_cost ?? 0;
                }

                // Store dimensions for after-sales processing/checking
                $orderLineData['custom_data']['dimensions'] = [
                    'inv_dim1' => $product->inv_dim1,
                    'inv_dim2' => $product->inv_dim2,
                    'inv_dim3' => $product->inv_dim3,
                    'inv_dim4' => $product->inv_dim4,
                    'brand_id' => $product->brand_id,
                    'model_id' => $product->model_id,
                ];

            }

            if ($orderRefLine = $line->getCustomData('order_line')) {
                $orderLineData['ref_line_num'] = $orderRefLine['line_num'];
                $orderLineData['unit_points_earned'] = $orderRefLine['unit_points_earned'];
            }

            $tags = [
                'is_demo'        => 'demo',
                'is_staff_price' => 'staff price',
            ];

            $labels = [];

            foreach ($tags as $key => $label) {
                // Is a demo set prepend a tag
                if (array_get($orderLineData, 'custom_data.'.$key, false)) {
                    $labels[] = $label;
                }
            }

            if ($labels) {
                $orderLineData['item_desc'] = '['.implode(', ', $labels).'] '.$orderLineData['item_desc'];
            }

            $orderLineData['created_by'] = user_id() ?? ($this->customer->id ?? null);
            $orderLineData['modified_by'] = user_id() ?? ($this->customer->id ?? null);

            $orderLine = $order->lines()->create($orderLineData);

            if ($lineId > 0 && $basketLines->where('parent_id', $lineId)->count()) {
                $basketLines->map(function ($line) use ($lineId, $orderLine) {
                    if ($line->parent_id === $lineId) {
                        $line->parent_id = $orderLine->id;
                    }
                });
            }

        }

        return $order;
    }

    public function refreshLineNumber($order)
    {
        $lines = $order->lines->sortBy('id');

        $lineNum = 1;

        foreach ($lines as $line) {
            $line->line_num = $lineNum++;
            $line->save();
        }
    }

    public function mapOrderLine(BasketLine $line)
    {
        $regPrice = $line->regular_price;

        // dynamic gift card value need to update regular price
        if ($line->item_type === 'G' && Decimal::create($line->regular_price)->eq(0)) {
            $regPrice = $line->unit_price;
        }

        return [
            'parent_id'            => $line->parent_id,
            'item_type'            => $line->item_type,
            'item_id'              => $line->item_id,
            'item_desc'            => $line->item_desc,
            'regular_price'        => $regPrice,
            'unit_price'           => $line->unit_price,
            'qty_ordered'          => $line->item_qty,
            'unit_discount_amount' => $line->unit_discount_amount,
            'discount_total'       => $line->discount_total,
            'delv_method'          => $line->delv_method,
            'loc_id'               => $line->loc_id,
            'custom_data'          => $line->custom_data,
            'status_level'         => $line->status_level,
            'modified_by'          => user_id(),
            'created_by'           => user_id(),
        ];
    }

    public function addBillAndShipAddresses($order)
    {

        // Billing
        $address = AddressBook::billing()->default()->where('customer_id', $order->customer_id)->first();

        $address = $this->mapOrderAddress($address);

        $address->phone_num = $order->customer_phone;

        $address = $order->addresses()->save($address);

        $order->billing_address_id = $address->id;

        if (!$order->lines()->whereIn('delv_method', ['STD', 'SDD'])->exists()) {
            return;
        }

        // Shipping
        $address = AddressBook::shipping()->default()->where('customer_id', $order->customer_id)->first();

        if ($address) {
            $address = $this->mapOrderAddress($address);

            $address = $order->addresses()->save($address);

            $order->shipping_address_id = $address->id;
        }

    }

    public function mapOrderAddress(AddressBook $addressBook)
    {
        $type = $addressBook->address_type;

        $address = new OrderAddress;
        $address->address_type = $type;
        $address->recipient_name = $addressBook->recipient_name;

        if ($this->order->order_type === 'HO') {
            $address->first_name = $addressBook->first_name;
            $address->last_name = $addressBook->last_name;
        }

        $address->company_name = $addressBook->company_name;
        $address->address_line_1 = $addressBook->address_line_1;
        $address->address_line_2 = $addressBook->address_line_2;
        $address->address_line_3 = $addressBook->address_line_3;
        $address->address_line_4 = $addressBook->address_line_4;
        $address->country_id = $addressBook->country_id;
        $address->state_name = $addressBook->state_name;
        $address->city_name = $addressBook->city_name;
        $address->postal_code = $addressBook->postal_code;
        $address->phone_num = $addressBook->phone_num;
        $address->full_address = $addressBook->full_address;

        return $address;
    }

    public function addShippingLine($order)
    {
        $standardFee = $order->getCustomData('shipping_fees.standard', 0);
        $expressFee = $order->getCustomData('shipping_fees.express', 0);

        $lineNum = $order->lines()->max('line_num');

        if ($standardFee > 0) {
            $lineNum++;

            $lines[] = [
                'line_num'      => $lineNum,
                'item_type'     => 'S',
                'item_id'       => '#DELV_CHG',
                'item_desc'     => 'Delivery Charges',
                'regular_price' => $standardFee,
                'unit_price'    => $standardFee,
                'qty_ordered'   => 1,
                'created_by'    => 'sys',
            ];
        }

        if ($expressFee > 0) {
            $lineNum++;

            $lines[] = [
                'line_num'      => $lineNum,
                'item_type'     => 'S',
                'item_id'       => '#DELV_EXPRESS',
                'item_desc'     => 'Express Delivery Charges',
                'regular_price' => $expressFee,
                'unit_price'    => $expressFee,
                'qty_ordered'   => 1,
                'created_by'    => 'sys',
            ];
        }

        if (isset($lines)) {
            $order->lines()->createMany($lines);
        }

        return $order;
    }

    public function resolveDiscounts(Order $order)
    {
        $order->discounts()->delete();

        // Remove coupon due to applied item been voided
        $discounts = $this->basket->discounts->reject(function ($obj) {
            return $obj->coupon_amount === null;
        });

        foreach ($discounts as $discount) {
            $coupon = new OrderDiscount([
                'order_id'      => $order->id,
                'coupon_id'     => $discount->coupon_id,
                'coupon_type'   => $discount->coupon_type,
                'coupon_name'   => $discount->coupon_name,
                'coupon_code'   => $discount->coupon_code,
                'coupon_amount' => $discount->coupon_amount,
            ]);

            $coupon->save();
        }
    }

    /**
     * Get the active order.
     *
     * @return Order
     */
    protected function getActiveOrder()
    {
        if ($this->basket->activeOrder) {
            $order = $this->basket->activeOrder;
            $order->customer_id = $this->basket->owner_id;
            $order->order_type = $this->type;
            $order->ref_id = $this->reference_id;
            $order->custom_data = $this->basket->custom_data;

            if ($order->ref_id && $order->ref_id === $order->order_num) {
                $order->ref_id = null;
            }

            $this->getCustomerInfo($order);

            if (in_array($this->type, ['DU', 'RF', 'E1', 'EX'])) {
                $order->due_on = Carbon::now()->addMinutes(15);
            }

            $order->save();

            return $order;
        }

        if ($this->basket->placedOrder) {
            throw new BasketHasPlacedOrderException;
        }

        return $this->createNewOrder();
    }

    protected function createNewOrder()
    {
        $order = new Order();
        $order->coy_id = $this->basket->coy_id;
        $order->cart_id = $this->basket->id;
        $order->loc_id = user_data('loc_id');
        $order->pos_id = request('pos_id') ?? user_data('pos_id');

        if ($this->basket->cart_name !== 'pos') {
            $order->loc_id = $this->basket->loc_id;
            $order->pos_id = null;
        }

        $order->customer_id = $this->basket->owner_id;
        $order->order_type = $this->type;
        $order->ref_id = $this->reference_id;
        $order->order_num = $this->getOrderNumber($order);
        $order->custom_data = $this->basket->custom_data;
        $order->order_status_level = 0;
        $order->modified_by = user_id();
        $order->created_by = user_id();

        $this->getCustomerInfo($order);

        if (in_array($this->type, ['DU', 'RF', 'E1', 'EX'])) {
            // Challenger point-of-sales order
            $order->due_on = Carbon::now()->addMinutes(15);
        } else if ($this->type === 'HO') {
            // Hachi sales order
            $order->due_on = Carbon::now()->addMinutes(15);
        } else if ($this->type === 'HQ') {
            // Hachi quote-to-order
            $order->due_on = Carbon::now()->endOfDay()->addDays(7);
        }

        return $order;
    }

    protected function getCustomerInfo($order)
    {
        if ($this->isGuestPurchase) {
            $guest = $this->basket->getCustomData('forms.guest_details');

            if ($guest) {
                $order->customer_name = !empty($guest['last_name']) ? $guest['first_name'].' '.$guest['last_name'] : $guest['first_name'];
                $order->customer_name = Str::upper($order->customer_name);
                $order->customer_email = $guest['email'];
                $order->customer_phone = $guest['contact_num'];
            }

        } else if ($this->customer) {

            $order->customer_name = !empty($this->customer->last_name) ? $this->customer->first_name.' '.$this->customer->last_name : $this->customer->first_name;
            $order->customer_name = Str::upper($order->customer_name);
            $order->customer_email = $this->customer->email_addr;
            $order->customer_phone = $this->customer->contact_num;

            $order->setCustomData('member.type', $this->customer->mbr_type);
            $order->setCustomData('member.points_available', $this->customer->points_available);
            $order->setCustomData('member.exp_date', Str::upper(Carbon::createFromTimeString($this->customer->exp_date)->format('d M Y')));
        }
    }

    public function getOrderNumber(Order $order)
    {

        if (!empty($this->orderNumber)) {
            return $this->orderNumber;
        }

        return $this->orderNumber = $this->getNextOrderNumber($order);
    }

    /**
     * Get the next order reference.
     *
     * @param Order $order
     *
     * @return string
     */
    public function getNextOrderNumber(Order $order)
    {
        // Need to replace
        if (in_array($order->order_type, ['PS', 'DM', 'DU', 'EX', 'RF', 'E1', 'ID', 'GS', 'IS', 'DI', 'DZ'])) {
            $posId = $order->pos_id ?? '00';
            $year = Carbon::now()->format('y');
            $diff = (int)$year - 18;
            // Warning!!! get from A - Z only, last for 26 years then worry about it
            $prefix = ($order->loc_id === 'JL') ? $posId.$year : $posId.chr(65 + $diff); // Only JL uses "9919" prefix

            return app('generator')->with('seq_alphanum')->name("RETAIL.POS_{$posId}_{$year}_SALES_ORDER")->prefix($prefix)->start('00001')->length(5)->generate();
        }

        if ($order->order_type === 'HO') {

            $year = Carbon::now()->format('y');
            $month = Carbon::now()->format('m');
            $prefix = 'HO'.$year.$month;

            return app('generator')->with('seq_alphanum')->name("HACHI.{$year}_{$month}_SALES_ORDER")->prefix($prefix)->start('000001')->length(6)->generate();
        }

        return app('generator')->with('seq_alphanum')->name('SALES_ORDER_CODE')->prefix('SI')->generate();
    }

}