<?php

namespace App\Core\Orders;

use App\Core\Orders\Interfaces\OrderCriteriaInterface;
use App\Core\Orders\Models\Order;
use App\Core\Scaffold\AbstractCriteria;

class OrderCriteria extends AbstractCriteria implements OrderCriteriaInterface
{
    protected $includes = [];

    /**
     * The scopes to take off.
     *
     * @var array
     */
    protected $without_scopes = ['open'];

    public function getBuilder()
    {
        $order   = new Order;
        $builder = $order->with($this->includes ?: []);

        foreach ($this->without_scopes as $scope) {
            $builder = $builder->withoutGlobalScope($scope);
        }

        if ($this->id) {
            $order = $order->where('id', '=', $order->decodeId($this->id));
        }

        return $order;
    }
}