<?php

namespace App\Core\Orders\Events;

use App\Core\Orders\Models\Order;

class OrderProcessedEvent
{
    protected $order;

    /**
     * Create a new event instance.
     *
     * @param  Order  $order
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }
}