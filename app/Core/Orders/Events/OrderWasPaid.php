<?php

namespace App\Core\Orders\Events;

use App\Core\Orders\Models\Order;

class OrderWasPaid
{
    public $order;

    /**
     * Create a new event instance.
     *
     * @param  Order $order
     *
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function getOrder()
    {
        return $this->order;
    }
}