<?php

namespace App\Core\Orders\Events;

use App\Core\Baskets\Models\Basket;
use App\Core\Orders\Models\Order;

class OrderSavedEvent
{
    public $order;

    public $basket;

    /**
     * Create a new event instance.
     *
     * @param  Order $order
     * @param Basket $basket
     *
     */
    public function __construct(Order $order, Basket $basket = null)
    {
        $this->order  = $order;
        $this->basket = $basket ?: $order->basket;
    }
}