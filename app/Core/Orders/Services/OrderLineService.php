<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 26/3/2019
 * Time: 6:13 PM
 */

namespace App\Core\Orders\Services;


use App\Core\Orders\Models\Order;
use App\Core\Orders\Models\OrderLine;
use App\Core\Pricing\PriceCalculatorInterface;
use App\Core\Scaffold\BaseService;

class OrderLineService extends BaseService
{
    protected $model;

    protected $orders;

    protected $variants;

    protected $calculator;

    public function __construct(
        OrderService $orders,
        PriceCalculatorInterface $calculator
    )
    {
        $this->model      = new OrderLine;
        $this->orders     = $orders;
        $this->calculator = $calculator;
    }

    /**
     * Delete an order line.
     *
     * @param string $lineId
     *
     * @return Order
     */
    public function delete($lineId)
    {
        $line   = $this->model->find($lineId);

        $line->delete();

        return $line->order;
    }
}