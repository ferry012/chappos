<?php


namespace App\Core\Orders\Services;


use Illuminate\Support\Arr;

class CreateRecurringSubscriptionInvoice
{
    public function create($invoiceId)
    {

        $newInvoiceId = '';
        $now = now();
        $createdBy = 'sys';
        $invoiceList = db('pg_cherps')->table('sms_invoice_list')->where('coy_id', 'CTL')->where('invoice_id', $invoiceId)->first();
        $invoiceItemList = db('pg_cherps')->table('sms_invoice_item')->where('coy_id', 'CTL')->where('invoice_id', $invoiceId)->get();
        $paymentList = db('pg_cherps')->table('sms_payment_list')->where('coy_id', 'CTL')->where('invoice_id', $invoiceId)->get();
        $addressList = db('pg_cherps')->table('coy_address_book')->where('coy_id', 'CTL')->where('ref_id', $invoiceId)->get();

        if (!isset($invoiceList, $invoiceItemList, $paymentList, $addressList)) {
            return false;
        }

        $newInvoiceId = ctl_next_invoice_number();

        if ($invoiceList) {

            $invoiceList = (array)$invoiceList;
            Arr::forget($invoiceList, ['cust_po_code', 'delv_date', 'approve_date']);

            $invoiceList['invoice_id'] = $newInvoiceId;
            $invoiceList['invoice_date'] = $now;
            $invoiceList['status_level'] = 1;
            $invoiceList['cust_po_code'] = '';
            $invoiceList['delv_date'] = $now;
            $invoiceList['approve_date'] = $now;
            $invoiceList['created_by'] = $createdBy;
            $invoiceList['created_on'] = $now;
            $invoiceList['modified_by'] = $createdBy;
            $invoiceList['modified_on'] = $now;
        }

        if ($invoiceItemList) {
            $invoiceItemList = $invoiceItemList->map(function ($item) use ($newInvoiceId, $now, $createdBy) {
                $item->invoice_id = $newInvoiceId;
                $item->ref_id = '';
                $item->delv_code = '';
                $item->code_send = '';
                $item->created_by = $createdBy;
                $item->created_on = $now;
                $item->modified_by = $createdBy;
                $item->modified_on = $now;

                $item = (array)$item;
                unset($item['code_send'], $item['delv_date'], $item['posted_on'], $item['pick_date']);
                return $item;
            })->toArray();
        }

        if ($paymentList) {
            $paymentList = $paymentList->map(function ($item) use ($newInvoiceId, $now, $createdBy) {
                $item->invoice_id = $newInvoiceId;
                $item->created_by = $createdBy;
                $item->created_on = $now;
                $item->modified_by = $createdBy;
                $item->modified_on = $now;

                $item = (array)$item;
                unset($item['trans_ref1'], $item['trans_ref3']);

                return $item;
            })->toArray();
        }

        if ($addressList) {
            $addressList = $addressList->map(function ($item) use ($newInvoiceId, $now, $createdBy) {
                $item->ref_id = $newInvoiceId;
                $item->created_by = $createdBy;
                $item->created_on = $now;
                $item->modified_by = $createdBy;
                $item->modified_on = $now;

                $item = (array)$item;
                unset($item['trans_ref1'], $item['trans_ref3']);
                return $item;
            })->toArray();
        }

        db('pg_cherps')->beginTransaction();

        try {

            db('pg_cherps')->table('sms_invoice_list')->insert($invoiceList);
            db('pg_cherps')->table('sms_invoice_item')->insert($invoiceItemList);
            db('pg_cherps')->table('sms_payment_list')->insert($paymentList);
            db('pg_cherps')->table('coy_address_book')->insert($addressList);
            db('pg_cherps')->commit();

            return true;
        } catch (\Exception $e) {
            db('pg_cherps')->rollback();

            return false;
        }
    }
}