<?php

namespace App\Core\Orders\Services;

use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Output\ConsoleOutput;
use App\Support\Str;
use Carbon\Carbon;

class CustomCommonOrder
{

    public function getInsertVoucher_MREP10OFF50AG($order,$line){
        $output = new ConsoleOutput();
        //$output->writeln("New Member, voucher insert".($line->item_id));

        //--- Disable code below, we will be using a batch job
        // if ($line->item_id == '!MEMBER-UPG28' ||  $line->item_id == '!MEMBER-NEW28' || $line->item_id == '!MEMBER-REN28' || $line->item_id == '!MEMBER-RR28') {
        //     $expiryDate = Carbon::now()->addMonths(28); 
        //     $couponId = "!MREP-10OFF50AG";
        //     $records = [];
        //     $mbrId = $order->customer_id;
        //     for ($x=0; $x<3;$x++) {
        //         $output->writeln((string)$x);
        //         $ts = Carbon::now()->timestamp;
        //         array_push($records, [
        //             "coy_id"=>"CTL",
        //             "coupon_id"=>$couponId ,
        //             "coupon_serialno"=>"$couponId-$ts-$x",
        //             "promocart_id"=>$couponId,
        //             "mbr_id"=> $mbrId,
        //             "trans_date"=>$order->created_on,
        //             "ho_date"=>$order->created_on,
        //             "issue_date"=>$order->created_on,
        //             "sale_date"=>$order->created_on,
        //             "expiry_date"=>$expiryDate,
        //             "receipt_id1"=>$order->order_num,
        //             "voucher_amount"=>10,
        //             "issue_date"=>$order->created_on,
        //             "status_level"=>0,
        //             "created_by"=>$mbrId,
        //             "modified_by"=>$mbrId,
        //             "issue_type" => "PAY_VCH"
        //         ]);
        //     }
        //     $strcnt = (string)count($records);
        //     $output->writeln("New Member, voucher insert done $strcnt");
        //     return $records;
        // }
        //--- eof Disable code below, we will be using a batch job
        
        return [];
    }
    public function custom_validate(){
        $output = new ConsoleOutput();
        $action = $_GET['action'];
        $type = $_GET['type'];
        $mbrId = $_GET['mbrId'];
        $sn = $_GET['sn'];
        if ($type=="MREP10OFF50AG"){
            $code = 0;
            $msg = "Invalid request";
            $sql = "SELECT utilize_date FROM crm_voucher_list where mbr_id = ? and coupon_id = '!MREP-10OFF50AG' order by utilize_date desc limit 1";
            $row = DB::connection('vc')->select(DB::raw($sql),[$mbrId]);
            if (count($row)>0){
                $utilize_date = substr($row[0]->utilize_date,0,10);
                $now = Carbon::now()->format('Y-m-d');
                if ($utilize_date==$now){
                    $code = 0;
                    $msg = "Cannot utilize voucher on thesame day ref->!MREP-10OFF50AG->SN#:$sn";
                }else{
                    $code = 1;
                    $msg = "Success";
                }
                $valid = $utilize_date!==$now; // if same day not valid
            }
            $response['code'] = $code;
            $response['msg'] = $msg;
            echo json_encode($response);
            return;
        }
        $response['code'] = 0;
        $response['msg'] = "Invalid request -> $action-$type";
        echo json_encode($response);
    }
   
}

// Notes
// $output = new ConsoleOutput();
// $sql = "SELECT * FROM crm_voucher_list limit 1";
// $result = DB::connection('vc')->select(DB::raw($sql));
// $arr = [];
// foreach($result as $row)
// {
//     $arr[] = (array) $row;
// }
// $output->writeln("insertVoucher->".$arr[0]['coupon_id']);
