<?php

namespace App\Core\Orders\Services;

use App\Core\Orders\Models\Order;
use App\Core\Orders\Models\OrderLine;
use App\Support\Str;
use App\Utils\Helper;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use RtLopez\Decimal;
use App\Core\Orders\Services\CustomCommonOrder;
use Symfony\Component\Console\Output\ConsoleOutput;

class CreatePointOfSaleInvoice
{
    public function create(Order $order)
    {
//        dd('CREATE',$order);
        $orderLines                      = [];
        $lotLines                        = [];
        $secondSalespersonLines          = [];
        $orderPaymentLines               = [];
        $membershipRenewalByRebateAmount = .0;
        $invoiceId                       = $order->getCustomData('order_ref_invoice_id');
        $customCommonOrder = (new CustomCommonOrder);
//        dd('CREATE',$invoiceId,$customCommonOrder,app()->environment('local'));
        // If running from localhost, delete from CHERPS to re-process
        if (app()->environment('local')) {
            db('pg_cherps')->table('pos_transaction_list')->where('coy_id', 'CTL')->where('trans_id', $order->order_num)->delete();
            db('pg_cherps')->table('pos_transaction_item')->where('coy_id', 'CTL')->where('trans_id', $order->order_num)->delete();
            db('pg_cherps')->table('pos_trans_salesperson')->where('coy_id', 'CTL')->where('trans_id', $order->order_num)->delete();
            db('pg_cherps')->table('pos_transaction_lot')->where('coy_id', 'CTL')->where('trans_id', $order->order_num)->delete();
            db('pg_cherps')->table('pos_payment_list')->where('coy_id', 'CTL')->where('trans_id', $order->order_num)->delete();
        }

        if (db('pg_cherps')->table('pos_transaction_list')->where('coy_id', 'CTL')->where('trans_id', $order->order_num)->exists()) {
            return true;
        }

        $orderHead = [
            'coy_id'             => $order->coy_id,
            'trans_id'           => $order->order_num,
            'trans_type'         => $order->order_type,
            'trans_date'         => $order->pay_on ?? $order->created_on,
            'status_level'       => $order->order_status_level,
            'mbr_id'             => $order->customer_id ?? '',
            'mbr_savings'        => $order->total_savings,
            'invoice_id'         => '',
            'ref_id'             => Str::substr($order->ref_id, 0, 15) ?? '',
            'loc_id'             => $order->loc_id ?? '',
            'pos_id'             => $order->pos_id,
            'cashier_id'         => $order->cashier_id,
            'cust_id'            => $order->customer_id ?? '',
            'cust_name'          => Str::substr($order->customer_name, 0, 60) ?? '',
            'tel_code'           => $order->customer_phone ?? '', // For deposit contact phone
            'shift_id'           => 'N', // Current no shift @ default N
            'points_accumulated' => $order->getCustomData('member.points_available', 0),
            'points_redeemed'    => -1 * $order->points_used,
            'mbr_points'         => $order->points_earned,
            'utilize_status'     => 0, // 1 - when deposit payment been used
            'tax_percent'        => gst()->tax_percent,
            'promo_id'           => '',
            'promo_amt'          => .0,
            'deposit_amt'        => $order->getDepositPaid(),
            // when order type is DM, then deposit payment amount need to be filled.
            'gst_amount'         => $order->tax_amount,
            'rounding_adj'       => $order->rounding_adj,
            'voucher_adj'        => abs($order->getVoucherAdjustmentAmount()),
            'first_name'         => '',
            'last_name'          => '',
            'email_addr'         => '',
            'inv_name'           => '',
            'inv_addr'           => '',
            'email_ind'          => Str::substr($order->getCustomData('mbr_source'), 0, 1) ?? '',
            'remarks'            => '', // deposit item / refund reason / notes
            'created_on'         => $order->created_on ?? '',
            'created_by'         => $order->created_by ?? '',
            'modified_on'        => now(), // posting datetime
            'modified_by'        => 'POS',
        ];
//        dd('JAY CREATE',$orderHead);
        if (in_array($order->order_type, ['ID', 'IS', 'GS', 'DI', 'DZ'], true)) {

            $orderHead['invoice_id'] = $invoiceId;

            if ($order->order_type !== 'DZ') {
                $orderHead['ref_id'] = '';
            }
        }

        if (in_array($order->order_type, ['RF'], true)) {

            $orderHead['remarks'] = '[RF:'.$order->getCustomData('refund_reason').']';
        }


        if ($order->lines->whereIn('item_type', ['A', 'W'])->isNotEmpty()) {
            // @todo: should get details from custom_data
            $orderHead['first_name'] = Str::substr($order->customer_name, 0, 40) ?? '';
            $orderHead['last_name']  = '';
            $orderHead['email_addr'] = $order->customer_email ?? '';

        }

        $vouchers = []; // Mike !MREP-10OFF50AG

        foreach ($order->lines->sortBy('line_num') as $line) {

            $refNum = 0;

            // Details for email notification (AppleCare and StarShield warranty etc)
            if ($line->hasCustomData('form')) {
                $orderHead['first_name'] = Str::substr($line->getCustomData('form.first_name', $order->customer_name), 0, 40) ?? '';
                $orderHead['last_name']  = Str::substr($line->getCustomData('form.last_name'), 40) ?? '';
                $orderHead['email_addr'] = else_null($line->getCustomData('form.email_addr'), $orderHead['email_addr']);
            }

            if ($line->parent_id) {

                $parentLine = $order->lines->where('parent_id', $line->parent_id)->first();

                if ($parentLine) {
                    $refNum = $parentLine->line_num;
                }
            }

            // reset
            $lotLineNum = 1;

            if ($order->isDepositOrder()) {
                // Only post $DEP_MANUAL line to Cherps
                if (! Str::contains($line->item_id, '$DEP_MANUAL')) {
                    continue;
                }

                $line->line_num = 1;
            }

            if ($line->status_level === -2) {
                // deduct unit discount
                $line->unit_price += $line->unit_coupon_discount;
            }

            if ($order->order_status_level < 0) {
                if ($line->status_level >= 0 || $line->status_level === -2) {
                    $statusLevel = 0;
                } elseif ($line->status_level === -1) {
                    $statusLevel = -1;
                }
            } elseif ($order->order_status_level > 0) {
                if ($line->status_level >= 0 || $line->status_level === -2) {
                    $statusLevel = 1;
                } elseif ($line->status_level === -1) {
                    $statusLevel = -1;
                }

                if (Str::startsWith($line->item_id, '!MEMBER-RR', false)) {
                    $membershipRenewalByRebateAmount += $line->unit_price;
                    $line->unit_price                = .0;
                    $line->regular_price             = .0;
                }
            }

            // Special digital items to treat as deducted
            if (in_array($line->getCustomData('dimensions.inv_dim4',''),['MS-ESX-OFFICE'])) {
                $statusLevel = 2;
            }

            $orderLines[] = [
                'coy_id'         => $order->coy_id,
                'trans_id'       => $order->order_num,
                'line_num'       => $line->line_num,
                'item_id'        => $line->item_id,
                'item_desc'      => Str::substr($line->item_desc, 0, 60),
                'trans_qty'      => $line->qty_ordered,
                'unit_price'     => $line->unit_price,
                'regular_price'  => $line->regular_price,
                'disc_amount'    => $line->unit_discount_total,
                'disc_percent'   => .0,
                'salesperson_id' => $line->salesperson_id ?? '',
                'status_level'   => $statusLevel,
                'amend_ind'      => '',
                'promo_id'       => $line->getCustomData('tracking_id', $line->getCustomData('promo_id', '')),
                // Get from crm_promo_list
                'csd_status'     => 0,
                'csd_qty'        => $line->qty_refunded,
                'ref_num'        => $refNum,
                // when deduct inventory get the early initial date unit cost * qty (batch job run handle?)
                'trans_cost'     => .0,
                // Required to fill in when item_type is B, main item fill in 01M, and sub items fill in 01B, 02B. If more that 1 group bundle item 02M, 02B, 02B
                'bom_ind'        => '',
                'disc_rebate'    => $line->discount_total,
                'bin_id'         => '', // ignore
                'promoter_id'    => $line->promoter_id ?? '', // ignore
                'created_on'     => $line->created_on ?? '',
                'created_by'     => $line->created_by ?? '',
                'modified_on'    => now(),
                'modified_by'    => 'POS',
            ];

            if ($line->hasCustomData('custom_lot')) {
                // build lot record
                foreach ($line->getCustomData('custom_lot', []) as $index => $lot) {

                    $lotLine                 = $this->addLotLine($order, $line, $lot);
                    $lotLine['lot_line_num'] = $lotLineNum;
                    $lotLines[]              = $lotLine;

                    $lotLineNum++;
                }
            } else {
                // build lot record
                foreach ($line->getCustomData('lot_id', []) as $index => $lot) {

                    $lotLine                 = $this->addLotLine($order, $line, $lot);
                    $lotLine['lot_line_num'] = $lotLineNum;
                    $lotLines[]              = $lotLine;

                    $lotLineNum++;
                }
            }

            // @todo add lot line
            // Add multiple lines with assign unique depend on the SSEW warranty quantity
            if (in_array($line->item_type, [
                    'A', 'W',
                ]) && ! collect($lotLines)->where('line_num', $line->line_num)->count()
            ) {
                $lotLine                 = $this->addLotLine($order, $line);
                $lotLine['lot_line_num'] = $lotLineNum;
                $lotLines[]              = $lotLine;
            }

            if ($line->status_level < 0) {
                continue;
            }

            if ($line->isSecondSalespersonInvolved()) {
                $secondSalespersonLines[] = [
                    'coy_id'          => $order->coy_id,
                    'trans_id'        => $order->order_num,
                    'line_num'        => $line->line_num,
                    'salesperson_id2' => $line->second_salesperson_id,
                    'created_on'      => $line->created_on ?? '',
                    'created_by'      => $line->created_by ?? '',
                    'modified_on'     => now(),
                    'modified_by'     => 'POS',
                ];
            }

            // Mike: custom vouchers insert if any !MREP-10OFF50AG
            $vouchers = $customCommonOrder->getInsertVoucher_MREP10OFF50AG($order,$line);
        }

        // allocate serial to each SSEW record
        if ($order->lines->whereIn('item_type', ['A', 'W'])->isNotEmpty()) {
            $lotLines2 = $lotLines;

            foreach ($lotLines2 as $lotLine2) {
                foreach ($lotLines as $key => $lotLine) {
                    if ('L'.$lotLine2['line_num'] === $lotLine['lot_id']) {
                        $lotLines[$key]['lot_id'] = $lotLine2['lot_id'];
                        break;
                    }
                }
            }
        }

        // Remove unallocated lot indicator
        foreach ($lotLines as $key => $lotLine) {
            if (preg_match('/^L\d{1,3}$/', $lotLine['lot_id'])) {
                $lotLines[$key]['lot_id'] = '';
            }
        }

        $itemBomLines = $this->addItemBomLines($order);
        $orderLines = array_merge($orderLines, $itemBomLines);

        $carrierBagLines = $this->addCarrierBagLines($order, count($itemBomLines));
        $orderLines = array_merge($orderLines, $carrierBagLines);

        $lineNum = 1;

        foreach ($order->transactions->sortBy('id') as $payment) {

            if ($order->isRefundOrder() && $payment->pay_mode === 'CASH_CHANGE_DUE') {
                continue;
            }

            $payMode   = $payment->pay_mode;
            $transRef1 = $payment->card_num ?? '';
            $transRef3 = $payment->approval_code ?? '';
            $transRef4 = '';
            $finDim2   = '';
            $finDim3   = '';

            // order contain membership renewal paid by rebate need to offset the exists rebate amount
            if ($payMode === 'REBATE' && Decimal::create($membershipRenewalByRebateAmount)->gt(0)) {
                if (Decimal::create($membershipRenewalByRebateAmount)->le($payment->trans_amount)) {
                    $payment->trans_amount           -= $membershipRenewalByRebateAmount;
                    $membershipRenewalByRebateAmount = .0;
                } else {
                    $membershipRenewalByRebateAmount -= $payment->trans_amount;
                    $payment->trans_amount           = .0;
                }
            } else if ($payMode === 'ECREDIT') {
                $transRef1 = $payment->getCustomData('vouchers.0.voucher_id');
            }

            // Zero amount should skip to record
            if (Decimal::create($payment->trans_amount)->eq(0)) {
                continue;
            }

            $payMappingName = ! empty($payment->pay_type) ? $payment->pay_type : $payment->pay_mode;

            if ($payMapped = $this->paymentMapping($payMappingName)) {
                $payMode = $payMapped->func_dim1;
                $finDim2 = $payMapped->func_dim2;
            }

            if ($payment->hasCustomData('installment.month')) {
                $finDim3 = Helper::parseInstallmentMonth($payment->getCustomData('installment.month'));
            }

            $orderPaymentLines[] = [
                'coy_id'       => $order->coy_id,
                'trans_id'     => $order->order_num,
                'line_num'     => $lineNum++,
                'pay_mode'     => $payMode,
                'trans_amount' => $payment->trans_amount,
                'trans_ref1'   => $transRef1 ?? '', //mask card number
                'trans_ref2'   => '', // year
                'trans_ref3'   => $transRef3 ?? '', // approved code
                'trans_ref4'   => $transRef4 ?? '', // approved code
                'fin_dim2'     => $finDim2,
                'fin_dim3'     => $finDim3,
                'status_level' => 1,
                'created_on'   => $payment->created_on ?? '',
                'created_by'   => $payment->created_by ?? '',
                'modified_on'  => now(),
                'modified_by'  => 'POS',
            ];
        }

//        db('pg_cherps')->beginTransaction();
//        db('vc')->beginTransaction();
        
        try {

            if ($order->isRefundOrExchangeOrder()) {
                $refOrderRefundData = $order->lines->sortBy('line_num')->filter(function ($line) {
                    return $line->qty_refund > 0;
                });

                foreach ($refOrderRefundData as $line) {

                    db('pg_cherps')->table('pos_transaction_item')
                        ->where('coy_id', $order->coy_id)
                        ->where('trans_id', $order->ref_id)
                        ->where('line_num', $line->line_num)
                        ->where('item_id', $line->item_id)
                        ->update([
                            'csd_qty'     => DB::raw("csd_qty + {$line->qty_refunded}"),
                            'modified_on' => $order->modified_on ?? '',
                            'modified_by' => $order->modified_by ?? '',
                        ]);
                }
            }

            if ($order->isDepositUtiliseOrder() || ($order->isRefundOrder() && $order->lines->where('status_level', -2)->whereIn('item_id', [
                        '$DEP_MANUAL', '$DEPOSIT',
                    ])->count() > 0)
            ) {
                db('pg_cherps')->table('pos_transaction_list')
                    ->where('coy_id', $order->coy_id)
                    ->whereIn('trans_type', ['DM', 'DI'])
                    ->where('trans_id', $order->ref_id)
                    ->update([
                        'utilize_status' => 1,
                        'modified_on'    => $order->modified_on ?? '',
                        'modified_by'    => $order->modified_by ?? '',
                    ]);

                // Deposit invoice refund updates
                if ($order->lines->where('status_level', -2)->where('item_id', '$DEPOSIT')->count() > 0 && $refInvoiceId = optional($order->referenceOrder())->getCustomData('order_ref_invoice_id')) {

                    db('pg_cherps')->table('sms_invoice_list')->where('coy_id', 'CTL')->where('invoice_id', $refInvoiceId)->update([
                        'status_level' => 3,
                        'modified_on'  => $order->modified_on ?? '',
                        'modified_by'  => $order->modified_by ?? '',
                    ]);

                    db('pg_cherps')->table('sms_invoice_item')->where('coy_id', 'CTL')->where('invoice_id', $refInvoiceId)
                        ->where('status_level', '>=', 0)
                        ->update([
                            'qty_invoiced' => DB::raw('qty_invoiced * -1'),
                            'status_level' => -3,
                            'modified_on'  => $order->modified_on ?? '',
                            'modified_by'  => $order->modified_by ?? '',
                        ]);

                    db('pg_cherps')->table('sms_invoice_item')->where('coy_id', 'CTL')->where('invoice_id', $refInvoiceId)
                        ->where('status_level', '>=', 0)
                        ->update([
                            'qty_invoiced' => DB::raw('qty_invoiced * -1'),
                            'status_level' => -2,
                            'modified_on'  => $order->modified_on ?? '',
                            'modified_by'  => $order->modified_by ?? '',
                        ]);
                }
            }

            if (in_array($order->order_type, ['ID', 'IS', 'GS', 'DI', 'DZ'], true)) {

                if (in_array($order->order_type, ['ID', 'IS', 'GS', 'DZ'], true)) {

                    db('pg_cherps')->table('sms_invoice_list')
                        ->where('coy_id', coy_id())
                        ->where('invoice_id', $invoiceId)
                        ->update([
                            'status_level' => 3,
                            'modified_on'  => $order->modified_on ?? '',
                            'modified_by'  => $order->modified_by ?? '',
                        ]);

                    db('pg_cherps')->table('sms_invoice_item')
                        ->where('coy_id', coy_id())
                        ->where('invoice_id', $invoiceId)
                        ->update([
                            'status_level' => 3,
                            'modified_on'  => $order->modified_on ?? '',
                            'modified_by'  => $order->modified_by ?? '',
                        ]);

                }

                if ($order->order_type === 'DI') {
                    db('pg_cherps')->table('sms_invoice_item')
                        ->where('coy_id', coy_id())
                        ->where('invoice_id', $invoiceId)
                        ->where('item_id', '$DEPOSIT')
                        ->where('qty_invoiced', '>=', '0')
                        ->update([
                            'status_level' => 3,
                            'modified_on'  => $order->modified_on ?? '',
                            'modified_by'  => $order->modified_by ?? '',
                        ]);
                }

            }
            
            db('pg_cherps')->table('pos_transaction_list')->insert($orderHead);
            db('pg_cherps')->table('pos_transaction_item')->insert($orderLines);
            db('pg_cherps')->table('pos_trans_salesperson')->insert($secondSalespersonLines);
            db('pg_cherps')->table('pos_transaction_lot')->insert($lotLines);
            db('pg_cherps')->table('pos_payment_list')->insert($orderPaymentLines);
            
            // Mike: voucher !MREP-10OFF50AG"
            foreach ($vouchers as $voucher) {
                db('vc')->table('crm_voucher_list')->insert($voucher);
            }
            // eof Mike: voucher !MREP-10OFF50AG"
           
            /*
            // Buy voucher and use voucher need to insert a record
            // Challenger voucher need to update the sms_voucher_list
            // Use voucher_id as key to update the status level
            // 2 - voucher sold
            // 3 - voucher used

            db()->table('pos_voucher_list')->insert([
                'coy_id'         => 'CTL',
                'trans_id'       => '',
                'line_num'       => '',
                'item_id'        => '',
                'voucher_id'     => '',
                'loc_id'         => '',
                'status_level'   => 1,
                'trans_date'     => '',
                'trans_type'     => '', // P - used the cash voucher, S - Buy cash voucher
                'voucher_amount' => 0,
                'remarks'        => '',
                'created_by'     => '',
                'created_on'     => '',
                'modified_on'    => '',
                'modified_by'    => '',
            ]);

            // purchase cash voucher
            db('sql')->table('sms_voucher_list')->where('coy_id', coy_id())->where('voucher_id', '')->where('status_level', 1)->update([
                'receipt_id1'  => '', // purchase sales order number
                'sale_date'    => '',
                'loc_id'       => '',
                'status_level' => 2,
                'modified_on'  => '',
                'modified_by'  => '',
            ]);

            // use cash voucher
            db('sql')->table('sms_voucher_list')->where('coy_id', coy_id())->where('voucher_id')->where('status_level', 2)->update([
                'receipt_id2'  => '', // used sales order number
                'utilize_date' => '',
                'status_level' => 3,
                'modified_on'  => '',
                'modified_by'  => '',
            ]);
            */

//            db('pg_cherps')->commit();
//            db('vc')->commit();

            $order->is_posted = true;
            $order->save();

            return true;
        } catch
        (\Exception $e) {
            db('pg_cherps')->rollback();
            db('vc')->rollback();

            if (isset($_GET['debug'])) {
                dd($e);
            }

            throw $e;
        }
    }

    public function addLotLine(Order $order, OrderLine $line, $lot = null)
    {

        $lotId   = $lot;
        $lotId2  = '';
        $strUdf1 = $order->order_num;
        $strUdf2 = '';
        $strUdf3 = '';
        $dtUdf1  = $order->pay_on;

        if (is_array($lot)) {
            $lotId   = else_null(data_get($lot, 'lot_id'), '');
            $lotId2  = else_null(data_get($lot, 'lot_id2'), '');
            $strUdf1 = else_null(data_get($lot, 'string_udf1'), '');
            $strUdf2 = else_null(data_get($lot, 'string_udf2'), '');
        }

        if ($parentLine = $line->parent_line) {

            if (empty($lotId)) {
                $lotIds = $parentLine->getCustomData('lot_id', []);

                // @todo get unassigned lot id
                ! isset($lotIds[0]) ?: $lotId = $lotIds[0];
            }

            if ($line->item_type === 'W') {

                $lotId = 'L'.$parentLine->line_num;

                if ($parentProduct = $parentLine->product) {
                    $extendWarranty = $parentProduct->ext_warranty;
                    //$lotId2         = $extendWarranty !== 'N' ? '['.$extendWarranty.']' : '';
                    $lotId2 = $extendWarranty !== 'N' ? '['.$extendWarranty.'.0]' : ''; // Bad fix for [1.0] warranty period. Need find solution.
                }
            }

            $strUdf2 = $parentLine->item_id;
        }

        if ($line->item_type === 'W') {

            $cherpsService = new \App\Services\CherpsService;
            $strUdf3 = $cherpsService->sys_trans_list('SS', 'SSEW', 'ADS', '*', 8, 'NONE');

            // bought warranty for past order
            if ($line->hasCustomData('form')) {
                $lotIds = $line->getCustomData('lot_id', []);

                ! isset($lotIds[0]) ?: $lotId = $lotIds[0];

                $strUdf1 = $line->getCustomData('form.ref_order_num', $strUdf1);
                $strUdf2 = $line->getCustomData('form.ref_item_id', $strUdf2);
                $dtUdf1  = $line->getCustomData('form.ref_order_date', $dtUdf1);
            }
        }

        if (Str::length($lotId) > 30) {
            $lotId2 = Str::substr($lotId, 30);
        }

        return [
            'coy_id'        => $order->coy_id,
            'trans_id'      => $order->order_num,
            'line_num'      => $line->line_num,
            'lot_id'        => Str::substr($lotId ?? '', 0, 30), // remaining string append to lot_id2
            // ssew: extend manufacturer year in ims_item_list ext_warranty etc value [1] / [2]
            'lot_id2'       => $lotId2 ?? '',
            'status_level'  => 1,
            'string_udf1'   => $strUdf1 ?? '', // ssew: trans_id
            'string_udf2'   => $strUdf2 ?? '', // item id attach ssew
            'string_udf3'   => $strUdf3 ?? '', // ssew: call a function to generate value
            'datetime_udf1' => $dtUdf1 ?? '1900-01-01', // ssew: purchased date
            'created_on'    => $line->created_on ?? '',
            'created_by'    => $line->created_by ?? '',
            'modified_on'   => now(),
            'modified_by'   => 'POS',
        ];
    }

    public function addCarrierBagLines(Order $order, $lineNumOffset)
    {
        $lines    = [];
        $lineNum  = $order->lines->max('line_num') + $lineNumOffset + 1;
        $datetime = $order->lines->sortBy('line_num')->last()->created_on;

        if ($bags = $order->getCustomData('carrier_bag')) {

            $list = Cache::remember('table.pos_bag_list', now()->endOfDay(), function () {
                $result = db()->table('pos_function_list')->where('func_type', 'BAG')->get([
                    'func_id', 'func_name', 'func_dim1',
                ]);

                $result->map(function ($row) {

                    foreach (get_object_vars($row) as $k => $v) {
                        if (is_string($row->{$k})) {
                            $row->{$k} = trim($v);
                        }
                    }

                    return $row;
                });

                return $result;
            });

            foreach ($bags as $size => $qty) {

                if ($qty > 0 && ($bag = $list->where('func_id', 'BAG-'.$size)->first())) {

                    $lines[] = [
                        'coy_id'         => $order->coy_id,
                        'trans_id'       => $order->order_num,
                        'line_num'       => $lineNum++,
                        'item_id'        => $bag->func_dim1,
                        'item_desc'      => Str::substr($bag->func_name, 0, 60),
                        'trans_qty'      => $qty,
                        'unit_price'     => .0,
                        'regular_price'  => .0,
                        'disc_amount'    => .0,
                        'disc_percent'   => .0,
                        'salesperson_id' => '',
                        'status_level'   => 1,
                        'amend_ind'      => '',
                        'promo_id'       => '',
                        'csd_status'     => 0,
                        'csd_qty'        => .0,
                        'ref_num'        => 0,
                        'trans_cost'     => .0,
                        'bom_ind'        => 'BAG',
                        'disc_rebate'    => .0,
                        'bin_id'         => '',
                        'promoter_id'    => '',
                        'created_on'     => $datetime,
                        'created_by'     => 'POS',
                        'modified_on'    => now(),
                        'modified_by'    => 'POS',
                    ];
                }

            }
        }

        return $lines;
    }

    public function addItemBomLines(Order $order)
    {
        $lines    = [];
        $boms     = $order->lines->where('item_type','B')->sortBy('line_num')->all();
        if ($boms) {

            $lineNum  = $order->lines->max('line_num') + 1;
            $datetime = $order->lines->sortBy('line_num')->last()->created_on;

            foreach ($boms as $bom) {

                $bomList = db()->table('ims_item_bom')
                    ->join('ims_item_list', function ($join) {
                        $join->on('ims_item_bom.coy_id', '=', 'ims_item_list.coy_id')->on('ims_item_bom.item_id', '=', 'ims_item_list.item_id');
                    })->where('ims_item_bom.coy_id', $order->coy_id)->where('ims_item_bom.item_id', trim($bom->item_id))
                    ->get(['ims_item_bom.bom_item','ims_item_list.item_desc','ims_item_bom.bom_qty']);

                foreach($bomList as $itm) {
                    $lines[] = [
                        'coy_id'         => $order->coy_id,
                        'trans_id'       => $order->order_num,
                        'line_num'       => $lineNum++,
                        'item_id'        => trim($itm->bom_item),
                        'item_desc'      => Str::substr($itm->item_desc, 0, 60),
                        'trans_qty'      => $bom->qty_ordered * $itm->bom_qty,
                        'unit_price'     => .0,
                        'regular_price'  => .0,
                        'disc_amount'    => .0,
                        'disc_percent'   => .0,
                        'salesperson_id' => '',
                        'status_level'   => 1,
                        'amend_ind'      => '',
                        'promo_id'       => '',
                        'csd_status'     => 0,
                        'csd_qty'        => .0,
                        'ref_num'        => 0,
                        'trans_cost'     => .0,
                        'bom_ind'        => sprintf("%02d", $bom->line_num) . 'B',
                        'disc_rebate'    => .0,
                        'bin_id'         => '',
                        'promoter_id'    => '',
                        'created_on'     => $datetime,
                        'created_by'     => 'POS',
                        'modified_on'    => now(),
                        'modified_by'    => 'POS',
                    ];
                }

            }
        }

        return $lines;
    }

    protected function paymentMapping($name)
    {

        $data = Cache::remember('payment_mapping_list', 60, function () {
            $result = db()->table('pos_function_list')->whereIn('func_type', ['PAYMENT', 'PAYMENT1'])->get();

            $result->map(function ($row) {

                foreach (get_object_vars($row) as $k => $v) {
                    if (is_string($row->{$k})) {
                        $row->{$k} = trim($v);
                    }
                }

                return $row;
            });

            return $result;
        });

        return $data->where('func_id', $name)->first();
    }

}