<?php

namespace App\Core\Orders\Services;


use App\Core\Orders\Models\Order;
use App\Support\Str;

class CreateHachiMissingInvoice
{
    /*
    *  Post a copy to cherps for invoice
    */
    public function create(Order $order, $transaction = [])
    {

        $paymentMapping = $this->paymentMapping($transaction['pay_mode']);

        if ($paymentMapping) {
            $payMode = $paymentMapping->func_dim1;
        } else {
            $payMode = $this->fallbackPaymentMapping($transaction['pay_mode']);
        }

        $installment = '';
        $invoiceId = request()->get('invoiceId');
        $by = request()->get('by', 'Hachi');

        if (db('pg_cherps')->table('sms_invoice_list')->where('coy_id', 'CTL')->where('invoice_id', $invoiceId)->exists()) {
            return true;
        }

        $orderHead = [
            'coy_id'         => $order->coy_id,
            'invoice_id'     => $invoiceId,
            'inv_type'       => 'HI',
            'invoice_date'   => now(),
            'status_level'   => 1,
            'salesperson_id' => '',
            'cust_id'        => $order->customer_id ?? '',
            'cust_name'      => Str::substr($order->customer_name, 0, 60) ?? '',
            'cust_addr'      => '',
            'tel_code'       => $order->customer_phone ?? '', // For deposit contact phone
            'contact_person' => Str::substr($order->customer_name, 0, 30) ?? '',
            'cust_po_code'   => $transaction['trans_id'],
            'email_addr'     => '',
            'mbr_ind'        => 'Y',
            'delv_date'      => now(),
            'loc_id'         => 'HCL',
            'delv_addr'      => '',
            'curr_id'        => 'SGD',
            'exchg_rate'     => 1.0,
            'exchg_unit'     => 1,
            'payment_id'     => $payMode, // For online payment id only
            'delv_location'  => $installment, // Replace to record installment month
            'tax_id'         => gst()->tax_id,
            'tax_percent'    => gst()->tax_percent,
            'ref_id'         => $transaction['trans_id'],
            'ref_rev'        => 0,
            'fault_loc'      => 'N',
            'first_name'     => '',
            'last_name'      => '',
            'remarks'        => '', // Notes
            'created_on'     => now(),
            'created_by'     => $by,
            'modified_on'    => now(), // posting datetime
            'modified_by'    => $by,
        ];

        $orderLines[] = [
            'coy_id'         => $order->coy_id,
            'invoice_id'     => $invoiceId,
            'line_num'       => 1,
            'item_id'        => '#HI-MISSING',
            'item_desc'      => 'Transaction missing',
            'qty_invoiced'   => 1,
            'uom_id'         => 'pcs',
            'unit_price'     => $transaction['trans_amount'],
            'disc_amount'    => 0,
            'disc_percent'   => 0,
            'status_level'   => 1,
            'fin_dim1'       => '61-ONLINE SALES',
            'fin_dim2'       => 'SERVICES',
            'ref_id'         => '',
            'ref_rev'        => 0,
            'ref_num'        => 0,
            'salesperson_id' => '',
            'promo_id'       => '',
            'delv_mode_id'   => '',
            'inv_loc'        => '',
            'loc_id'         => 'HCL',
            'delv_code'      => '',
            'delv_date'      => now(),
            'created_on'     => now(),
            'created_by'     => 'Hachi',
            'modified_on'    => now(),
            'modified_by'    => 'Hachi',
        ];

        $orderPaymentLines[] = [
            'coy_id'       => $order->coy_id,
            'invoice_id'   => $invoiceId,
            'line_num'     => 1,
            'pay_mode'     => $payMode,
            'trans_amount' => $transaction['trans_amount'],
            'trans_ref1'   => $transaction['trans_ref_id'] ?? '',
            'trans_ref2'   => '',
            'trans_ref3'   => $transaction['approval_code'] ?? '', // approved code
            'fin_dim2'     => '',
            'fin_dim3'     => '', // installment month
            'status_level' => 1,
            'created_on'   => $transaction['created_on'] ?? '',
            'created_by'   => $transaction['created_by'] ?? '',
            'modified_on'  => $transaction['modified_on'] ?? now(),
            'modified_by'  => $transaction['modified_by'] ?? '',
        ];

        db('pg_cherps')->beginTransaction();

        try {

            db('pg_cherps')->table('sms_invoice_list')->insert($orderHead);
            db('pg_cherps')->table('sms_invoice_item')->insert($orderLines);
            db('pg_cherps')->table('sms_payment_list')->insert($orderPaymentLines);
            db('pg_cherps')->commit();

            return true;
        } catch (\Exception $e) {
            db('pg_cherps')->rollback();

            throw $e;
        }
    }

    protected function paymentMapping($payMode)
    {

        $result = db()->table('pos_function_list')->where('func_type', 'PAYMENT3')->get();

        if ($result) {
            $result = $result->map(function ($row) {

                foreach (get_object_vars($row) as $k => $v) {
                    if (is_string($row->{$k})) {
                        $row->{$k} = trim($v);
                    }
                }

                return $row;
            });

            $payMode = Str::upper(Str::trim($payMode));

            return $result->where('func_id', $payMode)->first();
        }

        return null;
    }

    protected function fallbackPaymentMapping($payMode)
    {
        $payMode = Str::upper(Str::trim($payMode));

        // Cherps payment code mapping
        switch ($payMode) {
            case 'AMEXEPP06':
            case 'AMEXEPP12':
                $payMode = 'AXI';
                break;
            case 'DBSIPP06':
            case 'DBSIPP12':
            case 'DBSIPP24':
                $payMode = 'DBI';
                break;        
            case 'AMEX':
                $payMode = 'AX';
                break;
            case 'WC':
                $payMode = 'IPD';
                break;
            case 'S2C2P':
                $payMode = 'CP';
                break;
            case 'WECHAT':
                $payMode = 'WCHAT';
                break;
            case 'NETSQR':
                $payMode = 'DC';
                break;
            case 'REBATE':
                $payMode = '!VCH-STAR001';
                break;
            case 'ECREDIT':
                $payMode = '!VCH-CREDIT';
                break;
            case 'EGIFT':
                $payMode = '!VCH-EGIFT';
                break;
            case 'VOUCHER_ADJ':
                $payMode = '!VCH-ADJ';
                break;
            default:
        }

        return $payMode;
    }
}