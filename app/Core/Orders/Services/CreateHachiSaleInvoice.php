<?php

namespace App\Core\Orders\Services;


use App\Core\Orders\Models\Order;
use App\Support\Carbon;
use App\Support\Str;

use App\Repositories\LogisticsRepository;
use App\Core\Orders\Services\CustomCommonOrder;
use Symfony\Component\Console\Output\ConsoleOutput;


class CreateHachiSaleInvoice
{
    /*
     *  Post a copy to cherps for invoice
     */
    public function create(Order $order)
    {
        $headLocId = '';
        $paymentId = '';
        $installment = '';
        $customCommonOrder = (new CustomCommonOrder);
        
        if (db('pg_cherps')->table('sms_invoice_list')->where('coy_id', 'CTL')->where('invoice_id', $order->invoice_id)->exists()) {
            return true;
        }

        // Support one time online payment only
        if ($payment = $order->transactions()->charged()->onlinePay()->first()) {
            $paymentMapping = $this->paymentMapping($payment->pay_mode);

            if ($paymentMapping) {
                $paymentId = $paymentMapping->func_dim1;
            }

            $installment = $payment->getCustomData('installment', '');

            if ($payment->pay_mode === 'AMEXEPP06') {
                $installment = '06 Months';
            } elseif ($payment->pay_mode === 'AMEXEPP12') {
                $installment = '12 Months';
            } elseif ($payment->pay_mode === 'DBSIPP06') {
                $installment = '06 Months';
            } elseif ($payment->pay_mode === 'DBSIPP12') {
                $installment = '12 Months';
            } elseif ($payment->pay_mode === 'DBSIPP24') {
                $installment = '24 Months';
            }
        }

        // Add staff Id
        $salespersonId = Str::substr($order->getCustomData('in_store.staff_id', ''), 0, 15);
        $approveBy = $order->getCustomData('in_store.store_id', '');

        $delvDate = null;

        $timeSlot = $order->getCustomData('delivery_date.time_slot');

        // user selected time slot
        if ($timeSlot && isset($timeSlot['date'], $timeSlot['start_time'])) {
            // check date format YYYY-MM-DD
            if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $timeSlot['date'])) {
                // use the start time only
                $delvDate = $timeSlot['date'].' '.$timeSlot['start_time'];
            }
        }

        if (empty($delvDate)) {
            $delvDate = now();
        }

        $orderHead = [
            'coy_id'         => $order->coy_id,
            'invoice_id'     => $order->invoice_id,
            'inv_type'       => 'HI',
            'invoice_date'   => $order->placed_on,
            'status_level'   => 1,
            'salesperson_id' => $salespersonId,
            'cust_id'        => $order->customer_id ?? '',
            'cust_name'      => Str::substr($order->customer_name, 0, 60) ?? '',
            'cust_addr'      => '0-PRIMARY',
            'tel_code'       => $order->customer_phone ?? '', // For deposit contact phone
            'contact_person' => Str::substr($order->customer_name, 0, 30) ?? '',
            'cust_po_code'   => $order->order_num,
            'email_addr'     => $order->customer_email ?? '',
            'mbr_ind'        => 'Y',
            'delv_date'      => $delvDate,
            'loc_id'         => '',
            'delv_addr'      => $order->hasShippingAddress() ? '2-DELIVERY' : '', // If got shipping
            'curr_id'        => 'SGD',
            'exchg_rate'     => 1.0,
            'exchg_unit'     => 1,
            'payment_id'     => $paymentId, // For online payment id only
            'delv_location'  => $installment, // Replace to record installment month
            'tax_id'         => gst()->tax_id,
            'tax_percent'    => gst()->tax_percent,
            'approve_by'     => $approveBy,
            'ref_id'         => $order->order_num,
            'ref_rev'        => 0,
            'fault_loc'      => 'N',
            'first_name'     => '',
            'last_name'      => '',
            'remarks'        => '', // Notes
            'created_on'     => $order->created_on ?? '',
            'created_by'     => $order->created_by ?? '',
            'modified_on'    => now(), // posting datetime
            'modified_by'    => 'Hachi',
        ];

        $orderLines = [];
        $delvCode = (string)random_int(1000, 9999); // delivery verification code

        $orderInvLines = db()->table('o2o_order_item_inv')->where('order_num', $order->order_num)->get();

        // Assign driver to promoter_id for Picking List printing
        $promoter_id = with(new LogisticsRepository)->assignDriver(optional($order->shipping_address)->postal_code);
        $vouchers = []; // Mike !MREP-10OFF50AG

        foreach ($orderInvLines->sortBy('inv_line_num') as $orderInvLine) {
            $line = $order->lines->where('line_num', $orderInvLine->line_num)->first();
            $discountTotal = $line->discount_total;
            $finDim2 = '';
            $statusLevel = 1;

            if ($product = $line->product) {
                // Use inv_dim1 instead fin_dim2
                $finDim2 = optional($product)->inv_dim1 ?? '';
            }

            if ($line->discount_total > 0 && $line->qty_ordered > $orderInvLine->qty_allocated) {
                $discountTotal = ($line->discount_total / $line->qty_ordered) * $orderInvLine->qty_allocated;
            }

            $orderLines[] = [
                'coy_id'         => $order->coy_id,
                'invoice_id'     => $order->invoice_id,
                'line_num'       => $orderInvLine->inv_line_num,
                'item_id'        => $line->item_id,
                'item_desc'      => Str::substr($line->item_desc, 0, 60),
                'qty_invoiced'   => $orderInvLine->qty_allocated,
                'uom_id'         => 'pcs',
                'unit_price'     => $line->unit_price,
                'disc_amount'    => $discountTotal,
                'disc_percent'   => 0,
                'status_level'   => $statusLevel,
                'fin_dim1'       => '61-ONLINE SALES',
                'fin_dim2'       => $finDim2,
                'ref_id'         => $order->order_num,
                'ref_rev'        => 0,
                'ref_num'        => $orderInvLine->inv_ref_line_num,
                'salesperson_id' => $salespersonId,
                'promo_id'       => $line->getCustomData('tracking_id', $line->getCustomData('promo_id', '')),
                'delv_mode_id'   => $orderInvLine->delv_mode_id ?? '',
                'inv_loc'        => $orderInvLine->inv_loc ?? '',
                'loc_id'         => $orderInvLine->loc_id ?? '',
                'delv_code'      => $delvCode, // Delivery collection code for verification
                'delv_date'      => now(),
                'promoter_id'    => $promoter_id,
                'created_on'     => $line->created_on ?? '',
                'created_by'     => $line->created_by ?? '',
                'modified_on'    => $line->modified_on ?? '',
                'modified_by'    => $line->modified_by ?? '',
            ];

            // Mike: custom vouchers insert if any !MREP-10OFF50AG
            $vouchers = $customCommonOrder->getInsertVoucher_MREP10OFF50AG($order,$line);

        }

        // Etc shipping charge
        $orderLineNum = $orderInvLines->max('line_num');
        $extraLines = $order->lines->where('line_num', '>', $orderLineNum);

        if ($extraLines->isNotEmpty()) {
            $orderInvLineNum = $orderInvLines->max('inv_line_num');

            foreach ($extraLines as $line) {

                $orderInvLineNum++;

                $finDim2 = '';
                $product = $line->product;

                if ($product) {
                    $finDim2 = optional($product)->fin_dim2 ?? '';
                }

                if (empty($finDim2)) {
                    if ($line->item_id === '#DELV_CHG') {
                        $finDim2 = 'SERVICES';
                    }
                }

                $orderLines[] = [
                    'coy_id'         => $order->coy_id,
                    'invoice_id'     => $order->invoice_id,
                    'line_num'       => $orderInvLineNum,
                    'item_id'        => $line->item_id,
                    'item_desc'      => Str::substr($line->item_desc, 0, 60),
                    'qty_invoiced'   => $line->qty_ordered,
                    'uom_id'         => 'pcs',
                    'unit_price'     => $line->unit_price,
                    'disc_amount'    => 0,
                    'disc_percent'   => 0,
                    'status_level'   => 1,
                    'fin_dim1'       => '61-ONLINE SALES',
                    'fin_dim2'       => $finDim2,
                    'ref_id'         => $order->order_num,
                    'ref_rev'        => 0,
                    'ref_num'        => 0,
                    'salesperson_id' => '',
                    'promo_id'       => '',
                    'delv_mode_id'   => '',
                    'inv_loc'        => '',
                    'loc_id'         => '',
                    'delv_code'      => $delvCode, // Delivery collection code for verification
                    'delv_date'      => now(),
                    'promoter_id'    => $promoter_id,
                    'created_on'     => $line->created_on ?? '',
                    'created_by'     => $line->created_by ?? '',
                    'modified_on'    => $line->modified_on ?? '',
                    'modified_by'    => $line->modified_by ?? '',
                ];
            }
        }

        /* TODO: Add addItemBomLines (app/Core/Orders/Services/CreatePointOfSaleInvoice.php) for item_type='B' */

        foreach ($orderLines as $orderLine) {
            // Get the first location ID
            if (!empty($orderLine['loc_id'])) {
                $headLocId = $orderLine['loc_id'];
                break;
            }
        }

        // Not found than set the default to HCL
        if (empty($headLocId)) {
            $headLocId = 'HCL';
        }

        // If any line location ID contains HCL then set the head location Id to HCL
        if (collect($orderLines)->where('loc_id', 'HCL')->count() > 0) {
            $headLocId = 'HCL';
        }

        $orderHead['loc_id'] = $headLocId;

        $orderPaymentLines = [];
        $lineNum = 1;

        foreach ($order->transactions()->charged()->get() as $payment) {

            $finDim3 = $payment->getCustomData('installment', '');
            $paymentMapping = $this->paymentMapping($payment->pay_mode);

            if ($paymentMapping) {

                $payMode = $paymentMapping->func_dim1;

                if ($paymentMapping->func_dim2) {
                    $finDim3 = $paymentMapping->func_dim2;
                }

            } else {
                $payMode = $this->fallbackPaymentMapping($payment->pay_mode);

                if ($payment->pay_mode === 'AMEXEPP06') {
                    $finDim3 = '06 Months';
                } elseif ($payment->pay_mode === 'AMEXEPP12') {
                    $finDim3 = '12 Months';
                } elseif ($payment->pay_mode === 'DBSIPP06') {
                    $finDim3 = '06 Months';
                } elseif ($payment->pay_mode === 'DBSIPP12') {
                    $finDim3 = '12 Months';
                } elseif ($payment->pay_mode === 'DBSIPP24') {
                    $finDim3 = '24 Months';
                }
            }

            $orderPaymentLines[] = [
                'coy_id'       => $order->coy_id,
                'invoice_id'   => $order->invoice_id,
                'line_num'     => $lineNum,
                'pay_mode'     => $payMode ?? '',
                'trans_amount' => $payment->trans_amount,
                'trans_ref1'   => $payment->trans_ref_id ?? '',
                'trans_ref2'   => '',
                'trans_ref3'   => $payment->approval_code ?? '', // approved code
                'fin_dim2'     => '',
                'fin_dim3'     => $finDim3, // installment month
                'status_level' => 1,
                'created_on'   => $payment->created_on ?? '',
                'created_by'   => $payment->created_by ?? '',
                'modified_on'  => $payment->modified_on ?? now(),
                'modified_by'  => $payment->modified_by ?? '',
            ];

            $lineNum++;
        }

        $orderAddresses = [];
        $address = $this->addAddress($order->invoice_id, $order->billing_address);

        if ($address !== null) {
            $orderAddresses[] = $address;
        }

        $shippingAddress = $order->shipping_address;

        if ($shippingAddress) {
            $orderHead['contact_person'] = Str::substr($shippingAddress->recipient_name, 0, 30) ?? $orderHead['contact_person'];
            $orderHead['tel_code'] = Str::substr($shippingAddress->phone_num, 0, 60) ?? $orderHead['tel_code'];

            $timeSlot = $order->getCustomData('delivery_date.time_slot');

            if ($timeSlot && isset($timeSlot['date'], $timeSlot['start_time'], $timeSlot['end_time'])) {
                $timeSlotStr = sprintf('%s, %s - %s', date("d M Y", strtotime($timeSlot['date'])), date("g:i A", strtotime($timeSlot['start_time'])), date("g:i A", strtotime($timeSlot['end_time'])));
                $shippingAddress->full_address .= "\nDelivery: ".$timeSlotStr;
            }

        }

        $address = $this->addAddress($order->invoice_id, $shippingAddress);

        if ($address !== null) {
            $orderAddresses[] = $address;
        }

        db('pg_cherps')->beginTransaction();
        db('vc')->beginTransaction();

        try {

            db('pg_cherps')->table('sms_invoice_list')->insert($orderHead);
            db('pg_cherps')->table('sms_invoice_item')->insert($orderLines);
            db('pg_cherps')->table('sms_payment_list')->insert($orderPaymentLines);
            db('pg_cherps')->table('coy_address_book')->insert($orderAddresses);

            // Mike: voucher !MREP-10OFF50AG"
            foreach ($vouchers as $voucher) {
                db('vc')->table('crm_voucher_list')->insert($voucher);
            }
            // eof Mike: voucher !MREP-10OFF50AG"
            
            db('pg_cherps')->commit();
            db('vc')->commit();

            $order->is_posted = true;
            $order->save();

            return true;
        } catch (\Exception $e0) {
            db('pg_cherps')->rollback();
            db('vc')->rollback();
            throw $e;
        }
    }

    public function addAddress($refId, $address)
    {
        if ($address === null) {
            return null;
        }

        $addressType = '0-PRIMARY';

        if ($address->address_type === 'SHIPPING') {
            $addressType = '2-DELIVERY';
        }

        list($floor, $unit) = array_pad(explode('-', $address->address_line_2), 2, '00');

        return [
            'coy_id'       => 'CTL',
            'ref_type'     => 'CASH_INV',
            'ref_id'       => $refId,
            'addr_type'    => $addressType,
            'addr_format'  => '1',
            'street_line1' => Str::substr($address->address_line_1, 0, 30),
            'street_line2' => $floor,
            'street_line3' => $unit,
            'street_line4' => Str::substr($address->address_line_3, 0, 30) ?? '',
            'city_name'    => $address->city_name ?? '',
            'state_name'   => $address->state_name ?? '',
            'country_id'   => $address->country_id ?? '',
            'postal_code'  => $address->postal_code ?? '',
            'addr_text'    => Str::substr($address->full_address, 0, 300) ?? '',
            'created_on'   => $address->created_on ?? '',
            'created_by'   => $address->created_by ?? '',
            'modified_on'  => $address->modified_on ?? now(),
            'modified_by'  => $address->modified_by ?? '',
        ];
    }

    protected function paymentMapping($payMode)
    {

        $result = db()->table('pos_function_list')->where('func_type', 'PAYMENT3')->get();

        if ($result) {
            $result = $result->map(function ($row) {

                foreach (get_object_vars($row) as $k => $v) {
                    if (is_string($row->{$k})) {
                        $row->{$k} = trim($v);
                    }
                }

                return $row;
            });

            $payMode = Str::upper(Str::trim($payMode));

            return $result->where('func_id', $payMode)->first();
        }

        return null;
    }

    protected function fallbackPaymentMapping($payMode)
    {
        $payMode = Str::upper(Str::trim($payMode));

        // Cherps payment code mapping
        switch ($payMode) {
            case 'AMEXEPP06':
            case 'AMEXEPP12':
                $payMode = 'AXI';
                break;
            case 'DBSIPP06':
            case 'DBSIPP12':
            case 'DBSIPP24':
                $payMode = 'DBI';
                break;    
            case 'AMEX':
                $payMode = 'AX';
                break;
            case 'WC':
                $payMode = 'IPD';
                break;
            case 'S2C2P':
                $payMode = 'CP';
                break;
            case 'WECHAT':
                $payMode = 'WCHAT';
                break;
            case 'NETSQR':
                $payMode = 'DC';
                break;
            case 'REBATE':
                $payMode = '!VCH-STAR001';
                break;
            case 'ECREDIT':
                $payMode = '!VCH-CREDIT';
                break;
            case 'EGIFT':
                $payMode = '!VCH-EGIFT';
                break;
            case 'VOUCHER_ADJ':
                $payMode = '!VCH-ADJ';
                break;
            default:
        }

        return $payMode;
    }

}