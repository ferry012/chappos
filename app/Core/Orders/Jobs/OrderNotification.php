<?php

namespace App\Core\Orders\Jobs;

use App\Core\Orders\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class OrderNotification implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $order;
    protected $content = [];

    public function __construct(Order $order, $content = [])
    {
        $this->order   = $order;
        $this->content = $content;
    }

    public function handle()
    {
        $email = $this->order->customer_email ?? null;

        if(!$email){
            return;
        }

        // send mail
    }
}