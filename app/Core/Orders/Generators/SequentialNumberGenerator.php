<?php

namespace App\Core\Orders\Generators;

use App\Core\Orders\Contracts\OrderNumberGenerator;
use App\Core\Orders\Models\Order;

class SequentialNumberGenerator implements OrderNumberGenerator
{
    protected $startNum;
    protected $prefix;
    protected $padLength;
    protected $padString;

    public function __construct()
    {
        $this->startNum  = 1;
        $this->prefix    = '';
        $this->padLength = 8;
        $this->padString = '0';
    }

    public function generateNumber(Order $order = null)
    {
        $last = Order::orderBy('id', 'desc')->limit(1)->first();

        $last = $last->id ?? 0;
        $next = $this->startNum + $last;

        return sprintf('%s%s', $this->prefix,
            str_pad($next, $this->padLength, $this->padString, STR_PAD_LEFT)
        );
    }
}