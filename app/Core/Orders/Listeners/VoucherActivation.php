<?php

namespace App\Core\Orders\Listeners;

use App\Core\Orders\Events\OrderWasPaid;
use App\Support\Guzzle\ChvoicesClient;

class VoucherActivation
{
    /**
     * Handle the event.
     *
     * @param  OrderWasPaid $event
     *
     * @return void
     */
    public function handle(OrderWasPaid $event)
    {
        $order = $event->order;
        $found = false;

        foreach ($order->lines as $line) {

            if ($line->status_level >= 0 && $line->item_id === '!VCH-50') {
                $found = true;
                break;
            }

        }

        if ($found) {
            $client = new ChvoicesClient;
            $client->triggerGiftVoucher($order->order_num);
        }
    }
}