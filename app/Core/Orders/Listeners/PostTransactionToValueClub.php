<?php

namespace App\Core\Orders\Listeners;

use App\Core\Orders\Events\OrderWasPaid;
use App\Http\Resources\Orders\OrderReceiptResource;
use App\Services\EmailService;
use App\Support\Carbon;
use App\Support\Str;

class PostTransactionToValueClub
{
    /**
     * Handle the event.
     *
     * @param OrderWasPaid $event
     *
     * @return void
     */
    public function handle(OrderWasPaid $event)
    {
        $order = $event->order;

        if (!$order->isMemberPurchase() || $order->pay_status_level < 2 || $order->getCustomData('is_posted_to_valubclub', false)) {
            return;
        }

        $data = new OrderReceiptResource($order);
        // convert to array
        $data = json_decode(json_encode($data), true);

        foreach ($data['items'] as $key => $item) {
            // limit to 60 char
            $data['items'][$key]['item_desc'] = Str::substr($item['item_desc'], 0, 60);
        }

        $response = app('api')->members()->sendTransaction($order->order_num, $data);

        if ($response && $response->status_code === 200) {
            $order->setCustomData('is_posted_to_valubclub', true);
        }

        $order->save();

        $startDate = Carbon::createFromFormat('Y-m-d', '2021-11-01');
        $endDate = Carbon::createFromFormat('Y-m-d', '2021-12-31');

        if (setting('pos.enable_ssew_mailing', false) && Carbon::now()->between($startDate, $endDate) && $order->hasMembershipCard()) {

            hc_emarsys_send_star_warranty([
                "source"   => 'pos_order_was_paid',
                "trans_id" => $order->order_num,
                "email"    => $order->customer_email,
                "data"     => [
                    "r_FirstName" => $order->customer_name,
                ],
            ]);

        }

        if (setting('pos.enable_mail_intel_day_21', false)) {
            (new EmailService)->issueIntelGameCode($order);
        }

    }
}