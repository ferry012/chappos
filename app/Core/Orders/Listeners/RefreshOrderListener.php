<?php

namespace App\Core\Orders\Listeners;

use App\Core\Orders\Events\OrderSavedEvent;

class RefreshOrderListener
{
    /**
     * Handle the event.
     *
     * @param  OrderSavedEvent $event
     *
     * @return void
     */
    public function handle(OrderSavedEvent $event)
    {
        $order = $event->order;
        // If the order has been placed DO NOT alter it, no matter what.
        if ($order->placed_on) {
            return;
        }

        app('api')->orders()->calculate($order);
    }
}