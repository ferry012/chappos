<?php

namespace App\Core\Orders\Listeners;


use App\Core\Discounts\Models\DiscountRule;
use App\Core\Orders\Events\OrderWasPaid;
use App\Support\Guzzle\ChvoicesClient;
use App\Support\Str;

class UpdateCoupon
{
    /**
     * Handle the event.
     *
     * @param OrderWasPaid $event
     *
     * @return void
     */
    public function handle(OrderWasPaid $event)
    {
        $order = $event->getOrder();

        $lines = $order->lines->where('item_type', 'D')->where('status_level', '>=', 0);

        if (!$lines->isEmpty()) {

            $locId = Str::trimRight($order->loc_id);

            foreach ($lines as $line) {

                if ($discount = DiscountRule::where('coupon_id', $line->item_id)->first()) {

                    $usageStats = $discount->usage_stats;

                    $totalCount = array_get($usageStats, 'total_count', 0);
                    $totalCount++;
                    array_set($usageStats, 'total_count', $totalCount);

                    // Individual location usage counter
                    if (!empty($locId)) {
                        $usesLocations = array_get($usageStats, 'locations_count', []);

                        if (!isset($usesLocations[$locId])) {
                            // Initial value
                            $usesLocations[$locId] = 0;
                        }

                        $usesLocations[$locId]++;

                        array_set($usageStats, 'locations_count', $usesLocations);
                    }

                    $discount->usage_stats = $usageStats;

                    $discount->save();
                }

            }

        }

        $order->discounts->filter(function ($discount) {
            return $discount->coupon_type === 'DISC_VOUCHER';
        })->groupBy('coupon_id')->each(function ($discounts, $couponId) use ($order) {

            (new ChvoicesClient())->triggerBurnDiscountVoucher($order->order_num, [
                'json' => [
                    'trans_id' => $order->order_num, 'voucher_id' => $discounts->pluck('coupon_code'),
                    'item_id' => $couponId, 'usr_id' => $order->cashier_id,
                ],
            ]);
        });

    }
}