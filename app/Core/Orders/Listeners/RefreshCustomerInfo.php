<?php

namespace App\Core\Orders\Listeners;


use App\Core\Orders\Events\OrderWasPaid;
use App\Support\Str;
use Carbon\Carbon;

class RefreshCustomerInfo
{
    /**
     * Handle the event.
     *
     * @param  OrderWasPaid $event
     *
     * @return void
     */
    public function handle(OrderWasPaid $event)
    {
        $order = $event->getOrder();

        // any membership card?
        $count = $order->lines->filter(function ($line) {
            return $line->item_type === 'M' && Str::startsWith($line->item_id, '!MEMBER', false);
        })->count();

        if (! $count || $order->isGuestPurchase()) {
            return;
        }

        if ($member = app('api')->members()->getById($order->customer_id, true)) {
            $expDate = Carbon::createFromTimeString($member->exp_date)->format('d M Y');

            $order->setCustomData('member.exp_date', Str::upper($expDate));

            $order->save();
        }
    }
}