<?php

namespace App\Core\Orders\Listeners;


use App\Core\Orders\Events\OrderWasPaid;
use App\Models\StaffPurchase;
use Illuminate\Support\Carbon;
use RtLopez\Decimal;

class UpdateStaffPurchase
{
    /**
     * Handle the event.
     *
     * @param  OrderWasPaid $event
     *
     * @return void
     */
    public function handle(OrderWasPaid $event)
    {
        $order = $event->order;

        if ($order->isStaffPurchase() && ! $order->getCustomData('staff_super_purchase', false) && ! $order->getCustomData('staff_purchase_updated', false)) {

            $purchaseAmount = $order->lines->sum(function ($line) {

                $isStaffPrice = $line->getCustomData('is_staff_price', false);

                if ($line->status_level >= 0 && $isStaffPrice) {
                    return $line->unit_price * $line->qty_ordered;
                }

                return .0;
            });

            if (Decimal::create($purchaseAmount)->le(0)) {
                return;
            }

            list(, $id) = array_pad(explode('_', $order->customer_id), -2, '');

            $staff = StaffPurchase::find([
                'coy_id'         => coy_id(), 'staff_id' => $id,
                'financial_year' => Carbon::now()->format('Y'),
            ]);

            $staff->purchase_balance  -= $purchaseAmount;
            $staff->last_purchased_on = now();
            $staff->save();

            $order->setCustomData('staff_purchase_updated', true);
            $order->save();
        }
    }
}