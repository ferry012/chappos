<?php

namespace App\Core\Orders\Listeners;

use App\Core\Orders\Events\OrderWasPaid;

class PostOrderToCherps
{
    /**
     * Handle the event.
     *
     * @param  OrderWasPaid $event
     *
     * @return void
     */
    public function handle(OrderWasPaid $event)
    {
        $order = $event->order;

        if (! $order->is_posted) {

            try {
                app('api')->orders()->createSaleInvoice($order);
            } catch (\Exception $e) {

            }
        }

    }
}