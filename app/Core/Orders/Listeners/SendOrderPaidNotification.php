<?php

namespace App\Core\Orders\Listeners;

use App\Core\Orders\Events\OrderWasPaid;

class SendOrderPaidNotification
{
    /**
     * Handle the event.
     *
     * @param  OrderWasPaid $event
     *
     * @return void
     */
    public function handle(OrderWasPaid $event)
    {
        $order = $event->getOrder();

        if (! $order->placed_on) {
            return;
        }

    }
}