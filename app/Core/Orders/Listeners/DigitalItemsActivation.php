<?php

namespace App\Core\Orders\Listeners;

use App\Core\Orders\Events\OrderWasPaid;
use App\Support\Guzzle\ChvoicesClient;
use App\Support\Str;

class DigitalItemsActivation
{
    /**
     * Handle the event.
     *
     * @param  OrderWasPaid $event
     *
     * @return void
     */
    public function handle(OrderWasPaid $event)
    {
        $order         = $event->order;
        $blackhawkCall = false;
        $ingramCall    = false;
        $incommCall    = false;

        foreach ($order->lines as $line) {

            if ($line->status_level >= 0 || $line->status_level === -2) {
                if (in_array($line->item_type, ['G', 'H'])) {
                    if ($line->product && Str::contains($line->product->inv_dim4, '-INCOMM', false))
                        $incommCall = true;
                    else
                        $blackhawkCall = true;
                }

                if ($line->item_type !== 'D' && $line->product && Str::contains($line->product->inv_dim4, '-ESD', false)) {
                    $ingramCall = true;
                }
            }

        }

        $client = new ChvoicesClient();

        if ($blackhawkCall) {
            $client->triggerItemsActivation($order->order_num);
        }

        if ($ingramCall) {
            $client->triggerPoGenerate($order->order_num);
        }

        if ($incommCall) {
            $client->triggerIncommActivation($order->order_num);
        }
    }

}