<?php

namespace App\Core\Payments\Models;

use App\Core\Orders\Models\Order;
use App\Core\Scaffold\BaseModel;
use App\Models\PaymentGateway;
use App\Support\Str;
use Illuminate\Database\Eloquent\Builder;

class Transaction extends BaseModel
{
    const UPDATED_AT = null;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'trans_amount' => 'float',
    ];

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'pg';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'trans_id', 'trans_ref_id', 'pay_mode', 'pay_type', 'card_num', 'approval_code', 'trans_amount', 'is_success',
        'custom_data',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     * @return Builder
     */
    protected $table = 'o2o_order_payment';

    public function order()
    {
        return $this->belongsTo(Order::class)->withoutGlobalScope('open');
    }

    public function scopeCharged(Builder $query)
    {
        // Need to include null as it cannot compare with value
        return $query->where(function ($query) {
            $query->whereNull('status_code')->orWhere(function ($query) {
                $query->where('status_code', '!=', 'voided')->Where('status_code', '!=', 'refunded');
            });
        })->where('is_success', true)->where('status_level', 1);
    }

    public function scopeOnlinePay(Builder $query)
    {
        return $query->whereIn('pay_mode', PaymentGateway::all()->pluck('pay_code'));
    }

    public function setPayTypeAttribute($value)
    {
        $this->attributes['pay_type'] = Str::upper($value);
    }
}