<?php

namespace App\Core\Payments;

use App\Core\Payments\Providers\Cash;
use App\Core\Payments\Providers\Cheque;
use App\Core\Payments\Providers\Credit;
use App\Core\Payments\Providers\Offline;
use App\Core\Payments\Providers\Voucher;
use App\Support\Manager;

class PaymentManager extends Manager
{
    /**
     * Get a driver instance.
     *
     * @param  string $driver
     *
     * @return mixed
     */
    public function with($driver)
    {
        return $this->driver($driver);
    }

    public function createOfflineDriver()
    {
        return $this->buildProvider(Offline::class);
    }

    public function createCashDriver()
    {
        return $this->buildProvider(Cash::class);
    }

    public function createChequeDriver()
    {
        return $this->buildProvider(Cheque::class);
    }

    public function createCreditDriver()
    {
        return $this->buildProvider(Credit::class);
    }

    public function createVoucherDriver()
    {
        return $this->buildProvider(Voucher::class);
    }

    public function buildProvider($provider)
    {
        return $this->app->make($provider);
    }

    public function getDefaultDriver()
    {
        // TODO: Implement getDefaultDriver() method.
    }
}