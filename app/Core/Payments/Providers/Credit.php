<?php

namespace App\Core\Payments\Providers;

use App\Core\Payments\Models\Transaction;
use App\Core\Payments\PaymentResponse;

class Credit extends AbstractProvider
{
    protected $name = 'Credit';

    public function validate($token)
    {
        return true;
    }

    public function getName()
    {
        return $this->name;
    }

    public function charge()
    {
        $response = new PaymentResponse(true);

        $response->transaction(
            $this->createSuccessTransaction($this->fields)
        );

        return $response;
    }

    public function refund($token, $amount, $description)
    {
        return true;
    }

    public function getClientToken()
    {
        return 'OFFLINE';
    }

    protected function createSuccessTransaction($content)
    {
        $transaction               = new Transaction();
        $transaction->trans_id     = $content['trans_id'];
        $transaction->trans_ref_id = array_get($content, 'trans_ref_id');
        $transaction->pay_mode     = $content['pay_mode'];
        $transaction->trans_amount = $content['trans_amount'];
        $transaction->is_success   = true;
        $transaction->setCustomData('code', array_get($content, 'code'));

        $this->order->transactions()->save($transaction);

        return $transaction;
    }
}