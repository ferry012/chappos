<?php

namespace App\Core\Payments\Providers;

use App\Core\Payments\Models\Transaction;
use App\Core\Payments\PaymentResponse;
use App\Support\Str;
use RtLopez\Decimal;

class Offline extends AbstractProvider
{

    protected $name = 'Offline';

    public function validate($token)
    {
        return true;
    }

    public function getName()
    {
        return $this->name;
    }

    public function charge()
    {
        $response = new PaymentResponse(true);

        $response->transaction(
            $this->createSuccessTransaction($this->fields)
        );

        return new PaymentResponse(true);
    }

    protected function createSuccessTransaction($content)
    {
        $transactions = [];
        $transLog = collect([]);

        foreach ($content as $payment) {

            $mode = array_get($payment, 'pay_mode');
            $amount = array_get($payment, 'trans_amount', 0);
            $payType = array_get($payment, 'pay_type');
            $approvalCode = Str::trimRight(array_get($payment, 'approval_code'));

            if (empty($mode)) {
                continue;
            }

            // Filter the duplicated record base on the criteria
            $transCount = $transLog->where('pay_type', $payType)
                ->where('approval_code', $approvalCode)
                ->where('approval_code', '!=', '')
                ->where('trans_amount', $amount)->count();
            $isTransCreated = ($transCount > 0);

            if ($isTransCreated) {
                continue;
            }

            $transLog->push([
                'pay_type'      => $payType,
                'approval_code' => $approvalCode,
                'trans_amount'  => $amount,
            ]);

            $transaction = new Transaction();
            $transaction->trans_id = array_get($payment, 'trans_id', '');
            $transaction->trans_ref_id = array_get($payment, 'trans_ref_id', '');
            $transaction->pay_mode = $mode;
            $transaction->pay_type = $payType;
            $transaction->card_num = array_get($payment, 'card_num');
            $transaction->approval_code = $approvalCode;
            $transaction->trans_date = array_get($payment, 'trans_date', now());
            $transaction->trans_amount = $amount;
            $transaction->is_success = true;
            $transaction->status_level = 1;
            $transaction->setCustomData(null, array_get($payment, 'custom_data'));
            $transaction->created_by = user_id();
            $transaction->modified_on = now();

            // Cash precautions, only adjust for new sales
            if ($transaction->pay_mode === 'CASH' && !$this->order->isRefundOrder()) {
                $transaction->trans_amount = round_to_0_or_5_cents($transaction->trans_amount);
            }

            $where = array_only($payment, [
                'trans_id',
                'trans_ref_id',
                'pay_mode',
                'trans_amount',
            ]);

            // Check is payment exists
            if (null === $this->order->transactions()->where($where)->first()
            ) {
                $transactions[] = $transaction;
            }

        }

        if ($transactions) {
            $this->order->transactions()->saveMany($transactions);
            $this->order->updateAmount();
            $baseCustomData = ['pos_printable' => false];

            // If cash is involved, need to insert the change due and rounding adjustment records
            if ($this->order->isCashInvolved()) {

                $transactions = [];
                $cash = $this->order->getCashPaid();

                if (!$this->order->isRefundOrder()) {
                    $paidAmountBeforeCash = $this->order->order_total - $this->order->getPaidAmountExceptCash();
                    $balancePayByCash = round_to_0_or_5_cents($paidAmountBeforeCash);
                    $roundingAdjustment = $paidAmountBeforeCash - $balancePayByCash;
                    $changeDue = $cash - $balancePayByCash;

                    if ($changeDue > 0 && $changeDue < $cash && $roundingAdjustment !== $changeDue) {
                        $transaction = new Transaction();
                        $transaction->trans_id = $this->order->order_num;
                        $transaction->pay_mode = 'CASH_CHANGE_DUE';
                        $transaction->trans_amount = -1 * $changeDue;
                        $transaction->is_success = true;
                        $transaction->status_level = 1;
                        $transaction->setCustomData(null, $baseCustomData);
                        $transaction->created_by = user_id();
                        $transaction->modified_on = now();
                        $transactions[] = $transaction;
                    }

                    // Not a full payment need to avoid cash rounding adjustment
                    if ($changeDue < 0 || $changeDue > $cash) {
                        $roundingAdjustment = 0.0;
                    }

                    // For Singapore cash rounding will always round down
                    if ($roundingAdjustment !== 0.0 && ($roundingAdjustment < 0.1 && $roundingAdjustment > -0.1)) {
                        // add adjustment record
                        $transaction = new Transaction();
                        $transaction->trans_id = $this->order->order_num;
                        $transaction->pay_mode = 'CASH_ROUNDING_ADJ';
                        $transaction->trans_amount = $roundingAdjustment;
                        $transaction->is_success = true;
                        $transaction->status_level = 1;
                        $transaction->setCustomData(null, $baseCustomData);
                        $transaction->created_by = user_id();
                        $transaction->modified_on = now();
                        $transactions[] = $transaction;
                    }
                } else {
                    $transaction = new Transaction();
                    $transaction->trans_id = $this->order->order_num;
                    $transaction->pay_mode = 'CASH_CHANGE_DUE';
                    $transaction->trans_amount = abs($cash);
                    $transaction->is_success = true;
                    $transaction->status_level = 1;
                    $transaction->setCustomData(null, $baseCustomData);
                    $transaction->created_by = user_id();
                    $transaction->modified_on = now();
                    $transactions[] = $transaction;
                }

                // Add records
                if ($transactions) {
                    $this->order->transactions()->saveMany($transactions);
                }

            }

            if ($this->order->isVoucherInvolved() && $this->order->updateAmount()->isOverPaid()) {

                $transaction = new Transaction();
                $transaction->trans_id = $this->order->order_num;
                $transaction->pay_mode = 'VOUCHER_ADJ';
                $transaction->trans_amount = $this->order->getOverPaidBalance();
                $transaction->is_success = true;
                $transaction->status_level = 1;
                $transaction->created_by = user_id();
                $transaction->modified_on = now();

                $this->order->transactions()->save($transaction);
            }

            return $this->order->transactions();
        }

        return new Transaction;
    }

    public function refund($token, $amount, $description)
    {
        return new PaymentResponse(true);
    }

    public function getClientToken()
    {
        return 'OFFLINE';
    }
}