<?php
/**
 * Created by PhpStorm.
 * User: GUO ZHIMING
 * Date: 16/4/2019
 * Time: 3:44 PM
 */

namespace App\Core\Payments\Providers;


use App\Core\Payments\PaymentResponse;

class Voucher extends AbstractProvider
{
    protected $name = 'Voucher';

    public function validate($token)
    {
        return true;
    }

    public function getName()
    {
        return $this->name;
    }

    public function charge()
    {
        return new PaymentResponse(true);
    }

    public function refund($token, $amount, $description)
    {
        return true;
    }

    public function getClientToken()
    {
        return 'OFFLINE';
    }
}