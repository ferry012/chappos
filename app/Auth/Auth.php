<?php

namespace App\Auth;


use  Dingo\Api\Auth\Auth as BaseAuth;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Auth extends BaseAuth
{
    /**
     * Throw the first exception from the exception stack.
     *
     * @param array $exceptionStack
     *
     * @throws \Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException
     *
     * @return void
     */
    protected function throwUnauthorizedException(array $exceptionStack)
    {
        $exception = array_shift($exceptionStack);

        if ($exception === null) {
            $exception = new UnauthorizedHttpException('', 'Failed to authenticate because of bad credentials or an invalid authorization header.');
        }

        throw $exception;
    }
}