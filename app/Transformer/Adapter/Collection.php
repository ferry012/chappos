<?php

namespace App\Transformer\Adapter;

use Dingo\Api\Contract\Transformer\Adapter;
use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Illuminate\Http\Resources\Json\ResourceCollection;


class Collection implements Adapter
{

    public function transform($response, $transformer, Binding $binding, Request $request)
    {
        $transformer->resource = $response;

        return $transformer->toArray($request);
    }
}